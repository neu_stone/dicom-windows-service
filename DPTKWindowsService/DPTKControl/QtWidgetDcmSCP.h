#ifndef QTWIDGETDCMSCP_H
#define QTWIDGETDCMSCP_H

#include <QWidget>
#include "ui_QtWidgetDcmSCP.h"

#include "Manager.h"

class QtWidgetDcmSCP : public QWidget
{
    Q_OBJECT

    friend class DPTKControl;

public:
    QtWidgetDcmSCP(QWidget *parent = 0);
    ~QtWidgetDcmSCP();

    ///.
    void setDcmServiceController(Manager* pDcmServiceController)
    {
        m_pDcmServiceController = pDcmServiceController;
    }

private slots:

    void onClickBtnGetRemoteAE();
    void onClickBtnSetRemoteAE();

    void onClickBtnAddRemoteAE();
    void onClickBtnDeleteRemoteAE();
    void onClickBtnCreateRemoteAE();

private:
    ///.
    void onNGetResponse( const DPDcmMsg& msgParam );
    void onNSetResponse( const DPDcmMsg& msgParam );

    ///.
    void onNEventRequest(const DPDcmMsg& msgParam);
    void onUnknownMessage(TYPE_DCM_MSG nType, const DPDcmMsg& msgParam);

private:

    Manager* m_pDcmServiceController;
    ///. class UID and instance UID of DICOM SCP
    QString m_strSOPClassUID, m_strSOPInstanceUID;

    Ui::QtWidgetDcmSCP ui;
};

#endif // QTWIDGETDCMSCP_H
