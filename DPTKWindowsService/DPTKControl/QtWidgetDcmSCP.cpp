#include "QtWidgetDcmSCP.h"
#include "DataSet.h"

#include <QtGui\qscrollbar.h>
#include <QtGui/qmessagebox.h>

namespace
{
    const int nNumOfElemetsInRemoteAE = 7;
    const char* pRemoteAEs[] = {
        "UserID", "AETitle", "IPAddress", "PortNumber", "UserName", "Role", "TLS"
    };
    const char* pColInWidgetRemoteAE[] = {
        "UserID", "AETitle", "IPAddress", "PortNumber", "UserName", "Role", "TLS"
    };
    enum RemoteAEColume
    {
	    RemoteAE_ID=0,
	    RemoteAE_Title,
        RemoteAE_Address,
        RemoteAE_Port,
        RemoteAE_Name,
        RemoteAE_Role,
        RemoteAE_TLS,
    };
}

QtWidgetDcmSCP::QtWidgetDcmSCP(QWidget *parent)
    : QWidget(parent)
    , m_pDcmServiceController(NULL)
    , m_strSOPClassUID("1.2.840.10008.15.0.4.4")/*UID_LDAP_dicomDevice*/
    , m_strSOPInstanceUID("")
{
    ui.setupUi(this);

	QStringList labels;

	///. table widget for remote AEs 
	ui.m_tableWidgetRemoteAE->setColumnCount(nNumOfElemetsInRemoteAE);
    for (int i=0; i<nNumOfElemetsInRemoteAE; ++i)
    {
        labels << pColInWidgetRemoteAE[i];
    }
	ui.m_tableWidgetRemoteAE->setHorizontalHeaderLabels(labels);
    for (int i=0; i<nNumOfElemetsInRemoteAE; ++i)
    {
        ui.m_tableWidgetRemoteAE->setColumnWidth(i, 64);
    }
	ui.m_tableWidgetRemoteAE->horizontalHeader()->setResizeMode(RemoteAE_ID, QHeaderView::Stretch);
	ui.m_tableWidgetRemoteAE->verticalHeader()->hide();
////    m_ui->m_tableWidgetDcmSCP->setEnabled(false);
    ui.m_tableWidgetRemoteAE->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui.m_tableWidgetRemoteAE->setSelectionMode(QAbstractItemView::SingleSelection);
}

QtWidgetDcmSCP::~QtWidgetDcmSCP()
{
    while (ui.m_tableWidgetRemoteAE->rowCount() > 0)
	{
		ui.m_tableWidgetRemoteAE->removeRow(0);
	}

    m_pDcmServiceController = NULL;
}

void QtWidgetDcmSCP::onClickBtnAddRemoteAE()
{
    int nRowIndex = ui.m_tableWidgetRemoteAE->rowCount();
    ui.m_tableWidgetRemoteAE->insertRow(nRowIndex);
    ///. UserID, AETitle, IPAddress, PortNumber, UserName, Role
    QTableWidgetItem* pQtItem;
    for (int i=0; i<nNumOfElemetsInRemoteAE; ++i)
    {
        pQtItem = new QTableWidgetItem();
        pQtItem->setText( pRemoteAEs[i] );
        ui.m_tableWidgetRemoteAE->setItem(
            nRowIndex, i, pQtItem
            );
    }
    ui.m_tableWidgetRemoteAE->selectRow(nRowIndex);
}

void QtWidgetDcmSCP::onClickBtnDeleteRemoteAE()
{
}

void QtWidgetDcmSCP::onClickBtnCreateRemoteAE()
{
    setCursor(Qt::WaitCursor);
    if (m_pDcmServiceController && m_pDcmServiceController->isWorking())
    {
        DataSet dcmDatasetAttrs;
        ///. UID_LDAP_dicomDevice
        dcmDatasetAttrs.initialize(m_strSOPClassUID.toUtf8());
        const DcmItemHandle pItemRoot = dcmDatasetAttrs.getRoot();
 //       m_pDcmServiceController->action(TYPE_MSG_N_ACTION_RQ_TURNON, m_strDicomDeviceInstanceUID.toUtf8());
        bool bReplaced(true);
        const char* pstrInstanceUID = "";
 
        //DCM_SOPInstanceUID
		DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
        dcmDatasetAttrs.setStringValue(pItemRoot, tagKey, pstrInstanceUID, bReplaced);

//        int nError = m_pDcmServiceController->get(dcmDatasetAttrs);
    }
    setCursor(Qt::ArrowCursor);
}

void QtWidgetDcmSCP::onClickBtnSetRemoteAE()
{
    setCursor(Qt::WaitCursor);
    if (m_pDcmServiceController && m_pDcmServiceController->isWorking())
    {
        ///. attributes to be set
        QItemSelectionModel *pSelection = ui.m_tableWidgetRemoteAE->selectionModel();
        if (pSelection)
        {
            DataSet dcmDatasetAttrs;
            ///. UID_LDAP_dicomDevice
            dcmDatasetAttrs.initialize(m_strSOPClassUID.toUtf8());
            const DcmItemHandle pItemRoot = dcmDatasetAttrs.getRoot();
            bool bReplaced(true), bSearchChild(false);
            long lItemNum(-2);
 
            //DCM_SOPInstanceUID
		    DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
            int nError = dcmDatasetAttrs.setStringValue(
                pItemRoot, tagKey, m_strSOPInstanceUID.toUtf8(), bReplaced
                );

            QModelIndexList indexList = pSelection->selectedRows();
            for (int i = 0; i < indexList.count(); ++i)
            {
                QModelIndex index = indexList.at(i);
                int nRowSelected = index.row();
                ///. Reference Instance seq
                tagKey.group = 0x0008, tagKey.element = 0x114a;
                DcmSequenceOfItemsHandle pSeqItemOfRefInstance = dcmDatasetAttrs.findOrCreateSequenceItem(
                    pItemRoot, tagKey, lItemNum
                    );
                if (pSeqItemOfRefInstance == NULL)
                    continue;

                ///. UserID, 
                const char* pstrSeqValueType = "TEXT";
                int nColumeNo = 0;
                ///. <element tag="0008,0104" vr="LO" vm="1" len="0" type="1" name="Code Meaning">Processing Type</element>
                tagKey.group = 0x0008, tagKey.element = 0x0104;
                nError = dcmDatasetAttrs.setStringValue(
                    pSeqItemOfRefInstance, tagKey, pRemoteAEs[nColumeNo], bReplaced
                    );
                ///. <element tag="0040,A040" vr="CS" vm="1" len="0" type="1" name="Value Type">TEXT</element>
                tagKey.group = 0x0040, tagKey.element = 0xA040;
                nError = dcmDatasetAttrs.setStringValue(
                    pSeqItemOfRefInstance, tagKey, pstrSeqValueType, bReplaced
                    );
                ///. <element tag="0008,0100" vr="SH" vm="1" len="0" type="1" name="Code Value">111724</element>
                tagKey.group = 0x0008, tagKey.element = 0x0100;
                nError = dcmDatasetAttrs.setStringValue(
                    pSeqItemOfRefInstance, tagKey,
                    ui.m_tableWidgetRemoteAE->item(nRowSelected, nColumeNo)->text().toUtf8(), 
                    bReplaced
                    );

                ///. AETitle, IPAddress, PortNumber, UserName, Role
                while (nColumeNo < nNumOfElemetsInRemoteAE-1)
                {
                     ++ nColumeNo;
                    ///. DCM_InstitutionCodeSequence
                    tagKey.group = 0x0008, tagKey.element = 0x0082;
                    const DcmItemHandle pSeqItemOfInstitutionCode = dcmDatasetAttrs.findOrCreateSequenceItem(
                        pSeqItemOfRefInstance, tagKey, lItemNum
                        );
                    if (pSeqItemOfInstitutionCode == NULL)
                        continue;

                    ///. <element tag="0008,0104" vr="LO" vm="1" len="0" type="1" name="Code Meaning">Processing Type</element>
                    tagKey.group = 0x0008, tagKey.element = 0x0104;
                    nError = dcmDatasetAttrs.setStringValue(
                        pSeqItemOfInstitutionCode, tagKey, pRemoteAEs[nColumeNo], bReplaced
                        );
                    ///. <element tag="0040,A040" vr="CS" vm="1" len="0" type="1" name="Value Type">TEXT</element>
                    tagKey.group = 0x0040, tagKey.element = 0xA040;
                    nError = dcmDatasetAttrs.setStringValue(
                        pSeqItemOfInstitutionCode, tagKey, pstrSeqValueType, bReplaced
                        );
                    ///. <element tag="0008,0100" vr="SH" vm="1" len="0" type="1" name="Code Value">111724</element>
                    tagKey.group = 0x0008, tagKey.element = 0x0100;
                    nError = dcmDatasetAttrs.setStringValue(
                        pSeqItemOfInstitutionCode, tagKey,
                        ui.m_tableWidgetRemoteAE->item(nRowSelected, nColumeNo)->text().toUtf8(), 
                        bReplaced
                        );
                }
                ///. end of while (nFieldNo < nNumOfElemetsInRemoteAE)

            }
            ///. end of for (int i = 0; i < indexList.count(); ++i)
            
            ///.
            nError = m_pDcmServiceController->set(dcmDatasetAttrs);
        }
        ///. end of if (pSelection)
    }
    else
    {
        QMessageBox::information(
            this, tr("QtWidgetDcmSCP"), tr("onClickBtnSetRemoteAE(...)")
            );
    }
    setCursor(Qt::ArrowCursor);
}

void QtWidgetDcmSCP::onClickBtnGetRemoteAE()
{
    setCursor(Qt::WaitCursor);
    if (m_pDcmServiceController && m_pDcmServiceController->isWorking())
    {
        DataSet dcmDatasetAttrs;
        ///. UID_LDAP_dicomDevice
        dcmDatasetAttrs.initialize(m_strSOPClassUID.toUtf8());
        const DcmItemHandle pItemRoot = dcmDatasetAttrs.getRoot();
        bool bReplaced(true);
        const char* pstrInstanceUID = "";
 
        //DCM_SOPInstanceUID
		DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
        dcmDatasetAttrs.setStringValue(pItemRoot, tagKey, pstrInstanceUID, bReplaced);

//        dcmDatasetAttrs.findOrCreateSequenceItem(
//            pItemRoot, tagKeySeq, lItemNum);
        int nError = m_pDcmServiceController->get(dcmDatasetAttrs);
    }
    setCursor(Qt::ArrowCursor);
}

//////////////////////////////////////////////////////////////////////////////////////////

void QtWidgetDcmSCP::onNSetResponse( const DPDcmMsg& msgParam )
{
    bool bVerbose(false);
    if (msgParam.pDatasetReq != NULL)
    {
        DataSet dcmDataset;
        int nError = dcmDataset.load(msgParam.pDatasetReq);
        if (bVerbose)
        {
            dcmDataset.save("..\\..\\temp\\QtWidgetDcmSCP.onNSetResponse.dcm", 0);
        }
    }
    ///. end of if (msgParam.pDatasetReq != NULL)

    QString strTemp = QString("NSet( Medical Device ): result#%1")
        .arg(msgParam.nTypeStatus);
    ui.m_labelResult->setText(strTemp);
}

void QtWidgetDcmSCP::onNGetResponse( const DPDcmMsg& msgParam )
{
    while (ui.m_tableWidgetRemoteAE->rowCount() > 0)
	{
		ui.m_tableWidgetRemoteAE->removeRow(0);
	}

    bool bVerbose(false);
    if (msgParam.pDatasetReq != NULL)
    {
        DataSet dcmDataset;
        int nError = dcmDataset.load(msgParam.pDatasetReq);
        if (bVerbose)
        {
            dcmDataset.save("..\\..\\temp\\QtWidgetDcmSCP.onNGetResponse.dcm", 0);
        }

        bool bFlag(false), bSearchChild(false);
        unsigned long lPos(0);
        char strValue[128];
        DcmItemHandle pItemRsp = dcmDataset.getRoot();//msgParam.pDatasetReq;

        //DCM_SOPInstanceUID
		DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
		bFlag = dcmDataset.findAndGetString(pItemRsp, tagKey, strValue, lPos, bSearchChild);
        if (bFlag)
        {
            m_strSOPInstanceUID = strValue;
        }

        ///. GENERAL PURPOSE PERFORMED PROCEDURE STEP IOD MODULES
        long lNumOfRefInstanceItems(0);
        tagKey.group = 0x0008, tagKey.element = 0x114a;
        DcmSequenceOfItemsHandle pSeqOfRefInstance = dcmDataset.findSequence(pItemRsp, tagKey, bSearchChild);
        if (pSeqOfRefInstance)
            lNumOfRefInstanceItems = dcmDataset.getNumOfItems(pSeqOfRefInstance);
        for (long nIndexOfRefInstanceItem=0; nIndexOfRefInstanceItem<lNumOfRefInstanceItems; ++nIndexOfRefInstanceItem)
        {
            DcmItemHandle pItemRefInstance = dcmDataset.getItemFromSeq(pSeqOfRefInstance, nIndexOfRefInstanceItem);
            ///. UserID, 
            ///. const char* pstrSeqCodeMeaning = "RemoteAE";
            ///. const char* pstrSeqValueType = "TEXT";
            ///. <element tag="0008,0104" vr="LO" vm="1" len="0" type="1" name="Code Meaning">Processing Type</element>
            ///. <element tag="0040,A040" vr="CS" vm="1" len="0" type="1" name="Value Type">TEXT</element>
            ///. <element tag="0008,0100" vr="SH" vm="1" len="0" type="1" name="Code Value">111724</element>
            tagKey.group = 0x0008, tagKey.element = 0x0100;
            nError = dcmDataset.findAndGetString(
                pItemRefInstance, tagKey, strValue, lPos, bSearchChild
                );
            int nRowIndex = ui.m_tableWidgetRemoteAE->rowCount();
            ui.m_tableWidgetRemoteAE->insertRow(nRowIndex);
            ///.
            QTableWidgetItem* pQtItem = new QTableWidgetItem();
            pQtItem->setText( strValue );
            ui.m_tableWidgetRemoteAE->setItem(
                nRowIndex, RemoteAE_ID, pQtItem
                );

            ///. DCM_InstitutionCodeSequence
            long lNumOfInstitutionCodeItems(0);
            tagKey.group = 0x0008, tagKey.element = 0x0082;
            DcmSequenceOfItemsHandle pSeqOfInstitutionCode = dcmDataset.findSequence(pItemRefInstance, tagKey, bSearchChild);
            if (pSeqOfInstitutionCode)
                lNumOfInstitutionCodeItems = dcmDataset.getNumOfItems(pSeqOfInstitutionCode);
            for (long nIndexOfInstitutionCodeItem=0; nIndexOfInstitutionCodeItem<lNumOfInstitutionCodeItems; ++nIndexOfInstitutionCodeItem)
            {
                DcmItemHandle pItemInstitutionCode = dcmDataset.getItemFromSeq(
                    pSeqOfInstitutionCode, nIndexOfInstitutionCodeItem
                    );
                ///. <element tag="0008,0100" vr="SH" vm="1" len="0" type="1" name="Code Value">111724</element>
                tagKey.group = 0x0008, tagKey.element = 0x0100;
                nError = dcmDataset.findAndGetString(
                    pItemInstitutionCode, tagKey, strValue, lPos, bSearchChild
                    );
                ///.
                pQtItem = new QTableWidgetItem();
                pQtItem->setText( strValue );
                ui.m_tableWidgetRemoteAE->setItem(
                    nRowIndex, nIndexOfInstitutionCodeItem+1, pQtItem
                    );
            }
            ///. end of for (long nIndexOfInstitutionCodeItem=0; nIndexOfInstitutionCodeItem<lNumOfInstitutionCodeItems; ++nIndexOfInstitutionCodeItem)
        }
        ///. end of for (long nIndexOfRefInstanceItem=0; nIndexOfRefInstanceItem<lNumOfRefInstanceItems; ++nIndexOfRefInstanceItem)
 
        ///. GENERAL PURPOSE PERFORMED PROCEDURE STEP IOD MODULES
        QString strCodeMeaning, strCodeValue, strDesignator;
        //DCM_PerformedStationNameCodeSequence
        tagKey.group = 0x0040, tagKey.element = 0x4028;
        DcmSequenceOfItemsHandle pSeq = dcmDataset.findSequence(pItemRsp, tagKey, bSearchChild);
        if (pSeq != NULL)
        {
            long nNumOfItems = dcmDataset.getNumOfItems(pSeq);
            for (long nIndex=0; nIndex<nNumOfItems; ++nIndex)
            {
                DcmItemHandle pItemOfSeq = dcmDataset.getItemFromSeq( pSeq, nIndex );
                //Code Meaning
                tagKey.group = 0x0008, tagKey.element = 0x0104;
		        if (dcmDataset.findAndGetString(pItemOfSeq, tagKey, strValue, lPos, bSearchChild) == true)
                {
                    strCodeMeaning = strValue;
                }
                //Code Value
                tagKey.group = 0x0008, tagKey.element = 0x0100;
		        if (dcmDataset.findAndGetString(pItemOfSeq, tagKey, strValue, lPos, bSearchChild) == true)
                {
                    strCodeValue = strValue;
                }
                //Coding Scheme Designator
                tagKey.group = 0x0008, tagKey.element = 0x0102;
                if (dcmDataset.findAndGetString(pItemOfSeq, tagKey, strValue, lPos, bSearchChild) == true)
                {
                    strDesignator = strValue;
                }
            }
            ///. end of for (long nIndex=0; nIndex<nNumOfItems; ++nIndex)
        }
        ///. end of if (pSeq != NULL)
        QString strTemp = QString("NGet( Medical Device ): result#%1, %2")
            .arg(strCodeMeaning)
            .arg(strCodeValue);
        ui.m_labelResult->setText(strTemp);
    }
    ///. end of if (msgParam.pDatasetReq != NULL)
}

void QtWidgetDcmSCP::onNEventRequest(const DPDcmMsg& msgParam)
{
//    ///. SCP Log
//    QString strTemp = QString("%1 << %2 : NEventReport Request.").
//        arg(msgParam.pstrCallingAE).
//        arg(msgParam.pstrCalledAE);
//    if (msgParam.pDatasetReq != NULL)
//    {
////        strTemp.append("\r");
//
//        DataSet dcmDatasetReq;
////        int nError = dcmDatasetReq.load(msgParam.pDatasetReq);
////        DcmItemHandle pItemReq = dcmDatasetReq.getRoot();
//        DcmItemHandle pItemReq = msgParam.pDatasetReq;
//
//        bool bFlag(false), bSearchChild(false);
//        unsigned long lPos(0);
//        char strValue[128];
//
//        ///. GENERAL PURPOSE PERFORMED PROCEDURE STEP IOD MODULES
//        //DCM_SOPInstanceUID
//		DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
//		bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
//        //if (bFlag && strlen(strValue)>0)
//        //{
//        //    strStudyInstanceUID = strValue;
//        //}
//        //else
//        //{
//        //    strStudyInstanceUID = "%";
//        //}
//        
//        //DCM_AdmissionID
//        tagKey.group = 0x0038, tagKey.element = 0x0010;
//		if (dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild) == true)
//        {
//            QString strSession = QString("\rSession#%1;").arg(strValue);
//            strTemp.append(strSession);
//        }
//        
//        //DCM_PerformedStationNameCodeSequence
//        tagKey.group = 0x0040, tagKey.element = 0x4028;
//        DcmSequenceOfItemsHandle pSeq = dcmDatasetReq.findSequence(pItemReq, tagKey, bSearchChild);
//        if (pSeq && dcmDatasetReq.getNumOfItems(pSeq)>0)
//        {
//            DcmItemHandle pItemOfSeq = dcmDatasetReq.getItemFromSeq( pSeq, lPos );
//            //Code Meaning
//            tagKey.group = 0x0008, tagKey.element = 0x0104;
//		    if (dcmDatasetReq.findAndGetString(pItemOfSeq, tagKey, strValue, lPos, bSearchChild) == true)
//            {
//                QString strCallingAE = QString("\rCallingAE#%1;").arg(strValue);
//                strTemp.append(strCallingAE);
//            }
//            //Code Value
//            tagKey.group = 0x0008, tagKey.element = 0x0100;
//		    if (dcmDatasetReq.findAndGetString(pItemOfSeq, tagKey, strValue, lPos, bSearchChild) == true)
//            {
//                QString strCallingIP = QString("\rCallingIP#%1;").arg(strValue);
//                strTemp.append(strCallingIP);
//            }
//
//            //Coding Scheme Designator
//            tagKey.group = 0x0008, tagKey.element = 0x0102;
//		    if (dcmDatasetReq.findAndGetString(pItemOfSeq, tagKey, strValue, lPos, bSearchChild) == true)
//            {
//                QString strDesignator = QString("\rDesignator#%1;").arg(strValue);
//                strTemp.append(strDesignator);
//            }
//        }
//
//        //DCM_ServiceEpisodeID
//        tagKey.group = 0x0038, tagKey.element = 0x0060;
//		if (dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild) == true)
//        {
//            QString strService = QString("\rService#%1;").arg(strValue);
//            strTemp.append(strService);
//        }
//        //Performed Procedure Step Start Date 0040,0244
//        tagKey.group = 0x0040, tagKey.element = 0x0244;
//        DataSet::TDcmDate dtValue;
//        if (dcmDatasetReq.findAndGetDate(pItemReq, tagKey, dtValue, lPos, bSearchChild) == true)
//        {
//            QString strService = QString("\rDate#%1-%2-%3").arg(dtValue.Year).arg(dtValue.Month).arg(dtValue.Day);
//            strTemp.append(strService);
//        }
//        //PerformedProcedureStepStartTime 0040,0245
//        tagKey.group = 0x0040, tagKey.element = 0x0245;
//        DataSet::TDcmTime tmValue;
//        if (dcmDatasetReq.findAndGetTime(pItemReq, tagKey, tmValue, lPos, bSearchChild) == true)
//        {
//            QString strService = QString("\rTime#%1:%2:%3").arg(tmValue.Hour).arg(tmValue.Minute).arg(tmValue.Second);
//            strTemp.append(strService);
//        }
//    }
//    strTemp.append("\r");
//
//    ui.m_textBrowserLogger->append(strTemp);
//
////    QScrollBar* pSB = ui.m_textBrowserLogger->verticalScrollBar();
////    pSB->setValue(pSB->maximum());
//    QTextCursor cursor = ui.m_textBrowserLogger->textCursor();
//    cursor.movePosition(QTextCursor::End);
//    ui.m_textBrowserLogger->setTextCursor(cursor);

}

void QtWidgetDcmSCP::onUnknownMessage(TYPE_DCM_MSG nType, const DPDcmMsg& msgParam)
{
    //QString strTemp = QString("%1 : service#%2, error#%3").
    //    arg(msgParam.pstrCalledAE).
    //    arg(nType).
    //    arg(msgParam.nTypeStatus);

    //ui.m_textBrowserLogger->append(strTemp);
}
