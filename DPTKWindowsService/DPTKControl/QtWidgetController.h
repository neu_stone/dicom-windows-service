#ifndef QTWIDGETCONTROLLER_H
#define QTWIDGETCONTROLLER_H

#include <QWidget>
//#include <QtCore\qqueue.h>
#include "ui_QtWidgetController.h"

#include "Manager.h"
#include "DataSet.h"

class QtWidgetController : public QWidget
{
    Q_OBJECT

    friend class DPTKControl;

public:
    QtWidgetController(QWidget *parent = 0);
    ~QtWidgetController();

    ///.
    void setDcmServiceController(Manager* pDcmServiceController)
    {
        m_pDcmServiceController = pDcmServiceController;
    }

public slots:

    void processEvent( QString strLog/*strCallingAE, QString strCalledAE, DataSet dsEvent*/ );

signals:

    void receiveEvent( QString strLog );

private slots:
    ///.
    void onClickBtnConnect();
    void onClickBtnTurnOn();
    void onClickBtnTurnOff();
    void onClickBtnCurrentSCP();

private:
    ///. DCM N Request
    void onNGetResponse( const DPDcmMsg& msgParam );
    //void onNActionResponse( const DPDcmMsg& msgParam )const;
    void onNActionResponse( int nMsgType, DPDcmMsg& msgParam );

    void onNEventRequest(const DPDcmMsg& msgParam);
    void onUnknownMessage(TYPE_DCM_MSG nType, const DPDcmMsg& msgParam);

    /////////////////////////////////////////////////////////////////
    ///.
    Manager* m_pDcmServiceController;
    QString m_strDicomDeviceInstanceUID;

    QMap<int, QString> m_mapServiceID;

//    ///.
//    mutable QQueue< DataSet* > m_queueServiceInfo;
    long m_lMaxNumOfEvents, m_lIndexOfEvent;

    ///.
    Ui::QtWidgetController ui;

};

#endif // QTWIDGETCONTROLLER_H
