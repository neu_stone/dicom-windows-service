 /****************************************************************************
 **
 ** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
 ** All rights reserved.
 ** Contact: Nokia Corporation (qt-info@nokia.com)
 **
 ** This file is part of the examples of the Qt Toolkit.
 **
 ** $QT_BEGIN_LICENSE:BSD$
 ** You may use this file under the terms of the BSD license as follows:
 **
 ** "Redistribution and use in source and binary forms, with or without
 ** modification, are permitted provided that the following conditions are
 ** met:
 **   * Redistributions of source code must retain the above copyright
 **     notice, this list of conditions and the following disclaimer.
 **   * Redistributions in binary form must reproduce the above copyright
 **     notice, this list of conditions and the following disclaimer in
 **     the documentation and/or other materials provided with the
 **     distribution.
 **   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
 **     the names of its contributors may be used to endorse or promote
 **     products derived from this software without specific prior written
 **     permission.
 **
 ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 ** $QT_END_LICENSE$
 **
 ****************************************************************************/

#include <QtGui>

#include "dptkcontrol.h"

namespace
{
    const QString pstrWndTitle = "DPTK Manager";
    const QString pstrIconFile = "..\\DPTKControl\\Resources\\AvatarDPTK?.png";
    const QSize szMainWindow = QSize(640,480);

    const QString pstrSettingsINI("DPTKService.ini");
    const QString strGroupDicom("DICOM");
//    const QString strGroupDatabase("DPTKDatabase");
    const QString strGroupUtility("DPTKUtility");

    const int nNumOfSemaphoreResources = 1;
    const int nNumOfAcquireResources = 1;
    const int nAcqTimeoutInMS = 500;
}

DPTKControl::DPTKControl(QWidget *parent, Qt::WFlags flags)
    : QDialog(parent, flags)
    , m_minimizeAction(NULL)
    , m_maximizeAction(NULL)
    , m_restoreAction(NULL)
    , m_quitAction(NULL)
    , m_pTrayIcon(NULL)
    , m_pTrayIconMenu(NULL)
    , m_pIconDPTK(NULL)
    , m_strIconFile()
    , m_pQtWidgetController(NULL)
    , m_nIndexOfTabController(-1)
    , m_pQtWidgetDcmSCP(NULL)
    , m_nIndexOfTabDcmSCP(-1)
    , m_pQtWidgetQuery(NULL)
    , m_nIndexOfTabQuery(-1)
    , m_nCurrentTabIndex(-1)
    , m_dcmSCU(this)
    , m_strServiceAE("DPDcmSCP?")
    , m_strServiceIP("127.0.0.1?")
    , m_strServicePort("1024")
    , m_strHostAE("DPDcmSCU?")
    , m_bServiceTSL(false)
    , m_semaphoreDcmSCP(nNumOfSemaphoreResources)
{
    ui.setupUi(this);
    ///.
    QSettings iniSettings(pstrSettingsINI, QSettings::IniFormat);
    if (iniSettings.status() == QSettings::NoError)
    {
        iniSettings.beginGroup(strGroupUtility);
        m_strIconFile = iniSettings.value("DPTKResource", pstrIconFile).toString();
        iniSettings.endGroup();

        iniSettings.beginGroup(strGroupDicom);
        m_strServiceAE = iniSettings.value("ControllerAE", "DPDcmSCP?").toString();
        m_strServiceIP = iniSettings.value("ControllerIP", "127.0.0.1?").toString();
        m_strServicePort = iniSettings.value("ControllerPort", "1024").toString();
        m_bServiceTSL = iniSettings.value("ControllerTSL", "1").toBool();
        m_strHostAE = iniSettings.value("HostAE", "DPDcmSCU?").toString();
        iniSettings.endGroup();
    }

//     createIconGroupBox();
//     createMessageGroupBox();

     //iconLabel->setMinimumWidth(durationLabel->sizeHint().width());

     createActions();
     createTrayIcon();

     //connect(showMessageButton, SIGNAL(clicked()), this, SLOT(showMessage()));
     //connect(
     //    showIconCheckBox, SIGNAL(toggled(bool)), m_pTrayIcon, SLOT(setVisible(bool))
     //        );
     //connect(
     //    iconComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(setIcon(int))
     //    );
     connect(
         m_pTrayIcon, SIGNAL(messageClicked()), this, SLOT(messageClicked())
         );
     connect(
         m_pTrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), 
         this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason))
         );
     ///.
     connect(
         this, SIGNAL(updateSignal(int)), this, SLOT(setIconStatus(int))
         );

     //QVBoxLayout *mainLayout = new QVBoxLayout;
     //mainLayout->addWidget(iconGroupBox);
     //mainLayout->addWidget(messageGroupBox);
     //setLayout(mainLayout);

     //iconComboBox->setCurrentIndex(1);
    m_pTrayIcon->show();

    ///.
    setWindowTitle(pstrWndTitle);
    onInitializeWidgets();

    ///. connect to DCM SCP
    int nError = m_dcmSCU.connect(
        m_strServiceAE.toStdString().c_str(),
        m_strServiceIP.toStdString().c_str(),
        m_strServicePort.toInt(),
        m_strHostAE.toStdString().c_str(), 
        m_bServiceTSL
        );
    if (nError < 0)
    {
        m_dcmSCU.close();

        setIconStatus(1);
        m_pQtWidgetController->ui.m_btnConnect->setText("connect to SCP");
        QString strTemp = QString("SCU (%1, localhost) -> SCP (%2, %3:%4) : Connect#%5.").
            arg(m_strHostAE).
            arg(m_strServiceAE).
            arg(m_strServiceIP).
            arg(m_strServicePort).
            arg(nError);
        m_pQtWidgetController->ui.m_labelResult->setText(strTemp);
   }

    ///.
    setVisible(true);
    resize(szMainWindow);
}

DPTKControl::~DPTKControl()
{
    if (m_dcmSCU.isWorking() == true)
    {
        m_dcmSCU.close();
    }

    ////////////////////////////////////////////////////////

    if (m_pTrayIcon)
    {
        delete m_pTrayIcon;
        m_pTrayIcon = NULL;
    }
    ///.
    if (m_pTrayIconMenu)
    {
        delete m_pTrayIconMenu;
        m_pTrayIconMenu = NULL;
    }
    ///.
    if (m_minimizeAction)
    {
        delete m_minimizeAction;
        m_minimizeAction = NULL;
    }
    if (m_maximizeAction)
    {
        delete m_maximizeAction;
        m_maximizeAction = NULL;
    }
    if (m_restoreAction)
    {
        delete m_restoreAction;
        m_restoreAction = NULL;
    }
    if (m_quitAction)
    {
        delete m_quitAction;
        m_quitAction = NULL;
    }
    ///.
    if (m_pIconDPTK)
    {
        delete m_pIconDPTK;
        m_pIconDPTK = NULL;
    }

    ////////////////////////////////////////////////////////
    
	if (m_pQtWidgetDcmSCP)
	{
		delete m_pQtWidgetDcmSCP;
		m_pQtWidgetDcmSCP  = NULL;
	}
	if (m_pQtWidgetController)
	{
		delete m_pQtWidgetController;
		m_pQtWidgetController  = NULL;
	}
    if (m_pQtWidgetQuery)
	{
		delete m_pQtWidgetQuery;
		m_pQtWidgetQuery  = NULL;
	}

}

void DPTKControl::onHandleSignal( int nIconStatus)
{
    emit updateSignal( nIconStatus );
}

int DPTKControl::onInitializeWidgets()
{
    int nError(0);

    ///. create DCM SCP Page
	if (m_pQtWidgetController == NULL)
	{
		m_pQtWidgetController = new QtWidgetController();
        m_pQtWidgetController->setObjectName(QString::fromUtf8("m_pQtWidgetController"));
        m_pQtWidgetController->setDcmServiceController( &m_dcmSCU );
        m_pQtWidgetController->ui.m_lineEditServiceAE->setText(m_strServiceAE);
        m_pQtWidgetController->ui.m_lineEditServiceIP->setText(m_strServiceIP);
        m_pQtWidgetController->ui.m_lineEditServicePort->setText(m_strServicePort);
        m_pQtWidgetController->ui.m_lineEditHostAE->setText(m_strHostAE);
        m_pQtWidgetController->ui.m_checkBoxTSL->setChecked(m_bServiceTSL);
	}
	m_nIndexOfTabController = ui.m_tabWidget->addTab(m_pQtWidgetController, QString("DICOM Windows Service"));
    ///. create DCM SCP Page
    if (m_pQtWidgetDcmSCP == NULL)
    {
	    m_pQtWidgetDcmSCP = new QtWidgetDcmSCP();
        m_pQtWidgetDcmSCP->setObjectName(QString::fromUtf8("m_pQtWidgetDcmSCP"));
        m_pQtWidgetDcmSCP->setDcmServiceController( &m_dcmSCU );
    }
    m_nIndexOfTabDcmSCP = ui.m_tabWidget->addTab(m_pQtWidgetDcmSCP, QString("DICOM SCP Reporter"));
    ///. create DCM qUERY Page
    if (m_pQtWidgetQuery == NULL)
    {
	    m_pQtWidgetQuery = new QtWidgetQuery();
        m_pQtWidgetQuery->setObjectName(QString::fromUtf8("m_pQtWidgetQuery"));
        m_pQtWidgetQuery->setDcmServiceController( &m_dcmSCU );
    }
    m_nIndexOfTabDcmSCP = ui.m_tabWidget->addTab(m_pQtWidgetQuery, QString("DICOM SCP Querier"));
    
    ui.m_tabWidget->setCurrentIndex(0);
    return nError;
}

void DPTKControl::setVisible(bool visible)
{
    m_minimizeAction->setEnabled( visible );
    m_maximizeAction->setEnabled( !isMaximized() );
    m_restoreAction->setEnabled( isMaximized() || !visible );

    QDialog::setVisible(visible);
}

void DPTKControl::closeEvent(QCloseEvent *event)
{
    ///. When the user closes our application's window the application itself must not quit,
    ///. the window must be merely hidden and to maintain user friendliness 
    ///. we must inform the user that the application is still running, 
    ///. so let's redefine our closeEvent:
    if (m_pTrayIcon->isVisible())
    {
        QMessageBox::information(
            this, 
            tr("DPTK Control"),
            tr("The program will keep running in the system tray. To terminate the program, "
            "choose <b>Quit</b> in the context menu of the system tray entry.")
            );
        QDialog::hide();

        ///. Don't let the event propagate to the base class
        event->ignore();
    }
}

void DPTKControl::setIconStatus( int nStatusIndex )
{
    ///. sets the image we've embedded in our resource to be our icon: 
    ///. (the ":/resource/" indicates our resource's prefix)
    QPixmap pixmap = m_pIconDPTK->pixmap(
        QSize(22, 22),
        nStatusIndex==0/*isEnabled()*/ ? QIcon::Normal : QIcon::Disabled,
        /*isChecked() ? QIcon::On : */QIcon::Off
        );
    QIcon icon(pixmap);
    m_pTrayIcon->setIcon(icon);
    setWindowIcon(icon);
    if (nStatusIndex!=0 && m_dcmSCU.isWorking())
    {
        m_dcmSCU.close();
    }
//    m_pTrayIcon->setToolTip(iconComboBox->itemText(index));
}

void DPTKControl::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    ///. When the user left clicks the tray icon the window should reopen. 
    ///. To achieve this let's implement the slot we have previously declared and
    ///. binded to the icon's activated signal:
    switch (reason)
    {
    case QSystemTrayIcon::Trigger:
    case QSystemTrayIcon::DoubleClick:
        //iconComboBox->setCurrentIndex(
        //    (iconComboBox->currentIndex() + 1)
        //    % iconComboBox->count()
        //    );
        break;
    case QSystemTrayIcon::MiddleClick:
        showMessage();
        break;
    default:
        break;
    }
}

void DPTKControl::showMessage()
{
    //QSystemTrayIcon::MessageIcon icon = QSystemTrayIcon::MessageIcon(
    //    typeComboBox->itemData(typeComboBox->currentIndex()).toInt()
    //    );
    //m_pTrayIcon->showMessage(
    //    titleEdit->text(), bodyEdit->toPlainText(), icon, durationSpinBox->value() * 1000
    //    );
}

void DPTKControl::messageClicked()
{
    QMessageBox::information(
        0, tr("Systray"), 
        tr("Sorry, I already gave what help I could.\nMaybe you should try asking a human?")
        );
}

void DPTKControl::createActions()
{
    ///. create the context menu's actions and bind them:
    ///.  If you are wondering about the & right next to the action's name 
    ///.  it is just a convenient way to indicate which key will serve as a shortcut for that
    ///.  action and the name will automatically appear with that letter underscored on the UI.
    m_minimizeAction = new QAction(tr("Mi&nimize"), this);
    connect(
        m_minimizeAction, SIGNAL(triggered()), this, SLOT(hide())
        );
    ///.
    m_maximizeAction = new QAction(tr("Ma&ximize"), this);
    connect(
        m_maximizeAction, SIGNAL(triggered()), this, SLOT(showMaximized())
        );
    ///.
    m_restoreAction = new QAction(tr("&Restore"), this);
    connect(
        m_restoreAction, SIGNAL(triggered()), this, SLOT(showNormal())
        );
    ///.
    m_quitAction = new QAction(tr("&Quit"), this);
    connect(
        m_quitAction, SIGNAL(triggered()), qApp, SLOT(quit())
        );
}

void DPTKControl::createTrayIcon()
{
    ///. create the system tray icon, it's context menu and 
    ///. the binding of the icon's activated signal to the proper slot:

    ///. create the menu of system tray icon
    m_pTrayIconMenu = new QMenu(this);
    m_pTrayIconMenu->addAction( m_minimizeAction );
    m_pTrayIconMenu->addAction( m_maximizeAction );
    m_pTrayIconMenu->addAction( m_restoreAction );
    m_pTrayIconMenu->addSeparator();
    m_pTrayIconMenu->addAction( m_quitAction );

    ///. create system tray icon
    m_pTrayIcon = new QSystemTrayIcon(this);
    m_pTrayIcon->setContextMenu(m_pTrayIconMenu);

    ///.
    m_pIconDPTK = new QIcon(
        m_strIconFile
        );
//    m_pTrayIcon->setIcon(*m_pIconDPTK);

//    setWindowIcon(*m_pIconDPTK);
}

void DPTKControl::onTabUtilityChanged()
{
    int nCurrentIndex = ui.m_tabWidget->currentIndex();
    if (nCurrentIndex == m_nCurrentTabIndex)
    {
        return;
    }

    if (nCurrentIndex == m_nIndexOfTabController &&
        m_pQtWidgetController != NULL)
    {

        ///. N-Get
//        bool bFlag = m_dcmSCU.isWorking();
      //  const DPDcmMsg& dcmMsg = m_dcmSCU.getInfoOfDcmSCP();
      //  if (bFlag && dcmMsg.nTypeStatus == 0)
      //  {
		    //DataSet dsTemp;
      //      int nTransferSytaxFlag(-1);
	     //   int nError = dsTemp.load((const DcmItemHandle) (dcmMsg.pDatasetReq), nTransferSytaxFlag);
		    //DcmItemHandle pItemRoot = dsTemp.getRoot();

      //      bool bFlag(false), bSearchChild(false);
      //      unsigned long lPos(0);
		    //char strValue[128];

      //      //sop instance uid
		    //DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
		    //bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, 0, FALSE);
      //      QString strSOPInstanceUID = strValue;
      //  }
      //  else
      //  {
      //      m_pQtWidgetController->ui.m_tableWidgetDcmSCP->clear();
      //      m_pQtWidgetController->ui.m_tableWidgetMoveAEs->clear();
      //  }
    }
    else if (nCurrentIndex == m_nIndexOfTabDcmSCP &&
        m_pQtWidgetDcmSCP != NULL)
    {
//        bool bFlag = m_dcmSCU.isWorking();
//        int nError = m_dcmSCU.echo();
    }
    else
    {
    }
    
    m_nCurrentTabIndex = nCurrentIndex;
}

int DPTKControl::notify(TYPE_DCM_MSG nType, DPDcmMsg& msgDPDcm)const
{
	int nError(0);
    try
    {
        if (m_semaphoreDcmSCP.tryAcquire(nNumOfAcquireResources, nAcqTimeoutInMS) == true)
        {
            DPTKControl* pDPTKControl  = (DPTKControl*) this;
	        switch(nType)
	        {
	        case TYPE_MSG_ASSOCIATE_AC:
		        pDPTKControl->onAssociateAccept(msgDPDcm);
		        break;
	        case TYPE_MSG_ASSOCIATE_RJ:
		        pDPTKControl->onAssociateReject(msgDPDcm);
		        break;
	        case TYPE_MSG_RELEASE_RSP:
		        pDPTKControl->onAssociateRelease(msgDPDcm);
		        break;
            case TYPE_MSG_ABORT:
                pDPTKControl->onAbortRequest(msgDPDcm);
                break;
            case TYPE_MSG_CLOSE:
                pDPTKControl->onClose(msgDPDcm);
                break;
            ///. C-Service
	        case TYPE_MSG_C_FIND_RSP:
                m_pQtWidgetQuery->onCFindResponse(msgDPDcm);
	    	    break;
            ///. N-Service
            case TYPE_MSG_N_EVENT_REPORT_RQ:
                m_pQtWidgetController->onNEventRequest(msgDPDcm);
                break;
	        case TYPE_MSG_N_GET_RSP:
		        m_pQtWidgetDcmSCP->onNGetResponse(msgDPDcm);
		        break;
	        case TYPE_MSG_N_SET_RSP:
		        m_pQtWidgetDcmSCP->onNSetResponse(msgDPDcm);
		        break;
            case TYPE_MSG_N_ACTION_RSP:
		        m_pQtWidgetController->onNActionResponse( nType, msgDPDcm );
		        break;
            ///. non processs msg
            case TYPE_MSG_N_ACTION_RQ_TURNON:
            case TYPE_MSG_N_ACTION_RQ_TURNOFF:
	        case TYPE_MSG_C_ECHO_RSP:
	        case TYPE_MSG_C_STORE_RQ:
	        case TYPE_MSG_C_GET_RQ:
	        case TYPE_MSG_C_MOVE_RQ:
	        case TYPE_MSG_C_CANCEL_RQ:
            case TYPE_MSG_ASSOCIATE_RQ:
	        default:
                m_pQtWidgetController->onUnknownMessage(nType, msgDPDcm);
		        nError = -1;
		        break;
	        }
            ///. release semaphore
            m_semaphoreDcmSCP.release(nNumOfAcquireResources);
        }
        ///. end of if (m_semaphoreDcmSCP.tryAcquire(nNumOfAcquireResources, nAcqTimeoutInMS) == true)
    }
    catch (...)
    {
        ///. release semaphore
        m_semaphoreDcmSCP.release(nNumOfAcquireResources);
        return -1;
    }
	return nError;
}

void DPTKControl::onAssociateAccept(const DPDcmMsg& msgParam)
{
    int nIndex(0);
//    DPTKControl::setIconStatus(nIndex);
    onHandleSignal( nIndex );

    m_pQtWidgetController->ui.m_btnConnect->setText("disconnect from SCP");

    m_strServiceAE = m_pQtWidgetController->ui.m_lineEditServiceAE->text();
    m_strServiceIP = m_pQtWidgetController->ui.m_lineEditServiceIP->text();
    m_strServicePort = m_pQtWidgetController->ui.m_lineEditServicePort->text();
    m_strHostAE = m_pQtWidgetController->ui.m_lineEditHostAE->text();
    m_bServiceTSL = m_pQtWidgetController->ui.m_checkBoxTSL->isChecked();

    QString strTemp = QString("SCU (%1, localhost) -> SCP (%2, %3:%4) : Associate Accepted #%5.").
        arg( m_strHostAE ).
        arg( m_strServiceAE ).
        arg( m_strServiceIP ).
        arg( m_strServicePort ).
        arg( msgParam.nTypeStatus );
    m_pQtWidgetController->ui.m_labelResult->setText(strTemp);
}

void DPTKControl::onAssociateReject(const DPDcmMsg& msgParam)
{
    int nIndex(1);
//    DPTKControl::setIconStatus(nIndex);
    onHandleSignal( nIndex );

    m_pQtWidgetController->ui.m_btnConnect->setText("connect to SCP");

    m_strServiceAE = m_pQtWidgetController->ui.m_lineEditServiceAE->text();
    m_strServiceIP = m_pQtWidgetController->ui.m_lineEditServiceIP->text();
    m_strServicePort = m_pQtWidgetController->ui.m_lineEditServicePort->text();
    m_strHostAE = m_pQtWidgetController->ui.m_lineEditHostAE->text();
    m_bServiceTSL = m_pQtWidgetController->ui.m_checkBoxTSL->isChecked();

    QString strTemp = QString("SCU (%1, localhost) -> SCP (%2, %3:%4) : Associate Rejected #%5.").
        arg( m_strHostAE ).
        arg( m_strServiceAE ).
        arg( m_strServiceIP ).
        arg( m_strServicePort ).
        arg( msgParam.nTypeStatus );
    m_pQtWidgetController->ui.m_labelResult->setText(strTemp);
}

void DPTKControl::onAssociateRelease(const DPDcmMsg& msgParam)
{
    int nIndex(1);
    DPTKControl::setIconStatus(nIndex);

    m_pQtWidgetController->ui.m_btnConnect->setText("connect to SCP");

    QString strTemp = QString("SCU (%1, localhost) -> SCP (%2, %3:%4) : Associate Released #%5.").
        arg( m_strHostAE ).
        arg( m_strServiceAE ).
        arg( m_strServiceIP ).
        arg( m_strServicePort ).
        arg( msgParam.nTypeStatus );
    m_pQtWidgetController->ui.m_labelResult->setText(strTemp);
}

void DPTKControl::onAbortRequest(const DPDcmMsg& msgParam)
{
    int nIndex(1);
//    DPTKControl::setIconStatus(nIndex);
    onHandleSignal( nIndex );

    m_pQtWidgetController->ui.m_btnConnect->setText("connect to SCP");
    QString strTemp = QString("SCU (%1, localhost) -> SCP (%2, %3:%4) : Abort Requested #%5.").
        arg( m_strHostAE ).
        arg( m_strServiceAE ).
        arg( m_strServiceIP ).
        arg( m_strServicePort ).
        arg( msgParam.nTypeStatus );
    m_pQtWidgetController->ui.m_labelResult->setText(strTemp);
}

void DPTKControl::onClose(const DPDcmMsg& msgParam)
{
    int nIndex(1);
    onHandleSignal( nIndex );

    m_pQtWidgetController->ui.m_btnConnect->setText("connect to SCP");
    QString strTemp = QString("SCU (%1, localhost) -> SCP (%2, %3:%4) : Connection closed #%5.").
        arg( m_strHostAE ).
        arg( m_strServiceAE ).
        arg( m_strServiceIP ).
        arg( m_strServicePort ).
        arg( msgParam.nTypeStatus );
    m_pQtWidgetController->ui.m_labelResult->setText(strTemp);
}

////////////////////////////////////////////////////////////////////////
//
//void DPTKControl::createIconGroupBox()
//{
//    iconGroupBox = new QGroupBox(tr("Tray Icon"));
//
//    iconLabel = new QLabel("Icon:");
//
//    iconComboBox = new QComboBox;
//    iconComboBox->addItem(QIcon(":/images/bad.svg"), tr("Bad"));
//    iconComboBox->addItem(QIcon(":/images/heart.svg"), tr("Heart"));
//    iconComboBox->addItem(QIcon(":/images/trash.svg"), tr("Trash"));
//
//    showIconCheckBox = new QCheckBox(tr("Show icon"));
//    showIconCheckBox->setChecked(true);
//
//    QHBoxLayout *iconLayout = new QHBoxLayout;
//    iconLayout->addWidget(iconLabel);
//    iconLayout->addWidget(iconComboBox);
//    iconLayout->addStretch();
//    iconLayout->addWidget(showIconCheckBox);
//    iconGroupBox->setLayout(iconLayout);
//}
//
//void DPTKControl::createMessageGroupBox()
//{
//    messageGroupBox = new QGroupBox(tr("Balloon Message"));
//
//    typeLabel = new QLabel(tr("Type:"));
//
//    typeComboBox = new QComboBox;
//    typeComboBox->addItem(tr("None"), QSystemTrayIcon::NoIcon);
//    typeComboBox->addItem(style()->standardIcon(
//            QStyle::SP_MessageBoxInformation), tr("Information"),
//            QSystemTrayIcon::Information);
//    typeComboBox->addItem(style()->standardIcon(
//            QStyle::SP_MessageBoxWarning), tr("Warning"),
//            QSystemTrayIcon::Warning);
//    typeComboBox->addItem(style()->standardIcon(
//            QStyle::SP_MessageBoxCritical), tr("Critical"),
//            QSystemTrayIcon::Critical);
//    typeComboBox->setCurrentIndex(1);
//
//    durationLabel = new QLabel(tr("Duration:"));
//
//    durationSpinBox = new QSpinBox;
//    durationSpinBox->setRange(5, 60);
//    durationSpinBox->setSuffix(" s");
//    durationSpinBox->setValue(15);
//
//    durationWarningLabel = new QLabel(tr("(some systems might ignore this "
//                                        "hint)"));
//    durationWarningLabel->setIndent(10);
//
//    titleLabel = new QLabel(tr("Title:"));
//
//    titleEdit = new QLineEdit(tr("Cannot connect to network"));
//
//    bodyLabel = new QLabel(tr("Body:"));
//
//    bodyEdit = new QTextEdit;
//    bodyEdit->setPlainText(tr("Don't believe me. Honestly, I don't have a "
//                            "clue.\nClick this balloon for details."));
//
//    showMessageButton = new QPushButton(tr("Show Message"));
//    showMessageButton->setDefault(true);
//
//    QGridLayout *messageLayout = new QGridLayout;
//    messageLayout->addWidget(typeLabel, 0, 0);
//    messageLayout->addWidget(typeComboBox, 0, 1, 1, 2);
//    messageLayout->addWidget(durationLabel, 1, 0);
//    messageLayout->addWidget(durationSpinBox, 1, 1);
//    messageLayout->addWidget(durationWarningLabel, 1, 2, 1, 3);
//    messageLayout->addWidget(titleLabel, 2, 0);
//    messageLayout->addWidget(titleEdit, 2, 1, 1, 4);
//    messageLayout->addWidget(bodyLabel, 3, 0);
//    messageLayout->addWidget(bodyEdit, 3, 1, 2, 4);
//    messageLayout->addWidget(showMessageButton, 5, 4);
//    messageLayout->setColumnStretch(3, 1);
//    messageLayout->setRowStretch(4, 1);
//    messageGroupBox->setLayout(messageLayout);
//}
