#include "QtWidgetController.h"
#include <QtGui>

//#include "Manager.h"
//#include "DataSet.h"

namespace
{
    const int nNumOfColumesInWidgetSCU=4;
    enum DcmScuColume
    {
        DcmScuColume_AE=0,
        DcmScuColume_IPAddress,
        DcmScuColume_Action,
        DcmScuColume_AddmisionID,
    };

    const int nNumOfColumesInWidgetHistory=2;
    enum DcmHistoryColume
    {
        DcmHistoryColume_IPAddress=0,
        DcmHistoryColume_History,
    };
}

QtWidgetController::QtWidgetController(QWidget *parent)
    : QWidget(parent)
    , m_pDcmServiceController(NULL)
    , m_strDicomDeviceInstanceUID()
    , m_lMaxNumOfEvents(64)
    , m_lIndexOfEvent(0)
{
    ui.setupUi(this);
    bool bFlag = QObject::connect( this, SIGNAL(receiveEvent(QString/*,QString,DataSet*/)), this, SLOT(processEvent(QString/*,QString,DataSet*/)) );

	QStringList labels;

	///. dataset table widget
	ui.m_tableWidgetDcmSCP->setColumnCount(nNumOfColumesInWidgetSCU);
	labels << tr("Current SCU") << tr("IP address") << tr("Last Request") << tr("Session");
	ui.m_tableWidgetDcmSCP->setHorizontalHeaderLabels(labels);
	ui.m_tableWidgetDcmSCP->setColumnWidth(DcmScuColume_AE, 128);
	ui.m_tableWidgetDcmSCP->setColumnWidth(DcmScuColume_IPAddress, 128);
	ui.m_tableWidgetDcmSCP->setColumnWidth(DcmScuColume_Action, 128);
	ui.m_tableWidgetDcmSCP->setColumnWidth(DcmScuColume_AddmisionID, 12);
	ui.m_tableWidgetDcmSCP->horizontalHeader()->setResizeMode(DcmScuColume_AE, QHeaderView::Stretch);
	ui.m_tableWidgetDcmSCP->verticalHeader()->hide();
//    m_ui->m_tableWidgetDcmSCP->setEnabled(false);
    ui.m_tableWidgetDcmSCP->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui.m_tableWidgetDcmSCP->setSelectionMode(QAbstractItemView::SingleSelection);
//    ui.m_tableWidgetDcmSCP->clear();

	///. Connection History table widget
    labels.clear();
	labels << tr("Remote IP address") << tr("Log");
    ui.m_tableWidgetHistory->setColumnCount(nNumOfColumesInWidgetHistory);
	ui.m_tableWidgetHistory->setHorizontalHeaderLabels(labels);
	ui.m_tableWidgetHistory->setColumnWidth(DcmHistoryColume_IPAddress, 320);
	ui.m_tableWidgetHistory->horizontalHeader()->setResizeMode(DcmHistoryColume_IPAddress, QHeaderView::Stretch);
	ui.m_tableWidgetHistory->verticalHeader()->hide();

    ///. map for service id and its meaning
    m_mapServiceID.insert(TYPE_MSG_NOTHING, "UNKNOWN");
    m_mapServiceID.insert(TYPE_MSG_C_STORE_RQ, "C_STORE_RQ");
    m_mapServiceID.insert(TYPE_MSG_C_GET_RQ, "C_GET_RQ");
    m_mapServiceID.insert(TYPE_MSG_C_FIND_RQ, "C_FIND_RQ");
    m_mapServiceID.insert(TYPE_MSG_C_ECHO_RQ, "C_ECHO_RQ");
    m_mapServiceID.insert(TYPE_MSG_C_MOVE_RQ, "C_MOVE_RQ");

    m_mapServiceID.insert(TYPE_MSG_C_CANCEL_RQ, "C_CANCEL_RQ");

    m_mapServiceID.insert(TYPE_MSG_ASSOCIATE_RQ, "ASSOCIATE_RQ");
    m_mapServiceID.insert(TYPE_MSG_ASSOCIATE_AC, "ASSOCIATE_AC");
    m_mapServiceID.insert(TYPE_MSG_ASSOCIATE_RJ, "ASSOCIATE_RJ");
    m_mapServiceID.insert(TYPE_MSG_RELEASE_RQ, "RELEASE_RQ");
    m_mapServiceID.insert(TYPE_MSG_RELEASE_RSP, "RELEASE_RSP");
    m_mapServiceID.insert(TYPE_MSG_ABORT, "ABORT");
    m_mapServiceID.insert(TYPE_MSG_CLOSE, "CLOSE");

    m_mapServiceID.insert(TYPE_MSG_C_STORE_RSP, "C_STORE_RSP");
    m_mapServiceID.insert(TYPE_MSG_C_GET_RSP, "C_GET_RSP");
    m_mapServiceID.insert(TYPE_MSG_C_FIND_RSP, "C_FIND_RSP");
    m_mapServiceID.insert(TYPE_MSG_C_MOVE_RSP, "C_MOVE_RSP");
    m_mapServiceID.insert(TYPE_MSG_C_ECHO_RSP, "C_ECHO_RSP");

    ///. type of DPTK internal message
    m_mapServiceID.insert(TYPE_MSG_DPTK_LOCALIZER, "DPTK_LOCALIZER");
    m_mapServiceID.insert(TYPE_MSG_DPTK_DELETE_FILE, "DPTK_DELETE_FILE");
    m_mapServiceID.insert(TYPE_MSG_N_ACTION_RQ_TURNON, "N_ACTION_RQ_TURNON");
    m_mapServiceID.insert(TYPE_MSG_N_ACTION_RQ_TURNOFF, "N_ACTION_RQ_TURNOFF");

    /// so far, we do not use following messages.
    m_mapServiceID.insert(TYPE_MSG_N_EVENT_REPORT_RQ, "N_EVENT_REPORT_RQ");
    m_mapServiceID.insert(TYPE_MSG_N_EVENT_REPORT_RSP, "N_EVENT_REPORT_RSP");
    m_mapServiceID.insert(TYPE_MSG_N_GET_RQ, "N_GET_RQ");
    m_mapServiceID.insert(TYPE_MSG_N_GET_RSP, "N_GET_RSP");
    m_mapServiceID.insert(TYPE_MSG_N_SET_RQ, "N_SET_RQ");
    m_mapServiceID.insert(TYPE_MSG_N_SET_RSP, "N_SET_RSP");
    m_mapServiceID.insert(TYPE_MSG_N_ACTION_RQ, "N_ACTION_RQ");
    m_mapServiceID.insert(TYPE_MSG_N_ACTION_RSP, "N_ACTION_RSP");
    m_mapServiceID.insert(TYPE_MSG_N_CREATE_RQ, "N_CREATE_RQ");
    m_mapServiceID.insert(TYPE_MSG_N_CREATE_RSP, "N_CREATE_RSP");
    m_mapServiceID.insert(TYPE_MSG_N_DELETE_RQ, "N_DELETE_RQ");
    m_mapServiceID.insert(TYPE_MSG_N_DELETE_RSP, "N_DELETE_RSP");

}

QtWidgetController::~QtWidgetController()
{
    m_pDcmServiceController = NULL;
}

void QtWidgetController::processEvent( QString strLog )
{
    ///. SCP Log
//    QString strLogger = QString("\r%1 << %2 : NEventReport Request.")
//        .arg(strCallingAE).arg(strCalledAE);
//    
//    DataSet& dcmDatasetReq = dsEvent;
////        int nError = dcmDatasetReq.load(msgParam.pDatasetReq);
//    DcmItemHandle pItemReq = dcmDatasetReq.getRoot();
////        DcmItemHandle pItemReq = msgParam.pDatasetReq;
//    {
//        bool bFlag(false), bSearchChild(false);
//        unsigned long lPos(0);
//        char strValue[128];
//
//        int nAdmissionID(0), nServiceID(TYPE_MSG_NOTHING);
//        DataSet::TDcmDate dtValue;
//        DataSet::TDcmTime tmValue;
//        ///. GENERAL PURPOSE PERFORMED PROCEDURE STEP IOD MODULES
//        //DCM_SOPInstanceUID
//		DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
//		bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
//        //DCM_AdmissionID
//        tagKey.group = 0x0038, tagKey.element = 0x0010;
//		if (dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild) == true)
//        {
//            nAdmissionID = QString(strValue).toInt();
//        }
//        //DCM_ServiceEpisodeID
//        tagKey.group = 0x0038, tagKey.element = 0x0060;
//		if (dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild) == true)
//        {
//            nServiceID = QString(strValue).toInt();
//        }
//        //Performed Procedure Step Start Date 0040,0244
//        tagKey.group = 0x0040, tagKey.element = 0x0244;
//        if (dcmDatasetReq.findAndGetDate(pItemReq, tagKey, dtValue, lPos, bSearchChild) == true)
//        {
////            QString strService = QString("\rDate#%1-%2-%3").arg(dtValue.Year).arg(dtValue.Month).arg(dtValue.Day);
////            strTemp.append(strService);
//        }
//        //PerformedProcedureStepStartTime 0040,0245
//        tagKey.group = 0x0040, tagKey.element = 0x0245;
//        if (dcmDatasetReq.findAndGetTime(pItemReq, tagKey, tmValue, lPos, bSearchChild) == true)
//        {
////            QString strService = QString("\rTime#%1:%2:%3").arg(tmValue.Hour).arg(tmValue.Minute).arg(tmValue.Second);
////            strTemp.append(strService);
//        }
//        
//        QString strCallingAE("UnknownAE"), strCallingIP("UnknownIP"), strDesignator("UnknownDesignator");
//        //DCM_PerformedStationNameCodeSequence
//        tagKey.group = 0x0040, tagKey.element = 0x4028;
//        DcmSequenceOfItemsHandle pSeq = dcmDatasetReq.findSequence(pItemReq, tagKey, bSearchChild);
//        if (pSeq && dcmDatasetReq.getNumOfItems(pSeq)>0)
//        {
//            DcmItemHandle pItemOfSeq = dcmDatasetReq.getItemFromSeq( pSeq, lPos );
//            //Code Meaning
//            tagKey.group = 0x0008, tagKey.element = 0x0104;
//		    if (dcmDatasetReq.findAndGetString(pItemOfSeq, tagKey, strValue, lPos, bSearchChild) == true)
//            {
//                strCallingAE = strValue;
//            }
//            //Code Value
//            tagKey.group = 0x0008, tagKey.element = 0x0100;
//		    if (dcmDatasetReq.findAndGetString(pItemOfSeq, tagKey, strValue, lPos, bSearchChild) == true)
//            {
//                strCallingIP = strValue;
//            }
//            //Coding Scheme Designator
//            tagKey.group = 0x0008, tagKey.element = 0x0102;
//		    if (dcmDatasetReq.findAndGetString(pItemOfSeq, tagKey, strValue, lPos, bSearchChild) == true)
//            {
//                QString strDesignator = strValue;
//            }
//        }
//
//        ///. SCU information
//        bool bNewSCU(false);
////        strTemp = QString("%1#%2").arg(strCallingAE).arg(nAdmissionID);
//        int nCountOfSCU = ui.m_tableWidgetDcmSCP->rowCount();
//        int nIndexOfSCU = nCountOfSCU-1;
//	    for (; nIndexOfSCU>-1; --nIndexOfSCU)
//	    {
//            if (ui.m_tableWidgetDcmSCP->item(nIndexOfSCU, DcmScuColume_AddmisionID)->text().toInt() == nAdmissionID)
//            //if (ui.m_tableWidgetDcmSCP->item(nIndexOfSCU, DcmScuColume_AE)->text().compare(strCallingAE) == 0 &&
//            //    ui.m_tableWidgetDcmSCP->item(nIndexOfSCU, DcmScuColume_IPAddress)->text().compare(strCallingIP) == 0)
//		    {
//                break;
//		    }
//	    }
//        QTableWidgetItem* pQtItem;
//        if (nIndexOfSCU < 0)
//        {
//            nIndexOfSCU = nCountOfSCU;
//	        ui.m_tableWidgetDcmSCP->insertRow(nIndexOfSCU);
//            pQtItem = new QTableWidgetItem();
//            pQtItem->setText(strCallingAE);
//	        pQtItem->setFlags(pQtItem->flags() ^ Qt::ItemIsEditable);
//	        ui.m_tableWidgetDcmSCP->setItem(
//		        nIndexOfSCU, DcmScuColume_AE, pQtItem
//		        );
//            ///.    
//            pQtItem = new QTableWidgetItem();
//            pQtItem->setText(strCallingIP);
//	        pQtItem->setFlags(pQtItem->flags() ^ Qt::ItemIsEditable);
//	        ui.m_tableWidgetDcmSCP->setItem(
//		        nIndexOfSCU, DcmScuColume_IPAddress, pQtItem
//		        );
//            ///.    
//            pQtItem = new QTableWidgetItem();
//            pQtItem->setText( QString("%1").arg(nAdmissionID) );
//	        pQtItem->setFlags(pQtItem->flags() ^ Qt::ItemIsEditable);
//	        ui.m_tableWidgetDcmSCP->setItem(
//		        nIndexOfSCU, DcmScuColume_AddmisionID, pQtItem
//		        );
//            ///.
//            bNewSCU = true;
//        }
//        pQtItem = new QTableWidgetItem();
//        pQtItem->setText(m_mapServiceID[nServiceID]);
//	    pQtItem->setFlags(pQtItem->flags() ^ Qt::ItemIsEditable);
//	    ui.m_tableWidgetDcmSCP->setItem(
//		    nIndexOfSCU, DcmScuColume_Action, pQtItem
//		    );
//
//        ///. log the history
//        QString strTemp;
//        if (nServiceID == TYPE_MSG_ASSOCIATE_RQ || bNewSCU)
//        {
//            int nRowIndex = ui.m_tableWidgetHistory->rowCount();
//	        ui.m_tableWidgetHistory->insertRow(nRowIndex);
//
//            strTemp = QString("%1 @ %2").arg(strCallingAE).arg(strCallingIP);
//            QTableWidgetItem* pQtItem = new QTableWidgetItem();
//            pQtItem->setText(strTemp);
//	        pQtItem->setFlags(pQtItem->flags() ^ Qt::ItemIsEditable);
//	        ui.m_tableWidgetHistory->setItem(
//		        nRowIndex, DcmHistoryColume_IPAddress, pQtItem
//		        );
//
//            //QDateTime dtTemp = QDateTime::currentDateTime();
//            strTemp = QString("%1-%2-%3 %4:%5:%6").
//                arg(dtValue.Year).arg(dtValue.Month).arg(dtValue.Day).
//                arg(tmValue.Hour).arg(tmValue.Minute).arg(tmValue.Second);
//            pQtItem = new QTableWidgetItem();
//	        pQtItem->setText(strTemp);
//	        pQtItem->setFlags(pQtItem->flags() ^ Qt::ItemIsEditable);
//	        ui.m_tableWidgetHistory->setItem(
//		        nRowIndex, DcmHistoryColume_History, pQtItem
//		        );
//        }
//
//        strTemp = QString("\r%1-%2-%3 %4:%5:%6, %7, %8 @ %9").
//                arg(dtValue.Year).arg(dtValue.Month).arg(dtValue.Day).
//                arg(tmValue.Hour).arg(tmValue.Minute).arg(tmValue.Second).
//                arg(strCallingAE).arg(m_mapServiceID[nServiceID]).arg(nAdmissionID);
//        strLogger.append(strTemp);
//    }
//    ///. end of if (msgParam.pDatasetReq != NULL)

    if (m_lMaxNumOfEvents <= m_lIndexOfEvent)
    {
        ui.m_textBrowserLogger->clear();
        ui.m_textBrowserLogger->setText( strLog );
        m_lIndexOfEvent = 0;
    }
    else
    {
        ui.m_textBrowserLogger->append( strLog );
        ++ m_lIndexOfEvent;
        QTextCursor cursor = ui.m_textBrowserLogger->textCursor();
        cursor.movePosition(QTextCursor::End);
        ui.m_textBrowserLogger->setTextCursor(cursor);
    }

}

//////////////////////////////////////////////////////////////////

void QtWidgetController::onNGetResponse( const DPDcmMsg& msgParam )
{
    QString strLogger = QString("\r%1 << %2 : NGet Response #%3.").
        arg(msgParam.pstrCalledAE).
        arg(msgParam.pstrCallingAE).
        arg(msgParam.nTypeStatus);
    ui.m_textBrowserLogger->append(strLogger);

    bool bVerbose(false);
    if (msgParam.pDatasetReq != NULL)
    {
        DataSet dcmDataset;
        int nError = dcmDataset.load(msgParam.pDatasetReq);
        if (bVerbose)
        {
            dcmDataset.save("..\\..\\temp\\QtWidgetController.onNGetResponse.dcm", 0);
        }

        bool bFlag(false), bSearchChild(false);
        unsigned long lPos(0);
        char strValue[128];
        DcmItemHandle pItemRsp = dcmDataset.getRoot();//msgParam.pDatasetReq;

        ///. GENERAL PURPOSE PERFORMED PROCEDURE STEP IOD MODULES
        //DCM_SOPInstanceUID
		DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
		bFlag = dcmDataset.findAndGetString(pItemRsp, tagKey, strValue, lPos, bSearchChild);
        if (bFlag)
        {
            m_strDicomDeviceInstanceUID = strValue;
        }

        QString strCodeMeaning, strCodeValue, strDesignator;
        //DCM_PerformedStationNameCodeSequence
        tagKey.group = 0x0040, tagKey.element = 0x4028;
        DcmSequenceOfItemsHandle pSeq = dcmDataset.findSequence(pItemRsp, tagKey, bSearchChild);
        if (pSeq != NULL)
        {
            long nNumOfItems = dcmDataset.getNumOfItems(pSeq);
            for (long nIndex=0; nIndex<nNumOfItems; ++nIndex)
            {
                DcmItemHandle pItemOfSeq = dcmDataset.getItemFromSeq( pSeq, nIndex );
                //Code Meaning
                tagKey.group = 0x0008, tagKey.element = 0x0104;
		        if (dcmDataset.findAndGetString(pItemOfSeq, tagKey, strValue, lPos, bSearchChild) == true)
                {
                    strCodeMeaning = strValue;
                }
                //Code Value
                tagKey.group = 0x0008, tagKey.element = 0x0100;
		        if (dcmDataset.findAndGetString(pItemOfSeq, tagKey, strValue, lPos, bSearchChild) == true)
                {
                    strCodeValue = strValue;
                }
                //Coding Scheme Designator
                tagKey.group = 0x0008, tagKey.element = 0x0102;
                if (dcmDataset.findAndGetString(pItemOfSeq, tagKey, strValue, lPos, bSearchChild) == true)
                {
                    strDesignator = strValue;
                }
            }
            ///. end of for (long nIndex=0; nIndex<nNumOfItems; ++nIndex)
        }
        ///. end of if (pSeq != NULL)
        
        QString strTemp = QString("NGet( Medical Device ): result#%1, %2")
            .arg(strCodeMeaning)
            .arg(strCodeValue);
        ui.m_labelResult->setText(strTemp);
    }
    else
    {
        QString strTemp("QtWidgetController::onNGetResponse");
        ui.m_labelResult->setText(strTemp);
    }
    ///. end of if (msgParam.pDatasetReq != NULL)

}

//void QtWidgetController::onNActionResponse( const DPDcmMsg& msgParam )const
//{
//        QString strTemp("QtWidgetController::onNActionResponse");
//        ui.m_labelResult->setText(strTemp);
//}

void QtWidgetController::onNActionResponse( int nMsgType, DPDcmMsg& msgParam )
{
    QString strTemp = QString("\r%1 << %2 : NAction Response #%3.").
        arg(msgParam.pstrCalledAE).
        arg(msgParam.pstrCallingAE).
        arg(msgParam.nTypeStatus);
    ui.m_textBrowserLogger->append(strTemp);
    ///.
    if (msgParam.pDatasetReq != NULL)
    {
        DataSet dcmDatasetRsp;
//        int nError = dcmDatasetReq.load(msgParam.pDatasetReq);
//        DcmItemHandle pItemReq = dcmDatasetReq.getRoot();
        DcmItemHandle pItemReq = msgParam.pDatasetReq;

        bool bFlag(false), bSearchChild(false);
        unsigned long lPos(0);
        char strValue[128];

        int nActionID(TYPE_MSG_NOTHING), nServiceEpisodeID(TYPE_MSG_NOTHING);
        //DCM_SOPInstanceUID
		DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
		bFlag = dcmDatasetRsp.findAndGetString( pItemReq, tagKey, strValue, lPos, bSearchChild );
        ///. Scheduled Procedure Step ID
        tagKey.group = 0x0040, tagKey.element = 0x0009;
        if (dcmDatasetRsp.findAndGetString( pItemReq, tagKey, strValue, lPos, bSearchChild ) == true)
        {
            nActionID = QString(strValue).toInt();
        }
        //DCM_ServiceEpisodeID
        tagKey.group = 0x0038, tagKey.element = 0x0060;
		if (dcmDatasetRsp.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild) == true)
        {
            nServiceEpisodeID = QString(strValue).toInt();
        }

        ///.
        strTemp = QString("NAction( %1 ): result#%2, status#%3")
            .arg(m_mapServiceID[nActionID])
            .arg(msgParam.nTypeStatus)
            .arg(m_mapServiceID[nServiceEpisodeID]);
        ui.m_labelResult->setText(strTemp);
    }
}

void QtWidgetController::onNEventRequest(const DPDcmMsg& msgParam)
{
    //if (msgParam.pDatasetReq != NULL)
    //{
    //    DataSet dcmDataSet;
    //    int nError = dcmDataSet.load(msgParam.pDatasetReq);
    //    emit receiveEvent( msgParam.pstrCallingAE, msgParam.pstrCalledAE, dcmDataSet);
    //}


    ///. SCP Log
    QString strLogger = QString("\r%1 << %2 : NEventReport Request.")
        .arg(msgParam.pstrCallingAE).arg(msgParam.pstrCalledAE);
    ///.
    if (msgParam.pDatasetReq != NULL)
    {
        DataSet dcmDatasetReq;
//        int nError = dcmDatasetReq.load(msgParam.pDatasetReq);
//        DcmItemHandle pItemReq = dcmDatasetReq.getRoot();
        DcmItemHandle pItemReq = msgParam.pDatasetReq;

        bool bFlag(false), bSearchChild(false);
        unsigned long lPos(0);
        char strValue[128];

        int nAdmissionID(0), nServiceID(TYPE_MSG_NOTHING);
        DataSet::TDcmDate dtValue;
        DataSet::TDcmTime tmValue;
        ///. GENERAL PURPOSE PERFORMED PROCEDURE STEP IOD MODULES
        //DCM_SOPInstanceUID
		DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
		bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
        //DCM_AdmissionID
        tagKey.group = 0x0038, tagKey.element = 0x0010;
		if (dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild) == true)
        {
            nAdmissionID = QString(strValue).toInt();
        }
        //DCM_ServiceEpisodeID
        tagKey.group = 0x0038, tagKey.element = 0x0060;
		if (dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild) == true)
        {
            nServiceID = QString(strValue).toInt();
        }
        //Performed Procedure Step Start Date 0040,0244
        tagKey.group = 0x0040, tagKey.element = 0x0244;
        if (dcmDatasetReq.findAndGetDate(pItemReq, tagKey, dtValue, lPos, bSearchChild) == true)
        {
//            QString strService = QString("\rDate#%1-%2-%3").arg(dtValue.Year).arg(dtValue.Month).arg(dtValue.Day);
//            strTemp.append(strService);
        }
        //PerformedProcedureStepStartTime 0040,0245
        tagKey.group = 0x0040, tagKey.element = 0x0245;
        if (dcmDatasetReq.findAndGetTime(pItemReq, tagKey, tmValue, lPos, bSearchChild) == true)
        {
//            QString strService = QString("\rTime#%1:%2:%3").arg(tmValue.Hour).arg(tmValue.Minute).arg(tmValue.Second);
//            strTemp.append(strService);
        }
        
        QString strCallingAE("UnknownAE"), strCallingIP("UnknownIP"), strDesignator("UnknownDesignator");
        //DCM_PerformedStationNameCodeSequence
        tagKey.group = 0x0040, tagKey.element = 0x4028;
        DcmSequenceOfItemsHandle pSeq = dcmDatasetReq.findSequence(pItemReq, tagKey, bSearchChild);
        if (pSeq && dcmDatasetReq.getNumOfItems(pSeq)>0)
        {
            DcmItemHandle pItemOfSeq = dcmDatasetReq.getItemFromSeq( pSeq, lPos );
            //Code Meaning
            tagKey.group = 0x0008, tagKey.element = 0x0104;
		    if (dcmDatasetReq.findAndGetString(pItemOfSeq, tagKey, strValue, lPos, bSearchChild) == true)
            {
                strCallingAE = strValue;
            }
            //Code Value
            tagKey.group = 0x0008, tagKey.element = 0x0100;
		    if (dcmDatasetReq.findAndGetString(pItemOfSeq, tagKey, strValue, lPos, bSearchChild) == true)
            {
                strCallingIP = strValue;
            }
            //Coding Scheme Designator
            tagKey.group = 0x0008, tagKey.element = 0x0102;
		    if (dcmDatasetReq.findAndGetString(pItemOfSeq, tagKey, strValue, lPos, bSearchChild) == true)
            {
                QString strDesignator = strValue;
            }
        }

        ///. SCU information
        bool bNewSCU(false);
//        strTemp = QString("%1#%2").arg(strCallingAE).arg(nAdmissionID);
        int nCountOfSCU = ui.m_tableWidgetDcmSCP->rowCount();
        int nIndexOfSCU = nCountOfSCU-1;
	    for (; nIndexOfSCU>-1; --nIndexOfSCU)
	    {
            if (ui.m_tableWidgetDcmSCP->item(nIndexOfSCU, DcmScuColume_AddmisionID)->text().toInt() == nAdmissionID)
            //if (ui.m_tableWidgetDcmSCP->item(nIndexOfSCU, DcmScuColume_AE)->text().compare(strCallingAE) == 0 &&
            //    ui.m_tableWidgetDcmSCP->item(nIndexOfSCU, DcmScuColume_IPAddress)->text().compare(strCallingIP) == 0)
		    {
                break;
		    }
	    }
        QTableWidgetItem* pQtItem;
        if (nIndexOfSCU < 0)
        {
            nIndexOfSCU = nCountOfSCU;
	        ui.m_tableWidgetDcmSCP->insertRow(nIndexOfSCU);
            pQtItem = new QTableWidgetItem();
            pQtItem->setText(strCallingAE);
	        pQtItem->setFlags(pQtItem->flags() ^ Qt::ItemIsEditable);
	        ui.m_tableWidgetDcmSCP->setItem(
		        nIndexOfSCU, DcmScuColume_AE, pQtItem
		        );
            ///.    
            pQtItem = new QTableWidgetItem();
            pQtItem->setText(msgParam.pstrCallingIP);
	        pQtItem->setFlags(pQtItem->flags() ^ Qt::ItemIsEditable);
	        ui.m_tableWidgetDcmSCP->setItem(
		        nIndexOfSCU, DcmScuColume_IPAddress, pQtItem
		        );
            ///.    
            pQtItem = new QTableWidgetItem();
            pQtItem->setText( QString("%1").arg(nAdmissionID) );
	        pQtItem->setFlags(pQtItem->flags() ^ Qt::ItemIsEditable);
	        ui.m_tableWidgetDcmSCP->setItem(
		        nIndexOfSCU, DcmScuColume_AddmisionID, pQtItem
		        );
            ///.
            bNewSCU = true;
        }
        pQtItem = new QTableWidgetItem();
        pQtItem->setText(m_mapServiceID[nServiceID]);
	    pQtItem->setFlags(pQtItem->flags() ^ Qt::ItemIsEditable);
	    ui.m_tableWidgetDcmSCP->setItem(
		    nIndexOfSCU, DcmScuColume_Action, pQtItem
		    );

        ///. log the history
        QString strTemp;
        if (nServiceID == TYPE_MSG_ASSOCIATE_RQ || bNewSCU)
        {
            int nRowIndex = ui.m_tableWidgetHistory->rowCount();
	        ui.m_tableWidgetHistory->insertRow(nRowIndex);

            strTemp = QString("%1 @ %2").arg(strCallingAE).arg(strCallingIP);
            QTableWidgetItem* pQtItem = new QTableWidgetItem();
            pQtItem->setText(strTemp);
	        pQtItem->setFlags(pQtItem->flags() ^ Qt::ItemIsEditable);
	        ui.m_tableWidgetHistory->setItem(
		        nRowIndex, DcmHistoryColume_IPAddress, pQtItem
		        );

            //QDateTime dtTemp = QDateTime::currentDateTime();
            strTemp = QString("%1-%2-%3 %4:%5:%6").
                arg(dtValue.Year).arg(dtValue.Month).arg(dtValue.Day).
                arg(tmValue.Hour).arg(tmValue.Minute).arg(tmValue.Second);
            pQtItem = new QTableWidgetItem();
	        pQtItem->setText(strTemp);
	        pQtItem->setFlags(pQtItem->flags() ^ Qt::ItemIsEditable);
	        ui.m_tableWidgetHistory->setItem(
		        nRowIndex, DcmHistoryColume_History, pQtItem
		        );
        }

        strTemp = QString("\r%1-%2-%3 %4:%5:%6, %7, %8 @ %9").
                arg(dtValue.Year).arg(dtValue.Month).arg(dtValue.Day).
                arg(tmValue.Hour).arg(tmValue.Minute).arg(tmValue.Second).
                arg(strCallingAE).arg(m_mapServiceID[nServiceID]).arg(nAdmissionID);
        strLogger.append(strTemp);
    }
    ///. end of if (msgParam.pDatasetReq != NULL)

    emit receiveEvent( strLogger );
}

void QtWidgetController::onUnknownMessage(TYPE_DCM_MSG nType, const DPDcmMsg& msgParam)
{
    QString strTemp = QString("%1 : service#%2, error#%3").
        arg(msgParam.pstrCalledAE).
        arg(nType).
        arg(msgParam.nTypeStatus);

    ui.m_textBrowserLogger->append(strTemp);
}

//////////////////////////////////////////////////////////////////

void QtWidgetController::onClickBtnConnect()
{
    if (m_pDcmServiceController != NULL)
    {
        setCursor(Qt::WaitCursor);

        if (m_pDcmServiceController->isWorking() == true)
        {
            m_pDcmServiceController->close();
        }
        else
        {
            int nError = m_pDcmServiceController->connect(
                ui.m_lineEditServiceAE->text().toStdString().c_str(),
                ui.m_lineEditServiceIP->text().toStdString().c_str(),
                ui.m_lineEditServicePort->text().toInt(),
                ui.m_lineEditHostAE->text().toStdString().c_str(),
                ui.m_checkBoxTSL->isChecked()
                );
        }
        ///. end of if (m_pDcmServiceController->isWorking() == true)

        setCursor(Qt::ArrowCursor);
    }
    else
    {
        QMessageBox::information(
            this, 
            tr("QtWidgetController"),
            tr("QtWidgetController::onClickBtnConnect()")
            );
    }
}

void QtWidgetController::onClickBtnTurnOn()
{
    setCursor(Qt::WaitCursor);
    if (m_pDcmServiceController && m_pDcmServiceController->isWorking())
    {
        m_pDcmServiceController->action(TYPE_MSG_N_ACTION_RQ_TURNON, m_strDicomDeviceInstanceUID.toUtf8());
    }
    else
    {
        QMessageBox::information(
            this, 
            tr("QtWidgetController"),
            tr("QtWidgetController::onClickBtnTurnOn()")
            );
    }
    setCursor(Qt::ArrowCursor);
}

void QtWidgetController::onClickBtnTurnOff()
{
    setCursor(Qt::WaitCursor);
    if (m_pDcmServiceController && m_pDcmServiceController->isWorking())
    {
        m_pDcmServiceController->action(TYPE_MSG_N_ACTION_RQ_TURNOFF, m_strDicomDeviceInstanceUID.toUtf8());
    }
    else
    {
        QMessageBox::information(
            this, 
            tr("QtWidgetController"),
            tr("QtWidgetController::onClickBtnTurnOn()")
            );
    }
    setCursor(Qt::ArrowCursor);
}

void QtWidgetController::onClickBtnCurrentSCP()
{
    setCursor(Qt::WaitCursor);
    //if (m_pDcmServiceController && m_pDcmServiceController->isWorking())
    //{
    //    m_pDcmServiceController->get();
    //}
    //else
    {
        QMessageBox::information(
            this, 
            tr("QtWidgetController"),
            tr("QtWidgetController::onClickBtnTurnOn()")
            );
    }
    setCursor(Qt::ArrowCursor);
}