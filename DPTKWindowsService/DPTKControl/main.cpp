#include "dptkcontrol.h"
//#include <QtGui>

#include <QtGui/QApplication>

int main(int argc, char *argv[])
{
//    Q_INIT_RESOURCE(systray);

    QApplication a(argc, argv);

     if (!QSystemTrayIcon::isSystemTrayAvailable()) 
     {
         //QMessageBox::critical(
         //    0, QObject::tr("Systray"), 
         //    QObject::tr("I couldn't detect any system tray on this system.")
         //    );
         return 1;
     }
     QApplication::setQuitOnLastWindowClosed(false);

    DPTKControl w;
    w.show();
    return a.exec();
}
