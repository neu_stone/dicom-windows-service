#ifndef QTWIDGETQUERY_H
#define QTWIDGETQUERY_H

#include <QWidget>
#include "ui_QtWidgetQuery.h"
class Manager;
class DPDcmMsg;

class QtWidgetQuery : public QWidget
{
    Q_OBJECT

    friend class DPTKControl;

public:
    QtWidgetQuery(QWidget *parent = 0);
    ~QtWidgetQuery();
    void setDcmServiceController(Manager* pDcmServiceController)
    {
        m_pDcmServiceController = pDcmServiceController;
    }

private slots:
    ///.
    void onClickBtnFindStudy();
    void onClickBtnDeleteImage();
    void onDbClickItemInWidget();

private:

    void onCFindResponse( const DPDcmMsg& msgParam );

private:
    
    Manager* m_pDcmServiceController;

    ///.
    Ui::QtWidgetQuery ui;
};

#endif // QTWIDGETQUERY_H
