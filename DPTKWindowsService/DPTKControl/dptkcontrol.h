 /****************************************************************************
 **
 ** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
 ** All rights reserved.
 ** Contact: Nokia Corporation (qt-info@nokia.com)
 **
 ** This file is part of the examples of the Qt Toolkit.
 **
 ** $QT_BEGIN_LICENSE:BSD$
 ** You may use this file under the terms of the BSD license as follows:
 **
 ** "Redistribution and use in source and binary forms, with or without
 ** modification, are permitted provided that the following conditions are
 ** met:
 **   * Redistributions of source code must retain the above copyright
 **     notice, this list of conditions and the following disclaimer.
 **   * Redistributions in binary form must reproduce the above copyright
 **     notice, this list of conditions and the following disclaimer in
 **     the documentation and/or other materials provided with the
 **     distribution.
 **   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
 **     the names of its contributors may be used to endorse or promote
 **     products derived from this software without specific prior written
 **     permission.
 **
 ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 ** $QT_END_LICENSE$
 **
 ****************************************************************************/

#ifndef DPTKCONTROL_H
#define DPTKCONTROL_H

#include <QSystemTrayIcon>
#include <QtGui/QDialog>
#include <QSemaphore>

#include "ui_dptkcontrol.h"

#include "Manager.h"

#include "QtWidgetController.h"
#include "QtWidgetDcmSCP.h"
#include "QtWidgetQuery.h"

class QAction;
class QCheckBox;
class QComboBox;
class QGroupBox;
class QLabel;
class QLineEdit;
class QMenu;
class QPushButton;
class QSpinBox;
class QTextEdit;

class DPTKControl
    : public QDialog, DPDcmObserver
{
    Q_OBJECT

signals:
    void updateSignal( int nIconStatus );

public:
    DPTKControl(QWidget *parent = 0, Qt::WFlags flags = 0);
    ~DPTKControl();
    
    void setVisible(bool visible);
    
protected:
    void closeEvent(QCloseEvent *event);

private slots:
     void setIconStatus( int nStatusIndex );
     void iconActivated(QSystemTrayIcon::ActivationReason reason);
     void showMessage();
     void messageClicked();
     ///.
     void onTabUtilityChanged();

private:
    ///. system tray icon and its actions
    QAction *m_minimizeAction;
    QAction *m_maximizeAction;
    QAction *m_restoreAction;
    QAction *m_quitAction;
    ///.
    QSystemTrayIcon *m_pTrayIcon;
    QMenu *m_pTrayIconMenu;
    QIcon * m_pIconDPTK;
    QString m_strIconFile;

    ///. Controller of DCM SCP
    QtWidgetController* m_pQtWidgetController;
    int m_nIndexOfTabController;
    ///. GUI for DCM SCP
    QtWidgetDcmSCP* m_pQtWidgetDcmSCP;
    int m_nIndexOfTabDcmSCP;
    ///.
    QtWidgetQuery* m_pQtWidgetQuery;
    int m_nIndexOfTabQuery;
    ///.
    int m_nCurrentTabIndex;

    ///. IPC to DICOM service
    Manager m_dcmSCU;
    QString m_strServiceAE, m_strServiceIP, m_strServicePort, m_strHostAE;
    bool m_bServiceTSL;
    ///.
    mutable QSemaphore m_semaphoreDcmSCP;
 
    ///////////////////////////////////////////////////////////

    int onInitializeWidgets();
    void onHandleSignal( int nIconStatus);
    
    void createActions();
    void createTrayIcon();

    ///. virtual functions
    int notify(TYPE_DCM_MSG nType, DPDcmMsg& msgDPDcm) const;
    ///. DCM Associate service
	void onAssociateAccept(const DPDcmMsg& msgParam);
	void onAssociateReject(const DPDcmMsg& msgParam);
	void onAssociateRelease(const DPDcmMsg& msgParam);
    void onAbortRequest(const DPDcmMsg& msgParam);
    void onClose(const DPDcmMsg& msgParam);

    ////////////////////////////////////////////////////////////
     //QGroupBox *iconGroupBox;
     //QLabel *iconLabel;
     //QComboBox *iconComboBox;
     //QCheckBox *showIconCheckBox;

     //QGroupBox *messageGroupBox;
     //QLabel *typeLabel;
     //QLabel *durationLabel;
     //QLabel *durationWarningLabel;
     //QLabel *titleLabel;
     //QLabel *bodyLabel;
     //QComboBox *typeComboBox;
     //QSpinBox *durationSpinBox;
     //QLineEdit *titleEdit;
     //QTextEdit *bodyEdit;
     //QPushButton *showMessageButton;

//    void createIconGroupBox();
//    void createMessageGroupBox();

private:
    Ui::DPTKControlClass ui;
};

#endif // DPTKCONTROL_H
