#include "QtWidgetQuery.h"
#include <QtGui/qmessagebox.h>

#include "Manager.h"
#include "DataSet.h"

namespace
{
    enum DcmQueryColume
    {
        DcmQueryColume_Name=0,
        DcmQueryColume_UID,
        DcmQueryColume_ClassUID,
    };
    const QString strLevelStudy("STUDY");
    const QString strLevelSeries("SERIES");
    const QString strLevelImage("IMAGE");
    const int nLimitOfImageSize=512;
    const int nMaxPath = 260;
}

QtWidgetQuery::QtWidgetQuery(QWidget *parent)
    : QWidget(parent)
    , m_pDcmServiceController(NULL)
{
    ui.setupUi(this);
	///. study root query widget
	QTreeWidgetItem* headerItem = new QTreeWidgetItem();
	headerItem->setText(DcmQueryColume_Name, QString("Query Result"));
	headerItem->setText(DcmQueryColume_UID, QString("UID"));
	headerItem->setText(DcmQueryColume_ClassUID, QString("ClassUID"));
	ui.m_treeWidgetQueryStudy->setHeaderItem(headerItem);
	ui.m_treeWidgetQueryStudy->setColumnWidth(DcmQueryColume_Name, 720);
	ui.m_treeWidgetQueryStudy->header()->setResizeMode(DcmQueryColume_UID, QHeaderView::Stretch);

    ///. study conditions
    ui.m_dateEditStudyStart->setDate(QDate::currentDate());
    ui.m_dateEditStudyEnd->setDate(QDate::currentDate());
}

QtWidgetQuery::~QtWidgetQuery()
{
    m_pDcmServiceController = NULL;
}

void QtWidgetQuery::onClickBtnFindStudy()
{
    setCursor(Qt::WaitCursor);
	ui.m_treeWidgetQueryStudy->clear();
    QString strTemp;
    if (m_pDcmServiceController && m_pDcmServiceController->isWorking())
    {
        DataSet dsAttributes;
        ///.<Class UID="1.2.840.10008.5.1.4.1.2.2.1" Comment="Find StudyRoot QueryRetrieve Information Model"/>
        int nError = dsAttributes.initialize("1.2.840.10008.5.1.4.1.2.2.1");
        const DcmItemHandle pAttriRoot = dsAttributes.getRoot();
        bool bReplaceOld(true);

        /// QueryRetrieveLevel
        DataSet::TDcmTagKey tagKey(0x0008, 0x0052);
        nError = dsAttributes.setStringValue( pAttriRoot, tagKey, strLevelStudy.toUtf8(), bReplaceOld );
	    /// stduy date
        tagKey.group = 0x0008, tagKey.element = 0x0020;
        QDate dateStart = ui.m_dateEditStudyStart->date();
        QDate dateEnd = ui.m_dateEditStudyEnd->date();
        strTemp = QString("%1%2%3\\%4%5%6")
            .arg(dateStart.year(), 4, 10, QChar('0'))
            .arg(dateStart.month(), 2, 10, QChar('0'))
            .arg(dateStart.day(), 2, 10, QChar('0'))
            .arg(dateEnd.year(), 4, 10, QChar('0'))
            .arg(dateEnd.month(), 2, 10, QChar('0'))
            .arg(dateEnd.day(), 2, 10, QChar('0'));
        nError = dsAttributes.setStringValue( pAttriRoot, tagKey, strTemp.toUtf8(), bReplaceOld );
//	    /// Study Instance UID
//        tagKey.group = 0x0020, tagKey.element = 0x000D;
//        strTemp = "";
//        dsAttributes.setStringValue( pAttriRoot, tagKey, strTemp.toUtf8(), bReplaceOld );

        ///. C-Find request
        nError = m_pDcmServiceController->find(dsAttributes);
        strTemp = QString("The manage: CFind#%1.").arg(nError);
    }
	else
    {
        strTemp = QString("The manager is not at working.");
    }
	ui.m_labelQueryRetrieve->setText(strTemp);
    setCursor(Qt::ArrowCursor);
}

void QtWidgetQuery::onClickBtnDeleteImage()
{
        QMessageBox::information(
            this, tr("QtWidgetDcmSCP"), tr("onClickBtnSetRemoteAE(...)")
            );
}

void QtWidgetQuery::onDbClickItemInWidget()
{
	QTreeWidgetItem *currentItem = ui.m_treeWidgetQueryStudy->currentItem();
	if (currentItem == NULL)
		return;
	if (currentItem->childCount() > 0)
		return;
    QString strTemp;
    if (m_pDcmServiceController && m_pDcmServiceController->isWorking())
    {
	    QString strQRLevel = currentItem->text(DcmQueryColume_Name);
	    if (strQRLevel.indexOf(strLevelImage, 0, Qt::CaseInsensitive) == 0)
	    {
            strTemp = QString("The manager: CGet at %1").arg(strLevelImage);
	    }
	    else if (strQRLevel.indexOf(strLevelSeries, 0, Qt::CaseInsensitive) == 0)
	    {
            DataSet dsAttributes;
            ///.<Class UID="1.2.840.10008.5.1.4.1.2.2.1" Comment="Find StudyRoot QueryRetrieve Information Model"/>
            int nError = dsAttributes.initialize("1.2.840.10008.5.1.4.1.2.2.1");
            const DcmItemHandle pAttriRoot = dsAttributes.getRoot();
            bool bReplaceOld(true);

            /// QueryRetrieveLevel
            DataSet::TDcmTagKey tagKey(0x0008, 0x0052);
            nError = dsAttributes.setStringValue( pAttriRoot, tagKey, strLevelImage.toUtf8(), bReplaceOld );
	        /// Series Instance UID
            tagKey.group = 0x0020, tagKey.element = 0x000E;
            strTemp = currentItem->text(DcmQueryColume_UID);
            nError = dsAttributes.setStringValue( pAttriRoot, tagKey, strTemp.toUtf8(), bReplaceOld );

            ///. C-Find request
		    nError = m_pDcmServiceController->find( dsAttributes );
            strTemp = QString("The manager : CFind#%1 at %2.").arg(nError).arg(strLevelImage);
	    }
	    else if (strQRLevel.indexOf(strLevelStudy, 0, Qt::CaseInsensitive) == 0)
	    {
            DataSet dsAttributes;
            ///.<Class UID="1.2.840.10008.5.1.4.1.2.2.1" Comment="Find StudyRoot QueryRetrieve Information Model"/>
            int nError = dsAttributes.initialize("1.2.840.10008.5.1.4.1.2.2.1");
            const DcmItemHandle pAttriRoot = dsAttributes.getRoot();
            bool bReplaceOld(true);

            /// QueryRetrieveLevel
            DataSet::TDcmTagKey tagKey(0x0008, 0x0052);
            nError = dsAttributes.setStringValue( pAttriRoot, tagKey, strLevelSeries.toUtf8(), bReplaceOld );
	        /// Study Instance UID
            tagKey.group = 0x0020, tagKey.element = 0x000d;
            strTemp = currentItem->text(DcmQueryColume_UID);
            nError = dsAttributes.setStringValue( pAttriRoot, tagKey, strTemp.toUtf8(), bReplaceOld );

            ///. C-Find request
		    nError = m_pDcmServiceController->find( dsAttributes );
            strTemp = QString("The manager : CFind#%1 at %2.").arg(nError).arg(strLevelSeries);
        }
	    else
	    {
            strTemp = QString("Unknown Level: %1").arg(strQRLevel);
	    }
	    ///. end of if (strQRLevel.indexOf(strLevelImage, 0, Qt::CaseInsensitive) == 0)
        ui.m_labelQueryRetrieve->setText(strTemp);
    }
    ///. end of if (m_pDcmServiceController && m_pDcmServiceController->isWorking())
}

void QtWidgetQuery::onCFindResponse( const DPDcmMsg& msgParam )
{
    QString strTemp;
    strTemp = QString("%1 << %2 : CFind Response.").
        arg(msgParam.pstrCalledAE).
        arg(msgParam.pstrCallingAE);
    ui.m_labelQueryRetrieve->setText(strTemp);
    ///.
	if (msgParam.pDatasetReq)
	{
		DataSet dsTemp;
		DcmItemHandle pItemRoot = msgParam.pDatasetReq;

        bool bFlag(false), bSearchChild(false), bVerbose(false);
        unsigned long lPos(0);
        char strValue[nMaxPath];
        QString strMsg, strSOPInstanceUID, strSOPClassUID;
	    /// QueryRetrieveLevel
	    DataSet::TDcmTagKey tagKey(0x0008, 0x0052);
        bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
        if (bFlag == true && strLevelImage.indexOf(strValue, 0, Qt::CaseInsensitive) == 0)
        {
            strMsg = QString("%1:  ").arg(strLevelImage);
            ///. Instance Number
	        tagKey.group = 0x0020, tagKey.element = 0x0013;
            bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            strMsg += QString(" InstanceNumber( %1 )").arg(strValue);
 			//DCM_PixelDataProviderURL
			tagKey.group = 0x0028, tagKey.element = 0x7FE0;
			bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            if (bFlag && strlen(strValue)>0)
                strMsg += QString(" Provider( %1 )").arg(strValue);
            else
                strMsg += QString(" Provider( NULL )");
            //DCM_TotalPixelMatrixColumns
            tagKey.group = 0x0048, tagKey.element = 0x0006;
            strMsg += QString(" VolumeSize( %1").arg(
                dsTemp.findAndGetLong(pItemRoot, tagKey, lPos, bSearchChild)
                );
            //DCM_TotalPixelMatrixRows
            tagKey.group = 0x0048, tagKey.element = 0x0007;
            strMsg += QString(", %1 )").arg(
                dsTemp.findAndGetLong(pItemRoot, tagKey, lPos, bSearchChild)
                );
            //DCM_ImagedVolumeWidth
            tagKey.group = 0x0048, tagKey.element = 0x0001;
            strMsg += QString(" Dimension( %1").arg(
                dsTemp.findAndGetDouble(pItemRoot, tagKey, lPos, bSearchChild)
                );
            //DCM_ImagedVolumeHeight
		    tagKey.group = 0x0048, tagKey.element = 0x0002;
            strMsg += QString(", %1 )").arg(
                dsTemp.findAndGetDouble(pItemRoot, tagKey, lPos, bSearchChild)
                );
            
            ///. SOP Instance UID
	        tagKey.group = 0x0008, tagKey.element = 0x0018;
            bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            strSOPInstanceUID = strValue;

            ///. SOP Class UID
	        tagKey.group = 0x0008, tagKey.element = 0x0016;
            bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            strSOPClassUID = strValue;
        }
        else if (bFlag == true && strLevelSeries.indexOf(strValue, 0, Qt::CaseInsensitive) == 0)
        {
            /// Modality
	        tagKey.group = 0x0008, tagKey.element = 0x0060;
            bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            QString strModality = strValue;
            /// Series Number
	        tagKey.group = 0x0020, tagKey.element = 0x0011;
            bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            strSOPClassUID = strValue;
	        /// Series Instance UID
	        tagKey.group = 0x0020, tagKey.element = 0x000E;
            bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            strSOPInstanceUID = strValue;
	        /// Number of Series Related Instances
	        tagKey.group = 0x0020, tagKey.element = 0x1209;
            long lNumOfSeriesRelatedInstances = dsTemp.findAndGetLong(pItemRoot, tagKey, lPos, bSearchChild);
	        /// Study Instance UID
	        tagKey.group = 0x0020, tagKey.element = 0x000D;
            bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            QString strStudyInstanceUID = strValue;
	        /// Retrieve AE Title
	        tagKey.group = 0x0008, tagKey.element = 0x0054;
            bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            QString strRetrieveAETitle = strValue;
            ///. Series Date
            DataSet::TDcmDate dtSeriesDate;
            tagKey.group = 0x0008, tagKey.element = 0x0021;
            bFlag = dsTemp.findAndGetDate(pItemRoot, tagKey, dtSeriesDate, lPos, bSearchChild);
	        /// Series Time
            DataSet::TDcmTime tmSeriesTime;
            tagKey.group = 0x0008, tagKey.element = 0x0031;
            bFlag = dsTemp.findAndGetTime(pItemRoot, tagKey, tmSeriesTime, lPos, bSearchChild);
            ///.
            strMsg = QString("%1:  Modality( %2 ), SeriesNumber( %3 ), SeriesDate#( %4.%5.%6 ), Images( %7 )").
                arg(strLevelSeries).
                arg(strModality).
                arg(strSOPClassUID).
                arg(dtSeriesDate.Year).
                arg(dtSeriesDate.Month).
                arg(dtSeriesDate.Day).
                arg(lNumOfSeriesRelatedInstances);
        }
        else if (bFlag == true && strLevelStudy.indexOf(strValue, 0, Qt::CaseInsensitive) == 0)
        {
	        /// stduy date
            DataSet::TDcmDate dtStudyDate;
            tagKey.group = 0x0008, tagKey.element = 0x0020;
            bFlag = dsTemp.findAndGetDate(pItemRoot, tagKey, dtStudyDate, lPos, bSearchChild);
	        /// stduy time
            DataSet::TDcmTime dtStudyTime;
            tagKey.group = 0x0008, tagKey.element = 0x0030;
            bFlag = dsTemp.findAndGetTime(pItemRoot, tagKey, dtStudyTime, lPos, bSearchChild);
	        /// Accession Number
            tagKey.group = 0x0008, tagKey.element = 0x0050;
            bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            QString strAccessionNumber(strValue);
	        /// Patient Name
            tagKey.group = 0x0010, tagKey.element = 0x0010;
            bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            QString strPatientName(strValue);
	        /// Patient ID
            tagKey.group = 0x0010, tagKey.element = 0x0020;
            bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            QString strPatientID(strValue);
	        /// Study ID
            tagKey.group = 0x0020, tagKey.element = 0x0010;
            bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            strSOPClassUID = strValue;
	        /// Study Instance UID
            tagKey.group = 0x0020, tagKey.element = 0x000D;
            bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            strSOPInstanceUID = strValue;
	        /// Retrieve AE Title
            tagKey.group = 0x0008, tagKey.element = 0x0054;
            bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            QString strRetrieveAETitle(strValue);
	        /// Number of Study Related Series
            tagKey.group = 0x0020, tagKey.element = 0x1206;
            long lNumOfStudyRelatedSeries = dsTemp.findAndGetLong(pItemRoot, tagKey, lPos, bSearchChild);

            ///.
            strMsg = QString("%1:  %2( ID#%3 ), StudyID( %4 ), StudyDate( %5.%6.%7 ), Series( %8 )").
                arg(strLevelStudy).
                arg(strPatientName).
                arg(strPatientID).
                arg(strSOPClassUID).
                arg(dtStudyDate.Year).
                arg(dtStudyDate.Month).
                arg(dtStudyDate.Day).
                arg(lNumOfStudyRelatedSeries);
        }
        else
        {
            strMsg = "Unknown:";
        }
        ///. end of if (dsTemp.findAndGetString( query level) == true)

        QTreeWidgetItem* pItem = new QTreeWidgetItem();
		pItem->setText(DcmQueryColume_Name, strMsg);
        pItem->setText(DcmQueryColume_UID, strSOPInstanceUID);
        pItem->setText(DcmQueryColume_ClassUID, strSOPClassUID);
		///.
        QTreeWidgetItem *currentItem = ui.m_treeWidgetQueryStudy->currentItem();
        if (currentItem == NULL)
        {
		    ui.m_treeWidgetQueryStudy->addTopLevelItem(pItem);
        }
        else
        {
            currentItem->addChild(pItem);
        }
        if (bVerbose)
        {
            dsTemp.load(msgParam.pDatasetReq);
            dsTemp.save("..\\..\\temp\\ScuImageInfoResp.dcm", 0);
        }
    }
    ///. end of if (msgParam.pDataset)
}