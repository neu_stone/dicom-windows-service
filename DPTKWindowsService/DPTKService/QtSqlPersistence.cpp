﻿#include <QtSql\QSqlDriver>
#include <QtSql\QSqlError>
#include <QtSql\QSqlQuery>
#include <QtSql\QSqlRecord>
#include <QtCore\qvariant.h>
#include <QtCore\qdatetime.h>
#include <QtCore\qdir.h>

#include "image/ImageRepository.h"
#include "image/io/ImageAccessor.h"
#include "image/io/global.h"
#include "image/io/ThreadedRegionBlock.h"
#include "image/io/ImagePyramid.h"

#include "image/tiff/TIFFParser.h"
#include "image/tiff/TIFFReader.h"
#include "global/geometry/Point.h"
#include "global/geometry/Size.h"
#include "global/geometry/Rect.h"
//#include "geometry/Point.h"
//#include "geometry/Size.h"
//#include "geometry/Rect.h"

#include "QtSqlPersistence.h"
#include "Service.h"
//#include "dicomlib/Service.h"


namespace
{
    const QString strLevelStudy("STUDY");
    const QString strLevelSeries("SERIES");
    const QString strLevelImage("IMAGE");
    
    const QString strFileExtension("img");

    const int nNumOfElemetsInRemoteAE = 7;
    const char* pRemoteAEs[] = {
        "UserID", "AETitle", "IPAddress", "PortNumber", "UserName", "Role", "TLS"
    };
}

QtSqlPersistence* QtSqlPersistence::m_pQtSqlPersistence = NULL;

/////////////////////////////////////////////////////////////////////////////////

void QtThreadSearchingDcm::attach(QtSqlPersistence* pQtSqlPersistence)
{
	m_pQtSqlPersistence = pQtSqlPersistence;
}

void QtThreadSearchingDcm::run()
{
	if (m_pQtSqlPersistence)
	{
        m_pQtSqlPersistence->m_nQtThreadSearchingDcm = QtSqlPersistence::ThreadState_Running;

        QString strSearchingDir(m_pQtSqlPersistence->m_strTempDataDir);
        int nError(0), nSleepInSec(3);
	    do 
	    {
            try
            {
		        nError = m_pQtSqlPersistence->onSearch(strSearchingDir, true);
            }
            catch (...)
            {
                nError = -1;
            }
            sleep(nSleepInSec);
	    }
        while (m_pQtSqlPersistence->m_nQtThreadSearchingDcm == QtSqlPersistence::ThreadState_Running);

	    m_pQtSqlPersistence->m_nQtThreadSearchingDcm = QtSqlPersistence::ThreadState_Idle;
	}
	return;
}

/////////////////////////////////////////////////////////////////////////////////

QtSqlPersistence::QtSqlPersistence(void)
    : QObject()
    , m_SqlDatabase()
    , m_strDatabaseType("QODBC")
    , m_strDSN("DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=..\\..\\data\\DPTK_WSI.mdb")
//    , m_strDSN("Driver={Microsoft Access Driver (*.mdb, *.accdb)}; DBQ=..\\..\\data\\DPTK_WSI.accdb;")
    , m_strHostName("")
    , m_strTempDataDir("..\\TEMP")
    , m_strWorkingDir("..\\DATA")
    , m_strPixelDataProvider("\\\\Pathcore\\data")
    , m_bPatientAnonymized(false)
    , m_strLastError("localhost")
    , m_pQtThreadSearchingDcm(NULL)
    , m_nQtThreadSearchingDcm(ThreadState_Idle)
    , m_lTimeoutInMS(500)
{
//    sedeen::image::ImageRepository& imageRepository = sedeen::image::ImageRepository::instance();
//    sedeen::image::ImageAccessor imageAccessor = imageRepository.get("C:\\02.Project\\1.GitDPTK_XG02\\data\\tiff\\M3_26_00_Pyramid.tif");
////    const sedeen::image::ImageParser* pImageParser =  &(imageAccessor.imageParser());
//    const sedeen::image::TIFFParser* pImageParser = static_cast<const sedeen::image::TIFFParser*>(&(imageAccessor.imageParser()));
//
//    int nNumOfComponents = pImageParser->components();
//    std::string strCompression = pImageParser->compression();
////    pImageParser->color();
//    sedeen::image::TIFFReader tiffReader = pImageParser->reader();
////
//////    sedeen::image::TIFFReader tiffReader("\\PATHCORE_DEV1\\data\\M3_26_00_Pyramid.tif");
//    uint16_t nNumOfDirs = tiffReader.numDirectories();

}


QtSqlPersistence::~QtSqlPersistence(void)
{
    if (m_SqlDatabase.isOpen())
    {
        m_SqlDatabase.close();
    }
}

QtSqlPersistence* QtSqlPersistence::getInstance()
{
    if (m_pQtSqlPersistence == NULL)
    {
        m_pQtSqlPersistence = new QtSqlPersistence();
    }
    return m_pQtSqlPersistence;
}

void QtSqlPersistence::releaseInstance()
{
    if (m_pQtSqlPersistence != NULL)
    {
        delete m_pQtSqlPersistence;
        m_pQtSqlPersistence = NULL;
    }
}

bool QtSqlPersistence::isConnected()
{
    return m_SqlDatabase.isOpen();
}

void QtSqlPersistence::setWorkingDir(const QString& strWorkingDir)
{
    m_strWorkingDir = strWorkingDir;
}

void QtSqlPersistence::setTempDataDir(const QString& strTempDataDir)
{
    m_strTempDataDir = strTempDataDir;
}

bool QtSqlPersistence::connect(const QString& pstrDatabaseType, const QString& pstrDSN, const QString& pstrHostName)
{
    bool bFlag(false);
    try
    {
        m_SqlDatabase = QSqlDatabase::addDatabase(pstrDatabaseType);
        m_SqlDatabase.setHostName(pstrHostName);
        m_SqlDatabase.setDatabaseName(pstrDSN);
//        m_SqlDatabase.setUserName("");
//        m_SqlDatabase.setPassword("");
        bFlag = m_SqlDatabase.open();
        if (bFlag == false)
        {
            m_strLastError = m_SqlDatabase.lastError().text();
        }
        else
        {
            m_strDatabaseType = pstrDatabaseType;
            m_strDatabaseType = pstrDSN;
            m_strHostName = pstrHostName;

//            QString strTemp;
//            QStringList list = m_SqlDatabase.tables(QSql::Tables);
//            for(int i=0;i<list.size(); ++i)
//            {   //qDebug() << "Table names " << list.at(i) << endl;
//                strTemp = list.at(i);
//            }

            ///. thread to search dcm files.
            if (m_pQtThreadSearchingDcm == NULL)
            {
                m_pQtThreadSearchingDcm = new QtThreadSearchingDcm();
                m_pQtThreadSearchingDcm->attach(this);
            }
            QDir qtDirTemp(m_strTempDataDir);
            if (qtDirTemp.exists() == true)
            {
                bFlag = true;
                m_pQtThreadSearchingDcm->start();
           }
            else
            {
                bFlag = false;
                m_strLastError = QString("ThreadSearchingDcm failed : %1").
                    arg(m_strTempDataDir);
            }
        }
    }
    catch (...)
    {
        m_strLastError = m_SqlDatabase.lastError().text();

        if (m_pQtThreadSearchingDcm != NULL &&
            m_nQtThreadSearchingDcm != ThreadState_Idle)
        {
            m_nQtThreadSearchingDcm = ThreadState_Idle;
            m_pQtThreadSearchingDcm->terminate();
            m_pQtThreadSearchingDcm->wait(m_lTimeoutInMS);
        }

        if (m_SqlDatabase.isOpen())
        {
            m_SqlDatabase.close();
        }
        
        return false;
    }
    return bFlag;
}

void QtSqlPersistence::close()
{
    if ( m_pQtThreadSearchingDcm != NULL )
    {
        if (m_nQtThreadSearchingDcm != ThreadState_Idle)
        {
            m_nQtThreadSearchingDcm = ThreadState_Idle;
    //        m_pQtThreadSearchingDcm->msleep();
            m_pQtThreadSearchingDcm->terminate();
            m_pQtThreadSearchingDcm->wait(m_lTimeoutInMS);
        }
        delete m_pQtThreadSearchingDcm;
        m_pQtThreadSearchingDcm = NULL;
    }

    m_SqlDatabase.close();
}

QString QtSqlPersistence::getLastError()
{
    return m_strLastError;
}

int QtSqlPersistence::getStudyInfo(DcmItemHandle pItemRoot)
{
	int nError(0);
    bool bFlag(false), bSearchChild(false), bVerbose(false);
    unsigned long lPos(0);

    QSqlQuery queryStudy, querySeries;
    queryStudy.setForwardOnly(true);
    querySeries.setForwardOnly(true);

	try
	{
        DataSet dcmDatasetReq;
        nError = dcmDatasetReq.load(pItemRoot);
        DcmItemHandle pItemReq = dcmDatasetReq.getRoot();
        if (bVerbose)
        {
            dcmDatasetReq.save("..\\..\\temp\\SqlStudyInfoReq.dcm", 0);
        }

        QString strStudyInstanceUID, strPatientID, strPatientName, strAccessNumber, strStudyID;
        char strValue[128];

        //DCM_StudyInstanceUID
		DataSet::TDcmTagKey tagKey(0x0020, 0x000d);
		bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
        if (bFlag && strlen(strValue)>0)
        {
            strStudyInstanceUID = strValue;
        }
        else
        {
            strStudyInstanceUID = "%";
        }
		QString strFilter = QString("SELECT PT.PatientID, PT.PatientName, PT.PatientBirthDate, "
            "ST.StudyInstanceUID, ST.StudyDate, ST.AccessionNumber, ST.StudyID "
            "FROM Patient PT, Study ST "
            "WHERE ST.PatientID=PT.PatientID AND ST.PatientName=PT.PatientName AND ST.StudyInstanceUID LIKE '%1'").
            arg(strStudyInstanceUID);
        
        //DCM_PatientID
		tagKey.group = 0x0010, tagKey.element = 0x0020;
		bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
        if (bFlag && strlen(strValue)>0)
        {
            strFilter += " AND PT.PatientID LIKE '";
            strFilter += strValue;
            strFilter += "'";
        }
        //DCM_PatientsName
		tagKey.group = 0x0010, tagKey.element = 0x0010;
		bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
        if (bFlag && strlen(strValue)>0)
        {
            strFilter += " AND PT.PatientName LIKE '";
            strFilter += strValue;
            strFilter += "'";
        }
		//DCM_AccessionNumber
		tagKey.group = 0x0008, tagKey.element = 0x0050;
		bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
        if (bFlag && strlen(strValue)>0)
        {
            strFilter += " AND ST.AccessionNumber LIKE '";
            strFilter += strValue;
            strFilter += "'";
        }
		//DCM_StudyID
		tagKey.group = 0x0020, tagKey.element = 0x0010;
		bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue,0,TRUE);
        if (bFlag && strlen(strValue)>0)
        {
            strFilter += " AND ST.StudyID LIKE '%";
            strFilter += strValue;
            strFilter += "%'";
        }
		//DCM_StudyDate
		DataSet::TDcmDate dtTemp;
		tagKey.group = 0x0008, tagKey.element = 0x0020;
		bFlag = dcmDatasetReq.findAndGetDate(pItemReq, tagKey, dtTemp, lPos, bSearchChild);
        if (bFlag == true)
        {
            QString strTemp;
            DataSet::TDcmDate dtTemp1;
            unsigned long lPos1(1);
            if (dcmDatasetReq.findAndGetDate(pItemReq, tagKey, dtTemp1, lPos1, bSearchChild) == true)
            {
                strTemp = QString(" AND ST.StudyDate BETWEEN #%1-%2-%3# AND #%4-%5-%6#").
                    arg(dtTemp.Year, 4, 10, QChar('0')).
                    arg(dtTemp.Month, 2, 10, QChar('0')).
                    arg(dtTemp.Day, 2, 10, QChar('0')).
                    arg(dtTemp1.Year, 4, 10, QChar('0')).
                    arg(dtTemp1.Month, 2, 10, QChar('0')).
                    arg(dtTemp1.Day, 2, 10, QChar('0'));
            }
            else
            {
                strTemp = QString(" AND ST.StudyDate LIKE '%1/%2/%3'").
                    arg(dtTemp.Month, 2, 10, QChar('0')).
                    arg(dtTemp.Day, 2, 10, QChar('0')).
                    arg(dtTemp.Year, 4, 10, QChar('0'));
            }
            strFilter += strTemp;
        }
//		//DCM_StudyTime
//		DataSet::TDcmTime tmTemp;
//		tagKey.group = 0x0008, tagKey.element = 0x0030;
//		bFlag = dcmDatasetReq.findAndGetTime(pItemReq, tagKey, tmTemp, lPos, bSearchChild);

        strFilter.replace("*", "%");
        strFilter += " ORDER BY PT.PatientID , PT.PatientName";
        if (queryStudy.exec(strFilter) == true)
        {
            QDate dtSQLDate;
            int i(0), nNumOfValues(1);
            bool bReplaced = true;

            while (queryStudy.next() == true)
            {
//                listDcmDataset.push_back(*pDcmDataset);
//                DataSet& dcmTemp = listDcmDataset[i];

                //DCM_PatientID
                int nFieldNo(0);
                QString strPatientID = queryStudy.value(nFieldNo).toString();
                tagKey.group = 0x0010, tagKey.element = 0x0020;
                nError = dcmDatasetReq.setStringValue(
                    pItemReq, tagKey, 
                    strPatientID.toUtf8(),
                    bReplaced
                    );
                //DCM_PatientsName
                ++ nFieldNo;
		        tagKey.group = 0x0010, tagKey.element = 0x0010;
                nError = dcmDatasetReq.setStringValue(
                    pItemReq, tagKey, 
                    queryStudy.value(nFieldNo).toString().toStdString().c_str(),
                    bReplaced
                    );
                //DCM_PatientirthDate
                ++ nFieldNo;
		        tagKey.group = 0x0010, tagKey.element = 0x0030;
                dtSQLDate = queryStudy.value(nFieldNo).toDate();
                dtTemp.Year = dtSQLDate.year();
                dtTemp.Month = dtSQLDate.month();
                dtTemp.Day = dtSQLDate.day();
                nError = dcmDatasetReq.setDateValue(
                    pItemReq, tagKey, dtTemp, bReplaced
                    );
//                QTime tmBirthDate = queryStudy.value(nFieldNo).toTime();
                //ST.StudyInstanceUID, 
                ++ nFieldNo;
                strStudyInstanceUID = queryStudy.value(nFieldNo).toString();
		        tagKey.group = 0x0020, tagKey.element = 0x000d;
                nError = dcmDatasetReq.setStringValue(
                    pItemReq, tagKey, 
                    strStudyInstanceUID.toUtf8(),
                    bReplaced
                    );
                //ST.StudyDate
                ++ nFieldNo;
                tagKey.group = 0x0008, tagKey.element = 0x0020;
                dtSQLDate = queryStudy.value(nFieldNo).toDate();
                dtTemp.Year = dtSQLDate.year();
                dtTemp.Month = dtSQLDate.month();
                dtTemp.Day = dtSQLDate.day();
                nError = dcmDatasetReq.setDateValue(
                    pItemReq, tagKey, dtTemp, bReplaced
                    );
                //, ST.AccessionNumber
                ++ nFieldNo;
		        tagKey.group = 0x0008, tagKey.element = 0x0050;
                nError = dcmDatasetReq.setStringValue(
                    pItemReq, tagKey, 
                    queryStudy.value(nFieldNo).toString().toStdString().c_str(),
                    bReplaced
                    );
                //, ST.StudyID
                ++ nFieldNo;
		        tagKey.group = 0x0020, tagKey.element = 0x0010;
                nError = dcmDatasetReq.setStringValue(
                    pItemReq, tagKey, 
                    queryStudy.value(nFieldNo).toString().toStdString().c_str(),
                    bReplaced
                    );
	            /// Number of Study Related Series
                tagKey.group = 0x0020, tagKey.element = 0x1206;
                long lNumOfStudyRelatedSeries = 0;
		        strFilter = QString("SELECT COUNT(*) FROM Series WHERE StudyInstanceUID='%1' AND PatientID='%2'").
                    arg(strStudyInstanceUID).
                    arg(strPatientID);
                if (querySeries.exec(strFilter) == true)
                {
                    querySeries.next();
                    lNumOfStudyRelatedSeries = querySeries.value(0).toLongLong();
                    ///.
                    querySeries.finish();
                }
                else
                {
                    m_strLastError = querySeries.lastError().text();
                }
                nError = dcmDatasetReq.setLongValue(
                    pItemReq, tagKey, &lNumOfStudyRelatedSeries, nNumOfValues, bReplaced
                    );
                if (bVerbose)
                {
                    dcmDatasetReq.save("..\\..\\temp\\SqlStudyInfoResp.dcm", 0);
                }

                //DCM_ReferencedStudySequence
                tagKey.group = 0x0008, tagKey.element = 0x1110;
                nError = dcmDatasetReq.copyAndInsertSeqItem(pItemReq, tagKey, pItemRoot);
                if (bVerbose)
                {
                    DataSet dcmTemp;
                    dcmTemp.load(pItemRoot);
                    dcmTemp.save("..\\..\\temp\\SqlStudyInfoResp.dcm", 0);
                }
 
                ++ i;
            }
            ///. end of while (queryStudy.next() == true)

            queryStudy.finish();
            nError = i;
        }
        else
        {
            nError = -1;
            m_strLastError = queryStudy.lastError().text();
        }
        ///. end of if (queryStudy.exec(strFilter) == true)
	}
	catch (...)
	{
        m_strLastError = m_SqlDatabase.lastError().text();
		return -1;
	}
    return nError;
}

int QtSqlPersistence::getSeriesInfo(DcmItemHandle pItemRoot)
{
	int nError(0);
    bool bFlag(false), bSearchChild(false), bVerbose(false);
    unsigned long lPos(0);
    int nNumOfValues(1);

    QSqlQuery querySeries, queryImage, queryPresentationState;
    querySeries.setForwardOnly(true);
    queryImage.setForwardOnly(true);
    queryPresentationState.setForwardOnly(true);

	try
	{
        DataSet dcmDatasetReq;
        nError = dcmDatasetReq.load(pItemRoot);
        DcmItemHandle pItemReq = dcmDatasetReq.getRoot();
        if (bVerbose)
        {
            dcmDatasetReq.save("..\\..\\temp\\SqlSeriesInfoReq.dcm", 0);
        }

        QString strSeriesInstanceUID;
        char strValue[128];
        double fValue;
        long lValue;

        //DCM_SeriesInstanceUID
		DataSet::TDcmTagKey tagKey(0x0020, 0x000e);
		bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
        if (bFlag && strlen(strValue)>0)
        {
            strSeriesInstanceUID = strValue;
        }
        else
        {
            strSeriesInstanceUID = "%";
        }
		QString strFilter = QString(
            "SELECT SeriesInstanceUID, SeriesDate, Modality, SeriesNumber, ImageCount, StudyInstanceUID "
            "FROM Series WHERE SeriesInstanceUID LIKE '%1'"
            ).
            arg(strSeriesInstanceUID);

        //DCM_StudyInstanceUID
		tagKey.group = 0x0020, tagKey.element = 0x000d;
		bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
        if (bFlag && strlen(strValue)>0)
        {
            strFilter += " AND StudyInstanceUID LIKE '";
            strFilter += strValue;
            strFilter += "'";
        }
        //DCM_Modality
		tagKey.group = 0x0008, tagKey.element = 0x0060;
		bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
        if (bFlag && strlen(strValue)>0)
        {
            strFilter += " AND Modality LIKE '";
            strFilter += strValue;
            strFilter += "'";
        }
        //DCM_SeriesNumber
		tagKey.group = 0x0020, tagKey.element = 0x0011;
		bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
        if (bFlag && strlen(strValue)>0)
        {
            strFilter += " AND SeriesNumber LIKE '%";
            strFilter += strValue;
            strFilter += "%'";
        }
		//DCM_SeriesDate
		DataSet::TDcmDate dtTemp;
		tagKey.group = 0x0008, tagKey.element = 0x0021;
		bFlag = dcmDatasetReq.findAndGetDate(pItemReq, tagKey, dtTemp, lPos, bSearchChild);
        if (bFlag == true)
        {
            QString strTemp;
            DataSet::TDcmDate dtTemp1;
            unsigned long lPos1(1);
            if (dcmDatasetReq.findAndGetDate(pItemReq, tagKey, dtTemp1, lPos1, bSearchChild) == true)
            {
                strTemp = QString(" AND SeriesDate BETWEEN #%1-%2-%3# AND #%4-%5-%6#").
                    arg(dtTemp.Year, 4, 10, QChar('0')).
                    arg(dtTemp.Month, 2, 10, QChar('0')).
                    arg(dtTemp.Day, 2, 10, QChar('0')).
                    arg(dtTemp1.Year, 4, 10, QChar('0')).
                    arg(dtTemp1.Month, 2, 10, QChar('0')).
                    arg(dtTemp1.Day, 2, 10, QChar('0'));
            }
            else
            {
                strTemp = QString(" AND SeriesDate LIKE '%1/%2/%3%'").
                    arg(dtTemp.Month).
                    arg(dtTemp.Day).
                    arg(dtTemp.Year);
            }
            strFilter += strTemp;
        }

        ///. query database
        strFilter.replace("*", "%");
        strFilter += " ORDER BY SeriesDate, SeriesNumber";
        if (querySeries.exec(strFilter) == true)
        {
            QDate dtSQLDate;
            int i(0);
            bool bReplaced = true;
            while (querySeries.next() == true)
            {
                //DCM_SeriesInstanceUID
                int nFieldNo(0);
		        tagKey.group = 0x0020, tagKey.element = 0x000e;
                QString strSeriesInstanceUID = querySeries.value(nFieldNo).toString();
                nError = dcmDatasetReq.setStringValue(
                    pItemReq, tagKey, strSeriesInstanceUID.toUtf8(), bReplaced
                    );
                //DCM_SeriesDate
                ++ nFieldNo;
                tagKey.group = 0x0008, tagKey.element = 0x0021;
                dtSQLDate = querySeries.value(nFieldNo).toDate();
                dtTemp.Year = dtSQLDate.year();
                dtTemp.Month = dtSQLDate.month();
                dtTemp.Day = dtSQLDate.day();
                nError = dcmDatasetReq.setDateValue(
                    pItemReq, tagKey, dtTemp, bReplaced
                    );
                //DCM_Modality
                ++ nFieldNo;
		        tagKey.group = 0x0008, tagKey.element = 0x0060;
                nError = dcmDatasetReq.setStringValue(
                    pItemReq, tagKey, 
                    querySeries.value(nFieldNo).toString().toStdString().c_str(),
                    bReplaced
                    );
                //DCM_SeriesNumber
                ++ nFieldNo;
		        tagKey.group = 0x0020, tagKey.element = 0x0011;
                nError = dcmDatasetReq.setStringValue(
                    pItemReq, tagKey, 
                    querySeries.value(nFieldNo).toString().toStdString().c_str(),
                    bReplaced
                    );
                //DCM_NumberOfSeriesRelatedImages
                ++ nFieldNo;
                long lNumberOfSeriesRelatedImages = querySeries.value(nFieldNo).toLongLong();
                //DCM_StudyInstanceUID
                ++ nFieldNo;
		        tagKey.group = 0x0020, tagKey.element = 0x000d;
                QString strStudyInstanceUID = querySeries.value(nFieldNo).toString();
                nError = dcmDatasetReq.setStringValue(
                    pItemReq, tagKey, strStudyInstanceUID.toUtf8(), bReplaced
                    );
                //DCM_NumberOfSeriesRelatedImages
                if (lNumberOfSeriesRelatedImages < 1)
                {
		            strFilter = QString("SELECT COUNT(*) FROM Image WHERE StudyInstanceUID='%1' AND SeriesInstanceUID='%2'").
                        arg(strStudyInstanceUID).
                        arg(strSeriesInstanceUID);
                    if (queryImage.exec(strFilter) == true)
                    {
                        queryImage.next();
                        lNumberOfSeriesRelatedImages = queryImage.value(0).toLongLong();
                        ///.
                        queryImage.finish();
                    }
                    else
                    {
                        m_strLastError = queryImage.lastError().text();
                    }
                }
		        tagKey.group = 0x0020, tagKey.element = 0x1209;
                nError = dcmDatasetReq.setLongValue(
                    pItemReq, tagKey, &lNumberOfSeriesRelatedImages, nNumOfValues, bReplaced
                    );
                ///.
		        strFilter = QString(
                    "SELECT ImagedVolumeWidth, ImagedVolumeHeight, ImagedVolumeDepth, TotalPixelMatrixColumns, TotalPixelMatrixRows, NumberOfLevels FROM PresentationState WHERE SeriesInstanceUID='%1'"
                    ).arg(strSeriesInstanceUID);
                if (queryPresentationState.exec(strFilter) == true)
                {
                    if (queryPresentationState.next() == true)
                    {
                        ///. ImagedVolumeWidth, in mm
                        tagKey.group = 0x0048, tagKey.element = 0x0001;
                        int nFieldNoInPS(0);
                        fValue = queryPresentationState.value(nFieldNoInPS).toDouble();
                        nError = dcmDatasetReq.setDoubleValue(
                            pItemReq, tagKey, &fValue, nNumOfValues, bReplaced
                            );
                        ///. ImagedVolumeHeight, in mm
                        ++ nFieldNoInPS;
                        fValue = queryPresentationState.value(nFieldNoInPS).toDouble();
                        tagKey.group = 0x0048, tagKey.element = 0x0002;
                        nError = dcmDatasetReq.setDoubleValue(
                            pItemReq, tagKey, &fValue, nNumOfValues, bReplaced
                            );
                        ///. ImagedVolumeDepth, in μm
                        ++ nFieldNoInPS;
                        fValue = queryPresentationState.value(nFieldNoInPS).toDouble();
                        tagKey.group = 0x0048, tagKey.element = 0x0003;
                        nError = dcmDatasetReq.setDoubleValue(
                            pItemReq, tagKey, &fValue, nNumOfValues, bReplaced
                            );
                        ///. TotalPixelMatrixColumns
                        ++ nFieldNoInPS;
                        lValue = queryPresentationState.value(nFieldNoInPS).toLongLong();
                        tagKey.group = 0x0048, tagKey.element = 0x0006;
                        nError = dcmDatasetReq.setLongValue(
                            pItemReq, tagKey, &lValue, nNumOfValues, bReplaced
                            );
                        ///. TotalPixelMatrixRows
                        ++ nFieldNoInPS;
                        lValue = queryPresentationState.value(nFieldNoInPS).toLongLong();
                        tagKey.group = 0x0048, tagKey.element = 0x0007;
                        nError = dcmDatasetReq.setLongValue(
                            pItemReq, tagKey, &lValue, nNumOfValues, bReplaced
                            );
                        ///. Instance Number
                        ++ nFieldNoInPS;
                        lValue = queryPresentationState.value(nFieldNoInPS).toLongLong();
                        tagKey.group = 0x0020, tagKey.element = 0x0013;
                        nError = dcmDatasetReq.setLongValue(
                            pItemReq, tagKey, &lValue, nNumOfValues, bReplaced
                            );
                    }
                    queryPresentationState.finish();
                }
                ///. end of if (queryPresentationState.exec(strFilter) == true)
                if (bVerbose)
                {
                    dcmDatasetReq.save("..\\..\\temp\\SqlSeriesInfoResp.dcm", 0);
                }


                //DCM_ReferencedSeriesSequence
                tagKey.group = 0x0008, tagKey.element = 0x1115;
                nError = dcmDatasetReq.copyAndInsertSeqItem(pItemReq, tagKey, pItemRoot);
                if (bVerbose)
                {
                    DataSet dcmTemp;
                    dcmTemp.load(pItemRoot);
                    dcmTemp.save("..\\..\\temp\\SqlSeriesInfoResp.dcm", 0);
                }

                ++ i;
            }
            ///. end of while (queryStudy.next() == true)

            querySeries.finish();
            nError = i;
        }
        else
        {
            nError = -1;
            m_strLastError = querySeries.lastError().text();
        }
        ///. end of if (queryStudy.exec(strFilter) == true)
    }
	catch (...)
	{
        //queryImage, queryPresentationState
        m_strLastError = querySeries.lastError().text();
		return -1;
	}
    return nError;
}

int QtSqlPersistence::getInfoRemoteAE(DcmItemHandle pItemRoot)
{
	int nError(0);
    bool bFlag(false), bSearchChild(false), bVerbose(false);
    unsigned long lPos(0);

    QSqlQuery queryRemoteAE;
    queryRemoteAE.setForwardOnly(true);

	try
	{
        DataSet dcmDatasetReq;
        DcmItemHandle pItemReq = pItemRoot;
        char strValue[16];
        bool bFlag(false);

//        //DCM_SOPInstanceUID
//		DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
//		bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
        ///. Move AE Title
        DataSet::TDcmTagKey tagKey(0x0008, 0x0054);
        bFlag = dcmDatasetReq.findAndGetString(
            pItemReq, tagKey, strValue, 0l, false
            );

        QString strFilter = QString(
            "SELECT UserID, AETitle, IPAddress, PortNumber, UserName, Role, TLS"
            " FROM RemoteAE WHERE AETitle='%1' ORDER BY UserID"
            ).arg(strValue);
        ///. query database
        if (queryRemoteAE.exec(strFilter) == true)
        {
            if (queryRemoteAE.next() == true)
            {
                //DCM_PatientID
                int nFieldNo(0);
                QString strUserID = queryRemoteAE.value(nFieldNo).toString();
                ++ nFieldNo;
                QString strAETitle = queryRemoteAE.value(nFieldNo).toString();
                ++ nFieldNo;
                QString strIPAddress = queryRemoteAE.value(nFieldNo).toString();
                ++ nFieldNo;
                QString strPortNumber = queryRemoteAE.value(nFieldNo).toString();
                ++ nFieldNo;
                QString strUserName = queryRemoteAE.value(nFieldNo).toString();
                ++ nFieldNo;
                QString strRole = queryRemoteAE.value(nFieldNo).toString();
                ++ nFieldNo;
                QString strTLS = queryRemoteAE.value(nFieldNo).toString();
            
                QString strTemp = QString("%1\\%2\\%3\\%4\\%5\\%6").
                    arg(strAETitle).
                    arg(strIPAddress).
                    arg(strPortNumber).
                    arg(strUserName).
                    arg(strRole).
                    arg(strTLS);
                dcmDatasetReq.setStringValue(pItemReq, tagKey, strTemp.toStdString().c_str(), true);
            }
            queryRemoteAE.finish();
            nError = 0;
        }
        else
        {
            nError = -1;
            m_strLastError = queryRemoteAE.lastError().text();
        }
        ///. end of if (queryRemoteAE.exec(strFilter) == true)
    }
	catch (...)
	{
        m_strLastError = queryRemoteAE.lastError().text();
		return -1;
	}
    return nError;
}

int QtSqlPersistence::setRemoteAE(DcmItemHandle pItemRoot)
{
	int nError(0);
    bool bFlag(false), bSearchChild(false), bVerbose(false);
    unsigned long lPos(0);

    QSqlQuery queryRemoteAE;
    queryRemoteAE.setForwardOnly(true);

	try
	{
        DataSet dcmDatasetReq;
//        nError = dcmDatasetReq.load(pItemRoot);
//        DcmItemHandle pItemReq = dcmDatasetReq.getRoot();
        DcmItemHandle pItemReq = pItemRoot;
        if (bVerbose)
        {
            DataSet dcmTemp;
            nError = dcmTemp.load(pItemReq);
            dcmTemp.save("..\\..\\temp\\SqlRemoteAEReq.dcm", 0);
        }

        bFlag = m_SqlDatabase.transaction();

        //DCM_SOPInstanceUID
		DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
        char /*strValue[128], */strCodeMeaning[64], strCodeValue[64];
//		bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
        long lNumOfRefInstanceItems(0);
        tagKey.group = 0x0008, tagKey.element = 0x114a;
        DcmSequenceOfItemsHandle pSeqOfRefInstance = dcmDatasetReq.findSequence(
            pItemReq, tagKey, bSearchChild
            );
        if (pSeqOfRefInstance)
            lNumOfRefInstanceItems = dcmDatasetReq.getNumOfItems(pSeqOfRefInstance);
        for (long nIndexOfRefInstanceItem=0; nIndexOfRefInstanceItem<lNumOfRefInstanceItems; ++nIndexOfRefInstanceItem)
        {
            DcmItemHandle pItemRefInstance = dcmDatasetReq.getItemFromSeq(pSeqOfRefInstance, nIndexOfRefInstanceItem);
            ///. UserID, 
            ///. <element tag="0008,0104" vr="LO" vm="1" len="0" type="1" name="Code Meaning">Processing Type</element>
            tagKey.group = 0x0008, tagKey.element = 0x0104;
            nError = dcmDatasetReq.findAndGetString(
                pItemRefInstance, tagKey, strCodeMeaning, lPos, bSearchChild
                );
            ///. <element tag="0008,0100" vr="SH" vm="1" len="0" type="1" name="Code Value">111724</element>
            tagKey.group = 0x0008, tagKey.element = 0x0100;
            nError = dcmDatasetReq.findAndGetString(
                pItemRefInstance, tagKey, strCodeValue, lPos, bSearchChild
                );
            QString strFilter = QString(
                "SELECT UserID, AETitle, IPAddress, PortNumber, UserName, Role, TLS"
                " FROM RemoteAE WHERE %1='%2' ORDER BY UserID"
                ).arg(strCodeMeaning).arg(strCodeValue);
            ///. query database
            if (queryRemoteAE.exec(strFilter) == true)
            {
                if (queryRemoteAE.next() == true)
                {
                    queryRemoteAE.finish();
                    ///.modify RemoteAE if there is
                    strFilter= QString(
                        "UPDATE RemoteAE"
                        " SET AETitle=:%1, IPAddress=:%2, PortNumber=:%3, UserName=:%4, Role=:%5, TLS=:%6"
                        " WHERE UserID=:%7"
                        ).
                        arg(pRemoteAEs[1]).arg(pRemoteAEs[2]).arg(pRemoteAEs[3]).
                        arg(pRemoteAEs[4]).arg(pRemoteAEs[5]).arg(pRemoteAEs[6]).arg(pRemoteAEs[0]);
                }
                else
                {
                    queryRemoteAE.finish();
                    ///.add RemoteAE if there is not
                    strFilter= QString(
                        "INSERT INTO"
                        " RemoteAE (UserID, AETitle, IPAddress, PortNumber, UserName, Role, TLS)"
                        " VALUES (:%1, :%2, :%3, :%4, :%5, :%6, :%7)"
                        ).
                        arg(pRemoteAEs[0]).arg(pRemoteAEs[1]).arg(pRemoteAEs[2]).arg(pRemoteAEs[3]).
                        arg(pRemoteAEs[4]).arg(pRemoteAEs[5]).arg(pRemoteAEs[6]);
                }
                queryRemoteAE.prepare(strFilter);

                strFilter = QString(":%1").arg(strCodeMeaning);
                queryRemoteAE.bindValue(strFilter, strCodeValue);
                ///. DCM_InstitutionCodeSequence
                long lNumOfInstitutionCodeItems(0);
                tagKey.group = 0x0008, tagKey.element = 0x0082;
                DcmSequenceOfItemsHandle pSeqOfInstitutionCode = dcmDatasetReq.findSequence(
                    pItemRefInstance, tagKey, bSearchChild
                    );
                if (pSeqOfInstitutionCode)
                    lNumOfInstitutionCodeItems = dcmDatasetReq.getNumOfItems(pSeqOfInstitutionCode);
                for (long nIndexOfInstitutionCodeItem=0; nIndexOfInstitutionCodeItem<lNumOfInstitutionCodeItems; ++nIndexOfInstitutionCodeItem)
                {
                    DcmItemHandle pItemInstitutionCode = dcmDatasetReq.getItemFromSeq(
                        pSeqOfInstitutionCode, nIndexOfInstitutionCodeItem
                        );
                    ///. <element tag="0008,0104" vr="LO" vm="1" len="0" type="1" name="Code Meaning">Processing Type</element>
                    tagKey.group = 0x0008, tagKey.element = 0x0104;
                    nError = dcmDatasetReq.findAndGetString(
                        pItemInstitutionCode, tagKey, strCodeMeaning, lPos, bSearchChild
                        );
                    ///. <element tag="0008,0100" vr="SH" vm="1" len="0" type="1" name="Code Value">111724</element>
                    tagKey.group = 0x0008, tagKey.element = 0x0100;
                    nError = dcmDatasetReq.findAndGetString(
                        pItemInstitutionCode, tagKey, strCodeValue, lPos, bSearchChild
                        );
                    ///.
                    strFilter = QString(":%1").arg(strCodeMeaning);
                    queryRemoteAE.bindValue(strFilter, strCodeValue);
                }
                ///. end of for (long nIndexOfInstitutionCodeItem=0; nIndexOfInstitutionCodeItem<lNumOfInstitutionCodeItems; ++nIndexOfInstitutionCodeItem)
                if (queryRemoteAE.exec())
                {
    			    nError = 1;
                }
                else
                {
                    m_strLastError = queryRemoteAE.lastError().text();
    			    nError = -1;
                }

                queryRemoteAE.finish();
            }
            else
            {
                nError = -1;
                m_strLastError = queryRemoteAE.lastError().text();
                break;
            }
            ///. end of if (queryRemoteAE.exec(strFilter) == true)
        }
        ///. end of for (long nIndexOfRefInstanceItem=0; nIndexOfRefInstanceItem<lNumOfRefInstanceItems; ++nIndexOfRefInstanceItem)

        bFlag = m_SqlDatabase.commit();
    }
	catch (...)
	{
        m_strLastError = queryRemoteAE.lastError().text();
        bFlag = m_SqlDatabase.rollback();
		return -1;
	}
    return nError;
}

int QtSqlPersistence::getRemoteAE(DcmItemHandle pItemRoot)
{
	int nError(0);
    bool bFlag(false), bSearchChild(false), bVerbose(false);
    unsigned long lPos(0);

    QSqlQuery queryRemoteAE;
    queryRemoteAE.setForwardOnly(true);

	try
	{
        DataSet dcmDatasetReq;
//        nError = dcmDatasetReq.load(pItemRoot);
//        DcmItemHandle pItemReq = dcmDatasetReq.getRoot();
        DcmItemHandle pItemReq = pItemRoot;
        if (bVerbose)
        {
            DataSet dcmTemp;
            nError = dcmTemp.load(pItemReq);
            dcmTemp.save("..\\..\\temp\\SqlRemoteAEReq.dcm", 0);
        }

        //DCM_SOPInstanceUID
		DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
//		bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);

        QString strFilter = QString(
            "SELECT UserID, AETitle, IPAddress, PortNumber, UserName, Role, TLS FROM RemoteAE ORDER BY UserID"
            );
        ///. query database
        if (queryRemoteAE.exec(strFilter) == true &&
            queryRemoteAE.next() == true)
        {
            int i(0), nCountOfValues(1);
            long lItemNum(-2);
            bool bReplaced = true;
            do
            {
                tagKey.group = 0x0008, tagKey.element = 0x114a;
                const DcmItemHandle pSeqItemOfRefInstance = dcmDatasetReq.findOrCreateSequenceItem(
                    pItemReq, tagKey, lItemNum
                    );
                if (pSeqItemOfRefInstance)
                {
                    ///. UserID, 
                    const char* pstrSeqCodeMeaning = "RemoteAE";
                    const char* pstrSeqValueType = "TEXT";
                    int nFieldNo(0);
                    ///. <element tag="0008,0104" vr="LO" vm="1" len="0" type="1" name="Code Meaning">Processing Type</element>
                    tagKey.group = 0x0008, tagKey.element = 0x0104;
                    nError = dcmDatasetReq.setStringValue(
                        pSeqItemOfRefInstance, tagKey, pstrSeqCodeMeaning, bReplaced
                        );
                    ///. <element tag="0040,A040" vr="CS" vm="1" len="0" type="1" name="Value Type">TEXT</element>
                    tagKey.group = 0x0040, tagKey.element = 0xA040;
                    nError = dcmDatasetReq.setStringValue(
                        pSeqItemOfRefInstance, tagKey, pstrSeqValueType, bReplaced
                        );
                    ///. <element tag="0008,0100" vr="SH" vm="1" len="0" type="1" name="Code Value">111724</element>
                    tagKey.group = 0x0008, tagKey.element = 0x0100;
                    nError = dcmDatasetReq.setStringValue(
                        pSeqItemOfRefInstance, tagKey, queryRemoteAE.value(nFieldNo).toString().toStdString().c_str(), bReplaced
                        );
                    
                    ///. AETitle, IPAddress, PortNumber, UserName, Role
                    while (nFieldNo < nNumOfElemetsInRemoteAE-1)
                    {
                        ///. DCM_InstitutionCodeSequence
                        ++ nFieldNo;
                        tagKey.group = 0x0008, tagKey.element = 0x0082;
                        const DcmItemHandle pSeqItemOfInstitutionCode = dcmDatasetReq.findOrCreateSequenceItem(
                            pSeqItemOfRefInstance, tagKey, lItemNum
                            );
                        if (pSeqItemOfInstitutionCode)
                        {
                           ///. <element tag="0008,0104" vr="LO" vm="1" len="0" type="1" name="Code Meaning">Processing Type</element>
                            tagKey.group = 0x0008, tagKey.element = 0x0104;
                            nError = dcmDatasetReq.setStringValue(
                                pSeqItemOfInstitutionCode, tagKey, pRemoteAEs[nFieldNo], bReplaced
                                );
                            ///. <element tag="0040,A040" vr="CS" vm="1" len="0" type="1" name="Value Type">TEXT</element>
                            tagKey.group = 0x0040, tagKey.element = 0xA040;
                            nError = dcmDatasetReq.setStringValue(
                                pSeqItemOfInstitutionCode, tagKey, pstrSeqValueType, bReplaced
                                );
                            ///. <element tag="0008,0100" vr="SH" vm="1" len="0" type="1" name="Code Value">111724</element>
                            tagKey.group = 0x0008, tagKey.element = 0x0100;
                            nError = dcmDatasetReq.setStringValue(
                                pSeqItemOfInstitutionCode, tagKey, queryRemoteAE.value(nFieldNo).toString().toStdString().c_str(), bReplaced
                                );
                        }
                        ///. end of if (pSeqItemOfInstitutionCode)
                    }
                    ///. end of while (nFieldNo < nNumOfElemetsInRemoteAE)
                }
                ///. end of if (pSeqItemOfRefInstance)

                ++ i;
            }
            while (queryRemoteAE.next() == true);
            ///. end of while (queryImage.next() == true)

            queryRemoteAE.finish();
            nError = i;
        }
        else
        {
            nError = -1;
            m_strLastError = queryRemoteAE.lastError().text();
        }
        ///. end of if (queryRemoteAE.exec(strFilter) == true)
    }
	catch (...)
	{
        m_strLastError = queryRemoteAE.lastError().text();
		return -1;
	}
    return nError;
}

int QtSqlPersistence::getImageInfo(DcmItemHandle pItemRoot)
{
	int nError(0);
    bool bFlag(false), bSearchChild(false), bVerbose(false);
    unsigned long lPos(0);

    QSqlQuery queryImage;
    queryImage.setForwardOnly(true);

	try
	{
        DataSet dcmDatasetReq;
        nError = dcmDatasetReq.load(pItemRoot);
        DcmItemHandle pItemReq = dcmDatasetReq.getRoot();
        if (bVerbose)
        {
            dcmDatasetReq.save("..\\..\\temp\\SqlImageInfoReq.dcm", 0);
        }

        QString strSOPInstanceUID;
        char strValue[128];
        short nValue;

        //DCM_SOPInstanceUID
		DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
		bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
        if (bFlag && strlen(strValue)>0)
        {
            strSOPInstanceUID = strValue;
        }
        else
        {
            strSOPInstanceUID = "%";
        }
		//QString strFilter = QString("SELECT SE.Modality, SE.SeriesNumber, "
  //          "IM.SOPInstanceUID, IM.InstanceNumber, IM.SOPClassUID, IM.SeriesInstanceUID, SE.StudyInstanceUID "
  //          "FROM Series SE, Image IM "
  //          "WHERE IM.SeriesInstanceUID=SE.SeriesInstanceUID AND IM.SOPInstanceUID LIKE '%1'").
  //          arg(strSOPInstanceUID);
		QString strFilter = QString(
            "SELECT SOPInstanceUID, InstanceNumber, SOPClassUID, SeriesInstanceUID, StudyInstanceUID"
            ", PixelDataProviderURL, ImagedVolumeWidth, ImagedVolumeHeight, ImagedVolumeDepth"
            ", TotalPixelMatrixColumns, TotalPixelMatrixRows, Rows, Columns"
            ", PhotometricInterpretation, SamplesPerPixel, BitsAllocated"
            ", TransferSyntaxUID"
            " FROM Image WHERE SOPInstanceUID LIKE '%1'").
            arg(strSOPInstanceUID);

        //DCM_SeriesInstanceUID
		tagKey.group = 0x0020, tagKey.element = 0x000e;
		bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
        if (bFlag && strlen(strValue)>0)
        {
            strFilter += " AND SeriesInstanceUID LIKE '";
            strFilter += strValue;
            strFilter += "'";
        }
        //DCM_InstanceNumber
		tagKey.group = 0x0020, tagKey.element = 0x0013;
		bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
        if (bFlag && strlen(strValue)>0)
        {
            strFilter += " AND InstanceNumber LIKE '%";
            strFilter += strValue;
            strFilter += "%'";
        }

        ///. query database
        strFilter.replace("*", "%");
        strFilter += " ORDER BY InstanceNumber";
        if (queryImage.exec(strFilter) == true)
        {
            QDate dtSQLDate;
            int i(0), nCountOfValues(1);
            bool bReplaced = true;
            while (queryImage.next() == true)
            {
                //DCM_SOPInstanceUID
                int nFieldNo(0);
		        tagKey.group = 0x0008, tagKey.element = 0x0018;
                nError = dcmDatasetReq.setStringValue(
                    pItemReq, tagKey, 
                    queryImage.value(nFieldNo).toString().toUtf8(),
                    bReplaced
                    );
                //DCM_InstanceNumber
                ++ nFieldNo;
		        tagKey.group = 0x0020, tagKey.element = 0x0013;
                nError = dcmDatasetReq.setStringValue(
                    pItemReq, tagKey, 
                    queryImage.value(nFieldNo).toString().toUtf8(),
                    bReplaced
                    );
                //DCM_SOPClassUID
                ++ nFieldNo;
		        tagKey.group = 0x0008, tagKey.element = 0x0016;
                nError = dcmDatasetReq.setStringValue(
                    pItemReq, tagKey, 
                    queryImage.value(nFieldNo).toString().toUtf8(),
                    bReplaced
                    );
                //DCM_SeriesInstanceUID
                ++ nFieldNo;
		        tagKey.group = 0x0020, tagKey.element = 0x000e;
                nError = dcmDatasetReq.setStringValue(
                    pItemReq, tagKey, 
                    queryImage.value(nFieldNo).toString().toUtf8(),
                    bReplaced
                    );
                //DCM_StudyInstanceUID
                ++ nFieldNo;
		        tagKey.group = 0x0020, tagKey.element = 0x000d;
                nError = dcmDatasetReq.setStringValue(
                    pItemReq, tagKey, 
                    queryImage.value(nFieldNo).toString().toUtf8(),
                    bReplaced
                    );
                
                //DCM_PixelDataProviderURL
                ++ nFieldNo;
		        tagKey.group = 0x0028, tagKey.element = 0x7FE0;
                nError = dcmDatasetReq.setStringValue(
                    pItemReq, tagKey, 
                    queryImage.value(nFieldNo).toString().toUtf8(),
                    bReplaced
                    );
                //DCM_ImagedVolumeWidth
                ++ nFieldNo;
		        tagKey.group = 0x0048, tagKey.element = 0x0001;
                double fValue = queryImage.value(nFieldNo).toDouble();
                nError = dcmDatasetReq.setDoubleValue(
                    pItemReq, tagKey, &fValue, nCountOfValues, bReplaced
                    );
                //DCM_ImagedVolumeHeight
                ++ nFieldNo;
		        tagKey.group = 0x0048, tagKey.element = 0x0002;
                fValue = queryImage.value(nFieldNo).toDouble();
                nError = dcmDatasetReq.setDoubleValue(
                    pItemReq, tagKey, &fValue, nCountOfValues, bReplaced
                    );
                //DCM_ImagedVolumeDepth
                ++ nFieldNo;
		        tagKey.group = 0x0048, tagKey.element = 0x0003;
                fValue = queryImage.value(nFieldNo).toDouble();
                nError = dcmDatasetReq.setDoubleValue(
                    pItemReq, tagKey, &fValue, nCountOfValues, bReplaced
                    );
                //DCM_TotalPixelMatrixColumns
                ++ nFieldNo;
		        tagKey.group = 0x0048, tagKey.element = 0x0006;
                long lValue = queryImage.value(nFieldNo).toLongLong();
                nError = dcmDatasetReq.setLongValue(
                    pItemReq, tagKey, &lValue, nCountOfValues, bReplaced
                    );
                //DCM_TotalPixelMatrixRows
                ++ nFieldNo;
		        tagKey.group = 0x0048, tagKey.element = 0x0007;
                lValue = queryImage.value(nFieldNo).toLongLong();
                nError = dcmDatasetReq.setLongValue(
                    pItemReq, tagKey, &lValue, nCountOfValues, bReplaced
                    );
                
                ///. Rows
                ++ nFieldNo;
                nValue = queryImage.value(nFieldNo).toInt();
                tagKey.group = 0x0028, tagKey.element = 0x0010;
                nError = dcmDatasetReq.setShortValue(
                    pItemReq, tagKey, &nValue, nCountOfValues, bReplaced
                    );
                ///. Columns
                ++ nFieldNo;
                nValue = queryImage.value(nFieldNo).toInt();
                tagKey.group = 0x0028, tagKey.element = 0x0011;
                nError = dcmDatasetReq.setShortValue(
                    pItemReq, tagKey, &nValue, nCountOfValues, bReplaced
                    );
                ///. PhotometricInterpretation
                ++ nFieldNo;
                tagKey.group = 0x0028, tagKey.element = 0x0004;
                nError = dcmDatasetReq.setStringValue(
                    pItemReq, tagKey, queryImage.value(nFieldNo).toString().toUtf8(), bReplaced
                    );
                //SamplesPerPixel
                ++ nFieldNo;
                nValue = queryImage.value(nFieldNo).toInt();
                tagKey.group = 0x0028, tagKey.element = 0x0002;
                nError = dcmDatasetReq.setShortValue(
                    pItemReq, tagKey, &nValue, nCountOfValues, bReplaced
                    );
               ///. BitsAllocated
                ++ nFieldNo;
                nValue = queryImage.value(nFieldNo).toInt();
                tagKey.group = 0x0028, tagKey.element = 0x0100;
                nError = dcmDatasetReq.setShortValue(
                    pItemReq, tagKey, &nValue, nCountOfValues, bReplaced
                    );
                ///. TransferSyntaxUID,
                ++ nFieldNo;
                tagKey.group = 0x0002, tagKey.element = 0x0010;
                nError = dcmDatasetReq.setStringValue(
                    pItemReq, tagKey, queryImage.value(nFieldNo).toString().toUtf8(), bReplaced
                    );
                ///.
                if (bVerbose)
                {
                    dcmDatasetReq.save("..\\..\\temp\\SqlImageInfoResp.dcm", 0);
                }

                ///. Module="Image Pixel"
                //Pixel Data Provider URL

                ///. Module="Whole Slide Microscopy Image"
                //ImageType
                //ImagedVolumeWidth
                //ImagedVolumeHeight
                //ImagedVolumeDepth
                //TotalPixelMatrixColumns
                //TotalPixelMatrixRows
                //TotalPixelMatrixOriginSequence
                //LossyImageCompression

                ///. Module="Multi-Resolution Navigation"
                //ReferencedSOPClassUID
                //ReferencedSOPInstanceUID
                //ReferencedFrameNumber
                //TopLeftHandCornerOfLocalizerArea
                //BottomRightHandCornerOfLocalizerArea
                //PixelSpacing
                //ZOffsetInSlideCoordinateSystem
                //SamplesPerPixel


                ///////////////////////////////////////////////////////////
                //DCM_ReferencedImageSequence
                tagKey.group = 0x0008, tagKey.element = 0x1140;
                nError = dcmDatasetReq.copyAndInsertSeqItem(pItemReq, tagKey, pItemRoot);
                if (bVerbose)
                {
                    DataSet dcmTemp;
                    dcmTemp.load(pItemRoot);
                    dcmTemp.save("..\\..\\temp\\SqlImageInfoResp.dcm", 0);
                }

                ++ i;
            }
            ///. end of while (queryImage.next() == true)

            queryImage.finish();
            nError = i;
        }
        else
        {
            nError = -1;
            m_strLastError = queryImage.lastError().text();
        }
        ///. end of if (queryImage.exec(strFilter) == true)
    }
	catch (...)
	{
        m_strLastError = queryImage.lastError().text();
		return -1;
	}
    return nError;
}

int QtSqlPersistence::deleteDcmFiles(DcmItemHandle pItemRoot)
{
    int nError(0);
    bool bSearchChild(false), bVerbose(false);
    unsigned long lPos(0);

    QDir dirTemp;
	try
	{
        DataSet dcmDatasetReq;
//        nError = dcmDatasetReq.load(pItemRoot);
//        DcmItemHandle pItemReq = dcmDatasetReq.getRoot();
        DcmItemHandle pItemReq = pItemRoot;
        if (bVerbose)
        {
            DataSet dsTemp;
            nError = dsTemp.load(pItemRoot);
            nError = dsTemp.save("..\\..\\temp\\SqlDeleteDcmFiles.dcm", 0);
        }
        char strValue[128];
        DataSet::TDcmTagKey tagKey(0,0);
        //DCM_ReferencedImageSequence
        tagKey.group = 0x0008, tagKey.element = 0x1140;
        DcmSequenceOfItemsHandle pSeq = dcmDatasetReq.findSequence(pItemReq, tagKey, bSearchChild);
        ///. remove temp dcm file
        int nNumOfFiles(0);
        long nNumOfItems = dcmDatasetReq.getNumOfItems(pSeq);
        for (long i=0; i<nNumOfItems; ++i)
        {
            const DcmItemHandle pSeqItem = dcmDatasetReq.getItemFromSeq(pSeq, i);
            ///. DCM_ReferencedFileID
            tagKey.group = 0x0004, tagKey.element = 0x1500;
            if (dcmDatasetReq.findAndGetString(pSeqItem, tagKey, strValue, lPos, bSearchChild) == true &&
                dirTemp.exists(strValue) == true &&
                dirTemp.remove( strValue ) == true)
            {
                
                ++ nNumOfFiles;
            }
            ///. end of if (dirTemp.exists(strValue) == true)
        }
        ///. end of for (long i=0; i<nNumOfItems; ++i)
        nError = nNumOfFiles;
    }
	catch (...)
	{
        m_strLastError = "Error deleting temp DCM files";
		return -1;
	}
    return nError;
}

//int QtSqlPersistence::getTempFileInfo(DcmItemHandle pItemRoot)
int QtSqlPersistence::getAndStoreTempFileInfo(DcmItemHandle pItemRoot, Service& dcmSCP, unsigned int nSessionIndex)
{
    int nError(0);
    bool bFlag(false), bSearchChild(false), bVerbose(false);
    unsigned long lPos(0);

    QSqlQuery queryImage;
    queryImage.setForwardOnly(true);
	try
	{
        DataSet dcmDatasetReq;
        nError = dcmDatasetReq.load(pItemRoot);
        DcmItemHandle pItemReq = dcmDatasetReq.getRoot();
        if (bVerbose)
        {
            dcmDatasetReq.save("..\\..\\temp\\SqlImageFileInfoReq.dcm", 0);
        }

        ///. Step#1: preprare the filter for query
        QString strSOPInstanceUID(""), strFilter("");
        char strValue[128];
        DataSet::TDcmTagKey tagKey(0,0);
	    /// QueryRetrieveLevel
        tagKey.group=0x0008, tagKey.element=0x0052;
        bFlag = dcmDatasetReq.findAndGetString(pItemRoot, tagKey, strValue, 0, false);
        if (bFlag == false)
        {   ///. it is not query
        }
        else if (strLevelStudy.compare(strValue, Qt::CaseInsensitive) == 0)
        {
        }
        else if (strLevelSeries.compare(strValue, Qt::CaseInsensitive) == 0)
        {
        }
        else if (strLevelImage.compare(strValue, Qt::CaseInsensitive) == 0)
        {
            //DCM_SOPInstanceUID
		    tagKey.group=0x0008, tagKey.element=0x0018;
		    bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
            if (bFlag && strlen(strValue)>0)
            {
                strSOPInstanceUID = strValue;
            }
            else
            {
                strSOPInstanceUID = "%";
            }
            strFilter = QString("SELECT "
                "StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID, SOPClassUID, InstanceNumber "
                "FROM Image "
                "WHERE SOPInstanceUID LIKE '%1'").
                arg(strSOPInstanceUID);
            //DCM_SeriesInstanceUID
		    tagKey.group = 0x0020, tagKey.element = 0x000e;
		    bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
            if (bFlag && strlen(strValue)>0)
            {
                strFilter += " AND SeriesInstanceUID LIKE '";
                strFilter += strValue;
                strFilter += "'";
            }
        }
        else
        {
        }
        long lReferencedFrameNumber = -1;
        ///. DCM_ReferencedImageNavigationSequence
        tagKey.group=0x0048, tagKey.element=0x0200;
        DcmSequenceOfItemsHandle pSeqOfNavigation = dcmDatasetReq.findSequence(pItemReq, tagKey, bSearchChild);
        if (pSeqOfNavigation && dcmDatasetReq.getNumOfItems(pSeqOfNavigation)>0)
        {
            const DcmItemHandle pItemOfSeq = dcmDatasetReq.getItemFromSeq( pSeqOfNavigation, lPos );
            ///. ReferencedFrameNumber
            tagKey.group=0x0008, tagKey.element=0x1160;
            lReferencedFrameNumber = dcmDatasetReq.findAndGetLong(pItemOfSeq, tagKey, lPos, bSearchChild);
        }
        ///. Step#2: perform QB query
        if (strFilter.length() > 0)
        {
            ///. query database
            strFilter.replace("*", "%");
            if (queryImage.exec(strFilter) == true)
            {
                QDate dtSQLDate;
                int nNumOfFound(0);
                
                DataSet dcmDatasetTemp;
                bool bReplaced = true;
                while (queryImage.next() == true)
                {
                    ///. DCM_ReferencedFileID
                    int nFieldNo(0);
                    QString strStudyInstanceUID(queryImage.value(nFieldNo).toString());
                    ++ nFieldNo;//DCM_SeriesInstanceUID
                    QString strSeriesInstanceUID(queryImage.value(nFieldNo).toString());
                    ++ nFieldNo;//DCM_SOPInstanceUID
                    strSOPInstanceUID = queryImage.value(nFieldNo).toString();
                    ++ nFieldNo;//DCM_SOPClasseUID
                    QString strSOPClassUID(queryImage.value(nFieldNo).toString());
                    ++ nFieldNo;//
                    long lInstanceNumber = queryImage.value(nFieldNo).toLongLong();
                    ///. DCM_ReferencedFileID
                    QString strReferencedFileInDcm = QString("%1//%2//%3//%4").
                        arg(m_strWorkingDir).
                        arg(strStudyInstanceUID).
                        arg(strSeriesInstanceUID).
                        arg(strSOPInstanceUID);
                    strReferencedFileInDcm.replace("\\", "//");
                    ///.
                    QDir dirConcatenation(strReferencedFileInDcm);
                    if (dirConcatenation.exists() == true)
                    {
                        if (lReferencedFrameNumber < 0)
                        {
                            dirConcatenation.setFilter(QDir::Files/* | QDir::Hidden | QDir::NoSymLinks*/);
                            //dirConcatenation.setSorting(QDir::Size | QDir::Reversed);
                            QFileInfoList listFiles= dirConcatenation.entryInfoList();
                            for (int i = 0; i < listFiles.size(); ++i)
                            {
                                QFileInfo fileInfo = listFiles.at(i);
                                //std::cout << qPrintable(QString("%1 %2").arg(fileInfo.size(), 10).arg(fileInfo.fileName()));
                                //std::cout << std::endl;
                                if ( dcmDatasetTemp.load( fileInfo.filePath().toUtf8() ) != 0)
                                {
                                    continue;
                                }
                                if (bVerbose)
                                {
                                    QDateTime dtTemp = QDateTime::currentDateTime();
                                    QString strReferencedFileIMG = QString("%1//temp%2%3%4.%5%6%7%8.dcm").
                                        arg(m_strWorkingDir).
                                        arg(dtTemp.date().year()).
                                        arg(dtTemp.date().month()).
                                        arg(dtTemp.date().day()).
                                        arg(dtTemp.time().hour()).
                                        arg(dtTemp.time().minute()).
                                        arg(dtTemp.time().second()).
                                        arg(dtTemp.time().msec());
                                    strReferencedFileIMG.replace("\\", "//");
                                    dcmDatasetTemp.save( strReferencedFileIMG.toUtf8(), 0 );
                                }
                                nError = dcmSCP.store(nSessionIndex, dcmDatasetTemp);
                                ++ nNumOfFound;
                            }
                            ///. end of for (int i = 0; i < listFiles.size(); ++i)
                        }
                        else
                        {
                            QString strReferencedFileInDcmFrm = QString("%1//%2.%3")
                                .arg(strReferencedFileInDcm).arg(strSOPInstanceUID).arg(lReferencedFrameNumber+1);
                            if (QFile::exists(strReferencedFileInDcmFrm) == true &&
                                dcmDatasetTemp.load( strReferencedFileInDcmFrm.toUtf8() ) == 0)
                            {
                                nError = dcmSCP.store(nSessionIndex, dcmDatasetTemp);
                                ++ nNumOfFound;
                            }
                        }
                        ///. end of if (lReferencedFrameNumber < 0)
                    }
                    else
                    {
                        QDateTime dtTemp = QDateTime::currentDateTime();
                        //QString strReferencedFileIMG = QString("%1//%2//%3//%4.%5").
                        //    arg(m_strWorkingDir).
                        //    arg(strStudyInstanceUID).
                        //    arg(strSeriesInstanceUID).
                        //    arg(strSOPInstanceUID).
                        //    arg(strFileExtension);
                        nError = dcmDatasetTemp.load( strReferencedFileInDcm.toUtf8() );
                        DcmItemHandle pItemTemp = dcmDatasetTemp.getRoot();
                        short nValue[2];
                        long lValue(0);

                        ///. ImageType: LOCALIZER, VOLUME, LABEL \\ NONE, RESAMPLED.
                        tagKey.group = 0x0008, tagKey.element = 0x0008;
                        nError = dcmDatasetTemp.findAndGetString(pItemTemp, tagKey, strValue, lPos+2, bSearchChild);
                        bool bLocalizer(false);
                        if (strcmp(strValue, "LOCALIZER") == 0)
                        {
                            bLocalizer = true;
                        }
                        ///. DCM_ReferencedImageNavigationSequence
                        tagKey.group = 0x0048, tagKey.element = 0x0200;
                        DcmSequenceOfItemsHandle pSeqOfNavigation = dcmDatasetTemp.findSequence(pItemTemp, tagKey, bSearchChild);
                        if (bLocalizer && pSeqOfNavigation)
                        {
                            const DcmItemHandle pSeqItem = dcmDatasetTemp.getItemFromSeq(pSeqOfNavigation, lPos);
                            //ReferencedSOPInstanceUID
                            tagKey.group = 0x0008, tagKey.element = 0x1155;
                            nError = dcmDatasetTemp.findAndGetString(pSeqItem, tagKey, strValue, lPos, bSearchChild);
                            QString strReferencedFileInImg = QString("%1//%2//%3//%4.%5").
                                arg(m_strWorkingDir).
                                arg(strStudyInstanceUID).
                                arg(strSeriesInstanceUID).
                                arg(strValue).
                                arg(strFileExtension);
                            strReferencedFileInImg.replace("\\", "//");

                            ///.
                            sedeen::image::ImageRepository& imageRepository = sedeen::image::ImageRepository::instance();
                            sedeen::image::ImageAccessor imageAccessor = imageRepository.get(
                                strReferencedFileInImg.toStdString()
                                );
                            ///.image data
                            int nIndexOfLevel(lInstanceNumber-1);
                            sedeen::Size sizeDimensionOfLevel = imageAccessor.getDimensions(nIndexOfLevel);
                            const sedeen::image::ImageParser& imageParser = imageAccessor.imageParser();
                            sedeen::image::Color colorImage = imageParser.color();
                            sedeen::SizeF sizePixelInImage = imageParser.pixelSize();
                            sedeen::image::ImagePyramid* pImagePyramid = imageParser.pyramid().get();
                            sedeen::Size sizeDimensionOfTile = pImagePyramid->tileSize();
                            int nBufWidth, nBufHeight, nBufBPP;
                            if (sizeDimensionOfTile.width() > sizeDimensionOfLevel.width())
                                nBufWidth = sizeDimensionOfLevel.width();
                            else
                                nBufWidth = sizeDimensionOfTile.width();
                            if (sizeDimensionOfTile.height() > sizeDimensionOfLevel.height())
                                nBufHeight = sizeDimensionOfLevel.height();
                            else
                                nBufHeight = sizeDimensionOfTile.height();
                            ///. ?
                            //nBufBPP = imageParser.bpp() / 8;
                            int nNumOfFrmInX = pImagePyramid->cols(nIndexOfLevel);
                            int nNumOfFrmInY = pImagePyramid->rows(nIndexOfLevel);
    //                        long lLenOfLevel(nBufWidth * nBufHeight * nBufBPP * nNumOfFrmInX * nNumOfFrmInY);
    //                        long lLenOfTile(nBufWidth * nBufHeight * nBufBPP);
                            sedeen::Point pointTile(0, 0);
                            sedeen::Size sizeTile(nBufWidth, nBufHeight);
                            sedeen::Rect regionTile( pointTile, sizeTile );
                            int nPosX(0), nPosY(0), nOffsetX(nNumOfFrmInX), nOffsetY(nNumOfFrmInY);
                            if (lReferencedFrameNumber < 0 || lReferencedFrameNumber>nNumOfFrmInY*nNumOfFrmInX-1)
                            {
                            }
                            else
                            {
                                nPosY = lReferencedFrameNumber/nNumOfFrmInX;
                                if (nPosY+1 < nNumOfFrmInY)
                                    nOffsetY = nPosY + 1;
                                //else
                                //    nOffsetY = nPosY;
                                nPosX = lReferencedFrameNumber%nNumOfFrmInX;
                                if (nPosX+1 < nNumOfFrmInX)
                                    nOffsetX = nPosX + 1;
                                //else
                                //    nOffsetX = nPosX;
                            }
                            for (int j=nPosY; j<nOffsetY; ++j)
                            {
                                regionTile.setY( j*nBufHeight );
                                for (int i=nPosX; i<nOffsetX; ++i)
                                {
                                    regionTile.setX( i*nBufWidth );
                                    sedeen::image::ThreadedRegionBlock blockTile(
                                        imageAccessor, nIndexOfLevel, regionTile
                                        );
                                    sedeen::image::RawImage imageTile = blockTile.getImage();
                                    const unsigned char *pBufTile = static_cast<const unsigned char*>( imageTile.data().get() );
                                    long lFrameNumber(j*nNumOfFrmInX + i);
                                    nBufBPP = imageTile.bytesPerPixel();
                                    long lLenOfTile(nBufWidth * nBufHeight * nBufBPP);
    //#ifdef _DEBUG
    //                                FILE	*fp;
    //                                if ((fp=fopen(strFileName.toStdString().c_str(), "wb")) != NULL)
    //                                {
    //                                    size_t lLenTemp = fwrite(pBufImage, sizeof(unsigned char), lLenOfLevel, fp);
    //                                    fclose(fp);
    //                                }
    //#endif
                                    //sop instance uid
                                    QString strSOPInstanceUIDTemp = QString("%1.%2").arg(strSOPInstanceUID).arg(lFrameNumber+1);
                                    tagKey.group = 0x0008, tagKey.element = 0x0018;
                                    bFlag = dcmDatasetTemp.setStringValue( pItemTemp, tagKey, strSOPInstanceUIDTemp.toUtf8(), bReplaced );
                                    ///. PixelData
                                    tagKey.group = 0x7fe0, tagKey.element = 0x0010;
                                    nError = dcmDatasetTemp.setShortValue(
                                        pItemTemp, tagKey, (short*)pBufTile, lLenOfTile/2, bReplaced
                                        );
                                    ///. number of frames
                                    nValue[0] = 1;
                                    tagKey.group = 0x0028, tagKey.element = 0x0008;
                                    nError = dcmDatasetTemp.setShortValue( pItemTemp, tagKey, nValue, 1, bReplaced );
                                     ///.InConcatenationNumber
                                    nValue[0] = lFrameNumber;
                                    tagKey.group = 0x0020, tagKey.element = 0x9162;
                                    nError = dcmDatasetTemp.setShortValue( pItemTemp, tagKey, nValue, 1, bReplaced );
                                    ///.
                                    //ReferencedSOPInstanceUID
                                    tagKey.group = 0x0008, tagKey.element = 0x1155;
                                    nError = dcmDatasetTemp.setStringValue(
                                        pSeqItem, tagKey, strSOPInstanceUID.toUtf8(), bReplaced
                                        );
                                    ///. ReferencedFrameNumber
                                    tagKey.group = 0x0008, tagKey.element = 0x1160;
                                    nError = dcmDatasetTemp.setLongValue(
                                        pSeqItem, tagKey, &lFrameNumber, 1, bReplaced
                                        );
                                    //DCM_TopLeftHandCornerOfLocalizerArea
                                    tagKey.group = 0x0048, tagKey.element = 0x0201;
                                    nValue[0] = regionTile.x();
                                    nValue[1] = regionTile.y();
                                    nError = dcmDatasetTemp.setShortValue(
                                        pSeqItem, tagKey, nValue, 2, bReplaced
                                        );
                                    //DCM_BottomRightHandCornerOfLocalizerArea
                                    tagKey.group = 0x0048, tagKey.element = 0x0202;
                                    nValue[0] += (regionTile.width()-1);
                                    nValue[1] += (regionTile.height()-1);
                                    nError = dcmDatasetTemp.setShortValue( pSeqItem, tagKey, nValue, 2, bReplaced );
                                    if (bVerbose)
                                    {
                                        QString strReferencedFileTemp = QString("%1//temp%2%3%4.%5%6%7%8.%9.dcm").
                                            arg(m_strWorkingDir).
                                            arg(dtTemp.date().year()).
                                            arg(dtTemp.date().month()).
                                            arg(dtTemp.date().day()).
                                            arg(dtTemp.time().hour()).
                                            arg(dtTemp.time().minute()).
                                            arg(dtTemp.time().second()).
                                            arg(dtTemp.time().msec()).
                                            arg(lFrameNumber);
                                        strReferencedFileTemp.replace("\\", "//");
                                        nError = dcmDatasetTemp.save( strReferencedFileTemp.toUtf8(), 0 );
                                    }
    /*
                                    ///. DCM_ReferencedFileID
                                    tagKey.group = 0x0004, tagKey.element = 0x1500;
                                    nError = dcmDatasetReq.setStringValue(
                                        pItemReq, tagKey, 
                                        strReferencedFileTemp.toUtf8(),
                                        bReplaced
                                        );
                                    //Referenced SOP Class UID
                                    nFieldNo = 3;
		                            tagKey.group = 0x0008, tagKey.element = 0x1150;
                                    nError = dcmDatasetReq.setStringValue(
                                        pItemReq, tagKey, strSOPClassUID.toUtf8(), bReplaced
                                        );
                                    //Referenced SOP Instance UID
                                    ++ nFieldNo;
		                            tagKey.group = 0x0008, tagKey.element = 0x1155;
                                    nError = dcmDatasetReq.setStringValue(
                                        pItemReq, tagKey, strSOPInstanceUIDTemp.toUtf8(), bReplaced
                                        );
                        
                                    //DCM_ReferencedImageSequence
                                    tagKey.group = 0x0008, tagKey.element = 0x1140;
                                    nError = dcmDatasetReq.copyAndInsertSeqItem(pItemReq, tagKey, pItemRoot);
                                    */
                                    nError = dcmSCP.store(nSessionIndex, dcmDatasetTemp);
                                }
                                ///. end of for for (int i=0; i<nNumOfFrmInX; ++i)
                            }
                            ///. end of for (int j=0; j<nNumOfFrmInY; ++j)
                        }
                        else
                        {
                            if (bVerbose)
                            {
                                QString strReferencedFileIMG = QString("%1//temp%2%3%4.%5%6%7%8.dcm").
                                    arg(m_strWorkingDir).
                                    arg(dtTemp.date().year()).
                                    arg(dtTemp.date().month()).
                                    arg(dtTemp.date().day()).
                                    arg(dtTemp.time().hour()).
                                    arg(dtTemp.time().minute()).
                                    arg(dtTemp.time().second()).
                                    arg(dtTemp.time().msec());
                                strReferencedFileIMG.replace("\\", "//");
                                dcmDatasetTemp.save( strReferencedFileIMG.toUtf8(), 0 );
                            }
    /*
                            ///. DCM_ReferencedFileID
                            tagKey.group = 0x0004, tagKey.element = 0x1500;
                            nError = dcmDatasetReq.setStringValue(
                                pItemReq, tagKey, 
                                strReferencedFileIMG.toUtf8(),
                                bReplaced
                                );
                            //Referenced SOP Class UID
                            nFieldNo = 3;
		                    tagKey.group = 0x0008, tagKey.element = 0x1150;
                            nError = dcmDatasetReq.setStringValue(
                                pItemReq, tagKey, strSOPClassUID.toUtf8(), bReplaced
                                );
                            //Referenced SOP Instance UID
                            ++ nFieldNo;
		                    tagKey.group = 0x0008, tagKey.element = 0x1155;
                            nError = dcmDatasetReq.setStringValue(
                                pItemReq, tagKey, strSOPInstanceUID.toUtf8(), bReplaced
                                );
                        
                            //DCM_ReferencedImageSequence
                            tagKey.group = 0x0008, tagKey.element = 0x1140;
                            nError = dcmDatasetReq.copyAndInsertSeqItem(pItemReq, tagKey, pItemRoot);
                            */
                            nError = dcmSCP.store(nSessionIndex, dcmDatasetTemp);
                        }
                        ///. end of if (bLocalizer && pSeqOfNavigation) 
                        if (bVerbose)
                        {
                            DataSet dcmTemp;
                            dcmTemp.load(pItemRoot);
                            dcmTemp.save("..\\..\\temp\\SqlImageInfoResp.dcm", 0);
                        }

                        ++ nNumOfFound;
                    }
                    ///. end of if (dirConcatenation.exist() == true)
                }
                ///. end of while (queryImage.next() == true)

                queryImage.finish();
                nError = nNumOfFound;
            }
            else
            {
                nError = -1;
                m_strLastError = queryImage.lastError().text();
            }
            ///. end of if (queryImage.exec(strFilter) == true)
        }
        ///. if (strFilter.length() > 0)
    }
	catch (...)
	{
        m_strLastError = queryImage.lastError().text();
		return -1;
	}
    return nError;
}

int QtSqlPersistence::getImageLocalizer(DcmItemHandle pItemRoot)
{
    int nError(-1);
    bool bFlag(false), bSearchChild(false), bVerbose(false), bReplace(true);
    unsigned long lPos(0), lNum(1);

    QSqlQuery queryImage;
    queryImage.setForwardOnly(true);
	try
	{
        DataSet dcmDatasetReq;
        DcmItemHandle pItemReq = pItemRoot;
//        nError = dcmDatasetReq.load(pItemRoot);
//        DcmItemHandle pItemReq = dcmDatasetReq.getRoot();
        if (bVerbose)
        {
            DataSet dcmReqTemp;
            dcmReqTemp.load(pItemRoot);
            dcmReqTemp.save("..\\..\\temp\\SqlImageLocalizerReq.dcm", 0);
        }

        QString strSOPInstanceUID(""), strFilter("");
        char strValue[128];
        DataSet::TDcmTagKey tagKey(0,0);
//        //DCM_SeriesInstanceUID
//		tagKey.group = 0x0020, tagKey.element = 0x000e;
//		bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);

        DataSet dsTemp;
        ///. DCM_ReferencedImageNavigationSequence
        tagKey.group = 0x0048, tagKey.element = 0x0200;
        DcmSequenceOfItemsHandle pSeq = dcmDatasetReq.findSequence(pItemReq, tagKey, bSearchChild);
        long nNumOfItems = dcmDatasetReq.getNumOfItems(pSeq);
        long nNumOfFiles(0);
        for (long i=0; i<nNumOfItems; ++i)
        {
            const DcmItemHandle pSeqItem = dcmDatasetReq.getItemFromSeq(pSeq, i);

            //DCM_ReferencedSOPInstanceUID
		    tagKey.group=0x0008, tagKey.element=0x1155;
		    bFlag = dcmDatasetReq.findAndGetString(pSeqItem, tagKey, strValue, lPos, bSearchChild);
            if (bFlag && strlen(strValue)>0)
            {
                strSOPInstanceUID = strValue;
            }
            else
            {   ///. there is no SOPInstanceUID
                continue;
            }
            strFilter = QString(
                "SELECT StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID, SOPClassUID, InstanceNumber"
                " FROM Image"
                " WHERE SOPInstanceUID = '%1'").
                arg(strSOPInstanceUID);
            ///. query database
            strFilter.replace("*", "%");
            if (queryImage.exec(strFilter) == false ||
                queryImage.next() == false)
            {   ///. there is no record in DB
                m_strLastError = queryImage.lastError().text();
                queryImage.finish();
                continue;
            }

            ///. Gnerate temprary DCM file
            int nFieldNo(0);//DCM_StudyInstanceUID
            QString strStudyInstanceUID(queryImage.value(nFieldNo).toString());
            ++ nFieldNo;//DCM_SeriesInstanceUID
            QString strSeriesInstanceUID(queryImage.value(nFieldNo).toString());
            ++ nFieldNo;//DCM_SOPInstanceUID
            strSOPInstanceUID = queryImage.value(nFieldNo).toString();
            ++ nFieldNo;//DCM_SeriesInstanceUID
            QString strSOPClassUID(queryImage.value(nFieldNo).toString());
            ///. DCM_ReferencedFileID
            QString strReferencedFileIMG = QString("%1//%2//%3//%4.%5").
                arg(m_strWorkingDir).
                arg(strStudyInstanceUID).
                arg(strSeriesInstanceUID).
                arg(strSOPInstanceUID).
                arg(strFileExtension);
            strReferencedFileIMG.replace("\\", "//");

	        /* check if file is readable */
            sedeen::image::ImageRepository& imageRepository = sedeen::image::ImageRepository::instance();
            sedeen::image::ImageAccessor imageAccessor = imageRepository.get(
                strReferencedFileIMG.toStdString()
                );
            sedeen::SizeF sizePixel = imageAccessor.getPixelSize();
            unsigned int nLevelOfTotalImage(0);
            sedeen::Size sizeDimensionOfTotalPixel = imageAccessor.getDimensions(nLevelOfTotalImage);
            nLevelOfTotalImage = imageAccessor.getNumResolutionLevels();
         //   if( rsa_dec_canread_file( strReferencedFileJP2.toStdString().c_str() ) == false )
         //   {
		       // //printf( "cannot read '%s'", pstrFileName);
         //       m_strLastError = strReferencedFileJP2;
         //       queryImage.finish();
         //       continue;
	        //}
	        ///* Create decoding object */
	        //pJasperDec = rsa_dec_open_file( strReferencedFileJP2.toStdString().c_str() );
         //   if (pJasperDec == NULL)
         //   {
         //       m_strLastError = strReferencedFileJP2;
         //       queryImage.finish();
         //       continue;
         //   }
            unsigned long nWidthOfTotalPixel = sizeDimensionOfTotalPixel.width();
            unsigned long nHeightOfTotalPixel = sizeDimensionOfTotalPixel.height();
            //DCM_ReferencedFrameNumber
            unsigned short nFrameNumber(0);
            tagKey.group = 0x0008, tagKey.element = 0x1160;
            nFrameNumber = dcmDatasetReq.findAndGetLong( pSeqItem, tagKey, lPos, bSearchChild);
            if ( nFrameNumber >= nLevelOfTotalImage )
            {
                nFrameNumber = nLevelOfTotalImage - 1;
            }
            sedeen::Size sizeDimensionOfThumbNail = imageAccessor.getDimensions(nFrameNumber);
            unsigned long nWidthOfLevelPixel = sizeDimensionOfThumbNail.width();
            unsigned long nHeightOfLevelPixel = sizeDimensionOfThumbNail.height();
            //DCM_TopLeftHandCornerOfLocalizerArea
            tagKey.group = 0x0048, tagKey.element = 0x0201;
            unsigned short nTopLeftX = dcmDatasetReq.findAndGetShort( pSeqItem, tagKey, lPos, bSearchChild);
            unsigned short nTopLeftY = dcmDatasetReq.findAndGetShort( pSeqItem, tagKey, lPos+1, bSearchChild);
            //DCM_BottomRightHandCornerOfLocalizerArea
            tagKey.group = 0x0048, tagKey.element = 0x0202;
            unsigned short nBottomRightX = dcmDatasetReq.findAndGetShort( pSeqItem, tagKey, lPos, bSearchChild);
            unsigned short nBottomRightY = dcmDatasetReq.findAndGetShort( pSeqItem, tagKey, lPos+1, bSearchChild);
            if (nBottomRightX > nTopLeftX+nWidthOfLevelPixel-1)
                nBottomRightX = nTopLeftX+nWidthOfLevelPixel-1;
            if (nBottomRightY > nTopLeftY+nHeightOfLevelPixel-1)
                nBottomRightY = nTopLeftY+nHeightOfLevelPixel-1;
            sedeen::Rect regionThumbnail;
            regionThumbnail.setX( nTopLeftX );
            regionThumbnail.setY( nTopLeftY );
            regionThumbnail.setWidth( nBottomRightX-nTopLeftX+1 );
            regionThumbnail.setHeight( nBottomRightY-nTopLeftY+1 );
	        /* Decode region  */
            sedeen::image::ThreadedRegionBlock regionBlock(
                imageAccessor, nFrameNumber, regionThumbnail
                );
            sedeen::image::RawImage imageThumbnail = regionBlock.getImage();

//	        if (rsa_dec_decoderegion(pJasperDec, nFrameNumber, nTopLeftX, nTopLeftY, nBottomRightX, nBottomRightY) == RSA_DEC_SUCCESS &&
//                (pBuffer=rsa_dec_getimageRGB(pJasperDec, &nBufWidth, &nBufHeight, &nBufBPP)) != NULL)
            if (imageThumbnail.isNull() == false)
            {
                int nBufWidth = imageThumbnail.width();
                int nBufHeight = imageThumbnail.height();
                int nBufBPP = imageThumbnail.bytesPerPixel();
                unsigned int nLenOfBuffer = nBufWidth * nBufHeight * nBufBPP;
                sedeen::image::ConstRawDataPtr pRawData = imageThumbnail.data();
                const unsigned char *pBuffer = static_cast<const unsigned char*>( pRawData.get() );
                ///.
                if (dsTemp.initialize(strSOPClassUID.toStdString().c_str()) == 0)
                {
                    DcmItemHandle pItemResp = dsTemp.getRoot();
                    short nValue[2];
                    if (nBufBPP == 3)
                    {
                        //SamplesPerPixel
                        nValue[0] = 3;
                        tagKey.group = 0x0028, tagKey.element = 0x0002;
                        nError = dsTemp.setShortValue(pItemResp, tagKey, nValue, lNum, bReplace);
                        ///. PhotometricInterpretation
                        tagKey.group = 0x0028, tagKey.element = 0x0004;
                        nError = dsTemp.setStringValue(pItemResp, tagKey, "RGB", bReplace);
                    }
                    else
                    {
                        //SamplesPerPixel
                        nValue[0] = 1;
                        tagKey.group = 0x0028, tagKey.element = 0x0002;
                        nError = dsTemp.setShortValue(pItemResp, tagKey, nValue, lNum, bReplace);
                        ///. PhotometricInterpretation
                        tagKey.group = 0x0028, tagKey.element = 0x0004;
                        nError = dsTemp.setStringValue(pItemResp, tagKey, "MONOCHROME2", bReplace);
                    }
                    ///. Rows
                    nValue[0] = nBufHeight;
                    tagKey.group = 0x0028, tagKey.element = 0x0010;
                    nError = dsTemp.setShortValue(pItemResp, tagKey, nValue, lNum, bReplace);
                    ///. Columns
                    nValue[0] = nBufWidth;
                    tagKey.group = 0x0028, tagKey.element = 0x0011;
                    nError = dsTemp.setShortValue(pItemResp, tagKey, nValue, lNum, bReplace);
                    ///. BitsAllocated
                    nValue[0] = nBufBPP*8;
                    tagKey.group = 0x0028, tagKey.element = 0x0100;
                    nError = dsTemp.setShortValue(pItemResp, tagKey, nValue, lNum, bReplace);
                    ///. BitsStored
                    nValue[0] = nBufBPP*8;
                    tagKey.group = 0x0028, tagKey.element = 0x0101;
                    nError = dsTemp.setShortValue(pItemResp, tagKey, nValue, lNum, bReplace);
                    ///. HighBit
                    nValue[0] = nBufBPP*8-1;
                    tagKey.group = 0x0028, tagKey.element = 0x0102;
                    nError = dsTemp.setShortValue(pItemResp, tagKey, nValue, lNum, bReplace);
                    ///. PixelRepresentation
                    nValue[0] = 0;
                    tagKey.group = 0x0028, tagKey.element = 0x0103;
                    nError = dsTemp.setShortValue(pItemResp, tagKey, nValue, lNum, bReplace);
                    ///. PixelData
                    tagKey.group = 0x7fe0, tagKey.element = 0x0010;
            //#ifdef _DEBUG
            //                FILE	*fp;
            //                if ((fp=fopen(strFileName.toStdString().c_str(), "wb")) != NULL)
            //                {
            //                    fwrite(pDataBuf, sizeof(unsigned char), nBufWidth*nBufHeight*nBufBPP, fp);
            //                    fclose(fp);
            //                }
            //#endif
                    nError = dsTemp.setShortValue(pItemResp, tagKey, (short*)pBuffer, nBufWidth*nBufHeight*nBufBPP/2, bReplace);
                    ///. ImageType
                    tagKey.group = 0x0008, tagKey.element = 0x0008;
                    nError = dsTemp.setStringValue(pItemResp, tagKey, "DERIVED\\PRIMARY\\VOLUME\\RESAMPLED", bReplace);
//                    ///. ImagedVolumeWidth
//                    double fValue = static_cast<double>(nWidthOfTotalPixel);
//                    tagKey.group = 0x0048, tagKey.element = 0x0001;
//                    if (dsTemp.findAndGetFloat(pItemRoot, tagKey, lPos, bSearchChild) < 1.0f)
//                    {
//                        nError = dsTemp.setDoubleValue(pItemResp, tagKey, &fValue, 1, bReplace);
//                    }
//                    ///. ImagedVolumeHeight
//                    fValue = static_cast<double>(nHeightOfTotalPixel);
//                    tagKey.group = 0x0048, tagKey.element = 0x0002;
//                    if (dsTemp.findAndGetFloat(pItemRoot, tagKey, lPos, bSearchChild) < 1.0f)
//                    {
//                        nError = dsTemp.setDoubleValue(pItemResp, tagKey, &fValue, 1, bReplace);
//                    }
//                    ///. ImagedVolumeDepth
//                    fValue = 1.0;
//                    tagKey.group = 0x0048, tagKey.element = 0x0003;
//                    if (dsTemp.findAndGetFloat(pItemRoot, tagKey, lPos, bSearchChild) < 1.0f)
//                    {
//                        nError = dsTemp.setDoubleValue(pItemResp, tagKey, &fValue, 1, bReplace);
//                    }
                    ///. TotalPixelMatrixColumns
                    long lValue = static_cast<long>(nWidthOfTotalPixel);
                    tagKey.group = 0x0048, tagKey.element = 0x0006;
                    nError = dsTemp.setLongValue(pItemResp, tagKey, &lValue, lNum, bReplace);
                    ///. TotalPixelMatrixRows
                    lValue = static_cast<long>(nHeightOfTotalPixel);
                    tagKey.group = 0x0048, tagKey.element = 0x0007;
                    nError = dsTemp.setLongValue(pItemResp, tagKey, &lValue, lNum, bReplace);
                    ///.
                    //DCM_ReferencedImageNavigationSequence
                    tagKey.group = 0x0048, tagKey.element = 0x0200;
                    const DcmItemHandle pItemOfRefImgNavgSeq = dcmDatasetReq.findOrCreateSequenceItem(
                        pItemResp, tagKey, -1
                        );
                    if (pItemOfRefImgNavgSeq)
                    {
                        ///. ReferencedSOPClassUID
                        tagKey.group = 0x0008, tagKey.element = 0x1150;
                        nError = dcmDatasetReq.setStringValue(
                            pItemOfRefImgNavgSeq, tagKey, strSOPClassUID.toStdString().c_str(), bReplace
                            );
                        ///. ReferencedSOPInstanceUID
                        tagKey.group = 0x0008, tagKey.element = 0x1155;
                        nError = dcmDatasetReq.setStringValue(
                            pItemOfRefImgNavgSeq, tagKey, strSOPInstanceUID.toStdString().c_str(), bReplace
                            );
                        ///. ReferencedFrameNumber
                        tagKey.group = 0x0008, tagKey.element = 0x1160;
                        lValue = nFrameNumber;
                        nError = dcmDatasetReq.setLongValue(
                            pItemOfRefImgNavgSeq, tagKey, &lValue, lPos, bReplace
                            );
                        ///. TopLeftHandCornerOfLocalizerArea
                        nValue[0] = nTopLeftX;
                        nValue[1] = nTopLeftY;
                        tagKey.group = 0x0048, tagKey.element = 0x0201;
                        nError = dcmDatasetReq.setShortValue(
                            pItemOfRefImgNavgSeq, tagKey, nValue, lNum+1, bReplace
                            );
                        ///. BottomRightHandCornerOfLocalizerArea
                        nValue[0] = nBottomRightX;
                        nValue[1] = nBottomRightY;
                        tagKey.group = 0x0048, tagKey.element = 0x0202;
                        nError = dcmDatasetReq.setShortValue(
                            pItemOfRefImgNavgSeq, tagKey, nValue, lNum+1, bReplace
                            );
                    }
	                /// Study Instance UID
                    tagKey.group = 0x0020, tagKey.element = 0x000D;
                    dsTemp.setStringValue(pItemResp, tagKey, strStudyInstanceUID.toStdString().c_str(), bReplace);
	                /// Series Instance UID
                    tagKey.group = 0x0020, tagKey.element = 0x000E;
                    dsTemp.setStringValue(pItemResp, tagKey, strSeriesInstanceUID.toStdString().c_str(), bReplace);
                    //sop instance uid
                    tagKey.group = 0x0008, tagKey.element = 0x0018;
                    QDateTime dtTemp = QDateTime::currentDateTime();
                    QString strSOPInstanceUIDTemp = QString("%1%2%3.%4%5%6%7").
                        arg(dtTemp.date().year()).
                        arg(dtTemp.date().month()).
                        arg(dtTemp.date().day()).
                        arg(dtTemp.time().hour()).
                        arg(dtTemp.time().minute()).
                        arg(dtTemp.time().second()).
                        arg(dtTemp.time().msec());
                    dsTemp.setStringValue(pItemResp, tagKey, strSOPInstanceUIDTemp.toStdString().c_str(), bReplace);
                    ///. save temprary dcm file to be c-stored
                    strReferencedFileIMG = QString("%1//temp%2.dcm").
                        arg(m_strWorkingDir).
                        arg(strSOPInstanceUIDTemp);
                    strReferencedFileIMG.replace("\\", "//");
                    nError = dsTemp.save(
                        strReferencedFileIMG.toStdString().c_str(), 0
                        );
                    /////.
                    //tagKey.group = 0x0004, tagKey.element = 0x1500;
                    //nError = dcmDatasetReq.setStringValue(
                    //    pItemReq, tagKey, 
                    //    strReferencedFileJP2.toStdString().c_str(),
                    //    bReplace
                    //    );

                    ///. DCM_ReferencedImageSequence
                    tagKey.group = 0x0008, tagKey.element = 0x1140;
                    const DcmItemHandle pItemOfRefImgSeq = dcmDatasetReq.findOrCreateSequenceItem(
                        pItemRoot, tagKey, -1
                        );
                    if (pItemOfRefImgSeq)
                    {
                        ///. DCM_ReferencedFileID
                        tagKey.group = 0x0004, tagKey.element = 0x1500;
                        nError = dcmDatasetReq.setStringValue(
                            pItemOfRefImgSeq, tagKey, strReferencedFileIMG.toStdString().c_str(), bReplace
                            );
                        ///. Referenced SOP Class UID
		                tagKey.group = 0x0008, tagKey.element = 0x1150;
                        nError = dcmDatasetReq.setStringValue(
                            pItemOfRefImgSeq, tagKey, strSOPClassUID.toStdString().c_str(), bReplace
                            );
                        ///. Referenced SOP Instance UID
                        tagKey.group = 0x0008, tagKey.element = 0x1155;
                        nError = dcmDatasetReq.setStringValue(
                            pItemOfRefImgSeq, tagKey, strSOPInstanceUIDTemp.toStdString().c_str(), bReplace
                            );
                    }
                    if (bVerbose)
                    {
                        DataSet dcmReqTemp;
                        dcmReqTemp.load(pItemRoot);
                        dcmReqTemp.save("..\\..\\temp\\SqlImageInfoResp.dcm", 0);
                    }
                    ++ nNumOfFiles;
                }
                ///. end of if (dsTemp.initialize(strSOPClassUID.toStdString().c_str()) == 0)
 
                pBuffer = NULL;
            }
            else
            {
//                     printf("could not generate bitmap stream.\n ");
                m_strLastError = strReferencedFileIMG;
            }
            ///. end of if (rsa_dec_decoderegion(...) == RSA_DEC_SUCCESS)

            ///.
            queryImage.finish();
            nError = nNumOfFiles;
        }
        ///. end of for (long i=0; i<nNumOfItems; ++i)
    }
	catch (...)
	{
        m_strLastError = queryImage.lastError().text();
		return -1;
	}
    return nError;
}

int QtSqlPersistence::getImageFileInfo(DcmItemHandle pItemRoot)
{
    int nError(0);
    bool bFlag(false), bSearchChild(false), bVerbose(false);
    unsigned long lPos(0);

    QSqlQuery queryImage;
    queryImage.setForwardOnly(true);
	try
	{
        DataSet dcmDatasetReq;
        nError = dcmDatasetReq.load(pItemRoot);
        DcmItemHandle pItemReq = dcmDatasetReq.getRoot();
        if (bVerbose)
        {
            dcmDatasetReq.save("..\\..\\temp\\SqlImageFileInfoReq.dcm", 0);
        }

        ///. Step#1: preprare the filter for query
        QString strSOPInstanceUID(""), strFilter("");
        char strValue[128];
        DataSet::TDcmTagKey tagKey(0,0);
	    /// QueryRetrieveLevel
        tagKey.group=0x0008, tagKey.element=0x0052;
        bFlag = dcmDatasetReq.findAndGetString(pItemRoot, tagKey, strValue, 0, false);
        if (bFlag == false)
        {   ///. it is not query
        }
        else if (strLevelStudy.compare(strValue, Qt::CaseInsensitive) == 0)
        {
        }
        else if (strLevelSeries.compare(strValue, Qt::CaseInsensitive) == 0)
        {
        }
        else if (strLevelImage.compare(strValue, Qt::CaseInsensitive) == 0)
        {
            //DCM_SOPInstanceUID
		    tagKey.group=0x0008, tagKey.element=0x0018;
		    bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
            if (bFlag && strlen(strValue)>0)
            {
                strSOPInstanceUID = strValue;
            }
            else
            {
                strSOPInstanceUID = "%";
            }
            strFilter = QString("SELECT "
                "StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID, SOPClassUID, InstanceNumber "
                "FROM Image "
                "WHERE SOPInstanceUID LIKE '%1'").
                arg(strSOPInstanceUID);
            //DCM_SeriesInstanceUID
		    tagKey.group = 0x0020, tagKey.element = 0x000e;
		    bFlag = dcmDatasetReq.findAndGetString(pItemReq, tagKey, strValue, lPos, bSearchChild);
            if (bFlag && strlen(strValue)>0)
            {
                strFilter += " AND SeriesInstanceUID LIKE '";
                strFilter += strValue;
                strFilter += "'";
            }
        }
        else
        {
        }

        ///. Step#2: perform QB query
        if (strFilter.length() > 0)
        {
            ///. query database
            strFilter.replace("*", "%");
            if (queryImage.exec(strFilter) == true)
            {
                QDate dtSQLDate;
                int i(0);
                bool bReplaced = true;
                while (queryImage.next() == true)
                {
                    ///. DCM_ReferencedFileID
                    int nFieldNo(0);
                    tagKey.group = 0x0004, tagKey.element = 0x1500;
                    QString strReferencedFileID = QString("%1//%2//%3//%4").
                        arg(m_strWorkingDir).
                        arg(queryImage.value(0).toString()).//DCM_StudyInstanceUID
                        arg(queryImage.value(1).toString()).//DCM_SeriesInstanceUID
                        arg(queryImage.value(2).toString());//DCM_SOPInstanceUID
                    strReferencedFileID.replace("\\", "//");
                    nError = dcmDatasetReq.setStringValue(
                        pItemReq, tagKey, 
                        strReferencedFileID.toStdString().c_str(),
                        bReplaced
                        );
                    //Referenced SOP Class UID
                    nFieldNo = 3;
		            tagKey.group = 0x0008, tagKey.element = 0x1150;
                    nError = dcmDatasetReq.setStringValue(
                        pItemReq, tagKey, 
                        queryImage.value(nFieldNo).toString().toStdString().c_str(),
                        bReplaced
                        );
                    //Referenced SOP Instance UID
                    ++ nFieldNo;
		            tagKey.group = 0x0008, tagKey.element = 0x1155;
                    nError = dcmDatasetReq.setStringValue(
                        pItemReq, tagKey, 
                        queryImage.value(nFieldNo).toString().toStdString().c_str(),
                        bReplaced
                        );
                    if (bVerbose)
                    {
                        dcmDatasetReq.save("..\\..\\temp\\SqlImageInfoResp.dcm", 0);
                    }

                    //DCM_ReferencedImageSequence
                    tagKey.group = 0x0008, tagKey.element = 0x1140;
                    nError = dcmDatasetReq.copyAndInsertSeqItem(pItemReq, tagKey, pItemRoot);
                    if (bVerbose)
                    {
                        DataSet dcmTemp;
                        dcmTemp.load(pItemRoot);
                        dcmTemp.save("..\\..\\temp\\SqlImageInfoResp.dcm", 0);
                    }

                    ++ i;
                }
                ///. end of while (queryImage.next() == true)

                queryImage.finish();
                nError = i;
            }
            else
            {
                nError = -1;
                m_strLastError = queryImage.lastError().text();
            }
            ///. end of if (queryImage.exec(strFilter) == true)
        }
        ///. if (strFilter.length() > 0)
    }
	catch (...)
	{
        m_strLastError = queryImage.lastError().text();
		return -1;
	}
    return nError;
}

//int QtSqlPersistence::getStudyInfo( DataSet& dcmDataset/*, QList<DataSet> listDcmDataset*/)
//{
//	int nError(0);
//    bool bFlag(false), bSearchChild(false);
//    unsigned long lPos(0);
//
//    QSqlQuery queryStudy;
//    queryStudy.setForwardOnly(true);
//
//	try
//	{
//        DataSet* pDcmDataset = (DataSet*) &dcmDataset;
//        const DcmItemHandle pItemRoot = pDcmDataset->getRoot();
//
//        QString strStudyInstanceUID, strPatientID, strPatientName, strAccessNumber, strStudyID;
//        char strValue[128];
//
//        //DCM_StudyInstanceUID
//		DataSet::TDcmTagKey tagKey(0x0020, 0x000d);
//		bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
//        if (bFlag && strlen(strValue)>0)
//        {
//            strStudyInstanceUID = strValue;
//        }
//        else
//        {
//            strStudyInstanceUID = "%";
//        }
//		QString strFilter = QString("SELECT PT.PatientID, PT.PatientName, PT.PatientBirthDate, "
//            "ST.StudyInstanceUID, ST.StudyDate, ST.AccessionNumber, ST.StudyID "
//            "FROM Patient PT, Study ST "
//            "WHERE ST.PatientID=PT.PatientID AND ST.PatientName=PT.PatientName AND ST.StudyInstanceUID LIKE '%1'").
//            arg(strStudyInstanceUID);
//        
//        //DCM_PatientID
//		tagKey.group = 0x0010, tagKey.element = 0x0020;
//		bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
//        if (bFlag && strlen(strValue)>0)
//        {
//            strFilter += " AND PT.PatientID LIKE '";
//            strFilter += strValue;
//            strFilter += "'";
//        }
//        //DCM_PatientsName
//		tagKey.group = 0x0010, tagKey.element = 0x0010;
//		bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
//        if (bFlag && strlen(strValue)>0)
//        {
//            strFilter += " AND PT.PatientName LIKE '";
//            strFilter += strValue;
//            strFilter += "'";
//        }
//		//DCM_AccessionNumber
//		tagKey.group = 0x0008, tagKey.element = 0x0050;
//		bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
//        if (bFlag && strlen(strValue)>0)
//        {
//            strFilter += " AND ST.AccessionNumber LIKE '";
//            strFilter += strValue;
//            strFilter += "'";
//        }
//		//DCM_StudyID
//		tagKey.group = 0x0020, tagKey.element = 0x0010;
//		bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strValue,0,TRUE);
//        if (bFlag && strlen(strValue)>0)
//        {
//            strFilter += " AND ST.StudyID LIKE '";
//            strFilter += strValue;
//            strFilter += "'";
//        }
//		//DCM_StudyDate
//		DataSet::TDcmDate dtTemp;
//		tagKey.group = 0x0008, tagKey.element = 0x0020;
//		bFlag = pDcmDataset->findAndGetDate(pItemRoot, tagKey, dtTemp, lPos, bSearchChild);
//        if (bFlag == true)
//        {
//            QString strTemp;
//            DataSet::TDcmDate dtTemp1;
//            unsigned long lPos1(1);
//            if (pDcmDataset->findAndGetDate(pItemRoot, tagKey, dtTemp1, lPos1, bSearchChild) == true)
//            {
//                strTemp = QString(" AND ST.StudyDate BETWEEN #%1-%2-%3# AND #%4-%5-%6#").
//                    arg(dtTemp.Year, 4, 10, QChar('0')).
//                    arg(dtTemp.Month, 2, 10, QChar('0')).
//                    arg(dtTemp.Day, 2, 10, QChar('0')).
//                    arg(dtTemp1.Year, 4, 10, QChar('0')).
//                    arg(dtTemp1.Month, 2, 10, QChar('0')).
//                    arg(dtTemp1.Day, 2, 10, QChar('0'));
//            }
//            else
//            {
//                strTemp = QString(" AND ST.StudyDate LIKE '%1/%2/%3%'").
//                    arg(dtTemp.Month).
//                    arg(dtTemp.Day).
//                    arg(dtTemp.Year);
//            }
//            strFilter += strTemp;
//        }
////		//DCM_StudyTime
////		DataSet::TDcmTime tmTemp;
////		tagKey.group = 0x0008, tagKey.element = 0x0030;
////		bFlag = pDcmDataset->findAndGetTime(pItemRoot, tagKey, tmTemp, lPos, bSearchChild);
//
//        strFilter.replace("*", "%");
//        strFilter += " ORDER BY PT.PatientID , PT.PatientName";
//        if (queryStudy.exec(strFilter) == true)
//        {
////            DataSet dcmTemp(*pDcmDataset);
//
//            QDate dtSQLDate;
//            int i(0);
//            bool bReplaced = true;
//            while (queryStudy.next() == true)
//            {
//                listDcmDataset.push_back(*pDcmDataset);
//                DataSet& dcmTemp = listDcmDataset[i];
//                const DcmItemHandle pItemTemp = dcmTemp.getRoot();
//
//                //DCM_PatientID
//                int nFieldNo(0);
//                tagKey.group = 0x0010, tagKey.element = 0x0020;
//                nError = dcmTemp.setStringValue(
//                    pItemTemp, tagKey, 
//                    queryStudy.value(nFieldNo).toString().toStdString().c_str(),
//                    bReplaced
//                    );
//                //DCM_PatientsName
//                ++ nFieldNo;
//		        tagKey.group = 0x0010, tagKey.element = 0x0010;
//                nError = dcmTemp.setStringValue(
//                    pItemTemp, tagKey, 
//                    queryStudy.value(nFieldNo).toString().toStdString().c_str(),
//                    bReplaced
//                    );
//                //DCM_PatientirthDate
//                ++ nFieldNo;
//		        tagKey.group = 0x0010, tagKey.element = 0x0030;
//                dtSQLDate = queryStudy.value(nFieldNo).toDate();
//                dtTemp.Year = dtSQLDate.year();
//                dtTemp.Month = dtSQLDate.month();
//                dtTemp.Day = dtSQLDate.day();
//                nError = dcmTemp.setDateValue(
//                    pItemTemp, tagKey, dtTemp, bReplaced
//                    );
////                QTime tmBirthDate = queryStudy.value(nFieldNo).toTime();
//
//                //ST.StudyInstanceUID, 
//                ++ nFieldNo;
//		        tagKey.group = 0x0020, tagKey.element = 0x000d;
//                nError = dcmTemp.setStringValue(
//                    pItemTemp, tagKey, 
//                    queryStudy.value(nFieldNo).toString().toStdString().c_str(),
//                    bReplaced
//                    );
//                //ST.StudyDate
//                ++ nFieldNo;
//                tagKey.group = 0x0008, tagKey.element = 0x0020;
//                dtSQLDate = queryStudy.value(nFieldNo).toDate();
//                dtTemp.Year = dtSQLDate.year();
//                dtTemp.Month = dtSQLDate.month();
//                dtTemp.Day = dtSQLDate.day();
//                nError = dcmTemp.setDateValue(
//                    pItemTemp, tagKey, dtTemp, bReplaced
//                    );
//                //, ST.AccessionNumber
//                ++ nFieldNo;
//		        tagKey.group = 0x0008, tagKey.element = 0x0050;
//                nError = dcmTemp.setStringValue(
//                    pItemTemp, tagKey, 
//                    queryStudy.value(nFieldNo).toString().toStdString().c_str(),
//                    bReplaced
//                    );
//                //, ST.StudyID
//                ++ nFieldNo;
//		        tagKey.group = 0x0020, tagKey.element = 0x0010;
//                nError = dcmTemp.setStringValue(
//                    pItemTemp, tagKey, 
//                    queryStudy.value(nFieldNo).toString().toStdString().c_str(),
//                    bReplaced
//                    );
//
////                //DCM_ReferencedStudySequence
////		        tagKey.group = 0x0008, tagKey.element = 0x1110;
////                nError = pDcmDataset->insertSeqItem(pItemRoot, tagKey, pItem);
//
//                ++ i;
//            }
//            ///. end of while (queryStudy.next() == true)
//
//            queryStudy.finish();
//            nError = i;
//        }
//        else
//        {
//            nError = -1;
//            m_strLastError = queryStudy.lastError().text();
//        }
//        ///. end of if (queryStudy.exec(strFilter) == true)
//	}
//	catch (...)
//	{
//        m_strLastError = m_SqlDatabase.lastError().text();
//		return -1;
//	}
//    return nError;
//}

int QtSqlPersistence::setDcmDataset(const DataSet& dcmDataset)
{
	int nError(0);
    DataSet* pDcmDataset = (DataSet*) &dcmDataset;
    const DcmItemHandle pItemRoot = pDcmDataset->getRoot();

	char strValue[128];
    unsigned long nPos(0);
    bool bChild(false);

	//DCM_SOPClassUID
	DataSet::TDcmTagKey tagKey(0x0008, 0x0016);
    bool bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strValue, nPos, bChild);
	if (!bFlag)
	{	//DCMDIR
		nError = -1;
	}
	else if (strcmp(strValue, "1.2.840.10008.5.1.4.1.1.481.3") == 0)
	{	//RT Structure Set Storage
//		if (OnSetRTStructureSet(dcmDataset) < 0)
//			return -1;
//		if (OnSetRTStructureSetROI(dcmDataset) < 0)
//			return -1;
//		if (OnSetRTStructureSetContour(dcmDataset) < 0)
//			return -1;
//		if (OnSetRTStructureSetPoint(dcmDataset) < 0)
//			return -1;
		nError = -1;
	}
	else if (strcmp(strValue, "1.2.840.10008.5.1.4.1.1.11.1") == 0)
	{	//Grayscale Softcopy Presentation State Storage
//		nError = OnSetPresentationState(dcmDataset);
		nError = -1;
	}
	else if (strcmp(strValue, "1.2.840.10008.5.1.4.1.1.7") == 0)
	{	//Secondary Capture IOD
		if (OnSetPatient(dcmDataset) < 0)
			return -1;
		if (OnSetStudy(dcmDataset) < 0)
			return -1;
		if (OnSetSeries(dcmDataset) < 0)
			return -1;
        if (OnSetPresentationState(dcmDataset) < 0)
            return -1;
		nError = OnSetImage(dcmDataset);
	}
	else
	{	//PIXEL
		if (OnSetPatient(dcmDataset) < 0)
			return -1;
		if (OnSetStudy(dcmDataset) < 0)
			return -1;
		if (OnSetSeries(dcmDataset) < 0)
			return -1;
        if (OnSetPresentationState(dcmDataset) < 0)
            return -1;
		nError = OnSetImage(dcmDataset);
	}
	return nError;
}
//
//int QtSqlPersistence::queryAtStudyLevel()
//{
//    try
//    {
//        QSqlQuery sqlQuery("SELECT * FROM Study");
//        QSqlDriver const * sqlDriver = sqlQuery.driver();
//        
//        //This correctly returns the column indexes
//        QSqlRecord sqlRecord = sqlDriver->record("Study");
//        int nIndexOfStudyInstanceUID = sqlRecord.indexOf("StudyInstanceUID");
//        int nIndexOfStudyDate = sqlRecord.indexOf("StudyDate");
//        int nIndexOfAccessionNumber = sqlRecord.indexOf("AccessionNumber");
//        int nIndexOfStudyID = sqlRecord.indexOf("StudyID");
//        
////        //This is giving me problems
////        if(!query.exec("SELECT * FROM EntityTypeTbl"))
////            qDebug() << query.lastError().text();
//        QString strTemp;
//        while (sqlQuery.next())
//        {
//            strTemp = sqlQuery.value(nIndexOfStudyInstanceUID).toString();
//            strTemp = sqlQuery.value(nIndexOfAccessionNumber).toString();
//        } 
//    }
//    catch (...)
//    {
//        return -1;
//    }
//    return 0;
//}

int QtSqlPersistence::OnSetPatient(const DataSet& dcmDataset)
{
	int nSaved(-1);
    bool bFlag(false), bSearchChild(false);
    unsigned long lPos(0);

	//_RecordsetPtr rsPatient;
	//rsPatient.CreateInstance(__uuidof(Recordset));
    QSqlQuery queryPatient;
    queryPatient.setForwardOnly(true);

	try
	{
        DataSet* pDcmDataset = (DataSet*) &dcmDataset;
        const DcmItemHandle pItemRoot = pDcmDataset->getRoot();

        //DCM_PatientsName
	    char strPatientName[128];
        memset(strPatientName, 0, 128);
		DataSet::TDcmTagKey tagKey(0x0010, 0x0010);
		bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strPatientName, lPos, bSearchChild);
//        strPatientName[63] = NULL;
		//DCM_PatientID
	    char strPatientID[128];
        memset(strPatientID, 0, 128);
		tagKey.group = 0x0010, tagKey.element = 0x0020;
		bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strPatientID, lPos, bSearchChild);
//        strPatientID[127] = NULL;

//		m_pDBAbaris->BeginTrans();
        bFlag = m_SqlDatabase.transaction();

		QString strFilter = QString("select * from Patient where PatientID='%1' and PatientName='%2'").
            arg(strPatientID).
            arg(strPatientName);
        bFlag = queryPatient.exec(strFilter);
        if (bFlag == false)
        {   ///. there is error
            m_strLastError = m_SqlDatabase.lastError().text();
            nSaved = -1;
        }
        else if (queryPatient.next() != true)
		{   ///. save new patient
            queryPatient.finish();
            
            ///.add Patient if there is not
            queryPatient.prepare(
                "INSERT INTO "
                "Patient (PatientName, PatientID, PatientBirthDate, PatientSex, EthnicGroup, PatientComments, RgstTime) "
                "VALUES (?, ?, ?, ?, ?, ?, ?)"
                );
            int nPosInQuery(0);
            queryPatient.bindValue(nPosInQuery, strPatientName);
            ++ nPosInQuery;
            queryPatient.bindValue(nPosInQuery, strPatientID);

			char strTempValue[128];
			//DCM_PatientsBirthDate
			tagKey.group = 0x0010, tagKey.element = 0x0030;
			DataSet::TDcmDate dtTemp;
			bFlag = pDcmDataset->findAndGetDate(pItemRoot, tagKey, dtTemp, lPos, bSearchChild);
			//DCM_PatientsBirthTime
			tagKey.group = 0x0010, tagKey.element = 0x0032;
			DataSet::TDcmTime tmTemp;
			bFlag = pDcmDataset->findAndGetTime(pItemRoot, tagKey, tmTemp, lPos, bSearchChild);
            ++ nPosInQuery;
            queryPatient.bindValue(
                nPosInQuery, 
                QDateTime(QDate(dtTemp.Year,dtTemp.Month,dtTemp.Day), QTime(tmTemp.Hour,tmTemp.Minute,tmTemp.Second))
                );

			//DCM_PatientsSex
			tagKey.group = 0x0010, tagKey.element = 0x0040;
			bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strTempValue, lPos, bSearchChild);
            ++ nPosInQuery;
			if (bFlag && strlen(strTempValue)==1)
				queryPatient.bindValue(nPosInQuery, strTempValue);
			else
				queryPatient.bindValue(nPosInQuery, "O");

			//DCM_EthnicGroup
			tagKey.group = 0x0010, tagKey.element = 0x2160;
			bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strTempValue, lPos, bSearchChild);
            ++ nPosInQuery;
			if (bFlag)
				queryPatient.bindValue(nPosInQuery, strTempValue);
            else
				queryPatient.bindValue(nPosInQuery, "");

			//DCM_PatientComments
			tagKey.group = 0x0010, tagKey.element = 0x4000;
			bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strTempValue, lPos, bSearchChild);
            ++ nPosInQuery;
			if (bFlag)
				queryPatient.bindValue(nPosInQuery, strTempValue);
            else
				queryPatient.bindValue(nPosInQuery, "");

            ///.
            ++ nPosInQuery;
			queryPatient.bindValue(nPosInQuery, QDateTime::currentDateTime());

            //. update the databasers
            ///. Patient->Update();
            bFlag = queryPatient.exec();
			nSaved = 1;
		}
		else
		{   ///. there is a record already
			nSaved = 0;
		}
        ///. end of if (queryPatient.exec(strFilter) == false)

//		rsPatient->Close();
//		rsPatient.Release();
        queryPatient.finish();

//		m_pDBAbaris->CommitTrans();
        bFlag = m_SqlDatabase.commit();
	}
	catch (.../*_com_error& e*/)
	{
//		rsPatient.Release();
//		m_pDBAbaris->RollbackTrans();
        bFlag = m_SqlDatabase.rollback();
//		dump_com_error(e);
        m_strLastError = m_SqlDatabase.lastError().text();
		return -1;
	}
	return nSaved;	
}

int QtSqlPersistence::OnSetStudy(const DataSet& dcmDataset)
{
	int nSaved(-1);
    bool bFlag(false), bSearchChild(false);
    unsigned long lPos(0);

    QSqlQuery queryStudy;
    queryStudy.setForwardOnly(true);

	try
	{
        DataSet* pDcmDataset = (DataSet*) &dcmDataset;
        const DcmItemHandle pItemRoot = pDcmDataset->getRoot();

        //DCM_StudyInstanceUID
		char strStudyInstanceUID[64];
		DataSet::TDcmTagKey tagKey(0x0020, 0x000d);
		bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strStudyInstanceUID, lPos, bSearchChild);
		//DCM_PatientID, 64
		char strPatientID[128];
		tagKey.group = 0x0010, tagKey.element = 0x0020;
		bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strPatientID, lPos, bSearchChild);

        bFlag = m_SqlDatabase.transaction();

		QString strFilter = QString("select * from Study where PatientID='%1' and StudyInstanceUID='%2'").
            arg(strPatientID).
            arg(strStudyInstanceUID);
        bFlag = queryStudy.exec(strFilter);
        if (bFlag == false)
        {   ///. there is error
            m_strLastError = queryStudy.lastError().text();
            nSaved = -1;
        }
        else if (queryStudy.next() != true)
		{   ///. save new study
            queryStudy.finish();
            
            ///.add Patient if there is not
            queryStudy.prepare(
                "INSERT INTO "
                "Study (StudyInstanceUID, StudyDate, AccessionNumber, StudyID, StudyDescription, PatientName, PatientID, Age, Height, Weight, ReferPhysician, Telephone, Address) "
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                );
            
			// study instance uid
            int nPosInQuery(0);
            queryStudy.bindValue(nPosInQuery, strStudyInstanceUID);

			//DCM_StudyDate
			DataSet::TDcmDate dtTemp;
			tagKey.group = 0x0008, tagKey.element = 0x0020;
			bFlag = pDcmDataset->findAndGetDate(pItemRoot, tagKey, dtTemp, lPos, bSearchChild);
			//DCM_StudyTime
			DataSet::TDcmTime tmTemp;
			tagKey.group = 0x0008, tagKey.element = 0x0030;
			bFlag = pDcmDataset->findAndGetTime(pItemRoot, tagKey, tmTemp, lPos, bSearchChild);
            ++ nPosInQuery;
            queryStudy.bindValue(
                nPosInQuery, 
                QDateTime(QDate(dtTemp.Year,dtTemp.Month,dtTemp.Day), QTime(tmTemp.Hour,tmTemp.Minute,tmTemp.Second))
                );

			char strTempValue[128];
			//DCM_AccessionNumber
			tagKey.group = 0x0008, tagKey.element = 0x0050;
			bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strTempValue, lPos, bSearchChild);
            ++ nPosInQuery;
            if (bFlag)
                queryStudy.bindValue( nPosInQuery, strTempValue );
            else
                queryStudy.bindValue( nPosInQuery, "" );

			//DCM_StudyID
			tagKey.group = 0x0020, tagKey.element = 0x0010;
			bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strTempValue,0,TRUE);
            ++ nPosInQuery;
            if (bFlag)
                queryStudy.bindValue( nPosInQuery, strTempValue );
            else
                queryStudy.bindValue( nPosInQuery, "" );

			//DCM_StudyDescription
			tagKey.group = 0x0008, tagKey.element = 0x1030;
			bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strTempValue,0,TRUE);
            ++ nPosInQuery;
            if (bFlag)
                queryStudy.bindValue( nPosInQuery, strTempValue );
            else
                queryStudy.bindValue( nPosInQuery, "" );

			//DCM_PatientsName
			tagKey.group = 0x0010, tagKey.element = 0x0010;
			bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strTempValue, lPos, bSearchChild);
            ++ nPosInQuery;
            if (bFlag)
                queryStudy.bindValue( nPosInQuery, strTempValue );
            else
                queryStudy.bindValue( nPosInQuery, "Anonymized" );

			// m_PatientID
            ++ nPosInQuery;
            queryStudy.bindValue( nPosInQuery, strPatientID );

			//DCM_PatientsAge
			tagKey.group = 0x0010, tagKey.element = 0x1010;
			bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strTempValue,0,TRUE);
            ++ nPosInQuery;
            if (bFlag)
                queryStudy.bindValue( nPosInQuery, strTempValue );
            else
                queryStudy.bindValue( nPosInQuery, "" );

			//DCM_PatientsSize
			tagKey.group = 0x0010, tagKey.element = 0x1020;
            ++ nPosInQuery;
            queryStudy.bindValue( nPosInQuery, pDcmDataset->findAndGetDouble(pItemRoot, tagKey, lPos, bSearchChild) );

			//DCM_PatientsWeight
			tagKey.group = 0x0010, tagKey.element = 0x1030;
            ++ nPosInQuery;
            queryStudy.bindValue( nPosInQuery, pDcmDataset->findAndGetDouble(pItemRoot, tagKey, lPos, bSearchChild) );

			//DCM_ReferringPhysiciansName
			tagKey.group = 0x0008, tagKey.element = 0x0090;
			bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strTempValue,0,TRUE);
            ++ nPosInQuery;
			if (bFlag)
                queryStudy.bindValue( nPosInQuery, strTempValue );
            else
                queryStudy.bindValue( nPosInQuery, "" );

			//DCM_PatientsTelephoneNumbers
			tagKey.group = 0x0010, tagKey.element = 0x2154;
			bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strTempValue, lPos, bSearchChild);
            ++ nPosInQuery;
			if (bFlag)
                queryStudy.bindValue( nPosInQuery, strTempValue );
            else
                queryStudy.bindValue( nPosInQuery, "" );

			//DCM_PatientsAddress
			tagKey.group = 0x0010, tagKey.element = 0x1040;
			bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strTempValue, lPos, bSearchChild);
            ++ nPosInQuery;
			if (bFlag)
                queryStudy.bindValue( nPosInQuery, strTempValue );
            else
                queryStudy.bindValue( nPosInQuery, "" );

			//update study table
            bFlag = queryStudy.exec();
            if (bFlag)
            {
    			nSaved = 1;
            }
            else
            {
                m_strLastError = queryStudy.lastError().text();
    			nSaved = -1;
            }
		}
		else
		{   ///. there is a record already
			nSaved = 0;
		}
        ///. end of if (queryStudy.exec(strFilter) == false)

        queryStudy.finish();
        bFlag = m_SqlDatabase.commit();
	}
	catch (...)
	{
        m_strLastError = m_SqlDatabase.lastError().text();
        bFlag = m_SqlDatabase.rollback();
		return -1;
	}
	return nSaved;
}

int QtSqlPersistence::OnSetSeries(const DataSet& dcmDataset)
{
	int nSaved(-1);
    bool bFlag(false), bSearchChild(false);
    unsigned long lPos(0);

    QSqlQuery querySeries;
    querySeries.setForwardOnly(true);

	try
	{
        DataSet* pDcmDataset = (DataSet*) &dcmDataset;
        const DcmItemHandle pItemRoot = pDcmDataset->getRoot();

        //DCM_StudyInstanceUID
		char strStudyInstanceUID[64];
		DataSet::TDcmTagKey tagKey(0x0020, 0x000d);
		bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strStudyInstanceUID, lPos, bSearchChild);
		//DCM_SeriesInstanceUID
		char strSeriesInstanceUID[64];
		tagKey.group = 0x0020, tagKey.element = 0x000e;
		bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strSeriesInstanceUID, lPos, bSearchChild);

        bFlag = m_SqlDatabase.transaction();

		QString strFilter = QString("SELECT * FROM Series WHERE StudyInstanceUID='%1' AND SeriesInstanceUID='%2'").
            arg(strStudyInstanceUID).
            arg(strSeriesInstanceUID);
        bFlag = querySeries.exec(strFilter);
        if (bFlag == false)
        {   ///. there is error
            m_strLastError = querySeries.lastError().text();
            nSaved = -1;
        }
        else if (querySeries.next() != true)
		{   ///. save new series
            querySeries.finish();
            
            ///.add Patient if there is not
            querySeries.prepare(
                "INSERT INTO "
                "Series (SeriesInstanceUID, Modality, SeriesNumber, SeriesDate"
                ", PerformPhysician, BodyPartExamed, ImageCount"
                ", PatientID, StudyInstanceUID, RgstTime) "
                "VALUES (?, ?, ?, ?"
                ", ?, ?, ?"
                ", ?, ?, ?)"
                );

			//DCM_SeriesInstanceUID
            int nPosInQuery(0);
            querySeries.bindValue(nPosInQuery, strSeriesInstanceUID);

			char strTempValue[128];
            //DCM_Modality
			tagKey.group = 0x0008, tagKey.element = 0x0060;
			bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strTempValue, lPos, bSearchChild);
            ++ nPosInQuery;
            if (bFlag)
                querySeries.bindValue( nPosInQuery, strTempValue );
            else
                querySeries.bindValue( nPosInQuery, "UNKNOWN" );

			//DCM_SeriesNumber
			tagKey.group = 0x0020, tagKey.element = 0x0011;
            ++ nPosInQuery;
            querySeries.bindValue( nPosInQuery, pDcmDataset->findAndGetLong(pItemRoot, tagKey, lPos, bSearchChild) );

			//DCM_SeriesDate
			DataSet::TDcmDate dtTemp;
			tagKey.group = 0x0008, tagKey.element = 0x0021;
			bFlag = pDcmDataset->findAndGetDate(pItemRoot, tagKey, dtTemp, lPos, bSearchChild);
            ++ nPosInQuery;
            if (bFlag)
            {
			    //DCM_SeriesTime
			    DataSet::TDcmTime tmTemp;
			    tagKey.group = 0x0008, tagKey.element = 0x0031;
			    bFlag = pDcmDataset->findAndGetTime(pItemRoot, tagKey, tmTemp, lPos, bSearchChild);
                querySeries.bindValue(
                    nPosInQuery, 
                    QDateTime(QDate(dtTemp.Year,dtTemp.Month,dtTemp.Day), QTime(tmTemp.Hour,tmTemp.Minute,tmTemp.Second))
                    );
            }
            else
            {
                querySeries.bindValue( nPosInQuery, QDateTime::currentDateTime() );
            }

			//DCM_PerformingPhysiciansName
			tagKey.group = 0x0008, tagKey.element = 0x1050;
			bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strTempValue, lPos, bSearchChild);
            ++ nPosInQuery;
            if (bFlag)
                querySeries.bindValue( nPosInQuery, strTempValue );
            else
                querySeries.bindValue( nPosInQuery, "" );

			//DCM_BodyPartExamined
			tagKey.group = 0x0018, tagKey.element = 0x0015;
			bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strTempValue, lPos, bSearchChild);
            ++ nPosInQuery;
            if (bFlag)
                querySeries.bindValue( nPosInQuery, strTempValue );
            else
                querySeries.bindValue( nPosInQuery, "" );

			//DCM_NumberOfSeriesRelatedInstances
			tagKey.group = 0x0020, tagKey.element = 0x1209;
            ++ nPosInQuery;
            querySeries.bindValue( nPosInQuery, pDcmDataset->findAndGetLong(pItemRoot, tagKey, lPos, bSearchChild) );

            //DCM_PatientID
			tagKey.group = 0x0010, tagKey.element = 0x0020;
			bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strTempValue, lPos, bSearchChild);
            ++ nPosInQuery;
            if (bFlag)
                querySeries.bindValue( nPosInQuery, strTempValue );
            else
                querySeries.bindValue( nPosInQuery, "" );

			//DCM_StudyInstanceUID
            ++ nPosInQuery;
            querySeries.bindValue(nPosInQuery, strStudyInstanceUID);
			
 			//the data and time of updating for this series
            ++ nPosInQuery;
            querySeries.bindValue( nPosInQuery, QDateTime::currentDateTime() );

           //update series table
            bFlag = querySeries.exec();
            if (bFlag)
            {
    			nSaved = 1;
            }
            else
            {
                m_strLastError = querySeries.lastError().text();
    			nSaved = -1;
            }
		}
		else
		{   ///. there is a record already
			nSaved = 0;
		}
        ///. end of if (querySeries.exec(strFilter) == false)

        querySeries.finish();
        bFlag = m_SqlDatabase.commit();
	}
	catch (...)
	{
        bFlag = m_SqlDatabase.rollback();
        m_strLastError = m_SqlDatabase.lastError().text();
		return -1;
	}
	return nSaved;
}

int QtSqlPersistence::OnSetPresentationState(const DataSet& dcmDataset)
{
	int nSaved(-1);
    bool bFlag(false), bSearchSub(false);
    unsigned long lPos(0);

    QSqlQuery queryPresentationState;
    queryPresentationState.setForwardOnly(true);

	try
	{
        DataSet* pDcmDataset = (DataSet*) &dcmDataset;
        const DcmItemHandle pItemRoot = pDcmDataset->getRoot();

        //DCM_StudyInstanceUID
		char strStudyInstanceUID[64];
		DataSet::TDcmTagKey tagKey(0x0020, 0x000d);
		bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strStudyInstanceUID, lPos, bSearchSub);
		//DCM_SeriesInstanceUID
		char strSeriesInstanceUID[64];
		tagKey.group = 0x0020, tagKey.element = 0x000e;
		bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strSeriesInstanceUID, lPos, bSearchSub);

        bFlag = m_SqlDatabase.transaction();

		QString strFilter = QString("SELECT * FROM PresentationState WHERE SeriesInstanceUID='%1'").arg(strSeriesInstanceUID);
        bFlag = queryPresentationState.exec(strFilter);
        if (bFlag == false)
        {   ///. there is error
            m_strLastError = queryPresentationState.lastError().text();
            nSaved = -1;
        }
        else if (queryPresentationState.next() != true)   ///. save new series
		{
            queryPresentationState.finish();
            
            ///.add PresentationState if there is not
            queryPresentationState.prepare(
                "INSERT INTO"
                " PresentationState (SeriesInstanceUID, ImagedVolumeWidth, ImagedVolumeHeight, ImagedVolumeDepth, TotalPixelMatrixColumns, TotalPixelMatrixRows, NumberOfLevels)"
                " VALUES (?, ?, ?, ?, ?, ?, ?)"
                );

			////DCM_SeriesInstanceUID
            int nPosInQuery(0);
            queryPresentationState.bindValue(nPosInQuery, strSeriesInstanceUID);
            ///. ImagedVolumeWidth, in mm
            tagKey.group = 0x0048, tagKey.element = 0x0001;
             ++ nPosInQuery;
            queryPresentationState.bindValue(
                nPosInQuery, pDcmDataset->findAndGetDouble(pItemRoot, tagKey, lPos, bSearchSub) 
                );
            ///. ImagedVolumeHeight, in mm
            tagKey.group = 0x0048, tagKey.element = 0x0002;
             ++ nPosInQuery;
            queryPresentationState.bindValue(
                nPosInQuery, pDcmDataset->findAndGetDouble(pItemRoot, tagKey, lPos, bSearchSub) 
                );
            ///. ImagedVolumeDepth, in μm
            tagKey.group = 0x0048, tagKey.element = 0x0003;
            ++ nPosInQuery;
            queryPresentationState.bindValue(
                nPosInQuery, pDcmDataset->findAndGetDouble(pItemRoot, tagKey, lPos, bSearchSub) 
                );
            ///. TotalPixelMatrixColumns
            tagKey.group = 0x0048, tagKey.element = 0x0006;
            ++ nPosInQuery;
            queryPresentationState.bindValue(
                nPosInQuery, pDcmDataset->findAndGetLong(pItemRoot, tagKey, lPos, bSearchSub) 
                );
            ///. TotalPixelMatrixRows
            tagKey.group = 0x0048, tagKey.element = 0x0007;
            ++ nPosInQuery;
            queryPresentationState.bindValue(
                nPosInQuery, pDcmDataset->findAndGetLong(pItemRoot, tagKey, lPos, bSearchSub) 
                );
            ///. Instance Number
            tagKey.group = 0x0020, tagKey.element = 0x0013;
            ++ nPosInQuery;
            queryPresentationState.bindValue(
                nPosInQuery, pDcmDataset->findAndGetLong(pItemRoot, tagKey, lPos, bSearchSub) 
                );

           //update PresentationState table
            bFlag = queryPresentationState.exec();
            if (bFlag)
            {
    			nSaved = 1;
            }
            else
            {
                m_strLastError = queryPresentationState.lastError().text();
    			nSaved = -1;
            }
		}
		else   ///. there is a record already
		{
            ///. TotalPixelMatrixColumns
            tagKey.group = 0x0048, tagKey.element = 0x0006;
            long lTotalPixelMatrixColumns = pDcmDataset->findAndGetLong(pItemRoot, tagKey, lPos, bSearchSub);
            ///. TotalPixelMatrixRows
            tagKey.group = 0x0048, tagKey.element = 0x0007;
            long lTotalPixelMatrixRows = pDcmDataset->findAndGetLong(pItemRoot, tagKey, lPos, bSearchSub);
            ///. Instance Number
            tagKey.group = 0x0020, tagKey.element = 0x0013;
            long lNumberOfLevels = pDcmDataset->findAndGetLong(pItemRoot, tagKey, lPos, bSearchSub);

            bool bUpdate(false);
            int nFieldNo(4);
            if (lTotalPixelMatrixColumns < queryPresentationState.value(nFieldNo).toLongLong())
                lTotalPixelMatrixColumns = queryPresentationState.value(nFieldNo).toLongLong();
            ++ nFieldNo;
            if (lTotalPixelMatrixRows < queryPresentationState.value(nFieldNo).toLongLong())
                lTotalPixelMatrixRows = queryPresentationState.value(nFieldNo).toLongLong();
            ++ nFieldNo;
            if (lNumberOfLevels < queryPresentationState.value(nFieldNo).toLongLong())
                lNumberOfLevels = queryPresentationState.value(nFieldNo).toLongLong();
            queryPresentationState.finish();

            strFilter= QString(
                "UPDATE PresentationState"
                " SET TotalPixelMatrixColumns=%1, TotalPixelMatrixRows=%2, NumberOfLevels=%3"
                " WHERE SeriesInstanceUID='%4'"
                ).
                arg(lTotalPixelMatrixColumns).arg(lTotalPixelMatrixRows).arg(lNumberOfLevels).
                arg(strSeriesInstanceUID);
            queryPresentationState.prepare(strFilter);
           //update series table
            if (queryPresentationState.exec())
            {
    			nSaved = 0;
            }
            else
            {
                m_strLastError = queryPresentationState.lastError().text();
    			nSaved = -1;
            }
		}
        ///. end of if (querySeries.exec(strFilter) == false)

        queryPresentationState.finish();
        bFlag = m_SqlDatabase.commit();
	}
	catch (...)
	{
        bFlag = m_SqlDatabase.rollback();
        m_strLastError = m_SqlDatabase.lastError().text();
		return -1;
	}
	return nSaved;
}

int QtSqlPersistence::OnSetImage(const DataSet& dcmDataset)
{
	int nSaved(-1);
    bool bFlag(false), bSearchChild(false);
    unsigned long lPos(0);

    QSqlQuery queryImage;
    queryImage.setForwardOnly(true);

	try
	{
        DataSet* pDcmDataset = (DataSet*) &dcmDataset;
        const DcmItemHandle pItemRoot = pDcmDataset->getRoot();

		//DCM_StudyInstanceUID
		char strStudyInstanceUID[64];
		DataSet::TDcmTagKey tagKey(0x0020, 0x000d);
		bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strStudyInstanceUID, lPos, bSearchChild);
		//DCM_SeriesInstanceUID
		char strSeriesInstanceUID[64];
		tagKey.group = 0x0020, tagKey.element = 0x000e;
		bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strSeriesInstanceUID, lPos, bSearchChild);
		//DCM_SOPInstanceUID
		char strSOPInstanceUIDTemp[64];
		tagKey.group = 0x0008, tagKey.element = 0x0018;
		bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strSOPInstanceUIDTemp, lPos, bSearchChild);
        ///.ConcatenationUID
        bool bConcatenation(false);
        char strConcatenationUID[64];
        memset(strConcatenationUID, NULL, 64);
        tagKey.group = 0x0020, tagKey.element = 0x9161;
        if (pDcmDataset->findAndGetString(pItemRoot, tagKey, strConcatenationUID, lPos, bSearchChild) &&
            strlen(strConcatenationUID) > 0)
        {
            bConcatenation = true;
        }
        ///.SOPInstanceUIDOfConcatenationSource
        char strSOPInstanceUIDOfConcatenationSource[64];
        memset(strSOPInstanceUIDOfConcatenationSource, NULL, 64);
        tagKey.group = 0x0020, tagKey.element = 0x0242;
        if (pDcmDataset->findAndGetString(pItemRoot, tagKey, strSOPInstanceUIDOfConcatenationSource, lPos, bSearchChild) == false ||
            strlen(strSOPInstanceUIDOfConcatenationSource) < 1)
        {
            memcpy( strSOPInstanceUIDOfConcatenationSource, strConcatenationUID, 64);
        }
        else
        {
            bConcatenation = true;
        }

        //////////////////////////////////////////////////////////////
        bFlag = m_SqlDatabase.transaction();

        QString strSOPInstanceUID;
        if (strlen(strSOPInstanceUIDOfConcatenationSource)>0 && bConcatenation)
        {
		    strSOPInstanceUID = strSOPInstanceUIDOfConcatenationSource;
        }
        else
        {
		    strSOPInstanceUID = strSOPInstanceUIDTemp;
        }
		QString strFilter = QString("SELECT * FROM Image WHERE StudyInstanceUID='%1' AND SeriesInstanceUID='%2' AND SOPInstanceUID='%3'").
            arg(strStudyInstanceUID).arg(strSeriesInstanceUID).arg(strSOPInstanceUID);
        bFlag = queryImage.exec(strFilter);
        if (bFlag == false)
        {   ///. there is error
            m_strLastError = queryImage.lastError().text();
            nSaved = -1;
        }
        else if (queryImage.next() != true)
		{   ///. save new series
            queryImage.finish();
            
            ///.add Patient if there is not
            queryImage.prepare(
                "INSERT INTO"
                " Image (SOPInstanceUID, InstanceNumber, SOPClassUID, TransferSyntaxUID, ReferencedFile"
                ", PatientID, StudyInstanceUID, SeriesInstanceUID"
                ", PixelDataProviderURL, ImagedVolumeWidth, ImagedVolumeHeight, ImagedVolumeDepth"
                ", TotalPixelMatrixColumns, TotalPixelMatrixRows, Rows, Columns, LossyImageCompression"
                ", PhotometricInterpretation, SamplesPerPixel, BitsAllocated)"
                " VALUES (?, ?, ?, ?, ?"
                ", ?, ?, ?"
                ", ?, ?, ?, ?"
                ", ?, ?, ?, ?, ?"
                ", ?, ?, ?)"
                );

			//DCM_SOPInstanceUID
            int nPosInQuery(0);
            queryImage.bindValue(nPosInQuery, strSOPInstanceUID);

			//DCM_InstanceNumber
			tagKey.group = 0x0020, tagKey.element = 0x0013;
            ++ nPosInQuery;
            queryImage.bindValue( nPosInQuery, pDcmDataset->findAndGetLong(pItemRoot, tagKey, lPos, bSearchChild) );

			char strTempValue[128];
			//DCM_SOPClassUID
			tagKey.group = 0x0008, tagKey.element = 0x0016;
			bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strTempValue, lPos, bSearchChild);
            ++ nPosInQuery;
            if (bFlag)
                queryImage.bindValue( nPosInQuery, strTempValue );
            else
                queryImage.bindValue( nPosInQuery, "UNKNOWN" );

            //TransferSyntaxUID
            ++ nPosInQuery;
            if (pDcmDataset->findAndGetTransferSyntaxUID(strTempValue, 128) < 0)
                queryImage.bindValue( nPosInQuery, "UNKNOWN" );
            else
                queryImage.bindValue( nPosInQuery, strTempValue );

			//Referenced File Name
            ++ nPosInQuery;
            queryImage.bindValue( nPosInQuery, strSOPInstanceUID );

			//DCM_PatientID
			tagKey.group = 0x0010, tagKey.element = 0x0020;
			bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strTempValue, lPos, bSearchChild);
            ++ nPosInQuery;
            if (bFlag)
                queryImage.bindValue( nPosInQuery, strTempValue );
            else
                queryImage.bindValue( nPosInQuery, "Anonymised" );

			//DCM_StudyInstanceUID
            ++ nPosInQuery;
            queryImage.bindValue( nPosInQuery, strStudyInstanceUID );

			//DCM_SeriesInstanceUID
            ++ nPosInQuery;
            queryImage.bindValue( nPosInQuery, strSeriesInstanceUID );

			//DCM_PixelDataProviderURL
			tagKey.group = 0x0028, tagKey.element = 0x7FE0;
			bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strTempValue, lPos, bSearchChild);
            ++ nPosInQuery;
            if (bFlag)
                queryImage.bindValue( nPosInQuery, strTempValue );
            else
                queryImage.bindValue( nPosInQuery, "" );

            ///.
            //DCM_ImagedVolumeWidth
            tagKey.group = 0x0048, tagKey.element = 0x0001;
            double fImagedVolumeWidth = static_cast<double>(pDcmDataset->findAndGetFloat(pItemRoot, tagKey, lPos, bSearchChild));
            if (fImagedVolumeWidth > 1.0)
            {
                ++ nPosInQuery;
                queryImage.bindValue( nPosInQuery, fImagedVolumeWidth );
                //DCM_ImagedVolumeHeight
                tagKey.group = 0x0048, tagKey.element = 0x0002;
                ++ nPosInQuery;
                queryImage.bindValue( 
                    nPosInQuery, static_cast<double>(pDcmDataset->findAndGetFloat(pItemRoot, tagKey, lPos, bSearchChild))
                    );
                //DCM_ImagedVolumeDepth
                tagKey.group = 0x0048, tagKey.element = 0x0003;
                ++ nPosInQuery;
                queryImage.bindValue( 
                    nPosInQuery, static_cast<double>(pDcmDataset->findAndGetFloat(pItemRoot, tagKey, lPos, bSearchChild))
                    );
                //DCM_TotalPixelMatrixColumns
                tagKey.group = 0x0048, tagKey.element = 0x0006;
                ++ nPosInQuery;
                long lTemp = pDcmDataset->findAndGetLong(pItemRoot, tagKey, lPos, bSearchChild);
                queryImage.bindValue( 
                    nPosInQuery, lTemp
                    );
                //DCM_TotalPixelMatrixRows
                tagKey.group = 0x0048, tagKey.element = 0x0007;
                ++ nPosInQuery;
                queryImage.bindValue( 
                    nPosInQuery, pDcmDataset->findAndGetLong(pItemRoot, tagKey, lPos, bSearchChild)
                    );
                 //DCM_Rows
                tagKey.group = 0x0028, tagKey.element = 0x0010;
                ++ nPosInQuery;
                queryImage.bindValue( 
                    nPosInQuery, pDcmDataset->findAndGetShort(pItemRoot, tagKey, lPos, bSearchChild)
                    );
                //DCM_Columns
                tagKey.group = 0x0028, tagKey.element = 0x0011;
                ++ nPosInQuery;
                queryImage.bindValue( 
                    nPosInQuery, pDcmDataset->findAndGetShort(pItemRoot, tagKey, lPos, bSearchChild)
                    );
                //LossyImageCompression
                ++ nPosInQuery;
                queryImage.bindValue( nPosInQuery, "" );
                //
                //TotalPixelMatrixOriginSequence
            }
            else
            {
                //DCM_Rows
                tagKey.group = 0x0028, tagKey.element = 0x0010;
                long nRows = pDcmDataset->findAndGetShort(pItemRoot, tagKey, lPos, bSearchChild);
                //DCM_Columns
                tagKey.group = 0x0028, tagKey.element = 0x0011;
                long nCols = pDcmDataset->findAndGetShort(pItemRoot, tagKey, lPos, bSearchChild);
                //DCM_PixelSpacing
                tagKey.group = 0x0028, tagKey.element = 0x0030;
                double fRatioX = pDcmDataset->findAndGetDouble(pItemRoot, tagKey, lPos, bSearchChild);
                double fRatioY = pDcmDataset->findAndGetDouble(pItemRoot, tagKey, lPos+1, bSearchChild);

                //ImagedVolumeWidth
                ++ nPosInQuery;
                queryImage.bindValue( nPosInQuery, fRatioX*nCols );
                //ImagedVolumeHeight
                ++ nPosInQuery;
                queryImage.bindValue( nPosInQuery, fRatioX*nRows );
                //DCM_SliceThickness
                tagKey.group = 0x0018, tagKey.element = 0x0050;
                ++ nPosInQuery;
                queryImage.bindValue( 
                    nPosInQuery, pDcmDataset->findAndGetDouble(pItemRoot, tagKey, lPos, bSearchChild)
                    );
                //TotalPixelMatrixColumns
                ++ nPosInQuery;
                queryImage.bindValue( nPosInQuery, nCols );
                //TotalPixelMatrixRows
                ++ nPosInQuery;
                queryImage.bindValue( nPosInQuery, nRows );
                 //DCM_Rows
                ++ nPosInQuery;
                queryImage.bindValue( nPosInQuery, nRows );
                //DCM_Columns
                ++ nPosInQuery;
                queryImage.bindValue( nPosInQuery, nCols );
                //LossyImageCompression
                ++ nPosInQuery;
                queryImage.bindValue( nPosInQuery, "" );
            }
            ///. end of if (fImagedVolumeWidth > 1.0)

            ///. PhotometricInterpretation
            tagKey.group = 0x0028, tagKey.element = 0x0004;
			bFlag = pDcmDataset->findAndGetString(pItemRoot, tagKey, strTempValue, lPos, bSearchChild);
            ++ nPosInQuery;
            if (bFlag)
                queryImage.bindValue( nPosInQuery, strTempValue );
            else
                queryImage.bindValue( nPosInQuery, "" );
            //SamplesPerPixel
            tagKey.group = 0x0028, tagKey.element = 0x0002;
            ++ nPosInQuery;
            queryImage.bindValue( 
                nPosInQuery, pDcmDataset->findAndGetShort(pItemRoot, tagKey, lPos, bSearchChild)
                );
            ///. BitsAllocated
            tagKey.group = 0x0028, tagKey.element = 0x0100;
            ++ nPosInQuery;
            queryImage.bindValue( 
                nPosInQuery, pDcmDataset->findAndGetShort(pItemRoot, tagKey, lPos, bSearchChild)
                );

            //update study table
            bFlag = queryImage.exec();
            if (bFlag)
            {
    			nSaved = 1;
            }
            else
            {
                m_strLastError = queryImage.lastError().text();
    			nSaved = -1;
            }
		}
		else
		{   ///. there is a record already
			nSaved = 0;
		}
        ///. end of if (queryImage.exec(strFilter) == false)

        queryImage.finish();
        bFlag = m_SqlDatabase.commit();
	}
	catch (...)
	{
        bFlag = m_SqlDatabase.rollback();
        m_strLastError = m_SqlDatabase.lastError().text();
		return -1;
	}
	return nSaved;
}

int QtSqlPersistence::createMultiFrameDcmFile(const char* pstrFileName, DcmItemHandle pItemDS)const
{
    int nError(-1);
//    bool bFlag(false), bSearchSub(false), bVerbose(false), bReplace(true);
//    unsigned long lPos(0), lNum(1);
//    penc_t* pJasperDec = NULL;
//	unsigned char* pBuffer = NULL;
////	unsigned int	nLenOfBuffer = 0;
////    int nBufWidth=0, nBufHeight=0, nBufBPP=1;
//
//	try
//	{
//        DataSet dcmDataset;
//        nError = -1;
//        nError = dcmDataset.load(pItemDS, nError);
//        DcmItemHandle pItemRoot = dcmDataset.getRoot();
//        if (bVerbose)
//        {
//            dcmDataset.save("..\\..\\temp\\SqlImageLocalizerReq.dcm", 0);
//        }
//        char strValue[128];
//
//        //DCM_StudyInstanceUID
//        QString strStudyInstanceUID;
//	    DataSet::TDcmTagKey tagKey(0x0020, 0x000d);
//        bFlag = dcmDataset.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchSub);
//        strStudyInstanceUID = strValue;
//        //DCM_SeriesInstanceUID
//        QString strSeriesInstanceUID;
//        tagKey.group = 0x0020, tagKey.element = 0x000e;
//        bFlag = dcmDataset.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchSub);
//        strSeriesInstanceUID = strValue;
//        //DCM_SOPInstanceUID
//        QString strSOPInstanceUID;
//        tagKey.group = 0x0008, tagKey.element = 0x0018;
//        bFlag = dcmDataset.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchSub);
//        strSOPInstanceUID = strValue;
//        ///. DCM transfer syntax
//        int nTransferSyntax = dcmDataset.findAndGetTransferSyntaxUID(strValue, 128);
//        if (nTransferSyntax < 0)
//        {
//            nTransferSyntax = 0;
//        }
//
//        QString strFileNameJP2;
//        bool bUrlWSI;
//        ///. PixelData
//        tagKey.group = 0x7fe0, tagKey.element = 0x0010;
//        long nLenOfPixelData = dcmDataset.getFirstElementFromItem(pItemRoot, tagKey, bSearchSub, lPos, NULL, 0);
//        ///. "Pixel Data Provider URL"
//        tagKey.group = 0x0028, tagKey.element = 0x7FE0;
//        if (dcmDataset.findAndGetString(pItemRoot, tagKey, strValue, 0, FALSE) == true &&
//            strlen(strValue) > 0 &&
//            nLenOfPixelData <= 0)
//        {   ///. can not find the URL
//            strFileNameJP2 = strValue;
//            bUrlWSI = true;
//        }
//        else
//        {
//            strFileNameJP2 = "";
//            bUrlWSI = false;
//        }
//
//        if (bUrlWSI == false)
//        {
////            nError = dcmDataset.save(pstrFileName, nTransferSyntax);
////            return nError;
//        }
//		else if (bUrlWSI/* && nTransferSyntax==JPEG2000*/ && rsa_dec_canread_file( strFileNameJP2.toStdString().c_str() ) > 0)
//        {
//            /* Create decoding object */
//            pJasperDec = rsa_dec_open_file( strFileNameJP2.toStdString().c_str() );
//            unsigned int nWidthOfTile = rsa_dec_tilewidth(pJasperDec);
//            unsigned int nHeightOfTile = rsa_dec_tilewidth(pJasperDec);
//            unsigned long nWidthOfTotalPixel = rsa_dec_imagewidth(pJasperDec);
//            unsigned long nHeightOfTotalPixel = rsa_dec_imageheight(pJasperDec);
//            ///.
//            unsigned int nIndexOfLevel = rsa_dec_numrlvls(pJasperDec) - 1;
//            for (; nIndexOfLevel>=0; --nIndexOfLevel)
//            {
//                unsigned int nNumOfTiles = rsa_dec_numtiles(pJasperDec);
//                unsigned int nNumOfTilesH = rsa_dec_numhtiles(pJasperDec);
//                unsigned int nNumOfTilesV = rsa_dec_numvtiles(pJasperDec);
//
//                rsa_codes_t n = rsa_dec_decodeimage(pJasperDec, nIndexOfLevel);
//
//                unsigned int x1 = JPC_CEILDIV(nWidthOfTotalPixel, 1 << nIndexOfLevel) - 1;
//                unsigned int y1 = JPC_CEILDIV(nHeightOfTotalPixel, 1 << nIndexOfLevel) - 1;
//                /* Decode region  */
////                unsigned char *pDataBuf = NULL;
//                int nBufWidth(0), nBufHeight(0), nBufBPP(0);
//                //if (rsa_dec_decoderegion(pJasperDec, nResLevel, 0, 0, x1, y1) == RSA_DEC_SUCCESS &&
//                //    (pBuffer=rsa_dec_getimageRGB(pJasperDec, &nBufWidth, &nBufHeight, &nBufBPP)) != NULL)
//                //{
//                //}
//            }
//        }
//        //else if (bUrlWSI && 
//        //    (pJasEnc=rsa_enc_create(strValue, strFileNameJP2.toStdString().c_str())) != NULL)
//        //{   /* Create encoding object */
//	       // /* Optional - Modify default encoding options */
//        //    int nResLevel(6);
//	       // rsa_enc_settilesize(pJasEnc, 256, 256);	//default tile size	: 256 x 256
//	       // rsa_enc_setlazy(pJasEnc, true);			//default lazy mode	: true
//	       // rsa_enc_setimage(pJasEnc, 0);			//default image		: 1st image in file
//	       // rsa_enc_setlossy(pJasEnc, false, 1.0);	//default lossy mode: false i.e rate=1.0
//	       // rsa_enc_setlevels(pJasEnc, nResLevel);	//default levels	: 6
//
//	       // /* Encode image */
//	       // if( rsa_enc_encode(pJasEnc) != RSA_ENC_SUCCESS )
//        //    {
//        //        m_ui->m_textBrowserLogger->append(
//        //            "Failed to transform: "
//        //            );
//        //        m_ui->m_textBrowserLogger->append(
//        //            strValue
//        //            );
//        //        bUrlWSI = false;
//        //    }
//	
//	       // /* Close and clean up */
//	       // rsa_enc_destroy(pJasEnc);
//        //}
//        else
//        {
////            return -1;
//        }
//        ///. end of if (bFlag && rsa_dec_canread_file( strValue ) > 0)
//
//        ///. save dcm file
//        ///. 30, JPEG2000 Part2 Multicomponent Image Compression LosslessOnly
//	    ///. EXS_JPIPReferenced = 32,
//	    ///. EXS_JPIPReferencedDeflate = 33,
//        nError = dcmDataset.save(pstrFileName, nTransferSyntax);
//
//	}
//	catch (...)
//	{
//        if (pBuffer)
//        {
//            rsa_dec_free(pBuffer);
//            pBuffer = NULL;
//        }
//        if (pJasperDec)
//        {
//            rsa_dec_close(pJasperDec);
//            pJasperDec = NULL;
//        }
//		return -1;
//	}
	return nError;
}

int QtSqlPersistence::saveDataset(const DataSet& dcmDataset)
{
    int nError(-1);
    try
    {
        DataSet* pDataset = (DataSet*) (&dcmDataset);
        DcmItemHandle pItemRoot = pDataset->getRoot();

        bool bFlag(false), bSearchChild(false);
        unsigned long lPos(0);
	    char strValue[128];

        //sop instance uid
		DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
		bFlag = pDataset->findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
        QString strSOPInstanceUID = strValue;

	    //DCM_PatientID
	    tagKey.group = 0x0010, tagKey.element = 0x0020;
	    bFlag = pDataset->findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
        if (m_bPatientAnonymized || bFlag==false || strlen(strValue) == 0)
        {
		    nError = pDataset->setStringValue(pItemRoot, tagKey, "Anonymized");
        }
        if (m_bPatientAnonymized)
        {
		    //DCM_PatientsName
		    tagKey.group = 0x0010, tagKey.element = 0x0010;
		    nError = pDataset->setStringValue(pItemRoot, tagKey, "Anonymized");
        }

        ///. store dataset info to DB, thread-safe???
        int nTransferSyntaxID = pDataset->findAndGetTransferSyntaxUID(strValue, 128);
        QString strFileName = QString("%1\\%2.tdcm").
            arg(m_strTempDataDir).
            arg(strSOPInstanceUID);
        nError = pDataset->save(strFileName.toStdString().c_str(), nTransferSyntaxID);
    }
    catch (...)
    {
        return -1;
    }
    return nError;
}

int QtSqlPersistence::onSearchSingle(const QString& strSearchDir, bool bSearchSub)const
{
    int nError(0), nCountOfFiles(0);
	DataSet dsTemp;

//    penc_t* pJasDec = NULL;
//    penc_t* pJasEnc = NULL;
//    unsigned int x0(0), y0(0);

    QDir qtDirTemp(strSearchDir);
    QFileInfoList listFileInfo = qtDirTemp.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot);
    for (int i=0; i<listFileInfo.size(); ++i)
    {
        const QFileInfo& fileInfo = listFileInfo.at(i);
        if (fileInfo.isFile() == true)
        {
            QDir dirTemp;
            QString strTempDcmFileName = fileInfo.filePath();
            ///.
            if (dsTemp.load(strTempDcmFileName.toStdString().c_str()) != 0)
            {
//                m_strLastError = QString("Failed to load: %1").
//                    arg(strTempDcmFileName);
                ///. remove temp dcm file
                dirTemp.remove( strTempDcmFileName );
                continue;
            }
            bool bFlag(false), bSearchChild(false), bReplace(true);
            unsigned long lPos(0);
		    char strValue[128];
		    DcmItemHandle pItemRoot = dsTemp.getRoot();
		
            //sop instance uid
		    DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
		    bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, 0, FALSE);
            QString strSOPInstanceUID = strValue;
            //SOPClassUID
		    tagKey.group = 0x0008, tagKey.element = 0x0016;
		    bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, 0, FALSE);
            QString strSOPClassUID = strValue;
            //Transfer Syntax
            int nTransferSyntaxID = dsTemp.findAndGetTransferSyntaxUID(strValue, 128);
            QString strTransferSyntaxUID = strValue;

            //DCM_PatientID
		    tagKey.group = 0x0010, tagKey.element = 0x0020;
		    bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            if (m_bPatientAnonymized || bFlag==false || strlen(strValue) == 0)
            {
		        nError = dsTemp.setStringValue(pItemRoot, tagKey, "Anonymized");
            }
            if (m_bPatientAnonymized)
            {
		        //DCM_PatientsName
		        tagKey.group = 0x0010, tagKey.element = 0x0010;
		        nError = dsTemp.setStringValue(pItemRoot, tagKey, "Anonymized");
            }
            //folder DCM_StudyInstanceUID
		    tagKey.group = 0x0020, tagKey.element = 0x000d;
		    bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            QString strDataDir = QString("%1\\%2").
                arg(m_strWorkingDir).
                arg(strValue);
		    //folder DCM_SeriesInstanceUID
		    tagKey.group = 0x0020, tagKey.element = 0x000e;
		    bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            strDataDir = QString("%1\\%2").
                arg(strDataDir).
                arg(strValue);
            ///. save Dcm dataset to local
            if (dirTemp.exists(strDataDir) == false)
            {
                bFlag = dirTemp.mkpath(strDataDir);
            } 
            if (dirTemp.exists() == false)
            {
//                m_ui->m_textBrowserLogger->append(
//                    QString("Can't find path: %1").arg(dirTemp.path())
//                    );
                ///. remove temp dcm file
                dirTemp.remove( strTempDcmFileName );
                continue;
            }
            //file DCM_SOPInstanceUID
            QString strFileName = QString("%1\\%2").
                    arg(strDataDir).
                    arg(strSOPInstanceUID);
            if (dirTemp.exists(strFileName) == true)
            {
//                m_ui->m_textBrowserLogger->append(
//                    QString("File is already exited: %1").arg(strFileName)
//                    );
                ///. remove temp dcm file
                dirTemp.remove( strTempDcmFileName );
                continue;
            }

            QString strFileNameIMG;
            bool bUrlWSI;
            ///. PixelData
            tagKey.group = 0x7fe0, tagKey.element = 0x0010;
            long nLenOfPixelData = dsTemp.getFirstElementFromItem(pItemRoot, tagKey, bSearchSub, lPos, NULL, 0);
            ///. "Pixel Data Provider URL"
            QString strFileSuffix;
            tagKey.group = 0x0028, tagKey.element = 0x7FE0;
            if (dsTemp.findAndGetString(pItemRoot, tagKey, strValue, 0, FALSE) == true &&
                strlen(strValue) > 0 &&
                nLenOfPixelData <= 0)
            {
                //QFileInfo infoFile(strValue);
                //strFileSuffix = QFileInfo(strValue).suffix();
                strFileNameIMG = QString("%1.%2").arg(strFileName).arg(strFileExtension);

                QFile fileTemp(strValue);
                if (fileTemp.exists(strFileNameIMG) == false &&
                    fileTemp.copy(strFileNameIMG) == false)
                {
                    QFile::FileError err = fileTemp.error();
//                    m_ui->m_textBrowserLogger->append(
//                        QString("Failed to copy: #%1;").arg(err)
//                        );
//                    m_ui->m_textBrowserLogger->append(
//                        strValue
//                        );
                    bUrlWSI = false;
                }
                else
                {
                    bUrlWSI = true;
                }
            }
            else
            {   ///. can not find the URL
                strFileNameIMG = "";
                bUrlWSI = false;
            }
            /* check if URL is readable */

            if (bUrlWSI == true)
            {
                ///. "\\\\PATHCORE_DEV1\\images\\M3_26_00_Pyramid.tif"
//                QString strTemp("C:\\02.Project\\4.NeuDPTK_WS\\DPTKWindowsService\\data\\9.99.999.1.20131028.155720.1\\9.99.999.1.20131028.155720.1.1\\9.99.999.1.20131028.155720.1.1.563.tif");
                QString strTemp(strFileNameIMG);
//                if (strFileSuffix)
//                {
//                }
                sedeen::image::ImageRepository& imageRepository = sedeen::image::ImageRepository::instance();
                sedeen::image::ImageAccessor imageAccessor = imageRepository.get(
                    strTemp.toStdString()
                    );
                sedeen::SizeF sizePixel = imageAccessor.getPixelSize();
                ///.
                unsigned int nLevelOfTotalImage(0);
                sedeen::Size sizeDimensionOfTotalPixel = imageAccessor.getDimensions(nLevelOfTotalImage);
                nLevelOfTotalImage = imageAccessor.getNumResolutionLevels();
                sedeen::Size sizeDimensionOfThumbNail = imageAccessor.getDimensions(nLevelOfTotalImage-1);
                ///.
                sedeen::Point pointThumbnail(0, 0);
                sedeen::Rect regionThumbnail(pointThumbnail, sizeDimensionOfThumbNail);
/*                ///. Tiff Parser
                //const sedeen::image::ImageParser* pImageParser =  &(imageAccessor.imageParser());
                const sedeen::image::TIFFParser* pImageParser = static_cast<const sedeen::image::TIFFParser*>(&(imageAccessor.imageParser()));
                //int nBufBPP = pImageParser->bpp() / 8;
                //int nIndexOfThumbnailDir = pImageParser->thumbnailDir();
                //sedeen::image::RawImage imageThumbnail = pImageParser->thumbnailImage();
                //if (imageThumbnail.isNull() != true)
                //{
                //    int nBufWidth(imageThumbnail.width()),
                //        nBufHeight(imageThumbnail.height()), 
                //        nBufBPP(imageThumbnail.bytesPerPixel());
                //    sedeen::image::ConstRawDataPtr pRawData = imageThumbnail.data();
                //}
                //sedeen::image::RawImage imageParser = pImageParser->thumbnailImage();
                //sedeen::image::ImagePyramidPtr ptrImagePyramid = pImageParser->pyramid();
                //sedeen::image::ImagePyramid* pImagePyramid = ptrImagePyramid.get();
                ///. Tiff Reader
                sedeen::image::TIFFReader tiffReader = pImageParser->reader();
                uint16_t nNumOfDirs = tiffReader.numDirectories();
                uint16_t nIndexOfDir=0;
                for (; nIndexOfDir<nNumOfDirs; ++nIndexOfDir)
                {
                    sedeen::Size sizeDimensionDir = tiffReader.dimensions(nIndexOfDir);
                    if (sizeDimensionDir == sizeDimensionOfThumbNail)
                    {
                        break;
                    }
                }
                if (nIndexOfDir >= nNumOfDirs)
                    nIndexOfDir = nNumOfDirs-1;
//                    sedeen::Size sizeTile = tiffReader.tileSize(nIndexOfDir);
//                    sedeen::image::RawImage imageRaw = tiffReader.getTile(nIndexOfDir, pointTile );
                sedeen::image::RawImage imageThumbnail = tiffReader.getRegion( nIndexOfDir, regionThumbnail );
*/
//                sedeen::image::RawImage imageThumbnail = sedeen::image::regionBlock(
//                    imageAccessor, nResLevel-1, regionThumbnail, sedeen::image::PriorityLevels::kNormal, false
//                    );
                sedeen::image::ThreadedRegionBlock regionBlock(
                    imageAccessor, nLevelOfTotalImage-1, regionThumbnail
                    );
                sedeen::image::RawImage imageThumbnail = regionBlock.getImage();

                ///. Tiff image
                unsigned long nWidthOfTotalPixel( sizeDimensionOfTotalPixel.width() );
                unsigned long nHeightOfTotalPixel( sizeDimensionOfTotalPixel.height() );
                int nBufWidth(imageThumbnail.width()),
                    nBufHeight(imageThumbnail.height()), 
                    nBufBPP(imageThumbnail.bytesPerPixel());
                sedeen::image::ConstRawDataPtr pRawData = imageThumbnail.data();
                const unsigned char *pDataBuf = static_cast<const unsigned char*>( pRawData.get() );

                short nValue;
                if (nBufBPP == 3)
                {
                    //SamplesPerPixel
                    nValue = 3;
                    tagKey.group = 0x0028, tagKey.element = 0x0002;
                    nError = dsTemp.setShortValue(pItemRoot, tagKey, &nValue, 1, bReplace);
                    ///. PhotometricInterpretation
                    tagKey.group = 0x0028, tagKey.element = 0x0004;
                    nError = dsTemp.setStringValue(pItemRoot, tagKey, "RGB", bReplace);
                }
                else
                {
                    //SamplesPerPixel
                    nValue = 1;
                    tagKey.group = 0x0028, tagKey.element = 0x0002;
                    nError = dsTemp.setShortValue(pItemRoot, tagKey, &nValue, 1, bReplace);
                    ///. PhotometricInterpretation
                    tagKey.group = 0x0028, tagKey.element = 0x0004;
                    nError = dsTemp.setStringValue(pItemRoot, tagKey, "MONOCHROME2", bReplace);
                }
                ///. Rows
                nValue = nBufHeight;
                tagKey.group = 0x0028, tagKey.element = 0x0010;
                nError = dsTemp.setShortValue(pItemRoot, tagKey, &nValue, 1, bReplace);
                ///. Columns
                nValue = nBufWidth;
                tagKey.group = 0x0028, tagKey.element = 0x0011;
                nError = dsTemp.setShortValue(pItemRoot, tagKey, &nValue, 1, bReplace);
                ///. BitsAllocated
                nValue = nBufBPP*8;
                tagKey.group = 0x0028, tagKey.element = 0x0100;
                nError = dsTemp.setShortValue(pItemRoot, tagKey, &nValue, 1, bReplace);
                ///. BitsStored
                nValue = nBufBPP*8;
                tagKey.group = 0x0028, tagKey.element = 0x0101;
                nError = dsTemp.setShortValue(pItemRoot, tagKey, &nValue, 1, bReplace);
                ///. HighBit
                nValue = nBufBPP*8-1;
                tagKey.group = 0x0028, tagKey.element = 0x0102;
                nError = dsTemp.setShortValue(pItemRoot, tagKey, &nValue, 1, bReplace);
                ///. PixelRepresentation
                nValue = 0;
                tagKey.group = 0x0028, tagKey.element = 0x0103;
                nError = dsTemp.setShortValue(pItemRoot, tagKey, &nValue, 1, bReplace);
                ///. PixelData
                tagKey.group = 0x7fe0, tagKey.element = 0x0010;
//#ifdef _DEBUG
//                FILE	*fp;
//                if ((fp=fopen(strFileName.toStdString().c_str(), "wb")) != NULL)
//                {
//                    fwrite(pDataBuf, sizeof(unsigned char), nBufWidth*nBufHeight*nBufBPP, fp);
//                    fclose(fp);
//                }
//#endif
                nError = dsTemp.setShortValue(
                    pItemRoot, tagKey, (short*)pRawData.get(), nBufWidth*nBufHeight*nBufBPP/2, bReplace
                    );
                ///. ImageType
                tagKey.group = 0x0008, tagKey.element = 0x0008;
                nError = dsTemp.setStringValue(pItemRoot, tagKey, "DERIVED\\PRIMARY\\VOLUME\\RESAMPLED", bReplace);
                ///. ImagedVolumeWidth
                double fValue = static_cast<double>(nWidthOfTotalPixel);
                tagKey.group = 0x0048, tagKey.element = 0x0001;
                if (dsTemp.findAndGetFloat(pItemRoot, tagKey, lPos, bSearchSub) < 1.0f)
                {
                    nError = dsTemp.setDoubleValue(pItemRoot, tagKey, &fValue, 1, bReplace);
                }
                ///. ImagedVolumeHeight
                fValue = static_cast<double>(nHeightOfTotalPixel);
                tagKey.group = 0x0048, tagKey.element = 0x0002;
                if (dsTemp.findAndGetFloat(pItemRoot, tagKey, lPos, bSearchSub) < 1.0f)
                {
                    nError = dsTemp.setDoubleValue(pItemRoot, tagKey, &fValue, 1, bReplace);
                }
                ///. ImagedVolumeDepth
                fValue = 1.0;
                tagKey.group = 0x0048, tagKey.element = 0x0003;
                if (dsTemp.findAndGetFloat(pItemRoot, tagKey, lPos, bSearchSub) < 1.0f)
                {
                    nError = dsTemp.setDoubleValue(pItemRoot, tagKey, &fValue, 1, bReplace);
                }
                ///. TotalPixelMatrixColumns
                long lValue = static_cast<long>(nWidthOfTotalPixel);
                tagKey.group = 0x0048, tagKey.element = 0x0006;
                nError = dsTemp.setLongValue(pItemRoot, tagKey, &lValue, 1, bReplace);
                ///. TotalPixelMatrixRows
                lValue = static_cast<long>(nHeightOfTotalPixel);
                tagKey.group = 0x0048, tagKey.element = 0x0007;
                nError = dsTemp.setLongValue(pItemRoot, tagKey, &lValue, 1, bReplace);

                ///. DCM_ReferencedImageNavigationSequence
                tagKey.group = 0x0048, tagKey.element = 0x0200;
                DcmSequenceOfItemsHandle pSeq = dsTemp.findSequence(pItemRoot, tagKey, bSearchChild);
                if (pSeq && dsTemp.getNumOfItems(pSeq)>0)
                {
                    const DcmItemHandle pSeqItem = dsTemp.getItemFromSeq(pSeq, lPos);
                    //ReferencedSOPClassUID
                    tagKey.group = 0x0008, tagKey.element = 0x1150;
                    nError = dsTemp.setStringValue(pSeqItem, tagKey, strSOPClassUID.toStdString().c_str(), bReplace);
                    //ReferencedSOPInstanceUID
                    tagKey.group = 0x0008, tagKey.element = 0x1155;
                    nError = dsTemp.setStringValue(pSeqItem, tagKey, strSOPInstanceUID.toStdString().c_str(), bReplace);
                    ///. ReferencedFrameNumber
                    lValue = static_cast<long>(nLevelOfTotalImage-1);
                    tagKey.group = 0x0008, tagKey.element = 0x1160;
                    nError = dsTemp.setLongValue(pSeqItem, tagKey, &lValue, 1, bReplace);

                    //DCM_TopLeftHandCornerOfLocalizerArea
                    tagKey.group = 0x0048, tagKey.element = 0x0201;
                    short nValueTemp[2];
                    nValueTemp[0] = 0;
                    nValueTemp[1] = 0;
                    nError = dsTemp.setShortValue(pSeqItem, tagKey, nValueTemp, 2, bReplace);
                    //DCM_BottomRightHandCornerOfLocalizerArea
                    tagKey.group = 0x0048, tagKey.element = 0x0202;
                    nValueTemp[0] = nBufWidth-1;
                    nValueTemp[1] = nBufHeight-1;
                    nError = dsTemp.setShortValue(pSeqItem, tagKey, nValueTemp, 2, bReplace);
                }
                ///. "Pixel Data Provider URL"
                tagKey.group = 0x0028, tagKey.element = 0x7FE0;
                nError = dsTemp.setStringValue(
                    pItemRoot, tagKey, m_strPixelDataProvider.toStdString().c_str(), bReplace
                    );

                ///. will save in UID_LittleEndianExplicitTransferSyntax
                nTransferSyntaxID = 0;
            }
            ///. end of if (bUrlWSI == true)

            ///. store dataset info to DK, thread-safe???
            nError = dsTemp.save(strFileName.toStdString().c_str(), nTransferSyntaxID);
            if (nError == 0)
            {
                ///. store dataset info to DB, thread-safe???
                nError = QtSqlPersistence::getInstance()->setDcmDataset(dsTemp);
            }
            
            ///. remove temp dcm file
            dirTemp.remove( fileInfo.filePath().toStdString().c_str() );
            nCountOfFiles += 1;
        }
        else if (fileInfo.isDir()==true && bSearchSub==true)
        {
            nCountOfFiles += onSearchSingle(fileInfo.filePath(), bSearchSub);
        }
        else
        {
        }
        ///. end of if (fileInfo.isFile() == true)
    }
    return nCountOfFiles;
}

int QtSqlPersistence::onSearch(const QString& strSearchDir, bool bSearchSub)const
{
    int nError(0), nCountOfFiles(0);
	DataSet dsTemp;
    char* pBufImage = NULL;

    QDir qtDirTemp(strSearchDir);
    QFileInfoList listFileInfo = qtDirTemp.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot);
    for (int i=0; i<listFileInfo.size(); ++i)
    {
        const QFileInfo& fileInfo = listFileInfo.at(i);
        if (fileInfo.isFile() == true)
        {
            QDir dirTemp;
            QString strTempDcmFileName = fileInfo.filePath();
            ///.
            if (dsTemp.load(strTempDcmFileName.toStdString().c_str()) != 0)
            {
//                m_strLastError = QString("Failed to load: %1").
//                    arg(strTempDcmFileName);
                ///. remove temp dcm file
                dirTemp.remove( strTempDcmFileName );
                continue;
            }
            bool bFlag(false), bSearchChild(false), bReplace(true);
            unsigned long lPos(0);
		    char strValue[128];
		    DcmItemHandle pItemRoot = dsTemp.getRoot();
		
            //sop instance uid
		    DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
		    bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, 0, FALSE);
            QString strSOPInstanceUID = strValue;
            //SOPClassUID
		    tagKey.group = 0x0008, tagKey.element = 0x0016;
		    bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, 0, FALSE);
            QString strSOPClassUID = strValue;
            //Transfer Syntax
            int nTransferSyntaxID = dsTemp.findAndGetTransferSyntaxUID(strValue, 128);
            QString strTransferSyntaxUID = strValue;

            //DCM_PatientID
		    tagKey.group = 0x0010, tagKey.element = 0x0020;
		    bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            if (m_bPatientAnonymized || bFlag==false || strlen(strValue) == 0)
            {
		        nError = dsTemp.setStringValue(pItemRoot, tagKey, "Anonymized");
            }
            if (m_bPatientAnonymized)
            {
		        //DCM_PatientsName
		        tagKey.group = 0x0010, tagKey.element = 0x0010;
		        nError = dsTemp.setStringValue(pItemRoot, tagKey, "Anonymized");
            }
            //folder DCM_StudyInstanceUID
		    tagKey.group = 0x0020, tagKey.element = 0x000d;
		    bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            QString strDataDir = QString("%1\\%2").
                arg(m_strWorkingDir).
                arg(strValue);
		    //folder DCM_SeriesInstanceUID
		    tagKey.group = 0x0020, tagKey.element = 0x000e;
		    bFlag = dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild);
            strDataDir = QString("%1\\%2").
                arg(strDataDir).
                arg(strValue);
            ///. save Dcm dataset to local
            if (dirTemp.exists(strDataDir) == false)
            {
                bFlag = dirTemp.mkpath(strDataDir);
            } 
            if (dirTemp.exists() == false)
            {   ///. remove temp dcm file
                dirTemp.remove( strTempDcmFileName );
                continue;
            }
            ///.ConcatenationUID
            QString strConcatenationUID("");
            tagKey.group = 0x0020, tagKey.element = 0x9161;
            if (dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild) == true &&
                strlen(strValue) > 0)
            {
                strConcatenationUID = strValue;
            }
            ///.SOPInstanceUIDOfConcatenationSource
            QString strSOPInstanceUIDOfConcatenationSource("");
            tagKey.group = 0x0020, tagKey.element = 0x0242;
            if (dsTemp.findAndGetString(pItemRoot, tagKey, strValue, lPos, bSearchChild) == true &&
                strlen(strValue) > 0)
            {
                strSOPInstanceUIDOfConcatenationSource = strValue;
            }
            else
            {
                strSOPInstanceUIDOfConcatenationSource = strConcatenationUID;
            }
            ///. folder ConcatenationSource
            if (strSOPInstanceUIDOfConcatenationSource.length() > 0)
            {
                strDataDir = QString("%1\\%2").arg(strDataDir).arg(strSOPInstanceUIDOfConcatenationSource);
                if (dirTemp.exists(strDataDir) == false)
                {
                    bFlag = dirTemp.mkpath(strDataDir);
                } 
                if (dirTemp.exists() == false)
                {   ///. remove temp dcm file
                    dirTemp.remove( strTempDcmFileName );
                    continue;
                }
            }
            //file DCM_SOPInstanceUID
            QString strFileName = QString("%1\\%2").
                    arg(strDataDir).
                    arg(strSOPInstanceUID);
            if (dirTemp.exists(strFileName) == true)
            {
                ///. remove temp dcm file
                dirTemp.remove( strTempDcmFileName );
                continue;
            }

            QString strFileNameIMG;
            bool bUrlWSI;
            ///. PixelData
            tagKey.group = 0x7fe0, tagKey.element = 0x0010;
            long nLenOfPixelData = dsTemp.getFirstElementFromItem(pItemRoot, tagKey, bSearchSub, lPos, NULL, 0);
            ///. "Pixel Data Provider URL"
            QString strFileSuffix;
            tagKey.group = 0x0028, tagKey.element = 0x7FE0;
            if (dsTemp.findAndGetString(pItemRoot, tagKey, strValue, 0, FALSE) == true &&
                strlen(strValue) > 0 &&
                nLenOfPixelData <= 0)
            {
                //QFileInfo infoFile(strValue);
                //strFileSuffix = QFileInfo(strValue).suffix();
                strFileNameIMG = QString("%1.%2").arg(strFileName).arg(strFileExtension);

                QFile fileTemp(strValue);
                if (fileTemp.exists(strFileNameIMG) == false &&
                    fileTemp.copy(strFileNameIMG) == false)
                {
                    QFile::FileError err = fileTemp.error();
//                    m_ui->m_textBrowserLogger->append(
//                        QString("Failed to copy: #%1;").arg(err)
//                        );
//                    m_ui->m_textBrowserLogger->append(
//                        strValue
//                        );
                    bUrlWSI = false;
                }
                else
                {
                    bUrlWSI = true;
                }
            }
            else
            {   ///. can not find the URL
                strFileNameIMG = "";
                bUrlWSI = false;
            }
            /* check if URL is readable */

            if (bUrlWSI == true)
            {
                //strFileName = "C:\\02.Project\\4.NeuDPTK_WS\\DPTKWindowsService\\data\\9.99.999.1.20131111.095201.1\\9.99.999.1.20131111.095201.1.1\\9.99.999.1.20131111.095201.1.1.799";
                //strFileNameIMG = "C:\\02.Project\\4.NeuDPTK_WS\\DPTKWindowsService\\data\\9.99.999.1.20131111.095201.1\\9.99.999.1.20131111.095201.1.1\\9.99.999.1.20131111.095201.1.1.799.img";
                /////.
                QString strTemp(strFileNameIMG);
                sedeen::image::ImageRepository& imageRepository = sedeen::image::ImageRepository::instance();
                sedeen::image::ImageAccessor imageAccessor = imageRepository.get(
                    strTemp.toStdString()
                    );
                sedeen::SizeF sizePixelInTotalVol = imageAccessor.getPixelSize();
                ///. PixelAspectRatio
                tagKey.group = 0x0028, tagKey.element = 0x0034;
                if (sizePixelInTotalVol.width() < 0.01)
                {
                    sizePixelInTotalVol.setWidth(
                        dsTemp.findAndGetFloat(pItemRoot, tagKey, lPos, bSearchChild)
                        );
                }
                if (sizePixelInTotalVol.height() < 0.01)
                {
                    sizePixelInTotalVol.setHeight(
                        dsTemp.findAndGetFloat(pItemRoot, tagKey, lPos+1, bSearchChild)
                        );
                }
                ///.
                if (sizePixelInTotalVol.width() < 0.01)
                    sizePixelInTotalVol.setWidth(1.0);
                if (sizePixelInTotalVol.height() < 0.01)
                    sizePixelInTotalVol.setHeight(1.0);
                ///.
                unsigned int nLevelOfTotalImage(0);
                sedeen::Size sizeDimensionOfTotalPixel = imageAccessor.getDimensions(nLevelOfTotalImage);
                nLevelOfTotalImage = imageAccessor.getNumResolutionLevels();

                ///. Processing thumbnail image
                sedeen::Size sizeDimensionOfThumbNail = imageAccessor.getDimensions(nLevelOfTotalImage-1);
                sedeen::Point pointThumbnail(0, 0);
                sedeen::Rect regionThumbnail(pointThumbnail, sizeDimensionOfThumbNail);
                sedeen::image::ThreadedRegionBlock regionBlock(
                    imageAccessor, nLevelOfTotalImage-1, regionThumbnail
                    );
                sedeen::image::RawImage imageThumbnail = regionBlock.getImage();
                int nBufWidth(imageThumbnail.width()),
                    nBufHeight(imageThumbnail.height()), 
                    nBufBPP(imageThumbnail.bytesPerPixel());
                ///. Tiff image
                sedeen::image::ConstRawDataPtr pRawData = imageThumbnail.data();
//                const unsigned char *pDataBuf = static_cast<const unsigned char*>( pRawData.get() );
                short nValue;
                short nSamplesPerPixel;
                double fPixelAspectRatio[2];
                ///. The data in each pixel may be represented as a “Composite Pixel Code”.
                ///.  If Samples Per Pixel is greater than one, Composite Pixel Code is a “k” bit concatenation of samples,
                ///.  where “k” = Bits Allocated multiplied by Samples Per Pixel,
                if (nBufBPP == 3)
                {
                    //SamplesPerPixel
                    nSamplesPerPixel = 3;
                    tagKey.group = 0x0028, tagKey.element = 0x0002;
                    nError = dsTemp.setShortValue(pItemRoot, tagKey, &nSamplesPerPixel, 1, bReplace);
                    ///. PhotometricInterpretation
                    tagKey.group = 0x0028, tagKey.element = 0x0004;
                    nError = dsTemp.setStringValue(pItemRoot, tagKey, "RGB", bReplace);
                }
                else
                {
                    //SamplesPerPixel
                    nSamplesPerPixel = 1;
                    tagKey.group = 0x0028, tagKey.element = 0x0002;
                    nError = dsTemp.setShortValue(pItemRoot, tagKey, &nSamplesPerPixel, 1, bReplace);
                    ///. PhotometricInterpretation
                    tagKey.group = 0x0028, tagKey.element = 0x0004;
                    nError = dsTemp.setStringValue(pItemRoot, tagKey, "MONOCHROME2", bReplace);
                }
                ///. Rows
                nValue = nBufHeight;
                tagKey.group = 0x0028, tagKey.element = 0x0010;
                nError = dsTemp.setShortValue(pItemRoot, tagKey, &nValue, 1, bReplace);
                ///. Columns
                nValue = nBufWidth;
                tagKey.group = 0x0028, tagKey.element = 0x0011;
                nError = dsTemp.setShortValue(pItemRoot, tagKey, &nValue, 1, bReplace);
                ///. BitsAllocated
                nValue = nBufBPP*8/nSamplesPerPixel;
                tagKey.group = 0x0028, tagKey.element = 0x0100;
                nError = dsTemp.setShortValue(pItemRoot, tagKey, &nValue, 1, bReplace);
                ///. PixelAspectRatio
//                double fPixelAspectRatio[2];
                tagKey.group = 0x0028, tagKey.element = 0x0034;
//                fPixelAspectRatio[0] = ;
//                fPixelAspectRatio[1] = sizeDimensionOfTotalPixel.height() * sizePixelInTotalVol.height() / sizeDimensionOfThumbNail.height();
                sprintf(
                    strValue, "%f\\%f", 
                    sizeDimensionOfTotalPixel.width() * sizePixelInTotalVol.width() / sizeDimensionOfThumbNail.width(),
                    sizeDimensionOfTotalPixel.height() * sizePixelInTotalVol.height() / sizeDimensionOfThumbNail.height()
                    );
                nError = dsTemp.setStringValue(pItemRoot, tagKey, strValue, bReplace);
                //DCM_PixelSpacing
                tagKey.group = 0x0028, tagKey.element = 0x0030;
                nError = dsTemp.setStringValue(pItemRoot, tagKey, strValue, bReplace);
                ///. BitsStored
                nValue = nBufBPP*8/nSamplesPerPixel;
                tagKey.group = 0x0028, tagKey.element = 0x0101;
                nError = dsTemp.setShortValue(pItemRoot, tagKey, &nValue, 1, bReplace);
                ///. HighBit
                nValue = nBufBPP*8/nSamplesPerPixel-1;
                tagKey.group = 0x0028, tagKey.element = 0x0102;
                nError = dsTemp.setShortValue(pItemRoot, tagKey, &nValue, 1, bReplace);
                ///. PixelRepresentation
                nValue = 0;
                tagKey.group = 0x0028, tagKey.element = 0x0103;
                nError = dsTemp.setShortValue(pItemRoot, tagKey, &nValue, 1, bReplace);
                ///. PixelData
                tagKey.group = 0x7fe0, tagKey.element = 0x0010;
//#ifdef _DEBUG
//                FILE	*fp;
//                if ((fp=fopen(strFileName.toStdString().c_str(), "wb")) != NULL)
//                {
//                    fwrite(pDataBuf, sizeof(unsigned char), nBufWidth*nBufHeight*nBufBPP, fp);
//                    fclose(fp);
//                }
//#endif
                nError = dsTemp.setShortValue(
                    pItemRoot, tagKey, (short*)pRawData.get(), nBufWidth*nBufHeight*nBufBPP/2, bReplace
                    );
                ///. ImageType: LOCALIZER, VOLUME, LABEL \\ NONE, RESAMPLED.
                tagKey.group = 0x0008, tagKey.element = 0x0008;
                nError = dsTemp.setStringValue(pItemRoot, tagKey, "DERIVED\\PRIMARY\\VOLUME\\NONE", bReplace);
                ///. ImagedVolumeWidth, in mm
                double fValue = static_cast<double>(
                    sizeDimensionOfTotalPixel.width() * sizePixelInTotalVol.width()
                    );
                tagKey.group = 0x0048, tagKey.element = 0x0001;
//                if (dsTemp.findAndGetDouble(pItemRoot, tagKey, lPos, bSearchSub) < 1.0f)
//                {
                    nError = dsTemp.setDoubleValue(pItemRoot, tagKey, &fValue, 1, bReplace);
//                }
                ///. ImagedVolumeHeight, in mm
                fValue = static_cast<double>(
                    sizeDimensionOfTotalPixel.height() * sizePixelInTotalVol.height()
                    );
                tagKey.group = 0x0048, tagKey.element = 0x0002;
//                if (dsTemp.findAndGetDouble(pItemRoot, tagKey, lPos, bSearchSub) < 1.0f)
//                {
                    nError = dsTemp.setDoubleValue(pItemRoot, tagKey, &fValue, 1, bReplace);
//                }
                ///. ImagedVolumeDepth, in μm
                fValue = 1.0;
                tagKey.group = 0x0048, tagKey.element = 0x0003;
                if (dsTemp.findAndGetDouble(pItemRoot, tagKey, lPos, bSearchSub) < 0.1f)
                {
                    nError = dsTemp.setDoubleValue(pItemRoot, tagKey, &fValue, 1, bReplace);
                }
                 //DCM_SliceThickness
                tagKey.group = 0x0018, tagKey.element = 0x0050;
                if (dsTemp.findAndGetDouble(pItemRoot, tagKey, lPos, bSearchSub) < 0.1f)
                {
                    nError = dsTemp.setDoubleValue(pItemRoot, tagKey, &fValue, 1, bReplace);
                }
                ///. TotalPixelMatrixColumns
                long lValue = static_cast<long>(sizeDimensionOfThumbNail.width());
                tagKey.group = 0x0048, tagKey.element = 0x0006;
                nError = dsTemp.setLongValue(pItemRoot, tagKey, &lValue, 1, bReplace);
                ///. TotalPixelMatrixRows
                lValue = static_cast<long>(sizeDimensionOfThumbNail.height());
                tagKey.group = 0x0048, tagKey.element = 0x0007;
                nError = dsTemp.setLongValue(pItemRoot, tagKey, &lValue, 1, bReplace);
                ///. number of frames
                nValue = 1;
                tagKey.group = 0x0028, tagKey.element = 0x0008;
                nError = dsTemp.setShortValue(pItemRoot, tagKey, &nValue, 1, bReplace);
                ///. DCM_ReferencedImageNavigationSequence
                tagKey.group = 0x0048, tagKey.element = 0x0200;
//                nError = dsTemp.removeSequenceItem(pItemRoot, tagKey, -2);
                DcmSequenceOfItemsHandle pSeqOfNavigation = dsTemp.findSequence(pItemRoot, tagKey, bSearchChild);
                if (pSeqOfNavigation && dsTemp.getNumOfItems(pSeqOfNavigation)>0)
                {
                    const DcmItemHandle pSeqItem = dsTemp.getItemFromSeq(pSeqOfNavigation, lPos);
                    //ReferencedSOPClassUID
                    tagKey.group = 0x0008, tagKey.element = 0x1150;
                    nError = dsTemp.setStringValue(pSeqItem, tagKey, strSOPClassUID.toUtf8(), bReplace);
                    //ReferencedSOPInstanceUID
                    tagKey.group = 0x0008, tagKey.element = 0x1155;
                    nError = dsTemp.setStringValue(pSeqItem, tagKey, strSOPInstanceUID.toUtf8(), bReplace);
                    ///. ReferencedFrameNumber
                    lValue = static_cast<long>(0);
                    tagKey.group = 0x0008, tagKey.element = 0x1160;
                    nError = dsTemp.setLongValue(pSeqItem, tagKey, &lValue, 1, bReplace);

                    //DCM_PixelSpacing
                    tagKey.group = 0x0028, tagKey.element = 0x0030;
                    sprintf(
                        strValue, "%f\\%f", 
                        sizeDimensionOfTotalPixel.width() * sizePixelInTotalVol.width() / sizeDimensionOfThumbNail.width(),
                        sizeDimensionOfTotalPixel.height() * sizePixelInTotalVol.height() / sizeDimensionOfThumbNail.height()
                        );
                    nError = dsTemp.setStringValue(pSeqItem, tagKey, strValue, bReplace);
                    //DCM_TopLeftHandCornerOfLocalizerArea
                    tagKey.group = 0x0048, tagKey.element = 0x0201;
                    short nValueTemp[2];
                    nValueTemp[0] = 0;
                    nValueTemp[1] = 0;
                    nError = dsTemp.setShortValue(pSeqItem, tagKey, nValueTemp, 2, bReplace);
                    //DCM_BottomRightHandCornerOfLocalizerArea
                    tagKey.group = 0x0048, tagKey.element = 0x0202;
                    nValueTemp[0] = nBufWidth-1;
                    nValueTemp[1] = nBufHeight-1;
                    nError = dsTemp.setShortValue(pSeqItem, tagKey, nValueTemp, 2, bReplace);
                }
                ///. "Pixel Data Provider URL"
                tagKey.group = 0x0028, tagKey.element = 0x7FE0;
                nError = dsTemp.setStringValue(
                    pItemRoot, tagKey, m_strPixelDataProvider.toStdString().c_str(), bReplace
                    );

                ///. Multi-frame Functional Groups Module
                ///. Instance Number
                lValue = nLevelOfTotalImage;
                tagKey.group = 0x0020, tagKey.element = 0x0013;
                nError = dsTemp.setLongValue(
                    pItemRoot, tagKey, &lValue, 1, bReplace
                    );
                ///. Concatenation Frame Offset Number
                ///.ConcatenationFrameOffsetNumber
                lValue = 0;
                tagKey.group = 0x0020, tagKey.element = 0x9228;
                nError = dsTemp.setLongValue(
                    pItemRoot, tagKey, &lValue, 1, bReplace
                    );
                ///.ConcatenationUID
                tagKey.group = 0x0020, tagKey.element = 0x9161;
                nError = dsTemp.setStringValue(
                    pItemRoot, tagKey, strSOPInstanceUID.toUtf8(), bReplace
                    );
                ///.SOPInstanceUIDOfConcatenationSource
                tagKey.group = 0x0020, tagKey.element = 0x0242;
                nError = dsTemp.setStringValue(
                    pItemRoot, tagKey, strSOPInstanceUID.toUtf8(), bReplace
                    );
                ///.InConcatenationNumber
                nValue = 1;
                tagKey.group = 0x0020, tagKey.element = 0x9162;
                nError = dsTemp.setShortValue(
                    pItemRoot, tagKey, &nValue, 1, bReplace
                    );
                ///.InConcatenationTotalNumber
                nValue = 1;
                tagKey.group = 0x0020, tagKey.element = 0x9163;
                nError = dsTemp.setShortValue(
                    pItemRoot, tagKey, &nValue, 1, bReplace
                    );

                ///. store dataset info to DK, thread-safe???
                ///. will save in UID_LittleEndianExplicitTransferSyntax
                nTransferSyntaxID = 0;
                nError = dsTemp.save(strFileName.toUtf8(), nTransferSyntaxID);
                if (nError == 0)
                {
                    ///. store dataset info to DB, thread-safe???
                    nError = QtSqlPersistence::getInstance()->setDcmDataset(dsTemp);
                }

                ///.
                ///. PixelData
                tagKey.group = 0x7fe0, tagKey.element = 0x0010;
                nError = dsTemp.removeElement(pItemRoot, tagKey);
                QString strFileNameTemp(strFileName), strSOPInstanceUIDTemp(strSOPInstanceUID);
                for (int nIndexOfLevel=nLevelOfTotalImage-2; nIndexOfLevel>=0; --nIndexOfLevel)
                {
                    DataSet& dsLevel = dsTemp;
                    DcmItemHandle pLevelRoot = dsLevel.getRoot();
                    ///. ImageType: LOCALIZER, VOLUME, LABEL \\ NONE, RESAMPLED.
                    tagKey.group = 0x0008, tagKey.element = 0x0008;
                    nError = dsLevel.setStringValue( pLevelRoot, tagKey, "DERIVED\\PRIMARY\\LOCALIZER\\NONE", bReplace );
                    ///. Instance Number
                    lValue = nIndexOfLevel+1;
                    tagKey.group = 0x0020, tagKey.element = 0x0013;
                    nError = dsLevel.setLongValue( pLevelRoot, tagKey, &lValue, 1, bReplace );
                    //sop instance uid
                    strSOPInstanceUIDTemp = QString("%1.%2").arg(strSOPInstanceUID).arg(nIndexOfLevel+1);
                    tagKey.group = 0x0008, tagKey.element = 0x0018;
                    bFlag = dsLevel.setStringValue( pLevelRoot, tagKey, strSOPInstanceUIDTemp.toUtf8(), bReplace );
                    ///.ConcatenationUID
                    tagKey.group = 0x0020, tagKey.element = 0x9161;
                    nError = dsLevel.setStringValue( pLevelRoot, tagKey, strSOPInstanceUIDTemp.toUtf8(), bReplace );
                    ///.SOPInstanceUIDOfConcatenationSource
                    tagKey.group = 0x0020, tagKey.element = 0x0242;
                    nError = dsLevel.setStringValue( pLevelRoot, tagKey, strSOPInstanceUIDTemp.toUtf8(), bReplace );

                    ///.image data
                    sedeen::Size sizeDimensionOfLevel = imageAccessor.getDimensions(nIndexOfLevel);
                    const sedeen::image::ImageParser& imageParser = imageAccessor.imageParser();
                    sedeen::image::Color colorImage = imageParser.color();
                    sedeen::SizeF sizePixelInImage = imageParser.pixelSize();
                    sedeen::image::ImagePyramid* pImagePyramid = imageParser.pyramid().get();
                    sedeen::Size sizeDimensionOfTile = pImagePyramid->tileSize();
                    if (sizeDimensionOfTile.width() > sizeDimensionOfLevel.width())
                        nBufWidth = sizeDimensionOfLevel.width();
                    else
                        nBufWidth = sizeDimensionOfTile.width();
                    if (sizeDimensionOfTile.height() > sizeDimensionOfLevel.height())
                        nBufHeight = sizeDimensionOfLevel.height();
                    else
                        nBufHeight = sizeDimensionOfTile.height();
                    //int nNumOfFrmInX = sizeDimensionOfLevel.width()/sizeDimensionOfTile.width()
                    //    + (sizeDimensionOfLevel.width() % sizeDimensionOfTile.width()) ? 1 : 0;
                    //int nNumOfFrmInY = sizeDimensionOfLevel.height()/sizeDimensionOfTile.height()
                    //    + (sizeDimensionOfLevel.height() % sizeDimensionOfTile.height()) ? 1 : 0;
                    int nNumOfFrmInX = pImagePyramid->cols(nIndexOfLevel);
                    int nNumOfFrmInY = pImagePyramid->rows(nIndexOfLevel);
                    ///. Rows
                    nValue = nBufHeight;
                    tagKey.group = 0x0028, tagKey.element = 0x0010;
                    nError = dsLevel.setShortValue(pLevelRoot, tagKey, &nValue, 1, bReplace);
                    ///. Columns
                    nValue = nBufWidth;
                    tagKey.group = 0x0028, tagKey.element = 0x0011;
                    nError = dsLevel.setShortValue(pLevelRoot, tagKey, &nValue, 1, bReplace);
                    ///. PixelAspectRatio
                    tagKey.group = 0x0028, tagKey.element = 0x0034;
                    fPixelAspectRatio[0] = sizeDimensionOfTotalPixel.width() * sizePixelInTotalVol.width() / sizeDimensionOfLevel.width()/*nBufWidth / nNumOfFrmInX*/;
                    fPixelAspectRatio[1] = sizeDimensionOfTotalPixel.height() * sizePixelInTotalVol.height() / sizeDimensionOfLevel.height()/*nBufHeight / nNumOfFrmInY*/;
                    sprintf( strValue, "%f\\%f", fPixelAspectRatio[0], fPixelAspectRatio[1] );
                    nError = dsLevel.setStringValue(pLevelRoot, tagKey, strValue, bReplace);
                    //DCM_PixelSpacing
                    tagKey.group = 0x0028, tagKey.element = 0x0030;
                    nError = dsLevel.setStringValue(pLevelRoot, tagKey, strValue, bReplace);
/*                    ///. PixelData
                    tagKey.group = 0x7fe0, tagKey.element = 0x0010;
                    long lLenOfLevel(nBufWidth * nBufHeight * nBufBPP * nNumOfFrmInX * nNumOfFrmInY);
                    long lLenOfTile(nBufWidth * nBufHeight * nBufBPP);
//                    pBufImage = new char[lLenOfLevel];
//                    char* pBufTemp = pBufImage;
//                    memset(pBufImage,);
                    sedeen::Point pointTile(0, 0);
                    sedeen::Size sizeTile(nBufWidth, nBufHeight);
                    sedeen::Rect regionTile( pointTile, sizeTile );
                    for (int j=0; j<nNumOfFrmInY; ++j)
                    {
                        regionTile.setY( j*nBufHeight );
                        for (int i=0; i<nNumOfFrmInX; ++i)
                        {
                            regionTile.setX( i*nBufWidth );
                            sedeen::image::ThreadedRegionBlock blockTile(
                                imageAccessor, nIndexOfLevel, regionTile
                                );
                            sedeen::image::RawImage imageTile = blockTile.getImage();
                            const unsigned char *pBufTile = static_cast<const unsigned char*>( imageTile.data().get() );
                            memcpy( pBufTemp, pBufTile, lLenOfTile );
                            pBufTemp += lLenOfTile;
                        }
                        ///. end of for for (int i=0; i<nNumOfFrmInX; ++i)
                    }
                    ///. end of for (int j=0; j<nNumOfFrmInY; ++j)
#ifdef _DEBUG
                    FILE	*fp;
                    if ((fp=fopen(strFileName.toStdString().c_str(), "wb")) != NULL)
                    {
                        size_t lLenTemp = fwrite(pBufImage, sizeof(unsigned char), lLenOfLevel, fp);
                        fclose(fp);
                    }
#endif
                    nError = dsLevel.setShortValue(
                        pLevelRoot, tagKey, (short*)pBufImage, lLenOfLevel/2, bReplace
                        );
                    delete[] pBufImage;
                    pBufImage = NULL;
*/
                    ///. TotalPixelMatrixColumns
                    long lValue = sizeDimensionOfLevel.width();//static_cast<long>( nBufWidth * nNumOfFrmInX );
                    tagKey.group = 0x0048, tagKey.element = 0x0006;
                    nError = dsLevel.setLongValue(pItemRoot, tagKey, &lValue, 1, bReplace);
                    ///. TotalPixelMatrixRows
                    lValue = sizeDimensionOfLevel.height();//static_cast<long>( nBufHeight * nNumOfFrmInY );
                    tagKey.group = 0x0048, tagKey.element = 0x0007;
                    nError = dsLevel.setLongValue(pItemRoot, tagKey, &lValue, 1, bReplace);
                    ///. number of frames
                    nValue = nNumOfFrmInX*nNumOfFrmInY;
                    tagKey.group = 0x0028, tagKey.element = 0x0008;
                    nError = dsLevel.setShortValue( pLevelRoot, tagKey, &nValue, 1, bReplace );
                    ///. InConcatenationTotalNumber
                    nValue = nNumOfFrmInX*nNumOfFrmInY;
                    tagKey.group = 0x0020, tagKey.element = 0x9163;
                    nError = dsLevel.setShortValue( pLevelRoot, tagKey, &nValue, 1, bReplace );
                    ///. DCM_ReferencedImageNavigationSequence
                    if (pSeqOfNavigation && dsTemp.getNumOfItems(pSeqOfNavigation)>0)
                    {
                        const DcmItemHandle pSeqItem = dsTemp.getItemFromSeq(pSeqOfNavigation, lPos);
                        //DCM_PixelSpacing
                        tagKey.group = 0x0028, tagKey.element = 0x0030;
                        sprintf( strValue, "%f\\%f", fPixelAspectRatio[0], fPixelAspectRatio[1] );
                        nError = dsLevel.setStringValue(pSeqItem, tagKey, strValue, bReplace);
                    }

                    ///. store dataset info to DK, thread-safe???
                    strFileNameTemp = QString("%1.%2").arg(strFileName).arg(nIndexOfLevel+1);
                    nError = dsLevel.save(strFileNameTemp.toUtf8(), nTransferSyntaxID);
                    if (nError == 0)
                    {
                        nError = QtSqlPersistence::getInstance()->setDcmDataset(dsLevel);
                    }
                }
                ///. end of for (unsigned int nIndexOfLevel=0; nIndexOfLevel<nLevelOfTotalImage; ++nIndexOfLevel)
            }
            else
            {
                ///. store dataset info to DK, thread-safe???
                nError = dsTemp.save(strFileName.toStdString().c_str(), nTransferSyntaxID);
                if (nError == 0)
                {
                    ///. store dataset info to DB, thread-safe???
                    nError = QtSqlPersistence::getInstance()->setDcmDataset(dsTemp);
                }
            }
            ///. end of if (bUrlWSI == true)
            
            ///. remove temp dcm file
            dirTemp.remove( fileInfo.filePath().toStdString().c_str() );
            nCountOfFiles += 1;
        }
        else if (fileInfo.isDir()==true && bSearchSub==true)
        {
            nCountOfFiles += onSearch(fileInfo.filePath(), bSearchSub);
        }
        else
        {
        }
        ///. end of if (fileInfo.isFile() == true)
    }
    return nCountOfFiles;
}
