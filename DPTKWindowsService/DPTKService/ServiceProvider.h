#pragma once

#include <QtCore\qsemaphore.h>
#include <QtCore\qstring.h>
#include <QtCore\qdatetime.h>
#include <QtCore\qqueue.h>
///.
#include "DataSet.h"
#include "Service.h"

#include "Logger.h"

class CServiceProvider :
    public DPDcmObserver
{
public:
    struct DCM_SERVICE_INFO
    {
        /// session ID of current service
        unsigned short nSessionID;
	    /// the role of message sender 
	    TYPE_DCM_ROLE nTypeRole;
        ///...
        TYPE_DCM_MSG nMsgType;
        /// the status of current service
	    TYPE_DCM_STATUS nTypeStatus;
        /// SCU AE title
	    char pstrCallingAE[64];
        /// SCU IP address
	    char pstrCallingIP[64];
        /// DICOM dataset
        bool bDataSetPresent;
    };

public:
    CServiceProvider(void);
    virtual ~CServiceProvider(void);
    
    int start(const char* pstrHostAE, int nPortNum, int nPeerNum, bool bTLS, const char* pstrWorkingDir);
    void stop();

    bool isWorking();
    inline void setVerbose(bool bVerbose)
    {
        m_bVerbose = bVerbose;
    }
    //inline void setSearchingDir(const char* pstrSearchDir)
    //{
    //    m_strSearchingDir = pstrSearchDir;
    //}

    bool lastServiceInfo(DCM_SERVICE_INFO& infoService)const;
    void removeLastServiceInfo();

private:
    ///.
    Service m_DPDcmSCP;
    TLogger m_logger;
    bool m_bVerbose, m_bPatientAnonymized;

    ///.
    const int m_nNumOfSemaphoreResources, m_nNumOfAcquireResources, m_nAcqTimeoutInMS;
    mutable QSemaphore m_semaphoreDcmSCP;
    ///.
    mutable QQueue< DCM_SERVICE_INFO* > m_queueServiceInfo;

    //QString m_strSearchingDir;
//    const QString m_strLevelStudy, m_strLevelSeries, m_strLevelImage;

    ////////////////////////////////////////////////////////////////////////

    ///. virtural function of DICOM observer
    int notify(TYPE_DCM_MSG nType, DPDcmMsg& msgDPDcm) const;
    ///. DICOM messages
    ///. DCM C Requests
	void onUnknownRequest( TYPE_DCM_MSG nType, DPDcmMsg& msgParam )const;
	void onAssociateRequest( DPDcmMsg& msgParam )const;
	void onAssociateAccept( DPDcmMsg& msgParam )const;
	void onAssociateReject( DPDcmMsg& msgParam )const;
	void onAssociateRelease( DPDcmMsg& msgParam )const;
	void onCEchoRequest( DPDcmMsg& msgParam )const;
	void onCStoreRequest( DPDcmMsg& msgParam )const;
	void onCFindRequest( DPDcmMsg& msgParam )const;
    void onCGetRequest( DPDcmMsg& msgParam )const;
    void onCMoveRequest( DPDcmMsg& msgParam )const;
	void onCancelRequest( DPDcmMsg& msgParam )const;
    void onAbortRequest( DPDcmMsg& msgParam )const;
    ///. DCM N Requests
//    void onNGetRequest( DPDcmMsg& msgParam )const;
//    void onNActionRequest( DPDcmMsg& msgParam )const;
    ///. JPIP Requests
    void onGenerateLocalizer( DPDcmMsg& msgParam )const;
    void onDeleteFile(DPDcmMsg& msgParam)const;

    ///. dead functions
    bool getLastServiceInfo(DCM_SERVICE_INFO& infoService) const;
};

