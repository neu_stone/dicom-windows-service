// DPTKService.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <stdio.h>
#include <tchar.h>

#include <windows.h>
#include <strsafe.h>
#include "Winsvc.h"

#include "Logger.h"
#include "ServiceDcmSCP.h"

#pragma comment(lib, "advapi32.lib")

///////////////////////////////////////////////////////////////////

#define SERVICE_NAME TEXT("DPTK.DICOM.SCP")

//TLogger logDPTKService;

CServiceDcmSCP g_svcDPTK(SERVICE_NAME, SERVICE_NAME);

VOID WINAPI ServiceMain ( DWORD, LPTSTR * ); 

///////////////////////////////////////////////////////////////////////

int _tmain(int argc, _TCHAR* argv[])
{
    int nError(0);
    // If command-line parameter is "install", install the service. 
    // Otherwise, the service is probably being started by the SCM.
    if ( argc > 1 ) 
    {
        if ((*argv[1] == L'-' || *argv[1] == L'/') &&
            _wcsicmp(L"install", argv[1] + 1) == 0 &&
            g_svcDPTK.getServiceState() < 0)
        {
            // Install the service when the command is "-install" or "/install". 
            nError = g_svcDPTK.installService();
        }
        else if ((*argv[1] == L'-' || *argv[1] == L'/') &&
            _wcsicmp(L"remove", argv[1] + 1) == 0 &&
            g_svcDPTK.getServiceState()&SERVICE_STOPPED == true)
        {
            // Uninstall the service when the command is "-remove" or "/remove".
            g_svcDPTK.uninstallService();
        }
        else if ((*argv[1] == L'-' || *argv[1] == L'/') &&
            _wcsicmp(L"start", argv[1] + 1) == 0 &&
            g_svcDPTK.getServiceState()&SERVICE_STOPPED == true)
        {
            // start the service when the command is "-start" or "/start".
            nError = g_svcDPTK.startService();
        }
        else if ((*argv[1] == L'-' || *argv[1] == L'/') &&
            _wcsicmp(L"stop", argv[1] + 1) == 0 &&
            g_svcDPTK.getServiceState()&SERVICE_RUNNING == true)
        {
            // stop the service when the command is "-sto" or "/stop".
            g_svcDPTK.stopService();
        }
        else if ((*argv[1] == L'-' || *argv[1] == L'/') &&
            _wcsicmp(L"test", argv[1] + 1) == 0)
        {
            g_svcDPTK.execute(false);
        }
        else
        {
            g_svcDPTK.execute(false);
            nError = -1;
        }
    } 
    else 
    {
        // TO_DO: Add any additional services for the process to this table.
        SERVICE_TABLE_ENTRY DispatchTable[] = 
        { 
            { SERVICE_NAME, (LPSERVICE_MAIN_FUNCTION) ServiceMain }, 
            { NULL, NULL } 
        }; 
 
        // This call returns when the service has stopped. 
        // The process should simply terminate when the call returns.
        if (StartServiceCtrlDispatcher (DispatchTable) == FALSE)
        {
            DWORD lastError = GetLastError();
            switch (lastError)
            {
            case ERROR_FAILED_SERVICE_CONTROLLER_CONNECT:
                /*
                This error is returned if the program is being run as a console application rather than as a service.
                If the program will be run as a console application for debugging purposes, structure it such that 
                service-specific code is not called when this error is returned.
                */
                g_svcDPTK.reportServiceEvent(L" ERROR_FAILED_SERVICE_CONTROLLER_CONNECT.\n"); 
                break;
            case ERROR_INVALID_DATA:
                g_svcDPTK.reportServiceEvent(L" ERROR_INVALID_DATA.\n"); 
                break;
            case ERROR_SERVICE_ALREADY_RUNNING:
                g_svcDPTK.reportServiceEvent(L" ERROR_SERVICE_ALREADY_RUNNING.\n"); 
                break;
            default:
                g_svcDPTK.reportServiceEvent(L" UNKNOWN.\n"); 
                break;
            }
        }
        else
        {
//            logDPTKService.writeInfoLog("StartServiceCtrlDispatcher was TRUE");
        }
        ///. end of if (StartServiceCtrlDispatcher (ServiceTable) == FALSE)
    } 
    ///. end of if ( argc > 1 )

    return nError;
}

//
// Purpose: 
//   Entry point for the service
//
// Parameters:
//   dwArgc - Number of arguments in the lpszArgv array
//   lpszArgv - Array of strings. The first string is the name of
//     the service and subsequent strings are passed by the process
//     that called the StartService function to start the service.
// 
// Return value:
//   None.
//
VOID WINAPI ServiceMain( DWORD dwArgc, LPTSTR *lpszArgv )
{
    g_svcDPTK.execute(true);
}
