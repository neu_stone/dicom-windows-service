#pragma once
#include <QtCore\qstring.h>
#include <QtCore\qsemaphore.h>

#include "ServiceBase.h"
#include "ServiceProvider.h"

class CServiceDcmSCP :
    public CServiceBase, DPDcmObserver
{
    friend class QtThreadBroadcasting;

public:

    CServiceDcmSCP(const LPCWSTR pszServiceName, const LPCWSTR pszDisplayName);
    virtual ~CServiceDcmSCP(void);

private:
    ///.
    const QString m_strSettingsINI, m_strGroupDicom, m_strGroupDatabase, m_strGroupUtility;
    QString /*m_strSearchingDir, */m_strWorkingDir;
    bool m_bVerbose;
    ///.
    const int m_nNumOfSemaphoreResources, m_nNumOfAcquireResources, m_nAcqTimeoutInMS;
    mutable QSemaphore m_semaphoreDcmSCP;

    ///. this is the Dcm SCP talking with remote controller
    Service m_serviceController;
    QString m_strControllerAE, m_strControllerIP, m_strControllerPort, m_strControllerPeers;
    bool m_bControllerTSL;
    ///. this is the Dcm SCP providing PACS service
    CServiceProvider m_serviceProvider;
    QString m_strProviderAE, m_strProviderIP, m_strProviderPort, m_strProviderPeers;
    bool m_bProviderTSL;
    ///.
    QString m_strPathcoreRootUID;

    /////.
    //enum THREAD_STATE
    //{
    //    ThreadState_Idle=0,
    //    ThreadState_Initial,
    //    ThreadState_Running,
    //};
    //QtThreadBroadcasting* m_pQtThreadBroadcasting;
    //THREAD_STATE m_nQtThreadBroadcasting;
    //unsigned long m_lTimeoutInMS;

    /////////////////////////////////////////////////
//    void onBroadcasting();

    ///... virtual functions
    void onStartService();
    void onService();
    void onStopService();
    ///.
    int notify(TYPE_DCM_MSG nType, DPDcmMsg& msgDPDcm) const;
    ///. virtual functions
    void onAssociateRequest( DPDcmMsg& msgParam );
	void onAssociateAccept( DPDcmMsg& msgParam );
	void onAssociateReject( DPDcmMsg& msgParam );
    void onAssociateRelease( DPDcmMsg& msgParam );
	//void onCancelRequest(const DPDcmMsg& msgParam)const;
    //void onAbortRequest(const DPDcmMsg& msgParam)const;
	void onCEchoRequest( DPDcmMsg& msgParam );
	void onCFindRequest( DPDcmMsg& msgParam );
    ///.
    void onNEventResponse( DPDcmMsg& msgParam );
    void onNGetRequest( DPDcmMsg& msgParam );
    void onNSetRequest( DPDcmMsg& msgParam );
    void onNActionRequest( TYPE_DCM_MSG nType, DPDcmMsg& msgParam );
    ///.
    void onUnknownRequest( TYPE_DCM_MSG nType, DPDcmMsg& msgParam );
};
//
//class QtThreadBroadcasting : public QThread
//{
//public:
//	void attach(CServiceDcmSCP* pServiceController);
//
//private:
//    CServiceDcmSCP* m_pServiceController;
//
//    ///.
//    void run();
//};
