//#include "stdafx.h"
#include "Logger.h"

#include <fstream>
#include <iostream>
#include <ctime>

TLogger::TLogger()
:m_bInitialized(false)
,m_nMaxTextUnit(10)
,m_strLogPath("")
,m_vectorLogStrings()
{
}

TLogger::~TLogger()
{
	m_vectorLogStrings.clear();
}

bool TLogger::initialize(const char* logfilename)
{
	m_strLogPath = logfilename==NULL ? "Log.txt" : logfilename;
	try
	{
		ofstream logFile(m_strLogPath.c_str(), ofstream::out | ofstream::app);
        if (logFile.good() == true)
        {
            char strTemp[256];
            logFile << endl << logfilename << endl;

            // current date/time based on current system
            time_t now = time(0);
            // convert now to string form
            ctime_s(strTemp, 256, &now);
            logFile << strTemp << endl;

           //cout << "The local date and time is: " << dt << endl;

           //// convert now to tm struct for UTC
           //tm *gmtm = gmtime(&now);
           //dt = asctime(gmtm);
           //cout << "The UTC date and time is:"<< dt << endl;
           ////            logFile.g
            logFile.close();
		    m_bInitialized = true;
        }
	}
	catch (...)//CException* e
	{
		//TCHAR szCause[255];
		//e->GetErrorMessage(szCause, 255);
		//AfxMessageBox(szCause);
		//e->Delete();
		m_bInitialized = false;
		return m_bInitialized;
	}
	return m_bInitialized;
}

void TLogger::flush()
{
	if(m_strLogPath.length()>0 && m_vectorLogStrings.size()>0)
	{
		ofstream logFile(m_strLogPath.c_str(), ios::app);

		for(size_t t = 0; t < m_vectorLogStrings.size(); t++)
		{
			logFile << m_vectorLogStrings[t];
		}
		m_vectorLogStrings.clear();
		logFile.close();
	}
}

void TLogger::writeImmidiateInfoLog(const char* strInfo)
{
	if(strInfo!=NULL)
	{
		m_vectorLogStrings.push_back(string("<+>") + string(strInfo) + "\n");
		flush();
	}
}

void TLogger::writeInfoLog(const char* strInfo)
{
	m_vectorLogStrings.push_back(string("<+>") + string(strInfo) + "\n");
	if (m_vectorLogStrings.size() >= m_nMaxTextUnit)
	{
		flush();
	}
}

void TLogger::writeErrorLog(const char* strInfo)
{
	if(strInfo!=NULL)
	{
		m_vectorLogStrings.push_back(string("<!>") + string(strInfo) + "\n");
		flush();
	}
}

void TLogger::writeFatalErrorLog(const char* strInfo)
{
	if(strInfo!=NULL)
	{
		m_vectorLogStrings.push_back(string("<X>") + string(strInfo) + "\n");

		flush();
	}
	exit(1);
}