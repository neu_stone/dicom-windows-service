//#include "StdAfx.h"

#include "ServiceBase.h"

namespace
{
//    ///. this event is used to provide a control for Service Manager
//    HANDLE eventServiceControl = INVALID_HANDLE_VALUE;
    // Internal name of the service 
    const LPCTSTR pszServiceEvent = TEXT("DPTK.DICOM.SCP.SERVICE");
    const int SVC_ERROR = 0;
//#define SERVICE_NAME             L"CppWindowsService" 
// Displayed name of the service 
//#define SERVICE_DISPLAY_NAME     L"CppWindowsService Sample Service" 

// Service start options. 
//#define SERVICE_START_TYPE       SERVICE_DEMAND_START 
 
// List of service dependencies - "dep1\0dep2\0\0" 
//#define SERVICE_DEPENDENCIES     L"" 
 
// The name of the account under which the service should run 
//#define SERVICE_ACCOUNT          L"NT AUTHORITY\\LocalService" 
 
// The password to the service account name 
//#define SERVICE_PASSWORD         NULL 
    int nArgc = 0;
}

//HANDLE CServiceBase::g_ServiceStopEvent = INVALID_HANDLE_VALUE;
SERVICE_STATUS CServiceBase::g_ServiceStatus;

/////////////////////////////////////////////////////////////////////////////////

void QtThreadWindowsService::attach(CServiceBase* pServiceBase)
{
	m_pServiceBase = pServiceBase;
}

void QtThreadWindowsService::run()
{
    if (m_pServiceBase != NULL)
    {
        if (m_pServiceBase->m_bWindowsService == true)
        {
            m_pServiceBase->service();
        }
        else
        {
            m_pServiceBase->simulate();
        }
    }
    else
    {
        Sleep(m_pServiceBase->m_lTimeoutInMS);
    }
    m_pServiceBase->quit();
}

/////////////////////////////////////////////////////////////////////////////////

CServiceBase::CServiceBase(const LPCWSTR pszServiceName, const LPCWSTR pszDisplayName)
    : QCoreApplication( nArgc, NULL)
    , m_strServiceName(pszServiceName)
    , m_strDisplayName(pszDisplayName)
    , m_strDPTKServiceExe()
    , m_eventServiceStop(INVALID_HANDLE_VALUE)
    , m_nServiceStatus(ServiceBase_State_Idle)
    , m_lTimeoutInMS(100)
    , g_ServiceStatusHandle(NULL)
    //, g_ServiceStopEvent(INVALID_HANDLE_VALUE)
    //, g_ServiceStatus()
    , m_logger()
    , m_pQtThreadWS(NULL)
    , m_nQtThreadWS(ThreadState_Idle)
    , m_bWindowsService(false)
{
//    ///.
//    m_logger.initialize("C:\\03.Temp\\DPTK.DICOM.SCP");
}

CServiceBase::~CServiceBase(void)
{
//    m_logger.flush();
    if (m_pQtThreadWS != NULL)
    {
        if (m_nQtThreadWS != ThreadState_Idle)
        {
            m_nQtThreadWS = ThreadState_Idle;
            m_pQtThreadWS->terminate();
            m_pQtThreadWS->wait(m_lTimeoutInMS);
        }
        delete m_pQtThreadWS;
        m_pQtThreadWS = NULL;
    }
}

int CServiceBase::installService()
{
    SC_HANDLE schSCManager;
    SC_HANDLE schService;
    int nError(-1);
    try
    {
        if (m_strDPTKServiceExe.length() < 1)
        {
            TCHAR szPath[MAX_PATH];
            if( !GetModuleFileName( NULL, szPath, MAX_PATH ) )
            {
                if (m_logger.isInitialized() == true)
                {
                    char strTemp[MAX_PATH];
			        sprintf_s(
				        strTemp, "Cannot get Module FileName : error#%d", GetLastError()
				        );
                    m_logger.writeErrorLog(strTemp);
                }
            }
            else
            {
                m_strDPTKServiceExe = szPath;
            }
        }

        if (m_strDPTKServiceExe.length() > 0)
        {
            char strTemp[MAX_PATH];
            /*// Get a handle to the SCM database. 
            it did work for me when an account  is member of domain admins group. 
            Win2k8 seems to be case sensitive on the service names where as win2k3 is not. 
            make sure you get the service name correctly.
            */
            schSCManager = OpenSCManager(
                NULL,                   // local computer
                NULL,                   // ServicesActive database 
                SC_MANAGER_ALL_ACCESS   // full access rights 
                );
            if (NULL == schSCManager) 
            {
                if (m_logger.isInitialized() == true)
                {
                    sprintf_s(
				        strTemp, "OpenSCManager failed : error#%d", GetLastError()
				        );
                    m_logger.writeErrorLog(strTemp);
                }
            }
            else
            {
                // Create the service
                schService = CreateService( 
                    schSCManager,               // SCM database 
                    m_strServiceName.c_str(),   // name of service 
                    m_strDisplayName.c_str(),   // service name to display 
                    SERVICE_ALL_ACCESS,         // desired access 
                    SERVICE_WIN32_OWN_PROCESS,  // service type 
                    SERVICE_DEMAND_START,       // Service start type 
                    SERVICE_ERROR_NORMAL,       // error control type 
                    m_strDPTKServiceExe.c_str(),// path to service's binary 
                    NULL,       // no load ordering group 
                    NULL,       // no tag identifier 
                    NULL,       // no dependencies 
                    NULL,       // Service running account : LocalSystem account.
                    NULL        // Password of the account : no password 
                    );
                if (schService == NULL) 
                {
                    if (m_logger.isInitialized() == true)
                    {
			            sprintf_s(
				            strTemp, "CreateService failed : error#%d", GetLastError()
				            );
                        m_logger.writeErrorLog(strTemp);
                    }
                }
                else
                {
                    if (m_logger.isInitialized() == true)
                    {
                        //std::string sTemp((const char*)(m_strDPTKServiceExe.c_str()), sizeof(wchar_t)/sizeof(char)*m_strDPTKServiceExe.size());
                        string sTemp(m_strDPTKServiceExe.begin(), m_strDPTKServiceExe.end());
                        sTemp.assign(m_strDPTKServiceExe.begin(), m_strDPTKServiceExe.end());
			            sprintf_s(
                            strTemp, "Service installed : %s", sTemp.c_str()
				            );
                        m_logger.writeErrorLog(strTemp);
                    }
                    nError = 0;
                    CloseServiceHandle(schService); 
                }
                ///. end of if (schService == NULL)

                CloseServiceHandle(schSCManager);
            }
            /// end of if (NULL == schSCManager)
        }
        else
        {
            nError = -1;
        }
        ///. end of if( !GetModuleFileName( NULL, szPath, MAX_PATH ) )
    }
    catch (...)
    {
        if (schService)
            CloseServiceHandle(schService);
        if (schSCManager)
            CloseServiceHandle(schSCManager);
        return -1;
    }
    return nError;
}

void CServiceBase::uninstallService()
{
    SC_HANDLE schSCManager;
    SC_HANDLE schService;
    int nError(0);
//    SERVICE_STATUS ssStatus; 
    try
    {
        // Get a handle to the SCM database. 
        schSCManager = OpenSCManager( 
            NULL,                   // local computer
            NULL,                   // ServicesActive database 
            SC_MANAGER_ALL_ACCESS   // full access rights 
            );
        if (NULL == schSCManager) 
        {
            if (m_logger.isInitialized() == true)
            {
                char strTemp[MAX_PATH];
			    sprintf_s(
				    strTemp, "OpenSCManager failed : error#%d", GetLastError()
				    );
                m_logger.writeErrorLog(strTemp);
            }
        }
        else
        {
            // Get a handle to the service.
            schService = OpenService( 
                schSCManager,       // SCM database 
                m_strServiceName.c_str(),     // name of service 
                DELETE              // need delete access 
                );
            if (schService == NULL)
            { 
                if (m_logger.isInitialized() == true)
                {
                    char strTemp[MAX_PATH];
			        sprintf_s(
				        strTemp, "CreateService failed : error#%d", GetLastError()
				        );
                    m_logger.writeErrorLog(strTemp);
                }
            }
            else
            {
                // Delete the service. 
                if (! DeleteService(schService) ) 
                {
                    nError = GetLastError();
                }
                else
                {
                    nError = 0;
                }
                ///. end of if (! DeleteService(schService) )
                if (m_logger.isInitialized() == true)
                {
                    char strTemp[MAX_PATH];
			        sprintf_s(
				        strTemp, "DeleteService : error#%d", nError
				        );
                    m_logger.writeErrorLog(strTemp);
                }

                CloseServiceHandle(schService); 
            }
            ///. end of if (schService == NULL)

            CloseServiceHandle(schSCManager);
        }
        /// end of if (NULL == schSCManager)
    }
    catch (...)
    {
        if (schService)
            CloseServiceHandle(schService);
        if (schSCManager)
            CloseServiceHandle(schSCManager);
        return ;
    }
    return ;
}

int CServiceBase::getServiceState() const
{
    SC_HANDLE schSCManager(NULL);
    SC_HANDLE schService(NULL);
    int nError(-1);
    try
    {
        // Get a handle to the SCM database. 
        schSCManager = OpenSCManager( 
            NULL,                   // local computer
            NULL,                   // ServicesActive database 
            SC_MANAGER_ALL_ACCESS   // full access rights 
            );
        if (NULL != schSCManager) 
        {
            // Get a handle to the service.
            schService = OpenService( 
                schSCManager,               // SCM database 
                m_strServiceName.c_str(),   // name of service 
                SERVICE_ALL_ACCESS          // full access
                );
            if (schService != NULL)
            { 
                nError = 0; 
                SERVICE_STATUS_PROCESS ssStatus; 
                DWORD dwBytesNeeded;
                // Check the status in case the service is not stopped. 
                if (QueryServiceStatusEx(
                        schService, SC_STATUS_PROCESS_INFO, (LPBYTE) &ssStatus, sizeof(SERVICE_STATUS_PROCESS),  &dwBytesNeeded
                        ) == FALSE)
                {
                    printf("QueryServiceStatusEx failed (%d)\n", GetLastError());
                }
                else
                {
                    nError |= ssStatus.dwCurrentState;
                }
                CloseServiceHandle(schService);
            }
            else
            {
//                printf("OpenService failed (%d)\n", GetLastError());
            }
            ///. end of if (schService != NULL)
            CloseServiceHandle(schSCManager);
        }
        else
        {
            printf("OpenSCManager failed (%d)\n", GetLastError());
        }
        ///. end of if (NULL != schSCManager)
    }
    catch (...)
    {
        if (schService)
            CloseServiceHandle(schService);
        if (schSCManager)
            CloseServiceHandle(schSCManager);
        return -1;
    }
    return nError;
}

int CServiceBase::startService()
{
    SC_HANDLE schSCManager;
    SC_HANDLE schService;

    SERVICE_STATUS_PROCESS ssStatus; 
    DWORD dwOldCheckPoint; 
    DWORD dwStartTickCount;
    DWORD dwWaitTime;
    DWORD dwBytesNeeded;

    int nError(0);
    try
    {

        // Get a handle to the SCM database. 
         schSCManager = OpenSCManager( 
            NULL,                    // local computer
            NULL,                    // servicesActive database 
            SC_MANAGER_ALL_ACCESS);  // full access rights 
        if (NULL == schSCManager) 
        {
            nError = GetLastError();
            printf("OpenSCManager failed (%d)\n", nError);
            throw "OpenSCManager";
        }

        // Get a handle to the service.
        schService = OpenService( 
            schSCManager,         // SCM database 
            m_strServiceName.c_str(),            // name of service 
            SERVICE_ALL_ACCESS);  // full access 
        if (schService == NULL)
        { 
            nError = GetLastError();
            printf("OpenService failed (%d)\n", nError);
            throw "OpenService";
        }    

        // Check the status in case the service is not stopped. 
        if (!QueryServiceStatusEx( 
                schService,                     // handle to service 
                SC_STATUS_PROCESS_INFO,         // information level
                (LPBYTE) &ssStatus,             // address of structure
                sizeof(SERVICE_STATUS_PROCESS), // size of structure
                &dwBytesNeeded ) )              // size needed if buffer is too small
        {
            nError = GetLastError();
            printf("QueryServiceStatusEx failed (%d)\n", nError);
            throw "QueryServiceStatusEx";
        }

        // Check if the service is already running. It would be possible 
        // to stop the service here, but for simplicity this example just returns. 
        if(ssStatus.dwCurrentState != SERVICE_STOPPED && ssStatus.dwCurrentState != SERVICE_STOP_PENDING)
        {
            printf("Cannot start the service because it is already running\n");
            throw "Cannot start the service because it is already running";
        }

        // Save the tick count and initial checkpoint.
        dwStartTickCount = GetTickCount();
        dwOldCheckPoint = ssStatus.dwCheckPoint;

        // Wait for the service to stop before attempting to start it.
        while (ssStatus.dwCurrentState == SERVICE_STOP_PENDING)
        {
            // Do not wait longer than the wait hint. A good interval is 
            // one-tenth of the wait hint but not less than 1 second  
            // and not more than 10 seconds. 
 
            dwWaitTime = ssStatus.dwWaitHint / 10;

            if( dwWaitTime < 1000 )
                dwWaitTime = 1000;
            else if ( dwWaitTime > 10000 )
                dwWaitTime = 10000;

            Sleep( dwWaitTime );

            // Check the status until the service is no longer stop pending. 
            if (!QueryServiceStatusEx( 
                    schService,                     // handle to service 
                    SC_STATUS_PROCESS_INFO,         // information level
                    (LPBYTE) &ssStatus,             // address of structure
                    sizeof(SERVICE_STATUS_PROCESS), // size of structure
                    &dwBytesNeeded ) )              // size needed if buffer is too small
            {
                nError = GetLastError();
                printf("QueryServiceStatusEx failed (%d)\n", nError);
                throw "QueryServiceStatusEx";
            }

            if ( ssStatus.dwCheckPoint > dwOldCheckPoint )
            {
                // Continue to wait and check.
                dwStartTickCount = GetTickCount();
                dwOldCheckPoint = ssStatus.dwCheckPoint;
            }
            else
            {
                if(GetTickCount()-dwStartTickCount > ssStatus.dwWaitHint)
                {
                    printf("Timeout waiting for service to stop\n");
                    throw "Timeout waiting for service to stop";
                }
            }
        }

        // Attempt to start the service.
        if (StartService(
                schService/*handle to service*/, 0/*number of arguments*/, NULL/*no arguments*/
                ) ==  FALSE)      //  
        {
            nError = GetLastError();
            printf("StartService failed (%d)\n", nError);
            throw "StartService";
        }
        else
        {
            printf("Service start pending...\n"); 
        }

        // Check the status until the service is no longer start pending. 
        if (!QueryServiceStatusEx( 
                schService,                     // handle to service 
                SC_STATUS_PROCESS_INFO,         // info level
                (LPBYTE) &ssStatus,             // address of structure
                sizeof(SERVICE_STATUS_PROCESS), // size of structure
                &dwBytesNeeded ) )              // if buffer too small
        {
            nError = GetLastError();
            printf("QueryServiceStatusEx failed (%d)\n", nError);
            throw "QueryServiceStatusEx";
        }
 
        // Save the tick count and initial checkpoint.
        dwStartTickCount = GetTickCount();
        dwOldCheckPoint = ssStatus.dwCheckPoint;
        while (ssStatus.dwCurrentState == SERVICE_START_PENDING) 
        { 
            // Do not wait longer than the wait hint. A good interval is 
            // one-tenth the wait hint, but no less than 1 second and no 
            // more than 10 seconds. 
 
            dwWaitTime = ssStatus.dwWaitHint / 10;

            if( dwWaitTime < 1000 )
                dwWaitTime = 1000;
            else if ( dwWaitTime > 10000 )
                dwWaitTime = 10000;

            Sleep( dwWaitTime );

            // Check the status again. 
 
            if (!QueryServiceStatusEx( 
                schService,             // handle to service 
                SC_STATUS_PROCESS_INFO, // info level
                (LPBYTE) &ssStatus,             // address of structure
                sizeof(SERVICE_STATUS_PROCESS), // size of structure
                &dwBytesNeeded ) )              // if buffer too small
            {
                printf("QueryServiceStatusEx failed (%d)\n", GetLastError());
                break; 
            }
 
            if ( ssStatus.dwCheckPoint > dwOldCheckPoint )
            {
                // Continue to wait and check.

                dwStartTickCount = GetTickCount();
                dwOldCheckPoint = ssStatus.dwCheckPoint;
            }
            else
            {
                if(GetTickCount()-dwStartTickCount > ssStatus.dwWaitHint)
                {
                    // No progress made within the wait hint.
                    break;
                }
            }
        } 

        // Determine whether the service is running.
        if (ssStatus.dwCurrentState == SERVICE_RUNNING) 
        {
            printf("Service started successfully.\n"); 
        }
        else 
        { 
            printf("Service not started. \n");
            printf("  Current State: %d\n", ssStatus.dwCurrentState); 
            printf("  Exit Code: %d\n", ssStatus.dwWin32ExitCode); 
            printf("  Check Point: %d\n", ssStatus.dwCheckPoint); 
            printf("  Wait Hint: %d\n", ssStatus.dwWaitHint); 
        } 

        if (schService)
            CloseServiceHandle(schService);
        if (schSCManager)
            CloseServiceHandle(schSCManager);
    }
    catch (...)
    {
        if (schService)
            CloseServiceHandle(schService);
        if (schSCManager)
            CloseServiceHandle(schSCManager);
        return -1;
    }
    return nError;
}

void CServiceBase::stopService()
{
    SC_HANDLE schSCManager;
    SC_HANDLE schService;

    SERVICE_STATUS_PROCESS ssp;
    DWORD dwStartTime = GetTickCount();
    DWORD dwBytesNeeded;
    DWORD dwTimeout = 30000; // 30-second time-out
    DWORD dwWaitTime;

    int nError(0);
    try
    {

        // Get a handle to the SCM database. 
         schSCManager = OpenSCManager( 
            NULL,                    // local computer
            NULL,                    // servicesActive database 
            SC_MANAGER_ALL_ACCESS);  // full access rights 
        if (NULL == schSCManager) 
        {
            nError = GetLastError();
            printf("OpenSCManager failed (%d)\n", nError);
            throw "OpenSCManager";
        }

        // Get a handle to the service.
        schService = OpenService( 
            schSCManager,         // SCM database 
            m_strServiceName.c_str(),            // name of service 
            SERVICE_ALL_ACCESS);  // full access 
        if (schService == NULL)
        { 
            nError = GetLastError();
            printf("OpenService failed (%d)\n", nError);
            throw "OpenService";
        }    

        // Make sure the service is not already stopped.
        if ( !QueryServiceStatusEx( 
                schService, 
                SC_STATUS_PROCESS_INFO,
                (LPBYTE)&ssp, 
                sizeof(SERVICE_STATUS_PROCESS),
                &dwBytesNeeded ) )
        {
            nError = GetLastError();
            printf("QueryServiceStatusEx failed (%d)\n", nError);
            throw "QueryServiceStatusEx";
        }
        if ( ssp.dwCurrentState == SERVICE_STOPPED )
        {
            printf("Service is already stopped.\n");
            throw "Service is already stopped.";
        }

        // If a stop is pending, wait for it.
        while ( ssp.dwCurrentState == SERVICE_STOP_PENDING ) 
        {
            printf("Service stop pending...\n");

            // Do not wait longer than the wait hint. A good interval is 
            // one-tenth of the wait hint but not less than 1 second  
            // and not more than 10 seconds. 
 
            dwWaitTime = ssp.dwWaitHint / 10;

            if( dwWaitTime < 1000 )
                dwWaitTime = 1000;
            else if ( dwWaitTime > 10000 )
                dwWaitTime = 10000;

            Sleep( dwWaitTime );

            if ( !QueryServiceStatusEx( 
                     schService, 
                     SC_STATUS_PROCESS_INFO,
                     (LPBYTE)&ssp, 
                     sizeof(SERVICE_STATUS_PROCESS),
                     &dwBytesNeeded ) )
            {
                nError = GetLastError();
                printf("QueryServiceStatusEx failed (%d)\n", nError);
                throw "QueryServiceStatusEx";
            }

            if ( ssp.dwCurrentState == SERVICE_STOPPED )
            {
                printf("Service stopped successfully.\n");
                throw "Service stopped successfully";
            }

            if ( GetTickCount() - dwStartTime > dwTimeout )
            {
                printf("Service stop timed out.\n");
                throw "Service stop timed out.";
            }
        }

        // If the service is running, dependencies must be stopped first.

        stopDependentServices(schSCManager, schService);

        // Send a stop code to the service.
        if ( !ControlService( 
                schService, 
                SERVICE_CONTROL_STOP, 
                (LPSERVICE_STATUS) &ssp ) )
        {
            nError = GetLastError();
            printf("ControlService failed (%d)\n", nError);
            throw "ControlService";
        }

        // Wait for the service to stop.
        while ( ssp.dwCurrentState != SERVICE_STOPPED ) 
        {
            Sleep( ssp.dwWaitHint );
            if ( !QueryServiceStatusEx( 
                    schService, 
                    SC_STATUS_PROCESS_INFO,
                    (LPBYTE)&ssp, 
                    sizeof(SERVICE_STATUS_PROCESS),
                    &dwBytesNeeded ) )
            {
                nError = GetLastError();
                printf("QueryServiceStatusEx failed (%d)\n", nError);
                throw "QueryServiceStatusEx";
            }

            if ( ssp.dwCurrentState == SERVICE_STOPPED )
                break;

            if ( GetTickCount() - dwStartTime > dwTimeout )
            {
                printf( "Wait timed out\n" );
                throw "Wait timed out";
            }
        }
        printf("Service stopped successfully\n");

        if (schService)
            CloseServiceHandle(schService);
        if (schSCManager)
            CloseServiceHandle(schSCManager);
    }
    catch (...)
    {
        if (schService)
            CloseServiceHandle(schService);
        if (schSCManager)
            CloseServiceHandle(schSCManager);
        return ;
    }
    return ;
}

VOID __stdcall CServiceBase::disableService(const LPCWSTR pszServiceName)
{
    SC_HANDLE schSCManager(NULL);
    SC_HANDLE schService(NULL);
    try
    {
        // Get a handle to the SCM database. 
        schSCManager = OpenSCManager( 
            NULL,                    // local computer
            NULL,                    // ServicesActive database 
            SC_MANAGER_ALL_ACCESS  // full access rights 
            );
        if (NULL == schSCManager) 
        {
            printf("OpenSCManager failed (%d)\n", GetLastError());
            return;
        }

        // Get a handle to the service.
        schService = OpenService( 
            schSCManager,            // SCM database 
            pszServiceName,               // name of service 
            SERVICE_CHANGE_CONFIG);  // need change config access 
 
        if (schService == NULL)
        { 
            printf("OpenService failed (%d)\n", GetLastError()); 
            CloseServiceHandle(schSCManager);
            return;
        }    

        // Change the service start type.

        if (! ChangeServiceConfig( 
            schService,        // handle of service 
            SERVICE_NO_CHANGE, // service type: no change 
            SERVICE_DISABLED,  // service start type 
            SERVICE_NO_CHANGE, // error control: no change 
            NULL,              // binary path: no change 
            NULL,              // load order group: no change 
            NULL,              // tag ID: no change 
            NULL,              // dependencies: no change 
            NULL,              // account name: no change 
            NULL,              // password: no change 
            NULL) )            // display name: no change
        {
            printf("ChangeServiceConfig failed (%d)\n", GetLastError()); 
        }
        else printf("Service disabled successfully.\n"); 

        CloseServiceHandle(schService); 
        CloseServiceHandle(schSCManager);
    }
    catch (...)
    {
        return;
    }
    return;
}

VOID __stdcall CServiceBase::enableService(const LPCWSTR pszServiceName)
{
    try
    {
        SC_HANDLE schSCManager;
        SC_HANDLE schService;

        // Get a handle to the SCM database. 
 
        schSCManager = OpenSCManager( 
            NULL,                    // local computer
            NULL,                    // ServicesActive database 
            SC_MANAGER_ALL_ACCESS);  // full access rights 
 
        if (NULL == schSCManager) 
        {
            printf("OpenSCManager failed (%d)\n", GetLastError());
            return;
        }

        // Get a handle to the service.

        schService = OpenService( 
            schSCManager,            // SCM database 
            pszServiceName,               // name of service 
            SERVICE_CHANGE_CONFIG);  // need change config access 
 
        if (schService == NULL)
        { 
            printf("OpenService failed (%d)\n", GetLastError()); 
            CloseServiceHandle(schSCManager);
            return;
        }    

        // Change the service start type.

        if (! ChangeServiceConfig( 
            schService,            // handle of service 
            SERVICE_NO_CHANGE,     // service type: no change 
            SERVICE_DEMAND_START,  // service start type 
            SERVICE_NO_CHANGE,     // error control: no change 
            NULL,                  // binary path: no change 
            NULL,                  // load order group: no change 
            NULL,                  // tag ID: no change 
            NULL,                  // dependencies: no change 
            NULL,                  // account name: no change 
            NULL,                  // password: no change 
            NULL) )                // display name: no change
        {
            printf("ChangeServiceConfig failed (%d)\n", GetLastError()); 
        }
        else printf("Service enabled successfully.\n"); 

        CloseServiceHandle(schService); 
        CloseServiceHandle(schSCManager);
    }
    catch (...)
    {
        return;
    }
    return ;
}

VOID __stdcall CServiceBase::updateServiceDesc(const LPCWSTR pszServiceName)
{
    SC_HANDLE schSCManager;
    SC_HANDLE schService;
    SERVICE_DESCRIPTION sd;
    LPTSTR szDesc = TEXT("This is a test description");

    try
    {
        // Get a handle to the SCM database. 
 
        schSCManager = OpenSCManager( 
            NULL,                    // local computer
            NULL,                    // ServicesActive database 
            SC_MANAGER_ALL_ACCESS);  // full access rights 
 
        if (NULL == schSCManager) 
        {
            printf("OpenSCManager failed (%d)\n", GetLastError());
            return;
        }

        // Get a handle to the service.

        schService = OpenService( 
            schSCManager,            // SCM database 
            pszServiceName,               // name of service 
            SERVICE_CHANGE_CONFIG);  // need change config access 
 
        if (schService == NULL)
        { 
            printf("OpenService failed (%d)\n", GetLastError()); 
            CloseServiceHandle(schSCManager);
            return;
        }    

        // Change the service description.

        sd.lpDescription = szDesc;

        if( !ChangeServiceConfig2(
            schService,                 // handle to service
            SERVICE_CONFIG_DESCRIPTION, // change: description
            &sd) )                      // new description
        {
            printf("ChangeServiceConfig2 failed\n");
        }
        else printf("Service description updated successfully.\n");

        CloseServiceHandle(schService); 
        CloseServiceHandle(schSCManager);
    }
    catch (...)
    {
        return;
    }
    return;
}

VOID __stdcall CServiceBase::queryService(const LPCWSTR pszServiceName)
{
    SC_HANDLE schSCManager;
    SC_HANDLE schService;
    LPQUERY_SERVICE_CONFIG lpsc; 
    LPSERVICE_DESCRIPTION lpsd;
    DWORD dwBytesNeeded, cbBufSize, dwError; 

    try
    {
        // Get a handle to the SCM database. 
 
        schSCManager = OpenSCManager( 
            NULL,                    // local computer
            NULL,                    // ServicesActive database 
            SC_MANAGER_ALL_ACCESS);  // full access rights 
 
        if (NULL == schSCManager) 
        {
            printf("OpenSCManager failed (%d)\n", GetLastError());
            return;
        }

        // Get a handle to the service.

        schService = OpenService( 
            schSCManager,          // SCM database 
            pszServiceName,             // name of service 
            SERVICE_QUERY_CONFIG); // need query config access 
 
        if (schService == NULL)
        { 
            printf("OpenService failed (%d)\n", GetLastError()); 
            CloseServiceHandle(schSCManager);
            return;
        }

        // Get the configuration information.
 
        if( !QueryServiceConfig( 
            schService, 
            NULL, 
            0, 
            &dwBytesNeeded))
        {
            dwError = GetLastError();
            if( ERROR_INSUFFICIENT_BUFFER == dwError )
            {
                cbBufSize = dwBytesNeeded;
                lpsc = (LPQUERY_SERVICE_CONFIG) LocalAlloc(LMEM_FIXED, cbBufSize);
            }
            else
            {
                printf("QueryServiceConfig failed (%d)", dwError);
                goto cleanup; 
            }
        }
  
        if( !QueryServiceConfig( 
            schService, 
            lpsc, 
            cbBufSize, 
            &dwBytesNeeded) ) 
        {
            printf("QueryServiceConfig failed (%d)", GetLastError());
            goto cleanup;
        }

        if( !QueryServiceConfig2( 
            schService, 
            SERVICE_CONFIG_DESCRIPTION,
            NULL, 
            0, 
            &dwBytesNeeded))
        {
            dwError = GetLastError();
            if( ERROR_INSUFFICIENT_BUFFER == dwError )
            {
                cbBufSize = dwBytesNeeded;
                lpsd = (LPSERVICE_DESCRIPTION) LocalAlloc(LMEM_FIXED, cbBufSize);
            }
            else
            {
                printf("QueryServiceConfig2 failed (%d)", dwError);
                goto cleanup; 
            }
        }
 
        if (! QueryServiceConfig2( 
            schService, 
            SERVICE_CONFIG_DESCRIPTION,
            (LPBYTE) lpsd, 
            cbBufSize, 
            &dwBytesNeeded) ) 
        {
            printf("QueryServiceConfig2 failed (%d)", GetLastError());
            goto cleanup;
        }
 
        // Print the configuration information.
 
        _tprintf(TEXT("%s configuration: \n"), pszServiceName);
        _tprintf(TEXT("  Type: 0x%x\n"), lpsc->dwServiceType);
        _tprintf(TEXT("  Start Type: 0x%x\n"), lpsc->dwStartType);
        _tprintf(TEXT("  Error Control: 0x%x\n"), lpsc->dwErrorControl);
        _tprintf(TEXT("  Binary path: %s\n"), lpsc->lpBinaryPathName);
        _tprintf(TEXT("  Account: %s\n"), lpsc->lpServiceStartName);

        if (lpsd->lpDescription != NULL && lstrcmp(lpsd->lpDescription, TEXT("")) != 0)
            _tprintf(TEXT("  Description: %s\n"), lpsd->lpDescription);
        if (lpsc->lpLoadOrderGroup != NULL && lstrcmp(lpsc->lpLoadOrderGroup, TEXT("")) != 0)
            _tprintf(TEXT("  Load order group: %s\n"), lpsc->lpLoadOrderGroup);
        if (lpsc->dwTagId != 0)
            _tprintf(TEXT("  Tag ID: %d\n"), lpsc->dwTagId);
        if (lpsc->lpDependencies != NULL && lstrcmp(lpsc->lpDependencies, TEXT("")) != 0)
            _tprintf(TEXT("  Dependencies: %s\n"), lpsc->lpDependencies);
 
        LocalFree(lpsc); 
        LocalFree(lpsd);

    cleanup:
        CloseServiceHandle(schService); 
        CloseServiceHandle(schSCManager);
    }
    catch (...)
    {
        return;
    }
    return;
}

VOID WINAPI CServiceBase::onServiceCtrlHandler (DWORD dwCtrlCode)
{
    switch (dwCtrlCode)
    {
    case SERVICE_CONTROL_STOP :
        //if (g_ServiceStatus.dwCurrentState != SERVICE_RUNNING)
        //{
        //}
        //else
        {
//            char strTemp[MAX_PATH];
//            const LPCTSTR pszServiceEvent = TEXT("DPTK.DICOM.SCP.SERVICE");
            BOOL bInheritHandle(FALSE);
            HANDLE eventServiceStop = OpenEvent(
                EVENT_ALL_ACCESS,   //_In_  DWORD dwDesiredAccess,
                bInheritHandle,   //_In_  BOOL bInheritHandle,
                pszServiceEvent //_In_  LPCTSTR lpName
                );
            if (eventServiceStop != INVALID_HANDLE_VALUE)
            {
                // Signal the service to stop.
                // This will signal the worker thread to start shutting down
                SetEvent (eventServiceStop);
            }
            else
            {
//                sprintf_s(
//                    strTemp, "service.CreateEvent : error#%d", GetLastError()
//                    );
            }

//            ///. Perform tasks necessary to stop the service here
//            HANDLE eventServiceControl = OpenEvent(
//                NULL,   //_In_  DWORD dwDesiredAccess,
//                FALSE,   //_In_  BOOL bInheritHandle,
//                pszServiceEvent //_In_  LPCTSTR lpName
//                );
//            if (eventServiceControl != INVALID_HANDLE_VALUE)
//            {
//        //    g_ServiceStatus.dwControlsAccepted = 0;
//        //    g_ServiceStatus.dwCurrentState = SERVICE_STOP_PENDING;
//        //    g_ServiceStatus.dwWin32ExitCode = 0;
//        //    g_ServiceStatus.dwCheckPoint = 4;
//        //    if (SetServiceStatus (g_StatusHandle, &g_ServiceStatus) == FALSE)
//        //    {
//        //        OutputDebugString(_T(
//        //          "My Sample Service: ServiceCtrlHandler: SetServiceStatus returned error"));
//        //    }
// 
//                // Signal the service to stop.
////              SetEvent(m_eventServiceControl);
//                // This will signal the worker thread to start shutting down
//                SetEvent (eventServiceControl);
//            }
//            SetEvent( eventServiceControl );
        }
        break;
    case SERVICE_CONTROL_INTERROGATE:
        break;
    default:
        break;
    }
}

void CServiceBase::onStartService()
{
    m_logger.writeInfoLog("CServiceBase::onStartService(...)");
}

void CServiceBase::onService()
{
    m_logger.writeInfoLog("CServiceBase::onService(...)");
}

void CServiceBase::onStopService()
{
    m_logger.writeInfoLog("CServiceBase::onStopService(...)");
}

void CServiceBase::setServiceStatus(ServiceBase_State nStatus)
{
    m_nServiceStatus = nStatus;
}

int CServiceBase::execute(bool bWindowsService)
{
    try
    {
        m_bWindowsService = bWindowsService;
        ///. thread to broadcast service info.
        if (m_pQtThreadWS == NULL)
        {
            m_pQtThreadWS = new QtThreadWindowsService();
            m_pQtThreadWS->attach(this);
        }
        m_pQtThreadWS->start();
    }
    catch(...)
    {
        return -1;
    }
    return QCoreApplication::exec();
}

int CServiceBase::simulate()
{
    int nError(0);
    m_nQtThreadWS = CServiceBase::ThreadState_Running;

    onStartService();

    while (m_nQtThreadWS == CServiceBase::ThreadState_Running) 
    {
        onService();
        Sleep(m_lTimeoutInMS);
    }

    onStopService();

    m_nQtThreadWS = CServiceBase::ThreadState_Idle;

    return nError;
}

//VOID WINAPI CServiceBase::ServiceMain (DWORD argc, LPTSTR *argv)
void CServiceBase::service()
{
    DWORD Status = E_FAIL;

    ZeroMemory (&g_ServiceStatus, sizeof (g_ServiceStatus));
    // These SERVICE_STATUS members remain as set here
    g_ServiceStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
    g_ServiceStatus.dwControlsAccepted = 0;
    
    try
    {
        ///. Register our service control handler with the SCM
        g_ServiceStatusHandle = RegisterServiceCtrlHandler (
            m_strServiceName.c_str(), CServiceBase::onServiceCtrlHandler
            );
        if (g_ServiceStatusHandle == NULL) 
        {
            char strTemp[MAX_PATH];
	        sprintf_s(
		        strTemp, "service.RegisterServiceCtrlHandler : error#%d", GetLastError()
		        );
            m_logger.writeErrorLog( strTemp );

            reportServiceEvent( TEXT("RegisterServiceCtrlHandler") );
            throw "RegisterServiceCtrlHandler() failed.";
        }

        ///. Tell the service controller we are starting
//        g_ServiceStatus.dwCurrentState = SERVICE_START_PENDING;
//        g_ServiceStatus.dwWin32ExitCode = 0;
//        g_ServiceStatus.dwServiceSpecificExitCode = 0;
//        g_ServiceStatus.dwCheckPoint = 0;
//        if (SetServiceStatus (g_StatusHandle , &g_ServiceStatus) == FALSE)
//        {
//            OutputDebugString(_T(
//              "My Sample Service: ServiceMain: SetServiceStatus returned error")
//              );
//        }
        // Report initial status to the SCM
        reportServiceStatus( SERVICE_START_PENDING, NO_ERROR, 3000 );
 
        /*
         * Perform service-specific initialization and work:
         * Perform tasks necessary to start the service here
         */
        // Create a service stop event to wait on later
        m_eventServiceStop = CreateEvent(
            NULL,           //_In_opt_  LPSECURITY_ATTRIBUTES lpEventAttributes,
            TRUE,           //_In_      BOOL bManualReset,
            TRUE,          //_In_      BOOL bInitialState,
            pszServiceEvent //_In_opt_  LPCTSTR lpName//
            );
        if (m_eventServiceStop == NULL) 
        {
            char strTemp[MAX_PATH];
            sprintf_s(
                strTemp, "service.CreateEvent : error#%d", GetLastError()
                );
            m_logger.writeErrorLog( strTemp );

            reportServiceStatus( SERVICE_STOPPED, NO_ERROR, 0 );
            //// Error creating event
            //// Tell service controller we are stopped and exit
            //g_ServiceStatus.dwControlsAccepted = 0;
            //g_ServiceStatus.dwCurrentState = SERVICE_STOPPED;
            //g_ServiceStatus.dwWin32ExitCode = GetLastError();
            //g_ServiceStatus.dwCheckPoint = 1;
 
            //if (SetServiceStatus (g_ServiceStatusHandle, &g_ServiceStatus) == FALSE)
            //{
            //    OutputDebugString(
            //        _T("My Sample Service: ServiceMain: SetServiceStatus returned error")
            //        );
            //}
            throw "failed to create a service event.";
        }

        // Report running status when initialization is complete.
        onStartService();
        reportServiceStatus( SERVICE_RUNNING, NO_ERROR, 0 );
//        ///. Tell the service controller we are started
//        g_ServiceStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP;
//        g_ServiceStatus.dwCurrentState = SERVICE_RUNNING;
//        g_ServiceStatus.dwWin32ExitCode = 0;
//        g_ServiceStatus.dwCheckPoint = 0; 
//        if (SetServiceStatus (g_ServiceStatusHandle, &g_ServiceStatus) == FALSE)
//        {
//            OutputDebugString(_T(
//              "My Sample Service: ServiceMain: SetServiceStatus returned error"));
//        }
 
        // TO_DO: Perform work until service stops.
        m_nServiceStatus = ServiceBase_State_Running;
        ResetEvent( m_eventServiceStop );
        DWORD dwWaitResult(0);
        do
        {
            // Start a thread that will perform the main task of the service
            onService();
//        reportServiceStatus( gSvcStatus.dwCurrentState, NO_ERROR, 0 );

//        HANDLE hThread = CreateThread (
//            NULL, 0, CServiceBase::serviceWorkThread, this, 0, NULL
//            );
//   
//        // Wait until our worker thread exits signaling that the service needs to stop
//        WaitForSingleObject (hThread, INFINITE);

            // Check whether to stop the service.
            dwWaitResult = WaitForSingleObject(
                m_eventServiceStop, m_lTimeoutInMS
                );//INFINITE
            switch (dwWaitResult) 
            {
            case WAIT_OBJECT_0: // The thread got ownership of the mutex
                m_nServiceStatus = ServiceBase_State_Idle;
                break; 
            case WAIT_ABANDONED: // The thread got ownership of an abandoned mutex
            default:
                break;
            }
        }
        while (m_nServiceStatus == ServiceBase_State_Running);

        ///.
        reportServiceStatus( SERVICE_STOP_PENDING, NO_ERROR, 0 );

//        g_ServiceStatus.dwControlsAccepted = 0;
//        g_ServiceStatus.dwCurrentState = SERVICE_STOPPED;
//        g_ServiceStatus.dwWin32ExitCode = 0;
//        g_ServiceStatus.dwCheckPoint = 3;
// 
//        if (SetServiceStatus (g_ServiceStatusHandle, &g_ServiceStatus) == FALSE)
//        {
//            OutputDebugString(_T(
//              "My Sample Service: ServiceMain: SetServiceStatus returned error"));
//        }

        ///. Perform any cleanup tasks 
        onStopService();
        ///.
        if ( m_eventServiceStop )
        {
            CloseHandle (m_eventServiceStop);
            m_eventServiceStop = NULL;
        }
        m_logger.flush();
        ///. Tell the service controller we are stopped
        reportServiceStatus( SERVICE_STOPPED, NO_ERROR, 0 );
   }
    catch (...)
    {
        ///. Perform any cleanup tasks 
        onStopService();
        if ( m_eventServiceStop )
        {
            CloseHandle (m_eventServiceStop);
            m_eventServiceStop = NULL;
        }
        m_logger.writeInfoLog("CServiceBase::service(...) failed");
        m_logger.flush();
        return;
    }
    return;
}

/*
Service Worker Thread
*/
DWORD WINAPI CServiceBase::serviceWorkThread (LPVOID lpParam)
{
    CServiceBase* pSvcDcmSCP = (CServiceBase*) lpParam;
    if (pSvcDcmSCP != NULL)
    {
        pSvcDcmSCP->onServiceWorkThread();
    }
    return ERROR_SUCCESS;
}

void CServiceBase::onServiceWorkThread()
{
    ////  Periodically check if the service has been requested to stop
    //while (WaitForSingleObject(eventServiceControl, 0) != WAIT_OBJECT_0)
    //{        
    //    /* 
    //     * Perform main service function here
    //     */
 
    //    //  Simulate some work by sleeping
    //    Sleep(3000);
    //}
}

bool CServiceBase::stopDependentServices(SC_HANDLE schSCManager, SC_HANDLE schService)
{
    DWORD i;
    DWORD dwBytesNeeded;
    DWORD dwCount;

    LPENUM_SERVICE_STATUS   lpDependencies = NULL;
    ENUM_SERVICE_STATUS     ess;
    SC_HANDLE               hDepService;
    SERVICE_STATUS_PROCESS  ssp;

    DWORD dwStartTime = GetTickCount();
    DWORD dwTimeout = 30000; // 30-second time-out

    // Pass a zero-length buffer to get the required buffer size.
    if ( EnumDependentServices( schService, SERVICE_ACTIVE, 
         lpDependencies, 0, &dwBytesNeeded, &dwCount ) ) 
    {
         // If the Enum call succeeds, then there are no dependent
         // services, so do nothing.
         return TRUE;
    } 
    else 
    {
        if ( GetLastError() != ERROR_MORE_DATA )
            return FALSE; // Unexpected error

        // Allocate a buffer for the dependencies.
        lpDependencies = (LPENUM_SERVICE_STATUS) HeapAlloc( 
            GetProcessHeap(), HEAP_ZERO_MEMORY, dwBytesNeeded );
  
        if ( !lpDependencies )
            return FALSE;

        __try {
            // Enumerate the dependencies.
            if ( !EnumDependentServices( schService, SERVICE_ACTIVE, 
                lpDependencies, dwBytesNeeded, &dwBytesNeeded,
                &dwCount ) )
            return FALSE;

            for ( i = 0; i < dwCount; i++ ) 
            {
                ess = *(lpDependencies + i);
                // Open the service.
                hDepService = OpenService( schSCManager, 
                   ess.lpServiceName, 
                   SERVICE_STOP | SERVICE_QUERY_STATUS );

                if ( !hDepService )
                   return FALSE;

                __try {
                    // Send a stop code.
                    if ( !ControlService( hDepService, 
                            SERVICE_CONTROL_STOP,
                            (LPSERVICE_STATUS) &ssp ) )
                    return FALSE;

                    // Wait for the service to stop.
                    while ( ssp.dwCurrentState != SERVICE_STOPPED ) 
                    {
                        Sleep( ssp.dwWaitHint );
                        if ( !QueryServiceStatusEx( 
                                hDepService, 
                                SC_STATUS_PROCESS_INFO,
                                (LPBYTE)&ssp, 
                                sizeof(SERVICE_STATUS_PROCESS),
                                &dwBytesNeeded ) )
                        return FALSE;

                        if ( ssp.dwCurrentState == SERVICE_STOPPED )
                            break;

                        if ( GetTickCount() - dwStartTime > dwTimeout )
                            return FALSE;
                    }
                } 
                __finally 
                {
                    // Always release the service handle.
                    CloseServiceHandle( hDepService );
                }
            }
        } 
        __finally 
        {
            // Always free the enumeration buffer.
            HeapFree( GetProcessHeap(), 0, lpDependencies );
        }
    } 
    return TRUE;
}

/////////////////////////////////////////////////////////////////////////

//VOID WINAPI CServiceBase::installService(const LPCWSTR pszServiceName, const LPCWSTR pszDisplayName)
//{
//    try
//    {
//        SC_HANDLE schSCManager;
//        SC_HANDLE schService;
//        TCHAR szPath[MAX_PATH];
//
//        if( !GetModuleFileName( NULL, szPath, MAX_PATH ) )
//        {
//            printf("Cannot install service (%d)\n", GetLastError());
//            return;
//        }
//
//        
//        /* Get a handle to the SCM database. 
//        it did work for me when an account  is member of domain admins group. 
//        Win2k8 seems to be case sensitive on the service names where as win2k3 is not. 
//        make sure you get the service name correctly.
//        */
//         schSCManager = OpenSCManager(
//            NULL,                   // local computer
//            NULL,                   // ServicesActive database 
//            SC_MANAGER_ALL_ACCESS   // full access rights 
//            );
//        if (NULL == schSCManager) 
//        {
//            printf("OpenSCManager failed (%d)\n", GetLastError());
//            return;
//        }
//
//        // Create the service
//
//        schService = CreateService( 
//            schSCManager,           // SCM database 
//            pszServiceName,         // name of service 
//            pszDisplayName,         // service name to display 
//            SERVICE_ALL_ACCESS,        // desired access 
//            SERVICE_WIN32_OWN_PROCESS, // service type 
//            SERVICE_START_TYPE,      // Service start type 
//            SERVICE_ERROR_NORMAL,   // error control type 
//            szPath,                 // path to service's binary 
//            NULL,                   // no load ordering group 
//            NULL,                   // no tag identifier 
//            SERVICE_DEPENDENCIES,                   // no dependencies 
//            SERVICE_ACCOUNT,        // Service running account : LocalSystem account.
//            SERVICE_PASSWORD        // Password of the account : no password 
//            );
//        if (schService == NULL) 
//        {
//            printf("CreateService failed (%d)\n", GetLastError()); 
//            CloseServiceHandle(schSCManager);
//            return;
//        }
//        else printf("Service installed successfully\n"); 
//
//        CloseServiceHandle(schService); 
//        CloseServiceHandle(schSCManager);
//    }
//    catch (...)
//    {
//        return;
//    }
//    return;
//}

//VOID WINAPI CServiceBase::uninstallService(const LPCWSTR pszServiceName)
//{
//    SC_HANDLE schSCManager(NULL);
//    SC_HANDLE schService(NULL);
////    SERVICE_STATUS ssStatus; 
//    try
//    {
//        // Get a handle to the SCM database. 
//        schSCManager = OpenSCManager( 
//            NULL,                    // local computer
//            NULL,                    // ServicesActive database 
//            SC_MANAGER_ALL_ACCESS);  // full access rights 
// 
//        if (NULL == schSCManager) 
//        {
//            printf("OpenSCManager failed (%d)\n", GetLastError());
//            return;
//        }
//
//        // Get a handle to the service.
//        schService = OpenService( 
//            schSCManager,       // SCM database 
//            pszServiceName,     // name of service 
//            DELETE              // need delete access 
//            );
//        if (schService == NULL)
//        { 
//            printf("OpenService failed (%d)\n", GetLastError()); 
//            CloseServiceHandle(schSCManager);
//            return;
//        }
//
//        // Delete the service. 
//        if (! DeleteService(schService) ) 
//        {
//            printf("DeleteService failed (%d)\n", GetLastError()); 
//        }
//        else
//        {
//            printf("Service deleted successfully\n"); 
//        }
//        CloseServiceHandle(schService); 
//        CloseServiceHandle(schSCManager);
//    }
//    catch (...)
//    {
//        return ;
//    }
//    return ;
//}

void CServiceBase::reportServiceEvent(LPCWSTR szFunction)
{
    LPCTSTR lpszStrings[2];
    TCHAR Buffer[80];

    HANDLE hEventSource = RegisterEventSource(NULL, m_strServiceName.c_str());
    if( NULL != hEventSource )
    {
        StringCchPrintf(
            Buffer, 80, TEXT("%s failed with %d"), szFunction, GetLastError()
            );

        lpszStrings[0] = m_strServiceName.c_str();
        lpszStrings[1] = Buffer;

        ReportEvent(
            hEventSource,        // event log handle
            EVENTLOG_ERROR_TYPE, // event type
            0,                   // event category
            SVC_ERROR,           // event identifier
            NULL,                // no security identifier
            2,                   // size of lpszStrings array
            0,                   // no binary data
            lpszStrings,         // array of strings
            NULL               // no binary data
            );

        DeregisterEventSource(hEventSource);
    }
}

void CServiceBase::reportServiceStatus( 
    DWORD dwCurrentState, DWORD dwWin32ExitCode, DWORD dwWaitHint
    )
{
    static DWORD dwCheckPoint = 1;

    // Fill in the SERVICE_STATUS structure.

    g_ServiceStatus.dwCurrentState = dwCurrentState;
    g_ServiceStatus.dwWin32ExitCode = dwWin32ExitCode;
    g_ServiceStatus.dwWaitHint = dwWaitHint;

    if (dwCurrentState == SERVICE_START_PENDING)
        g_ServiceStatus.dwControlsAccepted = 0;
    else
        g_ServiceStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP;

    if (dwCurrentState == SERVICE_RUNNING || dwCurrentState == SERVICE_STOPPED )
        g_ServiceStatus.dwCheckPoint = 0;
    else
        g_ServiceStatus.dwCheckPoint = dwCheckPoint++;

//    logDPTKService.writeInfoLog("ReportSvcStatus::SetServiceStatus");

    // Report the status of the service to the SCM.
    SetServiceStatus( g_ServiceStatusHandle, &g_ServiceStatus );
}
