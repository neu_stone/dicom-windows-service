#pragma once
#include <stdio.h>
#include <tchar.h>

#include <windows.h>
#include <strsafe.h>
#include "Winsvc.h"

#include <string>

#include <QCoreApplication>
#include <QtCore\qthread.h>

#include "Logger.h"
class QtThreadWindowsService;

class CServiceBase : public QCoreApplication
{
    friend class QtThreadWindowsService;

public:

    CServiceBase(const LPCWSTR pszServiceName, const LPCWSTR pszDisplayName);
    virtual ~CServiceBase(void);
    void reportServiceEvent( LPCWSTR szFunction );

    ///. Service State -- for CurrentState
    //#define SERVICE_STOPPED                        0x00000001
    //#define SERVICE_START_PENDING                  0x00000002
    //#define SERVICE_STOP_PENDING                   0x00000003
    //#define SERVICE_RUNNING                        0x00000004
    //#define SERVICE_CONTINUE_PENDING               0x00000005
    //#define SERVICE_PAUSE_PENDING                  0x00000006
    //#define SERVICE_PAUSED                         0x00000007
    ///. -1, there is no service
    int getServiceState() const;

    //
    // Purpose: 
    //   Installs a service in the SCM database
    //
    // Parameters:
    //   None
    // 
    // Return value:
    //   None
    //
    int installService();
    //
    // Purpose: 
    //   Deletes a service from the SCM database
    //
    // Parameters:
    //   None
    // 
    // Return value:
    //   None
    //
    void uninstallService();

    int startService();
    void stopService();

    //
    // Purpose: 
    //   Disables the service.
    //
    // Parameters:
    //   None
    // 
    // Return value:
    //   None
    //
    static VOID __stdcall disableService(const LPCWSTR pszServiceName);
    //
    // Purpose: 
    //   Enables the service.
    //
    // Parameters:
    //   None
    // 
    // Return value:
    //   None
    //
    static VOID __stdcall enableService(const LPCWSTR pszServiceName);
    //
    // Purpose: 
    //   Updates the service description to "This is a test description".
    //
    // Parameters:
    //   None
    // 
    // Return value:
    //   None
    //
    static VOID __stdcall updateServiceDesc(const LPCWSTR pszServiceName);
    //
    // Purpose: 
    //   Retrieves and displays the current service configuration.
    //
    // Parameters:
    //   None
    // 
    // Return value:
    //   None
    //
    VOID __stdcall queryService(const LPCWSTR pszServiceName);

    int execute(bool bWindowsService);

    ///.
    enum ServiceBase_State
    {
        ServiceBase_State_Idle=0,
        ServiceBase_State_Initial,
        ServiceBase_State_Running,
    };
    void setServiceStatus(ServiceBase_State nStatus);

protected:

    // name of service, and service name to display
    std::wstring m_strServiceName, m_strDisplayName, m_strDPTKServiceExe;
    ///.
    HANDLE m_eventServiceStop;
    ServiceBase_State m_nServiceStatus;
    unsigned long m_lTimeoutInMS;

    SERVICE_STATUS_HANDLE g_ServiceStatusHandle;
    static SERVICE_STATUS        g_ServiceStatus;

    ///.
    TLogger m_logger;

    ///.
    enum THREAD_STATE
    {
        ThreadState_Idle=0,
        ThreadState_Initial,
        ThreadState_Running,
    };
    QtThreadWindowsService* m_pQtThreadWS;
    THREAD_STATE m_nQtThreadWS;
    bool m_bWindowsService;

    ///////////////////////////////////////////////
    ///.
    static VOID WINAPI onServiceCtrlHandler (DWORD dwCtrlCode);

    ///.
    void service();
    int simulate();

    ///.
    bool stopDependentServices(SC_HANDLE schSCManager, SC_HANDLE schService);
    ///.
    void reportServiceStatus( DWORD dwCurrentState, DWORD dwWin32ExitCode, DWORD dwWaitHint );


    ///...
    virtual void onStartService();
    virtual void onService();
    virtual void onStopService();

    ///////////////////////////////////////////////////////////
    ///.useless

    static DWORD WINAPI serviceWorkThread (LPVOID lpParam);
    ///.
    void onServiceWorkThread();
//    static VOID WINAPI installService(const LPCWSTR pszServiceName, const LPCWSTR pszDisplayName);
//    static VOID WINAPI uninstallService(const LPCWSTR pszServiceName);
//    static VOID WINAPI ServiceMain (DWORD argc, LPTSTR *argv);
};


class QtThreadWindowsService : public QThread
{
public:
	void attach(CServiceBase* pServiceBase);

private:
    CServiceBase* m_pServiceBase;

    ///.
    void run();
};
