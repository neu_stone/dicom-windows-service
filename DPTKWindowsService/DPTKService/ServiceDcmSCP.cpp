#include "ServiceDcmSCP.h"
#include <QtCore\qdir.h>
#include <QtCore\qsettings.h>
#include <QtCore\qstring.h>

#include "QtSqlPersistence.h"

namespace
{
    // Internal name of the service 
    const char* pstrServiceName = "DPTK.DICOM.SCP.SERVICE";
    const char* pstrEventClassUID = "1.2.840.10008.5.1.4.32.3";//UID_GeneralPurposePerformedProcedureStepSOPClass
    
    const QString strLevelStudy("STUDY");
    const QString strLevelSeries("SERIES");
    const QString strLevelImage("IMAGE");
}

/////////////////////////////////////////////////////////////////////////////////

//void QtThreadBroadcasting::attach(CServiceDcmSCP* pServiceController)
//{
//	m_pServiceController = pServiceController;
//}
//
//void QtThreadBroadcasting::run()
//{
//    if (m_pServiceController)
//    {
//        m_pServiceController->m_nQtThreadBroadcasting = CServiceDcmSCP::ThreadState_Running;
//
//        int nError(0), nSleepInSec(3);
//        do 
//        {
//            try
//            {
////		        nError = m_pQtSqlPersistence->onSearch(strSearchingDir, true);
//            }
//            catch (...)
//            {
//                nError = -1;
//            }
//            sleep(nSleepInSec);
//        }
//        while (m_pServiceController->m_nQtThreadBroadcasting == CServiceDcmSCP::ThreadState_Running);
//
//        m_pServiceController->m_nQtThreadBroadcasting = CServiceDcmSCP::ThreadState_Idle;
//    }
//}

/////////////////////////////////////////////////////////////////////////////////

CServiceDcmSCP::CServiceDcmSCP(const LPCWSTR pszServiceName, const LPCWSTR pszDisplayName)
    : CServiceBase(pszServiceName, pszDisplayName)
    , DPDcmObserver()
    , m_strSettingsINI("DPTKService.ini")
    , m_strGroupDicom("DICOM")
    , m_strGroupDatabase("DPTKDatabase")
    , m_strGroupUtility("DPTKUtility")
//    , m_strSearchingDir("TEMP")
    , m_strWorkingDir(".\\?")
    , m_bVerbose(true)
    , m_nNumOfSemaphoreResources(1)
    , m_nNumOfAcquireResources(1)
    , m_nAcqTimeoutInMS(500)
    , m_semaphoreDcmSCP(m_nNumOfSemaphoreResources)
    , m_serviceController()
    , m_strControllerAE("DPTKController?")
    , m_strControllerIP("localhost?")
    , m_strControllerPort("0")
    , m_bControllerTSL(false)
    , m_serviceProvider()
    , m_strProviderAE("DPTKProvider?")
    , m_strProviderIP("localhost?")
    , m_strProviderPort("0")
    , m_bProviderTSL(false)
    , m_strPathcoreRootUID("9.99.999.?")
    //, m_pQtThreadBroadcasting(NULL)
    //, m_nQtThreadBroadcasting(ThreadState_Idle)
    //, m_lTimeoutInMS(500)
{
    ///. Services are started from an application called Service Control Manager. This application lives in the system directory %WinDir%\System32
    ///. For more information see Service Control Manager at MSDN.
    QString strCurrentPath = QDir::currentPath();
    QString strModulePath( strCurrentPath );
    char szPath[MAX_PATH];
    if (GetModuleFileNameA( NULL, szPath, MAX_PATH ) > 0)
    {
        QFileInfo fileInfo1( szPath );
        strModulePath = fileInfo1.dir().path();
    }
    QDir::setCurrent( strModulePath );

    ///. Get configurations
    QString strDatabaseType, strDSN, strHostName, strPixelDataProvider;
    QString strSearchingDir(".\\?");
    QSettings iniSettings(m_strSettingsINI, QSettings::IniFormat);
    if (iniSettings.status() == QSettings::NoError)
    {
        iniSettings.beginGroup(m_strGroupUtility);
        m_bVerbose = iniSettings.value("Verbose", "1").toBool();
        ///. (wchar_t*)myString.toUcs4().data();
        ///. std::wstring thingy = (wchar_t*)myString.utf16();
 //       QString strTemp = iniSettings.value("DPTKServiceExe", "").toString();
        m_strDPTKServiceExe = (wchar_t*)( iniSettings.value("DPTKServiceExe", "").toString().utf16() );
        m_strPathcoreRootUID = iniSettings.value("PathcoreRootUID", "?").toString();
        strPixelDataProvider = iniSettings.value("PixelDataProvider", "?").toString();
        iniSettings.endGroup();

        ///. Database section
        iniSettings.beginGroup(m_strGroupDatabase);
        strDatabaseType = iniSettings.value("DatabaseType", "QODBC?").toString();
        strDSN = iniSettings.value("DSN", "DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=..\\..\\data\\DPTK_WSI.mdb?").toString();
        strHostName = iniSettings.value("HostName", "?").toString();
        ///.
        m_strWorkingDir = iniSettings.value("WorkingDir", ".\\?").toString();
        strSearchingDir = iniSettings.value("TempDataDir", ".\\?").toString();
        iniSettings.endGroup();

        ///. Dicom section
        iniSettings.beginGroup(m_strGroupDicom);
        m_strControllerAE = iniSettings.value("ControllerAE", "DPDcmSCP?").toString();
        m_strControllerIP = iniSettings.value("ControllerIP", "127.0.0.1?").toString();
        m_strControllerPort = iniSettings.value("ControllerPort", "1024").toString();
        m_strControllerPeers = iniSettings.value("ControllerPeers", "1").toString();
        m_bControllerTSL = iniSettings.value("ControllerTSL", "1").toBool();
        ///.
        m_strProviderAE = iniSettings.value("ProviderAE", "DPDcmSCP?").toString();
        m_strProviderIP = iniSettings.value("ProviderIP", "127.0.0.1?").toString();
        m_strProviderPort = iniSettings.value("ProviderPort", "1024").toString();
        m_strProviderPeers = iniSettings.value("ProviderPeers", "1").toString();
        m_bProviderTSL = iniSettings.value("ProviderTSL", "1").toBool();
        iniSettings.endGroup();
    }
    ///. end of if (iniSettings.status() == QSettings::NoError)

    ///."C:\\03.Temp\\DPTK.DICOM.SCP"
    QString strLogFile = QString("%1\\%2.Controller").
        arg( m_strWorkingDir ).
        arg( pstrServiceName );
    m_logger.initialize( strLogFile.toStdString().c_str() );
    sprintf_s(
	    szPath, "Windows Service : %s", strCurrentPath.toStdString().c_str()
	    );
    m_logger.writeInfoLog( szPath );
    sprintf_s(
	    szPath, "Current Path : %s", strModulePath.toStdString().c_str()
	    );
    m_logger.writeInfoLog( szPath );
    sprintf_s(
	    szPath, "Working Path : %s", m_strWorkingDir.toStdString().c_str()
	    );
    m_logger.writeInfoLog( szPath );

    ///. DatabaseType="QODBC"
    ///. DSN="DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=..\\data\\DPTK_WSI.mdb"
    ///. HostName=""
    QtSqlPersistence* pDPTKDB = QtSqlPersistence::getInstance();
    pDPTKDB->setTempDataDir( strSearchingDir );
    pDPTKDB->setWorkingDir( m_strWorkingDir );
    if (pDPTKDB->isConnected() == false &&
        pDPTKDB->connect(strDatabaseType, strDSN, strHostName) == false)
    {
        ///. DATABASE FAIL
        QString strTemp = QString("%1 : %2\r\n Temp: %3\r\n Data: %4\r\n Error: %5").
            arg(strDatabaseType).
            arg(strDSN).
            arg(strSearchingDir).
            arg(m_strWorkingDir).
            arg(pDPTKDB->getLastError());
        m_logger.writeInfoLog( strTemp.toUtf8() );
    }
    else
    {
        QString strTemp = QString("%1 : %2\r\n Temp: %3\r\n Data: %4").
            arg(strDatabaseType).
            arg(strDSN).
            arg(strSearchingDir).
            arg(m_strWorkingDir);
        m_logger.writeInfoLog( strTemp.toUtf8() );
    }
}

CServiceDcmSCP::~CServiceDcmSCP(void)
{
    if (m_serviceProvider.isWorking() == true)
    {
        m_logger.writeErrorLog( "Service Provider(DICOM SCP) stop listening when destruction !" );
        m_serviceProvider.stop();
    }

    //if (m_pQtThreadBroadcasting != NULL)
    //{
    //    if (m_nQtThreadBroadcasting != ThreadState_Idle)
    //    {
    //        m_nQtThreadBroadcasting = ThreadState_Idle;
    //        m_pQtThreadBroadcasting->terminate();
    //        m_pQtThreadBroadcasting->wait(m_lTimeoutInMS);
    //    }
    //    delete m_pQtThreadBroadcasting;
    //    m_pQtThreadBroadcasting = NULL;
    //}

    if (m_serviceController.isWorking() == true)
    {
        m_logger.writeErrorLog( "Service Controller(DICOM SCP) stop listening when destruction !" );
        m_serviceController.listen(false);
    }
    m_serviceController.release();

    ///. DATABASE
    QtSqlPersistence::releaseInstance();
    
    ///.
    if (m_bVerbose)
    {
        m_logger.writeInfoLog( "Service Controller(DICOM SCP) is done." );
    }
    m_logger.flush();
}

void CServiceDcmSCP::onStartService()
{
    m_logger.writeInfoLog( "" );

    QString strTemp("");
    ///. start Service Controller
    int nError = m_serviceController.initialize(
        m_strControllerAE.toStdString().c_str(),
        m_strControllerPort.toInt(),
        m_strControllerPeers.toInt(),
        m_strWorkingDir.toStdString().c_str(),
        this,
        m_bControllerTSL
        );
    if (nError == 0)
    {
        nError = m_serviceController.listen(true);
        ///.
        strTemp = QString("Service Controller( %1 at %2 for %3 with TSL#%4) : listen#%5").
            arg(m_strControllerAE).
            arg(m_strControllerPort).
            arg(m_strControllerPeers).
            arg(m_bControllerTSL).
            arg(nError);
    }
    else
    {
        strTemp = QString("Service Controller( %1 at %2 for %3 with TSL#%4) : initialize#%5").
            arg(m_strControllerAE).
            arg(m_strControllerPort).
            arg(m_strControllerPeers).
            arg(m_bControllerTSL).
            arg(nError);
    }
    m_logger.writeErrorLog( strTemp.toUtf8() );

    ///. start Service Provider
    if (nError != 0)
    {
        CServiceBase::setServiceStatus( ServiceBase_State_Idle );
    }
    else
    {
        m_serviceProvider.setVerbose( m_bVerbose );
        //m_serviceProvider.setSearchingDir( m_strSearchingDir.toUtf8() );
        nError = m_serviceProvider.start(
            m_strProviderAE.toStdString().c_str(),
            m_strProviderPort.toInt(),
            m_strProviderPeers.toInt(),
            m_bProviderTSL,
            m_strWorkingDir.toStdString().c_str()
            );
        strTemp = QString("Service Provider( %1 at %2 for %3 with TSL#%4) : start#%5").
            arg(m_strProviderAE).
            arg(m_strProviderPort).
            arg(m_strProviderPeers).
            arg(m_bProviderTSL).
            arg(nError);
        m_logger.writeErrorLog( strTemp.toUtf8() );

/*        ///. thread to broadcast service info.
        if (m_pQtThreadBroadcasting == NULL)
        {
            m_pQtThreadBroadcasting = new QtThreadBroadcasting();
            m_pQtThreadBroadcasting->attach(this);
        }
        m_pQtThreadBroadcasting->start();
 */   }

    ///.
    m_logger.writeInfoLog( "" );
}

void CServiceDcmSCP::onService()
{
    try
    {
        CServiceProvider::DCM_SERVICE_INFO infoService;
        bool bSuccessful(false);
        while (m_serviceProvider.lastServiceInfo(infoService) == true)
        {
            bSuccessful = true;
            if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
            {
                if (m_bVerbose)
                {
                    QString strTemp = QString("CServiceDcmSCP::onService() : Service#%1").
                        arg(infoService.nMsgType);
                    m_logger.writeInfoLog(strTemp.toUtf8());
                }

                ///. GENERAL PURPOSE PERFORMED PROCEDURE STEP IOD MODULES
                QDateTime dtTemp = QDateTime::currentDateTime();
                bool bReplaced(true);
//                char strValue[128];
                DataSet dcmDataset;
                int nError = dcmDataset.initialize(pstrEventClassUID);
                DcmItemHandle pItemRoot = dcmDataset.getRoot();

                // SOP Common
                //DCM_SOPInstanceUID
		        DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
                QString strTemp = QString("%1.%2%3%4.%5%6%7.%8").
                    arg(m_strPathcoreRootUID).
                    arg(dtTemp.date().year()).
                    arg(dtTemp.date().month()).
                    arg(dtTemp.date().day()).
                    arg(dtTemp.time().hour()).
                    arg(dtTemp.time().minute()).
                    arg(dtTemp.time().second()).
                    arg(dtTemp.time().msec());
                nError = dcmDataset.setStringValue(pItemRoot, tagKey, strTemp.toUtf8(), bReplaced);

                // General Purpose Performed Procedure Step Relationship
                //DCM_AdmissionID
                tagKey.group = 0x0038, tagKey.element = 0x0010;
                nError = dcmDataset.setStringValue(pItemRoot, tagKey, QString("%1").arg(infoService.nSessionID).toUtf8(), bReplaced);
                //DCM_ServiceEpisodeID
                tagKey.group = 0x0038, tagKey.element = 0x0060;
                nError = dcmDataset.setStringValue(pItemRoot, tagKey, QString("%1").arg(infoService.nMsgType).toUtf8(), bReplaced);

                // General Purpose Performed Procedure Step Information
                //DCM_PerformedStationNameCodeSequence
                long lAppendNew(-2);
                tagKey.group = 0x0040, tagKey.element = 0x4028;
                const DcmItemHandle pItemSCP = dcmDataset.findOrCreateSequenceItem(
                    pItemRoot, tagKey, lAppendNew
                    );
                if (pItemSCP)
                {
                    //Code Value
                    tagKey.group = 0x0008, tagKey.element = 0x0100;
                    nError = dcmDataset.setStringValue(
                        pItemSCP, tagKey, infoService.pstrCallingIP, bReplaced
                        );
                    //Coding Scheme Designator
                    tagKey.group = 0x0008, tagKey.element = 0x0102;
                    nError = dcmDataset.setStringValue(
                        pItemSCP, tagKey, m_strProviderAE.toUtf8(), bReplaced
                        );
                    //Code Meaning
                    tagKey.group = 0x0008, tagKey.element = 0x0104;
                    nError = dcmDataset.setStringValue(
                        pItemSCP, tagKey, infoService.pstrCallingAE, bReplaced
                        );
                }
                //Performed Procedure Step Start Date 0040,0244
                tagKey.group = 0x0040, tagKey.element = 0x0244;
                DataSet::TDcmDate dtValue;
                dtValue.Year = dtTemp.date().year();
                dtValue.Month = dtTemp.date().month();
                dtValue.Day = dtTemp.date().day();
                nError = dcmDataset.setDateValue(pItemRoot, tagKey, dtValue, bReplaced);
                //PerformedProcedureStepStartTime 0040,0245
                tagKey.group = 0x0040, tagKey.element = 0x0245;
                DataSet::TDcmTime tmValue;
                tmValue.Hour =  dtTemp.time().hour();
                tmValue.Minute =  dtTemp.time().minute();
                tmValue.Second =  dtTemp.time().second();
                nError = dcmDataset.setTimeValue(pItemRoot, tagKey, tmValue, bReplaced);
                //Performed Procedure Step ID
                //Performed Procedure Step End Date
                //DCM_GeneralPurposePerformedProcedureStepStatus
                tagKey.group = 0x0040, tagKey.element = 0x4002;
                nError = dcmDataset.setStringValue(pItemRoot, tagKey, QString("%1").arg(infoService.nTypeStatus).toUtf8(), bReplaced);
                //Performed Workitem Code Sequence
                /*A sequence that conveys the (single) type of procedure performed. 
                Only a single Item shall be included in this sequence.*/

                // General Purpose Results
                //DCM_NonDICOMOutputCodeSequence           DcmTagKey(0x0040, 0x4032)

                ///. Event Report
                DPDcmMsg msgReport;
                msgReport.nTypeRole = infoService.nTypeRole;
                msgReport.nTypeStatus = infoService.nTypeStatus;
                msgReport.nSessionID = infoService.nSessionID;
                msgReport.pstrCalledAE = "";//(char*)(m_strProviderAE.toUtf8().toch);
                msgReport.pstrCallingAE = infoService.pstrCallingAE;
                msgReport.pstrCallingIP = infoService.pstrCallingIP;
                msgReport.pDatasetReq = dcmDataset.getRoot();
                ///. report event
                nError = m_serviceController.report( msgReport );
                //if (m_bVerbose)
                //{
                //    QString strTemp = QString("CServiceDcmSCP::onService() : report#%1").
                //        arg(nError);
                //    m_logger.writeInfoLog(strTemp.toUtf8());
                //    dcmDataset.save("c:\\temp\\bbb.dcm", 0);
                //}
                ///. release semaphore
                m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
            }
            ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
            
            if (bSuccessful)
            {
                m_serviceProvider.removeLastServiceInfo();
            }
        }
        ///. end of while (m_serviceProvider.lastServiceInfo(infoService) == true)
    }
    catch (...)
    {
        m_logger.writeErrorLog( "CServiceDcmSCP::onService() : crash" );
        ///. release semaphore
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    return;
}

void CServiceDcmSCP::onStopService()
{
    m_logger.writeInfoLog( "" );

    QString strTemp("");
    if (m_serviceProvider.isWorking() == true)
    {
        m_serviceProvider.stop();
        strTemp = QString("Service Provider( %1 at %2 for %3) : stopped").
                arg(m_strProviderAE).
                arg(m_strProviderPort).
                arg(m_strProviderPeers);
        m_logger.writeErrorLog( strTemp.toStdString().c_str() );
    }

    //if (m_pQtThreadBroadcasting != NULL)
    //{
    //    if (m_nQtThreadBroadcasting != ThreadState_Idle)
    //    {
    //        m_nQtThreadBroadcasting = ThreadState_Idle;
    //        m_pQtThreadBroadcasting->terminate();
    //        m_pQtThreadBroadcasting->wait(m_lTimeoutInMS);
    //    }
    //    delete m_pQtThreadBroadcasting;
    //    m_pQtThreadBroadcasting = NULL;
    //}

    if (m_serviceController.isWorking() == true)
    {
        m_serviceController.listen(false);
    }
    m_serviceController.release();
    strTemp = QString("Service Controller( %1 at %2 for %3) : released").
            arg(m_strControllerAE).
            arg(m_strControllerPort).
            arg(m_strControllerPeers);
    m_logger.writeErrorLog( strTemp.toStdString().c_str() );

    m_logger.writeInfoLog( "" );
}

int CServiceDcmSCP::notify(TYPE_DCM_MSG nType, DPDcmMsg& msgDPDcm)const
{
	int nError(0);
    if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        CServiceDcmSCP* pServiceController = (CServiceDcmSCP*) this;
	    switch(nType)
	    {
        ///. Associatie Request
	    case TYPE_MSG_ASSOCIATE_RQ:
	    	pServiceController->onAssociateRequest(msgDPDcm);
		    break;
	    case TYPE_MSG_ASSOCIATE_AC:
		    pServiceController->onAssociateAccept(msgDPDcm);
		    break;
	    case TYPE_MSG_ASSOCIATE_RJ:
		    pServiceController->onAssociateReject(msgDPDcm);
		    break;
	    case TYPE_MSG_RELEASE_RQ:
	    	pServiceController->onAssociateRelease(msgDPDcm);
	    	break;
        ///. C-Services
	    case TYPE_MSG_C_ECHO_RQ:
		    pServiceController->onCEchoRequest(msgDPDcm);
		    break;
	    case TYPE_MSG_C_FIND_RQ:
		    pServiceController->onCFindRequest(msgDPDcm);
		    break;
        ///. N-Services
        case TYPE_MSG_N_GET_RQ:
		    pServiceController->onNGetRequest(msgDPDcm);
            break;
        case TYPE_MSG_N_SET_RQ:
		    pServiceController->onNSetRequest(msgDPDcm);
            break;
        case TYPE_MSG_N_ACTION_RQ:
        case TYPE_MSG_N_ACTION_RQ_TURNON:
        case TYPE_MSG_N_ACTION_RQ_TURNOFF:
		    pServiceController->onNActionRequest(nType, msgDPDcm);
            break;
        //case TYPE_MSG_N_EVENT_RSP:
        //    onNEventResponse(msgDPDcm);
        //    break;
        ///. Unsupported Services
	    default:
		    pServiceController->onUnknownRequest(nType, msgDPDcm);
		    nError = -1;
		    break;
	    }
        ///. release semaphore
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
	return nError;
}

void CServiceDcmSCP::onAssociateRequest( DPDcmMsg& msgParam )
{
    QString strTemp = QString("%1 << %2 : Associate Request.").
        arg(msgParam.pstrCalledAE).
        arg(msgParam.pstrCallingAE);
    m_logger.writeErrorLog( strTemp.toStdString().c_str() );
    ///.
    msgParam.nTypeStatus = TYPE_STATUS_SUCCESS;
}

void CServiceDcmSCP::onAssociateAccept( DPDcmMsg& msgParam )
{
    QString strTemp = QString("%1 >> %2 : Associate Accepted.").
        arg(msgParam.pstrCalledAE).
        arg(msgParam.pstrCallingAE);
    m_logger.writeErrorLog( strTemp.toStdString().c_str() );
    ///.
    msgParam.nTypeStatus = TYPE_STATUS_SUCCESS;
}

void CServiceDcmSCP::onAssociateReject( DPDcmMsg& msgParam )
{
    QString strTemp = QString("%1 >> %2 : Associate Rejected.").
        arg(msgParam.pstrCalledAE).
        arg(msgParam.pstrCallingAE);
    m_logger.writeErrorLog( strTemp.toStdString().c_str() );
    ///.
    msgParam.nTypeStatus = TYPE_STATUS_SUCCESS;
}

void CServiceDcmSCP::onAssociateRelease( DPDcmMsg& msgParam )
{
    QString strTemp = QString("%1 << %2 : Associate Release.").
        arg(msgParam.pstrCalledAE).
        arg(msgParam.pstrCallingAE);
    m_logger.writeErrorLog( strTemp.toStdString().c_str() );
    ///.
    msgParam.nTypeStatus = TYPE_STATUS_SUCCESS;
}

void CServiceDcmSCP::onUnknownRequest( TYPE_DCM_MSG nType, DPDcmMsg& msgParam )
{
    QString strTemp = QString("%1 << %2 : UnknownRequest#%3.").
        arg(msgParam.pstrCalledAE).
        arg(msgParam.pstrCallingAE).
        arg(nType);
    m_logger.writeErrorLog( strTemp.toStdString().c_str() );
    ///.
    msgParam.nTypeStatus = TYPE_STATUS_FAILURE;
}

void CServiceDcmSCP::onNSetRequest( DPDcmMsg& msgParam )
{
    QString strTemp = QString("%1 << %2 : NSet Request.").
        arg(msgParam.pstrCalledAE).
        arg(msgParam.pstrCallingAE);
    m_logger.writeErrorLog( strTemp.toUtf8() );

    ///.
    if (msgParam.pDatasetReq != NULL)
    {
        DataSet dcmDataset;
        DcmItemHandle pReqRoot = msgParam.pDatasetReq;

        bool bReplaced(true);
        int nError;

        ///.
        nError = QtSqlPersistence::getInstance()->setRemoteAE(pReqRoot);

        //DCM_SOPInstanceUID
		DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
        // General Purpose Performed Procedure Step Information

        //DCM_GeneralPurposePerformedProcedureStepStatus
        tagKey.group = 0x0040, tagKey.element = 0x4002;
        nError = dcmDataset.setStringValue(
            pReqRoot, tagKey,
            QString("%1").arg(nError).toUtf8(), 
            bReplaced
            );
        //Performed Workitem Code Sequence
        /*A sequence that conveys the (single) type of procedure performed. 
        Only a single Item shall be included in this sequence.*/
    }
}

void CServiceDcmSCP::onNGetRequest( DPDcmMsg& msgParam )
{
    QString strTemp = QString("%1 << %2 : NGet Request.").
        arg(msgParam.pstrCalledAE).
        arg(msgParam.pstrCallingAE);
    m_logger.writeErrorLog( strTemp.toStdString().c_str() );

    ///.
    if (msgParam.pDatasetReq != NULL)
    {
        DataSet dcmDataset;
        DcmItemHandle pReqRoot = msgParam.pDatasetReq;

        bool bReplaced(true);
        int nError;

        ///.
        nError = QtSqlPersistence::getInstance()->getRemoteAE(pReqRoot);

        //DCM_SOPInstanceUID
		DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
        // General Purpose Performed Procedure Step Information
        //DCM_PerformedStationNameCodeSequence
        tagKey.group = 0x0040, tagKey.element = 0x4028;
        const DcmItemHandle pItemOfPerformedStationNameCodeSeq = dcmDataset.findOrCreateSequenceItem(
            pReqRoot, tagKey, -1
            );
        if (pItemOfPerformedStationNameCodeSeq)
        {
            QString strCodeValue = QString("%1 @ %2:%3, PEERS#%4, TSL#%5, WORKING#%6").
                arg(m_strProviderAE).
                arg(m_strProviderIP).
                arg(m_strProviderPort).
                arg(m_strProviderPeers).
                arg(m_bProviderTSL).
                arg(m_serviceProvider.isWorking());
            //Code Value
            tagKey.group = 0x0008, tagKey.element = 0x0100;
            nError = dcmDataset.setStringValue(
                pItemOfPerformedStationNameCodeSeq, tagKey, strCodeValue.toUtf8(), bReplaced
                );
            //Coding Scheme Designator
            tagKey.group = 0x0008, tagKey.element = 0x0102;
            nError = dcmDataset.setStringValue(
                pItemOfPerformedStationNameCodeSeq, tagKey, m_strControllerAE.toUtf8(), bReplaced
                );
            //Code Meaning
            tagKey.group = 0x0008, tagKey.element = 0x0104;
            nError = dcmDataset.setStringValue(
                pItemOfPerformedStationNameCodeSeq, tagKey, "DICOM SCP"/*m_strProviderAE.toUtf8()*/, bReplaced
                );
        }
        //Performed Procedure Step Start Date
        //Performed Procedure Step ID
        //Performed Procedure Step End Date
        //DCM_GeneralPurposePerformedProcedureStepStatus
        tagKey.group = 0x0040, tagKey.element = 0x4002;
        nError = dcmDataset.setStringValue(
            pReqRoot, tagKey,
            QString("%1").arg(m_serviceProvider.isWorking() ? TYPE_MSG_N_ACTION_RQ_TURNON : TYPE_MSG_N_ACTION_RQ_TURNOFF).toUtf8(), 
            bReplaced
            );
        //Performed Workitem Code Sequence
        /*A sequence that conveys the (single) type of procedure performed. 
        Only a single Item shall be included in this sequence.*/

    }
}

void CServiceDcmSCP::onCFindRequest( DPDcmMsg& msgParam )
{
    QString strTemp;
    if (m_bVerbose)
    {
        strTemp = QString("%1 << %2 : CFind Request.").
            arg(msgParam.pstrCalledAE).
            arg(msgParam.pstrCallingAE);
        m_logger.writeErrorLog( strTemp.toStdString().c_str() );
	    //if (msgParam.pDatasetReq)
     //   {
     //       DataSet dcmDataset;
     //       int nError = dcmDataset.load(msgParam.pDatasetReq);
     //       dcmDataset.save("");
     //   }
    }

	if (msgParam.pDatasetReq)
	{
        DataSet dcmDataset;
        DcmItemHandle pReqRoot = msgParam.pDatasetReq;

        char strValue[128];
        long lPos(0);
        bool bSearchSub(false), bFlag(false);

        int nNumOfResults(-1);
	    /// QueryRetrieveLevel
		DataSet::TDcmTagKey localTag( 0x0008, 0x0052 );
        bFlag = dcmDataset.findAndGetString( pReqRoot, localTag, strValue, lPos, bSearchSub );
        if (bFlag == false)
        {   ///. it is not query
        }
        else if (strLevelStudy.compare(strValue, Qt::CaseInsensitive) == 0)
        {
            nNumOfResults = QtSqlPersistence::getInstance()->getStudyInfo( pReqRoot );
        }
        else if (strLevelSeries.compare(strValue, Qt::CaseInsensitive) == 0)
        {
            nNumOfResults = QtSqlPersistence::getInstance()->getSeriesInfo( pReqRoot );
        }
        else if (strLevelImage.compare(strValue, Qt::CaseInsensitive) == 0)
        {
            nNumOfResults = QtSqlPersistence::getInstance()->getImageInfo( pReqRoot );
        }
        else
        {
        }
        ///. end of if (dsTemp.findAndGetString(...) == false)
	}
    ///. end of if (msgParam.pDatasetReq)
}

void CServiceDcmSCP::onCEchoRequest( DPDcmMsg& msgParam )
{
    QString strTemp = QString("%1 << %2 : CEcho Request.").
        arg(msgParam.pstrCalledAE).
        arg(msgParam.pstrCallingAE);
    m_logger.writeInfoLog( strTemp.toStdString().c_str() );
    ///.
    msgParam.nTypeStatus = TYPE_STATUS_SUCCESS;
}

void CServiceDcmSCP::onNEventResponse( DPDcmMsg& msgParam )
{
    QString strTemp = QString("%1 << %2 : CEventReport Response.").
        arg(msgParam.pstrCalledAE).
        arg(msgParam.pstrCallingAE);
    m_logger.writeInfoLog( strTemp.toStdString().c_str() );
    ///.
    msgParam.nTypeStatus = TYPE_STATUS_SUCCESS;
}

void CServiceDcmSCP::onNActionRequest( TYPE_DCM_MSG nType, DPDcmMsg& msgParam )
{
    QString strTemp;

    if (m_bVerbose)
    {
        strTemp = QString("%1 << %2 : NAction Request( ActionID#%3 ).").
            arg(msgParam.pstrCalledAE).
            arg(msgParam.pstrCallingAE).
            arg(nType);
        m_logger.writeErrorLog( strTemp.toStdString().c_str() );
    }

    ///. data processing
    short nActionID(TYPE_MSG_NOTHING);
	if (msgParam.pDatasetReq)
	{
        DataSet dcmDataset;
        DcmItemHandle pReqRoot = msgParam.pDatasetReq;
        char strValue[MAX_PATH];
        long lPos(0);
        bool bSearchSub(false), bReplaced(false), bFlag;

        //DCM_SOPInstanceUID
		DataSet::TDcmTagKey tagKey(0x0008, 0x0018);
        ///. Scheduled Procedure Step ID
        tagKey.group = 0x0040, tagKey.element = 0x0009;
        bFlag = dcmDataset.findAndGetString(
            pReqRoot, tagKey, strValue, lPos, bSearchSub
            );
        if (bFlag)
        {
            nActionID = QString(strValue).toInt();
        }
        //Scheduled Processing Applications Code Sequence	(0040,4004)
        //Scheduled Station Name Code Sequence	(0040,4025)
        //Scheduled Workitem Code Sequence	(0040,4018)	
        //Input Information Sequence	(0040,4021)

        ///. data processing
        int nServiceEpisodeID(TYPE_MSG_NOTHING);
        switch (nActionID)
        {
        case TYPE_MSG_N_ACTION_RQ_TURNON:
            if (m_serviceProvider.isWorking() == false)
            {
                m_logger.writeErrorLog( "" );
                m_serviceProvider.setVerbose( m_bVerbose );
                //m_serviceProvider.setSearchingDir( m_strSearchingDir.toUtf8() );
                int nError = m_serviceProvider.start(
                    m_strProviderAE.toStdString().c_str(),
                    m_strProviderPort.toInt(),
                    m_strProviderPeers.toInt(),
                    m_bProviderTSL,
                    m_strWorkingDir.toStdString().c_str()
                    );
                msgParam.nTypeStatus = nError==0 ? TYPE_STATUS_SUCCESS : TYPE_STATUS_FAILURE;
                nServiceEpisodeID = nError==0 ? TYPE_MSG_N_ACTION_RQ_TURNON : TYPE_MSG_N_ACTION_RQ_TURNOFF;
                ///.
                strTemp = QString("Service Provider( %1 at %2 for %3 with TSL#%4) : start#%5").
                    arg(m_strProviderAE).
                    arg(m_strProviderPort).
                    arg(m_strProviderPeers).
                    arg(m_bProviderTSL).
                    arg(nError);
                m_logger.writeErrorLog( strTemp.toStdString().c_str() );
                m_logger.writeErrorLog( "" );
            }
            else
            {
                msgParam.nTypeStatus = TYPE_STATUS_WARNING;
                nServiceEpisodeID = TYPE_MSG_N_ACTION_RQ_TURNON;
            }
            break;
        case TYPE_MSG_N_ACTION_RQ_TURNOFF:
            if (m_serviceProvider.isWorking() == true)
            {
                m_logger.writeErrorLog( "" );
                m_serviceProvider.stop();
                msgParam.nTypeStatus = TYPE_STATUS_SUCCESS;
                ///.
                strTemp = QString("Service Provider( %1 at %2 for %3) : stopped").
                        arg(m_strProviderAE).
                        arg(m_strProviderPort).
                        arg(m_strProviderPeers);
                m_logger.writeErrorLog( strTemp.toStdString().c_str() );
                m_logger.writeErrorLog( "" );
            }
            else
            {
                msgParam.nTypeStatus = TYPE_STATUS_WARNING;
            }
            nServiceEpisodeID = TYPE_MSG_N_ACTION_RQ_TURNOFF;
            break;
        default:
            msgParam.nTypeStatus = TYPE_STATUS_FAILURE;
            break;
        }
        //DCM_ServiceEpisodeID
        tagKey.group = 0x0038, tagKey.element = 0x0060;
        int nError = dcmDataset.setStringValue(pReqRoot, tagKey, QString("%1").arg(nServiceEpisodeID).toUtf8(), bReplaced);
//        //Performed Procedure Step Start Date 0040,0244
//        tagKey.group = 0x0040, tagKey.element = 0x0244;
//        if (dcmDatasetReq.findAndGetDate(pItemReq, tagKey, dtValue, lPos, bSearchChild) == true)
//        {
////            QString strService = QString("\rDate#%1-%2-%3").arg(dtValue.Year).arg(dtValue.Month).arg(dtValue.Day);
////            strTemp.append(strService);
//        }
//        //PerformedProcedureStepStartTime 0040,0245
//        tagKey.group = 0x0040, tagKey.element = 0x0245;
//        if (dcmDatasetReq.findAndGetTime(pItemReq, tagKey, tmValue, lPos, bSearchChild) == true)
//        {
////            QString strService = QString("\rTime#%1:%2:%3").arg(tmValue.Hour).arg(tmValue.Minute).arg(tmValue.Second);
////            strTemp.append(strService);
//        }
    }
    ///. end of if (msgParam.pDatasetReq)
}