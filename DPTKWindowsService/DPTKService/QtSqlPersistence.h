#ifndef QTSQLPERSISTENCE_H
#define QTSQLPERSISTENCE_H

#include <QtSql\QSqlDatabase>
#include <QtCore\QThread>

#include "Dataset.h"
class QtThreadSearchingDcm;
class Service;

class QtSqlPersistence : public QObject
{
    friend class QtThreadSearchingDcm;

public:
    static QtSqlPersistence* getInstance();
    static void releaseInstance();
    
    ///.
    bool isConnected();
    void setWorkingDir(const QString& strWorkingDir);
    void setTempDataDir(const QString& strTempDataDir);
    ///. http://www.connectionstrings.com/access/
    ///. "DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=C:\\02.Project\\1.GitDPTK_XG02\\data\\DPTKUtility2003.mdb"
    ///. "Driver={Microsoft Access Driver (*.mdb)}; FIL={MS Access}; Dbq=C:\\02.Project\\1.GitDPTK_XG02\\data\\DPTKUtility2003.mdb; Uid=; Pwd=;"
    ///. "Provider=microsoft.jet.oledb.4.0;data source=C:\\02.Project\\1.GitDPTK_XG02\\src\\DPTKUtility2003.mdb;user id=;password=;"
    ///. "Driver={Microsoft Access Driver (*.mdb, *.accdb)};DSN='';FIL={MS Access};DBQ=DPTKUtility.mdb"
    bool connect(const QString& pstrDatabaseType, const QString& pstrDSN, const QString& pstrHostName);
    void close();

    ///.
    ///. save DCM dataset into database
    int setDcmDataset(const DataSet& dcmDataset);
    ///. save DCM dataset into temp folder
    int saveDataset(const DataSet& dcmDataset);

    ///. search DCM dataset from database
//    int getStudyInfo( DataSet& dcmDataset/*, QList<DataSet> listDcmDataset*/);
    int getStudyInfo(DcmItemHandle pItemRoot);
    int getSeriesInfo(DcmItemHandle pItemRoot);
    int getImageInfo(DcmItemHandle pItemRoot);

    ///. this function will search database, then immediately send results to SCP.
    int getAndStoreTempFileInfo(DcmItemHandle pItemRoot, Service& dcmSCP, unsigned int nSessionIndex);
    ///. 
    int getImageFileInfo(DcmItemHandle pItemRoot);
    ///. 
    int getImageLocalizer(DcmItemHandle pItemRoot);

    ///. Todo: jasper library ?
    int createMultiFrameDcmFile(const char* pstrFileName, DcmItemHandle pItemDS)const;
    ///. Todo: database operation ?
    int deleteDcmFiles(DcmItemHandle pItemRoot);

    ///.
    int setRemoteAE(DcmItemHandle pItemRoot);
    int getRemoteAE(DcmItemHandle pItemRoot);
    int getInfoRemoteAE(DcmItemHandle pItemRoot);

    ///.
    QString getLastError();

private:
    ///.
    QSqlDatabase m_SqlDatabase;
    QString m_strDatabaseType, m_strDSN, m_strHostName;
    QString m_strTempDataDir, m_strWorkingDir;
    QString m_strPixelDataProvider;
    bool m_bPatientAnonymized;
    QString m_strLastError;

    ///.
    enum THREAD_STATE
    {
        ThreadState_Idle=0,
        ThreadState_Initial,
        ThreadState_Running,
    };
    QtThreadSearchingDcm* m_pQtThreadSearchingDcm;
    THREAD_STATE m_nQtThreadSearchingDcm;
    unsigned long m_lTimeoutInMS;

    ///.
    static QtSqlPersistence* m_pQtSqlPersistence;
    
	//////////////////////////////////////////////////////////////////////////

    ///. Todo: jasper library
    ///. this function will search a directory:
    ///. 1. load DCM file.
    ///. 2. save DCM file to database
    int onSearch(const QString& strSearchDir, bool bSearchSub)const;
    int onSearchSingle(const QString& strSearchDir, bool bSearchSub)const;

	///. Common Pixel
	int OnSetPatient(const DataSet& dcmDataset);
	int OnSetStudy(const DataSet& dcmDataset);
	int OnSetSeries(const DataSet& dcmDataset);
	int OnSetImage(const DataSet& dcmDataset);
    int OnSetPresentationState(const DataSet& dcmDataset);

    /////////////////////////////////////////////////////////

    QtSqlPersistence(void);
    ~QtSqlPersistence(void);
};


class QtThreadSearchingDcm : public QThread
{
public:
	void attach(QtSqlPersistence* pQtSqlPersistence);

private:
    QtSqlPersistence* m_pQtSqlPersistence;

    ///.
    void run();
};

#endif  //QTSQLPERSISTENCE_H