#ifndef LOGGER_H
#define LOGGER_H

#include <vector>
#include <string>
using namespace std;

//typedef TCHAR LOGCHAR
//typedef std::wstring LOGSTRING

class TLogger
{
public:
	void writeImmidiateInfoLog(const char* strInfo);
	void writeFatalErrorLog(const char* strInfo);
	void writeErrorLog(const char* strInfo);
	void writeInfoLog(const char* strInfo);

	///.
	TLogger();
	~TLogger();
	bool initialize(const char* logfilename = NULL);
	void flush();

	inline bool isInitialized()const
	{
		return m_bInitialized;
	}

private:

	bool m_bInitialized;
	const size_t m_nMaxTextUnit;
	///.
	std::string m_strLogPath; 
	vector<std::string> m_vectorLogStrings;
};

#endif