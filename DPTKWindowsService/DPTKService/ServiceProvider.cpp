#include "ServiceProvider.h"

#include "QtSqlPersistence.h"

namespace
{
    // Internal name of the service 
    const char* pstrServiceName = "DPTK.DICOM.SCP.SERVICE";

    // DICOM name of Query Level
    const char* pstrLevelStudy = "STUDY";
    const char* pstrLevelSeries= "SERIES";
    const char* pstrLevelImage = "IMAGE";
}

CServiceProvider::CServiceProvider(void)
    : DPDcmObserver()
    , m_DPDcmSCP()
    , m_logger()
    , m_bVerbose(true)
    , m_bPatientAnonymized(false)
    , m_nNumOfSemaphoreResources(1)
    , m_nNumOfAcquireResources(1)
    , m_nAcqTimeoutInMS(500)
    , m_semaphoreDcmSCP(m_nNumOfSemaphoreResources)
    //, m_strSearchingDir("TEMP")
    //, m_strLevelStudy("STUDY");
    //, m_strLevelSeries("SERIES");
    //, m_strLevelImage("IMAGE");
    , m_queueServiceInfo()
{
}

CServiceProvider::~CServiceProvider(void)
{
    if (m_DPDcmSCP.isWorking() == true)
    {
        m_DPDcmSCP.listen(false);
    }
    m_DPDcmSCP.release();

    while (m_queueServiceInfo.isEmpty() == false)
    {
        ///. Removes the head item in the queue and returns it. 
        ///. This function assumes that the queue isn't empty.
        DCM_SERVICE_INFO* pServiceInfo = m_queueServiceInfo.dequeue();
        delete pServiceInfo;
        pServiceInfo = NULL;
    }

    m_logger.flush();
}

void CServiceProvider::removeLastServiceInfo()
{
    if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        if (m_queueServiceInfo.isEmpty() == false)
        {
            DCM_SERVICE_INFO* pServiceInfo = m_queueServiceInfo.dequeue();
            delete pServiceInfo;
            pServiceInfo = NULL;
        }
        ///.
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
}

bool CServiceProvider::lastServiceInfo(DCM_SERVICE_INFO& infoService)const
{
    bool bFlag(false);
    if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        if (m_queueServiceInfo.isEmpty() == false)
        {
            DCM_SERVICE_INFO* pServiceInfo = m_queueServiceInfo.head();

            infoService.nSessionID = pServiceInfo->nSessionID;
            infoService.nTypeRole = pServiceInfo->nTypeRole;
            infoService.nMsgType = pServiceInfo->nMsgType;
            infoService.nTypeStatus = pServiceInfo->nTypeStatus;
            memcpy_s(infoService.pstrCallingAE, 64, pServiceInfo->pstrCallingAE, 64);
            memcpy_s(infoService.pstrCallingIP, 64, pServiceInfo->pstrCallingIP, 64);
            infoService.bDataSetPresent = pServiceInfo->bDataSetPresent;

            bFlag = true;
        }
        ///.
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    return bFlag;
}

bool CServiceProvider::getLastServiceInfo(DCM_SERVICE_INFO& infoService)const
{
    bool bFlag(false);
    if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        if (m_queueServiceInfo.isEmpty() == false)
        {
            DCM_SERVICE_INFO* pServiceInfo = m_queueServiceInfo.dequeue();
            infoService.nSessionID = pServiceInfo->nSessionID;
            infoService.nTypeRole = pServiceInfo->nTypeRole;
            infoService.nMsgType = pServiceInfo->nMsgType;
            infoService.nTypeStatus = pServiceInfo->nTypeStatus;
            memcpy_s(infoService.pstrCallingAE, 64, pServiceInfo->pstrCallingAE, 64);
            memcpy_s(infoService.pstrCallingIP, 64, pServiceInfo->pstrCallingIP, 64);
            infoService.bDataSetPresent = pServiceInfo->bDataSetPresent;
            delete pServiceInfo;
            pServiceInfo = NULL;

            bFlag = true;
        }
        ///.
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    return bFlag;
}

int CServiceProvider::start(const char* pstrHostAE, int nPortNum, int nPeerNum, bool bTLS, const char* pstrWorkingDir)
{
    ///."C:\\03.Temp\\DPTK.DICOM.SCP"
    QString strLogFile = QString("%1\\%2.Provider").
        arg( pstrWorkingDir ).
        arg( pstrServiceName );
    m_logger.initialize( strLogFile.toStdString().c_str() );

    ///. start Service Controller
    int nError = m_DPDcmSCP.initialize(
        pstrHostAE, nPortNum, nPeerNum, pstrWorkingDir, this, bTLS
        );
    if (nError == 0)
    {
        nError = m_DPDcmSCP.listen(true);
    }
    else
    {
        nError = -1;
    }
    if (m_bVerbose)
    {
        QString strTemp = QString("Service Provider( %1 at %2 for %3 with TSL#%4) : start#%5").
            arg(pstrHostAE).
            arg(nPortNum).
            arg(nPeerNum).
            arg(bTLS).
            arg(nError);
        m_logger.writeErrorLog( strTemp.toUtf8() );
    }
    return nError;
}

void CServiceProvider::stop()
{
    if (m_DPDcmSCP.isWorking() == true)
    {
        m_DPDcmSCP.listen(false);
    }
    m_DPDcmSCP.release();
    ///.
    if (m_bVerbose)
    {
        QString strTemp("Service Provider is stopped.");
        m_logger.writeInfoLog( strTemp.toUtf8() );
    }
    m_logger.flush();
}

bool CServiceProvider::isWorking()
{
    return m_DPDcmSCP.isWorking();
}

int CServiceProvider::notify(TYPE_DCM_MSG nType, DPDcmMsg& msgDPDcm)const
{
	int nError(0);
    if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        DCM_SERVICE_INFO* pServiceInfo = new DCM_SERVICE_INFO;
        pServiceInfo->nSessionID = msgDPDcm.nSessionID;
        pServiceInfo->nTypeRole = msgDPDcm.nTypeRole;
        pServiceInfo->nMsgType = nType;
        pServiceInfo->nTypeStatus = msgDPDcm.nTypeStatus;
        sprintf_s(
            pServiceInfo->pstrCallingAE, "%s", msgDPDcm.pstrCallingAE
            );
        sprintf_s(
            pServiceInfo->pstrCallingIP, "%s", msgDPDcm.pstrCallingIP
            );
        pServiceInfo->bDataSetPresent = msgDPDcm.pDatasetReq==NULL ? false : true;
        m_queueServiceInfo.enqueue( pServiceInfo );

        ///.
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)

    switch(nType)
	{
    ////////////////////////////////////////
	case TYPE_MSG_ASSOCIATE_RQ:
		onAssociateRequest(msgDPDcm);
		break;
	case TYPE_MSG_ASSOCIATE_AC:
		onAssociateAccept(msgDPDcm);
		break;
	case TYPE_MSG_ASSOCIATE_RJ:
		onAssociateReject(msgDPDcm);
		break;
	case TYPE_MSG_RELEASE_RQ:
		onAssociateRelease(msgDPDcm);
		break;
	case TYPE_MSG_ABORT:
		onAbortRequest(msgDPDcm);
		break;
	case TYPE_MSG_C_CANCEL_RQ:
		onCancelRequest(msgDPDcm);
		break;
    ////////////////////////////////////////
	case TYPE_MSG_C_ECHO_RQ:
		onCEchoRequest(msgDPDcm);
		break;
	case TYPE_MSG_C_STORE_RQ:
		onCStoreRequest(msgDPDcm);
		break;
	case TYPE_MSG_C_FIND_RQ:
		onCFindRequest(msgDPDcm);
		break;
	case TYPE_MSG_C_GET_RQ:
		onCGetRequest(msgDPDcm);
		break;
	case TYPE_MSG_C_MOVE_RQ:
		onCMoveRequest(msgDPDcm);
		break;
    //  ////////////////////////////////////////
    //  case TYPE_MSG_N_GET_RQ:
		//onNGetRequest(msgDPDcm);
    //      break;
    //  case TYPE_MSG_N_ACTION_RQ:
		//onNActionRequest(msgDPDcm);
    //      break;
    //  case TYPE_MSG_N_DELETE_RQ:
    //      ///. todo: ?
		//onDeleteFile(msgDPDcm);
    //      break;
    ////////////////////////////////////////
    case TYPE_MSG_DPTK_LOCALIZER:
	    onGenerateLocalizer(msgDPDcm);
	    break;
	case TYPE_MSG_DPTK_DELETE_FILE:
		onDeleteFile(msgDPDcm);
		break;
	default:
        onUnknownRequest(nType, msgDPDcm);
		nError = -1;
		break;
	}
	return nError;
}

void CServiceProvider::onUnknownRequest(TYPE_DCM_MSG nType, DPDcmMsg& msgParam)const
{
    if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        TLogger* pLogger = (TLogger*)(&m_logger);
        if (m_bVerbose)
        {
            QString strTemp = QString("%1 << %2 : UnknownRequest, msgType#%3.").
                arg(msgParam.pstrCalledAE).
                arg(msgParam.pstrCallingAE).
                arg(nType);
            pLogger->writeInfoLog( strTemp.toUtf8() );
        }


        ///.
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)

    ///. data processing
    msgParam.nTypeStatus = TYPE_STATUS_FAILURE;
}

void CServiceProvider::onAssociateRequest( DPDcmMsg& msgParam )const
{
    if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        TLogger* pLogger = (TLogger*)(&m_logger);
        if (m_bVerbose)
        {
            QDateTime dtTemp = QDateTime::currentDateTime();
            QString strTemp = QString("%1 @ %2, %3-%4-%5 %6:%7:%8").
                arg(msgParam.pstrCallingAE).
                arg(msgParam.pstrCallingIP).
                arg(dtTemp.date().year()).
                arg(dtTemp.date().month()).
                arg(dtTemp.date().day()).
                arg(dtTemp.time().hour()).
                arg(dtTemp.time().minute()).
                arg(dtTemp.time().second());
            pLogger->writeInfoLog( strTemp.toUtf8() );
        }
        
        ///. data processing
        msgParam.nTypeStatus = TYPE_STATUS_SUCCESS;

        ///. release semaphore
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    //else
    //{
    //    ///. data processing
    //    msgParam.nTypeStatus = TYPE_STATUS_FAILURE;
    //}
    ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
}

void CServiceProvider::onAssociateAccept( DPDcmMsg& msgParam )const
{
    if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        TLogger* pLogger = (TLogger*)(&m_logger);
        if (m_bVerbose)
        {
            QString strTemp = QString("%1 << %2 : Associate Accept.").
                arg(msgParam.pstrCalledAE).
                arg(msgParam.pstrCallingAE);
            pLogger->writeInfoLog( strTemp.toUtf8() );
        }
        ///.
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
}

void CServiceProvider::onAssociateReject( DPDcmMsg& msgParam )const
{
    if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        TLogger* pLogger = (TLogger*)(&m_logger);
        if (m_bVerbose)
        {
            QString strTemp = QString("%1 << %2 : Associate Reject.").
                arg(msgParam.pstrCalledAE).
                arg(msgParam.pstrCallingAE);
            pLogger->writeInfoLog( strTemp.toUtf8() );
        }
        ///.
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
}

void CServiceProvider::onAssociateRelease( DPDcmMsg& msgParam )const
{
    if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        TLogger* pLogger = (TLogger*)(&m_logger);
        if (m_bVerbose)
        {
            QString strTemp = QString("%1#%2 << %3 : Associate Release.").
                arg(msgParam.pstrCalledAE).
                arg(msgParam.nSessionID).
                arg(msgParam.pstrCallingAE);
            pLogger->writeInfoLog( strTemp.toUtf8() );
        }

        ///. release semaphore
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
}

void CServiceProvider::onCancelRequest( DPDcmMsg& msgParam )const
{
    if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        TLogger* pLogger = (TLogger*)(&m_logger);
        if (m_bVerbose)
        {
            QString strTemp = QString("%1#%2 << %3 : Cancel Request.").
                arg(msgParam.pstrCalledAE).
                arg(msgParam.nSessionID).
                arg(msgParam.pstrCallingAE);
            pLogger->writeInfoLog( strTemp.toUtf8() );
        }

        ///. release semaphore
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
}

void CServiceProvider::onAbortRequest( DPDcmMsg& msgParam )const
{
    if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        TLogger* pLogger = (TLogger*)(&m_logger);
        if (m_bVerbose)
        {
            QString strTemp = QString("%1#%2 << %3 : Associate Abort.").
                arg(msgParam.pstrCalledAE).
                arg(msgParam.nSessionID).
                arg(msgParam.pstrCallingAE);
            pLogger->writeInfoLog( strTemp.toUtf8() );
        }

        ///. release semaphore
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
}

void CServiceProvider::onCEchoRequest( DPDcmMsg& msgParam )const
{
    if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        TLogger* pLogger = (TLogger*)(&m_logger);
        if (m_bVerbose)
        {
            QString strTemp = QString("%1#%2 << %3 : CEcho Request.").
                arg(msgParam.pstrCalledAE).
                arg(msgParam.nSessionID).
                arg(msgParam.pstrCallingAE);
            pLogger->writeInfoLog( strTemp.toUtf8() );
        }

        ///. release semaphore
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
}

void CServiceProvider::onCMoveRequest( DPDcmMsg& msgParam )const
{
    if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        TLogger* pLogger = (TLogger*)(&m_logger);
        if (m_bVerbose)
        {
            QString strTemp = QString("%1#%2 << %3 : CMove Request.").
                arg(msgParam.pstrCalledAE).
                arg(msgParam.nSessionID).
                arg(msgParam.pstrCallingAE);
            pLogger->writeInfoLog( strTemp.toUtf8() );
        }

        ///. release semaphore
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)

    ///. data processing
    QtSqlPersistence* pQtSqlPersistence = QtSqlPersistence::getInstance();
    int nDataProcessing(0);
    if (msgParam.pDatasetReq)
    {
        nDataProcessing |= pQtSqlPersistence->getInfoRemoteAE( msgParam.pDatasetReq );

//        nDataProcessing |= pQtSqlPersistence->getTempFileInfo( msgParam.pDatasetReq ); 
    }
    ///. end of if (msgParam.pDatasetReq)
    if (nDataProcessing < 0 &&
        m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        ///. DATABASE FAIL
        TLogger* pLogger = (TLogger*)(&m_logger);
        if (m_bVerbose)
        {
            pLogger->writeInfoLog( pQtSqlPersistence->getLastError().toUtf8() );
        }
        ///. release semaphore
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
}

void CServiceProvider::onCGetRequest( DPDcmMsg& msgParam )const
{
    if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        TLogger* pLogger = (TLogger*)(&m_logger);
        if (m_bVerbose)
        {
            QString strTemp = QString("%1#%2 << %3 : CGet Request.").
                arg(msgParam.pstrCalledAE).
                arg(msgParam.nSessionID).
                arg(msgParam.pstrCallingAE);
            pLogger->writeInfoLog( strTemp.toUtf8() );
        }

        ///. release semaphore
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)

    ///. data processing
    QtSqlPersistence* pQtSqlPersistence = QtSqlPersistence::getInstance();
    int nDataProcessing(0);
    if (msgParam.pDatasetReq)
    {
        Service* pDcmSCP = (Service*)(&m_DPDcmSCP);
        nDataProcessing = pQtSqlPersistence->getAndStoreTempFileInfo( msgParam.pDatasetReq, *pDcmSCP, msgParam.nSessionID );
    }
    ///. end of if (msgParam.pDatasetReq)
    if (nDataProcessing < 0 &&
        m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        ///. DATABASE FAIL
        TLogger* pLogger = (TLogger*)(&m_logger);
        if (m_bVerbose)
        {
            pLogger->writeInfoLog( pQtSqlPersistence->getLastError().toUtf8() );
        }
        ///. release semaphore
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    ///. end of if (nDataProcessing < 0)
}

void CServiceProvider::onCFindRequest( DPDcmMsg& msgParam )const
{
    if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        TLogger* pLogger = (TLogger*)(&m_logger);
        if (m_bVerbose)
        {
            QString strTemp = QString("%1#%2 << %3 : CFind Request.").
                arg(msgParam.pstrCalledAE).
                arg(msgParam.nSessionID).
                arg(msgParam.pstrCallingAE);
            pLogger->writeInfoLog( strTemp.toUtf8() );
        }

        ///. release semaphore
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)

    ///. data processing
    QtSqlPersistence* pQtSqlPersistence = QtSqlPersistence::getInstance();
    int nNumOfResults(0);
	if (msgParam.pDatasetReq)
	{
		DataSet dsTemp;
//        dsTemp.load((const DcmItemHandle) (msgParam.pDatasetReq));
//		DcmItemHandle pItemRoot = dsTemp.getRoot();
        DcmItemHandle pItemRoot = (DcmItemHandle) (msgParam.pDatasetReq);

		DataSet::TDcmTagKey localTag(0, 0);
        char strValue[128];
        int nNumOfResults(-1);
	    /// QueryRetrieveLevel
        localTag.group=0x0008, localTag.element=0x0052;
        bool bFlag = dsTemp.findAndGetString(pItemRoot, localTag, strValue, 0, false);
        QString strQueryLevel(strValue);
        if (bFlag == false)
        {   ///. it is not query
        }
        else if (strQueryLevel.compare(pstrLevelStudy, Qt::CaseInsensitive) == 0)
        {
            nNumOfResults = pQtSqlPersistence->getStudyInfo( pItemRoot );
        }
        else if (strQueryLevel.compare(pstrLevelSeries, Qt::CaseInsensitive) == 0)
        {
            nNumOfResults = pQtSqlPersistence->getSeriesInfo( pItemRoot );
        }
        else if (strQueryLevel.compare(pstrLevelImage, Qt::CaseInsensitive) == 0)
        {
            nNumOfResults = pQtSqlPersistence->getImageInfo( pItemRoot );
        }
        else
        {
        }
        ///. end of if (dsTemp.findAndGetString(...) == false)
	}
    ///. end of if (msgParam.pDatasetReq)
    if (nNumOfResults < 0 &&
        m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        ///. DATABASE FAIL
        TLogger* pLogger = (TLogger*)(&m_logger);
        if (m_bVerbose)
        {
            pLogger->writeInfoLog( pQtSqlPersistence->getLastError().toUtf8() );
        }
        ///. release semaphore
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    ///. end of if (nDataProcessing < 0)
}

void CServiceProvider::onCStoreRequest( DPDcmMsg& msgParam )const
{
    if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        TLogger* pLogger = (TLogger*)(&m_logger);
        if (m_bVerbose)
        {
            QString strTemp = QString("%1#%2 << %3 : CStore Request.").
                arg(msgParam.pstrCalledAE).
                arg(msgParam.nSessionID).
                arg(msgParam.pstrCallingAE);
            pLogger->writeInfoLog( strTemp.toUtf8() );
        }

        ///. release semaphore
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)

    ///. data processing
    QtSqlPersistence* pQtSqlPersistence = QtSqlPersistence::getInstance();
    int nDataProcessing(0);
    if (msgParam.pDatasetReq)
    {
	    DataSet dsTemp;
        int nTransferSytaxFlag(-1);
	    int nError = dsTemp.load((const DcmItemHandle) (msgParam.pDatasetReq), nTransferSytaxFlag);

        ///. store dataset info to DB, thread-safe???
        nError = QtSqlPersistence::getInstance()->saveDataset(dsTemp);
        if (nError<0 &&
            m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
        {
            TLogger* pLogger = (TLogger*)(&m_logger);
            QString strTemp = QString("%1#%2 << %3 : CStore Request, dataset#%4.").
                arg(msgParam.pstrCalledAE).
                arg(msgParam.nSessionID).
                arg(msgParam.pstrCallingAE).
                arg(nError);
            pLogger->writeInfoLog( strTemp.toUtf8() );

            ///. release semaphore
            m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
        }
        ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    }
    ///. end of if (msgParam.pDataset)
}

void CServiceProvider::onDeleteFile(DPDcmMsg& msgParam)const
{
    if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        TLogger* pLogger = (TLogger*)(&m_logger);
        if (m_bVerbose)
        {
            QString strTemp = QString("%1 : delete tempray DCM files.").arg(msgParam.pstrCalledAE);
            pLogger->writeInfoLog( strTemp.toUtf8() );
        }

        ///. release semaphore
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)

    ///. data processing
    QtSqlPersistence* pQtSqlPersistence = QtSqlPersistence::getInstance();
    int nDataProcessing(0);
	if (msgParam.pDatasetReq)
	{
        nDataProcessing = pQtSqlPersistence->deleteDcmFiles( msgParam.pDatasetReq );
    }
    ///. end of if (msgParam.pDatasetReq)
        
    if (nDataProcessing < 0 &&
        m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        ///. DATABASE FAIL
        TLogger* pLogger = (TLogger*)(&m_logger);
        if (m_bVerbose)
        {
            pLogger->writeInfoLog( pQtSqlPersistence->getLastError().toUtf8() );
        }
        ///. release semaphore
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
}

void CServiceProvider::onGenerateLocalizer(DPDcmMsg& msgParam)const
{
    if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        TLogger* pLogger = (TLogger*)(&m_logger);
        if (m_bVerbose)
        {
            QString strTemp = QString("%1#%2 << %3 : CGet Request for LOCALIZER.").
                arg(msgParam.pstrCalledAE).
                arg(msgParam.nSessionID).
                arg(msgParam.pstrCallingAE);
            pLogger->writeInfoLog( strTemp.toUtf8() );
        }

        ///. release semaphore
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
    ///. end of if (m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)

    ///. data processing
    QtSqlPersistence* pQtSqlPersistence = QtSqlPersistence::getInstance();
    int nDataProcessing(0);
	if (msgParam.pDatasetReq)
	{
        nDataProcessing = pQtSqlPersistence->getImageLocalizer( msgParam.pDatasetReq );
        
    }
    ///. end of if (msgParam.pDatasetReq)
    if (nDataProcessing < 0 &&
        m_semaphoreDcmSCP.tryAcquire(m_nNumOfAcquireResources, m_nAcqTimeoutInMS) == true)
    {
        ///. DATABASE FAIL
        TLogger* pLogger = (TLogger*)(&m_logger);
        if (m_bVerbose)
        {
            pLogger->writeInfoLog( pQtSqlPersistence->getLastError().toUtf8() );
        }
        ///. release semaphore
        m_semaphoreDcmSCP.release(m_nNumOfAcquireResources);
    }
}
