/// Brief description
/// The user interface of DPDicomlib to listen to the request from DICOM SCU
//
/// Detailed description
/// 1. listen to the request from remote DICOM SCU.
/// 2. call back to the observer when a request arrive at DCM SCP side.
/// 3. provide multi-association in real-time at DCM SCP side.

#ifndef DPDCMSCPUI_H
#define DPDCMSCPUI_H

#include "Global.h"
#include "DataSet.h"
class CDcmtksSCP;
class DPDcmObserver;

/** DICOM SCP will listen to the PortNumber at current IP address.
 */
class DPTK_DICOMLIB_API Service {
public:

  /**
   * Constructor.
   */
  Service();
  
  /**
   * Destructor.
   */
  virtual ~Service();
  
  /**
     * Initialize DCM server.
     * @param strHostName: DICOM AE Title.
     * @param nHostPort: Network port.
     * @param nMaxPeers: maximum number of associations.
     * @param strWorkingDir: data directory.
     * @param pSCPObserver: the observer of receiving messages from DCM SCP
     * @param bTransportLayer: the flag to add Transport Layer Security
     * @return int: 0 if successful, otherwise failure.
     */
  int initialize(const char* strHostName, int nHostPort, int nMaxPeers,
                 const char* strWorkingDir, const DPDcmObserver* pSCPObserver,
                 bool bTransportLayer=true);
                 
  /**
  * Release DCM server.
   * @return void.
   */
  void release();
  
  /**
   * Make DCM server start/stop its service.
     * @param bTurnOn : turn Dcm SCP on/off.
     * @return : zero if successful; otherwise FALSE.
     */
  int listen(bool bTurnOn);
  
  /**
   * Check if DCM server is working.
   * @return : true if DCM server is working; otherwise FALSE.
  */
  bool isWorking();
  
  int report(const DPDcmMsg& msgReport);
  int store(unsigned int nSessionIndex, const DataSet& dsDcmData);
  
private:

  ///. Dcm SCP
  CDcmtksSCP* m_pDcmtksNet;
  
};


#endif // DPDCMSCPUI_H
