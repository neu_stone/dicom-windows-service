#ifndef SEDEEN_SRC_DLLAPICONTROL_H
#define SEDEEN_SRC_DLLAPICONTROL_H

// Using flags set in CMake
#if defined SEDEEN_UNIX_BUILD && __GNUC__ >= 4
#define SEDEEN_API_EXPORT __attribute__ ((visibility ("default")))
#define SEDEEN_API_IMPORT
#elif defined SEDEEN_MSVC_BUILD
#define SEDEEN_API_EXPORT __declspec(dllexport)
#define SEDEEN_API_IMPORT __declspec(dllimport)
#elif defined SEDEEN_CYGWIN_BUILD
#define SEDEEN_API_EXPORT __attribute__ ((dllexport))
#define SEDEEN_API_IMPORT __attribute__ ((dllimport))
#else
#define SEDEEN_API_EXPORT
#define SEDEEN_API_IMPORT
#endif

// Error-check the variables
#ifndef SEDEEN_API_EXPORT
#error "SEDEEN_API_EXPORT not configured for this system."
#endif

#ifndef SEDEEN_API_IMPORT
#error "SEDEEN_API_IMPORT not configured for this system."
#endif

#endif
