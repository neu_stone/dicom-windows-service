/// Brief description
/// The user interface of DPDicomlib to send the request to DICOM SCP
//
/// Detailed description
/// 1. connect to DICOM SCP.
/// 2. send DICOM request to DICOM SCP
/// 3. call back to the observer when specific request/response arrive at DCM SCU side.

#ifndef DPDCMSCUUI_H
#define DPDCMSCUUI_H

#include "Global.h"
#include "DataSet.h"
#include "DllApi.h"

class CStore;
class DcmDataset;
class DPDcmObserver;

/** send the request to DICOM SCP.
 */
class DPTK_DICOMLIB_API Client {
public:

  /**
   * Constructor.
     */
  Client(const DPDcmObserver* pSCPObserver=NULL);
  
  /**
   * Destructor.
   */
  virtual ~Client();
  bool addPreferredDcmDatasetSyntax(
    const char* pstrAbstractSyntax, const char* pstrTransferSyntax
  );
  
  /**
   * Connect to DCM server.
   * @param strAETitle: server's AE title
   * @param strIP: server's IP address.
   * @param nPort: server's port.
     * @param strSCUAETitle: client's AE title
     * @param bTransportLayer: the flag to add Transport Layer Security
   * @return : zero if successful; otherwise failed.
   */
  int connect(
    const char* strSCPAETitle, const char* strSCPIP, int nSCPPort,
    const char* strSCUAETitle, bool bTransportLayer=true
  );
  
  /**
  * Close the connection.
  * @return void.
  */
  void close();
  
  /**
   * CEchoe DCM class with DCM server.
   * @return : result of CEcho service.
   */
  const DPDcmMsg& echo();
  
  /**
   * Store DCM dataset to DCM server.
   * @param dcmDataset: DCM dataset.
   * @return : result of CStore service.
   */
  const DPDcmMsg& store(DataSet& dcmDataset);
  
  /**
   * clear DCM datasets stored at DCM SCU side.
   */
  void clear();
  
  /**
   * get number of query results at DCM SCU side.
   * @return : number of query results at DCM SCU side.
   */
  size_t getNumOfQueryInfo() const;
  
  /**
   * request CFind service at DICOM SCP side.
   * for example, Study Root Query/Retrieve Information Model might be
   * UID_FINDPatientRootQueryRetrieveInformationModel        "1.2.840.10008.5.1.4.1.2.1.1"
   * UID_MOVEPatientRootQueryRetrieveInformationModel        "1.2.840.10008.5.1.4.1.2.1.2"
   * UID_GETPatientRootQueryRetrieveInformationModel         "1.2.840.10008.5.1.4.1.2.1.3"
   * UID_FINDStudyRootQueryRetrieveInformationModel      "1.2.840.10008.5.1.4.1.2.2.1"
   * UID_MOVEStudyRootQueryRetrieveInformationModel      "1.2.840.10008.5.1.4.1.2.2.2"
   * UID_GETStudyRootQueryRetrieveInformationModel       "1.2.840.10008.5.1.4.1.2.2.3"
     * @param dcmDataset condition of the service.
   * @return status of the service at DCM SCP side.
   */
  const DPDcmMsg& find(DataSet& dcmDataset);
  /**
   * request CMove service at DICOM SCP side.
     * @param dcmDataset condition of the service.
   * @return status of the service at DCM SCP side.
   */
  const DPDcmMsg& move(DataSet& dcmDataset);
  /**
   * request CGet service at DICOM SCP side.
     * @param dcmDataset condition of the service.
   * @return status of the service at DCM SCP side.
   */
  const DPDcmMsg& get(DataSet& dcmDataset);
  /**
  * get DICOM dataset from query results at DCM SCU side.
   * @param nIndex position of query result
   * @param dcmDataset DCM dataset from query result
  * @return true if successful; otherwise FALSE.
  */
  bool getQueryInfo(size_t nIndex, DataSet& dcmDataset)const;
  bool isWorking();
  
  ///.
  const DPDcmMsg& getInfoOfDcmSCP();
  const DPDcmMsg& action(int nActionID);
  
private:

  /// the instance of DICOM SCU
  CStore* m_pDcmtksCStore;
  /// the status of DICOM service at DICOM SCP side.
  DPDcmMsg m_DPDcmMsg;
  
  //////////////////////////////////
  ///. dead function
  /////////////////////////////////
  
  ///. study level keys
  struct QR_Study_Info {
    DataSet::TDcmDate dtStudyDate;  //R
    DataSet::TDcmDate dtDateEnd;
    DataSet::TDcmTime tmStudyTime;  //R
    char strAccessionNumber[16];      //R
    char strPatientName[64];        //R
    char strPatientID[64];          //R
    char strStudyID[16];          //R
    char strStudyInstanceUID[64];     //U
    unsigned long lNumOfStudyRelatedSeries; //O
    char strRetrieveAETitle[16];
  };
  /**
  * request CFind/CMove/CGet service at DICOM SCP side.
  * @param infoStudyRequest: the keys of query request at the Study Information level of the Study Root Query/Retrieve Information Model.
  * @return status of the service at DCM SCP side.
  */
  const DPDcmMsg& find(const QR_Study_Info& infoStudyRequest);
  const DPDcmMsg& move(const QR_Study_Info& infoStudyRequest);
  const DPDcmMsg& get(const QR_Study_Info& infoStudyRequest);
  /**
  * get query result by its index.
  * @param nIndex: the index of query result.
  * @param infoStudyCond: the keys of query response at the Study Information level.
  * @return true if successful; otherwise FALSE.
  */
  bool getQueryInfo(size_t nIndex, QR_Study_Info& infoStudy);
  
  ///. series level keys
  struct QR_Series_Info {
    char strModality[16];         //R
    char strSeriesNumber[16];       //R
    char strSeriesInstanceUID[64];    //U
    unsigned long lNumOfSeriesRelatedInstances; //O
    char strStudyInstanceUID[64];   //O
    char strRetrieveAETitle[16];
    DataSet::TDcmDate dtSeriesDate;
    DataSet::TDcmTime tmSeriesTime;
  };
  /**
  * request CFind/CMove/CGet service at DICOM SCP side.
  * @param infoSeriesRequest: the keys of query request at the Series Information level of the Study Root Query/Retrieve Information Model.
  * @return status of the service at DCM SCP side.
  */
  const DPDcmMsg& find(const QR_Series_Info& infoSeriesRequest);
  const DPDcmMsg& move(const QR_Series_Info& infoSeriesRequest);
  const DPDcmMsg& get(const QR_Series_Info& infoSeriesRequest);
  /**
  * get query result by its index.
  * @param nIndex: the index of query result.
  * @param infoSeries: the keys of query response at the series Information level.
  * @return true if successful; otherwise FALSE.
  */
  bool getQueryInfo(size_t nIndex, QR_Series_Info& infoSeries);
  
  ///. image level keys
  struct QR_Image_Info {
    unsigned long lInstanceNumber;  //R
    char strSOPInstanceUID[64];     //U
    char strSOPClassUID[64];      //O
    char strContainerIdentifier[64];//O
    char strSeriesInstanceUID[64];  //O
    char strStudyInstanceUID[64]; //O
    char strRetrieveAETitle[16];
    ///. true if it is a Tiff file
    bool bJPIP;
    long lReferencedFrameNumber;
//        ///. unproved
//        char strTarget[256];
//        long lFullSize[2], lRegionSize[2], lRegionOff[2], lImageLevel;
//        int nRoundType;

    QR_Image_Info()
      : bJPIP(false) {
    }
  };
  /**
  * request CFind/CMove/CGet service at DICOM SCP side.
  * @param infoImageRequest: the keys of query request at the Image Information level of the Study Root Query/Retrieve Information Model.
  * @return status of the service at DCM SCP side.
  */
  const DPDcmMsg& find(const QR_Image_Info& infoImageRequest);
  const DPDcmMsg& move(const QR_Image_Info& infoImageRequest);
  const DPDcmMsg& get(const QR_Image_Info& infoImageRequest);
  const DPDcmMsg& del(const QR_Image_Info& infoImageRequest);
  /**
  * get query result by its index.
  * @param nIndex: the index of query result.
  * @param infoImage: the keys of query response at the Image Information level.
  * @return true if successful; otherwise FALSE.
  */
  bool getQueryInfo(size_t nIndex, QR_Image_Info& infoImage);
  
  /// initialize DCM dataset with Request keys
  void onGenerateDcmDataset(const QR_Image_Info& infoImageRequest,
                            DcmDataset& dcmDataset);
  void onGenerateDcmDataset(const QR_Series_Info& infoSeriesRequest,
                            DcmDataset& dcmDataset);
  void onGenerateDcmDataset(const QR_Study_Info& infoStudyRequest,
                            DcmDataset& dcmDataset);
};


#endif // DPDCMSCUUI_H
