/// Brief description
/// The user interface of DPDicomlib to read/write DICOM file
//
/// Detailed description
/// 1. load DICOM dataset from local file, or DICOM dataset memory.
/// 2. translate XML file into DICOM dataset
/// 3. save DICOM dataset in different transfer syntax.
/// 4. find and get specifc information in DICOM dataset
/// 5. set specific information in DICOM dataset

#ifndef DPDCMDATASETUI_H
#define DPDCMDATASETUI_H

#include "Global.h"
#include "DllApi.h"

#define nLenOfVR 4
#define nLenOfTageName 128

class DcmFileFormat;
class DcmDataDictionary;
typedef void* DcmSequenceOfItemsHandle;
typedef void* DcmItemHandle;

/** read, edit, or write DICOM dataset.
 */
class DPTK_DICOMLIB_API DataSet {
  friend class Client;
public:

  ///  It is used to define a specific DICOM element
  struct TDcmTagKey {
    /// group part in tag key
    unsigned short group;
    /// element part in tag key
    unsigned short element;
    
    ///. there is no necessary to fill belowd items
    ///. value multiplicity
    unsigned long lVM;
    ///. Value Representation
    char strVR[nLenOfVR];
    ///. attribute tag name
    char strTagName[nLenOfTageName];
    ///. is this tag a element
    bool bLeaf;
    ///.
    void* pData;
    
    /// constructor
    TDcmTagKey(unsigned short nGroup, unsigned short nElement)
      : group(nGroup)
      , element(nElement)
      , lVM(0)
      , bLeaf(true)
      , pData(NULL) {
      memset(strVR, NULL, nLenOfVR);
      memset(strTagName, NULL, nLenOfTageName);
    }
  };
  
  /// It is used to define a specific day
  struct TDcmDate {
    /// currently stored year value
    unsigned int Year;
    /// currently stored month value
    unsigned int Month;
    /// currently stored day value
    unsigned int Day;
    
    /// constructor
    TDcmDate()
      :Year(1900),Month(1),Day(1)
    {}
  };
  
  /// It is used to define a specific time
  struct TDcmTime {
    /// currently stored hour value
    unsigned int Hour;
    /// currently stored minute value
    unsigned int Minute;
    /// currently stored second value (incl. fraction of seconds)
    double Second;
    /// currently stored time zone value
    double TimeZone;
    
    /// constructor
    TDcmTime()
      :Hour(0),Minute(0),Second(0),TimeZone(0)
    {}
  };
  
public:

  /** default constructor
   */
  DataSet();
  
  /** copy constructor
   *  @param dcmDatasetUI dcm dataset, must not be empty
   */
  DataSet(const DataSet& dcmDatasetUI);
  
  /** destructor
   */
  virtual ~DataSet();
  
  /** copy assignment operator
   *  @param dcmDatasetUI dcm dataset, must not be empty
   */
  DataSet& operator=(const DataSet& dcmDatasetUI);
  
  /** load DICOM dataset from local.
   *  if it is a XML file, DICOM dataset will be created from the XML file
   *  @param pstrFileName name of file to be load.
   *  return 0 if it is OK, 1 if error, failure otherwise
   */
  int load(const char* pstrFileName);
  
  /** load DICOM dataset from memory.
   *  User may change dataset into new transfer syntax.
   *  @param dcmDatasetRoot DCM dataset
   *  @param nFlag new transfer syntax, default will keep original transfer syntax
   *  return 0 if it is OK, 1 if error, otherwise failure
   */
  int load(const DcmItemHandle dcmDatasetRoot, signed int nFlag=-1);
  
  /** load DICOM dictioary for matching tag number and tag name.
   *  @param pstrFileName DCM dictionary file
   *  return true if it is OK, failure otherwise
   */
  bool loadLocalDictionary(const char* pstrFileName);
  
  /** create empty DICOM dataset of specific class.
   *  @param strSOPClassUID DCM SOP class UID
   *  return 0 if it is OK, otherwise failure
   */
  int initialize(const char* strSOPClassUID);
  
  /** find and get transfer syntax of current dcm dataset.
   *  @param pstrTransferSyntaxUID DCM transfer syntax UID
   *  @param nLen string length
   *  return transfer syntax ID, -1 when EXS_Unknown
   */
  int findAndGetTransferSyntaxUID(char* pstrTransferSyntaxUID,
                                  unsigned short nLen);
                                  
  /** save current dcm dataset in specific transfer syntax.
   *  EXS_LittleEndianImplicit = 0;
   *  EXS_BigEndianImplicit = 1;
   *  EXS_LittleEndianExplicit = 2,
   *  EXS_BigEndianExplicit = 3,
   *  @param pstrFileName DCM file name
   *  @param uFlag transfer syntax
   *  return 0 if it is OK, failure otherwise
   */
  int save(const char* pstrFileName, unsigned int uFlag=0);
  
  /** save current dcm dataset in specific transfer syntax.
   *  EXS_JPEG2000LosslessOnly = 26,
   *  EXS_JPEG2000 = 27,
   *  EXS_JPIPReferenced = 32,
   *  EXS_JPIPReferencedDeflate = 33,
   *  @param pstrFileName DCM file name
   *  @param uFlag transfer syntax
   *  return 0 if it is OK, failure otherwise
   */
  int saveWithDecoding(const char* strFileName, unsigned int uFlag=0);
  
  /** get the handle of current DICOM dataset.
   *  return the handle of DICOM dataset
   */
  const DcmItemHandle getRoot();
  
  /** check if current DICOM dataset is encapsulated.
   *  return ture if encapsulated, false otherwise
   */
  bool isEncapsulated();
  
  /** check if current DICOM dataset has pixeldata.
   *  return ture if there is pixel data, false otherwise
   */
  bool isPixelData();
  
  /** get number of DICOM elements under specific DICOM item.
   *  @param pItem DCM item
   *  return number of DICOM elements
   */
  long getNumOfElements(const DcmItemHandle pItem);
  
  /** get DICOM element with the same position under specific DICOM item.
   *  @param pItem DCM item
   *  @param lIndex position of DCM element under DCM item
   *  @param tagKey structure of DCM element under DCM item
   *  return data length of DICOM element
   */
  long getElementFromItem(
    const DcmItemHandle pItem, unsigned long lIndex, TDcmTagKey& tagKey
  );
  
  /** get data of first DICOM element with the same tag under specific DICOM item.
   *  @param pItem DCM item
   *  @param tagKey structure of DCM element under DCM item
   *  @param bChild if searching sub items under DCM item
   *  @param lPos position of data in DCM element under DCM item
   *  @param pBuf data in DCM element
   *  @param lLenOfBuf length of data in DCM element
   *  return data length of DICOM element
   */
  long getFirstElementFromItem(
    const DcmItemHandle pItem, const TDcmTagKey& tagKey, bool bChild, long lPos,
    char* pBuf, unsigned long lLenOfBuf
  );
  
  /** get the data of DICOM element with the same tag under specific DICOM item.
   *  @param pItem DCM item
   *  @param tagKey structure of DCM element under DCM item
   *  @param dtValue data of DCM element under DCM item
   *  @param pos position of data in DCM element under DCM item
   *  @param bChild if searching sub items under DCM item
   *  return true if it is OK, failure otherwise
   */
  bool findAndGetDate(const DcmItemHandle pItem, const TDcmTagKey& tagKey,
                      TDcmDate& dtValue, unsigned long pos, bool bChild=true);
                      
  /** get the data of DICOM element with the same tag under specific DICOM item.
   *  @param pItem DCM item
   *  @param tagKey structure of DCM element under DCM item
   *  @param tmValue data of DCM element under DCM item
   *  @param pos position of data in DCM element under DCM item
   *  @param bChild if searching sub items under DCM item
   *  return true if it is OK, failure otherwise
   */
  bool findAndGetTime(const DcmItemHandle pItem, const TDcmTagKey& tagKey,
                      TDcmTime& tmValue, unsigned long pos, bool bChild=true);
                      
  /** get the data of DICOM element with the same tag under specific DICOM item.
   *  @param pItem DCM item
   *  @param tagKey structure of DCM element under DCM item
   *  @param lPos position of data in DCM element under DCM item
   *  @param bChild if searching sub items under DCM item
   *  return data of DICOM element
   */
  short findAndGetShort(const DcmItemHandle pItem, const TDcmTagKey& tagKey,
                        unsigned long lPos, bool bChild=true);
  long  findAndGetLong(const DcmItemHandle pItem, const TDcmTagKey& tagKey,
                       unsigned long lPos, bool bChild=true);
  float findAndGetFloat(const DcmItemHandle pItem, const TDcmTagKey& tagKey,
                        unsigned long lPos, bool bChild=true);
  double  findAndGetDouble(const DcmItemHandle pItem, const TDcmTagKey& tagKey,
                           unsigned long lPos, bool bChild=true);
                           
  /** get the data of DICOM element with the same tag under specific DICOM item.
   *  @param pItem DCM item
   *  @param tagKey structure of DCM element under DCM item
   *  @param strValue data in DCM element
   *  @param lPos position of data in DCM element
   *  @param bChild if searching sub items under DCM item
   *  return true if it is OK, failure otherwise
   */
  bool findAndGetString(const DcmItemHandle pItem, const TDcmTagKey& tagKey,
                        char* strValue, unsigned long pos, bool bChild=true);
                        
  /** get the data of DICOM element with the same tag under specific DICOM item.
   *  @param pItem DCM item
   *  @param tagKey structure of DCM element under DCM item
   *  @param pValue data in DCM element
   *  @param nLen length of data in DCM element
   *  @param bChild if searching sub items under DCM item
   *  return true if it is OK, failure otherwise
   */
  bool findAndGetOW(const DcmItemHandle pItem, const TDcmTagKey& tagKey,
                    short* pValue, int nLen, bool bChild=true);
                    
  /** create DICOM element with the same tag under specific DICOM item.
   *  @param pItem DCM item
   *  @param tagKey structure of DCM element under DCM item
   *  @param pfValue data in DCM element
   *  @param nCount number of data in DCM element
   *  @param bReplaced if there is already a DCM element under DCM item
   *  return 0 if it is OK, failure otherwise
   */
  int setDoubleValue(const DcmItemHandle pItem, const TDcmTagKey& tagKey,
                     double* pfValue, int nCount, bool bReplaced=true);
                     
  /** create DICOM element with the same tag under specific DICOM item.
   *  @param pItem DCM item
   *  @param tagKey structure of DCM element under DCM item
   *  @param plValue data in DCM element
   *  @param nCount number of data in DCM element
   *  @param bReplaced if there is already a DCM element under DCM item
   *  return 0 if it is OK, failure otherwise
   */
  int setLongValue(const DcmItemHandle pItem, const TDcmTagKey& tagKey,
                   long* plValue, int nCount, bool bReplaced=true);
                   
  /** create DICOM element with the same tag under specific DICOM item.
   *  @param pItem DCM item
   *  @param tagKey structure of DCM element under DCM item
   *  @param pValue data in DCM element
   *  @param nCount number of data in DCM element
   *  @param bReplaced if there is already a DCM element under DCM item
   *  return 0 if it is OK, failure otherwise
   */
  int setShortValue(const DcmItemHandle pItem, const TDcmTagKey& tagKey,
                    const short* pValue, int nCount, bool bReplaced=true);
                    
  /** create DICOM element with the same tag under specific DICOM item.
   *  @param pItem DCM item
   *  @param tagKey structure of DCM element under DCM item
   *  @param pValue data in DCM element
   *  @param bReplaced if there is already a DCM element under DCM item
   *  return 0 if it is OK, failure otherwise
   */
  int setStringValue(const DcmItemHandle pItem, const TDcmTagKey& tagKey,
                     const char* pValue, bool bReplaced=true);
                     
  /** create DICOM element with the same tag under specific DICOM item.
   *  @param pItem DCM item
   *  @param tagKey structure of DCM element under DCM item
   *  @param dtValue data in DCM element
   *  @param bReplaced if there is already a DCM element under DCM item
   *  return 0 if it is OK, failure otherwise
   */
  int setDateValue(const DcmItemHandle pItem, const TDcmTagKey& tagKey,
                   const TDcmDate& dtValue, bool bReplaced=true);
                   
  /** create DICOM element with the same tag under specific DICOM item.
   *  @param pItem DCM item
   *  @param tagKey structure of DCM element under DCM item
   *  @param dtValue data in DCM element
   *  @param bReplaced if there is already a DCM element under DCM item
   *  return 0 if it is OK, failure otherwise
   */
  int setTimeValue(const DcmItemHandle pItem, const TDcmTagKey& tagKey,
                   const TDcmTime& dtValue, bool bReplaced=true);
                   
  /** get DICOM sequence with the same tag under specific DICOM item.
   *  @param pItem DCM item
   *  @param tagKey structure of DCM sequence under DCM item
   *  @param bChild if searching sub items under DCM item
   *  return DICOM sequence, NULL otherwise
   */
  DcmSequenceOfItemsHandle findSequence(const DcmItemHandle pItem,
                                        const TDcmTagKey& tagKey, bool bChild=true);
                                        
  /** insert DICOM sequence under specific DICOM item.
   *  @param pItem DCM item
   *  @param pSeq DCM sequence
   *  return 0 if it is OK, failure otherwise
   */
  int inserSequence(const DcmItemHandle pItem, DcmSequenceOfItemsHandle pSeq);
  
  /** get number of DICOM items under specific DICOM sequence.
   *  @param pSequence DCM sequence
   *  return number of DICOM items, -1 if pSequence is NULL
   */
  long getNumOfItems(DcmSequenceOfItemsHandle pSequence);
  
  /** get DICOM item with the same index under specific DICOM sequence.
   *  @param pSequence DCM sequence
   *  @param lIndex position of DCM item under DCM sequence
   *  return DICOM item
   */
  const DcmItemHandle getItemFromSeq(
    DcmSequenceOfItemsHandle pSequence, unsigned long lIndex
  );
  
  /** get sub DICOM item with the same index under tag-defined DICOM sequence under specific DICOM item.
   *  If there is no such item, create one and return it
   *  @param pItemRoot DCM item
   *  @param tagKeySeq tag of DCM sequence
   *  @param itemNum number of the item to be searched for (0..n-1, -1 for last, -2 for append new)
   *  return sub DICOM item
   */
  const DcmItemHandle findOrCreateSequenceItem(const DcmItemHandle pItemRoot,
      const TDcmTagKey& tagKeySeq, long itemNum);
      
  /** copy DICOM item into tag-defined DICOM sequence under specific DICOM item.
   *  If there is no such item, create one and return it
   *  @param pItemSrc source DCM item
   *  @param tagSeqKey tag of DCM sequence
   *  @param pItemDest destination DCM item
   *  return 0 if it is OK, failure otherwise
   */
  int copyAndInsertSeqItem(const DcmItemHandle pItemSrc,
                           const TDcmTagKey& tagSeqKey, DcmItemHandle pItemDest);
                           
  int removeElement(const DcmItemHandle pItemRoot, const TDcmTagKey& tagKey);
  int removeSequenceItem(const DcmItemHandle pItemRoot,
                         const TDcmTagKey& tagKeySeq, long lItemNum);
  int setPixelData(const DcmItemHandle pItemRoot, const TDcmTagKey& tagKey,
                   const char* pBuffer, unsigned long lLenOfBuffer, bool bReplaced=true);
                   
  ///.
  DcmSequenceOfItemsHandle findPixelSequence(const DcmItemHandle pItemRoot,
      bool bSearchSub=false, int nPreferTransferSyntax=-1,
      unsigned long lNumOfItems=0);
  int createPixelItemFromObserver(const DcmSequenceOfItemsHandle pDcmPixelSeq,
                                  DPDcmObserver* pDPDcmObserver, const char* strFileName, int nLevelIndex,
                                  int nTileIndex, unsigned long lLenOfBuf);
  int getPixelItem(const DcmSequenceOfItemsHandle pDcmPixelSeq,
                   unsigned long lIndexOfItem, char* pBuffer, unsigned long& lLenOfBuf);
                   
private:

  /// current instance of DCM dataset.
  DcmFileFormat* m_pDcmFormat;
  
  /// the instance of DCM dictionary.
  DcmDataDictionary* m_pDcmDataDictionary;
  
  ///////////////////////////////////////////////////////////////////////
  ///. functions not be used.
  int insertPixelItem(const DcmSequenceOfItemsHandle pDcmPixelSeq,
                      const char* pBuffer, unsigned long lLenOfBuffer, long lPosOfItem=-1);
  int loadAllDataInMemory(const DcmSequenceOfItemsHandle pDcmPixelSeq);
};


class DPTK_DICOMLIB_API RequestFactory {

public:
  ///. study level keys
  static DataSet getDataSetOfRequest(
    const DataSet::TDcmDate& dtStudyDate,
    const DataSet::TDcmDate& dtDateEnd,
    const char* pstrAccessionNumber,
    const char* pstrPatientName,
    const char* pstrPatientID,
    const char* pstrStudyID,
    const char* pstrStudyInstanceUID,
    const char* pstrRetrieveAETitle
  );
  ///. series level keys
  static DataSet getDataSetOfRequest(
    const char* pstrModality,
    const char* pstrSeriesNumber,
    const char* pstrSeriesInstanceUID,
    const char* pstrStudyInstanceUID,
    const char* pstrRetrieveAETitle
  );
  ///. image level keys
  static DataSet getDataSetOfRequest(
    const char* pstrSOPInstanceUID,
    const char* pstrSOPClassUID,
    const char* pstrSeriesInstanceUID,
    const char* pstrStudyInstanceUID,
    const char* pstrRetrieveAETitle,
    long lReferencedFrameNumber
  );
  
};

#endif // DPDCMDATASETUI_H
