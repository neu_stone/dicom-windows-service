#ifndef DPDICOMLIB_GLOBAL_H
#define DPDICOMLIB_GLOBAL_H

#include "DllApi.h"
#include <memory.h>
#ifndef NULL
#define NULL 0
#endif

/** Type of messages, DPDicomLib will send DCM message to its observer.
 */
enum TYPE_DCM_MSG {
  TYPE_MSG_NOTHING = 0x0000,  /* none of the rest !! */
  
  ///. type of request message
  TYPE_MSG_C_STORE_RQ = 0x0001, /* also known as C_SEND_RQ  */
  TYPE_MSG_C_GET_RQ   = 0x0010,
  TYPE_MSG_C_FIND_RQ  = 0x0020,
  TYPE_MSG_C_ECHO_RQ  = 0x0030,
  TYPE_MSG_C_MOVE_RQ  = 0x0021,
  ///.
  TYPE_MSG_C_CANCEL_RQ =  0x0fff,
  
  ///. type of association message
  TYPE_MSG_ASSOCIATE_RQ   = 0x1000,
  TYPE_MSG_ASSOCIATE_AC   = 0x1001,
  TYPE_MSG_ASSOCIATE_RJ   = 0x1002,
  TYPE_MSG_RELEASE_RQ     = 0x1003,
  TYPE_MSG_RELEASE_RSP    = 0x1004,
  TYPE_MSG_ABORT          = 0x1005,
  TYPE_MSG_CLOSE          = 0x1006,
  
  ///. type of response message
  TYPE_MSG_C_STORE_RSP    = 0x8001, /* also known as C_SEND_RSP */
  TYPE_MSG_C_GET_RSP      = 0x8010,
  TYPE_MSG_C_FIND_RSP     = 0x8020,
  TYPE_MSG_C_MOVE_RSP     = 0x8021,
  TYPE_MSG_C_ECHO_RSP     = 0x8030,
  /* there is no TYPE_MSG_C_CANCEL_RSP */
  
  ///. type of DPTK internal message
  TYPE_MSG_DPTK_LOCALIZER     = 0x9030,
  TYPE_MSG_DPTK_DELETE_FILE   = 0x9031,
  TYPE_MSG_DPTK_FILE_BUFFER   = 0x9032,
  TYPE_MSG_N_ACTION_RQ_TURNON     = 0x0131,
  TYPE_MSG_N_ACTION_RQ_TURNOFF    = 0x0133,
  
  /// so far, we do not use following messages.
  TYPE_MSG_N_EVENT_REPORT_RQ  = 0x0100,
  TYPE_MSG_N_EVENT_REPORT_RSP = 0x8100,
  TYPE_MSG_N_GET_RQ = 0x0110,
  TYPE_MSG_N_GET_RSP = 0x8110,
  TYPE_MSG_N_SET_RQ = 0x0120,
  TYPE_MSG_N_SET_RSP = 0x8120,
  TYPE_MSG_N_ACTION_RQ = 0x0130,
  TYPE_MSG_N_ACTION_RSP = 0x8130,
  TYPE_MSG_N_CREATE_RQ = 0x0140,
  TYPE_MSG_N_CREATE_RSP = 0x8140,
  TYPE_MSG_N_DELETE_RQ = 0x0150,
  TYPE_MSG_N_DELETE_RSP = 0x8150,
  
};

/** the role of current object.
 */
enum TYPE_DCM_ROLE {
  TYPE_ROLE_NULL=0,
  TYPE_ROLE_DCM_SCP,
  TYPE_ROLE_DCM_PRINTEE,
  TYPE_ROLE_DCM_STOREE,
};

/** the status of current service.
 */
enum TYPE_DCM_STATUS {
  TYPE_STATUS_SUCCESS=0,
  TYPE_STATUS_PENDING,
  TYPE_STATUS_WARNING,
  TYPE_STATUS_FAILURE,
  TYPE_STATUS_DATA_ERROR,
};

/** the message from DPDicomLib.
 */
struct DPDcmMsg {
public:
  /// the role of message sender
  TYPE_DCM_ROLE nTypeRole;
  /// the status of current service
  TYPE_DCM_STATUS nTypeStatus;
  /// session ID of current service
  unsigned short nSessionID;
  /// SCP AE title
  char* pstrCalledAE;
  /// SCU AE title
  char* pstrCallingAE;
  /// SCU IP address
  char* pstrCallingIP;
  /// DICOM dataset, normally from DCM SCU
  void* pDatasetReq;
};

/** abstract class of DPDicomLib observer.
 */
class DPTK_DICOMLIB_API DPDcmObserver {
public:
  /**
  * Constructor.
  */
  DPDcmObserver() {};
  
  /**
  * Destructor.
  */
  virtual ~DPDcmObserver() {};
  
  /**
  * Receive messages from DPDicomLib.
  * @param nType message type.
  * @param msgDPDcm message
  * @return : 0 if successful; otherwise failure.
  */
  virtual int notify(TYPE_DCM_MSG nType, DPDcmMsg& msgDPDcm)const = 0;
};

#endif ///. DPDICOMLIB_GLOBAL_H
