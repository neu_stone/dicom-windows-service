#ifndef SEDEEN_SRC_GLOBAL_GEOMETRY_H
#define SEDEEN_SRC_GLOBAL_GEOMETRY_H

#ifdef WIN32
#include <cfloat>
#endif

// STL includes
#include <cmath>
#include <limits>

// Qt includes
#include <QPointF>

// User includes
#include "global/UnitDefs.h"
#include "global/DllApi.h"

namespace sedeen {
namespace geometry {

/// Calculates the distance between two points.
//
/// @param  p1 the first point
/// @param  p2 the second point
/// @return the distance between points
SEDEEN_GLOBAL_API
double l2Norm(const QPointF& p1, const QPointF& p2);

/// Calculates the magnitude of a point.
//
/// @param  p the point
/// @return the magnitude
SEDEEN_GLOBAL_API
double magnitude(const QPointF& p);

/// Calculates the center of a line defined by two points.
//
/// @param  p1 the first points
/// @param  p2 the second points
/// @return the centre of the line defined by two points.
SEDEEN_GLOBAL_API
QPointF lineCenter(const QPointF& p1, const QPointF& p2);

/// Detects if the value is NaN.
//
/// \note This is not a portable solution. Don't use this. There is a standard
/// method to address this in C++11, but not before.
SEDEEN_GLOBAL_API
bool my_isnan(const double &val);

/// Detects if a number is +/- infinity.
//
/// \return TRUE iff the platform has a representation for infinity and this
/// value matches it. FALSE otherwise.
SEDEEN_GLOBAL_API
bool isinf(const double &val);

} // namespace geometry
} // namespace sedeen

#endif
