// Primary header
#include "SRTTransform.h"

// System includes
#include <cmath>
#include <iostream>

// User includes
#include "global/Geometry.h"
#include "global/UnitDefs.h"
#include "global/Debug.h"

namespace sedeen {

SRTTransform::SRTTransform()
    : _qtransform(),
      _dirty(false),
      _tx(0.), _ty(0.),
      _sx(1.), _sy(1.),
      _cx(0.), _cy(0.),
      _phi(0.),
      _cosv(1.),
      _sinv(0.)
{}

SRTTransform::SRTTransform(const double tx, const double ty, const double sx,
                           const double sy, const double phi, const double cx,
                           const double cy)
    : _qtransform(),
      _dirty(false),
      _tx(tx), _ty(ty),
      _sx(sx), _sy(sy),
      _cx(cx), _cy(cy),
      _phi(phi),
      _cosv( std::cos(phi / angle::rad) ),
      _sinv( std::sin(phi / angle::rad) )
{}

SRTTransform::~SRTTransform() {}

const QTransform &SRTTransform::qTransform() const {
  if (_dirty) {
    // combine transform
    // [-center] [scale] [rotate] [+center] [translate]
    // in reverse order
    _dirty = false;
    _qtransform.reset();
    _qtransform.translate(+_tx, +_ty)
        .translate(+_cx, +_cy)
        .rotate(_phi)
        .scale(_sx, _sy)
        .translate(-_cx, -_cy);
  }

  return _qtransform;
}

void SRTTransform::reset() {
  _cx = 0.;	_cy = 0.;
  _tx = 0.;	_ty = 0.;
  _sx = 1.;	_sy = 1.;
  _phi = 0.;
  _cosv = 1.;   _sinv = 0.;

  _qtransform.reset();
  _dirty = false;
}

bool SRTTransform::operator==(const SRTTransform &right) const {
  // check for self assignment
  if (this == &right) {
    return true;
  } else if (this->translation() == right.translation() &&
             this->center() == right.center() &&
             this->rotation() == right.rotation() &&
             this->scale() == right.scale()) {
    return true;
  } else {
    return false;
  }
}

void SRTTransform::forward(QPointF &point) const {
  double x = point.rx() - _cx;
  double y = point.ry() - _cy;

  x *= _sx;
  y *= _sy;

  point.rx() = ( _cosv*x - _sinv*y) + _cx + _tx;
  point.ry() = ( _sinv*x + _cosv*y) + _cy + _ty;
}

void SRTTransform::inverse(QPointF &point) const {
  double x = point.rx() - _cx - _tx;
  double y = point.ry() - _cy - _ty;

  double x1 = ( _cosv*x + _sinv*y);
  double y1 = (-_sinv*x + _cosv*y);

  point.rx() = x1 / _sx + _cx;
  point.ry() = y1 / _sy + _cy;
}

void SRTTransform::setCenter(const QPointF &center) {
  setCenter(center.x(), center.y());
}

void SRTTransform::setCenter(const double x, const double y) {
  _cx = x;
  _cy = y;
  _dirty = true;;
}

void SRTTransform::moveCenter(const double delx, const double dely) {
  setCenter(_cx + delx, _cy + dely);
}

QPointF SRTTransform::center() const {
  return QPointF(_cx, _cy);
}

void SRTTransform::setRotation(const double degree) {
  _phi = degree;
  _phi = _phi -  std::floor(_phi/360) * 360;

  double rad = degree / angle::rad;
  _cosv = std::cos(rad);
  _sinv = std::sin(rad);

  _dirty = true;;
}

void SRTTransform::addRotation(const double degree) {
  setRotation( rotation() + degree );
}

void SRTTransform::setRotation(const double degree, const QPointF &center) {
  setCenter(center);
  setRotation(degree);
}

double SRTTransform::rotation() const {
  return _phi;
}

void SRTTransform::setTranslation(const QPointF &trans) {
  setTranslation(trans.x(), trans.y());
}

void SRTTransform::setTranslation(const double tx, const double ty) {
  _tx = tx;
  _ty = ty;
  _dirty = true;;
}

void SRTTransform::addTranslation(const double delx, const double dely) {
  _tx += delx;
  _ty += dely;
  _dirty = true;;
}

void SRTTransform::addTranslation(const QPointF &del) {
  addTranslation(del.x(), del.y());
}

QPointF SRTTransform::translation() const {
  return QPointF(_tx, _ty);
}

void SRTTransform::scaleBy(const double xf, const double yf) {
  _sx *= xf;
  _sy *= yf;
  _dirty = true;;
}

void SRTTransform::setScale(const double sx, const double sy) {
  _sx = sx;
  _sy = sy;
  _dirty = true;;
}

void SRTTransform::setScale(const QSizeF &scale) {
  setScale(scale.width(), scale.height());
}

QSizeF SRTTransform::scale() const {
  return QSizeF(_sx, _sy);
}

bool SRTTransform::readXml(QXmlStreamReader &reader, SRTTransform &tx) {
  // check reader
  if (reader.atEnd() || reader.hasError())
    return false;

  /* Read file */
  while (!reader.atEnd()) {
    /* Read next token */
    if (reader.isWhitespace()) {
      reader.readNext();
      continue;
    }

    /* Check Start Elements */
    if (reader.isStartElement()) {
      QString tagname = reader.name().toString();

      if (tagname.compare("Transform", Qt::CaseInsensitive) == 0) {
        reader.readNext();
        return readXmlMain(reader, tx);
      }
    }
    // read next element
    reader.readNext();
  }
  return false;
}

bool SRTTransform::readXmlMain(QXmlStreamReader &reader, SRTTransform &tx) {
  double x, y, z;
  SRTTransform t;
  int tagsFound = 0;

  // check reader
  if (reader.atEnd() || reader.hasError())
    return false;

  /* Read file */
  while (!reader.atEnd()) {
    if (reader.isWhitespace()) {
      /* Read next token */
    } else if (reader.isStartElement()) {
      /* Check Start Elements */
      QString tagname = reader.name().toString();

      /* Look for transform tag */
      if (tagname.compare("Translation", Qt::CaseInsensitive) == 0) {
        if (readTiplet(reader, x, y, z)) {
          t.setTranslation(x,y);
        } else {
          break;
        }
        tagsFound++;
      } else if (tagname.compare("Rotation", Qt::CaseInsensitive) == 0) {
        if (readTiplet(reader, x, y, z)) {
          t.setRotation(z);
        } else {
          break;
        }
        tagsFound++;
      } else if (tagname.compare("Scale", Qt::CaseInsensitive) == 0) {
        if (readTiplet(reader, x, y, z)) {
          t.setScale(x,y);
        } else {
          break;
        }
        tagsFound++;
      } else if (tagname.compare("Center", Qt::CaseInsensitive) == 0) {
        if (readTiplet(reader, x, y, z)) {
          t.setCenter(x,y);
        } else {
          break;
        }
        tagsFound++;
      } else {
        debug::warning(
            (std::string("Unknown tag (")+
             tagname.toStdString() +
             std::string(") detected in Transform structure.")).c_str(),
            __FUNCTION__, __LINE__);
      }
    }

    // read next element
    if (tagsFound == 4) {
      break;
    } else {
      reader.readNext();
    }
  }

  // check for errors
  if (reader.hasError()) {
    debug::warning("Error in XML stream.", __FUNCTION__, __LINE__);
    return false;
  } else if (tagsFound < 4) {
    debug::warning("Some tags not found Transform structure.",
                   __FUNCTION__, __LINE__);
    return false;
  } else {
    tx = t;
    return true;
  }
}

bool SRTTransform::writeXml(QXmlStreamWriter &writer) const {
  // error check
  if (writer.device() == 0)
    return false;

  writer.writeStartElement("Transform");

  writer.writeStartElement("Translation");
  writer.writeAttribute("units", "um");
  writer.writeTextElement("x",  QString("%1").arg(translation().x()));
  writer.writeTextElement("y",  QString("%1").arg(translation().y()));
  writer.writeTextElement("z", "0.0");
  writer.writeEndElement();

  writer.writeStartElement("Rotation");
  writer.writeAttribute("units", "degree");
  writer.writeAttribute("direction", "clockwise");
  writer.writeTextElement("x", "0");
  writer.writeTextElement("y", "0");
  writer.writeTextElement("z",  QString("%1").arg(rotation()));
  writer.writeEndElement();

  writer.writeStartElement("Scale");
  writer.writeTextElement("x", QString("%1").arg(scale().width()));
  writer.writeTextElement("y", QString("%1").arg(scale().height()));
  writer.writeTextElement("z", "1.0");
  writer.writeEndElement();

  writer.writeStartElement("Center");
  writer.writeAttribute("units", "um");
  writer.writeTextElement("x", QString("%1").arg(center().x()));
  writer.writeTextElement("y", QString("%1").arg(center().y()));
  writer.writeTextElement("z", "0.0");
  writer.writeEndElement();

  writer.writeEndElement();

  return true;
}

bool SRTTransform::readTiplet(QXmlStreamReader &reader, double &x, double &y,
                              double &z) {
  reader.readNext();reader.readNext();
  if (reader.name().toString().compare("x", Qt::CaseInsensitive) != 0)
    return false;
  x = reader.readElementText().toDouble();

  reader.readNext();reader.readNext();
  if (reader.name().toString().compare("y", Qt::CaseInsensitive) != 0)
    return false;
  y = reader.readElementText().toDouble();

  reader.readNext();reader.readNext();
  if (reader.name().toString().compare("z", Qt::CaseInsensitive) != 0)
    return false;
  z = reader.readElementText().toDouble();

  return true;
}

void transform(const SRTTransform &trans, std::vector<QPointF> &vec,
               TransformDirection dir) {
  /* Calculates transformation */
  if (Inverse == dir) {
    for (uint i = 0; i < vec.size(); i++) {
      trans.inverse(vec[i]);
    }
  } else {
    for (uint i = 0; i < vec.size(); i++) {
      trans.forward(vec[i]);
    }
  }
}

void transform(const SRTTransform &trans, QVector<QPointF> &vec,
               TransformDirection dir) {
  /* Calculates transformation */
  if (Inverse == dir) {
    for (int i = 0; i < vec.size(); i++) {
      trans.inverse(vec[i]);
    }
  } else {
    for (int i = 0; i < vec.size(); i++) {
      trans.forward(vec[i]);
    }
  }
}

void transform(const SRTTransform &trans, QPolygonF &poly,
               TransformDirection dir) {
  /* Calculates transformation */
  if (Inverse == dir) {
    for (int i = 0; i < poly.count(); i++) {
      trans.inverse(poly[i]);
    }
  } else {
    for (int i = 0; i < poly.count(); i++) {
      trans.forward(poly[i]);
    }
  }
}

void transform(const SRTTransform &trans, QPointF &point,
               TransformDirection dir) {
  if (Inverse == dir)
    trans.inverse(point);
  else
    trans.forward(point);
}

QPointF transformed(const SRTTransform &trans, const QPointF &point,
                    TransformDirection dir)
{
  QPointF p = point;
  if (Inverse == dir)
    trans.inverse(p);
  else
    trans.forward(p);
  return p;
}

} // namespace sedeen
