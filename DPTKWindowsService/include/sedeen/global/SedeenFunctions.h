#ifndef SEDEEN_SRC_GLOBAL_SEDEENFUNCTIONS_H
#define SEDEEN_SRC_GLOBAL_SEDEENFUNCTIONS_H

#include <QString>
#include <QObject>
#include <QWidget>

#include "global/DllApi.h"

namespace sedeen {
    /*!
      \brief Checks if the path exists.

      If it does not, it will ask to create it.

      \param  path the path to check
      \param parent
      \return true if exists or if it's created, false otherwise
    */
SEDEEN_GLOBAL_API
bool pathExists(const QString path, QWidget *parent = 0);

    /*!
      \brief Gets the name of the Overlay export file for the given image.

      Returns: path(imageName) + "/sedeen/" + base(imageName) + ".roi"

      \param  filename the image of the image
      \return the export filename
    */
SEDEEN_GLOBAL_API
QString getOverlayExportName(const QString &filename);

    /*!
      \brief Gets the name of the Transform export file for the given image.

      Returns: path(imageName) + "/sedeen/" + base(imageName) + ".xml"

      \param  filename the image of the image
      \return the export filename
    */
SEDEEN_GLOBAL_API
QString getTransformExportName(const QString &filename);

    /*!
      \brief Gets the name color list file.

      Returns: application executable path + "colorlist"

      \return the colorlist filename
    */
SEDEEN_GLOBAL_API
QString getColorListExportName();

    /*!
      \brief Returns the filter string for all supported images.
      Useful for the QFileDialog::getOpenFileNames() and related
      functions.

      \return image filter for supported images when using open dialog
    */
SEDEEN_GLOBAL_API
QString openDialogImageFilter();

    /*!
      \brief sets the default loading directory.

      default value is the current app directory (null string).

      \param dir the default directory
    */
SEDEEN_GLOBAL_API
void setLoadDir(const QString &dir);

    /*!
      \brief Gets the default loading directory.

      \return dir the default directory
    */
SEDEEN_GLOBAL_API
const QString& loadDir();

    /*!
      \brief sets the default save directory.

      default value is the current app directory (null string).

      \param dir the default directory
    */
SEDEEN_GLOBAL_API
void setSaveDir(const QString &dir);

    /*!
      \brief Gets the default save directory.

      \return dir the default directory
    */
SEDEEN_GLOBAL_API
const QString& saveDir();

    // /*!
    //   \brief Checks to see if the file exists and is readable.

    //   Also checks to see if a file is readable.
    //   Displays appropriate warnings and errors if file
    //   is not readable in a dialog box.

    //   \param  filename the file name to check
    //   \param  verbose, if true, errors are displayed in a dialog box
    //   \param  parent the parent of the dialog box
    //   \return true if file is ok for reading, false otherwise
    // */
    // bool canReadFile( const QString &line, const bool verbose = true, QWidget *parent = 0);

}   // namespace sedeen

#endif // SEDEEN_UTIL_SEDEENFUNCTIONS_H

