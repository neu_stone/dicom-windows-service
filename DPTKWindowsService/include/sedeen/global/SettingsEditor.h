#ifndef SEDEEN_SRC_GLOBAL_SETTINGSEDITOR_H
#define SEDEEN_SRC_GLOBAL_SETTINGSEDITOR_H

// qt headers
#include <QtGui>

#include "global/DllApi.h"

namespace sedeen {

class SEDEEN_GLOBAL_API SettingsEditor : public QWidget {
  Q_OBJECT;

 public:
  /// \brief General settings editor.
  //
  /// Each SettingsEditor visualized the settings of some object and can modify
  /// the the object's settings via apply.
  SettingsEditor(QWidget* parent = 0);

  virtual ~SettingsEditor();

  /// Saves the current settings of this editor.
  //
  /// \param settings
  ///
  /// \param prefix
  /// The key-prefixes for this editor
  virtual void saveSettings(QSettings& settings, QString prefix) const = 0;

  /// Loads the current settings of this editor.
  //
  /// \param settings
  ///
  /// \param prefix
  /// The key-prefix's for this editor
  ///
  /// \return
  /// \c true on success, \c false otherwise
  virtual void loadSettings(const QSettings& settings, QString prefix) = 0;

 public slots:
  /// Applies changes to the object being managed.
  virtual void apply() = 0;
};

} // namespace sedeen

#endif
