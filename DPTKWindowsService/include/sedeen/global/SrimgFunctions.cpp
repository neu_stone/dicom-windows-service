#include "global/SrimgFunctions.h"

#include <QFileInfo>
#include <QDir>
#include <QObject>
#include <QMessageBox>

// required headers for sleep and usleep
#ifdef WIN32
	#include <windows.h>
#else
	#include <unistd.h>
#endif

namespace sedeen {

void sleep(const unsigned int ms)
{
	#ifdef WIN32
		Sleep(ms);		//milli seconds
	#else
		usleep(ms*1000); //micro secconds
	#endif
}

bool canReadFile( const QString &filename, const bool verbose, QWidget *parent)
{
    QString errMessage, errSubject;
    QFileInfo file( filename );

    // check readability
    if( !file.exists() )
    {
        errMessage = QObject::tr(" Could not find file:\n%1").arg(filename);
        errSubject = QObject::tr("File Not Found");
    }

    // Is file readable
    else if( !file.isReadable() )
    {
        errMessage = QObject::tr(" Could not read file:\n%1").arg(filename);
        errSubject = QObject::tr("Insufficient Permissions");
    }

    // show message
    if( verbose && !errMessage.isEmpty() )
        QMessageBox::critical(parent, errSubject, errMessage);


    //return
    return errMessage.isEmpty();
}

bool canWriteFile( const QString &filename, const bool verbose, QWidget *parent)
{
    QString errMessage, errSubject;
    QFileInfo file( filename);
    QDir dir = file.dir();
    bool error = false;

    // Check if outfile is set
    if( file.baseName().isEmpty() )
    {
        errSubject = QObject::tr("Invalid File");
        errMessage = QObject::tr("Output filename is not set.");
        error = true;
    }

    // If outfile exists - overwirte or not?
    else if( file.exists() )
    {
        if( verbose )
        {
            const int rtn =
                QMessageBox::question(parent, QObject::tr("Overwrite File?"),
                                            QObject::tr("Output file already exists. Do you want to overwrite it?"),
                                            QMessageBox::Yes | QMessageBox::No, QMessageBox::No );
            if( QMessageBox::No == rtn )
            {
                error = true;
            }
        }

        if( !error && !file.isWritable() )
        {
            errSubject = QObject::tr("Permmission Denied");
            errMessage = QObject::tr("Insufficient permission to overwrite file.");
            error = true;
        }
    }

    // Does the directory exist?
    else if( !dir.exists() )
    {
        if( verbose )
        {
            const int rtn =
            QMessageBox::question(parent, QObject::tr("Create Directory?"),
                                        QObject::tr("The output directory does not exist. Do you want to create it?"),
                                        QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes );

            if( QMessageBox::No == rtn )
            {
                error = true;
            }
        }

        if( !error && !dir.mkpath(dir.path()) )
        {
            errSubject = QObject::tr("Permmission Denied");
            errMessage = QObject::tr("Failed to create directory. Maybe due to lack of pcermissions.");
            error = true;
        }
    }

    // Do we have write permission in this directory?
    else if( !file.isWritable() )
    {
        errSubject = QObject::tr("Permmission Denied");
        errMessage = QObject::tr("Failed to write to file. Check directory permissions.");
        error = true;
    }


    // show message
    if( verbose && !errMessage.isEmpty() )
        QMessageBox::critical(parent, errSubject, errMessage);

    //sucess , lets try to write
    return !error;
}


} // namespacesrimg

