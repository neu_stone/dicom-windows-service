#ifndef SEDEEN_SRC_GLOBAL_SRIMGFUNCTIONS_H
#define SEDEEN_SRC_GLOBAL_SRIMGFUNCTIONS_H

#include <QString>
#include <QObject>
#include <QWidget>
#include <boost/shared_ptr.hpp>

#include "global/DllApi.h"

namespace sedeen {

/// Checks to see if the file exists and is readable.
//
/// Also checks to see if a file is readable. Displays appropriate warnings and
/// errors if file is not readable in a dialog box.
///
/// \param filename
/// the file name to check
///
/// \param verbose
/// If true, errors are displayed in a dialog box
///
/// \param parent
/// the parent of the dialog box
///
/// \return
/// \c true if file is ok for reading, \c false otherwise
SEDEEN_GLOBAL_API
bool canReadFile(const QString &filename, const bool verbose = true,
                 QWidget *parent = 0);

/// Checks to see if the given filename can be written.
//
/// If file exists and verbose is on, will ask the user for overwrite
///confirmation, otherwise, function returns true if no other error are
/// encountered.
///
/// Similarly if the file path does not exist and verbose is on, will ask the
/// user for confirmation to create the path, otherwise, the path is created
/// silently.
///
/// \param filename
/// the file name to check
///
/// \param verbose
/// if true, errors are displayed in a dialog box
///
/// \param parent
/// the parent of the dialog box
///
/// \return
/// \c true if file is ok for writing, \c false otherwise
SEDEEN_GLOBAL_API
bool canWriteFile(const QString &filename, const bool verbose = true,
                  QWidget *parent = 0);

/// Causes the thread to sleep for some milli-seconds.
//
/// \param ms
/// the time to sleep (ms)
SEDEEN_GLOBAL_API
void sleep(unsigned int ms);

template<typename T>
void ensureUnique(boost::shared_ptr<T> &sharedPtr);

} // namespace sedeen

#include "SrimgFunctions.icc"

#endif // SEDEEN_GLOBAL_SRIMGFUNCTIONS_H
