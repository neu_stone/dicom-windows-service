#ifndef SEDEEN_SRC_GLOBAL_SRTTRANSFORM_H
#define SEDEEN_SRC_GLOBAL_SRTTRANSFORM_H

// System includes
#include <QtGui>
#include <QtCore>
#include <vector>

#include "global/DllApi.h"

namespace sedeen {

class PhysicalProperties;

/// Transform direction.
enum TransformDirection {
  Forward,
  Inverse
};

/// Performs rotations, scaling and translations (semi-affine).
//
/// Calculates a scaling, rotation, and translation (in order) about a arbitrary
/// center. This is different from QTransform because it does not use a matrix,
/// so all the transformation values can be explicitly set and changed. As a
/// result, transformations CANNOT be easily combined (e.g. T3 = T1  T2),
/// because the resulting transformation isn't easily decomposable in terms of
/// its rotation scale factor, and translation.
///
/// By default, the transform is identity and the center of rotation is the
/// origin. Use setCenter() to change the center of rotation.
///
/// Note that the origin in space is defined as the top-left of image, with y
/// increasing downward and x increasing rightward. This has consequences for
/// rotations - i.e. positive rotation angles produce clockwise rotations.
class SEDEEN_GLOBAL_API SRTTransform {
 public:
  /// Constructor.
  //
  /// Creates identity transform.
  SRTTransform();

  /// Constructor - overloaded.
  //
  /// \param tx
  /// The horizontal translation
  ///
  /// \param ty
  /// The vertical translation
  ///
  /// \param sx
  /// The horizontal scale factor
  ///
  /// \param sy
  /// The horizontal scale factor
  ///
  /// \param phi
  /// The clock-wise rotation angle
  ///
  /// \param cx
  ///
  /// \param cy
  SRTTransform(double tx, double ty, double sx = 1, double sy = 1,
               double phi = 0, double cx = 0, double cy = 0);

  ~SRTTransform();

  /// Equality operator.
  //
  /// \param right
  bool operator==(const SRTTransform& right) const;

  /// Sets the transform to identity.
  void reset();

  /// Gets the QTransform equivalent o this SRTTransform.
  //
  /// \return
  /// The equivalent QTransform
  const QTransform& qTransform() const;

  /// Forward transform applied to a point.
  //
  /// Transformation is calculated at the given center by scaling, rotation, and
  /// translation in the order given.
  ///
  /// \sa inverse()
  ///
  /// \param point
  /// Transforms the point
  void forward(QPointF& point) const;

  /// Inverse transform applied to a point.
  //
  /// \sa forward()
  ///
  /// \param point
  /// transforms the point
  void inverse(QPointF& point) const;

  /// Set the center of rotation. Default is (0,0).
  //
  /// \param center
  void setCenter(const QPointF& center);

  /// Set the center of rotation - overloaded. Default is (0,0).
  //
  /// \param x
  ///
  /// \param y
  void setCenter(double x, double y);

  /// Set the center of rotation. Default is (0,0).
  //
  /// \param delx
  ///
  /// \param dely
  void moveCenter(double delx, double dely);

  /// Gets the center of rotation.
  //
  /// For the current transformation parameters
  QPointF center() const;

  /// Set the clockwise rotation angle (degrees).
  //
  /// Default rotation is zero.
  ///
  /// \param degree
  void setRotation(double degree);

  /// Set the clockwise rotation angle (degrees).
  //
  /// Default rotation is zero.
  ///
  /// \param degree
  void addRotation(double degree);

  /// Set the clockwise rotation angle (degrees) with a cente of rotation.
  //
  /// Default rotation is zero and the center is (0,0)
  ///
  /// \param degree
  ///
  /// \param center
  void setRotation(double degree, const QPointF& center);

  /// Gets the current rotation angle in degrees (clockwise).
  double rotation() const;

  /// Sets the transforms translation in physical space (mm).
  //
  /// \param trans
  void setTranslation(const QPointF& trans);

  /// Sets the transforms translation in physical space (mm) - overloaded.
  //
  /// \param tx
  ///
  /// \param ty
  void setTranslation(double tx, double ty);

  /// Changes the transforms translation in physical space (mm) - overloaded.
  //
  /// \param delx
  ///
  /// \param dely
  void addTranslation(double delx, double dely);

  /// Changes the transforms translation in physical space (mm) - overloaded.
  //
  /// \param del
  void addTranslation(const QPointF& del);

  /// Gets the translation for the current transform in physical space (mm).
  QPointF translation() const;

  /// Adjusts the transforms scale factor - overloaded
  //
  /// \param xf
  /// The x-axis scale factor
  ///
  /// \param yf
  /// The y-axis scale factor
  void scaleBy(double xf, double yf);

  /// Sets the transforms scale factor - overloaded. Default value is unity.
  //
  /// \param sx
  ///
  /// \param sy
  void setScale(double sx, double sy);

  /// Sets the transforms scale factor - overloaded. Default value is unity.
  //
  /// \param scale
  void setScale(const QSizeF& scale);

  /// Gets the transforms current scale factor.
  QSizeF scale() const;

  /// Writes a description of the transform to the stream.
  //
  /// \param writer
  /// The xml stream to write to
  ///
  /// \return
  /// \c true if successful, \c false otherwise
  bool writeXml(QXmlStreamWriter& writer) const;

  /// Reads the stream until a SRTTransform description is found.
  //
  /// Uses readXmlMain to do the work. Fill \a tx with thethe description found.
  /// If errors are detected, \a tx is not modified.
  ///
  /// \param reader
  /// The xml stream to read
  ///
  /// \param tx
  /// The transform object to fill
  ///
  /// \return
  /// \c true on success, \c false otherwise
  static bool readXml(QXmlStreamReader& reader, sedeen::SRTTransform& tx);

  /// Reads the ttriplet of (x,y,z) values from an xml stream.
  //
  /// The values must occur adjacent to each other otherwise an error occurs and
  /// the values are unusable.
  ///
  /// \param reader
  /// The stream to read from
  ///
  /// \param x
  /// The x-value
  ///
  /// \param y
  /// The y-value
  ///
  /// \param z
  /// The z-value
  ///
  /// \return
  /// \c true if successful, \c false otherwise
  static bool readTiplet(QXmlStreamReader& reader, double& x, double& y,
                         double& z);

 private:
  /// Reads the stream until a SRTTransform description is found.
  //
  /// Fill \a tx with thethe description found. If errors are detected, \a tx is not modified.
  ///
  /// \param reader
  /// The xml stream to read
  ///
  /// \param tx
  /// The transform object to fill
  ///
  /// \return
  /// \c true on success, \c false otherwise
  static bool readXmlMain(QXmlStreamReader& reader, sedeen::SRTTransform& tx);

  friend bool importPropertiesV1(QXmlStreamReader& reader,
                                 PhysicalProperties& pp,
                                 const QString expectedImageName);

 private:
  mutable QTransform _qtransform;
  mutable bool _dirty;
  double _tx;  ///< x translation
  double _ty;  ///< y translation
  double _sx;  ///< x scale
  double _sy;  ///< y scale
  double _cx;  ///< x center of rotation
  double _cy;  ///< y center of rotation
  double _phi;  ///< rotation angle in degrees
  double _cosv;  ///< cos value of phi
  double _sinv;  ///< sin value of phi
};

/// Transforms a QPolygonF
//
/// \relates SRTTransform
///
/// \param trans
/// The transformation
///
/// \param poly
/// The object to transforms
///
/// \param dir
/// The direction of the transform
SEDEEN_GLOBAL_API
void transform(const SRTTransform& trans, QPolygonF& poly,
               TransformDirection dir);

/// Transforms a point and stores the result in the same point.
//
/// \relates SRTTransform
///
/// \param trans
/// The transformation
///
/// \param point
/// The point to be transformed.
///
/// \param dir
/// The transform direction (forward/inverse)
SEDEEN_GLOBAL_API
void transform(const SRTTransform& trans, QPointF& point,
               TransformDirection dir);


/// Returns the transformed point.
//
/// \relates SRTTransform
///
/// \param trans
/// The transformation
///
/// \param point
/// The point to be transformed
///
/// \param dir
/// The transform direction (forward/inverse)
SEDEEN_GLOBAL_API
QPointF transformed(const SRTTransform& trans, const QPointF& point,
                    TransformDirection dir);

/// Transforms a vector of points and stores the result in the same vector.
//
/// \relates SRTTransform
///
/// \param trans
/// The transformation
///
/// \param vec
/// The points to be transformed
///
/// \param dir
/// The transform direction (forward/inverse)
SEDEEN_GLOBAL_API
void transform(const SRTTransform& trans, std::vector<QPointF>& vec,
               TransformDirection dir);

/// Transforms a vector of points and stores the result in the same vector.
//
/// \relates SRTTransform
///
/// \param trans
/// The transformation
///
/// \param vec
/// The points to be transformed
///
/// \param dir
/// The transform direction (forward/inverse)
SEDEEN_GLOBAL_API
void transform(const SRTTransform& trans, QVector<QPointF>& vec,
               TransformDirection dir);

} // namespace sedeen

#endif

