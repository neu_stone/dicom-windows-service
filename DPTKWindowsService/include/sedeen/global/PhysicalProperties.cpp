#include "global/PhysicalProperties.h"

#include <iostream>

#include "global/Debug.h"

namespace sedeen {

PhysicalProperties::PhysicalProperties(const QSize& dims, const QSizeF& pixsize,
                                       const SRTTransform& tx)
    : m_dims(dims),
      m_pix(pixsize),
      m_tx(tx) {
  calculate();
}

SRTTransform& PhysicalProperties::transform() {
  return m_tx;
}

const SRTTransform& PhysicalProperties::transform() const {
  return m_tx;
}

void PhysicalProperties::setTransform(const SRTTransform &tx) {
  m_tx = tx;
}

const QSizeF& PhysicalProperties::pixelSize() const {
  return m_pix;
}

void PhysicalProperties::setPixelSize(const QSizeF &pixsize) {
  if (pixsize != m_pix) {
    m_pix = pixsize;
    calculate();
  }
}

const QSize& PhysicalProperties::dims() const {
  return m_dims;
}

const QSizeF& PhysicalProperties::size() const {
  return m_size;
}

const QRectF& PhysicalProperties::rect() const {
  return m_rect;
}

const QPointF& PhysicalProperties::center() const {
  return m_center;
}

void PhysicalProperties::calculate() {
  m_size = QSizeF(m_dims.width() * m_pix.width(),
                  m_dims.height() * m_pix.height());
  m_rect = QRectF(QPointF(0,0), m_size);
  m_center = m_rect.center();
}

bool PhysicalProperties::writeXml(QXmlStreamWriter& writer) const {
  // error check
  if (writer.device() == 0)
    return false;

  writer.writeStartElement("PhysicalProperties");

  /* Write spacing properties */
  writer.writeStartElement("Spacing");
  writer.writeAttribute("units", "um");
  writer.writeTextElement("x", QString("%1").arg(pixelSize().width()) );
  writer.writeTextElement("y", QString("%1").arg(pixelSize().height()));
  writer.writeTextElement("z", "1.0");
  writer.writeEndElement();

  /* Write transform properties */
  bool tsuccess = transform().writeXml(writer);

  // close starting element
  writer.writeEndElement();

  // return
  return tsuccess;
}

bool PhysicalProperties::readXml(QXmlStreamReader& reader,
                                 PhysicalProperties& pp) {
  // check reader
  if (reader.atEnd() || reader.hasError())
    return false;

  /* Read file */
  while (!reader.atEnd()) {
    /* Read next token */
    if (reader.isWhitespace()) {
      reader.readNext();
      continue;
    }

    /* Check Start Elements */
    if (reader.isStartElement()) {
      QString tagname = reader.name().toString();

      if (tagname.compare("PhysicalProperties", Qt::CaseInsensitive) == 0) {
        reader.readNext();
        return readXmlMain(reader, pp);
      }
    }
    // read next element
    reader.readNext();
  }

  return false;
}

bool PhysicalProperties::readXmlMain(QXmlStreamReader& reader,
                                     PhysicalProperties& pp) {
  double x, y, z;
  int tagsFound = 0;
  PhysicalProperties p(pp.dims(), pp.pixelSize(), pp.transform());

  // check reader
  if (reader.atEnd() || reader.hasError())
    return false;

  // Read file //
  while (!reader.atEnd()) {
    // Read next token
    if (reader.isWhitespace()) {
      reader.readNext();
      continue;
    }

    // Check Start Elements
    if (reader.isStartElement()) {
      QString tagname = reader.name().toString();

      if (tagname.compare("Spacing", Qt::CaseInsensitive) == 0) {
        if (SRTTransform::readTiplet(reader, x, y, z)) {
          p.setPixelSize(QSizeF(x,y));
        } else {
          break;
        }
        tagsFound++;
      } else if (tagname.compare("Transform", Qt::CaseInsensitive) == 0) {
        SRTTransform tx;
        if (SRTTransform::readXml(reader, tx)) {
          p.setTransform(tx);
        } else {
          break;
        }
        tagsFound++;
      } else {
        debug::warning((std::string("Unknown tag (") +
                        tagname.toStdString() +
                        ") detected in Transform structure.").c_str(),
                       __FUNCTION__, __LINE__);
      }
    }

    // read next element
    if (tagsFound == 2) {
      break;
    } else {
      reader.readNext();
    }
  }

  // check for errors
  if (reader.hasError()) {
    QString err  = reader.errorString();
    debug::warning("Error in XML stream.",__FUNCTION__, __LINE__);
    return false;
  } else if (tagsFound < 2) {
    debug::warning("Some tags not found Transform structure.",
                   __FUNCTION__, __LINE__);
    return false;
  } else {
    pp = p;
    return true;
  }
}

} // namespace sedeen
