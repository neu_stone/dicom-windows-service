#ifndef SEDEEN_SRC_GLOBAL_COMMANDBASE_H
#define SEDEEN_SRC_GLOBAL_COMMANDBASE_H

#include "global/DllApi.h"

namespace sedeen {

/// Interface on an undo/redo-able command class compatible with CommandHistory.
class SEDEEN_GLOBAL_API CommandBase {
 public:
  /// Get the commands type.
  //
  /// A unique command identifier
  virtual unsigned int id() const = 0;

  /// Indicates if the command is undoable
  //
  /// \return true if possible, false otherwise
  virtual bool canUndo() const = 0;

  /// Indicates if the command is redoable
  //
  /// \return true if possible, false otherwise
  virtual bool canRedo() const = 0;

  /// Executes the command
  virtual void execute() = 0;

  /// Undoes the command if possible. see canUndo().
  virtual void unExecute()= 0;

  /// Merges the command with another command if possible.
  //
  /// Only like commands can be merged. If the request has no obvious meaning,
  /// the function does nothing. I.e. two move commands can be merged, while two
  /// insert commands cannot be merged.
  ///
  /// \param command
  /// The command to merge with current
  ///
  /// \return true if sucessful, false otherwise
  virtual bool merge(const CommandBase &command) = 0;
}; // class CommandBase

/// Registgers the command ID.
//
/// \return A unique command ID
SEDEEN_GLOBAL_API
unsigned int uniqueCommandID();

} // namespace sedeen

#endif

