#ifndef SEDEEN_SRC_GLOBAL_GEOMETRY_RECT_H
#define SEDEEN_SRC_GLOBAL_GEOMETRY_RECT_H

#include <ostream>

#include <QRect>

// User includes
#include "global/geometry/Span.h"

#include "global/DllApi.h"

namespace sedeen {

class Point;
class Size;

/// Representation of a rectangle in discrete space.
//
/// The class offers two interface types. The more familiar (x, y, width,
/// height) as well as a (Span, Span) construction. The Span class is a
/// description of the location and extent of an object along a single
/// dimension. See Span class documentation for details.
///
/// An important note is that the minimum size of a Rect in either dimension is
/// zero. Negative values are not allowed.
class SEDEEN_GLOBAL_API Rect {
 public:
  /// Construct an empty rectangle.
  Rect();

  /// Construct a rectangle with a position and size.
  //
  /// \param position
  /// The position of the minimum x- and y-value for the region.
  ///
  /// \param size
  /// The extent of the region from position in the positive x- and
  /// y-directions.
  Rect(const Point& position, const Size& size);

  /// Construct a rectangle from two orthogonal spans.
  //
  /// \param x_span
  /// Span in the x-direction.
  ///
  /// \param y_span
  /// Span in the y-direction.
  Rect(const Span& x_span, const Span& y_span);

  /// Test if the location and dimensions of \a this and \a other match.
  bool operator==(const Rect& other) const;

  /// X-coordinate of the left-hand column of elements.
  int x() const;

  /// Set the x-coordinate of the minimum column of elements.
  //
  /// Will translate the region to align the minimum column with \a x.
  ///
  /// \param x
  /// All values allowed.
  void setX(int x);

  /// Y-coordinate of the minimum row of elements.
  int y() const;

  /// Set the y-coordinate of the minimum row of elements.
  //
  /// Will translate the region to align the minimum row with \a y.
  ///
  /// \param y
  /// All values allowed.
  void setY(int y);

  /// Width of the region.
  //
  /// \return
  /// The width of the region. Will not be negative.
  int width() const;

  /// Set the width of the region.
  //
  /// \param width
  /// New width of the region. Must not be less than zero.
  void setWidth(int width);

  /// Height of the region.
  //
  /// \return
  /// The height of the region. Will not be negative.
  int height() const;

  /// Set the height of the region.
  //
  /// \param height
  /// New height of the region. Must not be less than zero.
  void setHeight(int height);

  /// Get a Span object describing the x-extent of the region.
  Span xSpan() const;

  /// Set the x-range of the region using a Span object.
  //
  /// \param x_span
  /// The span of the x-dimension of the region.
  ///
  /// \return
  /// *this
  Rect& setXSpan(const Span& x_span);

  /// Get a Span object describing the y-extent of the region.
  Span ySpan() const;

  /// Set the y-range of the region using a Span object.
  //
  /// \param y_span
  /// The span of the y-dimension of the region.
  ///
  /// \return
  /// *this
  Rect& setYSpan(const Span& y_span);

  /// Check if this Rect fully contains another.
  //
  /// \param rect
  /// Rect to check for full containment in this Rect.
  bool contains(const Rect& rect) const;

  /// Check if a point lies within this Rect.
  //
  /// \param point
  bool contains(const Point& point) const;

 private:
  /// Describes the span along the x-dimension.
  Span m_x_span;

  /// Describes the span along the y-dimension.
  Span m_y_span;
}; // class Rect

/// Pretty-printing function for debugging purposes.
//
/// \relates Rect
///
/// Prints the width and height as
/// \code
/// (\a width, \a height)
/// \endcode
///
/// \param out
///
/// \param rect
SEDEEN_GLOBAL_API
std::ostream& operator<<(std::ostream& out, const Rect& rect);

/// Inequality operator.
//
/// \relates Rect
///
/// \param lhs
///
/// \param rhs
SEDEEN_GLOBAL_API
bool operator!=(const Rect& lhs, const Rect& rhs);

/// Offset the region.
//
/// \relates Rect
///
/// \param rect
/// Region to translate.
///
/// \param offset
/// Amount by which to shift the region.
///
/// \return
/// The translated \a rect.
SEDEEN_GLOBAL_API
Rect& operator+=(Rect& rect, const Point& offset);

/// Compute the maximum x-index contained in the region.
//
/// \relates Rect
///
/// \param rect
///
/// \note
/// If the Rect is empty, this will return a value one less than rect.x(), i.e.
/// the maximum x-index will be one less than the minimum x-index.
SEDEEN_GLOBAL_API
int xMax(const Rect& rect);

/// Compute the maximum y-index contained in the region.
//
/// \relates Rect
///
/// \param rect
///
/// \note
/// If the Rect is empty, this will return a value one less than rect.y(), i.e.
/// the maximum y-index will be one less than the minimum y-index.
SEDEEN_GLOBAL_API
int yMax(const Rect& rect);

/// Project a region from one 2-D space onto another.
//
/// \sa project(Span,int,int,int,int)
///
/// \relates Rect
///
/// \param region
/// Region relative to \p source_space that you would like translated into \p
/// target_space.
///
/// \param source_space
/// A region in the source frame that exactly overlaps \p target_space. Must not
/// be empty.
///
/// \param target_space
/// A region in the target frame that exactly overlaps \p source_space. This may
/// be empty.
///
/// \return
/// \p region as projected into the target_space. Since this involves integer
/// values, any partially overlapping spans will be rounded up. Because of this,
/// project() is not necessarily symmetrical.
SEDEEN_GLOBAL_API
Rect project(Rect region, const Rect& source_space, const Rect& target_space);

/// Get the contained area of the region.
//
/// \relates Rect
///
/// \param region
///
/// \return
/// The number of elements contained in the region. Will not be negative.
SEDEEN_GLOBAL_API
int area(const Rect& region);

/// Get the size of a Rect.
//
/// \relates Rect
///
/// \param region
SEDEEN_GLOBAL_API
Size size(const Rect& region);

/// Get the smallest x-y point in the region.
//
/// \relates Rect
///
/// \param region
SEDEEN_GLOBAL_API
Point origin(const Rect& region);

/// Check if either dimension of the Rect is 0.
//
/// \relates Rect
///
/// \param region
///
/// \return
/// \c true if \a this is empty, \c false otherwise.
SEDEEN_GLOBAL_API
bool isEmpty(const Rect& region);

/// Test if an x-y point lies within the region.
//
/// \relates Rect
///
/// \param container
///
/// \param x
///
/// \param y
///
/// \return
/// \c true if the point (\a x,\a y) is one of the elements contained in
/// \a this, otherwise \c false.
SEDEEN_GLOBAL_API
bool contains(const Rect& container, int x, int y);

/// Check for intersection of \a this and \a other.
//
/// \relates Rect
///
/// \param a
///
/// \param b
///
/// \return
/// \c true if some portion of the two regions overlap, \c false otherwise.
SEDEEN_GLOBAL_API
bool intersects(const Rect& a, const Rect& b);

/// Get the overlapping region of \a a and \a b.
//
/// \relates Rect
///
/// \param a
///
/// \param b
///
/// \return
/// A Rect describing the intersecting region of the two input Rect objects. May
/// be empty.
SEDEEN_GLOBAL_API
Rect intersection(Rect a, const Rect& b);

/// Get a copy of \a region that sits completely within \a container.
//
/// \relates Rect
///
/// Moves \a region as little as possible to sit entirely within \a container.
/// In the case that \a region is larger than \a container, \a region will be
/// cropped to fit.
///
/// \param container
/// Region in which \a region will be made to sit.
///
/// \param region
/// Starting position and size of the region to be moved as little as possible
/// to sit within \a container.
///
/// \return
/// The \a region Rect translated and, as a last resort, resized to fit within
/// \a container.
SEDEEN_GLOBAL_API
Rect constrain(const Rect& container, Rect region);

/// Convert this class into its Qt-equivalent.
//
/// \relates Rect
SEDEEN_GLOBAL_API
QRect toQt(const Rect& rect);

/// Create a Rect object from a QRect object.
//
/// \relates Rect
SEDEEN_GLOBAL_API
Rect fromQt(const QRect& qrect);

} // namespace sedeen

#endif
