#ifndef SEDEEN_SRC_GLOBAL_GEOMETRY_SIZE_H
#define SEDEEN_SRC_GLOBAL_GEOMETRY_SIZE_H

#include <ostream>

#include <QSize>

#include "global/DllApi.h"

namespace sedeen {

/// Extent in two dimensions of discrete space.
class SEDEEN_GLOBAL_API Size {
 public:
  /// Create a size object with zero height and width.
  Size();

  /// Create a size object with specific height and width.
  //
  /// \param width
  /// Extent of the region in the x-direction. Must not be negative.
  ///
  /// \param height
  /// Extent of the region in the y-direction. Must not be negative.
  Size(int width, int height);

  /// Get the number of contained elements in the x-direction.
  int width() const;

  /// Set the width component of the size.
  //
  /// \param width
  /// New width of the size. Must not be negative.
  ///
  /// \return
  /// *this
  Size& setWidth(int width);

  /// Get the number of contained elements in the y-direction.
  int height() const;

  /// Set the height component of the size.
  //
  /// \param height
  /// New height of the size. Must not be negative.
  ///
  /// \return
  /// *this
  Size& setHeight(int height);

 private:
  /// Width of the region. Must not be negative.
  int m_w;

  /// Height of the region. Must not be negative.
  int m_h;
}; // class Size

/// Streamer for debugging purposes.
//
/// \relates Size
SEDEEN_GLOBAL_API
std::ostream& operator<<(std::ostream& out, const Size& size);

/// Tests equality on matching dimensions.
//
/// \relates Size
///
/// \param lhs
///
/// \param rhs
SEDEEN_GLOBAL_API
bool operator==(const Size& lhs, const Size& rhs);

/// Inequality operator
//
/// \relates Size
///
/// \param lhs
///
/// \param rhs
SEDEEN_GLOBAL_API
bool operator!=(const Size& lhs, const Size& rhs);

/// Test if the Size has zero area.
SEDEEN_GLOBAL_API
bool isEmpty(const Size& size);

/// Compute the area of the size.
//
/// \relates Size
///
/// \param size
///
/// \return
/// The area of the defined region. Shall not be negative.
SEDEEN_GLOBAL_API
int area(const Size& size);

/// Convert this class into its Qt-equivalent.
//
/// \relates Size
SEDEEN_GLOBAL_API
QSize toQt(const Size& size);

/// Create a Size object from a QSize object.
//
/// \relates Size
SEDEEN_GLOBAL_API
Size fromQt(const QSize& qsize);

} // namespace sedeen

#endif
