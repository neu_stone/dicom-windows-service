#include "global/geometry/PointF.h"

#include <cassert>
#include <cmath>

#include "global/geometry/Point.h"

namespace sedeen {

PointF::PointF() : x_(), y_() {
}

PointF::PointF(const Point& point) : x_(point.getX()), y_(point.getY()) {
}

PointF::PointF(double x, double y) : x_(x), y_(y) {
}

double PointF::getX() const {
  return x_;
}

PointF& PointF::setX(double x) {
  x_ = x;
  return *this;
}

double PointF::getY() const {
  return y_;
}

PointF& PointF::setY(double y) {
  y_ = y;
  return *this;
}

PointF& PointF::operator+=(const PointF& point) {
  x_ += point.x_;
  y_ += point.y_;
  return *this;
}

PointF& PointF::operator-=(const PointF& point) {
  x_ -= point.x_;
  y_ -= point.y_;
  return *this;
}

PointF& PointF::operator*=(double scale) {
  x_ *= scale;
  y_ *= scale;
  return *this;
}

PointF& PointF::operator/=(double scale) {
  assert(0 != scale);

  x_ /= scale;
  y_ /= scale;
  return *this;
}

PointF& PointF::rotateBy(double delta_radians) {
  auto cos_term = std::cos(delta_radians);
  auto sin_term = std::sin(delta_radians);

  PointF new_point(*this);

  // Rotate the components
  new_point.setX(x_ * cos_term - y_ * sin_term);
  new_point.setY(x_ * sin_term + y_ * cos_term);

  return this->swap(new_point);
}

PointF& PointF::swap(PointF& point) {
  std::swap(x_, point.x_);
  std::swap(y_, point.y_);
  return *this;
}

std::ostream& operator<<(std::ostream& out, const PointF& point) {
  return out << '(' << point.getX()
             << ',' << point.getY()
             << ')';
}

PointF operator+(PointF lhs, const PointF& rhs) {
  return lhs += rhs;
}

PointF operator-(PointF lhs, const PointF& rhs) {
  return lhs -= rhs;
}

PointF operator*(PointF point, double scale) {
  return point *= scale;
}

PointF operator/(PointF point, double scale) {
  assert(0 != scale);
  return point /= scale;
}

} // namespace sedeen
