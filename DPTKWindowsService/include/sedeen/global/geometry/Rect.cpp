
#include "global/geometry/Rect.h"

#include <cassert>

#include "global/geometry/Point.h"
#include "global/geometry/Size.h"

namespace sedeen {

Rect::Rect() : m_x_span(), m_y_span() {}

Rect::Rect(const Point& point, const Size& size)
    : m_x_span(point.getX(), size.width()),
      m_y_span(point.getY(), size.height()) {}

Rect::Rect(const Span& x_span, const Span& y_span)
    : m_x_span(x_span),
      m_y_span(y_span) {}

bool Rect::operator==(const Rect& other) const {
  return m_x_span == other.m_x_span
      && m_y_span == other.m_y_span;
}

int Rect::x() const {
  return m_x_span.begin();
}

void Rect::setX(int x) {
  m_x_span.setBegin(x);
}

int Rect::y() const {
  return m_y_span.begin();
}

void Rect::setY(int y) {
  m_y_span.setBegin(y);
}

int Rect::width() const {
  return m_x_span.extent();
}

void Rect::setWidth(int width) {
  assert(0 <= width);
  m_x_span.setExtent(width);
}

int Rect::height() const {
  return m_y_span.extent();
}

void Rect::setHeight(int height) {
  assert(0 <= height);
  m_y_span.setExtent(height);
}

Span Rect::xSpan() const {
  return m_x_span;
}

Rect& Rect::setXSpan(const Span& x_span) {
  m_x_span = x_span;
  return *this;
}

Span Rect::ySpan() const {
  return m_y_span;
}

Rect& Rect::setYSpan(const Span& y_span) {
  m_y_span = y_span;
  return *this;
}

std::ostream& operator<<(std::ostream& out, const Rect& rect) {
  return out << "(" << rect.x()
             << ',' << rect.y()
             << ',' << size(rect) << ')';
}

bool operator!=(const Rect& lhs, const Rect& rhs) {
  return !(lhs==rhs);
}

Rect& operator+=(Rect& rect, const Point& point) {
  rect.setX(rect.x() + point.getX());
  rect.setY(rect.y() + point.getY());
  return rect;
}

int xMax(const Rect& rect) {
  return rect.x() + rect.width() - 1;
}

int yMax(const Rect& rect) {
  return rect.y() + rect.height() - 1;
}

Rect project(Rect region, const Rect& source_space, const Rect& target_space) {
  assert(!isEmpty(source_space));
  region.setXSpan(project(region.xSpan(), source_space.xSpan(),
                          target_space.xSpan()));
  region.setYSpan(project(region.ySpan(), source_space.ySpan(),
                          target_space.ySpan()));
  return region;
}

int area(const Rect& region) {
  return region.width() * region.height();
}

Size size(const Rect& region) {
  return Size(region.width(), region.height());
}

Point origin(const Rect& region) {
  return Point(region.x(), region.y());
}

bool isEmpty(const Rect& region) {
  return (0 == region.width()) || (0 == region.height());
}

bool contains(const Rect& container, const int x, const int y) {
  return container.xSpan().contains(x)
      && container.ySpan().contains(y);
}

bool Rect::contains(const Point& point) const {
  return m_x_span.contains(point.getX())
      && m_y_span.contains(point.getY());
}

bool Rect::contains(const Rect& region) const {
  return m_x_span.contains(region.m_x_span)
      && m_y_span.contains(region.m_y_span);
}

bool intersects(const Rect& a, const Rect& b) {
  return intersects(a.xSpan(), b.xSpan())
      && intersects(a.ySpan(), b.ySpan());
}

Rect intersection(Rect a, const Rect& b) {
  a.setXSpan(intersection(a.xSpan(), b.xSpan()));
  a.setYSpan(intersection(a.ySpan(), b.ySpan()));
  return a;
}

Rect constrain(const Rect& container, Rect region) {
  region.setXSpan(constrain(container.xSpan(), region.xSpan()));
  region.setYSpan(constrain(container.ySpan(), region.ySpan()));
  return region;
}

QRect toQt(const Rect& rect) {
  return QRect(toQt(origin(rect)), toQt(size(rect)));
}

Rect fromQt(const QRect& qrect) {
  return Rect(fromQt(qrect.topLeft()), fromQt(qrect.size()));
}

} // namespace sedeen
