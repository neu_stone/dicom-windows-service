#include "global/geometry/Span.h"

#include <algorithm>
#include <cassert>
#include <cmath>

#include "global/MathOps.h"

namespace sedeen {

Span::Span() : m_begin(), m_extent() {}

Span::Span(int begin, int extent) : m_begin(begin), m_extent(extent) {
  assert(0 <= extent);
}

int Span::begin() const {
  return m_begin;
}

Span& Span::setBegin(int begin) {
  m_begin = begin;
  return *this;
}

int Span::extent() const {
  return m_extent;
}

Span& Span::setExtent(int extent) {
  assert(0 <= extent);
  m_extent = extent;
  return *this;
}

bool Span::contains(int point) const {
  return m_begin <= point && end(*this) > point;
}

bool Span::contains(const Span& span) const {
  return m_begin <= span.m_begin && end(*this) >= end(span);
}

std::ostream& operator<<(std::ostream& out, const Span& span) {
  return out << '[' << span.begin()
             << ',' << end(span)
             << ')';
}

bool operator==(const Span& lhs, const Span& rhs) {
  return lhs.begin() == rhs.begin()
      && lhs.extent() == rhs.extent();
}

bool operator!=(const Span& lhs, const Span& rhs) {
  return !(lhs==rhs);
}

int end(const Span& span) {
  return span.begin() + span.extent();
}

bool intersects(const Span& a, const Span& b) {
  return a.begin() < end(b)
      && b.begin() < end(a);
}

Span intersection(Span a, const Span& b) {
  int begin = std::max(a.begin(), b.begin());
  a.setExtent(std::max(0, std::min(end(a), end(b)) - begin));
  a.setBegin(begin);
  return a;
}

Span constrain(const Span& container, Span region) {
  if (region.extent() > container.extent()) {
    // If the region is larger than the container, the result is the region
    // exactly filling the container
    region = container;
  } else {
    // We now know the region is not larger than the container
    if (region.begin() < container.begin()) {
      // If the start of region is lower than the start of container, advance it
      region.setBegin(container.begin());
    } else if (end(region) > end(container)) {
      // If the end of region is higher than the container, back it up
      region.setBegin(end(container) - region.extent());
    }
  }
  return region;
}

bool isEmpty(const Span& span) {
  return 0 == span.extent();
}

Span project(Span span, const Span& source_space, const Span& target_space) {
  // Perform the scaling using integer math, manually selecting rounding towards
  // positive infinity for the end-point and towards negative infinity for the
  // starting-point variables.

  assert(!isEmpty(source_space));

  // Compute the start of the span, but wait to write the value
  long long source_begin = span.begin() - source_space.begin();
  int target_begin = math_ops::intDivideRoundDown<long long>(
      source_begin * target_space.extent(),
      source_space.extent());
  // Compute and write the end of the span
  if (!isEmpty(span)) {
    long long source_end = end(span) - source_space.begin();
    int target_end = math_ops::intDivideRoundUp<long long>(
        source_end * target_space.extent(),
        source_space.extent());
    span.setExtent(target_end - target_begin);
  }
  // Write the start of the span
  span.setBegin(target_begin + target_space.begin());
  return span;
}

} // namespace sedeen
