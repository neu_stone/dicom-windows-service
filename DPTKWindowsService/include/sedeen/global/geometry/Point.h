#ifndef SEDEEN_SRC_GLOBAL_GEOMETRY_POINT_H
#define SEDEEN_SRC_GLOBAL_GEOMETRY_POINT_H

#include <ostream>

#include <QPoint>

#include "global/DllApi.h"

namespace sedeen {

class Size;

/// A location in discrete, two-dimensional space.
class SEDEEN_GLOBAL_API Point {
 public:
  /// Construct a point a (0, 0), the origin.
  Point();

  /// Construct a point at (x, y).
  //
  /// \param x
  /// X-coordinate. May be any value.
  ///
  /// \param y
  /// Y-coordinate. May be any value.
  Point(int x, int y);

  /// Get the X-coordinate
  int getX() const;

  /// Set the X-coordinate
  //
  /// \param x
  /// X-coordinate. May be any value.
  Point& setX(int x);

  /// Get the Y-coordinate
  int getY() const;

  /// Set the Y-coordinate
  //
  /// \param y
  /// Y-coordinate. May be any value.
  Point& setY(int y);

 private:
  /// X-coordinate.
  int x_;

  /// Y-coordinate.
  int y_;
}; // class Point

/// Streaming operator for debugging purposes.
//
/// \relates Point
///
/// \param out
///
/// \param point
SEDEEN_GLOBAL_API
std::ostream& operator<<(std::ostream& out, const Point& point);

/// Test equality of two Point objects.
//
/// \relates Point
SEDEEN_GLOBAL_API
bool operator==(const Point& lhs, const Point& rhs);

/// Invert the signs of the coordinates.
//
/// \relates Point
///
/// \param point
/// The point to invert.
///
/// \return
/// The inverted \a point.
SEDEEN_GLOBAL_API
Point operator-(Point point);

/// In-place addition of another point.
//
/// \relates Point
///
/// \param a
/// Point to which to add \a b.
///
/// \param b
/// Point to add to \a a.
///
/// \return
/// The modified \a a.
SEDEEN_GLOBAL_API
Point& operator+=(Point& a, const Point& b);

/// Copy and add two points.
//
/// \relates Point
///
/// \param p1
///
/// \param p2
SEDEEN_GLOBAL_API
Point operator+(Point p1, const Point& p2);

/// In-place subtraction of another point.
//
/// \relates Point
///
/// \param a
/// Point from which to subtract \a b.
///
/// \param b
/// Point to subtract from \a a.
///
/// \return
/// The modified \a a.
SEDEEN_GLOBAL_API
Point& operator-=(Point& a, Point b);

/// Copy and subtract two points.
//
/// \relates Point
///
/// \param p1
///
/// \param p2
SEDEEN_GLOBAL_API
Point operator-(Point p1, const Point& p2);

/// Vector multiplication of this point with \a point.
//
/// \relates Point
///
/// \param a
/// Point whose elements to modify.
///
/// \param b
/// Point whose elements to multiply with their counterparts in \a a.
///
/// \return
/// The modified \a a.
SEDEEN_GLOBAL_API
Point& operator*=(Point& a, const Point& b);

/// Return the vector multiple of two Point objects.
//
/// \relates Point
///
/// \param p1
///
/// \param p2
SEDEEN_GLOBAL_API
Point operator*(Point p1, const Point& p2);

/// Vector multiplication of this point with \a size.
//
/// \relates Point
///
/// \param point
///
/// \param size
/// Size whose elements to multiply with their counterparts in \a this.
///
/// \return
/// The modified \a point.
SEDEEN_GLOBAL_API
Point& operator*=(Point& point, const Size& size);

/// Return the vector multiple of a Point and a Size.
//
/// \relates Point
///
/// \param point
///
/// \param size
SEDEEN_GLOBAL_API
Point operator*(Point point, const Size& size);

/// \copydoc operator*(Point,const Size&)
SEDEEN_GLOBAL_API
Point operator*(const Size& size, Point point);

/// Convert this class into its Qt-equivalent.
//
/// \relates Point
SEDEEN_GLOBAL_API
QPoint toQt(const Point& point);

/// Create a Point object from a QPoint object.
//
/// \relates Point
SEDEEN_GLOBAL_API
Point fromQt(const QPoint& qpoint);

} // namespace sedeen

#endif
