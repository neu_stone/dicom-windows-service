#include "global/geometry/Placed.h"

#include <cmath>

namespace sedeen {

Placed::~Placed() {
}

PointF Placed::getOrigin() const {
  return doGetOrigin();
}

void Placed::setOrigin(const PointF& origin) {
  doSetOrigin(origin);
}

double Placed::getRotation() const {
  return doGetRotation();
}

void Placed::setRotation(double radians) {
  return doSetRotation(radians);
}

Placed::Placed() {
}

void rotateAbout(Placed& shape, const PointF& origin, double delta_radians) {
  // Translate into the frame of the rotation point
  auto rotation_arm = shape.getOrigin() - origin;

  // Apply the rotation
  rotation_arm.rotateBy(delta_radians);

  // Translate back into the frame of the shape
  rotation_arm += origin;

  // Apply the translation
  shape.setOrigin(rotation_arm);

  // Apply the rotation to the shape
  shape.setRotation(shape.getRotation() + delta_radians);
}

} // namespace sedeen
