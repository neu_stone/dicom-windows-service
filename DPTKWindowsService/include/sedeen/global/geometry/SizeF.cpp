#include "global/geometry/SizeF.h"

#include <cassert>

namespace sedeen {

SizeF::SizeF() : _w(0), _h(0) {}

SizeF::SizeF(double width, double height) : _w(width), _h(height) {
  assert(0 <= width);
  assert(0 <= height);
}

double SizeF::width() const {
  return _w;
}

SizeF& SizeF::setWidth(double width) {
  assert(0 <= width);
  _w = width;
  return *this;
}

double SizeF::height() const {
  return _h;
}

SizeF& SizeF::setHeight(double height) {
  assert(0 <= height);
  _h = height;
  return *this;
}

std::ostream& operator<<(std::ostream& out, const SizeF& size_f) {
  return out << '(' << size_f.width()
             << ',' << size_f.height() << ')';
}

bool isEmpty(const SizeF& size) {
  return 0 == size.width() || 0 == size.height();
}

bool operator==(const SizeF& lhs, const SizeF& rhs) {
  return lhs.width() == rhs.width() && lhs.height() == rhs.height();
}

bool operator!=(const SizeF& lhs, const SizeF& rhs) {
  return !(lhs==rhs);
}

double area(const SizeF& size) {
  return size.width() * size.height();
}

QSizeF toQt(const SizeF& sizef) {
  return QSizeF(sizef.width(), sizef.height());
}

} // namespace sedeen
