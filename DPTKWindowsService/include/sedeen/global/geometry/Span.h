#ifndef SEDEEN_SRC_GLOBAL_GEOMETRY_SPAN_H
#define SEDEEN_SRC_GLOBAL_GEOMETRY_SPAN_H

#include <ostream>

#include "global/DllApi.h"

namespace sedeen {

/// Represents a finite number of elements along a single discrete dimension.
class SEDEEN_GLOBAL_API Span {
 public:
  /// Construct an empty Span.
  Span();

  /// Construct a finite Span.
  //
  /// \param begin
  /// Minimum inclusive point in the span.
  ///
  /// \param extent
  /// The number of elements contained in the span. Must not be negative, but
  /// may be zero. Includes elements above the beginning point.
  Span(int begin, int extent);

  /// Get the starting point of the span.
  int begin() const;

  /// Set the starting point of the span.
  //
  /// \param begin
  /// Inclusive beginning element of the span. May be any value.
  Span& setBegin(int begin);

  /// Get the number of elements contained in the span.
  int extent() const;

  /// Set the extent of the span.
  //
  /// \param extent
  /// The number of elements contained in the span. Must not be negative, but
  /// may be zero.
  Span& setExtent(int extent);

  /// Test if \a point is contained in \a this.
  //
  /// \param point
  ///
  /// \return
  /// \c true if \a point is included in \a this, otherwise \c false.
  bool contains(int point) const;

  /// Check if \a span is completely enclosed by \a this.
  //
  /// \param span
  /// If \a span is empty, the test always fails.
  ///
  /// \return
  /// \c true if \a span is non-empty and all of its elements are also in
  /// \a this, otherwise \c false.
  bool contains(const Span& span) const;

 private:
  /// Inclusive starting point of the span.
  int m_begin;

  /// Number of elements contained in the span.
  int m_extent;
}; // class Span

/// Streaming operator, for debugging.
//
/// \relates Span
///
/// Prints span in the set notation
/// \code
/// [inclusive-begin, exclusive-end)
/// \endcode
///
/// The empty set appears as
/// \code
/// [x,x)
/// \endcode
SEDEEN_GLOBAL_API
std::ostream& operator<<(std::ostream& out, const Span& span);

/// Equality operator
//
/// \relates Span
///
/// \param lhs
///
/// \param rhs
SEDEEN_GLOBAL_API
bool operator==(const Span& lhs, const Span& rhs);

/// Inequality operator
//
/// \relates Span
///
/// \param lhs
///
/// \param rhs
SEDEEN_GLOBAL_API
bool operator!=(const Span& lhs, const Span& rhs);

/// Return the first element after the end of the span.
//
/// \relates Span
///
/// \param span
SEDEEN_GLOBAL_API
int end(const Span& span);

/// Check if the two spans share some number of elements.
//
/// \relates Span
///
/// \param a
///
/// \param b
///
/// \return
/// \c true if the two spans overlap, otherwise \c false. Note that if either of
/// the spans is empty, this test will return \c false.
SEDEEN_GLOBAL_API
bool intersects(const Span& a, const Span& b);

/// Get the Span of the overlap area of \a a and \a b.
//
/// \relates Span
///
/// \param a
///
/// \param b
///
/// \return
/// The span of the overlap area. In the case of non-overlap, the extent shall
/// be zero, though the result of begin() is undefined.
SEDEEN_GLOBAL_API
Span intersection(Span a, const Span& b);

/// Translate \a region to be within \a container.
//
/// \relates Span
///
/// Attempts to keep \a region as close to its original position as possible.
///
/// \param container
/// Containing Span in which \a region will be placed.
///
/// \param region
/// Region to relocate within \a container.
///
/// \return
/// A copy of \a region translated so as much as possible fits within
/// \a container. In the case that \a region is larger than \a container, the
/// beginning of both regions will be lined up and \a region will be reduced in
/// size.
SEDEEN_GLOBAL_API
Span constrain(const Span& container, Span region);

/// Test if the span is empty.
//
/// \relates Span
SEDEEN_GLOBAL_API
bool isEmpty(const Span& span);

/// Project a Span onto a new space defined by a linear relationship.
//
/// \relates Span
///
/// Used to project a Span from one discrete space onto another.
/// Partially-overlapped elements are included, so the region may wind up being
/// slightly larger after the projection.
///
/// \note
/// Because of the intentional rounding-up that occurs, scaling a Span by a
/// factor and then by its inverse is not expected to result in the recovery of
/// the original Span. This means that scaling factors should be computed
/// entirely before their application to the Span, rather than using the Span
/// as an intermediate variable.
///
/// To project from x1 onto x2, for example,
/// \verbatim
/// x1: -1     0     1     2     3     4     5
/// x2: -5 -4 -3 -2 -1  0  1  2  3  4  5  6  7
/// \endverbatim
/// One would use project(span, Span(0, 1), Span(-3, 2));
///
/// \param span
/// The Span in the source coordinate system to be projected.
///
/// \param source_space
/// A span in the source space that exactly overlaps \p target_space. In the
/// example above, a span in the source space, x1, that exactly overlaps a span
/// in the target space, x2, would be Span(0, 1). This is a single step starting
/// at 0.
///
/// \param target_space
/// A span in the target space that exactly overlaps \p source_space. In the
/// example above, two steps starting at -3
///
/// \pre
/// !isEmpty(source_space)
///
/// \return
/// The shortest possible Span in the target coordinate system that completely
/// encompasses the Span in the source coordinate system.
SEDEEN_GLOBAL_API
Span project(Span span, const Span& source_space, const Span& target_space);

} // namespace sedeen

#endif
