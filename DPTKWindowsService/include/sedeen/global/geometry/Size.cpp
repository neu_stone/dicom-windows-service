#include "global/geometry/Size.h"

#include <cassert>

namespace sedeen {

Size::Size() : m_w(0), m_h(0) {}

Size::Size(int width, int height) : m_w(width), m_h(height) {
  assert(0 <= width);
  assert(0 <= height);
}

int Size::width() const {
  return m_w;
}

Size& Size::setWidth(int width) {
  assert(0 <= width);
  m_w = width;
  return *this;
}

int Size::height() const {
  return m_h;
}

Size& Size::setHeight(int height) {
  assert(0 <= height);
  m_h = height;
  return *this;
}

std::ostream& operator<<(std::ostream& out, const Size& size) {
  return out << '(' << size.width()
             << ',' << size.height() << ')';
}

bool operator==(const Size& lhs, const Size& rhs) {
  return lhs.width() == rhs.width() && lhs.height() == rhs.height();
}

bool operator!=(const Size& lhs, const Size& rhs) {
  return !(lhs==rhs);
}

bool isEmpty(const Size& size) {
  return (0 == size.width()) || (0 == size.height());
}

int area(const Size& size) {
  return size.width() * size.height();
}

QSize toQt(const Size& size) {
  return QSize(size.width(), size.height());
}

Size fromQt(const QSize& qsize) {
  return Size(qsize.width(), qsize.height());
}

} // namespace sedeen
