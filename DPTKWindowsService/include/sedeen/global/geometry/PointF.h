#ifndef SEDEEN_SRC_GLOBAL_GEOMETRY_POINTF_H
#define SEDEEN_SRC_GLOBAL_GEOMETRY_POINTF_H

#include <ostream>

#include "global/DllApi.h"

namespace sedeen {

class Point;

/// A floating-point location.
class SEDEEN_GLOBAL_API PointF {
 public:
  PointF();

  /// Construct a floating-point PointF from a fixed-point Point.
  explicit PointF(const Point& point);

  /// Construct a PointF from an (x,y) coordinate pair.
  PointF(double x, double y);

  /// Get the current x-coordinate.
  double getX() const;

  /// Set the current x-coordinate.
  PointF& setX(double x);

  /// Get the current y-coordinate.
  double getY() const;

  /// Set the current y-coordinate.
  PointF& setY(double y);

  /// Add the respective components of \p point to *this.
  PointF& operator+=(const PointF& point);

  /// Subtract the respective components of \p point from *this.
  PointF& operator-=(const PointF& point);

  /// Multiply both components of *this by \p scale.
  PointF& operator*=(double scale);

  /// Divide both components of *this by \p scale.
  //
  /// \param scale.
  /// Must not be 0.
  PointF& operator/=(double scale);

  /// Rotate the point about the origin by an angle, specified in radians.
  PointF& rotateBy(double delta_radians);

  /// No-fail swap function.
  PointF& swap(PointF& point);

 private:
  double x_;

  double y_;
};

SEDEEN_GLOBAL_API
std::ostream& operator<<(std::ostream& out, const PointF& point);

SEDEEN_GLOBAL_API
PointF operator+(PointF lhs, const PointF& rhs);

SEDEEN_GLOBAL_API
PointF operator-(PointF lhs, const PointF& rhs);

SEDEEN_GLOBAL_API
PointF operator*(PointF point, double scale);

SEDEEN_GLOBAL_API
PointF operator/(PointF point, double scale);

} // namespace sedeen

#endif
