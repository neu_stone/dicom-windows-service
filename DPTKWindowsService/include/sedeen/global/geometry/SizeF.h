#ifndef SEDEEN_SRC_GLOBAL_GEOMETRY_SIZEF_H
#define SEDEEN_SRC_GLOBAL_GEOMETRY_SIZEF_H

#include <ostream>

#include <QtGui>

#include "global/DllApi.h"

namespace sedeen {

/// A two-dimensional measurement.
class SEDEEN_GLOBAL_API SizeF {
 public:
  SizeF();

  /// Create a 2D measurement object with given height and width.
  //
  /// \param width
  /// Width of the region. Must not be negative.
  ///
  /// \param height
  /// Height of the region. Must not be negative.
  SizeF(double width, double height);

  /// Get the width of the region.
  double width() const;

  /// Set the width of the region.
  //
  /// \param width
  /// New width of the region. Must not be negative.
  ///
  /// \return
  /// *this
  SizeF& setWidth(double width);

  /// Get the height of the region.
  double height() const;

  /// Set the height of the region.
  //
  /// \param height
  /// New height of the region. Must not be negative.
  ///
  /// \return
  /// *this
  SizeF& setHeight(double height);

  /// Returns TRUE if either of the dimensions is less than or equal to zero
  bool isEmpty() const;

 private:
  /// Width of the region. Will not be negative.
  double _w;

  /// Height of the region. Will not be negative.
  double _h;
}; // class SizeF

SEDEEN_GLOBAL_API
std::ostream& operator<<(std::ostream& out, const SizeF& size_f);

/// Test if the SizeF object has a non-zero area.
//
/// param size
///
/// \return
/// \c true if both dimensions are non-zero, otherwise \c false.
SEDEEN_GLOBAL_API
bool isEmpty(const SizeF& size);

/// Test for matching dimensions.
SEDEEN_GLOBAL_API
bool operator==(const SizeF&, const SizeF&);

/// Inequality test.
SEDEEN_GLOBAL_API
bool operator!=(const SizeF&, const SizeF&);

/// Convert to the equivalent class in Qt.
//
/// \relates SizeF
SEDEEN_GLOBAL_API
QSizeF toQt(const SizeF& size);

} // namespace sedeen

#endif
