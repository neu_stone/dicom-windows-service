#include "global/geometry/Point.h"

#include "global/geometry/Size.h"

namespace sedeen {

Point::Point() : x_(), y_() {}

Point::Point(int x, int y) : x_(x), y_(y) {}

int Point::getX() const {
  return x_;
}

Point& Point::setX(int x) {
  x_ = x;
  return *this;
}

int Point::getY() const {
  return y_;
}

Point& Point::setY(int y) {
  y_ = y;
  return *this;
}

std::ostream& operator<<(std::ostream& out, const Point& point) {
  return out << '(' << point.getX()
             << ',' << point.getY()
             << ')';
}

bool operator==(const Point& lhs, const Point& rhs) {
  return lhs.getX() == rhs.getX() && lhs.getY() == rhs.getY();
}

Point operator-(Point point) {
  point.setX(-point.getX());
  point.setY(-point.getY());
  return point;
}

Point& operator+=(Point& a, const Point& b) {
  a.setX(a.getX() + b.getX());
  a.setY(a.getY() + b.getY());
  return a;
}

Point operator+(Point p1, const Point& p2) {
  return p1 += p2;
}

Point& operator-=(Point& a, Point b) {
  return a += -b;
}

Point operator-(Point p1, const Point& p2) {
  return p1 -= p2;
}

Point& operator*=(Point& a, const Point& b) {
  a.setX(a.getX() * b.getX());
  a.setY(a.getY() * b.getY());
  return a;
}

Point operator*(Point p1, const Point& p2) {
  return p1 *= p2;
}

Point& operator*=(Point& point, const Size& size) {
  point.setX(point.getX() * size.width());
  point.setY(point.getY() * size.height());
  return point;
}

Point operator*(Point point, const Size& size) {
  return point *= size;
}

Point operator*(const Size& size, Point point) {
  return point *= size;
}

QPoint toQt(const Point& point) {
  return QPoint(point.getX(), point.getY());
}

Point fromQt(const QPoint& qpoint) {
  return Point(qpoint.x(), qpoint.y());
}

} // namespace sedeen
