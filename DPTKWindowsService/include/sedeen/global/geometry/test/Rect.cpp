#include "global/geometry/Rect.h"

#include "gtest/gtest.h"

#include "global/geometry/Point.h"
#include "global/geometry/Size.h"

namespace sedeen {

TEST(RectTest, DefaultConstruction) {
  Rect rect;
  EXPECT_TRUE(isEmpty(rect));
}

TEST(RectTest, ConstructorPositionSize) {
  const int x = 4;
  const int y = -10;
  const Point p(x, y);

  const int w = 11;
  const int h = 12;
  const Size s(w, h);

  const Rect rect(p, s);
  EXPECT_EQ(x, rect.x());
  EXPECT_EQ(y, rect.y());
  EXPECT_EQ(w, rect.width());
  EXPECT_EQ(h, rect.height());
}

TEST(RectTest, Project) {
  const Rect region(Point(-5, -5), Size(10, 10));

  /// Forward projection
  const Rect from1(Point(), Size(100, 100));
  const Rect to1(Point(), Size(10, 10));
  EXPECT_EQ(Rect(Point(-1, -1), Size(2, 2)), project(region, from1, to1));

  /// Reverse projection
  const Rect& from2 = to1;
  const Rect& to2 = from1;
  EXPECT_EQ(Rect(Point(-50, -50), Size(100, 100)), project(region, from2, to2));

  /// Project onto an empty Rect results in an empty Rect at the origin
  EXPECT_EQ(Rect(), project(region, from1, Rect()));
  EXPECT_EQ(Rect(), project(region, from2, Rect()));

  /// Projection of an empty Rect to a valid space translates the beginning
  const Rect empty_region(Point(-5, -5), Size());
  EXPECT_EQ(Rect(Point(-1, -1), Size()), project(empty_region, from1, to1));
  EXPECT_EQ(Rect(Point(-50, -50), Size()), project(empty_region, from2, to2));
}

TEST(RectTest, IntersectWith) {
  const Rect r0(Point(0, 0), Size(5, 5));

  // Case 1: non-intersection
  const Rect r1(Point(-10, -10), Size(5, 5));
  EXPECT_TRUE(isEmpty(intersection(r0, r1)));

  // Case 2: total overlap
  const Rect r2(Point(-5, -5), Size(20, 20));
  EXPECT_EQ(r0, intersection(r0, r2));

  // Case 3: partial overlap
  const Rect r3(Point(-2, -2), Size(5, 5));
  EXPECT_EQ(Rect(Point(0, 0), Size(3, 3)), intersection(r0, r3));
}

TEST(RectTest, Constrain) {
  Point containerOrigin(0, 0);
  Size containerSize(5, 5);
  const Rect r0(containerOrigin, containerSize);

  // Case 1: Smaller already inside
  Point innerOrigin(1, 1);
  Size smallerSize(2, 2);
  Rect r1(innerOrigin, smallerSize);
  EXPECT_EQ(Rect(r1), constrain(r0, r1));

  // Case 2: Smaller from lower x and y
  Point lowerOrigin(-40, -40);
  Rect r2(lowerOrigin, smallerSize);
  EXPECT_EQ(Rect(containerOrigin, smallerSize), constrain(r0, r2));

  // Case 3: Smaller from higher x and y
  Point higherOrigin(40, 40);
  Rect r3(higherOrigin, smallerSize);
  EXPECT_EQ(Rect(Point(3, 3), smallerSize), constrain(r0, r3));

  Size largerSize(10, 10);

  // Case 4: Larger overlapping
  EXPECT_EQ(r0, constrain(r0, Rect(Point(-1, -1), largerSize)));

  // Case 5: Larger from lower x and y
  EXPECT_EQ(r0, constrain(r0, Rect(lowerOrigin, largerSize)));

  // Case 6: Larger from higher x and y
  EXPECT_EQ(r0, constrain(r0, Rect(higherOrigin, largerSize)));
}

/// Test both containing indices and containing Point objects.
TEST(RectTest, Contains) {
  const Rect r(Point(0, 0), Size(10, 10));
  EXPECT_TRUE(contains(r, 0, 0));
  EXPECT_TRUE(r.contains(Point()));

  EXPECT_TRUE(contains(r, 6, 6));
  EXPECT_TRUE(r.contains(Point(6, 6)));

  EXPECT_FALSE(contains(r, 10, 10));
  EXPECT_FALSE(r.contains(Point(10, 10)));

  EXPECT_FALSE(contains(r, -1, -1));
  EXPECT_FALSE(r.contains(Point(-1, -1)));
}

TEST(RectTest, ToQt) {
  int x = 2;
  int y = 5;
  int w = 19;
  int h = 2000;

  EXPECT_EQ(QRect(x, y, w, h), toQt(Rect(Point(x, y), Size(w, h))));
}

TEST(RectTest, FromQt) {
  int x = 56;
  int y = -11;
  int w = 1;
  int h = 2;

  EXPECT_EQ(Rect(Point(x, y), Size(w, h)), fromQt(QRect(x, y, w, h)));
}

} // namespace sedeen
