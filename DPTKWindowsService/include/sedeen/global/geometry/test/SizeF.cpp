#include "global/geometry/SizeF.h"

#include "gtest/gtest.h"

namespace sedeen {

TEST(SizeFTest, Constructor) {
  double width = 10;
  double height = 20;
  SizeF size(width, height);
  EXPECT_EQ(width, size.width());
  EXPECT_EQ(height, size.height());
}

TEST(SizeFTest, Invalid) {
  SizeF s1(0, 1);
  EXPECT_TRUE(isEmpty(s1));
  SizeF s2(1, 0);
  EXPECT_TRUE(isEmpty(s2));
  SizeF s3(1, 1);
  EXPECT_FALSE(isEmpty(s3));
}

TEST(SizeFTest, Equality) {
  SizeF s1(3, 6);
  SizeF s2(5, 0);

  EXPECT_EQ(s1, s1);
  EXPECT_NE(s1, s2);
  EXPECT_EQ(s2, s2);
}

} // namespace
