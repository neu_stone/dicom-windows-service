#include "gtest/gtest.h"

#include "global/geometry/Point.h"

namespace sedeen {

TEST(PointTest, ToQt) {
  int x = 92;
  int y = -1;

  EXPECT_EQ(QPoint(x, y), toQt(Point(x, y)));
}

TEST(PointTest, FromQt) {
  int x = -28;
  int y = 55;

  EXPECT_EQ(Point(x, y), fromQt(QPoint(x, y)));
}

} // namespace sedeen
