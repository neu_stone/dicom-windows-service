#include "global/geometry/Size.h"

#include "gtest/gtest.h"

namespace sedeen {

TEST(SizeTest, Constructor) {
  int width = 10;
  int height = 20;
  Size size(width, height);
  EXPECT_EQ(width, size.width());
  EXPECT_EQ(height, size.height());
}

TEST(SizeTest, Invalid) {
  Size s1(0, 1);
  EXPECT_TRUE(isEmpty(s1));
  Size s2(1, 0);
  EXPECT_TRUE(isEmpty(s2));
  Size s3(1, 1);
  EXPECT_FALSE(isEmpty(s3));
}

TEST(SizeTest, Equality) {
  Size s1(3, 6);
  Size s2(5, 0);

  EXPECT_EQ(s1, s1);
  EXPECT_NE(s1, s2);
  EXPECT_EQ(s2, s2);
}

TEST(SizeTest, Area) {
  EXPECT_EQ(100, area(Size(10, 10)));
  EXPECT_EQ(0, area(Size(5, 0)));
  EXPECT_EQ(0, area(Size(0, 88)));
}

TEST(SizeTest, ToQt) {
  int w = 5;
  int h = 10;
  EXPECT_EQ(QSize(w, h), toQt(Size(w, h)));
}

TEST(SizeTest, FromQt) {
  int w = 5;
  int h = 10;
  EXPECT_EQ(Size(w, h), fromQt(QSize(w, h)));
}

} // namespace
