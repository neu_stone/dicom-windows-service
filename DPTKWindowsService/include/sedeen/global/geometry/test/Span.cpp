#include "global/geometry/Span.h"

#include "gtest/gtest.h"

#include <cmath>

namespace sedeen {

TEST(SpanTest, DefaultConstruction) {
  Span span;

  EXPECT_EQ(0, span.begin());
  EXPECT_EQ(0, span.extent());
}

TEST(SpanTest, IntegerProject) {
  const Span s(1, 1);
  const Span from(0, 1);
  const Span to(0, 5);
  EXPECT_EQ(Span(5, 5), project(s, from, to));
}

TEST(SpanTest, UnevenProjection) {
  Span s(0, 1);
  const Span from(0, 2);
  const Span to(0, 3);
  EXPECT_EQ(Span(0, 2), project(s, from, to));

  s.setBegin(1);
  EXPECT_EQ(Span(1, 2), project(s, from, to));

  s.setBegin(2);
  EXPECT_EQ(Span(3, 2), project(s, from, to));
}

TEST(SpanTest, NegativeIntegerProject) {
  Span s(-20, 5);

  const Span to(0, 1);

  const Span from1(0, 1);
  EXPECT_EQ(Span(-20, 5), project(s, from1, to));

  const Span from2(0, 5);
  EXPECT_EQ(Span(-4, 1), project(s, from2, to));

  const Span from3(-10, 5);
  EXPECT_EQ(Span(-2, 1), project(s, from3, to));
}

TEST(SpanTest, End) {
  Span s(20, 20);
  EXPECT_EQ(40, end(s));
}

TEST(SpanTest, ContainsInt) {
  Span s(-5, 15);
  EXPECT_FALSE(s.contains(-6));
  EXPECT_TRUE(s.contains(-5));
  EXPECT_TRUE(s.contains(0));
  EXPECT_TRUE(s.contains(9));
  EXPECT_FALSE(s.contains(10));

  EXPECT_TRUE(s.contains(s.begin()));
  EXPECT_FALSE(s.contains(end(s)));
}

TEST(SpanTest, ContainsSpan) {
  // Self-containment
  Span container(-1, 3);
  EXPECT_TRUE(container.contains(container));

  // Left non-overlap
  Span r0(-10, 2);
  EXPECT_FALSE(container.contains(r0));

  // Partial left-overlap
  Span r1(-5, 6);
  EXPECT_FALSE(container.contains(r1));

  // Fully contained overlap
  Span r2(0, 1);
  EXPECT_TRUE(container.contains(r2));
  EXPECT_FALSE(r2.contains(container));

  // Partial right-overlap
  Span r3(0, 10);
  EXPECT_FALSE(container.contains(r3));

  // Right non-overlap
  Span r4(10, 5);
  EXPECT_FALSE(container.contains(r4));

  // Empty non-overlap
  EXPECT_TRUE(container.contains(Span()));
  EXPECT_FALSE(Span().contains(container));
}

TEST(SpanTest, Intersects) {
  // Self-intersection
  Span s0(0,5);
  EXPECT_TRUE(intersects(s0, s0));

  // Also checking symmetry of the function

  // Non-overlap, but abutted.
  Span s1(-2, 2);
  EXPECT_FALSE(intersects(s0, s1));
  EXPECT_FALSE(intersects(s1, s0));

  // Partial left-overlap
  Span s2(-2, 4);
  EXPECT_TRUE(intersects(s0, s2));
  EXPECT_TRUE(intersects(s2, s0));

  // Full overlap
  Span s3(1,2);
  EXPECT_TRUE(intersects(s0, s3));
  EXPECT_TRUE(intersects(s3, s0));
}

TEST(SpanTest, Intersection) {
  //              s0: | -  -  -  -  -  -  -  -  -  -|
  //              s0: | -  -  -  -  -  -  -  -  -  -|
  //              s0: | -  -  -  -  -  -  -  -  -  -|
  // -6 -5 -4 -3 -2 -1  0  1  2  3  4  5  6  7  8  9 10 11 12
  Span s0(0, 10);
  EXPECT_EQ(s0, intersection(s0, s0));

  // Full overlap
  //              s0: | -  -  -  -  -  -  -  -  -  -|
  //              s1: | -  -  -  -  -|
  //          result: | -  -  -  -  -|
  // -6 -5 -4 -3 -2 -1  0  1  2  3  4  5  6  7  8  9 10 11 12
  Span s1(0, 5);
  EXPECT_EQ(s1, intersection(s0, s1));
  EXPECT_EQ(s1, intersection(s1, s0));

  // Partial left overlap
  //              s0: | -  -  -  -  -  -  -  -  -  -|
  //        s2: | -  -  -  -  -|
  //          result: | -  -  -|
  // -6 -5 -4 -3 -2 -1  0  1  2  3  4  5  6  7  8  9 10 11 12
  Span s2(-2, 5);
  EXPECT_EQ(Span(0, 3), intersection(s0, s2));
  EXPECT_EQ(Span(0, 3), intersection(s2, s0));

  // Partial right overlap
  //              s0: | -  -  -  -  -  -  -  -  -  -|
  //                    s3: | -  -  -  -  -  -  -  -  -  -|
  //                result: | -  -  -  -  -  -  -  -|
  // -6 -5 -4 -3 -2 -1  0  1  2  3  4  5  6  7  8  9 10 11 12
  Span s3(2, 10);
  EXPECT_EQ(Span(2, 8), intersection(s0, s3));
  EXPECT_EQ(Span(2, 8), intersection(s3, s0));

  // Non-overlap, but abutment.
  // Only guarantees the zero span, begin() could be anywhere.
  //              s0: | -  -  -  -  -  -  -  -  -  -|
  //                                            s4: | -  -  -|
  //       result: <a zero-length span, somewhere>
  // -6 -5 -4 -3 -2 -1  0  1  2  3  4  5  6  7  8  9 10 11 12
  Span s4(10, 3);
  EXPECT_TRUE(isEmpty(intersection(s0, s4)));
  EXPECT_TRUE(isEmpty(intersection(s4, s0)));
}

TEST(SpanTest, SnapWithin) {
  // Result 1: Snap to self. Should have no effect.
  //                       s0: | -  -  -  -|
  //                       s0: | -  -  -  -|
  //                 result 1: | -  -  -  -|
  // -6 -5 -4 -3 -2 -1  0  1  2  3  4  5  6  7  8  9 10 11 12
  Span s0(3, 4);
  // #1:
  EXPECT_EQ(s0, constrain(s0, s0));

  // Result 2: Snap from non-overlap to the left, smaller region
  // Result 3: Snap from non-overlap to the right, larger region
  //                       s0: | -  -  -  -|
  //           s1: | -  -  -|
  //                 result 2: | -  -  -|
  //     result 3: | -  -  -|
  // -6 -5 -4 -3 -2 -1  0  1  2  3  4  5  6  7  8  9 10 11 12
  Span s1(-1, 3);
  // #2:
  EXPECT_EQ(Span(3, 3), constrain(s0, s1));
  // #3:
  EXPECT_EQ(s1, constrain(s1, s0));

  // Result 4: Snap from non-overlap to the left, larger region
  // Result 5: Snap from non-overlap to the right, smaller region
  //                       s0: | -  -  -  -|
  //     s2: | -  -  -  -  -|
  //                 result 4: | -  -  -  -|
  // result 5:  | -  -  -  -|
  // -6 -5 -4 -3 -2 -1  0  1  2  3  4  5  6  7  8  9 10 11 12
  Span s2(-3, 5);
  // #4:
  EXPECT_EQ(s0, constrain(s0, s2));
  // #5:
  EXPECT_EQ(Span(-2, 4), constrain(s2, s0));
}

TEST(SpanTest, IsEmpty) {
  EXPECT_TRUE(isEmpty(Span(55,0)));
  EXPECT_TRUE(isEmpty(Span()));
  EXPECT_FALSE(isEmpty(Span(0, 10)));
  EXPECT_FALSE(isEmpty(Span(-3, 3)));
}

} // namespace sedeen
