#ifndef SEDEEN_SRC_GLOBAL_GEOMETRY_PLACED_H
#define SEDEEN_SRC_GLOBAL_GEOMETRY_PLACED_H

#include "global/DllApi.h"

namespace sedeen {

/// Interface for classes that need to work with physical placement algorithms.
//
/// Represents anything that has a modifiable position and rotation.
class Placed {
 public:
  virtual ~Placed();

  /// Get the origin of the shape in the enclosing frame of reference.
  PointF getOrigin() const;

  /// Set the origin of the shape in the enclosing frame of reference.
  //
  /// \param origin
  /// Location with respect to the enclosing frame. Specified in um.
  void setOrigin(const PointF& origin);

  /// Get the rotation of this shape with respect to the enclosing frame.
  //
  /// Returned value will be in radians.
  double getRotation() const;

  /// Set the rotation of this shape with respect to the enclosing frame.
  //
  /// \param radians
  /// Counterclockwise rotation of this object, in radians, of this object with
  /// respect to its enclosing frame of reference.
  void setRotation(double radians);

 protected:
  Placed();

 private:
  /// \copydoc getOrigin()
  virtual PointF doGetOrigin() const = 0;

  /// \copydoc setOrigin()
  virtual void doSetOrigin(const PointF& origin) = 0;

  /// \copydoc getRotation()
  virtual double doGetRotation() const = 0;

  /// \copydoc setRotation()
  virtual void doSetRotation(double radians) = 0;
};

/// Rotate the placed shape about an arbitrary origin.
//
/// \relates Placed
///
/// Transform the passed-in \p shape by an incremental rotation about an
/// arbitrary \p origin, specified in um with respect to the frame of reference
/// containing \p shape.
///
/// \param shape
///
/// \param origin
///
/// \param delta_radians
void rotateAbout(Placed& shape, const PointF& origin, double delta_radians);

} // namespace sedeen

#endif
