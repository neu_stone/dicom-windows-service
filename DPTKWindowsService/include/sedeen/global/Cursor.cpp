#include "global/Cursor.h"

namespace sedeen {
namespace cursor {

namespace {

QCursor initCursor(const QString& resource_name, double xhot, double yhot) {
  QPixmap pix(resource_name);
  int w = pix.width() / 2;
  int h = pix.height() / 2;
  return QCursor(pix.scaled(w, h), w * xhot, h * yhot);
}

} // namespace

QCursor leftArrow() {
  static QCursor cur = initCursor(":/sedeen/icons/arrow_left.png", 0, 0.5);
  return cur;
}

QCursor rightArrow() {
  static QCursor cur = initCursor(":/sedeen/icons/arrow_right.png", 1, 0.5);
  return cur;
}

QCursor upArrow() {
  static QCursor cur = initCursor(":/sedeen/icons/arrow_up.png", 0.5, 0);
  return cur;
}

QCursor downArrow() {
  static QCursor cur = initCursor(":/sedeen/icons/arrow_down.png", 0.5, 1);
  return cur;
}

} // namespace cursor
} // namespace sedeen
