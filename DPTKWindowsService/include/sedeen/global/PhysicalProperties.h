#ifndef SEDEEN_SRC_GLOBAL_PHYSICALPROPERTIES_H
#define SEDEEN_SRC_GLOBAL_PHYSICALPROPERTIES_H

// System includes
#include <QSizeF>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include "global/SRTTransform.h"

#include "global/DllApi.h"

namespace sedeen {

/// Handles transforming an image in physical space.
//
/// The calss mergs the SRTTransform with the image pixel size in order to have
/// the required information for handling physical mappings.
///
/// \sa SRTTransform for more informaiton on the transformations.
class SEDEEN_GLOBAL_API PhysicalProperties {
 public:
  /// Constructor
  //
  /// Required the image's dimentions to perform physical mappings. The image's
  /// pixel size and transform are assumed to be unit and identity by default.
  ///
  /// \param dims
  /// The image dimensions
  ///
  /// \param pixsize
  /// The image pixel size
  ///
  /// \param tx
  /// The image transformation
  PhysicalProperties(const QSize& dims,
                     const QSizeF& pixsize = QSizeF(1.0, 1.0),
                     const SRTTransform& tx = SRTTransform());
  /// Gets the image transform
  //
  /// \return
  /// The transform
  SRTTransform& transform();

  /// Gets the image transform
  //
  /// \return
  /// The transform
  const SRTTransform& transform() const;

  /// Gets the image transform
  //
  /// \param tx
  /// The transform
  void setTransform(const SRTTransform& tx);

  /// Gets the image pixel size
  //
  /// \return
  /// The pixel size
  const QSizeF& pixelSize() const;

  /// Sets the image pixel size
  //
  /// \param pixsize
  /// The pixel size
  void setPixelSize(const QSizeF& pixsize);

  /// Gets the image dimensions in pixels
  //
  /// \return
  /// The image dimensions
  const QSize& dims() const;

  /// Gets the image dimensions in physical units
  //
  /// \return
  /// The image size
  const QSizeF& size() const;

  /// Gets the image dimensions in physical units
  //
  /// \return
  /// The image size
  const QRectF& rect() const;

  /// Gets the image dimensions in physical units
  //
  /// \return
  /// The image size
  const QPointF& center() const;

  /// Reads the stream until a PhysicalProperties description is found
  //
  /// Returns the PhysicalProperties object for the description.
  ///
  /// \param reader
  /// The xml stream to read
  ///
  /// \return
  /// PhysicalProperties object
  static PhysicalProperties* readXml(QXmlStreamReader& reader);

  /// Writes a description of the physical properties to the stream.
  //
  /// \param writer
  /// The xml stream to write to
  ///
  /// \return
  /// \c true if sucessful, \c false otherwise
  bool writeXml(QXmlStreamWriter& writer) const;

  /// Reads the stream until a PhysicalProperties description is found
  //
  /// Uses readXmlMain() to the work. Fill \a pp with the descriptions of pixel
  /// spacing and transform if found. If errors are detected, \a pp is not
  /// modified.
  ///
  /// \param reader
  /// The xml stream to read
  ///
  /// \param pp
  /// The properties object to fill
  ///
  /// \return
  /// \c true if sucessful, \c false otherwise
  static bool readXml(QXmlStreamReader& reader, PhysicalProperties& pp);

 private:
  /// Calcualates \a m_size, \a m_rect and \a m_center;
  //
  /// Should be called when pixel size is changed.
  void calculate();

  /// Reads the stream until a PhysicalProperties description is found
  //
  /// Uses readXmlMain() to the work. Fill \a pp with the descriptions of pixel
  /// spacing and transform if found. If errors are detected, \a pp is not
  /// modified.
  ///
  /// \param reader
  /// The xml stream to read
  ///
  /// \param pp
  /// The properties object to fill
  ///
  /// \return
  /// \c true if sucessful, \c false otherwise
  static bool readXmlMain(QXmlStreamReader& reader, PhysicalProperties& pp);

 private:
  QSize           m_dims;
  QSizeF          m_pix;
  SRTTransform    m_tx;

  QSizeF          m_size;
  QRectF          m_rect;
  QPointF         m_center;
};

} // namespace sedeen

#endif

