#include "global/MathOps.h"

#include <cassert>
#include <cmath>

namespace sedeen {
namespace math_ops {

double intdiv(double dividend, double divisor) {
  assert(0.0 != divisor);
  return dividend / divisor;
}

double round(double number) {
  double integral;
  double remainder = std::modf(number, &integral);
  if (remainder >= 0.5) {
    ++integral;
  } else if (remainder < -0.5) {
    --integral;
  }
  return integral;
}

double log2(double number) {
  return logn(number, 2);
}

double logn(double number, unsigned int base) {
  assert(0.0 < number);
  assert(0.0 != base && 1.0 != base);

  return std::log(number) / std::log(static_cast<double>(base));
}

double metricToCompact(double value, MetricUnits& after, MetricUnits before) {
  const double absval = std::fabs(value);
  const double valFact = pow(10.0, before);
  const int max = Tera;
  const int min = Femto;

  for (int unit = max; unit >= min; unit -= 3) {
    double factor = pow(10.0, unit) / valFact;

    // Find the correct unit
    if (absval >= factor) {
      after = static_cast<MetricUnits>(unit);
      return (value / factor);
    }

    // For everything less than the smallest unit
    if (min == unit) {
      after = static_cast<MetricUnits>(unit);
      return (value / factor);
    }
  }

  // should not be here
  assert(false);
  return 0;
}

double metricToMetric(double value, MetricUnits before, MetricUnits after) {
  return value * pow(10.0, before - after);
}

char fromMetricToString(MetricUnits unit) {
  switch( unit ) {
    case Femto:
      return 'f';

    case Pico:
      return 'p';

    case Nano:
      return 'n';

    case Micro:
      return 'u';

    case Milli:
      return 'm';

    case Base:
      return ' ';

    case Kilo:
      return 'k';

    case Mega:
      return 'M';

    case Giga:
      return 'G';

    case Tera:
      return 'T';

    default:
      return'?'; // should not be here;
  }
}

float intDivideRoundUp(float, float) {
  assert(!"Attempt to int-divide a float");
  return 0;
}

float intDivideRoundDown(float, float) {
  assert(!"Attempt to int-divide a float");
  return 0;
}

double intDivideRoundUp(double, double) {
  assert(!"Attempt to int-divide a double");
  return 0;
}

double intDivideRoundDown(double, double) {
  assert(!"Attempt to int-divide a double");
  return 0;
}

} // namespace math_ops
} // namespace sedeen
