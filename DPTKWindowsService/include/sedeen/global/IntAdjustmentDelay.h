#ifndef SEDEEN_SRC_GLOBAL_INTADJUSTMENTDELAY_H
#define SEDEEN_SRC_GLOBAL_INTADJUSTMENTDELAY_H

#include <QtCore>

#include "global/DllApi.h"

namespace sedeen {
namespace global {
        /// Collects value updates and transmits
class SEDEEN_GLOBAL_API IntAdjustmentDelay : public QObject
        {
            Q_OBJECT;
        public:
            IntAdjustmentDelay(int delayMs,
                               QObject* parent = 0);

        public slots:
            void
            setValue(int value);

        private slots:
            void
            update() const;

        signals:
            void
            update(int value) const;

        private:
            QTimer* m_delayTimer;

            const int m_delay;

            int m_value;
        }; // end class AdjustmentDelay
    } // end namespace global
} // end namespace sedeen

#endif
