// Primary header
#include "global/Geometry.h"

// Qt includes
#include <QLineF>

namespace sedeen {
namespace geometry {

double l2Norm(const QPointF& p1, const QPointF& p2) {
  return QLineF(p1,p2).length();
}

double magnitude(const QPointF& p) {
  return std::sqrt( p.x()*p.x() + p.y()*p.y() );
}

QPointF lineCenter(const QPointF& p1, const QPointF& p2) {
  return ( p2 + p1 ) / 2.0;
}

bool my_isnan(const double &val) {
#ifdef WIN32
  return _isnan(val);
#else
  return val != val;
#endif
}

bool isinf(const double &val) {
  return std::numeric_limits<double>::has_infinity &&
      std::numeric_limits<double>::infinity() == std::abs(val);
}

} // namespace geometry
} // namespace sedeen
