#include "global/CommandHistory.h"

#include <cassert>

namespace sedeen {

CommandHistory::CommandHistory() : m_history(), m_histit(m_history.begin()) {}

CommandHistory::~CommandHistory() {}

void CommandHistory::executeCommand(CommandBase *command, bool merge) {
  // Clear any redos in buffer
  m_history.erase(m_histit, m_history.end());

  // Add the new command to the history buffer
  m_history.push_back( command );
  m_histit = m_history.end();

  // Execute the command
  command->execute();

  // merge with previous command
  if( merge )
    mergeLatestCommands( );
}

bool CommandHistory::mergeLatestCommands() {
  bool updateReq = false;

  if( m_history.size() < 2 )
    return updateReq;

  for( int i = (int)m_history.size() - 2; i >= 0 ; i-- ) {
    bool merged = m_history[i]->merge(*(m_history[i+1]));
    if( merged ) {
      updateReq = merged;
      m_history.pop_back();
      m_histit = m_history.end();
    } else {
      break;
    }
  }
  return updateReq;
}

unsigned int CommandHistory::size() const {
  return m_history.size();
}

unsigned int CommandHistory::clear(unsigned int num) {
  unsigned int count = num;

  if( num == 0 || num > size() )
    count = size();

  for( unsigned int i = 0; i < count; i++ )
    m_history.pop_back();

  m_histit = m_history.end();

  return count;
}

unsigned int CommandHistory::clearUndos() {
  unsigned int count = m_history.size();

  m_history.clear();;
  m_histit = m_history.end();

  return count;
}

unsigned int CommandHistory::clearRedos() {
  unsigned int count = 0;

  while( m_histit <= m_history.end() ) {
    m_history.pop_back();
    count++;
  }
  m_histit = m_history.end();

  return count;
}

const CommandBase& CommandHistory::command(unsigned int index) const {
  assert( index < size() );
  return *(m_history[index]);
}

void CommandHistory::undo() {
  while( m_histit > m_history.begin() ) {
    --m_histit;
    // ignore these in undoing
    if( (*m_histit)->canUndo() ) {
      (*m_histit)->unExecute();
      break;
    }
  }
}

void CommandHistory::redo() {
  while( m_histit < m_history.end() ) {
    // ignore these when redoing
    if( (*m_histit)->canRedo() ) {
      (*m_histit)->execute();
      ++m_histit;
      break;
    }
    ++m_histit;
  }
}

int CommandHistory::currentPosition() const {
  ListType::const_iterator it = m_histit;
  if( m_history.size() ) {
    return (it - m_history.begin());
  } else {
    return -1;
  }
}

int CommandHistory::numUndos() const {
  int count  = 0;
  ListType::const_iterator it = m_histit;
  while( it > m_history.begin() ) {
    --it;
    if( (*it)->canUndo() )
      ++count;
  }
  return count;
}

int CommandHistory::numRedos() const {
  int count  = 0;
  ListType::const_iterator it = m_histit;
  it++;
  while( it < m_history.end() ) {
    if( (*it)->canRedo() )
      count++;
    it++;
  }
  return count;
}

} // namespace sedeen
