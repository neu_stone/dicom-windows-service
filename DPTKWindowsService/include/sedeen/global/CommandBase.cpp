// Primary header
#include "global/CommandBase.h"

// System headers
#include <QAtomicInt>

namespace sedeen {

unsigned int uniqueCommandID() {
  static QAtomicInt ids;
  return ids.fetchAndAddAcquire(1);
}

} // namespace sedeen
