#include "global/memory/MemoryInfo.h"

#include "gtest/gtest.h"

#include <boost/scoped_ptr.hpp>

namespace sedeen {

class MemoryInfoTest : public ::testing::Test {
 protected:
  MemoryInfoTest() : m_mem(MemoryInfo::instance()) {
  }

  virtual void SetUp() {
    ASSERT_TRUE(nullptr != m_mem);
  }

  boost::scoped_ptr<MemoryInfo> m_mem;
}; // class MemoryInfoTest

TEST_F(MemoryInfoTest, SystemMemory) {
  EXPECT_FALSE(0 == m_mem->system());
}

TEST_F(MemoryInfoTest, AvailableMemory) {
  EXPECT_FALSE(0 == m_mem->available());
}

TEST_F(MemoryInfoTest, ProcessMemory) {
  EXPECT_FALSE(0 == m_mem->process());
}

}
