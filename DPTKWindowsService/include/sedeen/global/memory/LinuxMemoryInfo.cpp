#include "global/memory/LinuxMemoryInfo.h"

#ifdef SEDEEN_OS_LINUX

// For process info
#include <unistd.h>
#include <ios>
#include <iostream>
#include <fstream>
#include <string>

// For sysinfo
#include <sys/types.h>
#include <sys/sysinfo.h>

namespace sedeen {

LinuxMemoryInfo::LinuxMemoryInfo() {
}

LinuxMemoryInfo::~LinuxMemoryInfo() {
}

double LinuxMemoryInfo::getSystem() const {
  // see sysinfo man page
  // this method works for Linux version >= 2.3.48
  // because sysinfo.mem_unit field is not defined in lower versions
  // an alternate method whould be to use utsname.h and check the kernel version
  // or we can also parse /proc/meminfo file, like we do to get the process
  // memory in linux

  double total = 0.0;
  struct sysinfo memInfo;

  // Get structure - non zero return is error
  if (sysinfo(&memInfo))
    return total;

  total = memInfo.totalram * memInfo.mem_unit;
  return total;
}

double LinuxMemoryInfo::getAvailable() const {
  // see sysinfo man page
  // this method works for Linux version >= 2.3.48
  // becuause sysinfo.mem_unit field is not defined in lower versions
  // an alternate method whould be to use utsname.h and check the kernel version
  // or we can also parse /proc/meminfo file, like we do to get the process
  // memory in linux

  double total = 0.0;
  struct sysinfo memInfo;

  // Get structure - non zero return is error
  if (sysinfo(&memInfo))
    return total;

  total = memInfo.freeram * memInfo.mem_unit;

  return total;
}

double LinuxMemoryInfo::getProcess() const {
  //http://stackoverflow.com/questions/669438/how-to-get-memory-usage-at-run-time-in-c
  //////////////////////////////////////////////////////////////////////////////
  //
  // process_mem_usage(double &, double &) - takes two doubles by reference,
  // attempts to read the system-dependent data for a process' virtual memory
  // size and resident set size, and return the results in KB.
  //
  // On failure, returns 0.0, 0.0
  using std::ios_base;
  using std::ifstream;
  using std::string;

  double resident_set = 0.0;

  // 'file' stat seems to give the most reliable results
  //
  ifstream stat_stream("/proc/self/stat",ios_base::in);
  if (!stat_stream.good())
    return 0;

  // dummy vars for leading entries in stat that we don't care about
  //
  string pid, comm, state, ppid, pgrp, session, tty_nr;
  string tpgid, flags, minflt, cminflt, majflt, cmajflt;
  string utime, stime, cutime, cstime, priority, nice;
  string O, itrealvalue, starttime;

  // the two fields we want
  //
  unsigned long vsize;
  long rss;

  stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
              >> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
              >> utime >> stime >> cutime >> cstime >> priority >> nice
              >> O >> itrealvalue >> starttime >> vsize >> rss;
  // don't care about the rest

  // error check - we should not be at eof or have any errors
  if (!stat_stream.good())
    return 0;

 // in case x86-64 is configured to use 2MB pages
  long page_size_kb = sysconf(_SC_PAGE_SIZE);
  resident_set = rss * page_size_kb;

  return resident_set;
}

} // namespace sedeen

#endif
