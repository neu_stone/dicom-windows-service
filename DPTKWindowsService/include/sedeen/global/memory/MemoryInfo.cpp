#include "global/memory/MemoryInfo.h"

#include "global/memory/LinuxMemoryInfo.h"
#include "global/memory/MacMemoryInfo.h"
#include "global/memory/WindowsMemoryInfo.h"

namespace sedeen {

MemoryInfo* MemoryInfo::instance() {
#if defined SEDEEN_OS_LINUX
  return new LinuxMemoryInfo();
#elif defined SEDEEN_OS_WINDOWS
  return new WindowsMemoryInfo();
#elif defined SEDEEN_OS_APPLE
  return new MacMemoryInfo();
#else
  return new MemoryInfo();
#endif
}

MemoryInfo::~MemoryInfo() {
}

double MemoryInfo::system() const {
  return getSystem();
}

double MemoryInfo::available() const {
  return getAvailable();
}

double MemoryInfo::process() const {
  return getProcess();
}

MemoryInfo::MemoryInfo() {
}

double MemoryInfo::getSystem() const {
  return 0;
}

double MemoryInfo::getAvailable() const {
  return 0;
}

double MemoryInfo::getProcess() const {
  return 0;
}

} // namespace sedeen
