#ifndef SEDEEN_SRC_GLOBAL_MEMORY_MACMEMORYINFO_H
#define SEDEEN_SRC_GLOBAL_MEMORY_MACMEMORYINFO_H

#include "global/memory/MemoryInfo.h"

namespace sedeen {

class MacMemoryInfo : public MemoryInfo {
 public:
  MacMemoryInfo();

  virtual ~MacMemoryInfo();

 private:
  virtual double getSystem() const;

  virtual double getAvailable() const;

  virtual double getProcess() const;
}; // class MacMemoryInfo

} // namespace sedeen

#endif
