#include "global/memory/MacMemoryInfo.h"

#ifdef SEDEEN_OS_APPLE

#include <sys/types.h>
#include <sys/sysctl.h>
#include <mach/vm_statistics.h>
#include <mach/mach_types.h>
#include <mach/mach_init.h>
#include <mach/mach_host.h>

namespace sedeen {

MacMemoryInfo::MacMemoryInfo() {
}

MacMemoryInfo::~MacMemoryInfo() {
}

double MacMemoryInfo::getSystem() const {
  // this code is untested - but obtained from a reliable source
  //http://stackoverflow.com/questions/63166/how-to-determine-cpu-and-memory-consumption-from-inside-a-process
  int64_t physical_memory;
  mib[0] = CTL_HW;
  mib[1] = HW_MEMSIZE;
  length = sizeof(int64_t);
  sysctl(mib, 2, &physical_memory, &length, NULL, 0);

  return physical_memory;
}

double MacMemoryInfo::getAvailable() const {
  // this code is untested - but obtained from a reliable source
  //http://stackoverflow.com/questions/63166/how-to-determine-cpu-and-memory-consumption-from-inside-a-process
  vm_size_t page_size;
  mach_port_t mach_port;
  mach_msg_type_number_t count;
  vm_statistics_data_t vm_stats;

  mach_port = mach_host_self();
  count = sizeof(vm_stats) / sizeof(natural_t);
  if (KERN_SUCCESS == host_page_size(mach_port, &page_size) &&
      KERN_SUCCESS == host_statistics(mach_port, HOST_VM_INFO,
                                      (host_info_t)&vm_stats, &count))
  {
    return (int64_t)vm_stats.free_count * (int64_t)page_size;
  }
}

double MacMemoryInfo::getProcess() const {
  // this code is untested - but obtained from a reliable source
  //http://stackoverflow.com/questions/63166/how-to-determine-cpu-and-memory-consumption-from-inside-a-process
  struct task_basic_info t_info;
  mach_msg_type_number_t t_info_count = TASK_BASIC_INFO_COUNT;

  if (KERN_SUCCESS != task_info(mach_task_self(),
                                TASK_BASIC_INFO, (task_info_t)&t_info,
                                &t_info_count))
  {
    return 0.0;
  }

  //resident size is in t_info.resident_size;
  // virtual size is in t_info.virtual_size;
  return t_info.resident_size;
}

#endif
