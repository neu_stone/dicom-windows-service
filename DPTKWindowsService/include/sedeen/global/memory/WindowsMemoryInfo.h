#ifndef SEDEEN_SRC_GLOBAL_MEMORY_WINDOWSMEMORYINFO_H
#define SEDEEN_SRC_GLOBAL_MEMORY_WINDOWSMEMORYINFO_H

#include "global/memory/MemoryInfo.h"

namespace sedeen {

class WindowsMemoryInfo : public MemoryInfo {
 public:
  WindowsMemoryInfo();

  virtual ~WindowsMemoryInfo();

 private:
  virtual double getSystem() const;

  virtual double getAvailable() const;

  virtual double getProcess() const;
}; // class WindowsMemoryInfo

} // namespace sedeen

#endif
