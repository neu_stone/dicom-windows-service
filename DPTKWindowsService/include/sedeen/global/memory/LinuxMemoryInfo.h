#ifndef SEDEEN_SRC_GLOBAL_MEMORY_LINUXMEMORYINFO_H
#define SEDEEN_SRC_GLOBAL_MEMORY_LINUXMEMORYINFO_H

#include "global/memory/MemoryInfo.h"

namespace sedeen {

class LinuxMemoryInfo : public MemoryInfo {
 public:
  LinuxMemoryInfo();

  virtual ~LinuxMemoryInfo();

 private:
  virtual double getSystem() const;

  virtual double getAvailable() const;

  virtual double getProcess() const;
}; // class LinuxMemoryInfo

} // namespace sedeen

#endif
