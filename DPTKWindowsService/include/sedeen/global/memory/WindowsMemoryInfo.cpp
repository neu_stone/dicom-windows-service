#include "global/memory/WindowsMemoryInfo.h"

#ifdef SEDEEN_OS_WINDOWS

#include <windows.h>
#include <psapi.h>

namespace sedeen {

WindowsMemoryInfo::WindowsMemoryInfo() {
}

WindowsMemoryInfo::~WindowsMemoryInfo() {
}

double WindowsMemoryInfo::getSystem() const {
  //from :  http://msdn.microsoft.com/en-us/library/aa455130.aspx
  MEMORYSTATUS   memInfo;
  memInfo.dwLength = sizeof(memInfo);
  GlobalMemoryStatus(&memInfo);
  return static_cast<double>(memInfo.dwTotalPhys);
}

double WindowsMemoryInfo::getAvailable() const {
  //from :  http://msdn.microsoft.com/en-us/library/aa455130.aspx
  MEMORYSTATUS   memInfo;
  memInfo.dwLength = sizeof(memInfo);
  GlobalMemoryStatus(&memInfo);
  return static_cast<double>(memInfo.dwAvailPhys);
}

double WindowsMemoryInfo::getProcess() const {
  //from: http://msdn.microsoft.com/en-us/library/ms682050(VS.85).aspx

  // get process status
  HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
                                FALSE, GetCurrentProcessId());
  double usage = 0;
  if (NULL != hProcess) {

    PROCESS_MEMORY_COUNTERS pmc;
    if (GetProcessMemoryInfo( hProcess, &pmc, sizeof(pmc))) {
      usage = static_cast<double>(pmc.WorkingSetSize);
    }

    CloseHandle(hProcess);
  }
  return usage;
}

} // namespace sedeen

#endif
