#ifndef SEDEEN_SRC_GLOBAL_MEMORYINFO_H
#define SEDEEN_SRC_GLOBAL_MEMORYINFO_H

#include "global/DllApi.h"

namespace sedeen {

/// Determine memory status for the current environment.
class SEDEEN_GLOBAL_API MemoryInfo {
 public:
  static MemoryInfo* instance();

  virtual ~MemoryInfo();

  /// Get the total number of bytes of physical memory.
  //
  /// \return
  /// The total number of bytes
  double system() const;

  /// Get the available number of bytes of physical memory.
  double available() const;

  /// Get the memory footprint, in bytes, of the current process.
  double process() const;

 protected:
  MemoryInfo();

 private:
  virtual double getSystem() const;

  virtual double getAvailable() const;

  virtual double getProcess() const;
}; // class MemoryInfo

} // namespace sedeen

#endif
