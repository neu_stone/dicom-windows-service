#ifndef SEDEEN_SRC_GLOBAL_COMMANDHISTORY_H
#define SEDEEN_SRC_GLOBAL_COMMANDHISTORY_H

#include <vector>

#include "global/CommandBase.h"
#include "global/DllApi.h"

namespace sedeen {

/// A class for keeping track of commands. Useful for undo and redo operations.
class SEDEEN_GLOBAL_API CommandHistory {
 public:
  CommandHistory();

  ~CommandHistory ();

  /// Executes the command and merges with previous if possible.
  //
  /// \param command
  /// The command to process
  ///
  /// \param merge
  /// Merges the current commad with previous, if possible.
  void executeCommand(CommandBase *command, bool merge = true);

  /// Merges a series of commands perfomred on the same object.
  //
  /// The merge only applies to consecutives commands that are of the same type
  /// as the last executed command. For example, a series of move commands can
  /// be merged into one move command if they all occured consectutively and the
  /// last command executed was a move command.
  ///
  /// \return true if at least one merge was sucessful, false otherwise
  bool mergeLatestCommands();

  /// Gets the number of commands in the buffer
  //
  /// \sa currentPosition()
  unsigned int size() const;

  /// Clear some commands starting from the latest.
  //
  /// \param num
  /// the maximum number of commands to remove (default value of 0, clears all
  /// commands)
  ///
  /// \return the number of commands deleted
  unsigned int clear(unsigned int num = 0);

  /// Clears all the undos currently available.
  //
  /// \return the number of commands deleted
  unsigned int clearUndos();

  /// Clears all the redos currently available.
  //
  /// \return the number of commands deleted
  unsigned int clearRedos();

  /// Gets the number of commands
  const CommandBase& command(unsigned int index) const;

  /// Undo the next executable action.
  //
  /// Ignores commands that are not undoable and proceeds to the next undoable
  /// command
  void undo();

  /// Redo the next executable action.
  //
  /// Ignores commands that are not redoable and proceeds to the next redoable
  /// command
  void redo();

  /// Returns the current position in the buffer.
  //
  /// All commands before currentPosition() are undos and all commands after it
  /// are redo events upto size().
  ///
  /// \note Not all commands can be performed, some commands such as MergeBlock
  /// are inserted for seperating commands and cannot really be undone or
  /// redone.
  ///
  /// \sa numUndos() and numRedos()
  ///
  /// \return the current position of the undo/redo buffer, or -1 if buffer is
  /// empty
  int currentPosition() const;

  /// Returns the number of undos available.
  //
  /// Includes only executable commands and ignores command such as MergeBlock
  /// that have no action associated with them.
  ///
  /// \sa size(), numRedos, and currentPosition()
  ///
  /// \return the number of undos available
  int numUndos() const;

  /// Returns the number of redos available.
  //
  /// Includes only executable commands and ignores command such as MergeBlock
  /// that have no action associated with them.
  ///
  /// \sa size(), numUndos, and currentPosition()
  ///
  /// \return the number of undos available
  int numRedos() const;

 private:
  typedef std::vector<CommandBase*> ListType;

  ListType m_history;  ///< Undo/redo history
  ListType::iterator m_histit;///< histoty iterator
}; // class CommandHistory

}  // namespace sedeen

#endif

