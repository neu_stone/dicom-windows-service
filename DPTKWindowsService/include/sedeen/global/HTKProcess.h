#ifndef SEDEEN_SRC_GLOBAL_HTKPROCESS_H
#define SEDEEN_SRC_GLOBAL_HTKPROCESS_H

#include <QtCore>

#include "global/DllApi.h"

namespace sedeen {

/// Class for creating, and montoring, HTK instance.
//
/// Create and runs HTK as a child process of caller. Can launch, monitor
/// output, and termintate an HTK process with a given filename.
///
/// Must use the launch(), if monitorin HTK is required.
class SEDEEN_GLOBAL_API HTKProcess : public QProcess {
  Q_OBJECT;

 public:
  /// HTK view modes
  enum ViewMode {
    CubeView,       ///< displays the image in cube mode with x/y/z planes
    RenderView      ///< displays the image in a 3D rendering mode
  };

  /// Constructor.
  //
  /// \param parent
  /// The widget's parent
  HTKProcess(QObject *parent = 0);

  virtual ~HTKProcess();

  /// launches an HTK instance with the given filename.
  //
  /// If a previous HTK instance is open, it will be closed. Once launched,
  /// HTK's output is monitored for user selected coodinates relative to the
  /// stack. These coordinates are emitted with coordinateSelected() signal.
  ///
  /// Should check the start() signal, to make sure process was started.
  ///
  /// \param filename
  /// The file to open with HTK
  ///
  /// \param view
  /// The view mode for this file
  ///
  /// \return
  /// \c true if launch is attempted, \c false otherwise
  bool launch(const QString &filename, ViewMode view);

  /// Gets the view mode used to launch HTK.
  //
  /// The view mode can be changed by the user from within HTK. So this result
  /// may not be the actual view mode.
  ///
  /// Does not check to see if an HTK instance is currently running.
  ///
  /// \return
  /// The view mode currently selected
  ViewMode viewMode() const;

  /// Gets the filename that HTK was launched with.
  //
  /// \return
  /// The filename
  const QString& filename() const;

 signals:
  /// Signal emitted when HTK outputs a user selected coordiante.
  //
  /// Upon user request, HTK will output an (x,y,z) coordinate relative to the
  /// current stack it is displaying. This user selected coordinate will be
  /// emitted when HTK genereates it.
  ///
  /// \param x
  /// The x-coordinate
  ///
  /// \param y
  /// The y-coordinate
  ///
  /// \param z
  /// The z-coordinate
  void coordinateSelected(double x, double y, double z);

 protected slots:
  /// Handles the file monitor's fileChanged() event.
  //
  /// \param path
  /// The file changed
  void fileChanged(const QString &path);

  void childEvent(QChildEvent *event);

 private:
  /// The file system monitor
  QFileSystemWatcher m_monitor;

  /// The filename launched
  QString m_filename;

  /// The view mode launched
  ViewMode m_viewmode;

  /// Tempfile for output
  QTemporaryFile m_tempfile;
};

} // namespace sedeen

#endif
