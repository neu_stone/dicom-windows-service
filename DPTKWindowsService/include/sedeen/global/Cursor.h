#ifndef SEDEEN_SRC_GLOBAL_CURSOR_H
#define SEDEEN_SRC_GLOBAL_CURSOR_H

#include <QtGui>

#include "global/DllApi.h"

namespace sedeen {
/// Special cursors
namespace cursor {

/// Get a left-arrow cursor.
SEDEEN_GLOBAL_API
QCursor leftArrow();

/// Get a right-arrow cursor.
SEDEEN_GLOBAL_API
QCursor rightArrow();

/// Get an up-arrow cursor.
SEDEEN_GLOBAL_API
QCursor upArrow();

/// Get a down-arrow cursor.
SEDEEN_GLOBAL_API
QCursor downArrow();

} // namespace cursor
} // namespace sedeen

#endif

