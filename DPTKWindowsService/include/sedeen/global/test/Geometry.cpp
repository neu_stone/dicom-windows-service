#include "global/Geometry.h"

#ifndef _WIN32
#include <tr1/tuple>
#endif
#include <vector>
#include "gtest/gtest.h"

namespace sedeen {
namespace geometry {
namespace {

class GeometryTestPair
    : public ::testing::TestWithParam<std::tr1::tuple<QPointF,QPointF> > {
  virtual void SetUp() {
    p1 = std::tr1::get<0>(GetParam());
    p2 = std::tr1::get<1>(GetParam());
  }
 protected:
  QPointF p1;
  QPointF p2;
};

class GeometryTestSingle
    : public ::testing::TestWithParam<QPointF> {
 protected:
  virtual void SetUp() {
    p1 = GetParam();
  }
  QPointF p1;
};

double myL2Norm(const QPointF& p1, const QPointF& p2) {
  double dx = p2.x() - p1.x();
  double dy = p2.y() - p1.y();
  return std::sqrt(dx*dx + dy*dy);
}

} // namespace

TEST_P(GeometryTestPair, L2Norm) {
  EXPECT_FLOAT_EQ(myL2Norm(p1, p2), l2Norm(p1, p2));
}

TEST_P(GeometryTestPair, L2NormSymmetry) {
  EXPECT_FLOAT_EQ(l2Norm(p1, p2), l2Norm(p2, p1));
}

TEST_P(GeometryTestSingle, Magnitude) {
  EXPECT_FLOAT_EQ(myL2Norm(QPointF(0,0), p1), magnitude(p1));
}

TEST_P(GeometryTestPair, LinceCenterSymmetry) {
  QPointF c1 = lineCenter(p1, p2);
  QPointF c2 = lineCenter(p2, p1);
  EXPECT_FLOAT_EQ(c1.x(), c2.x());
  EXPECT_FLOAT_EQ(c1.y(), c2.y());
}

namespace {

std::vector<QPointF> testPoints() {
  std::vector<QPointF> points;
  points.reserve(4);
  points.push_back(QPointF(0, 0));
  points.push_back(QPointF(-2, -5));
  points.push_back(QPointF(10, 0));
  return points;
}

} // namespace

INSTANTIATE_TEST_CASE_P(GeometryPoints, GeometryTestPair,
                        ::testing::Combine(::testing::ValuesIn(testPoints()),
                                           ::testing::ValuesIn(testPoints())));

INSTANTIATE_TEST_CASE_P(GeometryPoints, GeometryTestSingle,
                        ::testing::ValuesIn(testPoints()));

TEST(Geometry, Nan) {
  EXPECT_FALSE(my_isnan(33));
  EXPECT_TRUE(my_isnan(std::numeric_limits<double>::quiet_NaN()));
}

TEST(Geometry, Infinity) {
  EXPECT_FALSE(isinf(1));
  EXPECT_TRUE(isinf(std::numeric_limits<double>::infinity()));
  EXPECT_TRUE(isinf(-std::numeric_limits<double>::infinity()));
}

} // namespace geometry
} // namespace sedeen
