#include "global/MathOps.h"

#include <gtest/gtest.h>

namespace sedeen {
namespace math_ops {

TEST(MathOps, IntDiv) {
  EXPECT_DOUBLE_EQ(5.0 / 2.0, intdiv(5,2));
  EXPECT_DOUBLE_EQ(100.0 / 1.0, intdiv(100,1));
}

TEST(MathOps, RoundPositive) {
  EXPECT_DOUBLE_EQ(7, round(6.7));
  EXPECT_DOUBLE_EQ(6, round(6.3));
  EXPECT_DOUBLE_EQ(7, round(6.5));
}

TEST(MathOps, RoundNegative) {
  EXPECT_DOUBLE_EQ(-4, round(-4.2));
  EXPECT_DOUBLE_EQ(-4, round(-3.9));
  EXPECT_DOUBLE_EQ(-3, round(-3.5));
}

TEST(MathOps, RoundToPositive) {
  EXPECT_DOUBLE_EQ(5.2, roundTo(5.249909, 1));
  EXPECT_DOUBLE_EQ(5.5, roundTo(5.5000, 1));
  EXPECT_DOUBLE_EQ(5.56, roundTo(5.5555, 2));
  EXPECT_DOUBLE_EQ(6, roundTo(5.5555, 0));
  EXPECT_DOUBLE_EQ(0, roundTo(0.0068799, 1));
}

TEST(MathOps, RoundToNegative) {
  EXPECT_DOUBLE_EQ(-5.5, roundTo(-5.521, 1));
  EXPECT_DOUBLE_EQ(-5.6, roundTo(-5.58, 1));
  EXPECT_DOUBLE_EQ(0, roundTo(-0.0055, 1));
}

TEST(MathOps, Log2Positive) {
  EXPECT_DOUBLE_EQ(0, log2(1));
  EXPECT_DOUBLE_EQ(1, log2(2));
  EXPECT_DOUBLE_EQ(2, log2(4));
  EXPECT_DOUBLE_EQ(4.55, log2(std::pow(2, 4.55)));
}

TEST(MathOps, Log2Negative) {
  EXPECT_DOUBLE_EQ(-1, log2(1.0/2.0));
  EXPECT_DOUBLE_EQ(-3, log2(1.0/8.0));
  EXPECT_DOUBLE_EQ(-0.234, log2(std::pow(2, -0.234)));
}

TEST(MathOps, LogNPositive) {
  EXPECT_DOUBLE_EQ(1, logn(99,99));
  EXPECT_DOUBLE_EQ(3, logn(8, 2));
  EXPECT_DOUBLE_EQ(0.124, logn(std::pow(5, 0.124), 5));
}

TEST(MathOps, LogNNegative) {
  EXPECT_DOUBLE_EQ(-2, logn(std::pow(38.0, -2), 38));
  EXPECT_DOUBLE_EQ(-3, logn(1.0 / 125.0, 5));
}

TEST(MathOps, MetricToCompactSmallBase) {
  double number = 5e-5;
  MetricUnits post_unit = Base;
  double compact = metricToCompact(number, post_unit);

  EXPECT_DOUBLE_EQ(50, compact);
  EXPECT_EQ(Micro, post_unit);
}

TEST(MathOps, MetricToCompactLargeBase) {
  double number = 14000;
  MetricUnits post_unit = Base;
  double compact = metricToCompact(number, post_unit);

  EXPECT_DOUBLE_EQ(14, compact);
  EXPECT_EQ(Kilo, post_unit);
}

TEST(MathOps, MetricToCompactSmall) {
  double number = 4e-9;
  MetricUnits before = Kilo;
  MetricUnits after = Base;
  double compact = metricToCompact(number, after, before);

  EXPECT_DOUBLE_EQ(4, compact);
  EXPECT_EQ(Micro, after);
}

TEST(MathOps, MetricToCompactLarge) {
  double number = 1e2;
  MetricUnits before = Kilo;
  MetricUnits after = Base;
  double compact = metricToCompact(number, after, before);

  EXPECT_DOUBLE_EQ(compact, 100);
  EXPECT_EQ(after, Kilo);
}

TEST(MathOps, FromMetricToString) {
  EXPECT_EQ('f', fromMetricToString(Femto));
  EXPECT_EQ('p', fromMetricToString(Pico));
  EXPECT_EQ('n', fromMetricToString(Nano));
  EXPECT_EQ('u', fromMetricToString(Micro));
  EXPECT_EQ('m', fromMetricToString(Milli));
  EXPECT_EQ(' ', fromMetricToString(Base));
  EXPECT_EQ('k', fromMetricToString(Kilo));
  EXPECT_EQ('M', fromMetricToString(Mega));
  EXPECT_EQ('G', fromMetricToString(Giga));
  EXPECT_EQ('T', fromMetricToString(Tera));
}

TEST(MathOps, MetricToMetric) {
  // 4 kg == 4 x 10^12 ng
  EXPECT_DOUBLE_EQ(4e12, metricToMetric(4, Kilo, Nano));
  // 22 mg == 22 x 10-9 Mg
  EXPECT_DOUBLE_EQ(22e-9, metricToMetric(22, Milli, Mega));
}

TEST(MathOps, IntDivideRoundingUp) {
  EXPECT_EQ(3, intDivideRoundUp(14, 6));
  EXPECT_EQ(1, intDivideRoundUp(20, 500));
  EXPECT_EQ(-2, intDivideRoundUp(5, -2));
  EXPECT_EQ(-5, intDivideRoundUp(-33, 6));
}

TEST(MathOps, IntDivideRoundDown) {
  EXPECT_EQ(2, intDivideRoundDown(14, 6));
  EXPECT_EQ(0, intDivideRoundDown(20, 500));
  EXPECT_EQ(-3, intDivideRoundDown(5, -2));
  EXPECT_EQ(-6, intDivideRoundDown(-33, 6));
}

} // namespace math_ops
} // namespace sedeen
