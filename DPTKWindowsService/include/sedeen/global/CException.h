#ifndef SEDEEN_SRC_GLOBAL_CEXCEPTION_H
#define SEDEEN_SRC_GLOBAL_CEXCEPTION_H

#ifdef WIN32

#include <eh.h>
#include <windows.h>

#include "global/DllApi.h"

namespace sedeen {
/// C++ Exception Object
//
/// Recored all of the paramters of the structured exception object thrown by
/// C-code
///
/// \note Must call
/// \code
/// _set_se_translator( trans_func );
/// \endcode
/// to register the C-exception handler in C++ code
///
/// <b> Example usage: </b>
/// \code
/// // exceptions_Exception_Handling_Differences3.cpp
/// // compile with: /EHa
/// #include <stdio.h>
/// #include <eh.h>
/// #include <windows.h>
///
/// class SE_Exception {
///  private:
///   SE_Exception() {}
///   unsigned int nSE;
///
///  public:
///   SE_Exception( SE_Exception& e) : nSE(e.nSE) {}
///   SE_Exception(unsigned int n) : nSE(n) {}
///   ~SE_Exception() {}
///   unsigned int getSeNumber() { return nSE; }
/// };
///
/// void SEFunc() {
///   __try {
///     int x, y = 0;
///     x = 5 / y;
///   }
///   __finally {
///     printf_s( "In finally\n" );
///   }
/// }
///
/// void trans_func( unsigned int u, _EXCEPTION_POINTERS* pExp ) {
///   printf_s( "In trans_func.\n" );
///   throw SE_Exception( u );
/// }
///
/// int main() {
///   _set_se_translator( trans_func );
///   try {
///     SEFunc();
///   }
///   catch( SE_Exception e ) {
///     printf_s( "Caught a __try exception with SE_Exception.\n" );
///     printf_s( "nSE = 0x%x\n", e.getSeNumber() );
///   }
/// }
/// \endcode
///
/// <b> Exception codes: </b>
/// \li \c EXCEPTION_ACCESS_VIOLATION
///         The thread tried to read from or write to a virtual address for
///         which it does not have the appropriate access.
/// \li \c EXCEPTION_ARRAY_BOUNDS_EXCEEDED
///         The thread tried to access an array element that is out of bounds
///         and the underlying hardware supports bounds checking.
/// \li \c EXCEPTION_BREAKPOINT
///         A breakpoint was encountered.
/// \li \c EXCEPTION_DATATYPE_MISALIGNMENT
///         The thread tried to read or write data that is misaligned on
///         hardware that does not provide alignment. For example, 16-bit values
///         must be aligned on 2-byte boundaries, 32-bit values on 4-byte
///         boundaries, and so on.
/// \li \c EXCEPTION_FLT_DENORMAL_OPERAND
///         One of the operands in a floating-point operation is denormal. A
///         denormal value is one that is too small to represent as a standard
///         floating-point value.
/// \li \c EXCEPTION_FLT_DIVIDE_BY_ZERO
///         The thread tried to divide a floating-point value by a
///         floating-point divisor of zero.
/// \li \c EXCEPTION_FLT_INEXACT_RESULT
///         The result of a floating-point operation cannot be represented
///         exactly as a decimal fraction.
/// \li \c EXCEPTION_FLT_INVALID_OPERATION
///         This exception represents any floating-point exception not included
///         in this list.
/// \li \c EXCEPTION_FLT_OVERFLOW
///         The exponent of a floating-point operation is greater than the
///         magnitude allowed by the corresponding type.
/// \li \c EXCEPTION_FLT_STACK_CHECK
///         The stack overflowed or underflowed as the result of a
///         floating-point operation.
/// \li \c EXCEPTION_FLT_UNDERFLOW
///         The exponent of a floating-point operation is less than the
///         magnitude allowed by the corresponding type.
/// \li \c EXCEPTION_ILLEGAL_INSTRUCTION
///         The thread tried to execute an invalid instruction.
/// \li \c EXCEPTION_IN_PAGE_ERROR
///         The thread tried to access a page that was not present, and the
///         system was unable to load the page. For example, this exception
///         might occur if a network connection is lost while running a program
///         over the network.
/// \li \c EXCEPTION_INT_DIVIDE_BY_ZERO
///         The thread tried to divide an integer value by an integer divisor of
///         zero.
/// \li \c EXCEPTION_INT_OVERFLOW
///         The result of an integer operation caused a carry out of the most
///         significant bit of the result.
/// \li \c EXCEPTION_INVALID_DISPOSITION
///         An exception handler returned an invalid disposition to the
///         exception dispatcher. Programmers using a high-level language such
///         as C should never encounter this exception.
/// \li \c EXCEPTION_NONCONTINUABLE_EXCEPTION
///         The thread tried to continue execution after a noncontinuable
///         exception occurred.
/// \li \c EXCEPTION_PRIV_INSTRUCTION
///         The thread tried to execute an instruction whose operation is not
///         allowed in the current machine mode.
/// \li \c EXCEPTION_SINGLE_STEP
///         A trace trap or other single-instruction mechanism signaled that one
///         instruction has been executed.
/// \li \c EXCEPTION_STACK_OVERFLOW
///         The thread used up its stack.
class SEDEEN_GLOBAL_API SE_Exception {
 public:
  /// Exception code, see below
  DWORD ExceptionCode;

  /// Address where the exception occurred
  PVOID ExceptionAddress;

  /// Number of parameters in the ExceptionInformation array.
  DWORD NumberParameters;

  /// This member can be either zero, indicating a continuable exception, or
  /// EXCEPTION_NONCONTINUABLE indicating a noncontinuable exception. Any
  /// attempt to continue execution after a noncontinuable exception causes the
  /// EXCEPTION_NONCONTINUABLE_EXCEPTION exception.
  DWORD ExceptionFlags;

  /// ONLY valid for: EXCEPTION_ACCESS_VIOLATION The first element of the array
  /// contains a read-write flag that indicates the type of operation that
  /// caused the access violation. If this value is zero, the thread attempted
  /// to read the inaccessible data. If this value is 1, the thread attempted to
  /// write to an inaccessible address. The second array element specifies the
  /// virtual address of the inaccessible data.
  ULONG_PTR ExceptionInformation[EXCEPTION_MAXIMUM_PARAMETERS];

  ~SE_Exception() {}

  SE_Exception(SE_Exception& e) {
    ExceptionCode  = e.ExceptionCode;
    ExceptionFlags  = e.ExceptionFlags;
    ExceptionAddress = e.ExceptionAddress;
    NumberParameters = e.NumberParameters;
    for (DWORD i=0; i<NumberParameters; i++)
      ExceptionInformation[i] = e.ExceptionInformation[i];
  }

  SE_Exception(_EXCEPTION_POINTERS* pExp) {
    ExceptionCode  = pExp->ExceptionRecord->ExceptionCode;
    ExceptionFlags  = pExp->ExceptionRecord->ExceptionFlags;
    ExceptionAddress = pExp->ExceptionRecord->ExceptionAddress;
    NumberParameters = pExp->ExceptionRecord->NumberParameters;
    for (DWORD i=0; i<NumberParameters; i++)
      ExceptionInformation[i] = pExp->ExceptionRecord->ExceptionInformation[i];
  }
}; // class SE_Exception

/// C Exception handler
SEDEEN_GLOBAL_API
void trans_func(unsigned int u, _EXCEPTION_POINTERS* pExp);

} // namespace

#endif // ifdef win32
#endif // ifdef
