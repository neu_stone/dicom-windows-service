#ifndef SEDEEN_SRC_GLOBAL_DEBUG_H
#define SEDEEN_SRC_GLOBAL_DEBUG_H

#include "global/DllApi.h"

namespace sedeen{
namespace debug {

SEDEEN_GLOBAL_API
void message(const char *message, const char *fileName = 0,
             int lineNumber = -1);

SEDEEN_GLOBAL_API
void warning(const char *message, const char *fileName = 0,
             int lineNumber = -1);

SEDEEN_GLOBAL_API
void error(const char *message, const char *fileName = 0, int lineNumber = -1);

SEDEEN_GLOBAL_API
void warning_x(bool condition, const char *message, const char *fileName = 0,
               int lineNumber = -1);

SEDEEN_GLOBAL_API
void assert_x(bool condition, const char *message, const char *fileName = 0,
              int lineNumber = -1);

} //namespace sedeen
} //namespace debug


#endif // sedeen_src_global_Debug_h


