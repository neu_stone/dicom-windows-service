#include "global/CException.h"

#ifdef WIN32

namespace sedeen {

/* C Exception handler */
void trans_func(unsigned int, _EXCEPTION_POINTERS* pExp)  {
  throw SE_Exception(pExp);
}

} // namespace sedeen

#endif
