#ifndef SEDEEN_SRC_GLOBAL_MATHOPS_H
#define SEDEEN_SRC_GLOBAL_MATHOPS_H

/// \file MathOps.h
/// Contains a list of math operations for quick calculations.

#include <algorithm>
#include <cmath>

#include "global/DllApi.h"

namespace sedeen {
namespace math_ops {
/**
 *  Definition of Units
 */
enum MetricUnits {
  Femto  = -15,
  Pico  = -12,
  Nano  = -9,
  Micro  = -6,
  Milli  = -3,
  Base  = +0,
  Kilo  = +3,
  Mega  = +6,
  Giga  = +9,
  Tera  = +12,
};

/**
 *  Divides two integers with double precision (a/b).
 *
 *  @param dividend
 *         The dividend.
 *
 *  @param divisor
 *         The divisor. Must not be zero.
 */
SEDEEN_GLOBAL_API
double intdiv(double dividend, double divisor);

/**
 *  Rounds to the nearest whole number.
 *
 *  Values exactly halfway between integers, both negative and positive, are
 *  rounded towards positive infinity.
 *
 *  @param num
 *         The number to round.
 */
SEDEEN_GLOBAL_API
double round(double num);

/**
 *  Rounds off a floating point number with given precision.
 *
 *  @param number
 *         The number to round.
 *
 *  @param precision
 *          The places to keep after decimal. Must not be negative.
 */
template<typename T>
T roundTo(T number, int precision);

/**
 * Finds the log base 2 of a number.
 *
 * @param num
 *        Must be greater than zero.
 */
SEDEEN_GLOBAL_API
double log2(double num);

/**
 *  Finds the log base N of a number.
 *
 *  @param  num
 *          Number of which to find the logarithm. Must be greater than zero.
 *
 *  @param  base
 *          The base of the target logarithm. May be anything but 0 and 1.
 */
SEDEEN_GLOBAL_API
double logn(double num, unsigned int base);

/**
 *  Converts the given number to a a more compact representation.
 *
 *  Removes all leading zeros (before the decimal) and returns the resulting
 *  metric unit for the conversion. Assumes the input is specified in base
 *  metric unit (i.e. meters, grams, liter).
 *
 *  @param  value
 *          The value in base metric units
 *
 *  @param  after
 *          The resulting unit after removing leading zeros
 *
 *  @param  before
 *          The units of the input value
 */
SEDEEN_GLOBAL_API
double metricToCompact(double value, MetricUnits& after,
                       MetricUnits before = Base);

/**
 *  Returns a single character SI representation of the enum member.
 *
 *  @param  unit
 *
 *  @return the character representation on the unit
 */
SEDEEN_GLOBAL_API
char fromMetricToString(MetricUnits unit);

/**
 *  Converts the value from one scale to another. For example,
 *  this can convert micro to kilo or visa versa.
 *
 *  @param  value
 *          The value to convert
 *
 *  @param  before
 *          The desired scale after conversion (kilo, Micro, etc.)
 *
 *  @param  after
 *          The scale of the input value (default assumes the base metric unit)
 *
 *  @return The value scaled to the units \c after.
 */
SEDEEN_GLOBAL_API
double metricToMetric(double value, MetricUnits before,
                      MetricUnits after = Base);

/**
 *  Divide two integers, rounding towards positive infinity.
 *
 *  @pre
 *  The input type ought to be a signed or unsigned integer of any precision.
 *  The trick it uses has no meaning for floating-point values.
 *
 *  @param dividend
 *         Any integer value.
 *
 *  @param divisor
 *         Must not equal zero.
 */
template<typename T>
T intDivideRoundUp(T dividend, T divisor);

/**
 * Divide two integers, rounding towards negative infinity.
 *
 * @pre
 * The input type ought to be a signed or unsigned integer of any precision.
 * The trick it uses has no meaning for floating-point values.
 *
 * @param dividend
 *        Any integer value.
 *
 * @param divisor
 *        Must not equal zero.
 */
template<typename T>
T intDivideRoundDown(T dividend, T divisor);

/// Error-checking trap to overload the above function templates.
float intDivideRoundUp(float, float);

/// Error-checking trap to overload the above function templates.
float intDivideRoundDown(float, float);

/// Error-checking trap to overload the above function templates.
double intDivideRoundUp(double, double);

/// Error-checking trap to overload the above function templates.
double intDivideRoundDown(double, double);

} // namespace math_ops
} // namespace sedeen

#endif

#include "MathOps.icc"
