// Primary header
#include "global/UnitDefs.h"

namespace sedeen {
const unsigned int BITS_PER_BYTE = 8;

namespace binary_prefix {

// Gibi-
const BinaryPrefix Gi = 1 << 30;

// Mebi-
const BinaryPrefix Mi = 1 << 20;

// Kibi-
const BinaryPrefix Ki = 1 << 10;

} // namespace binary_prefix

namespace binary_size {

// Byte
const BinaryUnit B = 1;

// Kibibyte
const BinaryUnit KiB = binary_prefix::Ki*B;

// Mebibyte
const BinaryUnit MiB = binary_prefix::Mi*B;

// Gibibyte
const BinaryUnit GiB = binary_prefix::Gi*B;

} // namespace binary_size

namespace si_prefix {

// Yotta-
const SiPrefix Y = 1e24;

// Zetta-
const SiPrefix Z = 1e21;

// Exa-
const SiPrefix E = 1e18;

// Peta-
const SiPrefix P = 1e15;

// Tera-
const SiPrefix T = 1e12;

// Giga-
const SiPrefix G = 1e9;

// Meta-
const SiPrefix M = 1e6;

// Kilo-
const SiPrefix k = 1e3;

// Hecto-
const SiPrefix h = 1e2;

// Deca-
const SiPrefix da = 1e1;

// Deci-
const SiPrefix d = 1e-1;

// Centi-
const SiPrefix c = 1e-2;

// Milli-
const SiPrefix m = 1e-3;

// Micro-
const SiPrefix u = 1e-6;

// Nano-
const SiPrefix n = 1e-9;

// Pica-
const SiPrefix p = 1e-12;

// Femto-
const SiPrefix f = 1e-15;

// Atto-
const SiPrefix a = 1e-18;

// Zepto-
const SiPrefix z = 1e-21;

// Yocto-
const SiPrefix y = 1e-24;

} // namespace si_prefix

namespace length {

// Base unit: micron
const SiPrefix BASE_UNIT = si_prefix::u;

const LengthUnit m = 1 / BASE_UNIT;

const LengthUnit Ym = si_prefix::Y*m;

const LengthUnit Zm = si_prefix::Z*m;

const LengthUnit Em = si_prefix::E*m;

const LengthUnit Pm = si_prefix::P*m;

const LengthUnit Tm = si_prefix::T*m;

const LengthUnit Gm = si_prefix::G*m;

const LengthUnit Mm = si_prefix::M*m;

const LengthUnit km = si_prefix::k*m;

const LengthUnit hm = si_prefix::h*m;

const LengthUnit dam = si_prefix::da*m;

const LengthUnit dm = si_prefix::d*m;

const LengthUnit cm = si_prefix::c*m;

const LengthUnit mm = si_prefix::m*m;

const LengthUnit um = si_prefix::u*m;

const LengthUnit nm = si_prefix::n*m;

const LengthUnit pm = si_prefix::p*m;

const LengthUnit fm = si_prefix::f*m;

const LengthUnit am = si_prefix::a*m;

const LengthUnit zm = si_prefix::z*m;

const LengthUnit ym = si_prefix::y*m;

// ==== IMPERIAL DISTANCE MEASURES ====

// Thou, thousandth of an inch
const LengthUnit thou = inch / 1000;

// Inch (in)
const LengthUnit inch = 25.4 * mm;

// Foot (ft)
const LengthUnit foot = 12 * inch;

// Yard
const LengthUnit yard = 3 * foot;

// Chain
const LengthUnit chain = 22 * yard;

// Furlong
const LengthUnit furlong = 10 * chain;

// Mile
const LengthUnit mile = 8 * furlong;

// League
const LengthUnit league = 3 * mile;

} // namespace length

// ==== GLOBAL CONSTANT ====
/// Pi
const double PI = 3.14159265358979323846264338327950288419716939937510582097;

namespace angle {

// Degree
const AngleUnit deg = 1.0;

// Radian
const AngleUnit rad = 360.0 / 2.0 / PI;

// Gradian
const AngleUnit grad = 360.0 / 400.0;

} // namespace angle

namespace {

/// Convert all of the SI-prefixes into a map
const std::map<SiPrefix, std::string> buildMap() {
  using namespace sedeen;

  std::map<SiPrefix, std::string> unitMap;

  unitMap[si_prefix::Y]   = "Y";
  unitMap[si_prefix::Z]   = "Z";
  unitMap[si_prefix::E]   = "E";
  unitMap[si_prefix::P]   = "P";
  unitMap[si_prefix::T]   = "T";
  unitMap[si_prefix::G]   = "G";
  unitMap[si_prefix::M]   = "M";
  unitMap[si_prefix::k]   = "k";
  // unitMap[si_prefix::h]   = "h";
  // unitMap[si_prefix::da]  = "da";
  unitMap[1.0] = ""; // Base unit
  // unitMap[si_prefix::d]   = "d";
  // unitMap[si_prefix::c]   = "c";
  unitMap[si_prefix::m]   = "m";
  unitMap[si_prefix::u]   = "u";
  unitMap[si_prefix::n]   = "n";
  unitMap[si_prefix::p]   = "p";
  unitMap[si_prefix::f]   = "f";
  unitMap[si_prefix::a]   = "a";
  unitMap[si_prefix::z]   = "z";
  unitMap[si_prefix::y]   = "y";

  return unitMap;
}

} // namespace

/// Statically create the map once.
const std::map<SiPrefix, std::string>& unitMap() {
  static const std::map<SiPrefix, std::string> unitMap = buildMap();
  return unitMap;
}

double siValuetoCompact(double value, SiPrefix& outputPrefix){
    std::map<SiPrefix, std::string>::const_reverse_iterator it = unitMap().rbegin();
    double compactValue = 0;
    while( it != unitMap().rend() ) {
        compactValue = value / it->first;
        outputPrefix = it->first;
        if( compactValue > 1 )
            break;
        ++it;
    }
    return compactValue;
}

double metricToCompact(double number, std::string& prefix,
                       LengthUnit inputUnit)
{
  // Move to the SI base-unit
  number = metricToMetric(number, inputUnit, length::m);

  // Get the upper bound
  std::map<SiPrefix, std::string>::const_iterator i =
      unitMap().lower_bound(number);

  // If there's a smaller unit available, take it
  if (unitMap().begin() != i) {
    --i;
  }

  // Divide the number by the unit
  number /= i->first;

  // Copy the string value to the output variable
  prefix = i->second;

  return number;
}

std::string fromMetricToString(SiPrefix scale) {
  std::string prefix;

  std::map<SiPrefix, std::string>::const_iterator i = unitMap().find(scale);

  if (unitMap().end() != i) {
    prefix = i->second;
  } else {
    prefix = "?";
  }

  return prefix;
}

LengthUnit fromStringToMetric(const char length_unit) {
  std::map<SiPrefix, std::string>::const_iterator i = unitMap().begin();
  while (unitMap().end() != i) {
    if (length_unit == i->second[0]) {
      return i->first / length::BASE_UNIT;
    } ++i;
  }
  return 0;
}

double metricToMetric(double number, LengthUnit inputUnit,
                      LengthUnit outputUnit) {
  return number * inputUnit / outputUnit;
}

} // namespace sedeen
