// Main includes
#include "global/Debug.h"

// System includes
#include <iostream>
#include <cassert>

namespace sedeen{
namespace debug {

void warning(const char *mes, const char *fileName, int lineNumber) {
#ifndef NDEBUG
    std::cout << "Warning: ";
    message(mes, fileName, lineNumber);
#endif
}

void error(const char *mes, const char *fileName, int lineNumber) {
#ifndef NDEBUG
    std::cout << "Error: ";
    message(mes, fileName, lineNumber);
#endif
}

void message(const char *message, const char *fileName, int lineNumber) {
#ifndef NDEBUG
    if(fileName) std::cout << fileName;
    if(lineNumber >= 0) std::cout << " (" << lineNumber << ")";
    std::cout << " - " << message << std::endl;
#endif
}

void warning_x(bool condition, const char *message, const char *fileName,
               int lineNumber) {
  if(!condition) warning(message, fileName, lineNumber);
}

void assert_x(bool condition, const char *message, const char *fileName,
              int lineNumber) {
  warning_x(condition, message, fileName, lineNumber);
  assert(condition);
}

} //namespace sedeen
} //namespace debug

