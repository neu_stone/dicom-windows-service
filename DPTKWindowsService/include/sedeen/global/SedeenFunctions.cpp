#include "global/SedeenFunctions.h"

#include <QFileInfo>
#include <QDir>
#include <QMessageBox>
#include <QCoreApplication>

// Declare variable for this file
namespace
{
    QString _saveDir;
    QString _loadDir;
}

bool sedeen::pathExists(const QString path, QWidget *parent)
{
    QDir dir(path);

    if( !dir.exists() )
    {
        int rtn = QMessageBox::question(parent, QObject::tr("Path Error"),
                                                QObject::tr("The output path does not exits. Should I create it?."),
                                                QMessageBox::Yes | QMessageBox::No );
        if( rtn == QMessageBox::Yes )
        {
            if( !dir.mkdir(path) )
            {
                QMessageBox::critical(parent, QObject::tr("Path Error"), QObject::tr("Could not create the path. Check the path and its permissions."));
                return false;
            }
        }
        else
            return false;
    }

    return true;
}

QString sedeen::openDialogImageFilter()
{
    QString str =   QString("All Images") +
                    " (*.bmp; *.gif; *.jpeg; *.jpg; *.jp2; *.j2k; *.jpc; *.ppm; *.pgm; *.png; *.svs, *.tif; *.tiff);;" +
                    "BMP (*.bmp);;" +
                    "GIF (*.gif);;" +
                    "JPEG (*.jpeg; *.jpg);;" +
                    "JPEG-2000 (*.jp2; *.j2k; *.jpc);;" +
                    "PGM/PPM (*.ppm; *.pgm);;" +
                    "PNG (*.png);;" +
                    "SVS (*.svs);;" +
                    "TIFF (*.tif; *.tiff);;" +
                    "All (*.*);;";
    return str;
}

QString sedeen::getOverlayExportName(const QString &filename)
{
	QFileInfo fileinfo(filename);
	QString	fpath = fileinfo.absolutePath() + "/sedeen/" + fileinfo.baseName() + ".roi" ;
	return fpath;
}


QString sedeen::getTransformExportName(const QString &filename)
{
	QFileInfo fileinfo(filename);
	QString	fpath = fileinfo.absolutePath() + "/sedeen/" + fileinfo.baseName() + ".xml";
	return fpath;
}

QString sedeen::getColorListExportName()
{
    return QCoreApplication::applicationDirPath()+"/colorlist";
}

void sedeen::setLoadDir(const QString &dir)
{
    _loadDir = dir;
}

const QString& sedeen::loadDir()
{
    return _loadDir;
}

void sedeen::setSaveDir(const QString &dir)
{
    _saveDir = dir;
}

const QString& sedeen::saveDir()
{
    return _saveDir;
}
