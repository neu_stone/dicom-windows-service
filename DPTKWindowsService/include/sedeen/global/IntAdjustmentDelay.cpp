// Primary header
#include "global/IntAdjustmentDelay.h"

sedeen::global::IntAdjustmentDelay::
IntAdjustmentDelay(int delayMs,
                   QObject* parent)
    : m_delayTimer(new QTimer(this)),
      m_delay(delayMs),
      m_value()
{
    connect(m_delayTimer, SIGNAL(timeout()), this, SLOT(update()));
}

void
sedeen::global::IntAdjustmentDelay::
setValue(int value)
{
    // Save this as the new update-value
    m_value = value;

    // (Re)start the countdown to updating the client
    m_delayTimer->start(m_delay);
}

void
sedeen::global::IntAdjustmentDelay::
update() const
{
    m_delayTimer->stop();
    emit update(m_value);
}
