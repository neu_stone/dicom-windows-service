// Primary header
#include "global/HTKProcess.h"

// User includes
#include "global/SedeenFunctions.h"
#include "global/SrimgFunctions.h"

// STL includes
#include <cstdio>
#include <fstream>
#include <iostream>

namespace sedeen {

HTKProcess::HTKProcess(QObject *parent)
    : QProcess(parent),
      m_viewmode(CubeView) {
  if (!m_tempfile.open()) {
    std::cerr << QString("Could not open temporary file. Cannot receive HTK "
                         "feedback").toStdString();
  }

  // Set up the system watcher
  m_monitor.addPath(m_tempfile.fileName());
  connect(&m_monitor, SIGNAL(fileChanged(const QString&)),
          this, SLOT(fileChanged(const QString&)));
}

HTKProcess::~HTKProcess() {
}

bool HTKProcess::launch(const QString &filename, ViewMode view) {
  // error check
  if (!sedeen::canReadFile(filename))
    return false;

  // Set parameters
  m_filename = filename;
  m_viewmode = view;

  // terminate old process
  if (state() == QProcess::Starting || state() == QProcess::Running)
    kill();

  // create process
#ifdef SEDEEN_USE_HTK
  m_monitor.blockSignals(true);
  m_tempfile.resize(0);
  m_monitor.blockSignals(false);
  start(QString("htk\\htkapp.exe"),
        QStringList() << filename << m_tempfile.fileName());
#else
  std::cerr << "\nCannot launch HTK. Option not supported with this version of "
      "the applications.";
#endif
  // return
  return true;
}

const QString& HTKProcess::filename() const {
  return m_filename;
}

HTKProcess::ViewMode HTKProcess::viewMode() const {
  return m_viewmode;
}

void HTKProcess::fileChanged(const QString &path) {
  std::fstream stream(path.toStdString().c_str(), std::ios_base::in);

  if (stream.is_open()) {
    double x = 0.0;
    stream >> x;
    double y = 0.0;
    stream >> y;
    double z = 0.0;
    stream >> z;
    emit coordinateSelected(x, y, z);
  } else {
    std::cerr << tr("Could not read HTK's output.").toStdString();
  }
}

void HTKProcess::childEvent(QChildEvent *event) {
  QProcess::childEvent(event);
}

} // namespace sedeen
