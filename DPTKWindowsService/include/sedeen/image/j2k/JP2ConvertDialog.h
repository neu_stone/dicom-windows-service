#ifndef SEDEEN_SRC_IMAGE_J2K_JP2CONVERTDIALOG_H
#define SEDEEN_SRC_IMAGE_J2K_JP2CONVERTDIALOG_H

// Qt includes
#include <QtGui>

// System includes
#include <memory>

#include "image/DllApi.h"

namespace Ui {

class dialogConvert;

}

namespace sedeen {
namespace image {

/**
 * JPEG-2000 to Big-TIFF (and visa-versa) Conversion class
 */
class SEDEEN_IMAGE_API JP2ConvertDialog : public QThread {
  Q_OBJECT;
 public:
  /**
   * Constructor
   *
   * @param  parent the parent window
   * @param  filename the name of file to convert
   */
  JP2ConvertDialog(QWidget *parent = 0, QString filename = QString() );

  /**
   * Destructor
   */
  ~JP2ConvertDialog();

  /**
   * Sets the string as the input filename. Also fills the
   * output filename with an appropriate name.
   *
   * @param  filename the name of the file to use as input
   */
  void setInputFilename(QString filename);

  /**
   * Sets the string as the output filename. Enables
   * conversion button if all fields are properly set.
   *
   * @param  filename the name of the file to set as output
   */
  void setOutputFilename(QString filename);

  /**
   * Clears the all fiels of the form
   */
  void clear();

  /**
   * Makes dialog visible and modal (blocks parent window).
   *
   * @return  the return QDialog::DialogCode result
   */
  int  showmodal();

  /**
   * Makes dialog visible and modeless
   */
  void show();

 protected:
  /**
   * Worker thread method.
   */
  void run();

  /**
   * Populates the input/output setting of the form
   *
   * @param  filename the name of file to convert
   * @return  ture on sucess, false otherwise
   */
  bool setFields(QString filename);

 protected slots:
  /**
   * Event handler for input filename browse button .
   */
  void browseForInput();

  /**
   * Event handler for output filename browse button .
   */
  void browseForOutput();

  /**
   * Thread event handler.
   */
  void threadEvent();

  /**
   * Progress bar event handler
   */
  void progress();

  /**
   * Convert button event handler
   */
  void convert();

  /**
   * Stop button event handler
   */
  void stop();

  /**
   * Ouput setting group box event handler
   *
   * @param  val true if toggled on, false otherwise
   */
  void outSettingsToggled(bool val);

  /**
   * Lossy combo box event handler
   */
  void lossyValueChanged(int index);

  /**
   * Close the dialog
   *
   * @return  the return QDialog::DialogCode result
   */
  void close();

 protected:
  std::auto_ptr<Ui::dialogConvert> ui; ///< Qt designer class
  QDialog *dialog; ///< conversion dialog
  QTimer timer;  ///< progress bar timer
  quint32 tilesize; ///< tile size
  quint32 imgw;  ///< image width
  quint32 imgh;  ///< image height
  quint16 imgspp;  ///< image samples per pixel
  quint16 imgbps;  ///< image bits per sample

  bool lossy;  ///< lossy mode
  int code;  ///< coversion return code
  QString lastindir; ///< last search directory for input image
  QString lastoutdir; ///< last search directory for output image
}; // class JP2ConvertDialog

} // namespace image
} // namespace sedeen

#endif

