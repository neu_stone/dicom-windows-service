#ifndef SEDEEN_SRC_IMAGE_J2K_JP2DECODER_H
#define SEDEEN_SRC_IMAGE_J2K_JP2DECODER_H

// Qt includes
#include <QtGui>

// User includes
#include "image/DllApi.h"
#include "image/io/SelectiveDecoder.h"


namespace sedeen {
namespace image {

/// JPEG-2000 decoder.
///
/// Selectively decodes JPEG-2000 images from disk using the pJasPer library 
/// (v2.0.0 or later). See SelectiveDecoder for more details.
class SEDEEN_IMAGE_API JP2Decoder : public SelectiveDecoder {
  Q_OBJECT;

public:

  /// Creates a decoder for the given image.
  ///
  /// On error a runtime_error exception of type FileAccessError
  /// or InvalidImage is thrown. See getRegion().
  ///
  /// \param filename
  /// \param parent
  JP2Decoder(const std::string& filename, QObject * parent = 0);

  /// Closes the image and releases all resources.
  ~JP2Decoder();

  virtual SelectiveDecoderPtr clone() const;

  /// Checks to make sure file is readable. 
  /// The file must be readable, otherwise, return value is FALSE.
  ///
  /// \param filename the name of the file to check
  /// \return TRUE if readable, FALSE otherwise
  static bool canRead(const std::string &filename);

protected:
  virtual RawImage decodeRegion (const DecodeRequest &request);

  //virtual QImage decodeTile (uint32_t lvl, uint32_t tileno);

  //virtual QImage decodeImage (uint32_t lvl);

private:
  // clone if needed to not copy
  JP2Decoder(const JP2Decoder& decoder);
  JP2Decoder& operator=(const JP2Decoder&);

  QMutex      _codecMutex;		///< Mutex for controlling access to the decoder
}; // class JP2Decoder


/// \relates JP2Decoder
/// Decodes the specified region from a JP2/JPC handle
///
/// \param sdec
/// Must be a valid JP2 handle created using pJapser
//
//  \param level
//  the resolution level to extract
//
//  \param error_codes
//  Is set with the pJapser error code on error, unaffected otherwise
//
/// \return
/// An image of the region or a NULL image on failure
RawImage JP2DecodeRegion(void *sdec, unsigned int level, unsigned int x1, 
                    unsigned int y1, unsigned int x2, unsigned int y2,
                    int &error_code);

} // end namespace image
} // end namespace sedeen

#endif // SEDEEN_SRC_IMAGE_J2K_JP2DECODER_H
