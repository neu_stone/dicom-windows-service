// Primary header
#include "image/j2k/JP2BatchConvertDialog.h"

// STL includes
#include <algorithm>
#include <iostream>

// System includes
#include <tiffio.h>

// External module includes
#include "pjasper/include/pjasper.h"

// User headers
#include "global/SedeenFunctions.h"
#include "image/j2k/forms/ui_dialogBatchConverter.h"

namespace sedeen {
namespace image {

JP2BatchConverter::~JP2BatchConverter() {}

JP2BatchConverter::JP2BatchConverter(QWidget *parent, Qt::WindowFlags f)
    : QDialog(parent, f),
      m_ui(new Ui::dialogBatchConverter) {
  /* Setup dialog */
  m_ui->setupUi(this);
  setWindowIcon(QIcon(":/sedeen/icons/convert.png"));

  /* Connect signcals */
  connect(m_ui->buttonBrowseDir, SIGNAL(clicked()),
          this, SLOT(browseDir()));
  connect(m_ui->buttonUseDir, SIGNAL(clicked()),
          this, SLOT(useDir()));
  connect(m_ui->buttonRemoveFiles, SIGNAL(clicked()),
          this, SLOT(removeFiles()));
  connect(m_ui->buttonAddFiles, SIGNAL(clicked()),
          this, SLOT(addFiles()));
  connect(m_ui->buttonClose, SIGNAL(clicked()),
          this, SLOT(accept()));
  connect(m_ui->buttonConvert, SIGNAL(clicked()),
          this, SLOT(convert()));
  connect(m_ui->buttonStop, SIGNAL(clicked()),
          this, SLOT(stop()));
  connect(m_ui->buttonMinimize, SIGNAL(clicked()),
          parent, SLOT(showMinimized()));
  connect(m_ui->comboboxPriority, SIGNAL(currentIndexChanged(int)),
          this, SLOT(priority(int)));

  connect(&m_converter, SIGNAL(progress(int)),
          this, SLOT(progress(int)));
  connect(&m_converter, SIGNAL(warning(QString)),
          this, SLOT(warning(QString)));
  connect(&m_converter, SIGNAL(complete(QString)),
          this, SLOT(complete(QString)));
}


/*
  EVENT HANDELING (SLOTS)
  ----------------------------------------------*/


void JP2BatchConverter::stop() {
  rsa_enc_terminate();
}

void JP2BatchConverter::priority(int index) {
  switch (index) {
    case 2:
      m_converter.setPriority(QThread::HighPriority);
      break;
    case 1:
      m_converter.setPriority(QThread::NormalPriority);
      break;
    case 0:
    default:
      m_converter.setPriority(QThread::LowestPriority); break;
  }
}

void JP2BatchConverter::progress(int percent) {
  double total = (0.01*percent + m_current) /
      m_ui->listwidgetNames->count() * 100;

  m_ui->progressbarFile->setValue(percent);
  m_ui->progressbarTotal->setValue((int)total);
}

void JP2BatchConverter::warning(QString mes) {
  m_ui->plaintextOutput->moveCursor(QTextCursor::End);
  m_ui->plaintextOutput->insertPlainText(mes + QString("\n"));
}

void JP2BatchConverter::complete(QString mes) {
  int cnt = m_ui->listwidgetNames->count();

  m_ui->plaintextOutput->moveCursor(QTextCursor::End);
  m_ui->plaintextOutput->insertPlainText(mes + QString("\n\n"));

  if (++m_current < cnt) {
    convert2();
  } else { // All files are done
    enableDialog(true);
    m_ui->buttonStop->setEnabled(false);
  }
}

void JP2BatchConverter::convert() {
  /* disable form */
  enableDialog(false);
  m_ui->plaintextOutput->clear();
  m_ui->buttonStop->setEnabled(true);
  m_ui->progressbarTotal->setValue(0);
  m_ui->progressbarFile->setValue(0);

  m_current = 0;
  convert2();
}

void JP2BatchConverter::convert2() {
  QString name, outname;
  int  tiles, levels, imgnum, cnt;
  bool lossy, lazy;
  double rate;

  name = m_ui->listwidgetNames->item(m_current)->text();
  outname = generateOutname(name);
  tiles = m_ui->comboboxTilesize->currentText().toULong();
  levels = m_ui->spinboxLevels->value();
  rate = m_ui->spinboxRate->value();
  lazy = m_ui->checkboxLazy->isChecked();
  lossy = m_ui->comboboxTilesize->currentIndex();
  imgnum = tifImageNumber(name);


  /* Update output log */
  cnt = m_ui->listwidgetNames->count();
  m_ui->plaintextOutput->moveCursor(QTextCursor::End);
  m_ui->plaintextOutput->insertPlainText(QString("%1 of %2: %3\n").
                                         arg(m_current+1).arg(cnt).
                                         arg(outname));

  m_converter.setInputFilename(name, imgnum);
  m_converter.setOutputFilename(outname);
  m_converter.setJP2Options(tiles, tiles, levels, lazy, lossy, rate);

  m_converter.convert();

  /* set the priority */
  priority(m_ui->comboboxPriority->currentIndex());
}

void JP2BatchConverter::browseDir() {
  QString dir =
      QFileDialog::getExistingDirectory(this, tr("Select Output Directory"),
                                        saveDir());
  if (!dir.isNull()) {
    setSaveDir(dir);
    m_ui->lineeditOutput->setText(dir);
  }
}

void JP2BatchConverter::useDir() {
  if (m_ui->listwidgetNames->count()) m_ui->lineeditOutput->setText(loadDir());
}

void JP2BatchConverter::addFiles() {
  /* Creat open dialog */
  QStringList names =
      QFileDialog::getOpenFileNames(this, tr("Select file(s)"),
                                    loadDir(), tr("TIFF (*.tif);;") +
                                    tr("SVS (*.svs);;") +
                                    tr("JPEG-2000 (*.jp2 *.jpc *.j2k);;") +
                                    tr("ALL (*.*)") );
  /* Set fields */
  if (names.count()) {
    // Add files
    QFileInfo file(names.at(0));
    m_ui->listwidgetNames->addItems(names);

    if (m_ui->lineeditOutput->text().isEmpty())
      m_ui->lineeditOutput->setText(file.absolutePath());

    // remember load dir
    setLoadDir(file.absolutePath());
  }

  /* Enable convert button */
  if (m_ui->listwidgetNames->count())
    m_ui->buttonConvert->setEnabled(true);
}


void JP2BatchConverter::removeFiles() {
  QListWidgetItem  *item;
  int cnt = m_ui->listwidgetNames->count();

  /* Must loop backwards */
  for (int i = cnt-1; i >= 0; i--) {
    item = m_ui->listwidgetNames->item(i);

    if (item->isSelected()) {
      m_ui->listwidgetNames->takeItem(i);
      delete item;
    }
  }

  /* Disable convert button */
  if (m_ui->listwidgetNames->count() == 0)
    m_ui->buttonConvert->setEnabled(false);
}

void JP2BatchConverter::enableDialog(bool enabled) {
  m_ui->buttonAddFiles->setEnabled(enabled);
  m_ui->buttonRemoveFiles->setEnabled(enabled);
  m_ui->comboboxFormat->setEnabled(enabled);
  m_ui->listwidgetNames->setEnabled(enabled);
  m_ui->groupboxInsettings->setEnabled(enabled);

  m_ui->groupboxOutput->setEnabled(enabled);
  m_ui->groupboxOutsettings->setEnabled(enabled);

  m_ui->buttonConvert->setEnabled(enabled);
  m_ui->buttonClose->setEnabled(enabled);
}


QString JP2BatchConverter::generateOutname(QString name) {
  QString ext;
  QString outname;
  QString path;
  int  i = 1;
  bool rename = m_ui->radioButtonRename->isChecked();

  /* Convert to JPEG-2000 */
  switch (m_ui->comboboxFormat->currentIndex()) {
    case 0:
      ext = ".jp2";
      break;
    case 1:
      ext = ".tif";
      break;
    default:
      Q_ASSERT(false);
  }

  /* Get output path */
  path = m_ui->lineeditOutput->text();
  if (!path.endsWith("/")) path += "/";

  /* Generate the output name */
  QFileInfo file(name);
  outname = path + file.fileName();
  outname.replace( outname.lastIndexOf("."), outname.length(), ext );

  /* Make sure we rename is neccessary */
  name = outname;
  while (QFile::exists(outname) && rename) {
    outname = name;
    outname.replace(name.lastIndexOf("."), 1, QString("_%1.").arg(i++));
  }
  return outname;
}


int JP2BatchConverter::tifImageNumber(QString filename) {
  TIFF *tif = NULL;
  uint32 imgw, imgh;
  std::vector<uint32> area;
  std::vector<uint32>::const_iterator it;
  int num = 0;

  /* if file is not TIFF */
  if (!(tif = TIFFOpen(filename.toAscii().data(), "r"))) return 0;

  for (tdir_t i=0; i<TIFFNumberOfDirectories(tif); i++) {
    TIFFSetDirectory(tif, (tdir_t)i);
    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imgw);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imgh);
    area.push_back(imgw*imgh);
  }
  TIFFClose(tif);

  switch (m_ui->comboboxMultipage->currentIndex()) {
    case 0:
      it  = max_element(area.begin(), area.end());
      num = (int)(it - area.begin());
      break;
    case 1:
    default:
      it  = min_element(area.begin(), area.end());
      num = (int)(it - area.begin());
      break;
  }
  return num;
}

} // namespace image
} // namespace sedeen
