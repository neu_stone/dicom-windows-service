#ifndef SEDEEN_SRC_IMAGE_J2K_JP2PARSER_H
#define SEDEEN_SRC_IMAGE_J2K_JP2PARSER_H

// System headers
#include <vector>
#include <boost/cstdint.hpp>

// User header
#include "image/DllApi.h"
#include "image/io/ImageParser.h"

namespace sedeen {
namespace image {
  /// \class JP2Parser
  /// \brief Looks at a JP2 file and determins its properties. Maintains a handle() and pyramid() to the image.
class SEDEEN_IMAGE_API JP2Parser : public ImageParser {
  public:
    /// \brief Attempts to open and parse \a filename. Throws an InvalidImage or FileAccessError exception on error.
    JP2Parser(const char *filename);

    /// \brief closes the open file and frees resources.
    ~JP2Parser();

    /// \brief Get the JP2 handle to the open file.
    void* handle() const;

  private:
    // do not allow copying
    JP2Parser(const JP2Parser&);
    JP2Parser& operator= (const JP2Parser&);

    // virual functions
    bool doGetHasLabelImage() const;
    bool doGetHasMacroImage() const;
    bool doGetHasThumbnailImage() const;
    std::string doGetFilename() const;
    std::string doGetProducer() const;
    Color doGetColor() const;
    std::string doGetCompression() const;
    std::string doGetDescription() const;
    SizeF doGetPixelSize() const;
    int doGetComponents() const;
    int doGetBpp() const;
    int doGetBpc(const int comp) const;
    double doGetMagnification() const;
    RawImage doGetLabelImage() const;
    RawImage doGetThumbnailImage() const;
    RawImage doGetMacroImage() const;
    ImagePyramidPtr doGetPyramid() const;

  private:
      std::string         _filename;
      void*               _jp2;
      ImagePyramidPtr     _pyramid;
};

}   // namespace image
}   // namespace sedeen
#endif // SEDEEN_SRC_IMAGE_J2K_JP2PARSER_H

