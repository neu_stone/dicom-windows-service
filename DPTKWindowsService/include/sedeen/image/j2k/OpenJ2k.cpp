// Primary header
#include "image/j2k/OpenJ2k.h"

// User headers
#include "image/j2k/JP2Decoder.h"
#include "image/j2k/JP2Parser.h"

using namespace sedeen::image;

OpenJ2k::
~OpenJ2k()
{
}

std::auto_ptr<SelectiveDecoder>
OpenJ2k::
getSelectiveDecoder(const std::string& filename) const
{
    std::auto_ptr<SelectiveDecoder> dec;

    try{
        dec.reset(new JP2Decoder(filename));
    }
    catch( std::exception&)
    {
        dec.reset();
    }

    return dec;
}

QStringList
OpenJ2k::
getFilter() const
{
  QStringList suf;
  suf << "JPEG 2000 (*.jpg)";
  return suf;
}
