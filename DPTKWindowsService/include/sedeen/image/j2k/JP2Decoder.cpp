// Primary header
#include "image/j2k/JP2Decoder.h"

// Boost includes
#include <boost/make_shared.hpp>

// Jasper includes
#include "pjasper/include/pjasper.h"

// User includes
#include "global/geometry/Rect.h"
#include "image/io/DecodeRequest.h"
#include "image/io/Exceptions.h"
#include "image/j2k/JP2Parser.h"
#include "image/j2k/JP2Pyramid.h"

#ifdef WIN32
#include "global/CException.h"
#endif
#include "global/MathOps.h"

namespace sedeen {
namespace image {

namespace {
// Deleter funtion for JP2 codec allocated memory
void DeleteJasperBuffer(void *buffer) {
  rsa_dec_free(buffer);
}

// Map error codes from JP2 coded to SelectiveDecoder types
DecoderErrorType mapToDecoderErrors(const rsa_codes_t code) {
  switch(code) {
    case RSA_ENC_STOP:
    case RSA_ENC_SUCCESS:
    case RSA_DEC_SUCCESS: return NoError;
    case RSA_READ_ERROR:
	  case RSA_WRITE_ERROR:
	  case RSA_ENC_ERROR:
	  case RSA_DEC_ERROR:
	  case RSA_FORMAT_UNSUPPORTED: return DecodeError;
    default:                      assert(false);
    case RSA_DEC_INVALID_DECODER:
	  case RSA_DEC_INVALID_REGION:
	  case RSA_DEC_INVALID_TILE:
    case RSA_DEC_INVALID_RESOLUTION: return RegionError;
    case RSA_INSUFFICIENT_MEMORY: return MemoryError;
  }
}
} // namespace


JP2Decoder::JP2Decoder(const std::string& filename, QObject *parent)
    : SelectiveDecoder(boost::make_shared<JP2Parser>(filename.c_str()),
                       parent) {
}

SelectiveDecoder::SelectiveDecoderPtr JP2Decoder::clone() const {
  SelectiveDecoderPtr ptr;
  try {
    ptr.reset( new JP2Decoder(filename(), parent()) );
  } catch( const std::runtime_error&) {
  }
  return ptr;
}


JP2Decoder::~JP2Decoder() {
  // Cleanup codec
  waitForExit();
}

bool JP2Decoder::canRead(const std::string& filename) {
  return rsa_dec_canread_file(filename.c_str());
}

RawImage JP2Decoder::decodeRegion(const DecodeRequest &request) {
  // Get JP2 handle
  const JP2Parser* jParser
                  = static_cast<const JP2Parser*>(&parser());
  sdec_t* sdec    = (sdec_t*)jParser->handle();

  // Get decode region
  int error_code = RSA_DEC_SUCCESS;
  int32_t level = request.resolution();
  Rect region = request.region(level);

  // Get Region
  QMutexLocker locker(&_codecMutex);
  RawImage image = JP2DecodeRegion(sdec, level, region.x(), region.y(),
                                   xMax(region), yMax(region), error_code);

  // Check for errors
  if (image.isNull()) {
    setLastError(mapToDecoderErrors(static_cast<rsa_codes_t>(error_code)));
  }

  return image;
}

RawImage JP2DecodeRegion(void *sdec, unsigned int level, unsigned int x1,
                    unsigned int y1, unsigned int x2, unsigned int y2,
                    int &error_code ) {
  assert(sdec);
  sdec_t *jp2 = static_cast<sdec_t*>(sdec);

#ifdef WIN32
  // Register the C exception handler
  // must be called on each stack (function) with try block
  _set_se_translator(trans_func);
#endif

  try {
    // Decode at new resolution level equal to  <lvl>
    rsa_codes_t code = rsa_dec_decoderegion(jp2, level, x1, y1, x2, y2);
    if (RSA_DEC_SUCCESS != code) {
      error_code = code;
      return RawImage();
    }

    // Get decoded data to buffer
    int w, h, spp;
    uchar *buf = 0;
    if (!(buf = rsa_dec_getimageRGB(jp2, &w, &h, &spp))) {
      error_code = RSA_INSUFFICIENT_MEMORY;
      return RawImage();
    }
    Color color = (spp>1 ? RGB_8 : Gray_8);

    // Return Image Buffer
    RawDataPtr rawData(static_cast<void*>(buf), DeleteJasperBuffer);
    return RawImage(rawData, Size(w, h), color, Interleaved);
  }
#ifdef WIN32
  catch(SE_Exception e) {
    error_code = RSA_READ_ERROR;
    // format of SE_Exception
    /*
    tr("Caught a __try exception with SE_Exception.\n") +
    tr("Exception Code=0x%1\n").
    arg(e.ExceptionCode, 0, 16) +
    tr("Exception Address=0x%1\n").
    arg((unsigned long)e.ExceptionAddress, 0, 16) +
    tr("Exception Continuable=0x%1 (0==yes)(1==no)\n").
    arg(e.ExceptionFlags, 0, 16) +
    tr("Memory Violation Read(0)/Write(1)=%1\n").
    arg(e.ExceptionInformation[0], 0, 16) +
    tr("Memory Violation Address=0x%1\n").
    arg(e.ExceptionInformation[1], 0, 16));
    */
    return RawImage();
  }
#else
  catch (...) {
    // Just rethrow what we have caught
    throw;
  }
#endif
}


} // namespace image
} // namespace sedeen
