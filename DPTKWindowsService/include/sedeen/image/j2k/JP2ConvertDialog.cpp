#include "JP2ConvertDialog.h"

// System includes
#include <tiffio.h>

// External module includes
#include "pjasper/include/pjasper.h"

// User includes
#include "image/j2k/forms/ui_dialogConvert.h"

namespace sedeen {
namespace image {

JP2ConvertDialog::~JP2ConvertDialog() {
  delete dialog;
}


JP2ConvertDialog::JP2ConvertDialog(QWidget *parent, QString filename)
    : QThread(parent),
      ui(new Ui::dialogConvert) {
  /* Setup dialog */
  dialog = new QDialog(parent);
  ui->setupUi( dialog );
  dialog->setWindowIcon(QIcon(":/sedeen/icons/convert.png"));

  setInputFilename(filename);
  timer.setInterval(1000);

  /* Connect signcals */
  connect(ui->groupboxOutsettings, SIGNAL(toggled(bool)),
          this, SLOT(outSettingsToggled(bool)));
  connect(ui->comboboxLossy, SIGNAL(currentIndexChanged(int)),
          this, SLOT(lossyValueChanged(int)));
  connect(ui->buttonBrowseOutput, SIGNAL(clicked()),
          this, SLOT(browseForOutput()));
  connect(ui->buttonBrowseInput, SIGNAL(clicked()),
          this, SLOT(browseForInput()));
  connect(ui->buttonClose, SIGNAL(clicked()), dialog, SLOT(close()));
  connect(ui->buttonConvert, SIGNAL(clicked()), this, SLOT(convert()));
  connect(ui->buttonStop, SIGNAL(clicked()), this, SLOT(stop()));
  connect(this, SIGNAL(finished()), this, SLOT(threadEvent()));
  connect(this, SIGNAL(started()), this, SLOT(threadEvent()));
  connect(&timer, SIGNAL(timeout()), this, SLOT(progress()));
}

void JP2ConvertDialog::show() {
  dialog->setModal(false);
  dialog->show();
  dialog->raise();
  dialog->activateWindow();
  return;
}

int JP2ConvertDialog::showmodal() {
  return dialog->exec();
}

/**
 * The code to run in the second thread
 *
 **/
void JP2ConvertDialog::run() {
  QByteArray in = ui->lineeditInput->text().toAscii();
  QByteArray out = ui->lineeditOutput->text().toAscii();
  penc_t *enc;

  /* Get encoding options from dialog feilds */
  enc = rsa_enc_create(in.data(), out.data());

  rsa_enc_settilesize(enc, ui->comboboxTilesize->currentText().toULong(),
                      ui->comboboxTilesize->currentText().toULong() );
  rsa_enc_setlossy(enc, ui->comboboxLossy->currentIndex(),
                   ui->spinboxRate->value() );
  rsa_enc_setlevels(enc, ui->spinboxLevels->value() );
  rsa_enc_setimage(enc, ui->comboboxMultipage->currentIndex() );

  if( ui->checkbox_lazy->checkState() == Qt::Checked )
    rsa_enc_setlazy(enc, true);
  else
    rsa_enc_setlazy(enc, false);

  /* Start encoding */
  code = rsa_enc_encode(enc);

  /* Close encoder */
  rsa_enc_destroy(enc);

  return;
}

void JP2ConvertDialog::setOutputFilename(QString filename) {
  /* Check input value */
  if (filename.isEmpty()) {
    return;
  }

  /* Populate lineedit widget */
  if (filename.endsWith(".jp2", Qt::CaseInsensitive) ||
      filename.endsWith(".jpc", Qt::CaseInsensitive) ||
      filename.endsWith(".j2k", Qt::CaseInsensitive)) {
    ui->lineeditOutput->setText(filename);
  }

  /* Enable convert button */
  if (!ui->lineeditInput->text().isEmpty()) {
    ui->buttonConvert->setEnabled(true);
  } else {
    ui->buttonConvert->setEnabled(false);
  }
}

void JP2ConvertDialog::setInputFilename(QString filename) {
  QString append;

  /* Check input value */
  if (filename.isEmpty()) {
    return;
  }

  /* format of output */
  if (filename.endsWith(".jp2") ||
      filename.endsWith(".jpc") ||
      filename.endsWith(".j2k")) {
    append = ".tif";
  } else {
    append = ".jp2";
  }

  /* Populate the fields for this input */
  if (!setFields(filename)) {
    return;
  }
  ui->lineeditInput->setText(filename);

  /* Set output filename */
  filename.resize( filename.lastIndexOf("."));
  filename += append;
  QFile file(filename);
  ui->lineeditOutput->setText(filename);

  /* Enable convert button */
  if (ui->lineeditOutput->text().isEmpty()) {
    ui->buttonConvert->setEnabled(false);
  } else {
    ui->buttonConvert->setEnabled(true);
  }
}

void JP2ConvertDialog::clear() {
  ui->lineEditInfo->setText(tr(""));
  ui->lineeditInput->setText(tr(""));
  ui->lineeditOutput->setText(tr(""));
  ui->progressBar->setValue(0);
  ui->comboboxLossy->setCurrentIndex(0);
  ui->comboboxTilesize->setCurrentIndex(0);
  ui->comboboxMultipage->clear();
}

bool JP2ConvertDialog::setFields(QString filename) {
  QImageReader reader(filename);
  TIFF *tif = NULL;
  sdec_t *dec;
  QImage img;

  /* Set input image parameters */
  if (filename.endsWith(".tif", Qt::CaseInsensitive) ||
      filename.endsWith(".svs", Qt::CaseInsensitive)) {

    if (!(tif = TIFFOpen( filename.toAscii().data(), "r"))) {
      QMessageBox::information(dialog, tr("Error"),
                               tr("Cannot read this image."));
      return false;
    }

    for (tdir_t i=0; i<TIFFNumberOfDirectories(tif); i++) {
      TIFFSetDirectory(tif, (tdir_t)i);
      TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imgw);
      TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imgh);
      TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &imgspp);
      TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &imgbps);

      /* set image information */
      ui->comboboxMultipage->addItem( tr("%1 x %2").arg(imgw).arg(imgh));
    }
    TIFFClose(tif);
  } else if (filename.endsWith(".jp2", Qt::CaseInsensitive) || // Set input
             filename.endsWith(".j2k", Qt::CaseInsensitive) || // image param-
             filename.endsWith(".jpc", Qt::CaseInsensitive)) { // meters
    if (!(dec = rsa_dec_open_file(filename.toAscii().data()))) {
      QMessageBox::information(dialog, tr("Error"),
                               tr("Cannot read this image."));
      return false;
    }
    imgw = rsa_dec_imagewidth(dec);
    imgh = rsa_dec_imageheight(dec);
    imgspp = rsa_dec_numcomps(dec);
    imgbps = rsa_dec_compprec(dec, 0);

    /* set image information */
    ui->comboboxMultipage->addItem( tr("%1 x %2").arg(imgw).arg(imgh));
  } else if (filename.endsWith(".jpg", Qt::CaseInsensitive) ||
             filename.endsWith(".bmp", Qt::CaseInsensitive)) {
    img = reader.read();
    if (img.isNull()) {
      QMessageBox::information(dialog, tr("Error"),
                               tr("Cannot read this image."));
      return false;
    }

    imgw = img.width();
    imgh = img.height();
    imgbps = 8;
    imgspp = (img.isGrayscale() ? 1 : 3);

    /* set image information */
    ui->comboboxMultipage->addItem( tr("%1 x %2").arg(imgw).arg(imgh));
  } else {
    QMessageBox::information(dialog, tr("Error"),
                             tr("Unsupported format."));
    return false;
  }
  return true;
}

/*
  EVENT HANDELING (SLOTS)
  ----------------------------------------------*/
void JP2ConvertDialog::outSettingsToggled(bool val) {
  ui->spinboxLevels->setEnabled(val);
  ui->comboboxTilesize->setEnabled(val);
  ui->comboboxLossy->setEnabled(val);

  if (ui->comboboxLossy->currentIndex())
    ui->spinboxRate->setEnabled(val);
  else
    ui->spinboxRate->setEnabled(false);
}

void JP2ConvertDialog::lossyValueChanged(int index) {
  if (index == 0) {
    ui->spinboxRate->setDisabled(true);
  } else {
    ui->spinboxRate->setDisabled(false);
  }
}

void JP2ConvertDialog::progress() {
  double ntiles(rsa_enc_numtiles());
  double enctiles(rsa_enc_numenctiles());

  if (ntiles > 0)
    ui->progressBar->setValue(int(enctiles/ntiles*100));
}

void JP2ConvertDialog::close() {
  dialog->accept();
}

void JP2ConvertDialog::stop() {
  rsa_enc_terminate();
}

void JP2ConvertDialog::convert() {
  QString inname(ui->lineeditInput->text().trimmed());
  QString outname(ui->lineeditOutput->text().trimmed());
  QFile infile(inname);
  QFile outfile(outname);

  /* Make sure user input is ok */
  if (!infile.exists()) {
    QMessageBox::information(dialog, tr("Error"),
                             tr("Input file does not exist.") );
    return;
  } else if (inname.compare(outname, Qt::CaseInsensitive) == 0) {
    QMessageBox::information(dialog, tr("Error"), tr("Input and output files have the same name (cannot overwrite the input)"));
    return;
  } else if (outfile.exists()) {
    int rtn = QMessageBox::question(dialog, tr("Warning"),tr("Do you want to overwrite the existing file?"), QMessageBox::Yes | QMessageBox::No);
    if (rtn == QMessageBox::No) {
      return;
    }
  }
  start();
}

void JP2ConvertDialog::threadEvent() {
  if (isRunning()) {
    ui->buttonStop->setEnabled(true);
    ui->buttonClose->setEnabled(false);
    ui->buttonConvert->setEnabled(false);
    ui->groupboxInput->setEnabled(false);
    ui->groupboxOutput->setEnabled(false);
    ui->groupboxInsettings->setEnabled(false);
    ui->groupboxOutsettings->setEnabled(false);
    ui->progressBar->setValue(0);
    ui->lineEditInfo->setText(tr("Conversion in progress ... "));
    timer.start();

    /* warn about delays */
    QString dims = ui->comboboxMultipage->itemText(ui->comboboxMultipage->
                                                   currentIndex());
    QStringList dim = dims.split("x");
    long size = dim[0].toLong() * dim[1].toLong() * 3 / 1024 / 1000;
    if (size > 100) {
      ui->lineEditInfo->setText( ui->lineEditInfo->text() +
                                 tr("wait several minutes"));
    }
  } else {
    switch (code) {
      case RSA_ENC_SUCCESS:
        ui->progressBar->setValue( 100 );
        ui->lineEditInfo->setText(tr("Conversion successfuly completed."));
        break;
      case RSA_ENC_STOP:
        ui->lineEditInfo->setText(tr("Conversion stopped"));
        QMessageBox::information(dialog, tr("Error"),
                                 tr("Conversion stopped.") );
        break;
      case RSA_ENC_ERROR:
        ui->lineEditInfo->setText(tr("Conversion was not successful"));
        QMessageBox::information(dialog, tr("Error"),
                                 tr("Conversion was not successful.\n") +
                                 tr("Supported formats are TIFF, JPG, BMP, PNM, RAS, PGX, and MIF.") );
        break;
      default:
        ui->lineEditInfo->setText(tr("Conversion was not successful"));
        QMessageBox::information(dialog, tr("Error"),
                                 tr("Conversion error. Check filenames.\n") +
                                 tr("Supported formats are TIFF, JPG, BMP, PNM, RAS, PGX, and MIF.") );
    }
    ui->buttonStop->setEnabled(false);
    ui->buttonClose->setEnabled(true);
    ui->buttonConvert->setEnabled(true);
    ui->groupboxInput->setEnabled(true);
    ui->groupboxOutput->setEnabled(true);
    ui->groupboxInsettings->setEnabled(true);
    ui->groupboxOutsettings->setEnabled(true);
    timer.stop();
  }
}

void JP2ConvertDialog::browseForInput() {
  QDir dir(".");
  if (lastindir.isEmpty()) {
    lastindir = QDir::currentPath();
  }

  /* Creat open dialog */
  QString filename =
      QFileDialog::getOpenFileName(dialog, tr("Select file to convert"),
                                   lastindir);
  lastindir = dir.filePath(filename);

  /* Set fields */
  setInputFilename(filename);
}

void JP2ConvertDialog::browseForOutput() {
  QDir dir(".");
  if (lastoutdir.isEmpty()) {
    lastoutdir = QDir::currentPath();
  }

  /* Creat open dialog */
  QString filename =
      QFileDialog::getSaveFileName(dialog,
                                   tr("Save As"), lastoutdir,
                                   tr("Image Files (*.jp2 *.jpc *.j2k)"));
  lastoutdir = dir.filePath(filename);

  /* Set fields */
  setOutputFilename( filename );
}

} // namespace image
} // namespace sedeen
