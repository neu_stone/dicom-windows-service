//main header
#include "image/j2k/JP2Pyramid.h"

// system includes
#include <cassert>
#include <vector>
#include <cmath>
#include <boost/make_shared.hpp>

#include "global/MathOps.h"

namespace sedeen
{
namespace image
{

namespace {
    std::vector<Size> jp2ImageDims(const Size & imageSize, const int32_t levels)
    {
        std::vector<Size> dims;
        for( int32_t i = 0; i <levels; i++ )
            dims.push_back( Size( (int32_t) std::ceil( (double)imageSize.width() / (1<<i) ),
                                  (int32_t) std::ceil( (double)imageSize.height() / (1<<i) )) );
        return dims;
    }

    std::vector<Size> jp2TileOffsets(const Size & tileOffset, const int32_t levels)
    {
        std::vector<Size> dims;
        for( int32_t i = 0; i <levels; i++ )
            dims.push_back( Size( (int32_t) std::floor( (double)tileOffset.width() / (1<<i) ),
                                  (int32_t) std::floor( (double)tileOffset.height() / (1<<i) )) );
        return dims;
    }
}

JP2Pyramid::JP2Pyramid(const Size & imageSize,
                       const Size & tileSize,
                       int32_t levels,
                       const Size &tileOffset
                       ) :
ImagePyramid(   jp2ImageDims(imageSize, levels),
                tileSize,
                jp2TileOffsets(tileOffset, levels))
{
    assert( levels > 0 );
    assert( tileSize.width() <= imageSize.width() );
    assert( tileSize.height() <= imageSize.height() );
}

JP2Pyramid::~JP2Pyramid()
{
}

ImagePyramidPtr JP2Pyramid::doGetClone() const
{ return boost::make_shared<JP2Pyramid>(size(0), tileSize(), levels(), tileOffset(0));
}

int32_t JP2Pyramid::doGetLevel(const double scale) const {
  using namespace std;
  if (scale <= 0.5)
    return min(static_cast<int32_t>(floor(-math_ops::log2(scale))), levels()-1);
  else
    return 0;
}


}   // namespace sedeen
}   // namespace imagePyramid.h"


