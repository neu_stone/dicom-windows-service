#include "image/j2k/JP2Convert.h"

// System includes
#include <tiffio.h>

#include "pjasper/include/pjasper.h"

namespace sedeen {
namespace image {

JP2Convert::~JP2Convert() {}

JP2Convert::JP2Convert(QWidget *parent, QString filename) : QThread(parent) {
  setInputFilename(filename);
  m_timer.setInterval(1000);

  /* Connect signcals */
  connect(this, SIGNAL(finished()), this, SLOT(threadEvent()));
  connect(this, SIGNAL(started()), this, SLOT(threadEvent()));
  connect(&m_timer, SIGNAL(timeout()), this, SLOT(timeout()));
}

void JP2Convert::setInputFilename(QString filename, int number) {
  m_inname = filename;
  m_imgnum = number;
}

void JP2Convert::setOutputFilename(QString filename) {
  m_outname = filename;
}

void JP2Convert::setJP2Options(int tilew, int tileh, int levels, bool lazy,
                               bool lossy, double rate) {
  m_tilew = tilew;
  m_tileh = tileh;
  m_levels = levels;
  m_rate = rate;
  m_lossy = lossy;
  m_lazy = lazy;
}

/**
 * The code to run in the second thread
 *
 **/
void JP2Convert::run() {
  QByteArray in = m_inname.toAscii();
  QByteArray out = m_outname.toAscii();
  penc_t *enc = 0;

  /* Get encoding options from dialog feilds */
  enc = rsa_enc_create(in.data(), out.data());

  rsa_enc_settilesize (enc, m_tilew, m_tileh);
  rsa_enc_setlossy (enc, m_lossy, m_rate);
  rsa_enc_setlevels (enc, m_levels);
  rsa_enc_setimage (enc, m_imgnum);
  rsa_enc_setlazy  (enc, m_lazy);

  /* Start encoding */
  emit progress(0);
  m_code = rsa_enc_encode(enc);

  /* Close encoder */
  rsa_enc_destroy(enc);
  enc = 0;
}

int JP2Convert::getProgress() {
  double ntiles   = (double) rsa_enc_numtiles();
  double enctiles = (double) rsa_enc_numenctiles();

  if (ntiles > 0)
    return(int(enctiles/ntiles*100));
  else
    return 0;
}

/*
  EVENT HANDELING (SLOTS)
  ----------------------------------------------*/
void JP2Convert::stop() {
  rsa_enc_terminate();
}

void JP2Convert::timeout() {
  emit progress(getProgress());
}

void JP2Convert::convert() {
  QFile infile (m_inname.trimmed());
  QFile outfile(m_outname.trimmed());

  /* Make sure user input is ok */
  if (!infile.exists()) {
    emit complete(tr("Error: Input file does not exist."));
    return;
  } else if (m_inname.compare(m_outname, Qt::CaseInsensitive) == 0) {
    emit complete(tr("Error: Input and output files have the same name (cannot overwrite the input.)"));
    return;
  } else if( outfile.exists() ) {
    emit warning( tr("Warning: Overwriting existing file.") );
  }
  /* Start conversion */
  start();
}

void JP2Convert::threadEvent() {
  if (isRunning()) {
    m_timer.start();
    return;
  }

  m_timer.stop();
  emit progress(100);

  /* Check return code */
  switch (m_code) {
    case RSA_ENC_SUCCESS:
      emit complete(tr("Conversion successfuly completed."));
      break;
    case RSA_ENC_STOP:
      emit complete(tr("Conversion terminated"));
      break;
    case RSA_ENC_ERROR:
      emit complete(tr("Conversion was not successful.") +
                    tr(" File format or color space not supported."));
      break;
    default:
      emit complete(tr("Conversion error. Check filenames.") +
                    tr(" File format or color space not supported."));
      break;
  }
}

} // namespace image
} // namespace sedeen
