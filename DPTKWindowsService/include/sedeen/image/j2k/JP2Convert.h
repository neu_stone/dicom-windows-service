#ifndef SEDEEN_SRC_IMAGE_J2K_JP2CONVERT_H
#define SEDEEN_SRC_IMAGE_J2K_JP2CONVERT_H

// Qt includes
#include <QtGui>

#include "image/DllApi.h"

namespace sedeen {
namespace image {

/**
 * JPEG-2000 to Big-TIFF (and visa-versa) conversion class.
 * <br/>
 * <br/>
 * This is a multi-threaded class for converting between the
 * JPEG-2000 and TIFF (Classic/Big) formats. Convertsion is
 * always performed in the background.
 * <br/>
 * <br/>
 * To convert a file use setInputFilename(), setOutputFilename(), and
 * optionally setJP2Options() if required. Start the decoding process
 * by using the convert() slot and when decoding is finished the
 * complete() signal will be emitted with a message indicating
 * sucess or failure.
 *
 */
class SEDEEN_IMAGE_API JP2Convert : public QThread {
  Q_OBJECT;
 public:
  /**
   * Constructor
   *
   * @param  parent the parent window
   * @param  filename the name of file to convert
   */
  JP2Convert(QWidget *parent = 0, QString filename = QString());

  /**
   * Destructor
   */
  ~JP2Convert();

  /**
   * Sets the string as the input filename.
   *
   * @param  filename the name of the file to use as input
   * @param  number the image to use for multi-image format such as TIFF
   */
  void setInputFilename(QString filename, int number = 0);

  /**
   * Sets the string as the output filename.  If
   * output file already exists, it's overwritten.
   *
   * @param  filename the name of the file to set as output
   */
  void setOutputFilename(QString filename);

  /**
   * JPEG-2000 encoding options.
   *
   * @param  tilew tile width
   * @param  tileh tile height
   * @param  levels the number of resolution levels
   * @param lazy
   * @param  lossy lossy/lossless coding option
   * @param  rate the lossy coding rate (0,1], ignored for lossless coding
   */
  void setJP2Options(int tilew, int tileh, int levels, bool lazy, bool lossy,
                     double rate);

  /**
   * The percentage of conversion completed.
   *
   * @return  the percentage value of the conversion completed
   */
  int getProgress();

 signals:
  /**
   * Emits when conversion is complete with the conversion message.
   * Either if sucessful or on failure.
   *
   * @return  the percentage value of the conversion completed
   */
  void complete(QString message);

  /**
   * Emits when conversion progress percentage.
   * Either if sucessful or on failure.
   *
   * @param percent the percentage value of the conversion completed
   */
  void progress(int percent);

  /**
   * Emits warning message during conversion.
   *
   * @param  message the warning message
   */
  void warning(QString message);

 public slots:
  /**
   * Begins the conversion
   */
  void convert();

  /**
   * Stops an ongoing conversion
   */
  void stop();

 protected:
  /**
   * Worker thread method.
   */
  void run();

 protected slots:
  /**
   * Thread event handler.
   */
  void threadEvent();

  /*
   * Progress update
   */
  void timeout();

 protected:
  QTimer m_timer;  ///< progress bar timer3
  QString m_messages;  ///< the messages returned with complete signal
  QString m_inname;  ///< input file name
  QString m_outname;  ///< output file name
  int  m_tilew;  ///< tile size
  int m_tileh;  ///< tile size
  int m_levels;  ///< number of resolution levels
  int m_imgnum;  ///< the image to encod
  double m_rate;   ///< coding rate
  bool m_lossy;  ///< lossy coding option
  bool m_lazy;   ///< lazy coding option
  int  m_code;   ///< coversion return code
}; // class JP2Convert

} // namespace image
} // namespace sedeen

#endif

