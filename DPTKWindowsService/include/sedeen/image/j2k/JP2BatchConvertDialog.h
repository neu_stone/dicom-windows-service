#ifndef SEDEEN_SRC_IMAGE_J2K_JP2BATCHCONVERTDIALOG_H
#define SEDEEN_SRC_IMAGE_J2K_JP2BATCHCONVERTDIALOG_H

// Qt includes
#include <QtGui>

// System headers
#include <memory>

// User includes
#include "image/j2k/JP2Convert.h"
#include "image/DllApi.h"

namespace Ui {

class dialogBatchConverter;

}

namespace sedeen {
namespace image {

/**
 *	JPEG-2000 to Big-TIFF (and visa-versa) Conversion class
 */
class SEDEEN_IMAGE_API JP2BatchConverter : public QDialog {
  Q_OBJECT;
public:
  /**
   *	Constructor
   *
   *	@param		parent the parent window
   *	@param		f
   */
  JP2BatchConverter(QWidget * parent = 0, Qt::WindowFlags f = 0);

  /**
   *	Destructor
   */
  ~JP2BatchConverter();

 private:
  /**
   *	Disables dialog except for stop button.
   */
  void enableDialog(bool enabled);

  /**
   *	Creates an output name for the input filename.
   */
  QString generateOutname(QString name);

  /**
   *	Gets the image number to use from TIFF file. For JPEG-2000
   *	images uses the 0th image.
   */
  int tifImageNumber(QString filename);

 private slots:
  /**
   *	Event handler for input filename browse button .
   */
  void addFiles();

  /**
   *	Event handler for output filename browse button .
   */
  void removeFiles();

  /**
   *	Convert button event handler
   */
  void convert();

  /**
   *	Actual convert function. Calls codec functions.
   */
  void convert2();

  /**
   *	Stop button event handler
   */
  void stop();

  /**
   *	Progress bar event handler
   *
   *	@param	percent the amount of current image that is complete
   */
  void progress(int percent);

  /**
   *	Receives converter messages.
   *
   *	@param	mes the message to be displayed
   */
  void warning(QString mes);

  /**
   *	Receives converter messages.
   *
   *	@param	mes the message to be displayed
   */
  void complete(QString mes);

  /**
   *	Browse for output directory
   */
  void browseDir();

  /**
   *	Use input dir for the ouput directory
   */
  void useDir();

  /**
   *	Chanes thread priority in response to comobobox index changes.
   *
   *	@param	index new index of combobox
   */
  void priority(int index);

 private:
  /// Qt designer class for convert dialog
  std::auto_ptr<Ui::dialogBatchConverter> m_ui;

  /// converter class
  JP2Convert	m_converter;

  /// current image being processed from list
  int	m_current;
}; // class JP2BatchConvertDialog

} // namespace image
} // namespace sedeen

#endif

