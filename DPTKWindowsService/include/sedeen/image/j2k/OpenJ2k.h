#ifndef SEDEEN_SRC_IMAGE_J2K_OPENJ2K_H
#define SEDEEN_SRC_IMAGE_J2K_OPENJ2K_H

// System headers
#include <QtCore>
#include <memory>

// User includes
#include "image/DllApi.h"
#include "image/io/OpenTiledImage.h"

namespace sedeen {
namespace image {

class SelectiveDecoder;

/// Create an ImageData class for a JPEG2000 image.
class SEDEEN_IMAGE_API OpenJ2k : public OpenTiledImage {
 public:
  virtual ~OpenJ2k();

 private:
  virtual std::auto_ptr<SelectiveDecoder> getSelectiveDecoder(
      const std::string& filename) const;

  virtual QStringList getFilter() const;
}; // end clas OpenJ2k

} // end namespace image
} // end namespace sedeen

#endif
