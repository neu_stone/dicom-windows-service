#ifndef SEDEEN_SRC_IMAGE_J2K_JP2PYRAMID_H
#define SEDEEN_SRC_IMAGE_J2K_JP2PYRAMID_H

// User includes
#include "global/geometry/Size.h"
#include "image/DllApi.h"
#include "image/io/ImagePyramid.h"

namespace sedeen {
namespace image {

class TileInfo;

/*!
  \class JP2Pyramid
  \brief Describes the tile pyramid structure for a JPEG-2000 image.

  Note that resolution level (reslvl) 0 must the represents full resolution image.

*/
class SEDEEN_IMAGE_API JP2Pyramid : public ImagePyramid {
 public:
  /*!
    \brief constructor
  */
  JP2Pyramid(const Size &imageSize, const Size &tileSize, int32_t levels,
             const Size &tileOffset = Size(0,0) );

  /*!
    \brief Destructor
  */
  ~JP2Pyramid();

 protected:
  int32_t doGetLevel(const double scale) const;
  ImagePyramidPtr doGetClone() const;
};

}   // namespace sedeen
}   // namespace image

#endif


