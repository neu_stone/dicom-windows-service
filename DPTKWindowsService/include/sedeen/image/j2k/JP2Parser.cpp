// main header
#include "image/j2k/JP2Parser.h"

// System includes
#include <cassert>
#include <sstream>

// External module includes
#include "pjasper/include/pjasper.h"

// User includes
#include "image/io/Exceptions.h"
#include "image/j2k/JP2Pyramid.h"

namespace sedeen
{
namespace image
{

JP2Parser::JP2Parser(const char *filename) :
_filename(filename),
_jp2(rsa_dec_open_file(filename)),
_pyramid()
{
  using boost::uint16_t;
  using boost::uint32_t;

  // error check
  if( 0 == _jp2 )
      throw FileAccessError(filename);

  // Get properties
  uint32_t imgw       = rsa_dec_imagewidth(_jp2),
           imgh       = rsa_dec_imageheight(_jp2),
           tilew      = rsa_dec_tilewidth(_jp2),
           tileh      = rsa_dec_tileheight(_jp2),
           levels     = rsa_dec_numrlvls(_jp2),
           tileXoff   = rsa_dec_tilexoff(_jp2),
           tileYoff   = rsa_dec_tileyoff(_jp2),
           imageXoff  = rsa_dec_imagexoff(_jp2),
           imageYoff  = rsa_dec_imageyoff(_jp2),
           xOffset    = imageXoff - tileXoff,
           yOffset    = imageYoff - tileYoff;

  // Return a pyramid
  _pyramid.reset(new JP2Pyramid(Size(imgw, imgh),
                                Size(tilew, tileh),
                                levels,
                                Size(xOffset, yOffset)));

  if (static_cast<uint32_t>(_pyramid->count(0)) != rsa_dec_numtiles(_jp2) ||
      static_cast<uint32_t>(_pyramid->cols(0)) != rsa_dec_numhtiles(_jp2) ||
      static_cast<uint32_t>(_pyramid->rows(0)) != rsa_dec_numvtiles(_jp2)) {
    rsa_dec_close(_jp2);
    _pyramid.reset();
    throw InvalidImage(filename, "Invalid image detected");
  }
}

JP2Parser::~JP2Parser() {
  rsa_dec_close(_jp2);
}

void* JP2Parser::handle() const {
  return _jp2;
}

ImagePyramidPtr JP2Parser::doGetPyramid() const {
  return _pyramid;
}

std::string JP2Parser::doGetFilename() const {
  return _filename;
}

std::string JP2Parser::doGetProducer() const {
  return "Unknown";
}

std::string JP2Parser::doGetCompression() const {
  return "JPEG-2000";
}

std::string JP2Parser::doGetDescription() const {
    assert(false);
  return std::string();
}

bool JP2Parser::doGetHasLabelImage() const {
  return false;
}

bool JP2Parser::doGetHasMacroImage() const {
  return false;
}

bool JP2Parser::doGetHasThumbnailImage() const {
  return false;
}

RawImage JP2Parser::doGetLabelImage() const {
   return RawImage();
}

RawImage JP2Parser::doGetMacroImage() const {
   return RawImage();
}

RawImage JP2Parser::doGetThumbnailImage() const {
   return RawImage();
}

SizeF JP2Parser::doGetPixelSize() const {
  return SizeF(0,0);
}

int JP2Parser::doGetComponents() const {
  return rsa_dec_numcomps(handle());
}

int JP2Parser::doGetBpp() const
{
  int bpp = 0;
  for (int i = 0; i < components(); ++i)
      bpp += doGetBpc(i);
  return bpp;
}

int JP2Parser::doGetBpc(int channel) const {
  assert(channel < doGetComponents());
  return rsa_dec_compprec(handle(), channel);
}

double JP2Parser::doGetMagnification() const {
  return 0;
}

Color JP2Parser::doGetColor() const
{
    int bps = rsa_dec_compprec(handle(), 1);
    switch (rsa_dec_imageclrspc(handle()))
    {
        case RSA_COLOR_RGB:
          if (8==bps) return RGB_8;
          else if (16==bps) return RGB_16;
          else return Unknown;
        case RSA_COLOR_GRAY:
          if (8==bps) return Gray_8;
          else if (16==bps) return Gray_16;
          else return Unknown;
        //case RSA_COLOR_YCC: return YCC;
        //  //vs = (vs == 4) ? 0 : vs;
        //  //vs = (vs == 1) ? 4 : vs;
        //  //hs = (hs == 1) ? 4 : hs;
        //  //str << "YCbCr 4" << ":" << hs << ":" << vs;
        //  //break;
        //case RSA_COLOR_ICC: return ICC;
        //case RSA_COLOR_UNKNOWN:
        default: return Unknown;
    }
}

}   // namespace image
}   // namespace sedeen

