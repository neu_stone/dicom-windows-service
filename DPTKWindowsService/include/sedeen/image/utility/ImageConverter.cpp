// Main includes
#include "image/utility/ImageConverter.h"

// System includes
#include <boost/cstdint.hpp>
#include <cassert>

// User includes
#include "global/Debug.h"
#include "image/buffer/RawImage.h"

namespace sedeen {
namespace image {
namespace utility {

namespace {
// Make 8-bit color table
QVector<QRgb> grayscaleColorTable() {
  static QVector<QRgb> table;
  if (table.empty()) {
    table.resize(256);
    for (int i = 0; i < 256; ++i)
      table[i] = qRgb(i,i,i);
  }
  return table;
}

// Make binary color table
QVector<QRgb> opacityColorTable() {
  static QVector<QRgb> table;
  if (table.empty()) {
    table.resize(256);
    for (int i = 0; i < 256; ++i)
      table[i] = qRgba(255,255,255,i);
  }
  return table;
}
} // namespace

class RawImageAutoScale;

typedef std::auto_ptr<RawImageAutoScale> RawImageAutoScalePtr;

RawImageAutoScalePtr getImageAutoScaler(const RawImage& image);

QImage fromInterleavedRGBImage(const RawImageAutoScale& scaler);

QImage fromPlanarRGBImage(const RawImageAutoScale& scaler);

QImage fromInterleavedRGBImage(const RawImage& image);

QImage fromPlanarRGBImage(const RawImage& image);

QImage toRGBQImage(const RawImage& image, const bool volatile_share);

QImage toGrayQImage(const RawImage& image, const bool volatile_share);

QImage toQImage(const RawImage& image, const bool volatile_share) {
  // Create image
  switch (image.components()) {
    case 1:
      debug::warning_x(image.color() == image::Gray_8 ||
                       image.color() == image::Gray_16 ||
                       image.color() == image::Alpha,
                       "Incorrect color space specified.",
                       __FUNCTION__, __LINE__);
      return toGrayQImage(image, volatile_share);
      break;

    case 3:
      debug::warning_x(image.color() == image::RGB_8 ||
                       image.color() == image::RGB_16,
                       "Incorrect color space specified.",
                       __FUNCTION__, __LINE__);
      return toRGBQImage(image, volatile_share);
      break;

    case 4:
      debug::warning_x(image.color() == image::ARGB_8 ||
                       image.color() == image::ARGB_16,
                       "Incorrect color space specified.",
                       __FUNCTION__, __LINE__);
      return toRGBQImage(image, volatile_share);
      break;

    default:
      return QImage();
  }
}

class RawImageAutoScale {
 public:
  RawImageAutoScale() {}
  virtual ~RawImageAutoScale() {}
  virtual const RawImage& image() const = 0;
  virtual int operator[](int index) const = 0;
};

class RawImageAutoScale255 : public RawImageAutoScale {
 public:
  RawImageAutoScale255(const RawImage& image)
      : _image(image),
        _scale(static_cast<double>(image.maxValue()) / 255.0) {}
  const RawImage& image() const { return _image; }
  int operator[](int index) const {
    int val = static_cast<int>(static_cast<double>(_image[index]) /
                               _scale + 0.5);
    Q_ASSERT(val >= 0 && val <= 255);
    return val;
  }
 private:
  RawImage _image;
  double _scale;
};

class RawImageAutoScaleByCasting : public RawImageAutoScale {
 public:
  RawImageAutoScaleByCasting(const RawImage& image) : _image(image) {}
  const RawImage& image() const { return _image; }
  int operator[](int index) const { return static_cast<int>(_image[index]); }
 private:
  RawImage _image;
};

RawImageAutoScalePtr getImageAutoScaler(const RawImage& image) {
  switch (image.bitsPerComponent()) {
    case 8:
      return RawImageAutoScalePtr(new RawImageAutoScaleByCasting(image));
      break;

    case 16:
      // Fall-through
    default:
      return RawImageAutoScalePtr(new RawImageAutoScale255(image));
  }
}

QImage toRGBQImage(const RawImage& image, const bool volatile_share) {
  // Can we attache this to a buffer
  const bool interleaved = (image.order() == image::Interleaved ? true : false);

  // get the format of buffer
  Q_ASSERT((4==image.components()) || (3==image.components()));
  QImage::Format format = (3==image.components() ? QImage::Format_RGB888 :
                                                   QImage::Format_ARGB32);

  // return an image
  if (volatile_share && interleaved) {
    const uchar* buffer = static_cast<const uchar*>(image.data().get());
    const image::int32_t bytesPerLine = image.width() * image.components();
    return QImage(buffer, image.width(), image.height(), bytesPerLine, format);
  } else if (interleaved) {
    return fromInterleavedRGBImage(*getImageAutoScaler(image));
  } else {
    return fromPlanarRGBImage(*getImageAutoScaler(image));
  }
}

QImage fromPlanarRGBImage(const RawImageAutoScale& scaler) {
  // Test pre conditions
  assert( scaler.image().color() == image::RGB_8 ||
          scaler.image().color() == image::RGB_16 ||
          scaler.image().color() == image::ARGB_16 ||
          scaler.image().color() == image::ARGB_8);
  assert( scaler.image().components() == 3 ||
          scaler.image().components() == 4 );

  // Make image
  QImage img(scaler.image().width(), scaler.image().height(),
             QImage::Format_ARGB32);
  if (img.isNull()) return QImage();

  // Calulate plane offsets
  int32_t plane1 = 0;
  int32_t plane2 = scaler.image().height() * scaler.image().width();
  int32_t plane3 = plane2 * 2;
  int32_t plane4 = plane2 * 3;

  if (scaler.image().components()==3) {
    for (int32_t row=0; row<scaler.image().height(); ++row) {
      QRgb* scanline = reinterpret_cast<QRgb*>(img.scanLine(row));
      for (image::int32_t col=0; col<scaler.image().width(); ++col)
        scanline[col] = qRgb(scaler[plane1++],
                             scaler[plane2++],
                             scaler[plane3++]);
    }
  }
  else if (scaler.image().components()==4) {
    for (int32_t row=0; row<scaler.image().height(); ++row) {
      QRgb* scanline = reinterpret_cast<QRgb*>(img.scanLine(row));
      for (image::int32_t col=0; col<scaler.image().width(); ++col)
        scanline[col] = qRgba(scaler[plane1++],
                             scaler[plane2++],
                             scaler[plane3++],
                             scaler[plane4++]);
    }
  }
  else assert(false);

  return img;
}

QImage fromInterleavedRGBImage(const RawImageAutoScale& scaler) {
  // Test pre conditions
  assert( scaler.image().color() == image::RGB_8 ||
          scaler.image().color() == image::RGB_16 ||
          scaler.image().color() == image::ARGB_8 ||
          scaler.image().color() == image::ARGB_16);
  assert(scaler.image().components() == 3 ||
          scaler.image().components() == 4 );

  // Make image
  QImage img(scaler.image().width(),
             scaler.image().height(),
             QImage::Format_ARGB32);

  if (img.isNull())
    return img;

  if (scaler.image().components()==3) {
    for (image::int32_t row=0, index=0; row<scaler.image().height(); ++row) {
      QRgb* scanline = reinterpret_cast<QRgb*>(img.scanLine(row));
      for (image::int32_t col=0; col<scaler.image().width(); col++, index+=3) {
        scanline[col] = qRgb(scaler[index],
                             scaler[index+1],
                             scaler[index+2]);
      }
    }
  }
  else if (scaler.image().components()==4) {
    for (image::int32_t row=0, index=0; row<scaler.image().height(); ++row) {
      QRgb* scanline = reinterpret_cast<QRgb*>(img.scanLine(row));
      for (image::int32_t col=0; col<scaler.image().width(); col++, index+=4) {
        scanline[col] = qRgba(scaler[index],
                             scaler[index+1],
                             scaler[index+2],
                             scaler[index+3]);
      }
    }
  }
  else assert(false);
  return img;
}

QImage toGrayQImage(const RawImage& image, const bool volatile_share) {
  // Test pre conditions
  assert( image.color() == image::Alpha ||
          image.color() == image::Gray_8 ||
          image.color() == image::Gray_16);
  assert(image.components() == 1);
  const bool byteImage = (image.bitsPerComponent() == 8);

  // pick a table
  QVector<QRgb> table = (image.color() == image::Alpha ?
                          opacityColorTable() : grayscaleColorTable());

  // Can we attache this to a buffer
  if (volatile_share && byteImage) {
    const uchar* buffer = static_cast<const uchar*>(image.data().get());
    QImage img(buffer, image.width(), image.height(), image.width(),
               QImage::Format_Indexed8);
    img.setColorTable(table);
    return img;
  }

  // Create Image
  QImage img(image.width(), image.height(), QImage::Format_Indexed8);
  const image::int32_t bytesPerLine = img.bytesPerLine();

  if (!img.isNull()) {
    img.setColorTable(table);
    RawImageAutoScalePtr scaler = getImageAutoScaler(image);

    for (image::int32_t row=0, index=0; row<image.height(); ++row) {
      uchar* scanline = img.scanLine(row);
      image::int32_t col = 0;

      // Fill row with data
      for (; col < image.width(); ++col, ++index) {
        scanline[col] = (*scaler)[index];
      }

      // scanlines are 32bit aligned fill with zeros
      for (; col < bytesPerLine; ++col) {
        scanline[col] = 0;
      }
    }
  }
  // This approach, although cleaner, is much slower in debug and release modes
  //
  //if( !img.isNull() )
  //{
  //    img.setColorTable(table);
  //    RawImageAutoScalePtr caster = getImageAutoScaler(image);
  //
  //    // Fill the image and scale if required
  //    for(image::int32_t row = 0, index = 0; row < image.height(); row++)
  //        for(image::int32_t col = 0; col < image.width(); col++, index++)
  //            img.setPixel(col, row, (*caster)[index]);
  //}

  return img;
}

} // namespace utility
} // namespace image
} // namespace sedeen
