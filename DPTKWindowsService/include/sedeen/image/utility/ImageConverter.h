#ifndef SEDEEN_SRC_IMAGE_UTILITY_IMAGECONVERTER_H
#define SEDEEN_SRC_IMAGE_UTILITY_IMAGECONVERTER_H

// System headers
#include <QImage>

#include "image/DllApi.h"

namespace sedeen {
namespace image {

class RawImage;

namespace utility {

/// Converts from ImageBuffer to QImage.
//
/// By default \a volatile_share is FALSE and \a image pixel data is copied into
/// the QImage. IF \a volatile_share is TRUE, the \a image pixel buffer is
/// linked with QImage where possible. This leads to a fast conversion but with
/// the condition that the \a image pixel buffer must remain valid for the
/// duration of the QImage's lifetime.
/// \param image
/// \param volatile_share
SEDEEN_IMAGE_API
QImage toQImage(const sedeen::image::RawImage& image,
                bool volatile_share = false);

} // namespace utility
} // namespace image
} // namespace sedeen

#endif
