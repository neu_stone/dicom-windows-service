#ifndef SEDEEN_SRC_IMAGE_UTILITY_IMAGEIOTYPES_H
#define SEDEEN_SRC_IMAGE_UTILITY_IMAGEIOTYPES_H

// System headers
#include <boost/shared_ptr.hpp>

namespace sedeen {
namespace image {

// Read only class
class ImagePyramid;
typedef boost::shared_ptr<ImagePyramid> ImagePyramidPtr;

// Read only class
class DecodeRequest;
typedef boost::shared_ptr<DecodeRequest> DecodeRequestPtr;

// Read only class
class ImageParser;
typedef boost::shared_ptr<ImageParser> ImageParserPtr;

}   // namespace image
}   // namespace sedeen

#endif
