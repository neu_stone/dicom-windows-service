#ifndef SEDEEN_SRC_IMAGE_FILTER_FILTERTOOL_H
#define SEDEEN_SRC_IMAGE_FILTER_FILTERTOOL_H

#include <QtGui>
#include <memory>

#include "image/DllApi.h"
#include "image/filter/ImageFilter.h"

class QtColorPicker;

namespace Ui {

class levelsWidget;

} // end namespace Ui

namespace sedeen {
namespace image {
namespace filter {

class SEDEEN_IMAGE_API FilterTool : public QWidget {
  Q_OBJECT;
 public:
  /// Construct a new FilterTool with a set of default filter values.
  //
  /// \param parent
  /// Parent widget. May be null.
  ///
  /// \param flags
  /// Window options flags. May be 0.
  FilterTool(QWidget* parent, Qt::WindowFlags flags);

  ~FilterTool();

  /// Set a new ImageFilter as the base state.
  void setFilter(const ImageFilter& filter);

 public slots:
  /// Apply a Brightness filter evenly to all colour channels.
  //
  /// See the Brightness class for details.
  void setBrightness(int brightness);

  /// Apply a Contrast filter evenly to all colour channels.
  //
  /// See the Contrast class for details.
  void setContrast(int contrast);

  /// Apply a Gamma filter evenly to all colour channels.
  //
  /// See the Gamma class for details.
  void setGamma(int gamma);

  /// Apply a pseudocolour to the image.
  //
  /// Though applying a pseudocolour to an already coloured image will not cause
  /// an error, the results will probably not be what you were expecting.
  ///
  /// The pseudocolour function works by applying a Scale filter to each of the
  /// channels, scaling them to the values of the selected colour. For a
  /// greyscale image, this results in shades of grey becoming shades of the
  /// desired colour.
  ///
  /// \param color
  /// An RGB colour-triple to be applied to the image. Any alpha information is
  /// ignored.
  void setPseudocolor(const QColor& color);

  /// Set the filter to its default state.
  void reset();

  /// Set the fitler to the state last set by setFilter().
  void revert();

  /// Revert to the settings passed to the constructor and close.
  void cancel();

 private:
  void connectBrightness();

  void connectContrast();

  void connectGamma();

  void connectButtons();

  void updateFilter(unsigned int filterStage, int r_param, int g_param,
                    int b_param);

  bool updateChannel(unsigned int filterStage, unsigned int channel,
                     int parameter);

  void extractCurrentSettings();

  void setChannel(unsigned int channel);

 private slots:
  void setR(bool active);

  void setG(bool active);

  void setB(bool active);

  void setAll(bool active);

  void updateColor(bool active);

 signals:
  void filterUpdated(const image::filter::ImageFilter& filter) const;

  void cancel() const;

 private:
  std::auto_ptr<Ui::levelsWidget> m_levelsWidget;

  QtColorPicker* m_colorPicker;

  unsigned int m_activeChannel;

  bool m_allChannels;

  ImageFilter m_initialFilter;

  ImageFilter m_filter;
}; // class FilterTool

namespace filter_tool {

/// Handles the mutual update of the gamma slider and spin-box.
//
/// Since the slider works on int and the spin box on double, a conversion needs
/// to occur before they may both be updated. This class wraps the rescaling.
class SEDEEN_IMAGE_API GammaSync : public QObject {
  Q_OBJECT;
 public:
  GammaSync(int minI, int maxI, double minD, double maxD, QObject *parent);

 public slots:
  /// To be wired to the slider's valueChanged() signal.
  void set(int value);
  /// To be wired to the spinbox's valueChanged() signal.
  void set(double value);

 signals:
  /// To be wired to the slider's setValue() slot.
  void update(int value);
  /// To be wired to the spinbox's setValue() slot.
  void update(double value);

 private:
  int m_minI;
  int m_maxI;
  double m_minD;
  double m_maxD;
}; // class GammaSync

} // namespace filter_tool

} // namespace filter
} // namespace image
} // namespace sedeen

#endif
