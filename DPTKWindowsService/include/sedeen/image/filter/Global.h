#ifndef SEDEEN_SRC_IMAGE_FILTER_GLOBAL_H
#define SEDEEN_SRC_IMAGE_FILTER_GLOBAL_H

#include "image/DllApi.h"

namespace sedeen {
namespace image {
namespace filter {

SEDEEN_IMAGE_API
double rescale(double value, double oldMin, double oldMax, double newMin,
               double newMax);

/// Generates sequential numbers starting from 0.
class SEDEEN_IMAGE_API IndexGenerator {
 public:
  IndexGenerator();

  int operator()();

 private:
  int m_index;
}; // end class IndexGenerator

} // namespace filter
} // namespace image
} // namespace sedeen

#endif

#include "Global.icc"
