// Primary header
#include "image/filter/Brightness.h"

#include <QtCore>

#include "image/filter/Global.h"

namespace sedeen {
namespace image {
namespace filter {

Brightness::Brightness(unsigned int nLevels)
    : FilterBase(nLevels),
      m_brightness(0) {}

Brightness::~Brightness() {}

FilterBase* Brightness::doClone() const {
  return new Brightness(*this);
}

void Brightness::doReset() {
  setParameter(0);
}

int Brightness::getParameter() const {
  return qRound(rescale(m_brightness,
                        -double(maxLevel()), maxLevel(),
                        minParameter(), maxParameter()));
}

void Brightness::doSetParameter(int brightness) {
  // Scale parameter to [-255..255]
  m_brightness = qRound(rescale(brightness,
                                minParameter(), maxParameter(),
                                -double(maxLevel()), maxLevel()));
}

int Brightness::getMinParameter() const {
  return -50;
}

int Brightness::getMaxParameter() const {
  return 50;
}

int Brightness::doFilter(int value) const {
  return value + m_brightness;
}

} // namespace filter
} // namespace image
} // namespace sedeen
