#ifndef SEDEEN_SRC_IMAGE_FILTER_IMAGEFILTER_H
#define SEDEEN_SRC_IMAGE_FILTER_IMAGEFILTER_H

#include <QtGui>
#include <functional>
#include <memory>

#include "image/filter/ChannelCache.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {
namespace filter {

/// Filters listed in order of application.
enum EFilters {
  kGamma,
  kContrast,
  kBrightness,
  kScale
};

enum EChannels {
  kR, /// Red
  kG, /// Green
  kB, /// Blue
  N_RGB
};

class FilterBase;

/// Collection of filters for 24-bit RGB images.
//
/// Basically a wrapper of a number of filter::ChannelCache classes.
///
/// The contained ChannelCache classes use copy-on-write semantics, making ImageFilter cheap to copy.
class SEDEEN_IMAGE_API ImageFilter
    : std::binary_function<unsigned int,int,int> {
 public:
  ImageFilter();

  /// Get the number of channels being filtered.
  unsigned int channels() const;

  /// Get the number of values in each channel.
  unsigned int depth() const;

  /// Get the number of installed filters
  unsigned int filters() const;

  void reset();

  /// Get the current filter parameter for a given stage and channel.
  //
  /// \param filterStage
  /// The desired filter stage. May not be EFilter::NUM_FILTERS.
  ///
  /// \param channel
  /// The channel on which to inquire. Must be less than channels().
  int parameter(unsigned int filterStage, unsigned int channel) const;

  /// Set a filter's parameter.
  //
  /// \param filterStage
  /// The desired filter stage. May not be EFilter::NUM_FILTERS.
  ///
  /// \param channel
  /// The channel to modify. Must be less than channels().
  ///
  /// \param parameter
  /// The value of the parameter to set. Must be within [minParameter(),maxParameter()] for the selected filter.
  ///
  /// \return
  /// TRUE if this changed the value of the selected filter/channel and FALSE if \c parameter was already the current value.
  bool setParameter(unsigned int filterStage, unsigned int channel,
                    int parameter);

  /// Get the minimum valid parameter for a filter stage.
  //
  /// \param filter
  /// The desired filter stage. May not be EFilter::NUM_FILTERS.
  int minParameter(unsigned int filter) const;

  /// Get the maximum valid parameter for a filter stage.
  //
  /// \param filter
  /// The desired filter stage. May not be EFilter::NUM_FILTERS.
  int maxParameter(unsigned int filter) const;

  /// Filter a single channel value.
  //
  /// \param channel
  /// Channel
  ///
  /// \param value
  /// Image value to be modified. Must be within [0,depth()).
  int operator()(unsigned int channel, int value) const;

  /// Returns TRUE if all filters are in their default state.
  //
  /// The defaults are chosen so that the filter has no effect. Resetting the values back to their original settings does not restore an ImageFilter's nullTransform status. Overwrite the class with a default-constructed ImageTransform to accomplish this.
  bool nullTransform() const;

 private:
    void addFilter(const filter::FilterBase& filter);

 private:
  /// Collection of channel-specific filter collections.
  std::vector<filter::ChannelCache> m_channels;

  /// Flag to track modification from default construction.
  bool m_active;
}; // end class ImageFilter

namespace image_filter {

/// Apply an image filter to a QImage.
//
/// Skips processing if ImageFilter::nullTransform() is TRUE.
///
/// \param filter
/// The filter to apply to the image. The filter must contain at least three filter channels.
///
/// \param image
/// The QImage to be modified.
///
/// \return
/// The QImage with each component of each pixel modified by the filter.
SEDEEN_IMAGE_API
QImage apply(const ImageFilter& filter, QImage image);

/// Apply an ImageFilter to a series of QRgb pixels.
class SEDEEN_IMAGE_API Apply : public std::unary_function<QRgb,QRgb> {
 public:
  Apply(const ImageFilter& filter);

  QRgb operator()(const QRgb& pixel) const;

 private:
  const ImageFilter& m_filter;
}; // class Apply

} // namespace image_filter
} // namespace filter
} // namespace image
} // namespace sedeen

#endif

#include "ImageFilter.icc"
