// Primary header
#include "image/filter/Scale.h"

#include <QtCore>

#include "image/filter/Global.h"

namespace sedeen {
namespace image {
namespace filter {

Scale::Scale(unsigned int nLevels)
    : FilterBase(nLevels),
      m_scale(1),
      m_max(nLevels - 1) {}

Scale::~Scale() {}

FilterBase* Scale::doClone() const {
  return new Scale(*this);
}

int Scale::getParameter() const {
  return qRound(rescale(m_scale,
                        0, 1,
                        minParameter(), maxParameter()));
}

void Scale::doSetParameter(int p) {
  m_scale = rescale(p,
                    minParameter(), maxParameter(),
                    0, 1);
}

int Scale::getMinParameter() const {
  return 0;
}

int Scale::getMaxParameter() const {
  return m_max;
}

int Scale::doFilter(int level) const {
  return qRound(level * m_scale);
}

} // namespace filter
} // namespace image
} // namespace sedeen
