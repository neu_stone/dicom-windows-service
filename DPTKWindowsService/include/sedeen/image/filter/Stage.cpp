// Primary header
#include "image/filter/Stage.h"

#include "image/filter/FilterBase.h"

namespace sedeen {
namespace image {
namespace filter {

Stage::Stage(std::auto_ptr<FilterBase> filter)
    : m_filter(filter),
      m_table(m_filter.maxLevel() + 1) {
  updateTable();
}

unsigned int Stage::depth() const {
  return m_table.depth();
}

int Stage::parameter() const {
  return m_filter.parameter();
}

void Stage::setParameter(int parameter) {
  m_filter.setParameter(parameter);
  updateTable();
}

int Stage::minParameter() const {
  return m_filter.minParameter();
}

int Stage::maxParameter() const {
  return m_filter.maxParameter();
}

int Stage::operator()(int value) const {
  return m_table[value];
}

void Stage::updateTable() {
  m_table.reset();
  m_table.apply(m_filter);
}

} // namespace filter
} // namespace image
} // namespace sedeen
