#ifndef SEDEEN_SRC_IMAGE_FILTER_STAGE_H
#define SEDEEN_SRC_IMAGE_FILTER_STAGE_H

#include <functional>

#include "image/filter/Filter.h"
#include "image/filter/LookupTable.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {
namespace filter {

///
class SEDEEN_IMAGE_API Stage : public std::unary_function<int,int> {
 public:
  explicit Stage(std::auto_ptr<FilterBase> filter);

  unsigned int depth() const;

  int parameter() const;

  void setParameter(int parameter);

  /// Minimum acceptable value of the parameter.
  int minParameter() const;

  /// Maximum acceptable value of the parameter.
  int maxParameter() const;

  /// Translate the image channel value.
  int operator()(int value) const;

 private:
  void updateTable();

 private:
  /// The pimpl
  Filter m_filter;

  /// Translation table for this stage of the filter.
  LookupTable m_table;
}; // end class Stage

} // namespace filter
} // namespace image
} // namespace sedeen

#endif
