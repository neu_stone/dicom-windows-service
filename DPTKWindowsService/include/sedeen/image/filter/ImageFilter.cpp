// Primary header
#include "image/filter/ImageFilter.h"

#include "image/filter/Brightness.h"
#include "image/filter/Contrast.h"
#include "image/filter/Gamma.h"
#include "image/filter/Scale.h"

namespace {

const unsigned int N_LEVELS = 256;

}

namespace sedeen {
namespace image {
namespace filter {

ImageFilter::ImageFilter()
    : m_channels(N_RGB),
      m_active(false) {
  addFilter(Gamma(N_LEVELS));
  addFilter(Contrast(N_LEVELS));
  addFilter(Brightness(N_LEVELS));
  addFilter(Scale(N_LEVELS));
}

int ImageFilter::parameter(unsigned int filter, unsigned int channel) const {
  Q_ASSERT(channels() > channel);
  Q_ASSERT(filters() > filter);
  return m_channels[channel].parameter(filter);
}

bool ImageFilter::setParameter(unsigned int filter, unsigned int channel,
                               int parameter) {
  Q_ASSERT(channels() > channel);
  Q_ASSERT(filters() > filter);

  bool modified = m_channels[channel].setParameter(parameter, filter);
  m_active = m_active || modified;
  return modified;
}

int ImageFilter::minParameter(unsigned int filter) const {
  Q_ASSERT(filters() > filter);
  return m_channels.front().minParameter(filter);
}

int ImageFilter::maxParameter(unsigned int filter) const {
  Q_ASSERT(filters() > filter);
  return m_channels.front().maxParameter(filter);
}

void ImageFilter::addFilter(const filter::FilterBase& filter) {
  std::for_each(m_channels.begin(),
                m_channels.end(),
                filter::channel_cache::Push(filter));
}

namespace image_filter {
namespace {

/// Filter RGB888 image data.
QImage applyRgb888(const ImageFilter& filter, QImage image) {
  Q_ASSERT(QImage::Format_RGB888 == image.format());

  for (int y = 0; image.height() != y; ++y) {
    uchar* pixel = image.scanLine(y);
    /// Caution: the buffer returned by scanLine() is not 3*width() bytes long.
    /// It has been padded to 32-bit alignment.
    for (int x = 0; image.width() != x; ++x, pixel+=3) {
      *(pixel+0) = filter(0, *(pixel+0));
      *(pixel+1) = filter(1, *(pixel+1));
      *(pixel+2) = filter(2, *(pixel+2));
    }
  }
  return image;
}

/// Filter 32-bit image data.
//
/// Note that this will handle both the ARGB32 and RGB32 formats.
QImage applyArgb32(const ImageFilter& filter, QImage image) {
  Q_ASSERT(QImage::Format_ARGB32 == image.format() ||
           QImage::Format_RGB32 == image.format());

  for (int y = 0; image.height() != y; ++y) {
    QRgb* pixel = reinterpret_cast<QRgb*>(image.scanLine(y));
    std::transform(pixel, pixel + image.width(), pixel, Apply(filter));
  }
  return image;
}

} // namespace

QImage apply(const ImageFilter& filter, QImage image) {
  /// Make sure there are channels for R, G, and B.
  Q_ASSERT(filter.channels() >= 3);

  if (!filter.nullTransform() && !image.isNull()) {
    switch (image.format()) {
      case QImage::Format_RGB888:
        image = applyRgb888(filter, image);
        break;

      case QImage::Format_RGB32:
      case QImage::Format_ARGB32:
        image = applyArgb32(filter, image);
        break;

      default:
        image = applyArgb32(filter,
                            image.convertToFormat(QImage::Format_ARGB32));
        break;
    }
  }

  return image;
}

} // namespace image_filter
} // namespace filter
} // namespace image
} // namespace sedeen
