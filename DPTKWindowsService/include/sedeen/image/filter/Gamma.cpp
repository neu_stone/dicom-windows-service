// Primary header
#include "image/filter/Gamma.h"

#include <cmath>

#include "image/filter/Global.h"

namespace sedeen {
namespace image {
namespace filter {

Gamma::Gamma(unsigned int nLevels)
    : FilterBase(nLevels),
      m_gamma(1) {}

Gamma::~Gamma() {}

FilterBase* Gamma::doClone() const {
  return new Gamma(*this);
}

int Gamma::getParameter() const {
  return qRound(rescale(m_gamma,
                        0, 1,                             // from
                        minParameter(), maxParameter())); // to
}

void Gamma::doSetParameter(int parameter) {
  m_gamma = rescale(parameter,
                    minParameter(), maxParameter(), // from
                    0, 1);                          // to
}

int Gamma::doFilter(int level) const {
  double levelFrac = rescale(level,
                             0, maxLevel(),
                             0, 1);
  levelFrac = std::pow(levelFrac, m_gamma);
  return qRound(rescale(levelFrac,
                        0, 1,            // from
                        0, maxLevel())); // to
}

} // namespace filter
} // namespace image
} // namespace sedeen
