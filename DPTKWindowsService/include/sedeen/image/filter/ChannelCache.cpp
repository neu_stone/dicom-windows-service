// Primary header
#include "image/filter/ChannelCache.h"

#include <QtCore>
#include "image/filter/FilterBase.h"

namespace sedeen {
namespace image {
namespace filter {

ChannelCache::ChannelCache()
    : m_stale(false),
      m_table(),
      m_filters() {}

unsigned int ChannelCache::depth() const {
  return m_table.depth();
}

unsigned int ChannelCache::filters() const {
  return m_filters.size();
}

int ChannelCache::parameter(unsigned int filter) const {
  Q_ASSERT(filters() > filter);
  return m_filters[filter].parameter();
}

bool ChannelCache::setParameter(int value, unsigned int filter) {
  Q_ASSERT(filters() > filter);
  if (value != m_filters[filter].parameter()) {
    m_filters[filter].setParameter(value);
    m_stale = true;
  }
  return m_stale;
}

int ChannelCache::minParameter(unsigned int filter) const {
  Q_ASSERT(filters() > filter);
  return m_filters[filter].minParameter();
}

int ChannelCache::maxParameter(unsigned int filter) const {
  Q_ASSERT(filters() > filter);
  return m_filters[filter].maxParameter();
}

void ChannelCache::push(std::auto_ptr<FilterBase> filter) {
  // Insist on a valid pointer
  Q_ASSERT(filter.get());
  // Either this is the first filter inserted, or it's depth matches the rest
  Q_ASSERT(m_filters.empty() ||
           m_filters.front().depth() == filter_base::depth(*filter));
  // Guarantee that inserted filters have an no-op transformation to start.
  Q_ASSERT(filter_base::isIdentityTransform(*filter));

  // Initialize the lookup table
  if (m_filters.empty()) m_table = LookupTable(filter_base::depth(*filter));
  m_filters.push_back(Stage(filter));
}

void ChannelCache::updateTable() const {
  // Process the whole table with each filter, in turn
  m_table = std::for_each(m_filters.begin(), m_filters.end(),
                          lookup_table::BuildTable<Stage>(depth())).table();
  // Mark the table as up-to-date
  m_stale = false;
}

namespace channel_cache {

Push::Push(const FilterBase& filter) : m_filter(filter) {}

void Push::operator()(ChannelCache& channelCache) const {
  channelCache.push(std::auto_ptr<FilterBase>(m_filter.clone()));
}

} // namespace channel_cache
} // namespace filter
} // namespace image
} // namespace sedeen
