// Primary header
#include "image/filter/Filter.h"

#include <QtCore>

#include "image/filter/FilterBase.h"

namespace sedeen {
namespace image {
namespace filter {

Filter::Filter(std::auto_ptr<FilterBase> filter)
    : m_filterBase(filter) {
  if (0 == m_filterBase) {
    m_filterBase.reset(new NullFilter);
  }
}

unsigned int Filter::maxLevel() const {
  return m_filterBase->maxLevel();
}

int Filter::parameter() const {
  return m_filterBase->parameter();
}

void Filter::setParameter(int parameter) {
  ensureUnique();
  m_filterBase->setParameter(parameter);
}

int Filter::minParameter() const {
  return m_filterBase->minParameter();
}

int Filter::maxParameter() const {
  return m_filterBase->maxParameter();
}

int Filter::operator()(int value) const {
  return m_filterBase->filter(value);
}

void Filter::ensureUnique() {
  if (!m_filterBase.unique()) {
    m_filterBase.reset(m_filterBase->clone());
  }
}

} // namespace filter
} // namespace image
} // namespace sedeen
