// Primary header
#include "image/filter/Contrast.h"

#include <QtCore>
#include <cmath>

#include "image/filter/Global.h"

namespace sedeen {
namespace image {
namespace filter {

Contrast::Contrast(unsigned int nLevels)
    : FilterBase(nLevels),
      m_contrast(0),
      m_scale(1),
      m_mid(double(maxLevel()) / 2) {}

Contrast::~Contrast() {}

FilterBase* Contrast::doClone() const {
  return new Contrast(*this);
}

int Contrast::getParameter() const {
  return m_contrast;
}

void Contrast::doSetParameter(int p) {
  // Store the parameter value
  m_contrast = p;

  // Want to decrease the variation about zero
  double param = rescale(m_contrast,
                         minParameter(), maxParameter(),
                         -1, 1);
  param = std::pow(param, 3);
  m_scale = std::pow(double(maxLevel()), param);
}

int Contrast::getMinParameter() const {
  return -50;
}

int Contrast::getMaxParameter() const {
  return 50;
}

int Contrast::doFilter(int value) const {
  return qRound((double(value) - m_mid) * m_scale + m_mid);
}

} // namespace filter
} // namespace image
} // namespace sedeen
