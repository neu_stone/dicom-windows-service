#ifndef SEDEEN_SRC_IMAGE_FILTER_GLOBAL_ICC
#define SEDEEN_SRC_IMAGE_FILTER_GLOBAL_ICC

#include <QtCore>
#include <limits>

namespace sedeen {
namespace image {
namespace filter {

inline double rescale(double value, const double oldMin, const double oldMax,
                      const double newMin, const double newMax) {
  Q_ASSERT(oldMin < oldMax);
  Q_ASSERT(newMin < newMax);

  value -= oldMin;
  value /= (oldMax - oldMin);
  value *= (newMax - newMin);
  value += newMin;

  return value;
}

inline IndexGenerator::IndexGenerator()
    : m_index(-1) {}

inline int IndexGenerator::operator()() {
  Q_ASSERT(m_index != std::numeric_limits<int>::max());
  return ++m_index;
}

} // namespace filter
} // namespace image
} // namespace sedeen

#endif
