#ifndef SEDEEN_SRC_IMAGE_FILTER_CHANNELCACHE_H
#define SEDEEN_SRC_IMAGE_FILTER_CHANNELCACHE_H

#include <functional>
#include <vector>

#include "image/filter/LookupTable.h"
#include "image/filter/Stage.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {
namespace filter {

/// Computes the cumulative transformation of a number of filters.
//
/// The channel is assumed to be a single 8-bit integer value.
///
/// Currently, three filters are hard-coded: Gamma, Contrast, and Brightness, in that order. Each filter is applied in turn to an input value. Since we are working in the regime of discrete image levels, the transformation amounts to a mapping of [0,depth()) -> [0,depth()).
///
/// Internally administers a lookup table consolidating all installed filters. This table is updated a the first call to operator() after setParameter() has been called.
class SEDEEN_IMAGE_API ChannelCache : public std::unary_function<int,int> {
 public:
  /// Constructor
  ChannelCache();

  /// Return the number of values in a channel.
  unsigned int
  depth() const;

  /// Return the number of filter stages.
  unsigned int
  filters() const;

  /// Get the current filter parameter for a given stage.
  //
  /// \param filter
  /// The desired filter stage.
  ///
  /// \return
  /// The current parameter value. This value will be in the range [minParameter(),maxParameter()] for the given filter. Note that different filters may have different range limits on their parameters.
  int
  parameter(unsigned int filter) const;

  /// Set a specific stage's parameter.
  //
  /// Changing a filter's parameter will trigger an update to the cached filter values at the next invocation of operator().
  ///
  /// \param value
  /// The value of the parameter to set. This value must be within the range [minParameter(),maxParameter()] for the given filter. Note that different filters may have different range limits on their parameters.
  ///
  /// \param filter
  /// The desired filter stage.
  ///
  /// \return
  /// TRUE if this action changed the current parameter value; FALSE if \c value matched the previous parameter setting.
  bool
  setParameter(int value, unsigned int filter);

  /// Get the minimum allowed value of a filter's parameter.
  //
  /// \param filter
  /// The desired filter stage.
  ///
  /// Note that different filters may have different minimum parameter limits.
  int
  minParameter(unsigned int filter) const;

  /// Get the maximum allowed value of a filter's parameter.
  //
  /// \param filter
  /// The desired filter stage.
  ///
  /// Note that different filters may have different maximum parameter limits.
  int
  maxParameter(unsigned int filter) const;

  /// Compute the filtered value of the channel input.
  //
  /// If any filter parameters have been altered since the last call to this function, the filter table will be recalculated.
  ///
  /// \param inputValue
  /// The original image channel value to be filtered. Must be within the range [0,depth()).
  ///
  /// \return
  /// The filtered value. Will be within the range [0,depth()).
  int
  operator()(int inputValue) const;

  /// Insert a new filter layer.
  //
  /// The ChannelCache takes ownership of the passed object. Note that filters are applied in the order in which they are added.
  ///
  /// \param filter
  /// An auto_ptr to the requested filter. The filter must be in a default state, and all filtering operations must not change the input value. In addition, the depth of the added filter must match the depth of the current filters, except for the first filter added, which establishes the depth of the channel.
  void
  push(std::auto_ptr<FilterBase> filter);

 private:
  /// Recompute the filter mapping.
  void
  updateTable() const;

 private:
  /// Flag to track modifications to the contained filters.
  mutable bool m_stale;

  /// Local cache of the total transformation.
  mutable LookupTable m_table;

  /// Filter stages, held in order of application.
  std::vector<Stage> m_filters;

}; // class ChannelCache

/// Function objects related to ChannelCache.
namespace channel_cache {

class SEDEEN_IMAGE_API Push : public std::unary_function<ChannelCache,void> {
 public:
  Push(const FilterBase& filter);

  void operator()(ChannelCache& channelCache) const;

 private:
  const FilterBase& m_filter;
}; // class Push

} // namespace channel_cache
} // namespace filter
} // namespace image
} // namespace sedeen

#endif

#include "ChannelCache.icc"
