#ifndef SEDEEN_SRC_IMAGE_FILTER_CONTRAST_H
#define SEDEEN_SRC_IMAGE_FILTER_CONTRAST_H

#include "image/filter/FilterBase.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {
namespace filter {

class SEDEEN_IMAGE_API Contrast : public FilterBase {
 public:
  Contrast(unsigned int nLevels);

  virtual ~Contrast();

 private:
  virtual FilterBase* doClone() const;

  virtual int getParameter() const;

  virtual void doSetParameter(int parameter);

  virtual int getMinParameter() const;

  virtual int getMaxParameter() const;

  virtual int doFilter(int level) const;

 private:
  int m_contrast;

  double m_scale;

  const double m_mid;
}; // end class Contrast

} // namespace filter
} // namespace image
} // namespace sedeen

#endif
