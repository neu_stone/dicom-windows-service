#ifndef SEDEEN_SRC_IMAGE_FILTER_FILTER_H
#define SEDEEN_SRC_IMAGE_FILTER_FILTER_H

#include <functional>

#include <boost/shared_ptr.hpp>

#include "image/DllApi.h"

namespace sedeen {
namespace image {
namespace filter {

class FilterBase;

class SEDEEN_IMAGE_API Filter : public std::unary_function<int,int> {
 public:
  Filter(std::auto_ptr<FilterBase> filterBase);

  unsigned int maxLevel() const;

  int parameter() const;

  void setParameter(int parameter);

  int minParameter() const;

  int maxParameter() const;

  int operator()(int value) const;

 private:
  void ensureUnique();

 private:
  /// Shared implementation class
  boost::shared_ptr<FilterBase> m_filterBase;
}; // end class Filter

} // namespace filter
} // namespace image
} // namespace sedeen

#endif
