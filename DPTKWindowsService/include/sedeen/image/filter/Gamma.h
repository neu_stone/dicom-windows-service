#ifndef SEDEEN_SRC_IMAGE_FILTER_GAMMA_H
#define SEDEEN_SRC_IMAGE_FILTER_GAMMA_H

#include <QtGui>

#include "image/filter/FilterBase.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {
namespace filter {

class SEDEEN_IMAGE_API Gamma : public FilterBase {
 public:
  Gamma(unsigned int nLevels);

  virtual ~Gamma();

 private:
  virtual FilterBase* doClone() const;

  virtual int getParameter() const;

  virtual void doSetParameter(int parameter);

  virtual int doFilter(int level) const;

 private:
  double m_gamma;
}; // end class Gamma

} // namespace filter
} // namespace image
} // namespace sedeen

#endif
