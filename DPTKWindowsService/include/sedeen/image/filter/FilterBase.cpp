// Primary header
#include "image/filter/FilterBase.h"

#include <QtCore>
#include <algorithm>
#include <typeinfo>

#include "image/filter/Global.h"

namespace {

/// Default minimum parameter value
const int DEFAULT_MIN_PARAMETER = 0;

/// Default maximum parameter value
const int DEFAULT_MAX_PARAMETER = 100;

} // namespace

namespace sedeen {
namespace image {
namespace filter {

FilterBase::~FilterBase() {}

FilterBase* FilterBase::clone() const {
  FilterBase* aClone = doClone();
  Q_ASSERT(typeid(*aClone) == typeid(*this) &&
           "doClone incorrectly overridden");
  return aClone;
}

unsigned int FilterBase::maxLevel() const {
  return m_maxLevel;
}

int FilterBase::parameter() const {
  return getParameter();
}

void FilterBase::setParameter(const int parameter) {
  Q_ASSERT(parameter <= maxParameter());
  Q_ASSERT(parameter >= minParameter());

  doSetParameter(parameter);
}

int FilterBase::minParameter() const {
  return getMinParameter();
}

int FilterBase::maxParameter() const {
  return getMaxParameter();
}

int FilterBase::filter(int value) const {
  int filtered = doFilter(value);
  return std::min(std::max(filtered, 0), int(maxLevel()));
}

FilterBase::FilterBase(unsigned int nLevels)
    : m_maxLevel(nLevels-1) {
  Q_ASSERT(0 != nLevels);
}

FilterBase::FilterBase(const FilterBase& filter)
    : m_maxLevel(filter.m_maxLevel) {}

int FilterBase::getMinParameter() const {
  return DEFAULT_MIN_PARAMETER;
}

int FilterBase::getMaxParameter() const {
  return DEFAULT_MAX_PARAMETER;
}

namespace filter_base {

unsigned int depth(const FilterBase& filter) {
  return filter.maxLevel() + 1;
}

class FilteredValueDiffers : public std::unary_function<int,bool> {
 public:
  FilteredValueDiffers(const FilterBase& filter) : filter_(filter) {}
  bool operator()(int value) const {return value != filter_.filter(value);}
 private:
  const FilterBase& filter_;
}; // class FilteredValueDiffers

bool isIdentityTransform(const FilterBase& filter) {
  std::vector<int> inds;
  inds.reserve(depth(filter));
  std::generate(inds.begin(), inds.end(), IndexGenerator());
  return (std::find_if(inds.begin(), inds.end(), FilteredValueDiffers(filter))
          == inds.end());
}

} // namespace filter_base

NullFilter::NullFilter()
    : FilterBase(256) {}

FilterBase* NullFilter::doClone() const {
  return new NullFilter(*this);
}

int NullFilter::getParameter() const {
  return 0;
}

void NullFilter::doSetParameter(int) {}

int NullFilter::getMinParameter() const {
  return 0;
}

int NullFilter::getMaxParameter() const {
  return 0;
}

int NullFilter::doFilter(int level) const {
  return level;
}

} // namespace filter
} // namespace image
} // namespace sedeen
