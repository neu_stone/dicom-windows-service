#ifndef SEDEEN_SRC_IMAGE_FILTER_LOOKUPTABLE_H
#define SEDEEN_SRC_IMAGE_FILTER_LOOKUPTABLE_H

#include <vector>
#include <functional>

#include <boost/shared_ptr.hpp>

#include "image/DllApi.h"

namespace sedeen {
namespace image {
namespace filter {

/// A shared, copy-on-write lookup table of integer values.
class SEDEEN_IMAGE_API LookupTable {
 public:
  /// Create a new LookupTable of fixed size.
  //
  /// \param depth
  /// Number of entries in the table. Must be greater than zero.
  LookupTable(unsigned int depth = 0);

  /// Number of levels being translated.
  unsigned int depth() const;

  /// Translate one level to another.
  //
  /// \param level
  /// Input value. Must be in the range [0,depth()).
  ///
  /// \return
  /// Mapped value. Will be in the range [0,depth()).
  int operator[](int level) const;

  /// Resets the lookup table to an input==output state.
  //
  /// \post
  /// All calls to operator[] will return the input value.
  void reset();

  /// Apply a transformation to each of the contained values.
  //
  /// \param updateFunction
  /// A unary function, or function object that overloads operator(), that will accept and return values in the range [0,depth()).
  ///
  /// \post
  /// The function will have been applied to all elements contained in the LookupTable.
  template<typename T>
  void apply(T updateFunction);

 private:
  /// Shared memory containing the lookup table values.
  boost::shared_ptr<std::vector<int> > m_table;

  /// The size of the lookup table.
  unsigned int m_depth;
}; // end class LookupTable

/// Function objects related to LookupTable.
namespace lookup_table {

/// Computes the transform of a succession of Filter objects.
//
/// Builds the LookupTable resulting in applying a series of Fitler objects to a vector of level values.
template<typename T>
class BuildTable : public std::unary_function<T,void> {
 public:
  /// Construct a new aggregator.
  //
  /// \param depth
  /// Number of discrete values in the channel.
  ///
  /// \post
  /// Contains a default-constructed LookupTable of the given depth.
  BuildTable(unsigned int depth);

  /// Apply a filter to the LookupTable being built.
  //
  /// \param filter
  /// A function or function object overloading operator() used to apply a transfer function to each element of the LookupTable.
  void operator()(const T& filter);

  /// Return the current LookupTable.
  LookupTable table() const;

 private:
  /// Table to which each filter is applied.
  LookupTable m_table;
}; // end class BuildTable

} // namespace lookup_table
} // namespace filter
} // namespace image
} // namespace sedeen

#endif

#include "LookupTable.icc"
