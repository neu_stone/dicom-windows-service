// Primary header
#include "image/filter/FilterTool.h"

#include <limits>
#include <qtcolorpicker/src/qtcolorpicker.h>

#include "global/IntAdjustmentDelay.h"
#include "image/filter/FilterBase.h"
#include "image/filter/Global.h"
#include "image/filter/forms/ui_LevelsWidget.h"

namespace {

/// Only update the levels when the user pauses for a fifth of a second.
const int DELAY_MS = 200;

} // namespace

namespace sedeen {
namespace image {
namespace filter {

FilterTool::FilterTool(QWidget* parent, Qt::WindowFlags flags)
    : QWidget(parent, flags | Qt::Tool),
      m_levelsWidget(std::auto_ptr<Ui::levelsWidget>(new Ui::levelsWidget)),
      m_colorPicker(new QtColorPicker(this)),
      m_activeChannel(kR),
      m_allChannels(false),
      m_initialFilter(),
      m_filter() {
  m_levelsWidget->setupUi(this);
  setWindowIcon(QIcon(":/sedeen/icons/color.png"));

  m_levelsWidget->pseudocolourGroupBox->layout()->addWidget(m_colorPicker);

  connectBrightness();
  connectContrast();
  connectGamma();
  connectButtons();

  // Initially, start with all channels
  setAll(true);
}

FilterTool::~FilterTool() {}

void FilterTool::setFilter(const ImageFilter& filter) {
  m_initialFilter = filter;
  m_filter = filter;
  extractCurrentSettings();
}

void FilterTool::setBrightness(int brightness) {
  m_levelsWidget->brightnessSlider->setValue(brightness);
  updateFilter(kBrightness, brightness, brightness, brightness);
}

void FilterTool::setContrast(int contrast) {
  m_levelsWidget->contrastSlider->setValue(contrast);
  updateFilter(kContrast, contrast, contrast, contrast);
}

void FilterTool::setGamma(int gamma) {
  m_levelsWidget->gammaSlider->setValue(gamma);
  updateFilter(kGamma, gamma, gamma, gamma);
}

void FilterTool::setPseudocolor(const QColor& color) {
  m_colorPicker->setCurrentColor(color);
  updateFilter(kScale, color.red(), color.green(), color.blue());
}

void FilterTool::reset() {
  m_filter = ImageFilter();
  extractCurrentSettings();
  emit filterUpdated(m_filter);
}

void FilterTool::revert() {
  m_filter = m_initialFilter;
  extractCurrentSettings();
  emit filterUpdated(m_filter);
}

void FilterTool::cancel() {
  setVisible(false);
  revert();
  close();
}

void FilterTool::connectBrightness() {
  QSpinBox* spin = m_levelsWidget->brightnessSpinBox;
  spin->setMinimum(m_filter.minParameter(kBrightness));
  spin->setMaximum(m_filter.maxParameter(kBrightness));

  QSlider* slide = m_levelsWidget->brightnessSlider;
  slide->setMinimum(m_filter.minParameter(kBrightness));
  slide->setMaximum(m_filter.maxParameter(kBrightness));

  using global::IntAdjustmentDelay;
  IntAdjustmentDelay* timer = new IntAdjustmentDelay(DELAY_MS, this);
  connect(slide, SIGNAL(valueChanged(int)), timer, SLOT(setValue(int)));
  connect(timer, SIGNAL(update(int)), this, SLOT(setBrightness(int)));
}

void FilterTool::connectContrast() {
  QSpinBox* spin = m_levelsWidget->contrastSpinBox;
  spin->setMinimum(m_filter.minParameter(kContrast));
  spin->setMaximum(m_filter.maxParameter(kContrast));

  QSlider* slide = m_levelsWidget->contrastSlider;
  slide->setMinimum(m_filter.minParameter(kContrast));
  slide->setMaximum(m_filter.maxParameter(kContrast));

  using global::IntAdjustmentDelay;
  IntAdjustmentDelay* timer = new IntAdjustmentDelay(DELAY_MS, this);
  connect(slide, SIGNAL(valueChanged(int)), timer, SLOT(setValue(int)));
  connect(timer, SIGNAL(update(int)), this, SLOT(setContrast(int)));
}

void FilterTool::connectGamma() {
  QDoubleSpinBox* spin = m_levelsWidget->gammaDoubleSpinBox;
  spin->setMinimum(0);
  spin->setMaximum(1);

  QSlider* slide = m_levelsWidget->gammaSlider;
  slide->setMinimum(m_filter.minParameter(kGamma));
  slide->setMaximum(m_filter.maxParameter(kGamma));

  // Connect the slider and double spin box
  using filter_tool::GammaSync;
  GammaSync *sync = new GammaSync(slide->minimum(), slide->maximum(),
                                  spin->minimum(), spin->maximum(), this);
  connect(slide, SIGNAL(valueChanged(int)), sync, SLOT(set(int)));
  connect(sync, SIGNAL(update(int)), slide, SLOT(setValue(int)));
  connect(spin, SIGNAL(valueChanged(double)), sync, SLOT(set(double)));
  connect(sync, SIGNAL(update(double)), spin, SLOT(setValue(double)));

  using global::IntAdjustmentDelay;
  IntAdjustmentDelay* timer = new IntAdjustmentDelay(DELAY_MS, this);
  connect(slide, SIGNAL(valueChanged(int)), timer, SLOT(setValue(int)));
  connect(timer, SIGNAL(update(int)), this, SLOT(setGamma(int)));
}

void FilterTool::connectButtons() {
  connect(m_levelsWidget->redRadio, SIGNAL(toggled(bool)),
          this, SLOT(setR(bool)));
  connect(m_levelsWidget->greenRadio, SIGNAL(toggled(bool)),
          this, SLOT(setG(bool)));
  connect(m_levelsWidget->blueRadio, SIGNAL(toggled(bool)),
          this, SLOT(setB(bool)));
  connect(m_levelsWidget->allRadio, SIGNAL(toggled(bool)),
          this, SLOT(setAll(bool)));

  connect(m_levelsWidget->buttonBox->button(QDialogButtonBox::Reset),
          SIGNAL(clicked()), this, SLOT(reset()));
  connect(m_levelsWidget->buttonBox->button(QDialogButtonBox::Ok),
          SIGNAL(clicked()), this, SLOT(close()));
  connect(m_levelsWidget->buttonBox->button(QDialogButtonBox::Cancel),
          SIGNAL(clicked()), this, SLOT(cancel()));

  connect(m_colorPicker, SIGNAL(colorChanged(const QColor&)),
          this, SLOT(setPseudocolor(const QColor&)));
  connect(m_levelsWidget->pseudocolourGroupBox, SIGNAL(toggled(bool)),
          this, SLOT(updateColor(bool)));
}

void FilterTool::updateFilter(unsigned int filterStage, int r_param,
                              int g_param, int b_param) {
  bool rMod = updateChannel(filterStage, kR, r_param);
  bool gMod = updateChannel(filterStage, kG, g_param);
  bool bMod = updateChannel(filterStage, kB, b_param);

  if (rMod || gMod || bMod ) emit filterUpdated(m_filter);
}

bool FilterTool::updateChannel(unsigned int filterStage, unsigned int channel,
                               int parameter) {
  bool modified = false;
  if (channel == m_activeChannel || m_allChannels)
    modified = m_filter.setParameter(filterStage, channel, parameter);
  return modified;
}

void FilterTool::extractCurrentSettings() {
  setBrightness(m_filter.parameter(kBrightness, m_activeChannel));
  setContrast(m_filter.parameter(kContrast, m_activeChannel));
  setGamma(m_filter.parameter(kGamma, m_activeChannel));

  QColor color(m_filter.parameter(kScale, kR), m_filter.parameter(kScale, kG),
               m_filter.parameter(kScale, kB));
  setPseudocolor(color);
  m_levelsWidget->pseudocolourGroupBox->setChecked(QColor(Qt::white) != color);
}

void FilterTool::setChannel(unsigned int channel) {
  Q_ASSERT(channel < N_RGB);
  m_activeChannel = channel;
  extractCurrentSettings();
}

void FilterTool::setR(bool active) {
  if (active) {
    // In case the signal came from somewhere else than the GUI
    m_levelsWidget->redRadio->setChecked(true);
    setChannel(kR);
  }
}

void FilterTool::setG(bool active) {
  if (active) {
    // In case the signal came from somewhere else than the GUI
    m_levelsWidget->greenRadio->setChecked(true);
    setChannel(kG);
  }
}

void FilterTool::setB(bool active) {
  if (active) {
    // In case the signal came from somewhere else than the GUI
    m_levelsWidget->blueRadio->setChecked(true);
    setChannel(kB);
  }
}

void FilterTool::setAll(bool active) {
  if (active) {
    // In case the signal came from somewhere else than the GUI
    m_levelsWidget->allRadio->setChecked(true);
    m_allChannels = true;
  } else {
    m_allChannels = false;
  }
}

void FilterTool::updateColor(bool active) {
  if (active) {
    setPseudocolor(m_colorPicker->currentColor());
  } else {
    setPseudocolor(QColor(Qt::white));
  }
}

namespace filter_tool {

GammaSync::GammaSync(int minI, int maxI, double minD, double maxD,
                     QObject *parent)
    : QObject(parent),
      m_minI(minI),
      m_maxI(maxI),
      m_minD(minD),
      m_maxD(maxD) {
  Q_ASSERT(!(minI>maxI) && !(minD>maxD));
}

void GammaSync::set(int value) {
  emit update(rescale(value, m_minI, m_maxI, m_minD, m_maxD));
}

void GammaSync::set(double value) {
  emit update(qRound(rescale(value, m_minD, m_maxD, m_minI, m_maxI)));
}

} // namespace filter_tool

} // namespace filter
} // namespace image
} // namespace sedeen
