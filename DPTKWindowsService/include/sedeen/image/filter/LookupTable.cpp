// Primary header
#include "image/filter/LookupTable.h"

#include <QtCore>

#include "image/filter/Global.h"
#include "global/SrimgFunctions.h"

namespace sedeen {
namespace image {
namespace filter {

LookupTable::LookupTable(const unsigned int depth)
    : m_table(boost::shared_ptr<std::vector<int> >
              (new std::vector<int>(depth))),
      m_depth(depth) {
  Q_ASSERT(m_table);
  // Start off with a unique index in each entry.
  reset();
}

void LookupTable::reset() {
  Q_ASSERT(m_table);
  sedeen::ensureUnique(m_table);
  std::generate(m_table->begin(), m_table->end(), IndexGenerator());
}

} // namespace filter
} // namespace image
} // namespace sedeen
