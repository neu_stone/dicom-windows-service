#ifndef SEDEEN_SRC_IMAGE_FILTER_BRIGHTNESS_H
#define SEDEEN_SRC_IMAGE_FILTER_BRIGHTNESS_H

#include "image/filter/FilterBase.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {
namespace filter {

class SEDEEN_IMAGE_API Brightness : public FilterBase {
 public:
  Brightness(unsigned int nLevels);

  virtual ~Brightness();

 private:
  virtual FilterBase* doClone() const;

  virtual void doReset();

  virtual int getParameter() const;

  virtual void doSetParameter(int parameter);

  virtual int getMinParameter() const;

  virtual int getMaxParameter() const;

  virtual int doFilter(int level) const;

 private:
  int m_brightness;
}; // end class Brightness

} // namespace filter
} // namespace image
} // namespace sedeen

#endif
