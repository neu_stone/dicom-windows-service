#ifndef SEDEEN_SRC_IMAGE_FILTER_SCALE_H
#define SEDEEN_SRC_IMAGE_FILTER_SCALE_H

#include "image/filter/FilterBase.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {
namespace filter {

class SEDEEN_IMAGE_API Scale : public FilterBase {
 public:
  Scale(unsigned int nLevels);

  virtual ~Scale();

 private:
  virtual FilterBase* doClone() const;

  virtual int getParameter() const;

  virtual void doSetParameter(int parameter);

  virtual int getMinParameter() const;

  virtual int getMaxParameter() const;

  virtual int doFilter(int level) const;

 private:
  double m_scale;

  const int m_max;
}; // end class Scale

} // namespace filter
} // namespace image
} // namespace sedeen

#endif
