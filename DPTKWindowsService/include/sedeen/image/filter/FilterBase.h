#ifndef SEDEEN_SRC_IMAGE_FILTER_FILTERBASE_H
#define SEDEEN_SRC_IMAGE_FILTER_FILTERBASE_H

#include "image/DllApi.h"

namespace sedeen {
namespace image {
namespace filter {

/// Abstract base for image value filter objects.
class SEDEEN_IMAGE_API FilterBase {
 public:
  virtual ~FilterBase();

  FilterBase* clone() const;

  /// Return the maximum valid level
  unsigned int maxLevel() const;

  int parameter() const;

  void setParameter(int parameter);

  int minParameter() const;

  int maxParameter() const;

  int filter(int level) const;

 protected:
  explicit FilterBase(unsigned int nLevels);

  FilterBase(const FilterBase& filter);

 private:
  /// Disabled
  FilterBase& operator=(const FilterBase&);

  virtual FilterBase* doClone() const = 0;

  virtual int getParameter() const = 0;

  virtual void doSetParameter(int parameter) = 0;

  virtual int getMinParameter() const;

  virtual int getMaxParameter() const;

  virtual int doFilter(int level) const = 0;

 private:
  const unsigned int m_maxLevel;

}; // end class FilterBase

namespace filter_base {

/// Compute the number of discrete levels the filter is set for.
//
/// This is equivalent to FilterBase::maxLevel() + 1.
SEDEEN_IMAGE_API
unsigned int depth(const FilterBase& filter);

/// Test if filter does not modify the input.
//
/// Used for testing insert conditions.
SEDEEN_IMAGE_API
bool isIdentityTransform(const FilterBase& filter);

} // namespace filter_base

/// Null implementation of the filter.
//
/// Useful for default construction of handles, etc.
class SEDEEN_IMAGE_API NullFilter : public FilterBase {
 public:
  NullFilter();

 private:
  virtual FilterBase* doClone() const;

  virtual int getParameter() const;

  virtual void doSetParameter(int parameter);

  virtual int getMinParameter() const;

  virtual int getMaxParameter() const;

  virtual int doFilter(int level) const;
}; // end class NullFilter

} // namespace filter
} // namespace image
} // namespace sedeen

#endif
