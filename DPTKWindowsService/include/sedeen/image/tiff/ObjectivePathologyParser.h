#ifndef SEDEEN_SRC_IMAGE_TIFF_OBJECTIVEPATHOLOGYPARSER_H
#define SEDEEN_SRC_IMAGE_TIFF_OBJECTIVEPATHOLOGYPARSER_H

// System headers
#include <vector>

// User headers
#include "image/tiff/TIFFParser.h"
#include "image/tiff/TIFFReader.h"

namespace sedeen {
namespace image {

/// Contains Aperio specific details
class ObjectivePathologyParser : public TIFFParser {
 public:
  /// Attempts to open and read a SVS image with given \a filename.
  //
  /// \throws InvalidImage
  ///
  /// \throws FileAccessError
  ///
  /// \param filename
  /// the image to open
  explicit ObjectivePathologyParser(const char *filename);

  virtual ~ObjectivePathologyParser();

 private:
  /// Parses TIFF description tag for relevant information
  bool parseTiffDescription(TIFFReader reader);

  // virtual functions from TIFFParser
  virtual TIFFReader doGetReader() const;

  virtual const TIFFDirVector& doGetDirs() const;

  virtual int doGetLabelDir() const;

  virtual int doGetMacroDir() const;

  virtual int doGetThumbnailDir() const;

  // virtual functions from ImageParser
  virtual std::string doGetProducer() const;

  virtual SizeF doGetPixelSize() const;

  virtual double doGetMagnification() const;

  virtual ImagePyramidPtr doGetPyramid() const;

 private:
  typedef TIFFParser::TIFFDirVector TIFFDirVector;

  TIFFReader tif_;

  ImagePyramidPtr pyramid_;

  TIFFDirVector dirs_;

  int thumb_;

  int macro_;
};

}   // namespace image
}   // namespace sedeen

#endif

