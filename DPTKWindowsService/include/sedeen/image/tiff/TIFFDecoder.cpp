#include "image/tiff/TIFFDecoder.h"

#include <exception>

#include "global/geometry/Rect.h"
#include "image/io/DecodeRequest.h"
#include "image/tiff/StandardTIFFParser.h"
#include "image/tiff/TIFFPyramid.h"
#include "image/tiff/TIFFReader.h"

namespace sedeen {
namespace image {

TIFFDecoder::TIFFDecoder(const std::string& filename, QObject *parent)
    : SelectiveDecoder(ImageParserPtr(new StandardTIFFParser(filename.c_str())),
                       parent),
      reader_(new TIFFReader(parser().filename().c_str())) {
}

TIFFDecoder::TIFFDecoder(ImageParserPtr parser, QObject * parent)
    : SelectiveDecoder(parser, parent),
      reader_(new TIFFReader(parser->filename().c_str())) {
}

SelectiveDecoder::SelectiveDecoderPtr TIFFDecoder::clone() const {
  SelectiveDecoderPtr ptr;
  try {
    ptr.reset(new TIFFDecoder(filename(), parent()));
  }
  catch(const std::runtime_error&) {
  }
  return ptr;
}

TIFFDecoder::~TIFFDecoder() {
  /* Cleanup codec */
  waitForExit();
}

bool TIFFDecoder::canRead(const std::string &filename) {
  try {
    TIFFReader reader(filename.c_str());
    return true;
  }
  catch(...) {
    return false;
  }
}

RawImage TIFFDecoder::decodeRegion(const DecodeRequest &request) {
  const TIFFParser* tParser = static_cast<const TIFFParser*>(&parser());
  assert(tParser);

  const boost::uint16_t tdir = tParser->dirs()[request.resolution()];
  Rect region = request.region(request.resolution());

  return reader_->getRegionForDisplay(tdir, region);
}

} // namespace image
} // namespace sedeen
