#ifndef SEDEEN_SRC_IMAGE_TIFF_TIFFPYRAMID_H
#define SEDEEN_SRC_IMAGE_TIFF_TIFFPYRAMID_H

#include "image/io/ImagePyramid.h"

namespace sedeen {
namespace image {

/// Describes the tile pyramid structure for a multi-page TIFF image.
//
/// Note that resolution level (reslvl) 0 must the represents full resolution
/// image.
class TIFFPyramid : public ImagePyramid {
 public:
  /// Constructor
  //
  /// \param imageSize
  ///
  /// \param tileSize
  TIFFPyramid(const std::vector<Size>& imageSize, const Size& tileSize);

  virtual ~TIFFPyramid();

 protected:
  virtual int32_t doGetLevel(const double scale) const;

  virtual ImagePyramidPtr doGetClone() const;
};

} // namespace sedeen
} // namespace image

#endif
