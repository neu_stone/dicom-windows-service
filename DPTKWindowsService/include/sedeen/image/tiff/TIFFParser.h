#ifndef SEDEEN_SRC_IMAGE_TIFF_TIFFPARSER_H
#define SEDEEN_SRC_IMAGE_TIFF_TIFFPARSER_H

// System headers
#include <boost/cstdint.hpp>
#include <boost/shared_ptr.hpp>
#include <vector>

// user includes
#include "image/io/ImageParser.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {

class TIFFReader;

/// \class TIFFParser
/// \brief Looks at a TIFF file and determines its properties such
class SEDEEN_IMAGE_API TIFFParser : public ImageParser {
 public:
  /// directory vector type
  typedef std::vector<int> TIFFDirVector;

  typedef boost::shared_ptr<ImagePyramid> ImagePyramidPtr;

  /// Constructor
  TIFFParser();

  /// Destructor
  virtual ~TIFFParser();

  /// Gets a handle to the open file.
  TIFFReader reader() const;

  /// Gets the directories associated with each image in the pyramid.
  //
  /// The directories are arranged to match their respective resolution level
  /// (i.e. index 0 contains the directory with the largest image).
  ///
  /// \return
  /// TIFF direcotires for each resolution level.
  const TIFFDirVector& dirs() const;

  /// Returns the TIFF dir of the slide's label image.
  //
  /// \return
  /// -1 if one does not exist.
  ///
  /// \sa hasLabelImage()
  int labelDir() const;

  /// Returns the TIFF dir of the slide's macro image
  //
  /// \return
  /// -1 if one does not exist.
  ///
  /// \sa hasMacroImage()
  int macroDir() const;

  /// Returns the TIFF dir of the thumbnail image
  //
  /// \return
  /// -1 if one does not exist.
  ///
  /// \sa hasThumbnailImage()
  int thumbnailDir() const;

 private:
  // Implement some ImagePraser virtual methods
  virtual RawImage doGetLabelImage() const;
  virtual RawImage doGetThumbnailImage() const;
  virtual RawImage doGetMacroImage() const;
  virtual bool doGetHasLabelImage() const;
  virtual bool doGetHasMacroImage() const;
  virtual bool doGetHasThumbnailImage() const;
  virtual std::string doGetFilename() const;
  virtual Color doGetColor() const;
  virtual std::string doGetCompression() const;
  virtual std::string doGetDescription() const;
  virtual int doGetComponents() const;
  virtual int doGetBpp() const;
  virtual int doGetBpc(const int comp) const;

  virtual TIFFReader doGetReader() const = 0;

  virtual const TIFFDirVector& doGetDirs() const = 0;

  virtual int doGetLabelDir() const = 0;

  virtual int doGetMacroDir() const = 0;

  virtual int doGetThumbnailDir() const = 0;

 private:
  /// Deleted
  TIFFParser(const TIFFParser&);

  /// Deleted
  TIFFParser& operator=(const TIFFParser&);
};

/// Converts TIFF compression to human readable string
//
/// \param reader
///
/// \param dir
std::string getCompressionFromTiff(const TIFFReader& reader,
                                   boost::uint16_t dir);

/// Converts TIFF color type to RawImage color type
//
/// \param reader
///
/// \param dir
Color getColorFromTiff(const TIFFReader& reader, boost::uint16_t dir);

/// Converts TIFF Resolution and ResolutinUnit value to microns
//
/// \param reader
///
/// \param dir
SizeF getPixelSizeFromTiff(const TIFFReader& reader, boost::uint16_t dir);

/// Builds a TIFFImagePyramid.
//
/// \param dirs
/// List of the tiff directories used to generate the pyramid.
///
/// \param reader
///
/// \throws InvalidImage
TIFFParser::ImagePyramidPtr getTiffPyramid(
    const TIFFParser::TIFFDirVector& dirs,
    const TIFFReader& reader);

}   // namespace image
}   // namespace sedeen

#endif

