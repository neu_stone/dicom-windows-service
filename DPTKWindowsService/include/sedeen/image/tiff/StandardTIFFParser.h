#ifndef SEDEEN_SRC_IMAGE_TIFF_STANDARDTIFFPARSER_H
#define SEDEEN_SRC_IMAGE_TIFF_STANDARDTIFFPARSER_H

// System headers
#include <vector>

// User headers
#include "image/tiff/TIFFParser.h"
#include "image/tiff/TIFFReader.h"

namespace sedeen {
namespace image {

/// Contains generic tiff image details
//
/// \note
/// TIFF files do not contain information on image magnification.
///
/// Therefore a default value is calculated based on 20x being 0.5
/// micron_per_pixel and doGetHasMagnification() returns true if the image pixel
/// size is non-zero.
class StandardTIFFParser : public TIFFParser {
 public:
  typedef TIFFParser::TIFFDirVector TIFFDirVector;

  /// Attempts to open and read a TIFF image with given \a filename.
  //
  /// Throws a FileAccessError exception if the image cannot be read or
  /// an InvalidImage if the contents are not as expected.
  /// \param filename the image to open
  StandardTIFFParser(const char *filename);

  /// \brief closes the open file and frees resources.
  ~StandardTIFFParser();

 private:
  // virtual functions from TIFFParser
  virtual TIFFReader doGetReader() const;

  virtual const TIFFDirVector& doGetDirs() const;

  virtual int doGetLabelDir() const;

  virtual int doGetMacroDir() const;

  virtual int doGetThumbnailDir() const;

  // virtual functions from ImageParser
  virtual std::string doGetProducer() const;

  virtual SizeF doGetPixelSize() const;

  virtual double doGetMagnification() const;

  virtual ImagePyramidPtr doGetPyramid() const;

  /// \brief Read and sorts TIFF directories
  bool sortDirectories();

 private:
  TIFFReader tif_;

  ImagePyramidPtr pyramid_;

  TIFFDirVector dirs_;
};

} // namespace image
} // namespace sedeen

#endif
