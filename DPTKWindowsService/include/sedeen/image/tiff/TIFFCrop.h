#ifndef SEDEEN_SRC_IMAGE_TIFF_TIFFCROP_H
#define SEDEEN_SRC_IMAGE_TIFF_TIFFCROP_H

// System headers
#include <tiffio.h>

#include "image/DllApi.h"

namespace sedeen {
namespace image {

/* Structure to define in input image parameters
   for progressive encoding */
typedef struct tifhdr {

  tifhdr();

  /* TIFF file pointer */
  TIFF *tfile;

  /* Auto convert JPEG YCbCr to RGB
    If enabled tifGetRegion always return RGB for JPEG compression
    This is required for when vertical subsampling is in JPEG compression,
    otherwise libTIFF causes a crash in libJPEG
    */
  bool *forceRGBJPEG;

  /* Temporary buffer for progressive encoding */
  tdata_t buffer;

  uint32 bufx;
  uint32 bufy;
  uint32 bufw;
  uint32 bufh;

  /* TIFF File properties */
  uint16 ndirs;

  /* TIFF Directory specific properties */
  float *xres;
  float *yres;

  uint32 *imgw;
  uint32 *imgl;
  uint32 *tilew;
  uint32 *tilel;
  uint32 *ntiles;
  uint32 *rowsperstrip;

  uint16 *res_unit;
  uint16 *bps;
  uint16 *spp;
  uint16 *planar;
  uint16 *photometric;
  uint16 *hsubsampling;
  uint16 *vsubsampling;

  /* TIFF Directory specific properties
     YCC Color convertion values */
  float **lumaCoeff;
  float **refBlackWhite;

  /* TIFF Directory specific properties
     LAB Color convertion values */
  float **whitePoint;

} tifhdr_t;


/*
 * Public Function Declarations
 */
SEDEEN_IMAGE_API
tifhdr_t* tifOpen(const char *filename, const char *mode);

SEDEEN_IMAGE_API
void tifClose(tifhdr_t *tinfo);

SEDEEN_IMAGE_API
tdata_t tifGetTile(const tifhdr_t *tinfo, tdir_t tdir, uint32 x, uint32 y);

SEDEEN_IMAGE_API
tdata_t tifGetStrip(const tifhdr_t *tinfo, tdir_t tdir, uint32 row);

SEDEEN_IMAGE_API
tdata_t tifGetRegion(const tifhdr_t *tinfo, tdir_t tdir, uint32 x0, uint32 y0,
                     uint32 x1, uint32 y1);

SEDEEN_IMAGE_API
tdata_t tifGetRegionForDisplay(const tifhdr_t *tinfo, tdir_t tdir, uint32 x0,
                               uint32 y0, uint32 x1, uint32 y1);

SEDEEN_IMAGE_API
tdata_t tifConvertForDisplay(const tifhdr_t *tinfo, const tdir_t tdir,
                             const tdata_t buffer, const uint32 width,
                             const uint32 height);

SEDEEN_IMAGE_API
tdata_t tifGetComponent(uint32 cmptno, tdata_t srcbuf, uint32 bufw, uint32 bufh,
                        uint32 bufspp, uint32 bufbps);

SEDEEN_IMAGE_API
tdir_t tifGetLargestDirectory(TIFF *in);

SEDEEN_IMAGE_API
int tifWriteSetSize(tifhdr_t *tinfo, uint32 tilesize, uint32 rowsperstrip);

SEDEEN_IMAGE_API
int tifWriteSetCompression(tifhdr_t *tinfo, uint16 compression,
                           int jpegcolormode = JPEGCOLORMODE_RGB,
                           int jpegquality = 75);

SEDEEN_IMAGE_API
int writeCroppedImage(tifhdr_t *tin, tifhdr_t *tout, tdir_t tdir, uint32 x0,
                      uint32 y0, uint32 width, uint32 length);

int writeCroppedHeader(TIFF *in, TIFF *out, uint32 width, uint32 length);

} // end namespace image
} // end namespace sedeen

#endif  // Endif SEDEEN_SRC_IMAGE_TIFF_TIFFCROP_H

