// Main headers
#include "image/tiff/StandardTIFFParser.h"

#include <algorithm>

// User headers
#include "global/UnitDefs.h"
#include "global/geometry/Size.h"
#include "image/io/Exceptions.h"
#include "image/tiff/TIFFReader.h"

namespace sedeen {
namespace image {
namespace {

const std::string TIFF_PRODUCER("Unknown");

} // namespace

StandardTIFFParser::StandardTIFFParser(const char *filename)
    : TIFFParser(),
      tif_(filename),
      pyramid_(),
      dirs_() {
  // read and sort directories
  if (!sortDirectories())
    throw InvalidImage(filename,"No images found in file.");

  // create image pyramid -
  // thrown InvalidImage exception on error
  try {
    pyramid_ = getTiffPyramid(dirs_, tif_);
  }
  catch (InvalidPyramid &err) {
    throw InvalidImage(filename, err.what());
  }
}

StandardTIFFParser::~StandardTIFFParser() {
}

TIFFReader StandardTIFFParser::doGetReader() const {
  return tif_;
}

const StandardTIFFParser::TIFFDirVector& StandardTIFFParser::doGetDirs() const {
  return dirs_;
}

int StandardTIFFParser::doGetLabelDir() const {
  return -1;
}

int StandardTIFFParser::doGetMacroDir() const {
  return -1;
}

int StandardTIFFParser::doGetThumbnailDir() const {
  return -1;
}

std::string StandardTIFFParser::doGetProducer() const {
  return TIFF_PRODUCER;
}

SizeF StandardTIFFParser::doGetPixelSize() const {
  return getPixelSizeFromTiff(reader(), 0);
}

double StandardTIFFParser::doGetMagnification() const {
  return calculateDefaultMagnification(doGetPixelSize());
}

ImagePyramidPtr StandardTIFFParser::doGetPyramid() const {
  return pyramid_;
}

bool StandardTIFFParser::sortDirectories() {
  // assume all imagea are part of pyramid
  for (int dir = 0; dir < reader().numDirectories(); ++dir) {
    dirs_.push_back(dir);
  }

  // Sort directories according to widths in descending order
  TIFFReader &tiff_reader = tif_;
  std::sort(dirs_.begin(), dirs_.end(), [&] (int i, int j) {
      return tiff_reader.dimensions(i).width() >
          tiff_reader.dimensions(j).width();});

  return !dirs_.empty();
}

} // namespace image
} // namespace sedeen
