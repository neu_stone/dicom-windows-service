#ifndef SEDEEN_SRC_IMAGE_TIFF_TIFFWRITER_H
#define SEDEEN_SRC_IMAGE_TIFF_TIFFWRITER_H

#include <string>

#include <boost/cstdint.hpp>
#include <boost/smart_ptr/scoped_ptr.hpp>

#include "image/DllApi.h"

namespace sedeen {

class Rect;
class Size;

namespace image {

class ImageAccessor;

/// Simplifies writing of TIFF files
//
/// This class is not copyable since each instance is bound to a unique file.
/// However, the calss is threadsafe.
class SEDEEN_IMAGE_API TIFFWriter {
public:
  /// Opens a TIFF file for writing. Throws FileAccessError exception on error.
  //
  /// \param filename
  /// the file to open
  TIFFWriter(const std::string &filename);

  /// Destructor, close the file if open.
  ~TIFFWriter();

  /// Sets the abort flag and returns, see isDone()
  void abort();

  /// Get state of the abort flag
  bool aborted() const;

  /// Returns TRUE if the lats write operation succeedd
  bool success() const;

  /// Returns true if crop has completed or if it has not begun
  bool isDone() const;

  /// Reports progress 
  ///
  /// \return
  /// percentage of work done [0,100]
  int progress() const;

  /// Set the output format to be tiled, overrides setStripped()
  //
  /// \note
  /// Used by writeCroppedImage() to override the source image properties.
  ///
  /// \param tilesize
  /// the size of tiles in the output, 0 indicates no preference
  void setTiled(boost::uint32_t tilesize);

  /// Set the output format to be stripped, overrides setTiled()
  //
  /// \note
  /// Used by writeCroppedImage() to override the source image properties.
  ///
  /// \param enable
  /// TURE to enable stripped output, FALSE if no preference
  void setStripped(bool enable);

  /// Set the output format to be flat (i.e. non-pyramidal)
  //
  /// Crops from the largeset resolution and igonres all other resolutions.
  ///
  /// \param enable
  /// TURE to enable flat output, FALSE if no preference
  void setFlat(bool enable);

  ///  Set the output compression format. Default compression type is LZW.
  //
  /// \a jpegcolormore and jpegquality are used only if \a mode is specified as
  /// COMPRESSION_OJPEG or COMPRESSION_JPEG
  ///
  /// \note
  /// Used by writeCroppedImage() to override the source image properties
  ///
  /// \param mode
  /// TIFF defined values only
  ///
  /// \param jpegquality
  /// applies only for jpeg compression
  void setCompression(boost::uint16_t mode, int jpegquality = 75);

  /// Copies all images (if multi-resolution) from \a source to file.
  //
  /// If some or all of the output properties are not specified, \a source is
  /// as a reference.
  //
  /// File IO is perfomred ina seperate thread. See abort(), isDone() and 
  /// success()
  ///
  /// \note
  /// Multiple images can be writtin to one TIFF file, through this method.
  ///
  /// \param source
  /// the image to crop
  ///
  /// \param source_area
  /// the area to crop
  ///
  /// \param output_size
  /// specifies the output size of \a source_area if scaling is required. Must
  /// be valid (i.e. positive and non-zero dimensions)
  ///
  /// \return
  /// TRUE on sucess, FALSE otherwise
  void writeImage(const ImageAccessor &source,
                  const Rect &source_area);

  /// \copydoc WriteImage(ImageAccessor, const Rect&)
  ///
  /// \param output_size
  /// specifies the output size of \a source_area if scaling is required. Must
  /// be valid (i.e. positive and non-zero dimensions)
  void writeImage(const ImageAccessor &source, const Rect &source_area,
                  const Size &output_size);

 private:
  bool run(const ImageAccessor &source, const Rect &source_area,
                            const Size &output_size);

  bool writeResolution(ImageAccessor source, int resolution,
                       const Rect &source_area, const Size &output_size);

  bool writeHeader(const ImageAccessor &source, int resolution,
                   const Size &output_size);

  bool writeData(const ImageAccessor &source, const Rect &source_area, int resolution,
                 const Size &output_sizer);

  bool writeTiledData(const ImageAccessor &source, const Rect &source_area,
                      int resolution, const Size &output_size);

  bool writeStrippedData(const ImageAccessor &source, const Rect &source_area,
                         int resolution, const Size &output_size);
 private:
  class Private;

  /// Deleted
  TIFFWriter(const TIFFWriter &other);

  /// Deleted
  TIFFWriter& operator==(const TIFFWriter &rhs);

  boost::scoped_ptr<Private> d;
};

} // end namespace image
} // end namespace sedeen

#endif

