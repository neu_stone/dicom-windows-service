#include "image/tiff/OpenBigTiff.h"

#include "image/io/ImageIOTypes.h"
#include "image/aperio/AperioDecoder.h"
#include "image/leica/LeicaDecoder.h"
#include "image/tiff/ObjectivePathologyParser.h"
#include "image/tiff/TIFFDecoder.h"

namespace sedeen {
namespace image {

OpenBigTiff::~OpenBigTiff() {
}

std::auto_ptr<SelectiveDecoder> OpenBigTiff::getSelectiveDecoder(
    const std::string& filename) const {
  std::auto_ptr<SelectiveDecoder> dec;

  // try SVS - the specialized form of TIFF
  try {
    dec.reset(new AperioDecoder(filename));
    return dec;
  }
  catch(std::exception&) {
  }

  // try SCN - the specialized form of TIFF
  try {
    dec.reset(new LeicaDecoder(filename));
    return dec;
  }
  catch(std::exception&) {
  }

  // Try Objective Pathology TIFF
  try {
    boost::shared_ptr<TIFFParser> parser(
            new ObjectivePathologyParser(filename.c_str()));
    dec.reset(new AperioDecoder(parser));
    return dec;
  }
  catch(std::exception&) {
  }

  // Try standard TIFF
  try {
    dec.reset(new TIFFDecoder(filename));
    return dec;
  }
  catch(std::exception&) {
  }

  // Could not open the file
  return dec;
}

QStringList OpenBigTiff::getFilter() const {
  QStringList suf;
  suf << "TIFF (*.tif)";
  return suf;
}

} // namespace image
} // namespace sedeen
