// Main headers
#include "image/tiff/ObjectivePathologyParser.h"

// System headers
#include <sstream>
#include <cstring>

#include <boost/algorithm/string/predicate.hpp>

#include <tiffio.h>

// User headers
#include "global/UnitDefs.h"
#include "global/geometry/SizeF.h"
#include "image/io/Exceptions.h"
#include "image/tiff/TIFFReader.h"

namespace sedeen {
namespace image {
namespace {

const char* OP_FORMAT_STRING = "Objective Pathology";

} // namespace

ObjectivePathologyParser::ObjectivePathologyParser(const char *filename)
    : TIFFParser(),
      tif_(TIFFReader(filename)),
      pyramid_(),
      dirs_(),
      thumb_(-1),
      macro_(-1) {
  if (0 == tif_.numDirectories()) {
    throw InvalidImage(filename, "File does not contain an image");
  }

  // ensure aperio image
  if (!parseTiffDescription(tif_))
    throw InvalidImage(filename, "File does not appear to be in "
                       "Objective Patholoy format.");

  // create image pyramid -
  // thrown InvalidImage exception on error
  try {
    pyramid_ = getTiffPyramid(dirs_, tif_);
  }
  catch (InvalidPyramid &err) {
    throw InvalidImage(filename, err.what());
  }
}

ObjectivePathologyParser::~ObjectivePathologyParser() {
}

TIFFReader ObjectivePathologyParser::doGetReader() const {
  return tif_;
}

const ObjectivePathologyParser::TIFFDirVector&
ObjectivePathologyParser::doGetDirs() const {
  return dirs_;
}

int ObjectivePathologyParser::doGetLabelDir() const {
  return -1;
}

int ObjectivePathologyParser::doGetMacroDir() const {
  return macro_;
}

int ObjectivePathologyParser::doGetThumbnailDir() const {
  return thumb_;
}

std::string ObjectivePathologyParser::doGetProducer() const {
  return OP_FORMAT_STRING;
}

SizeF ObjectivePathologyParser::doGetPixelSize() const {
  return SizeF(0,0);
}

double ObjectivePathologyParser::doGetMagnification() const {
  return 0;
}

ImagePyramidPtr ObjectivePathologyParser::doGetPyramid() const {
  return pyramid_;
}

/*
  May contain an image labeled as macro
  The thumbnial image is non-tiled (assumed)
*/
bool ObjectivePathologyParser::parseTiffDescription(TIFFReader reader) {
  //Look in image description and find "Aperio"
  std::string description = reader.getTagString(0, TIFFTAG_IMAGEDESCRIPTION);
  if (!boost::contains(description, "Objective Pathology Services"))
    return false;

  // Find pyramid directories
  dirs_.push_back(0);
  for (int dir = 1; dir < reader.numDirectories(); ++dir) {

    //  Check for macro image
    description = reader.getTagString(dir, TIFFTAG_IMAGEDESCRIPTION);
    if (boost::icontains(description, "macro")) {
      macro_ = dir;
      continue;
    }

    // Check for thumbnail image
    if (reader.getTag32(dir, TIFFTAG_ROWSPERSTRIP) > 0) {
      thumb_ = dir;
      continue;
    }

    // part of image pyramid
    dirs_.push_back(dir);
  }

  return true;
}

} // namespace image
} // namespace sedeen
