// main header
#include "image/tiff/TIFFParser.h"

//system headers
#include <cassert>
#include <iostream>
#include <sstream>

#include <tiffio.h>

// User headers
#include "global/Debug.h"
#include "global/UnitDefs.h"
#include "global/geometry/Point.h"
#include "global/geometry/Rect.h"
#include "global/geometry/Size.h"
#include "image/io/Exceptions.h"
#include "image/buffer/RawImage.h"
#include "image/tiff/TIFFPyramid.h"
#include "image/tiff/TIFFReader.h"

namespace sedeen {
namespace image {
namespace {

double convertToMicron(double res, int unit) {
  // prevent division by zero
  if (0 == res) return 0;

  // convert to metric
  switch (unit) {
    case RESUNIT_INCH:
      return metricToMetric(1.0/res, sedeen::length::inch,
                            sedeen::length::um);

    case RESUNIT_CENTIMETER:
      return metricToMetric(1.0/res, sedeen::length::cm, sedeen::length::um);

    case RESUNIT_NONE:
      // Fall-through
    default:
      debug::warning(
          "Invalid TIFF RESOLUTION_UNIT'. Defaulting to inches",
          __FUNCTION__, __LINE__);
      return metricToMetric(1.0/res, sedeen::length::inch,
                            sedeen::length::um);
  }
}

} // namespace

TIFFParser::TIFFParser() : ImageParser() {
}

TIFFParser::~TIFFParser() {
}

TIFFReader TIFFParser::reader() const {
  return doGetReader();
}

const TIFFParser::TIFFDirVector& TIFFParser::dirs() const {
  assert(doGetDirs().size() > 0);
  return doGetDirs();
}

int TIFFParser::thumbnailDir() const {
  assert(doGetThumbnailDir() >= -1);
  return doGetThumbnailDir();
}

int TIFFParser::labelDir() const {
  assert(doGetLabelDir() >= -1);
  return doGetLabelDir();
}

int TIFFParser::macroDir() const {
  assert(doGetMacroDir() >= -1);
  return doGetMacroDir();
}

bool TIFFParser::doGetHasLabelImage() const {
  return doGetLabelDir() != -1;
}

bool TIFFParser::doGetHasMacroImage() const {
  return doGetMacroDir() != -1;
}

bool TIFFParser::doGetHasThumbnailImage() const {
  return doGetThumbnailDir() != -1;
}

RawImage TIFFParser::doGetLabelImage() const {
  if (hasLabelImage()) {
    Rect region(Point(0,0), reader().dimensions(labelDir()));
    RawImage img = reader().getRegionForDisplay(labelDir(), region);
    assert(!img.isNull());
    return img;
  }
  return RawImage();
}

RawImage TIFFParser::doGetThumbnailImage() const {
  if (hasThumbnailImage()) {
    Rect region(Point(0,0), reader().dimensions(thumbnailDir()));
    RawImage img = reader().getRegionForDisplay(thumbnailDir(), region);
    assert(!img.isNull());
    return img;
  }
  return RawImage();
}

RawImage TIFFParser::doGetMacroImage() const {
  if (hasMacroImage()) {
    Rect region(Point(0,0), reader().dimensions(macroDir()));
    RawImage img = reader().getRegionForDisplay(macroDir(), region);
    assert(!img.isNull());
    return img;
  }
  return RawImage();
}

std::string TIFFParser::doGetFilename() const {
  return reader().filename();
}

Color TIFFParser::doGetColor() const {
  int dir = dirs()[0];
  return getColorFromTiff(reader(), dir);
}

std::string TIFFParser::doGetCompression() const {
  int dir = dirs()[0];
  return getCompressionFromTiff(reader(), dir);
}

std::string TIFFParser::doGetDescription() const {
  int dir = dirs()[0];
  return reader().getTagString(dir, TIFFTAG_IMAGEDESCRIPTION);
}

int TIFFParser::doGetComponents() const {
  int dir = dirs()[0];
  return reader().getTag16(dir, TIFFTAG_SAMPLESPERPIXEL);
}

int TIFFParser::doGetBpp() const {
  return doGetComponents() * doGetBpc(0);
}

int TIFFParser::doGetBpc(int comp) const {
  assert(doGetComponents() > comp);
  int dir = dirs()[0];
  return reader().getTag16(dir, TIFFTAG_BITSPERSAMPLE);
}

SizeF getPixelSizeFromTiff(const TIFFReader& reader, boost::uint16_t dir) {
  assert(reader.numDirectories() > dir);
  float sizex = reader.getTagFloat(dir, TIFFTAG_XRESOLUTION);
  float sizey = reader.getTagFloat(dir, TIFFTAG_YRESOLUTION);
  int unit = reader.getTag16(dir, TIFFTAG_RESOLUTIONUNIT);
  return SizeF(convertToMicron(sizex, unit), convertToMicron(sizey, unit));
}

Color getColorFromTiff(const TIFFReader& reader, boost::uint16_t dir) {
  assert(reader.numDirectories() > dir);
  const uint16_t spp = reader.getTag16(dir, TIFFTAG_SAMPLESPERPIXEL);
  const uint16_t bps = reader.getTag16(dir, TIFFTAG_BITSPERSAMPLE);

  // Convert color
  switch (reader.getTag16(dir, TIFFTAG_PHOTOMETRIC)) {

    case PHOTOMETRIC_MINISBLACK:
      // Fall-through
    case PHOTOMETRIC_MINISWHITE:
      assert(8==bps || 16==bps);
      if (8==bps)
        return Gray_8;
      else
        return Gray_16;

    case PHOTOMETRIC_RGB:
      assert(3==spp || 4==spp);
      assert(8==bps || 16==bps);
      debug::warning_x(4==spp, "4 sample-per-pixel RGB image assumed to be ARGB",
                        __FUNCTION__, __LINE__);
      if (8==bps && 4==spp)
        return ARGB_8;
      else if (8==bps && 3==spp)
        return RGB_8;
      else if (4==spp)
        return ARGB_16;
      else
        return RGB_16;

    case PHOTOMETRIC_YCBCR:
      // Check for JPEG color conversions
      if (reader.getTag32(dir, TIFFTAG_JPEGCOLORMODE) == JPEGCOLORMODE_RGB) {
        return RGB_8;
      } else {
        debug::assert_x(false, "TIFFReader should always return RGB JPEG",
                        __FUNCTION__, __LINE__);
        return Unknown;
      }

    case PHOTOMETRIC_MASK:
    default:
      return Unknown;
  }
}

std::string getCompressionFromTiff(const TIFFReader& reader,
                                   boost::uint16_t dir) {
  assert(reader.numDirectories() > dir);

  // get compression
  switch(reader.getTag16(dir, TIFFTAG_COMPRESSION)) {
    case COMPRESSION_NONE:
      return "Uncompressed";

    case COMPRESSION_CCITTRLE:
      return "CCITT modified Huffman RLE";

    case COMPRESSION_CCITTFAX3:
      return "CCITT Group 3 fax encoding";

    case COMPRESSION_CCITTFAX4:
      return "CCITT Group 4 fax encoding";

    case COMPRESSION_LZW:
      return "LZW";

    case COMPRESSION_OJPEG:
      return "OJPEG";

    case COMPRESSION_JPEG:
      return "JPEG (old)";

    case COMPRESSION_NEXT:
      return "JPEG";

    case COMPRESSION_CCITTRLEW:
      return "CCIT RLE W";

    case COMPRESSION_PACKBITS:
      return "Pack Bits";

    case COMPRESSION_THUNDERSCAN:
      return "ThunderScan";

    case COMPRESSION_IT8CTPAD:
      return "IT8CTPAD";

    case COMPRESSION_IT8LW:
      return "IT8LW";

    case COMPRESSION_IT8MP:
      return "IT8MP";

    case COMPRESSION_IT8BL:
      return "IT8BL";

    case COMPRESSION_PIXARFILM:
      return "Pixar Film";

    case COMPRESSION_PIXARLOG:
      return "Pixar LOG";

    case COMPRESSION_DEFLATE:
      return "Deflate";

    case COMPRESSION_ADOBE_DEFLATE:
      return "Adobe Deflate";

    case COMPRESSION_DCS:
      return "DCS";

    case COMPRESSION_JBIG:
      return "JBIG";

    case COMPRESSION_SGILOG:
      return "SGI LOG";

    case COMPRESSION_SGILOG24:
      return "SGI LOG24";

    case COMPRESSION_JP2000:
      return "JPEG-2000";

    default:
      return "Unknown";
  }
}

TIFFParser::ImagePyramidPtr getTiffPyramid(
    const TIFFParser::TIFFDirVector &dirs,
    const TIFFReader& reader) {
  typedef TIFFParser::ImagePyramidPtr ImagePyramidPtr;

  assert(dirs.size());
  assert(reader.numDirectories() > *std::max_element(dirs.begin(),dirs.end()));

  // Get image tile size
  const int tdir = dirs[0];
  Size tilesize = reader.tileSize(tdir);

  // for un-tiled images, assign fixed tile size
  if (isEmpty(tilesize)) {
    const Size image_dims = reader.dimensions(tdir);
    int rows_per_strip = reader.getTag32(tdir, TIFFTAG_ROWSPERSTRIP);

    tilesize.setWidth(image_dims.width());
    tilesize.setHeight(std::min(rows_per_strip, image_dims.height()));
  }
  assert(!isEmpty(tilesize));

  // get image widths and height
  std::vector<Size> dims;
  for (unsigned int i = 0; i < dirs.size(); ++i) {
    dims.push_back(reader.dimensions(dirs[i]));
  }

  // Return a pyramid
  try {
    return ImagePyramidPtr(new TIFFPyramid(dims, tilesize));
  }
  catch (InvalidPyramid &err) {
    throw InvalidImage("", std::string("Could not create suitable image "
                                       "pyramid (") +
                       err.what() +
                       std::string(")"));
  }
}

}   // namespace image
}   // namespace sedeen
