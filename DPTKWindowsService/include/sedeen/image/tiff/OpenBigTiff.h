#ifndef SEDEEN_SRC_IMAGE_TIFF_OPENBIGTIFF_H
#define SEDEEN_SRC_IMAGE_TIFF_OPENBIGTIFF_H

#include <memory>

#include <QtCore>

#include "image/io/OpenTiledImage.h"

namespace sedeen {
namespace image {

class SelectiveDecoder;

/// Create an ImageData class for a big-TIFF image.
class OpenBigTiff : public OpenTiledImage {
 public:
  virtual ~OpenBigTiff();

 private:
  virtual std::auto_ptr<SelectiveDecoder> getSelectiveDecoder(
      const std::string& filename) const;

  virtual QStringList getFilter() const;
}; // end class OpenBigTiff

} // end namespace image
} // end namespace sedeen

#endif
