#ifndef SEDEEN_SRC_IMAGE_TIFF_TIFFDECODER_H
#define SEDEEN_SRC_IMAGE_TIFF_TIFFDECODER_H

#include <map>

#include <QtCore>

#include <boost/smart_ptr/scoped_ptr.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include "image/io/SelectiveDecoder.h"

namespace sedeen {
namespace image {

class TIFFReader;

/// TIFF decoder class implemented in Qt.
//
/// It selectively decodes TIFF images from disk using the libTiff library
/// (v4.0.0 or later).  See SelectiveDecoder for more details.
class TIFFDecoder : public SelectiveDecoder {
  Q_OBJECT;

 public:
  /// Creates a decoder for the given image.
  //
  /// \sa getRegion()
  ///
  /// \throws FileAccessError
  ///
  /// \throws InvalidImage
  ///
  /// \param filename
  ///
  /// \param parent
  explicit TIFFDecoder(const std::string& filename, QObject * parent = 0);

  /// Overloaded constructor.
  //
  /// \param parser
  /// The image parser
  ///
  /// \param parent
  explicit TIFFDecoder(ImageParserPtr parser, QObject * parent = 0);

  /// Closes the image and releases all resources.
  ~TIFFDecoder();

  virtual SelectiveDecoderPtr clone() const;

  /// Checks to make sure file is readable.
  //
  /// The file must be readable, otherwise, return value is FALSE.
  ///
  /// \param filename
  /// The name of the file to check
  ///
  /// \return
  /// \c true if readable, \c false otherwise
  static bool canRead(const std::string &filename);

 protected:
  virtual RawImage decodeRegion (const DecodeRequest &request);

 private:
  /// Deleted
  TIFFDecoder(const TIFFDecoder& decoder);

  /// Deleted
  TIFFDecoder& operator=(const TIFFDecoder&);

  /// Get TIFF pixel spacing info from the given directory.
  //
  /// \param dir
  void getPixelSpacing(uint16_t dir);

 private:
  boost::scoped_ptr<TIFFReader> reader_;
};

} // namespace image
} // namespace sedeen

#endif
