#include "image/tiff/TIFFPyramid.h"

#include <boost/make_shared.hpp>

namespace sedeen {
namespace image {

TIFFPyramid::TIFFPyramid(const std::vector<Size>& imageSize,
                         const Size& tileSize)
    : ImagePyramid(imageSize, tileSize) {
}

TIFFPyramid::~TIFFPyramid() {
}

int32_t TIFFPyramid::doGetLevel(const double scl) const {
  if (1.0 > scl) {
    for (int32_t lvl = levels()-1; lvl >= 0; --lvl) {
      if (scale(lvl).width() >= scl) {
        return lvl;
      }
    }
  }
  return 0;
}

ImagePyramidPtr TIFFPyramid::doGetClone() const {
  return boost::make_shared<TIFFPyramid>(sizeVec(), tileSize());
}

}   // namespace image
}   // namespace sedeen
