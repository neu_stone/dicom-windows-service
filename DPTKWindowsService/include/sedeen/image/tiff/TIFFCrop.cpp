#include "image/tiff/TIFFCrop.h"

#include <cstdlib>
#include <cassert>
#include <cstring>
#include <iostream>

#include "global/Debug.h"

namespace sedeen {
namespace image {

tifhdr::tifhdr()
    : tfile(),
      buffer(),
      bufx(),
      bufy(),
      bufw(),
      bufh(),
      ndirs(),
      xres(),
      yres(),
      imgw(),
      imgl(),
      tilew(),
      tilel(),
      ntiles(),
      rowsperstrip(),
      res_unit(),
      bps(),
      spp(),
      planar(),
      photometric(),
      hsubsampling(),
      vsubsampling(),
      lumaCoeff(),
      refBlackWhite(),
      whitePoint() {}

/*
  Static Function Declarations
*/
static bool copyTileToBuffer(const tifhdr_t *tinfo, tdir_t tdir,
                          uint32 tx, uint32 ty,
                          tdata_t buf,
                          uint32 bufx, uint32 bufy, uint32 bufw, uint32 bufh);
static bool
tifCopyStripToBuffer(const tifhdr_t *tinfo, tdir_t tdir, uint32 row,
                      tdata_t buf,
                      uint32 bufx, uint32 bufy, uint32 bufw, uint32 bufh);
tdata_t tifReadTile(const tifhdr_t *tinfo, tdir_t tdir, uint32 x, uint32 y);
tdata_t tifReadStrip(const tifhdr_t *tinfo, tdir_t tdir, uint32 row);
static tdata_t tifReadTilePart(const tifhdr_t *tinfo, tdir_t tdir,
            uint32 x, uint32 y, uint32 z, tsample_t sample);
static tdata_t tifReadStripPart(const tifhdr_t *tinfo, tdir_t tdir,
                  uint32 row, tsample_t sample);
static tdata_t getRegionFromTiles(const tifhdr_t *tinfo, tdir_t tdir, uint32 x0,
                                  uint32 y0, uint32 x1, uint32 y1);
static tdata_t getRegionFromStrips(const tifhdr_t *tinfo, tdir_t tdir, uint32 x0,
                                   uint32 y0, uint32 x1, uint32 y1);
static bool isRequestValid(const tifhdr_t *tinfo, tdir_t tdir);
static tifhdr_t* tifReadHeader(TIFF *in);
static tdata_t bufToUnitSampling(tdata_t buf, uint32 bufw, uint32 bufh,
                                 uint32 hs, uint32 vs, uint32 photometric);
static tdata_t tifYccToRgb(const tifhdr_t *tinfo, tdir_t tdir, tdata_t data,
                           uint32 bufw, uint32 bufh);
static tdata_t tifLabToRgb(const tifhdr_t *tinfo, tdir_t tdir, tdata_t data,
                           uint32 bufw, uint32 bufh);
static tdata_t tifPaletteToRGB(const tifhdr_t *tinfo, tdir_t tdir, tdata_t data,
                               uint32 bufw, uint32 bufh);
static tdata_t tif8bitScaling(const tifhdr_t *tinfo, tdir_t tdir, tdata_t data,
                              uint32 bufw, uint32 bufh);
static inline void copyBufferValue(tdata_t src, uint32 srci, tdata_t dest,
                                   uint32 desti, uint16 bps);
static inline int32 getSignedValue(tdata_t buffer, uint32 index, uint32 bps);
static inline uint32 getUnsignedValue(tdata_t buffer, uint32 index, uint32 bps);
static void cpTag(TIFF* in, TIFF* out, uint16 tag, uint16 count,
                  TIFFDataType type);

/*
  Some math operations
*/
#define tifDivCeil(a,b)  ( ((a) + ((b)-1) ) / (b) )
#define tifDivFloor(a,b) ( ((a) / (b))    )
#define tifMin(a,b)   ( (a) < (b) ? (a) : (b) )
#define tifMax(a,b)   ( (a) > (b) ? (a) : (b) )

/*
  Other math operations
  Copied and renamed from tiffio.h
  needed to calculate some structure sizes
*/
#define tifHowMany_32(x, y) ((((uint32)(x))+(((uint32)(y))-1))/((uint32)(y)))
#define tifRoundUp_32(x, y) (tifHowMany_32(x,y)*(y))

/*
  Tiff tag operations
*/
#define CopyField(tag, v)                                       \
  if (TIFFGetField(in, tag, &v)) TIFFSetField(out, tag, v)
#define CopyField2(tag, v1, v2)                                         \
  if (TIFFGetField(in, tag, &v1, &v2)) TIFFSetField(out, tag, v1, v2)
#define CopyField3(tag, v1, v2, v3)                                     \
  if (TIFFGetField(in, tag, &v1, &v2, &v3)) TIFFSetField(out, tag, v1, v2, v3)
#define CopyField4(tag, v1, v2, v3, v4)                                 \
  if (TIFFGetField(in, tag, &v1, &v2, &v3, &v4)) TIFFSetField(out, tag, v1, v2, v3, v4)
#define CopyTag(tag, count, type) cpTag(in, out, tag, count, type)

static void cpTag(TIFF* in, TIFF* out, uint16 tag, uint16 count, TIFFDataType type)
{
  switch (type) {
    case TIFF_SHORT:
      if (count == 1) {
        uint16 shortv;
        CopyField(tag, shortv);
      } else if (count == 2) {
        uint16 shortv1, shortv2;
        CopyField2(tag, shortv1, shortv2);
      } else if (count == 4) {
        uint16 *tr, *tg, *tb, *ta;
        CopyField4(tag, tr, tg, tb, ta);
      } else if (count == (uint16) -1) {
        uint16 shortv1;
        uint16* shortav;
        CopyField2(tag, shortv1, shortav);
      }
      break;
    case TIFF_LONG:
      { uint32 longv;
        CopyField(tag, longv);
      }
      break;
    case TIFF_RATIONAL:
      if (count == 1) {
        float floatv;
        CopyField(tag, floatv);
      } else if (count == (uint16) -1) {
        float* floatav;
        CopyField(tag, floatav);
      }
      break;
    case TIFF_ASCII:
      { char* stringv;
        CopyField(tag, stringv);
      }
      break;
    case TIFF_DOUBLE:
      if (count == 1) {
        double doublev;
        CopyField(tag, doublev);
      } else if (count == (uint16) -1) {
        double* doubleav;
        CopyField(tag, doubleav);
      }
      break;
    default:
      TIFFError(TIFFFileName(in),
                "Data type %d is not supported, tag %d skipped.",
                tag, type);
  }
}
static struct cpTag {
  uint16 tag;
  uint16 count;
  TIFFDataType type;
} tags[] = {
  { TIFFTAG_SUBFILETYPE,  1, TIFF_LONG },
  { TIFFTAG_THRESHHOLDING, 1, TIFF_SHORT },
  { TIFFTAG_DOCUMENTNAME,  1, TIFF_ASCII },
  { TIFFTAG_IMAGEDESCRIPTION, 1, TIFF_ASCII },
  { TIFFTAG_MAKE,   1, TIFF_ASCII },
  { TIFFTAG_MODEL,  1, TIFF_ASCII },
  { TIFFTAG_MINSAMPLEVALUE, 1, TIFF_SHORT },
  { TIFFTAG_MAXSAMPLEVALUE, 1, TIFF_SHORT },
  { TIFFTAG_XRESOLUTION,  1, TIFF_RATIONAL },
  { TIFFTAG_YRESOLUTION,  1, TIFF_RATIONAL },
  { TIFFTAG_PAGENAME,  1, TIFF_ASCII },
  { TIFFTAG_XPOSITION,  1, TIFF_RATIONAL },
  { TIFFTAG_YPOSITION,  1, TIFF_RATIONAL },
  { TIFFTAG_RESOLUTIONUNIT, 1, TIFF_SHORT },
  { TIFFTAG_SOFTWARE,  1, TIFF_ASCII },
  { TIFFTAG_DATETIME,  1, TIFF_ASCII },
  { TIFFTAG_ARTIST,  1, TIFF_ASCII },
  { TIFFTAG_HOSTCOMPUTER,  1, TIFF_ASCII },
  { TIFFTAG_WHITEPOINT,  (uint16) -1, TIFF_RATIONAL },
  { TIFFTAG_PRIMARYCHROMATICITIES,(uint16) -1,TIFF_RATIONAL },
  { TIFFTAG_HALFTONEHINTS, 2, TIFF_SHORT },
  { TIFFTAG_INKSET,  1, TIFF_SHORT },
  { TIFFTAG_DOTRANGE,  2, TIFF_SHORT },
  { TIFFTAG_TARGETPRINTER, 1, TIFF_ASCII },
  { TIFFTAG_SAMPLEFORMAT,  1, TIFF_SHORT },
  { TIFFTAG_YCBCRCOEFFICIENTS, (uint16) -1,TIFF_RATIONAL },
  { TIFFTAG_YCBCRSUBSAMPLING, 2, TIFF_SHORT },
  { TIFFTAG_YCBCRPOSITIONING, 1, TIFF_SHORT },
  { TIFFTAG_REFERENCEBLACKWHITE, (uint16) -1,TIFF_RATIONAL },
  { TIFFTAG_EXTRASAMPLES,  (uint16) -1, TIFF_SHORT },
  { TIFFTAG_SMINSAMPLEVALUE, 1, TIFF_DOUBLE },
  { TIFFTAG_SMAXSAMPLEVALUE, 1, TIFF_DOUBLE },
  { TIFFTAG_STONITS,  1, TIFF_DOUBLE },
};
#define NTAGS (sizeof (tags) / sizeof (tags[0]))

/**
 * Returns one component from the buffer.
 * Assumes buffer the pixels in the data buffer are
 * organized contigeous planar order ie. RGB  RGB RGB ...
 * The number of components and bits per component can vary.
 *
 * @param  cmptno the component to extract
 * @param  srcbuf the buffer with contiguous data
 * @param  bufw the width of scrbuf in pixels
 * @param  bufh the hieght of srcbuf in pixels
 * @param  bufspp the number of samples per buffer pixel
 * @param  bufbps the number of bits per buffer sample [8,16, or 32]
 * @return  pointer to requested component or 0 on error
 */
tdata_t tifGetComponent(uint32 cmptno, tdata_t srcbuf, uint32 bufw, uint32 bufh, uint32 bufspp, uint32 bufbps)
{
  uint32 si, di, bytes;
  tdata_t destbuf  = NULL;

  /* Error check */
  if( cmptno >= bufspp ) {
    printf("Error: Invalid sample requested\n");
    return 0;
  }

  /* Allocate return destination buffer */
  bytes = (bufbps + 7) >> 3;
  destbuf = _TIFFmalloc( (tsize_t)(bufw) * bufh  * bytes * bufspp );


  /* copy the component into the matrix */
  for(si=cmptno, di=0; si<bufh*bufw; si+=bufspp, di++) {
    switch( bufbps ) {
      case 8:  ((uint8 *)destbuf)[di] = ((uint8 *)srcbuf)[si]; break;
      case 16: ((uint16*)destbuf)[di] = ((uint16*)srcbuf)[si]; break;
      case 32: ((uint32*)destbuf)[di] = ((uint32*)srcbuf)[si]; break;
      default:
        _TIFFfree(destbuf);
        return 0;
        break;
    }
  }

  return destbuf;
}



/**
 * Opens a TIFF file.
 *
 * @param filename  filename name of file to open
 * @param mode
 * @return   pointe to tifhdr_t structure or 0 on error
 **/
tifhdr_t* tifOpen(const char *filename, const char * mode) {
  tifhdr_t* tinfo;
  TIFF*  in;

  /* Open TIFF file */
  if( !(in = TIFFOpen(filename, mode)) ) {
    return 0;
  }

  /* Create TIFF Structure */
  if( !(tinfo = tifReadHeader(in)) ) {
    return 0;
  }

  return tinfo;
}


void tifClose(tifhdr_t *tinfo)
{
  if( !tinfo ) {
    return;
  }
  if( tinfo->tfile ) {
    TIFFClose( tinfo->tfile );
  }
  if( tinfo->buffer ) {
    _TIFFfree(tinfo->buffer);
  }

  free(tinfo->forceRGBJPEG);
  free(tinfo->xres);
  free(tinfo->yres);
  free(tinfo->imgw);
  free(tinfo->imgl);
  free(tinfo->tilew);
  free(tinfo->tilel);
  free(tinfo->ntiles);
  free(tinfo->rowsperstrip);
  free(tinfo->res_unit);
  free(tinfo->bps);
  free(tinfo->spp);
  free(tinfo->planar);
  free(tinfo->photometric );
  free(tinfo->hsubsampling);
  free(tinfo->vsubsampling);
  free(tinfo->lumaCoeff);
  free(tinfo->refBlackWhite);
  free(tinfo->whitePoint);

  if( tinfo ) {
    free(tinfo);
  }
}

/*!
  Gets the tile containing the given coordinate

  The output <buf> is always stored in continuous planer configration,
  i.e. RGB RGB RGB ... RGB for RGB images

  \param tinfo pointer to tifhdr_t type
  \param tdir The TIFF directory to read (zero for first directory)
  \param x x-coordinate of point in tile of interest
  \param y y-coordinate of point in tile of interest
  \return The pointer to the buffer holding the ROI OR \a 0 on error
  */
tdata_t tifGetTile(const tifhdr_t *tinfo, tdir_t tdir, uint32 x, uint32 y) {
  // Ensure: Tiled, correct compression, correct directory, valid directory
  assert(tinfo);
  assert(tinfo->tfile);

  /* Account for sub-sampling */
  if (tinfo->forceRGBJPEG[tdir]) {
    TIFFSetField(tinfo->tfile, TIFFTAG_JPEGCOLORMODE, JPEGCOLORMODE_RGB );
  }
  if (1 != TIFFSetDirectory(tinfo->tfile, tdir)) {
    return 0;
  }

  /* Extract region*/
  if (0 == tinfo->rowsperstrip[tdir]) return tifReadTile(tinfo, tdir, x, y);
  else return 0;
}

/*!
  Gets the strip containing the given row

  The output <buf> is always stored in continuous planer configration,
  i.e. RGB RGB RGB ... RGB for RGB images

  \param tinfo pointer to tifhdr_t type
  \param tdir The TIFF directory to read (zero for first directory)
  \param row a row in the tile of interest
  \return The pointer to the buffer holding the ROI OR \a 0 on error
  */
tdata_t tifGetStrip(const tifhdr_t *tinfo, tdir_t tdir, uint32 row) {
  // Ensure: Tiled, correct compression, correct directory, valid directory
  assert(tinfo);
  assert(tinfo->tfile);

  /* Account for sub-sampling */
  if (tinfo->forceRGBJPEG[tdir]) {
    TIFFSetField(tinfo->tfile, TIFFTAG_JPEGCOLORMODE, JPEGCOLORMODE_RGB );
  }
  if (1 != TIFFSetDirectory(tinfo->tfile, tdir)) {
    return 0;
  }

  /* Extract region*/
  if( tinfo->rowsperstrip[tdir] > 0 ) return tifReadStrip(tinfo, tdir, row);
  else return 0;
}

/*!
  Gets the buffer containing the ROI from the TIFF file <in>.

  The output <buf> is always stored in continuous planer configration,
  i.e. RGB RGB RGB ... RGB for RGB images

  \param tinfo pointer to tifhdr_t type
  \param tdir The TIFF directory to read (zero for first directory)
  \param x0 The top-left x-index of ROI
  \param y0 The top-left y-index of ROI
  \param x1 The bottom-right x-index of ROI; use (uint32)(-1) for max  possible
            value (i.e. image width)
  \param y1 The bottom-right y-index of ROI; use (uint32)(-1) for max  possible
            value (i.e. imag height)
  \return The pointer to the buffer holding the ROI OR \a 0 on error
  */
tdata_t tifGetRegion(const tifhdr_t *tinfo, tdir_t tdir, uint32 x0, uint32 y0, uint32 x1, uint32 y1)
{
  tdata_t buf = 0;
  assert(tinfo);

  /* check for bottom-right-corner */
  if ((uint32)-1 == x1) x1 = tinfo->imgw[tdir] - 1;
  if ((uint32)-1 == y1) y1 = tinfo->imgl[tdir] - 1;

  /* Sanity Check */
  if( !isRequestValid(tinfo, tdir) ) {
    return 0;
  }

  /* Change Directory */
  if (!TIFFSetDirectory(tinfo->tfile, (tdir_t)tdir)) {
    printf("Error, setting subdirectory at %#x\n", tdir);
    return (0);
  }

  /* Account for sub-sampling */
  if (tinfo->forceRGBJPEG[tdir]) {
    TIFFSetField(tinfo->tfile, TIFFTAG_JPEGCOLORMODE, JPEGCOLORMODE_RGB );
  }

  /* Extract region*/
  if( tinfo->rowsperstrip[tdir] == 0 ) {
    buf = getRegionFromTiles(tinfo, tdir, x0, y0, x1, y1);
  } else {
    buf = getRegionFromStrips(tinfo, tdir, x0, y0, x1, y1);
  }

  /* Return buf, will be on error  */
  return buf;
}


/*!
  Gets the buffer containing the ROI from the TIFF file <in>.
  The returned buffer is always has RGB, RGBA or Grayscale color. If the
  image's color is not a RGB, RGBA or Grayscale, TIFF conversions are applied.

  The output <buf> is always stored in continuous planer configration,
  i.e. RGB RGB RGB ... RGB for RGB images

  Uses tifConvertForDisplay() to change color space.

  \param tinfo pointer to tifhdr_t type
  \param tdir The TIFF directory to read (zero for first directory)
  \param x0 The top-left x-index of ROI
  \param y0 The top-left y-index of ROI
  \param x1 The bottom-right x-index of ROI; use (uint32)(-1) for max  possible
            value (i.e. image width)
  \param y1 The bottom-right y-index of ROI; use (uint32)(-1) for max  possible
            value (i.e. imag height)
  \return The pointer to the buffer holding the ROI OR \a 0 on error
  */
tdata_t tifGetRegionForDisplay (const tifhdr_t *tinfo, tdir_t tdir, uint32 x0, uint32 y0, uint32 x1, uint32 y1)
{
  tdata_t data = 0,
          converted = 0;

  /* Get the requested region */
  if( !(data = tifGetRegion(tinfo, tdir, x0, y0, x1, y1)) ) {
    return 0;
  }

  /* Convert it to RGB or Grayscale*/
  converted = tifConvertForDisplay(tinfo, tdir, data, x1-x0+1, y1-y0+1);
  if( converted == data ) {
    // conversion not applied, buffer already in a suitable format
  } else if ( converted ) {
    // Data was converted, release old buffer
    _TIFFfree(data);
  } else {
    // conversion failed
    _TIFFfree(data);
  }

  return converted;
}

/*!
  Converts \a buffer to 8 bit-per-pixel RGB or Grayscale image, as appropriate.

  This function has no effect and returns \a buffer immediately if the \a buffer
  has 8 bits-per-samples and one the following photomoetric interpretations:
    - PHOTOMETRIC_RGB,
    - PHOTOMETRIC_MINISWHITE
    - PHOTOMETRIC_MINISBLACK
    - PHOTOMETRIC_MASK

  This function has no effect and returns 0 if \a buffer has one the following
  photomoetric interpretation values:
    - PHOTOMETRIC_LOGL:
    - PHOTOMETRIC_LOGLUV:
    - PHOTOMETRIC_SEPARATED:

  \note \a tinfo and \a tdir should point to the directory where \a buffer was
  extracted from in order for the correct conversion to be applied. \a width
  and \a height must be accurate. The output is scaled to 8 bits per pixel if
  required using tif8bitScaling().

  \note The return buffer is always stored in continuous planer configration,
  i.e. RGB...RGB...RGB in the case of RGB images

  \note Images with subsampled data, are conveted to unit sampling

  \param tinfo The tiff structure from while \a buffer was extracted
  \param tdir The TIFF directory from which \a buffer was extracted
  \param buffer the array holding data
  \param width \a buffer width
  \param height \a buffer height
  \return A new buffer holding 8 bit-per-sample RGB/Grayscale data or 0 if
  conversion cannot be perfromed
 */
tdata_t tifConvertForDisplay(const tifhdr_t *tinfo, const tdir_t tdir, const tdata_t buffer, const uint32 width, const uint32 height)
{
  tdata_t rgb = 0;

  if( 0 == buffer ||
      0 == tinfo ||
      tdir > (tdir_t)tinfo->ndirs )
    return 0;

  /* Apply color conversion if required */
  switch (tinfo->photometric[tdir]) {
    /* Do not convert these formats */
    case PHOTOMETRIC_MINISWHITE:
    case PHOTOMETRIC_MINISBLACK:
    case PHOTOMETRIC_MASK:
    case PHOTOMETRIC_RGB:
      /* convert to 8bit per pixel */
      if( tinfo->bps[tdir] > 8 ) {
        rgb = tif8bitScaling(tinfo, tdir, buffer, width, height);
      } else { rgb = buffer;
      }
      break;

      /* No conversion available ?? */
    case PHOTOMETRIC_LOGL:
    case PHOTOMETRIC_LOGLUV:
    case PHOTOMETRIC_SEPARATED:
      debug::warning("Could not convert color.", __FUNCTION__, __LINE__); break;

      /* Convert these formats */
    case PHOTOMETRIC_YCBCR:
      // Note JPEG compressed YCbCr is converted to RGB when read
      if (tinfo->forceRGBJPEG[tdir]) break;
      else if ((rgb = tifYccToRgb(tinfo, tdir, buffer, width, height))) break;

    case PHOTOMETRIC_CIELAB:
    case PHOTOMETRIC_ICCLAB:
    case PHOTOMETRIC_ITULAB:
      rgb = tifLabToRgb(tinfo, tdir, buffer, width, height); break;

    case PHOTOMETRIC_PALETTE:
      rgb = tifPaletteToRGB(tinfo, tdir, buffer, width, height); break;
  }

  /* delete old buffer */
  return rgb;
}


/**
 * Gets the directory number of the largest image by area.
 *
 * @param  in  TIFF pointer
 * @return    directory number
 **/
tdir_t  tifGetLargestDirectory(TIFF *in)
{
  uint32 i, imgw, imgh;
  tdir_t tdir = 0;
  double area = 0;


  /* Find the image with largest area */
  for(i=0; i<TIFFNumberOfDirectories(in); i++) {
    if( TIFFSetDirectory(in, (tdir_t)i) ) {
      TIFFGetField(in, TIFFTAG_IMAGEWIDTH, &imgw);
      TIFFGetField(in, TIFFTAG_IMAGELENGTH, &imgh);

      if( area < (double)imgw*imgh ) {
        area = (double)imgw*imgh;
        tdir = i;
      }
    }
    else
      return 0;
  }

  return tdir;
}




/******************************************************************************\
 *  Static Stuff
\******************************************************************************/

/**
 * Open tiff file.
 *
 * @param  in TIFF file pointer
 * @return  pointer to tifhdr_t structure or 0 on error
 **/
static tifhdr_t* tifReadHeader(TIFF *in)
{
  tifhdr_t *tif = 0;
  int   dir,
      ndirs;
  uint16 compression;

  /* Allocate structure */
  if( !(tif = (tifhdr_t*) malloc(sizeof(tifhdr_t))) ) {
    printf("Error: tifReadHeader - insufficient memory.");
    return 0;
  }
  tif->buffer = 0;

  /* Set file properties */
  tif->ndirs = TIFFNumberOfDirectories(in);
  tif->tfile = in;
  ndirs    = tif->ndirs;

  /* Allocate Directory properties */
  tif->forceRGBJPEG = (bool*) calloc(ndirs, sizeof(bool));

  tif->xres = (float*) calloc(ndirs, sizeof(float));
  tif->yres = (float*) calloc(ndirs, sizeof(float));

  tif->imgw = (uint32*) calloc(ndirs, sizeof(uint32));
  tif->imgl = (uint32*) calloc(ndirs, sizeof(uint32));
  tif->tilew = (uint32*) calloc(ndirs, sizeof(uint32));
  tif->tilel = (uint32*) calloc(ndirs, sizeof(uint32));
  tif->ntiles = (uint32*) calloc(ndirs, sizeof(uint32));
  tif->rowsperstrip = (uint32*) calloc(ndirs, sizeof(uint32));

  tif->res_unit = (uint16*) calloc(ndirs, sizeof(uint16));
  tif->bps  = (uint16*) calloc(ndirs, sizeof(uint16));
  tif->spp  = (uint16*) calloc(ndirs, sizeof(uint16));
  tif->planar  = (uint16*) calloc(ndirs, sizeof(uint16));
  tif->photometric  = (uint16*) calloc(ndirs, sizeof(uint16));
  tif->hsubsampling = (uint16*) calloc(ndirs, sizeof(uint16));
  tif->vsubsampling = (uint16*) calloc(ndirs, sizeof(uint16));

  /* Do not delete the conetencts of these poitners
     They are static array maintained by TIFFlib
     which is returned by TIFFGetFieldDefaulted() */
  tif->lumaCoeff  = (float**) calloc(ndirs, sizeof(float*));
  tif->refBlackWhite  = (float**) calloc(ndirs, sizeof(float*));
  tif->whitePoint  = (float**) calloc(ndirs, sizeof(float*));


  /* Read Directory properties */
  for(dir = 0; dir < ndirs; dir++)
  {
    if( !TIFFSetDirectory(in, dir) ) {
      return 0;
    }

    tif->hsubsampling[dir] = 1;
    tif->vsubsampling[dir] = 1;

    TIFFGetFieldDefaulted(in, TIFFTAG_BITSPERSAMPLE, &tif->bps[dir]);
    TIFFGetFieldDefaulted(in, TIFFTAG_SAMPLESPERPIXEL, &tif->spp[dir]);
    TIFFGetFieldDefaulted(in, TIFFTAG_PLANARCONFIG,  &tif->planar[dir]);
    TIFFGetFieldDefaulted(in, TIFFTAG_RESOLUTIONUNIT, &tif->res_unit[dir]);
    TIFFGetFieldDefaulted(in, TIFFTAG_PHOTOMETRIC,  &tif->photometric[dir]);
    TIFFGetFieldDefaulted(in, TIFFTAG_IMAGEWIDTH,  &tif->imgw[dir]);
    TIFFGetFieldDefaulted(in, TIFFTAG_IMAGELENGTH,  &tif->imgl[dir]);
    TIFFGetFieldDefaulted(in, TIFFTAG_XRESOLUTION,  &tif->xres[dir]);
    TIFFGetFieldDefaulted(in, TIFFTAG_YRESOLUTION,  &tif->yres[dir]);
    TIFFGetFieldDefaulted(in, TIFFTAG_COMPRESSION, &compression);

    if (PHOTOMETRIC_YCBCR == tif->photometric[dir] &&
       (COMPRESSION_OJPEG == compression || COMPRESSION_JPEG == compression))
      tif->forceRGBJPEG[dir] = true;
    else
      tif->forceRGBJPEG[dir] = false;

    /* Set Color Space specific values */
    switch( tif->photometric[dir] ) {
      case PHOTOMETRIC_YCBCR:
        TIFFGetFieldDefaulted(in, TIFFTAG_YCBCRSUBSAMPLING, &tif->hsubsampling[dir], &tif->vsubsampling[dir]);
        TIFFGetFieldDefaulted(in, TIFFTAG_YCBCRCOEFFICIENTS, &tif->lumaCoeff[dir]);
        TIFFGetFieldDefaulted(in, TIFFTAG_REFERENCEBLACKWHITE, &tif->refBlackWhite[dir]);
        break;

      case PHOTOMETRIC_CIELAB:
      case PHOTOMETRIC_ICCLAB:
      case PHOTOMETRIC_ITULAB:
        TIFFGetFieldDefaulted(in, TIFFTAG_WHITEPOINT, &tif->whitePoint[dir]);
        break;
    }


    /* Set tile properties */
    if( TIFFIsTiled(in) ) {
      TIFFGetFieldDefaulted(in, TIFFTAG_TILEWIDTH,  &tif->tilew[dir]);
      TIFFGetFieldDefaulted(in, TIFFTAG_TILELENGTH, &tif->tilel[dir]);

      tif->ntiles[dir]   = TIFFNumberOfTiles(in);
      tif->rowsperstrip[dir] = 0;
    }
    else {
      tif->tilew[dir] = 0;
      tif->tilel[dir] = 0;
      tif->ntiles[dir] = 0;

      TIFFGetFieldDefaulted(in, TIFFTAG_ROWSPERSTRIP, &tif->rowsperstrip[dir]);
    }
  }

  return tif;
}

/*!
  Returns TRUE if the request is valid, FALSE otherwise.

  \param tinfo pointer to tifhdr_t type
  \param tdir The TIFF directory to read
  \param x0 The top-left x-index of ROI
  \param y0 The top-left y-index of ROI
  \param x1 The bottom-right x-index of ROI
  \param y1 The bottom-right y-index of ROI
  \return
 */
static bool
isRequestValid(const tifhdr_t *tinfo, tdir_t tdir) {
  assert(tinfo);
  assert(tinfo->tfile);
  TIFF *in = tinfo->tfile;

  /* Check Input file parameters */
  if (0 == in) {
    printf("TIFF handle invalid.");
    return false;
  }
  if(TIFFNumberOfDirectories(in) < tdir){
    printf("Error, invalid directory %#x\n", tdir);
    return false;
  }

  // success
  return true;
}


/*!
  Extracts a tile from the TIFF file.

  The output is always stored in continuous planer configration, i.e. RGB RGB
  RGB ... RGB.  Images with subsampled data, are converted to unit sampling.

  Will work with bits-per-sample of 8, 16 or 33, however, multicomponent images
  should have the same # of bits.

  \param tinfo pointer to tifhdr_t type
  \param tdir The TIFF directory to read
  \param x a position within the tile on interest
  \param y a position coordinate within the tile on interest
  \return Pointer to a buffer holding the tile or 0 on error
 */
tdata_t
tifReadTile(const tifhdr_t *tinfo, tdir_t tdir, uint32 x, uint32 y) {
  // Ensure: Tiled, correct compression, correct directory, valid directory
  assert(tinfo);
  assert(tinfo->tfile);
  assert(tinfo->tilew[tdir] && tinfo->tilel[tdir]);
  assert(TIFFCurrentDirectory(tinfo->tfile) == tdir);

  // Get values
  const uint16  planer = tinfo->planar[tdir];   // planar config
  tdata_t       tile = 0;

  // In palanar mode there is only one tile part
  if (PLANARCONFIG_CONTIG == planer) {
    tile = tifReadTilePart(tinfo, tdir, x, y, 0, 0);
  }

  // In seperate mode there is mutiple tile parts (equal to image samples)
  else if(PLANARCONFIG_SEPARATE == planer) {
    const uint32  tilew = tinfo->tilew[tdir],
                  tileh = tinfo->tilel[tdir];
    const uint16  spp  = tinfo->spp[tdir],        // samples-per-pixel
                  bps  = tinfo->bps[tdir],        // bits-per-sample
                  bytes = (bps + 7) >> 3;
    tdata_t       buf = 0;                        // tile sample buffer

    tile = _TIFFmalloc( (tsize_t)(tilew) * tileh  * bytes * spp);
    for (uint32 s = 0; s < spp; ++s) {
      if( (buf = tifReadTilePart(tinfo, tdir, x, y, 0, s)) ) {
        for (uint32 h = 0; h < tileh; ++h) {
          for (uint32 w = 0, b = 0, t = s; w < tilew; ++w, ++b, t+=spp) {
            copyBufferValue(buf, b, tile, t, (uint16)bps);
          }
        } _TIFFfree(buf);
      } else {
        _TIFFfree(tile);
        tile = 0; break;
      }
    }
  }

  return tile;
}


/*!
  Extract a tile from file using TIFFReadTile() and additionaly ensures that the
  the returned buffer has unit sampling. (i.e. YCC is converted to RGB).

  Will work with bits-per-sample of 8, 16 or 33, however, multicomponent images
  should have the same # of bits.

  see TIFFReadTile()

  \param tinfo pointer to tifhdr_t type
  \param tdir The TIFF directory to read
  \param x a position within the tile on interest
  \param y a position coordinate within the tile on interest
  \param z used if the image is deeper than 1
  \param sample used only if data are organized in separate planes
  \return Pointer to a buffer holding the tile or 0 on error
 */
static tdata_t
tifReadTilePart(const tifhdr_t *tinfo, tdir_t tdir,
            uint32 x, uint32 y, uint32 z, tsample_t sample) {
  // Ensure: Tiled, correct compression, correct directory, valid directory
  assert(tinfo);
  assert(tinfo->tfile);
  assert(tinfo->tilew[tdir] && tinfo->tilel[tdir]);
  assert(TIFFCurrentDirectory(tinfo->tfile) == tdir);

  // Get values
  TIFF *        in = tinfo->tfile;
  const uint32  hs  = tinfo->hsubsampling[tdir],
                vs  = tinfo->vsubsampling[tdir],
                tilew = tinfo->tilew[tdir],
                tileh = tinfo->tilel[tdir];
  tdata_t        tile = 0 ;
  const uint16  planer = tinfo->planar[tdir];
  const bool    forceRGBJPEG = tinfo->forceRGBJPEG[tdir];
                  // If true sub subsampled YCC is always extracted as RGB

  // Allocate tile buffer
  if( !(tile = _TIFFmalloc(TIFFTileSize(in))) ) {
    printf("Error, could not allocate tile buffer.\n");
    return 0;
  }

  // Get tile
  if (TIFFReadTile(in, tile, x, y, z, sample) < 0) {
    printf("Error, could not read tile containing point (%d,%d).\n", x, y);
    return 0;
  }

  // Convert sub-sampled buffer to regular sampling
  if( ((1 != hs) || (1 != vs)) && (!forceRGBJPEG) ) {
    tdata_t tmp = bufToUnitSampling(tile, tilew, tileh, hs, vs, planer);
    _TIFFfree(tile);
    tile = tmp;
  }

  return tile;
}

/*!
  Extracts a region and return the results. Requires tiled file.

  The output is always stored in continuous planer configration, i.e. RGB RGB
  RGB ... RGB.  Images with subsampled data, are converted to unit sampling.

  Will work with bits-per-sample of 8, 16 or 33, however, multicomponent images
  should have the same # of bits.

  \param tinfo pointer to tifhdr_t type
  \param tdir The TIFF directory to read
  \param x0 The top-left x-index of ROI
  \param y0 The top-left y-index of ROI
  \param x1 The bottom-right x-index of ROI
  \param y1 The bottom-right y-index of ROI
  \return Pointer to a buffer holding the ROI
 */
static tdata_t
getRegionFromTiles(const tifhdr_t *tinfo, tdir_t tdir,
                   uint32 x0, uint32 y0, uint32 x1, uint32 y1) {
  // Ensure: Tiled, correct compression, correct directory, valid directory
  assert(tinfo);
  assert(tinfo);
  assert(tinfo->tfile);
  assert(tinfo->tilew[tdir] && tinfo->tilel[tdir]);
  assert(TIFFCurrentDirectory(tinfo->tfile) == tdir);

  // Get values
  const uint32 tilew = tinfo->tilew[tdir];
  const uint32 tileh = tinfo->tilel[tdir];
  const uint16 spp = tinfo->spp[tdir];        // samples-per-pixel
  const uint16 bps = tinfo->bps[tdir];        // bits-per-sample

  // Return buffer properties
  const uint32  bufw  = x1 - x0 + 1,   // width of ROI/buf
                bufh  = y1 - y0 + 1;   // height of ROI/buf

  // allocated buffer for region and tile
  const uint16 bytes = (bps + 7) >> 3;
  tdata_t buf  = _TIFFmalloc((tsize_t)(bufw) * bufh  * bytes * spp);
  if( 0 == buf ) {
    printf("Error, could not allocate buffer.\n");
    return 0;
  }

  // get tile from TIFF and copy to Buffer
  uint32 x = x0,  // a point in the next tile needed
         y = y0;  // a point in the next tile needed
  while( 1 ) {
    // copy tile
    if( !copyTileToBuffer(tinfo, tdir, x, y, buf, x0, y0, bufw, bufh) ) {
      printf("Error, could not allocate buffer.\n");
      goto error;
    }

    // break if buffer has been filled
    if( (x1 == x) && (y1 == y) ) {
      break;
    } // prepare for next row of tiles
    else if( x1 == x ) {
      x = x0;
      y = tifMin(y+tileh, y1);
    } // prepare to continue current row of tiles
    else {
      x = tifMin(x + tilew, x1);
    }
  }

  // success
  return buf;

error:
  if(buf)
    _TIFFfree(buf);
  return 0;
}


/*!
  Copies a tile into a buffer. The tile is determined from the pixel coordinates
  (\a tx, \a ty) and it's copied into the buffer defined by the rectangle
  (\a bufx, \a bufy, \a bufw, \a bufh). Tile and buffer areas should
  overlap otherwise, data is not transfered.

  \note The buffer \a buf must be preallocated and large enough to hold the
  overlapping portion of the tile being copied into it.

  The output is always stored in continuous planer configration, i.e. RGB RGB
  RGB ... RGB.  Images with subsampled data, are converted to unit sampling.

  Will work with bits-per-sample of 8, 16 or 33, however, multicomponent images
  should have the same # of bits.

  \param tinfo pointer to tifhdr_t type
  \param tdir The TIFF directory to read
  \param tx a point on the tile
  \param ty a point on the tile
  \param buf the destination buffer
  \param bufx the top-left pixel's x-coordinate of the \a buf
  \param bufy the top-left pixel's y-coordinate of the \a buf
  \param bufw the width of \a buf in pixels
  \param bufh the height of \a buf in pixels
  \return TRUE if successul , FALSE otherwise
 */
static bool copyTileToBuffer(const tifhdr_t *tinfo, tdir_t tdir,
                      uint32 tx, uint32 ty,
                      tdata_t buf,
                      uint32 bufx, uint32 bufy, uint32 bufw, uint32 bufh) {
  // Ensure: Tiled, correct compression, correct directory, valid directory
  assert(tinfo);
  assert(tinfo->tfile);
  assert(tinfo->tilew[tdir] && tinfo->tilel[tdir]);
  assert(TIFFCurrentDirectory(tinfo->tfile) == tdir);

  // Get values
  const uint32  tilew = tinfo->tilew[tdir],
                tileh = tinfo->tilel[tdir];
  const uint16  spp = tinfo->spp[tdir],        // samples-per-pixel
                bps = tinfo->bps[tdir],        // bits-per-sample
                planar = tinfo->planar[tdir];   // planar config

  // Calculate the tiles boundaries
  uint32  tx1 = (uint32)( tx / tilew) * tilew,
          ty1 = (uint32)( ty / tileh) * tileh,
          tx2 = tx1 + tilew - 1,
          ty2 = ty1 + tileh - 1;

  //Calcualte buffer boundaries
  uint32  bx1 = bufx,
          by1 = bufy,
          bx2 = bx1 + bufw - 1,
          by2 = by1 + bufh - 1;

  // Determine the starting point of the fill
  uint32 bx = 0, by = 0;
  if( tx1 > bx1 ) bx = tx1 - bx1;
  if( ty1 > by1 ) by = ty1 - by1;

  // Calulate first and last rows/cols needed from this tile
  uint32 r1 = 0, c1 = 0,
         r2 = tileh - 1,
         c2 = tilew - 1;
  if( bx1 > tx1 ) c1 = bx1 - tx1;
  if( by1 > ty1 ) r1 = by1 - ty1;
  if( bx2 < tx2 ) c2 = bx2 - tx1;
  if( by2 < ty2 ) r2 = by2 - ty1;

  // copy tile data to buffer
  if( PLANARCONFIG_CONTIG == planar ) {
    // Get tile - only one part
    tdata_t tbuf = tifReadTilePart(tinfo, tdir, tx, ty, 0, 0);
    if( 0 == tbuf ) {
      return false;
    }
    // copy rows, columns and samples from tile into buffer
    for(uint32 r = r1; r <= r2; ++r) {
      for(uint32 c = c1,
                bi = by*(bufw*spp)+(bx*spp),
                ti = r*(tilew*spp)+(c1*spp); c <= c2; ++c) {
        for(uint32 s = 0; s < spp; ++s) {
          copyBufferValue(tbuf, ti++, buf, bi++, (uint16)bps);
        }
      } by++;
    } bx += c2 - c1 + 1;
    _TIFFfree(tbuf);
  }

  else if(PLANARCONFIG_SEPARATE == planar) {
    // copy samples, rows and columns to buffer, while interleaving the sample
    for(uint32 s = 0; s < spp; ++s) {
      // Get tile part
      tdata_t tbuf = tifReadTilePart(tinfo, tdir, tx, ty, 0, s);
      if( 0 == tbuf ) {
        return false;
      }
      // to maintain Separate planes use: bi=(s*bufw*bufh)+(by*bufw)+bx
      for(uint32 r = r1; r <= r2; ++r) {
        for(uint32 c = c1,
                  bi = by*(bufw*spp)+(bx*spp)+s,
                  ti = (r*tilew)+c1; c <= c2; ++c) {
          copyBufferValue(tbuf, ti++, buf, bi, (uint16)bps);
          bi += spp;     //***to maintain Separate planes use: bi++;
        } by++;
      }
      by -= r2 - r1 + 1; // reset by for next component
      _TIFFfree(tbuf);
    }
    bx += c2 - c1 + 1;  // inceremt bx once all components written
    by += r2 - r1 + 1;  // inceremt by once all components written
  }

  // success,
  return true;
}


/**
 * Gets an ROI from the TIFF file 'in' and return the results
 * in 'buf'. Only works for files that are organized in strip
 *
 * NOTE: - The output is always stored in continuous planer
 *      configration, i.e. RGB RGB RGB ... RGB
 *   - Will work with bits-per-sample of 8, 16 or 32
 *     However, for multicomponent images, all components
 *     should have the same # of bits.
 *   - Images with subsampled data, are turned with unit sampling
 *
 * @param tinfo  pointer to tifhdr_t type
 * @param tdir  The TIFF directory to read
 * @param x0   The top-left x-index of ROI
 * @param y0   The top-left y-index of ROI
 * @param x1   The bottom-right x-index of ROI
 * @param  y1    The bottom-right y-index of ROI
 * @return    The pointer to the buffer holding the ROI
 **/
static tdata_t
getRegionFromStrips(const tifhdr_t *tinfo, tdir_t tdir,
                    uint32 x0, uint32 y0, uint32 x1, uint32 y1)
{
  // Ensure: Tiled, correct compression, correct directory, valid directory
  assert(tinfo);
  assert(tinfo->tfile);
  assert(tinfo->rowsperstrip[tdir]);
  assert(TIFFCurrentDirectory(tinfo->tfile) == tdir);

  // Get values
  const uint32 bufw  = x1 - x0 + 1, // width of  ROI
               bufh  = y1 - y0 + 1; // height of ROI
  const uint16  spp = tinfo->spp[tdir],        // samples-per-pixel
                bps = tinfo->bps[tdir],        // bits-per-sample
                rowsperstrip = tinfo->rowsperstrip[tdir];


  // allocated buffer for region strip
  uint32 bytes = ( bps + 7 ) >> 3;
  tdata_t buf = _TIFFmalloc( (tsize_t)(bufw)  * bufh  * bytes * spp );
  if( !buf ) {
    printf("Error, could not allocate buffer.\n");
    goto error;
  }

  // Copy strips to buffer
  for( uint32 r = y0; r <= y1; r+=rowsperstrip ) {
    if (!tifCopyStripToBuffer(tinfo, tdir, r, buf, x0, y0, bufw, bufh)) {
      goto error;
    }
  }

  // success
  return buf;

error:
  if(buf)
    _TIFFfree(buf);
  return 0;
}

/*!
  Copies a strip into a buffer. The strip is determined from the image \a row
  and it's copied into the buffer defined by the rectangle
  (\a bufx, \a bufy, \a bufw, \a bufh). The strip and buffer areas should
  overlap otherwise, data is not transfered.

  \note The buffer \a buf must be preallocated and large enough to hold the
  overlapping portion of the stip being copied into it.

  The output is always stored in continuous planer configration, i.e. RGB RGB
  RGB ... RGB.  Images with subsampled data, are converted to unit sampling.

  Will work with bits-per-sample of 8, 16 or 33, however, multicomponent images
  should have the same # of bits.

  \param tinfo pointer to tifhdr_t type
  \param tdir The TIFF directory to read
  \param row a row in the source strip
  \param buf the destination buffer
  \param bufx the top-left pixel's x-coordinate of the \a buf
  \param bufy the top-left pixel's y-coordinate of the \a buf
  \param bufw the width of \a buf in pixels
  \param bufh the height of \a buf in pixels
  \return TRUE if successul , FALSE otherwise
 */
static bool
tifCopyStripToBuffer(const tifhdr_t *tinfo, tdir_t tdir, uint32 row,
                      tdata_t buf,
                      uint32 bufx, uint32 bufy, uint32 bufw, uint32 bufh) {

  // Ensure: Tiled, correct compression, correct directory, valid directory
  assert(tinfo);
  assert(tinfo->tfile);
  assert(tinfo->rowsperstrip[tdir]);
  assert(TIFFCurrentDirectory(tinfo->tfile) == tdir);

  //Get Values
  TIFF *        in = tinfo->tfile;
  const uint16  spp = tinfo->spp[tdir],
                bps = tinfo->bps[tdir],
                planer = tinfo->planar[tdir],
                rowsperstrip = tinfo->rowsperstrip[tdir];
  const uint32  imgw = tinfo->imgw[tdir];

  // calculate the strips needed
  const uint32 strip = TIFFComputeStrip(in, row, 0);
  const uint32 srow1 = strip * rowsperstrip;
  const uint32 srow2 = srow1 + rowsperstrip - 1;

  //Calcualte buffer boundaries
  uint32  bx1 = bufx,
          by1 = bufy,
          bx2 = bx1 + bufw - 1,
          by2 = by1 + bufh - 1;

  // Calculate rows needed from strip
  uint32 r1 = 0,
         r2 = r1 + rowsperstrip - 1;
  if( by1 > srow1 ) r1 = by1 - srow1;
  if( by2 < srow2 ) r2 = by2 - srow1;

   // starting row of fill
  uint32 by = 0;
  if(srow1 > by1) by = srow1 - by1;

  // copy data to buffer
  if(planer == PLANARCONFIG_CONTIG ) {
    // Only has one part
    tdata_t sbuf = tifReadStripPart(tinfo, tdir, row, 0);
    if( 0 == sbuf ) return false;

    //copy to buffer for regular sampling
    for(uint32 r = r1; r <= r2; ++r) {
      for(uint32 c = bx1, sbi = (r*imgw+bx1)*spp, bi = by*bufw*spp; c <= bx2; ++c) {
        for(uint32 s = 0; s < spp; ++s) {
          copyBufferValue(sbuf, sbi++, buf, bi++, (uint16)bps);
        }
      } by++;
    }
    _TIFFfree(sbuf);
    sbuf = 0;
  }

  else if(planer == PLANARCONFIG_SEPARATE) {
    for(uint32 s = 0; s < spp; ++s ) {
      // read strip
      tdata_t sbuf = tifReadStripPart(tinfo, tdir, r1, s);
      if( 0 == sbuf ) return false;

      //copy to buffer for regular sampling
      for(uint32 r = r1; r <= r2; ++r) {
        for(uint32 c = bx1, sbi = r*imgw+bx1, bi = (by*bufw*spp)+s; c <= bx2; ++c) {
          copyBufferValue(sbuf, sbi++, buf, bi, (uint16)bps);
          bi += spp;
        } by++;
      } by -= r2-r1; //reset for next sample
      _TIFFfree(sbuf);
    } by += r2-r1;  //reset for next strip
  }

  // return
  return true;
}


/*!
  Extract a strip from file using TIFFEncodedStrip() and additionaly ensures
  that the returned buffer has unit sampling. (i.e. YCC is converted to RGB).

  The output is always stored in continuous planer configration, i.e. RGB RGB
  RGB ... RGB.  Images with subsampled data, are converted to unit sampling.

  Will work with bits-per-sample of 8, 16 or 33, however, multicomponent images
  should have the same # of bits.

  see TIFFReadEncodedStrip()

  \param tinfo pointer to tifhdr_t type
  \param tdir The TIFF directory to read
  \param row the row on interest
  \param sample used only if data are organized in separate planes
  \return Pointer to a buffer holding the strip or 0 on error
 */
tdata_t
tifReadStrip(const tifhdr_t *tinfo, tdir_t tdir, uint32 row) {
  // Ensure: Tiled, correct compression, correct directory, valid directory
  assert(tinfo);
  assert(tinfo->tfile);
  assert(tinfo->rowsperstrip[tdir]);
  assert(TIFFCurrentDirectory(tinfo->tfile) == tdir);

  // Get values
  tdata_t       strip = 0 ;
  const uint16  planer = tinfo->planar[tdir];

  // In palanar mode there is only one tile part
  if (PLANARCONFIG_CONTIG == planer) {
    strip = tifReadStripPart(tinfo, tdir, row, 0);
  }

  // In seperate mode there is mutiple tile parts (equal to image samples)
  else if(PLANARCONFIG_SEPARATE == planer) {
    const uint32 rowsperstrip = tinfo->rowsperstrip[tdir],
                 stripw = tinfo->imgw[tdir];
    const uint16  spp  = tinfo->spp[tdir],        // samples-per-pixel
                  bps  = tinfo->bps[tdir],        // bits-per-sample
                  bytes = (bps + 7) >> 3;
    tdata_t       buf = 0;                        // tile sample buffer

    strip = _TIFFmalloc( (tsize_t)(rowsperstrip) * stripw * bytes * spp);
    for (uint32 s = 0; s < spp; ++s) {
      if( (buf = tifReadStripPart(tinfo, tdir, row, s)) ) {
        for (uint32 r = 0, b = 0, st = s; r < rowsperstrip; ++r, ++b, st+=spp) {
          copyBufferValue(buf, b, strip, st, (uint16)bps);
        } _TIFFfree(buf);
      } else {
        _TIFFfree(strip);
        strip = 0; break;
      }
    }
  }

  return strip;
}


/*!
  Extract a strip from file using TIFFEncodedStrip() and additionaly ensures
  that the returned buffer has unit sampling. (i.e. YCC is converted to RGB).

  Will work with bits-per-sample of 8, 16 or 33, however, multicomponent images
  should have the same # of bits.

  see TIFFReadEncodedStrip()

  \param tinfo pointer to tifhdr_t type
  \param tdir The TIFF directory to read
  \param row the row on interest
  \param sample used only if data are organized in separate planes
  \return Pointer to a buffer holding the strip or 0 on error
 */
static tdata_t
tifReadStripPart(const tifhdr_t *tinfo, tdir_t tdir,
                  uint32 row, tsample_t sample) {
  // Ensure: Tiled, correct compression, correct directory, valid directory
  assert(tinfo);
  assert(tinfo->tfile);
  assert(tinfo->rowsperstrip[tdir]);
  assert(TIFFCurrentDirectory(tinfo->tfile) == tdir);

  // Get values
  TIFF *        in = tinfo->tfile;
  const uint32  hs  = tinfo->hsubsampling[tdir],
                vs  = tinfo->vsubsampling[tdir],
                rowsperstrip = tinfo->rowsperstrip[tdir],
                stripw = tinfo->imgw[tdir];
  const uint16  planer = tinfo->planar[tdir];
  const bool    forceRGBJPEG = tinfo->forceRGBJPEG[tdir];
                  // If true sub subsampled YCC is always extracted as RGB

  tdata_t strip = _TIFFmalloc(TIFFStripSize(in));
  if( !strip ) {
    printf("Error, could not allocate buffer.\n");
    goto error;
  }

  // Get strip
  // TODO: do we need the full strip? Can we determine what actual size we need?
  tstrip_t stripno;
  stripno = TIFFComputeStrip(in, row, sample);
  if( TIFFReadEncodedStrip(in, stripno, strip, -1) < 0 ) {
    printf("Error, error decoding strip #%d.\n", strip);
    goto error;
  }

  // Convert YCrCb to regular sampling
  if( ((1 != hs) || (1 != vs)) && (!forceRGBJPEG) ) {
    tdata_t tmp = bufToUnitSampling(strip, stripw, rowsperstrip, hs, vs,planer);
    _TIFFfree(strip);
    strip = tmp;
  }

  // return buffer
  return strip;

error:
  _TIFFfree(strip);
  return 0;
}


/**
 * Converts a YCbCr buffer to unit sampling.
 * Ouput buffer contains regularly sampled data.
 *
 * See exmaple code pal2rgb tools
 *
 * @param  buf the buffer with YCC data
 * @param  bufw the width of buffer in pixels
 * @param  bufh the height of buffer in pixels
 * @param  hs the vertical subsampling factor
 * @param  vs the horizontal subsampling factor
 * @param  photometric the buffer planar organzation
 * @return  the pointer to the converted buffer, or 0 on error
 */
static tdata_t bufToUnitSampling(tdata_t buf, uint32 bufw, uint32 bufh, uint32 hs, uint32 vs, uint32 photometric)
{
  unsigned char *outbuf,
      *inbuf;
  uint32 row, col,
      i, j, k, b,
      bind,
      crInd,
      crBase,
      brBase,
      brInd,
      bcr,  // a Cr sample
      bbr,  // a Br sample
      llen,  // number of luma smaples per block
      bnum,  // number of blocks
      spp = 3; // samples per output pixel

  /* Error check*/
  if( !buf  ) {
    printf("Error, buffer is empty.\n");
    return 0;
  }

  /* No conversion required */
  if( hs==1 && vs==1) {
    printf("Warning: NO coversion required, returning a copy of buffer");
  }

  /* allocated buffer for region strip */
  outbuf = (unsigned char*) _TIFFmalloc((tsize_t)(bufw)  * bufh * spp );
  if( !outbuf ) {
    printf("Error, could not allocate buffer.\n");
    return 0;
  }

  /* Copy to buffer */
  inbuf = (unsigned char*) buf;
  llen = vs * hs;
  bnum = tifDivCeil(bufw, hs) * tifDivCeil(bufh , vs);
  row  = col
      = bind
      = 0;

  switch( photometric ) {
    case PLANARCONFIG_CONTIG:
      for(b=0; b<bnum; b++) {
        /* Get chroma components from this block */
        bcr = inbuf[bind+llen+0];
        bbr = inbuf[bind+llen+1];

        /* Copy samples to buffer */
        for(i=0; i<vs; i++) {
          /* Don't exceed image boundries */
          if( (row+i) >= bufh) {
            bind += hs;
            continue;
          }
          k = (row+i)*bufw*spp + (col*spp);

          for(j=0; j<hs; j++, k+=spp) {
            /* Don't exceed image boundries */
            if( (col+j) >= bufw ) {
              bind++;
              continue;
            }
            outbuf[k] = inbuf[bind++];
            outbuf[k+1] = bcr;
            outbuf[k+2] = bbr;
          }
        }

        /* Fix buffer index */
        bind += 2;  //account for chroma samples
        col += hs; // account for output buffer column
        if( col == bufw ) {
          row += vs;
          col = 0;
        }
      }
      break;

    case PLANARCONFIG_SEPARATE:
      /* Copy samples to buffer */
      k  = 0;    // index of luma (input and output)
      crBase = bufw * bufh;  // index of Cr in output buffer
      brBase = bufw * bufh * 2; // index of Br in output buffer
      crInd = bufw * bufh;  // index of next Cr sample
      brInd = crInd + (tifDivCeil(bufw, hs) * tifDivCeil(bufh , vs));

      for(row=0; row<bufh; row+=vs) {
        for(col=0; col<bufw; col+=hs) {
          bcr = inbuf[crInd++];
          bbr = inbuf[brInd++];
          for(i=0; i<vs; i++) {

            /* Don't exceed image boundries */
            if( (row+i) >= bufh ) {
              break;
            }
            bind = bufw*(row+i)+col;
            for(j=0; j<hs; j++) {

              /* Don't exceed image boundries */
              if( (col+j) >= bufw ) {
                break;
              }
              outbuf[    bind+j] = inbuf[bind+j];
              outbuf[crBase+bind+j] = bcr;
              outbuf[brBase+bind+j] = bbr;
            }
          }
        }
      }
      break;
  }

  /* Return */
  return (tdata_t)outbuf;

}


/**
 * Scales buffer to 8bit per sample. Should be used to downscale
 * 16bps RGB or Grayscale images to 8bps.
 *
 * See exmaple code pal2rgb tools
 *
 * @param  tinfo pointer to tiff header structure
 * @param  tdir the tiff directory the data is from - needed to get YCC conversion constants
 * @param  data the buffer with contiguous YCC data
 * @param  bufw the width of buf in pixels
 * @param  bufh the hieght of buf in pixels
 * @return  pointer to converted data or 0 on error
 */
static tdata_t tif8bitScaling(const tifhdr_t *tinfo, tdir_t tdir, tdata_t data, uint32 bufw, uint32 bufh)
{
  uint32 i, bps, spp;
  unsigned char *obuf;

  /* Check directory */
  if( tdir < tinfo->ndirs ) {
    spp = tinfo->spp[tdir];
    bps = tinfo->bps[tdir];
  }
  else
    return(0);


  /* Create new buffer for RGB data */
  obuf  = (unsigned char*)_TIFFmalloc(bufw*bufh*spp);

  /* Convert buffer */
  if( bps == 16 )
    for(i=0; i<bufw*bufh*spp; i++)
      obuf[i] = getUnsignedValue(data, i, bps) >> 8;

  else if( bps == 32 )
    for(i=0; i<bufw*bufh*spp; i++)
      obuf[i] = getUnsignedValue(data, i, bps) >> 24;

  else  {
    _TIFFfree(obuf);
    obuf = 0;
  }

  /* Delete input buffer */
  return (tdata_t)obuf;
}


/**
 * Converts a Pallete buffer to an RGB buffer. Returnes
 * buffer contains RGB data.
 *
 * See exmaple code pal2rgb tools
 *
 * @param  tinfo pointer to tiff header structure
 * @param  tdir the tiff directory the data is from
 * @param  data the buffer with contiguous YCC data
 * @param  bufw the width of buf in pixels
 * @param  bufh the hieght of buf in pixels
 * @return  pointer to RGB data or 0 on error
 */
static tdata_t tifPaletteToRGB(const tifhdr_t *tinfo, tdir_t tdir, tdata_t data, uint32 bufw, uint32 bufh)
{
  uint16 *rmap, *gmap, *bmap;
  uint32 i, p, bps;
  unsigned char *obuf;

  if (!TIFFGetField(tinfo->tfile, TIFFTAG_COLORMAP, &rmap, &gmap, &bmap)) {
    TIFFError("tifPaletteToRGB", "No colormap (not a valid palette image)");
    return 0;
  }

  /* Check directory */
  if( tdir < tinfo->ndirs ) {
    bps = tinfo->bps[tdir];
  }
  else
    return(0);

  /* Create new buffer for RGB data */
  obuf  = (unsigned char*)_TIFFmalloc(bufw*bufh*3);

  /* Convert buffer */
  for(i=0, p=0; i<bufw*bufh; i++) {
    obuf[p++] = (unsigned char)rmap[ getUnsignedValue(data, i, bps) ];
    obuf[p++] = (unsigned char)gmap[ getUnsignedValue(data, i, bps) ];
    obuf[p++] = (unsigned char)bmap[ getUnsignedValue(data, i, bps) ];
  }

  /* Delete input buffer */
  return (tdata_t)obuf;
}


/**
 * Converts a an CIELAB buffer to an RGB buffer.
 * Assumes the input CIELAB buffer is contigous order
 * (i.e. Lab Lab....). Works with 8,16, and 32 bit input data.
 *
 * CODE from tiff examples in COLOR section
 *
 * @param  tinfo pointer to tiff header structure
 * @param  tdir the tiff directory the data is from - needed to get YCC conversion constants
 * @param  data the buffer with contiguous YCC data
 * @param  bufw the width of buf in pixels
 * @param  bufh the hieght of buf in pixels
 * @return  pointer to input buffer with converted data or 0 on error
 */
static tdata_t tifLabToRgb(const tifhdr_t *tinfo, tdir_t tdir, tdata_t data, uint32 bufw, uint32 bufh)
{
  TIFFCIELabToRGB *cielab;
  uint32 r, g, b,  L,
      i, spp, bps;
  int32 La, Lb;
  float X, Y, Z;
  bool is16bit;
  float   *whitePoint;
  float   refWhite[3];
  unsigned char *buf;
  TIFFDisplay display_sRGB = {
    { /* XYZ -> luminance matrix */
      { 3.2410F, -1.5374F, -0.4986F },
      { -0.9692F, 1.8760F, 0.0416F },
      { 0.0556F, -0.2040F, 1.0570F }
    },
    100.0F, 100.0F, 100.0F, /* Light o/p for reference white */
    255, 255, 255,   /* Pixel values for ref. white */
    1.0F, 1.0F, 1.0F,  /* Residual light o/p for black pixel */
    2.4F, 2.4F, 2.4F,  /* Gamma values for the three guns */
  };

  /* Check directory */
  if( tdir < tinfo->ndirs ) {
    spp = tinfo->spp[tdir];
    bps = tinfo->bps[tdir];

    is16bit = (bps == 16 ? true : false);
  }
  else
    return(0);


  /* Initialize structures */
  cielab = (TIFFCIELabToRGB *) _TIFFmalloc(sizeof(TIFFCIELabToRGB));
  if (!cielab) {
    TIFFError("CIE L*a*b*->RGB", "No space for CIE L*a*b*->RGB conversion state.");
    return 0;
  }

  whitePoint = tinfo->whitePoint[tdir];
  refWhite[1] = 100.0F;
  refWhite[0] = whitePoint[0] / whitePoint[1] * refWhite[1];
  refWhite[2] = (1.0F - whitePoint[0] - whitePoint[1]) / whitePoint[1] * refWhite[1];


  if (TIFFCIELabToRGBInit(cielab, &display_sRGB, refWhite) < 0) {
    TIFFError("CIE L*a*b*->RGB", "Failed to initialize CIE L*a*b*->RGB conversion state.");
    _TIFFfree(cielab);
    return 0;
  }

  /* Create new buffer for RGB data */
  buf  = (unsigned char*)_TIFFmalloc(bufw*bufh*3);

  /* Start conversion */
  for(i=0; i<bufw*bufh*spp; i+=3)
  {
    L  = getUnsignedValue(data, i, bps);
    La = getSignedValue(data, i+1, bps);
    Lb = getSignedValue(data, i+2, bps);

    if( is16bit ) {
      L >>= 8;
      La >>= 8;
      Lb >>= 8;
    }

    TIFFCIELabToXYZ(cielab, L, La, Lb, &X, &Y, &Z);
    TIFFXYZToRGB(cielab, X, Y, Z, &r, &g, &b);

    buf[i  ] = r;
    buf[i+1] = g;
    buf[i+2] = b;
  }

  /* Don�t forget to free the state structure */
  _TIFFfree(cielab);

  return (tdata_t)buf;
}

/**
 * Converts a an YCbCr buffer to an RGB buffer.
 * Assumes the input YCC buffer is in contigous order
 * (i.e. YCbCr YCbCr....) and 8bit per channel.
 *
 * CODE from tiff examples in COLOR section
 *
 * @param  tinfo pointer to tiff header structure
 * @param  tdir the tiff directory the data is from - needed to get YCC
 * conversion constants
 * @param  data the buffer with contiguous YCC data
 * @param  bufw the width of buf in pixels
 * @param  bufh the hieght of buf in pixels
 * @return  pointer to input buffer with converted data or 0 on error
 */
static tdata_t tifYccToRgb(const tifhdr_t *tinfo, tdir_t tdir, tdata_t data, uint32 bufw, uint32 bufh)
{
  TIFFYCbCrToRGB* ycbcr;
  uint32 r, g, b;
  uint32 Y;
  int32 Cb, Cr;
  uint32 i, spp, bps;
  unsigned char *buf;
  float *luma,
      *ref;

  /* Check directory */
  if( tdir < tinfo->ndirs && tinfo->photometric[tdir]==PHOTOMETRIC_YCBCR ) {
    spp = tinfo->spp[tdir];
    bps = tinfo->bps[tdir];
  }
  else
    return(0);


  /* Initialize structures YCC to RGB structure*/
  ycbcr = (TIFFYCbCrToRGB*)
      _TIFFmalloc(tifRoundUp_32(sizeof(TIFFYCbCrToRGB), sizeof(long))
                  + 4*256*sizeof(TIFFRGBValue)
                  + 2*256*sizeof(int)
                  + 3*256*sizeof(int32));

  if (ycbcr == NULL) {
    TIFFError("YCbCr->RGB", "No space for YCbCr->RGB conversion state");
    return(0);
  }
  /*
    should not these values should be able to use
    tinfo->lumaCoeff[tdir] and  tinfo->refBlackWhite[tdir] instead
    there seems to be bug in libtiff
  */
  TIFFGetFieldDefaulted(tinfo->tfile, TIFFTAG_YCBCRCOEFFICIENTS, &luma);
  TIFFGetFieldDefaulted(tinfo->tfile, TIFFTAG_REFERENCEBLACKWHITE, &ref);

  //if (TIFFYCbCrToRGBInit(ycbcr, tinfo->lumaCoeff[tdir], tinfo->refBlackWhite[tdir]) < 0) {
  if (TIFFYCbCrToRGBInit(ycbcr, luma, ref) < 0) {
    return(0);
  }

  /* Create new buffer for RGB data */
  buf  = (unsigned char*)_TIFFmalloc(bufw*bufh*spp);

  /* Start conversion */
  for(i=0; i<bufw*bufh*spp; i+=3)
  {
    Y  = (uint32) getUnsignedValue(data, i+0, bps);
    Cb = (uint32) getUnsignedValue(data, i+1, bps);
    Cr = (uint32) getUnsignedValue(data, i+2, bps);
    TIFFYCbCrtoRGB(ycbcr, Y, Cb, Cr, &r, &g, &b);
    buf[i  ] = r;
    buf[i+1] = g;
    buf[i+2] = b;
  }

  /* Free state structure */
  _TIFFfree(ycbcr);

  return (tdata_t)buf;
}


/**
 * Makes the correct casts for copying samples of warying widths.
 *
 * @param  src pointer to source array
 * @param  srci index of the source array to copy
 * @param  dest pointer to destination array
 * @param  desti index of the destination array to copy
 * @param  bps bits-per-sample of each buffer
 **/
static inline void copyBufferValue(tdata_t src, uint32 srci, tdata_t dest, uint32 desti, uint16 bps)
{
  switch( (bps) ){
    case 8:  ((uint8*) (dest))[desti] = ((uint8*) (src))[srci]; break;
    case 16: ((uint16*)(dest))[desti] = ((uint16*)(src))[srci]; break;
    case 32: ((uint32*)(dest))[desti] = ((uint32*)(src))[srci]; break;
  }
}


/**
 * Return the unsgined-value at a given index from a buffer.
 *
 * @param  buffer pointer to source array
 * @param  index position of the source array to get
 * @param  bps buffer element width in bits
 * @return  the requested value
 **/
static inline uint32 getUnsignedValue(tdata_t buffer, uint32 index, uint32 bps)
{
  switch( bps ) {
    case 16: return ((uint16*)buffer)[index]; break;
    case 32: return ((uint32*)buffer)[index]; break;
    case 8:
    default: return ((uint8*)buffer)[index]; break;
  }
}


/**
 * Return the sgined-value at a given index from a buffer.
 *
 * @param  buffer pointer to source array
 * @param  index position of the source array to get
 * @param  bps buffer element width in bits
 * @return  the requested value
 **/
static inline int32 getSignedValue(tdata_t buffer, uint32 index, uint32 bps)
{
  switch( bps ) {
    case 16: return ((int16*)buffer)[index]; break;
    case 32: return ((int32*)buffer)[index]; break;
    case 8:
    default: return ((int8*)buffer)[index]; break;
  }
}



/**
 * Set the tile/strip size properties for the current directory.
 * Use this before writing the a TIFF directory. When cropping
 * the default values will be coppied from the input file if they are unspecified.
 * The parameters \<rowsperstrip> is always used when it has a non-zero
 * value, otherwise, tilesize is used. If both are zero, the no changes
 * are made and the function returns 0.
 *
 * @param  tinfo the out TIFF image structure as returned by tifOpen()
 * @param  tilesize the size of tiles to use
 * @param  rowsperstrip the number of rows per strip
 * @return  1 on sucess, 0 on failure
 **/
int tifWriteSetSize (tifhdr_t *tinfo, uint32 tilesize, uint32 rowsperstrip)
{
  TIFF *out = tinfo->tfile;

  if( rowsperstrip > 0 )
  {
    TIFFSetField(out, TIFFTAG_ROWSPERSTRIP, rowsperstrip);
  }
  else if( tilesize > 0 )
  {
    TIFFSetField(out, TIFFTAG_TILEWIDTH, tilesize);
    TIFFSetField(out, TIFFTAG_TILELENGTH, tilesize);
  }
  else
  {
    return 0;
  }

  return 1;
}

/*!
   Set the compression properties for the current directory.
   Use this before writing the a TIFF directory. When cropping the default
   values will be coppied from the input file if they are unspecified.

   The parameters \<compression> and \<jpegcolormore> must be defined in
   LibTiff.

   \param  tinfo the out TIFF image structure as returned by tifOpen()
   \param  compression a valid compression mode as defined in LibTiff
   \param  jpegcolormode a valid mode as defined in LibTiff
      used only if compression is JPEG, use 0 for default value
   \param  jpegquality a qaulity valud [1,100]  -
      used only if compression is JPEG; , use 0 for default values
   \return  1 on sucess, 0 on failure
 **/
int tifWriteSetCompression(tifhdr_t *tinfo, uint16 compression, int jpegcolormode, int jpegquality)
{
  TIFF *out = tinfo->tfile;

  TIFFSetField(out, TIFFTAG_COMPRESSION, compression);
  if( COMPRESSION_JPEG == compression )
  {
    if( jpegquality) TIFFSetField(out, TIFFTAG_JPEGQUALITY, jpegquality);
    if( jpegcolormode) TIFFSetField(out, TIFFTAG_JPEGCOLORMODE, jpegcolormode);
  }

  return 1;
}

/**
 * Crops the image and writes the output to another TIF image. If the cropped
 * region falls outside the image, it will be corrected to valid values.
 * The user can specifiy which tiff directory in the input image to crop - the
 * default directory is the first one found. The cropped image is placed in the
 * specified directory in the output file. The output file directories can be modified
 * with other function, otherwise they willbe copied from the input directory.
 *
 *
 *
 * @param  tin the input TIFF image structure as returned by tifOpen()
 * @param  tout the output TIFF image structure as returned by tifOpenWrite()
 * @param  tdir the TIFF directory in the input to crop
 * @param  x0 the starting x-index of the crop region
 * @param  y0 the starting y-index of the crop region
 * @param  width the width of the crop region
 * @param  length the height of the crop region
 * @return  1 on sucess, 0 on failure
 **/
int writeCroppedImage(tifhdr_t *tin, tifhdr_t *tout, tdir_t tdir, uint32 x0, uint32 y0, uint32 width, uint32 length)
{
  TIFF *in   = tin->tfile;
  TIFF *out  = tout->tfile;
  int  outtiled;
  tdata_t buf, bufc;
  uint16 spp, bps,
      config,
      photo;
  uint32 x1, y1,
      strip,
      stripsize,
      tilelength,
      tilewidth,
      rowsperstrip;


  /* Do some error checking */
  if( TIFFSetDirectory(in,  tdir ) == 0 ||
      x0 >= tin->imgw[tdir]    ||
      y0 >= tin->imgl[tdir]     )
    return 0;

  if( x0+width > tin->imgw[tdir] )
    width = tin->imgw[tdir] - x0;
  if( y0+length > tin->imgl[tdir] )
    length = tin->imgl[tdir] - y0;

  /* Get/Set properties */
  writeCroppedHeader(in, out, width, length);
  TIFFGetField(out, TIFFTAG_PLANARCONFIG, &config);
  TIFFGetField(out, TIFFTAG_TILEWIDTH, &tilewidth);
  TIFFGetField(out, TIFFTAG_TILELENGTH, &tilelength);
  TIFFGetField(out, TIFFTAG_SAMPLESPERPIXEL, &spp);
  TIFFGetField(out, TIFFTAG_BITSPERSAMPLE, &bps);
  TIFFGetField(out, TIFFTAG_ROWSPERSTRIP, &rowsperstrip);
  TIFFGetField(out, TIFFTAG_PHOTOMETRIC, &photo );

  outtiled  = TIFFIsTiled(out);

  /* Copy the image parts */
  if (outtiled)
  {
    for( uint32 x = x0; x < x0+width; x += tilewidth )
    {
      for( uint32 y = y0; y < y0+length; y += tilelength )
      {
        x1  = x+tilewidth-1;
        y1  = y+tilelength-1;

        if( x1 >= tin->imgw[tdir] )
          x1 = tin->imgw[tdir]-1;
        if( y1 >= tin->imgl[tdir] )
          y1 = tin->imgl[tdir]-1;

        buf = tifGetRegion(tin, tdir, x, y, x1, y1);
        if( 0 == buf )
        {
          debug::warning(
          "Insufficient memory for this operation. Try using a smaller tile size.",
          __FUNCTION__, __LINE__);
          return 0;
        }

        if (config == PLANARCONFIG_CONTIG)
          TIFFWriteTile(out, buf, x-x0, y-y0, 0, 0);
        else
        {
          for( uint32 s = 0; s < spp; s++ )
          {
            bufc = tifGetComponent(s, buf, x1-x0+1, y1-y0+1, spp, bps);
            TIFFWriteTile(out, buf, x-x0, y-y0, 0, s);
            _TIFFfree(bufc);
          }
        }
        _TIFFfree(buf);
      }
    }
  }
  else
  {
    for( uint32 y = y0; y < y0+length; y += rowsperstrip)
    {
      x1  = x0+width-1;
      y1  = y+rowsperstrip-1;

      if( x1 >= tin->imgw[tdir] )
        x1 = tin->imgw[tdir]-1;
      if( y1 >= tin->imgl[tdir] )
        y1 = tin->imgl[tdir]-1;

      buf  = tifGetRegion(tin, tdir, x0, y, x1, y1);
      if( 0 == buf )
      {
        debug::warning(
          "Insufficient memory for this operation. Use tiled crop instead.\n",
          __FUNCTION__, __LINE__);
        return 0;
      }

      if (config == PLANARCONFIG_CONTIG)
      {
        strip   = TIFFComputeStrip(out, y-y0, 0);
        stripsize = (x1-x0+1) * (y1-y+1) * spp * (bps>>3) ;
        //uint32 k = TIFFStripSize(out);
        TIFFWriteEncodedStrip(out, strip, buf, stripsize);
      }
      else
      {
        for( uint32 s = 0; s < spp; s++ )
        {
          bufc   = tifGetComponent(s, buf, x1-x0+1, y1-y0+1, spp, bps);
          strip   = TIFFComputeStrip(out, y-y0, s);
          stripsize = (x1-x0+1) * (y1-y+1) * (bps>>3) ;
          TIFFWriteEncodedStrip(out, strip, buf, stripsize);
          _TIFFfree(bufc);
        }
      }
      _TIFFfree(buf);

    }
  }

  if ( !TIFFWriteDirectory(out))
    return 0;

  return 1;
}

/**
 * Prepares a TIF file for cropping. Coppies the header elements that
 * are not already set, from the the input file. Works on the curreny_min
 * set directory.
 *
 *  NOTES:
 *   - this is adapted from libtiff's writeCroppedImage(T) in tiffCrop.cpp
 *
 *
 * @param  in the input TIFF image structure as returned by tifOpen()
 * @param  out the output TIFF image structure as returned by tifOpenWrite()
 * @param  width the width of the crop region
 * @param  length the height of the crop region
 * @return  1 on sucess, 0 on failure
 **/
int writeCroppedHeader(TIFF *in, TIFF *out, uint32 width, uint32 length)
{
  uint16 bps = 0,
      spp = 0;
  struct cpTag* p;
  uint16 config;
  uint16 compression;
  uint16 predictor;
  uint16 fillorder;
  uint16 orientation;
  uint32 rowsperstrip;
  uint32 g3opts;
  int  quality   = 75;   /* JPEG quality */
  int  jpegcolormode = JPEGCOLORMODE_RGB;
  int  outtiled  = TIFFIsTiled(out);
  uint32 tilewidth;
  uint32 tilelength;
  uint16 pagenum   = 0;
  uint16 total_pages     = 0;

  /* Set Default values */
  if( TIFFGetField(out, TIFFTAG_GROUP3OPTIONS, &g3opts) == 0)
    g3opts = (uint32) -1;
  if( TIFFGetField(out, TIFFTAG_PREDICTOR, &predictor) == 0)
    predictor = PREDICTOR_HORIZONTAL;
  if( TIFFGetField(out, TIFFTAG_FILLORDER, &fillorder) == 0)
    fillorder = FILLORDER_MSB2LSB;
  if( TIFFGetField(out, TIFFTAG_PLANARCONFIG, &config) == 0)
    config = (uint16)-1;
  if( TIFFGetField(out, TIFFTAG_COMPRESSION, &compression) == 0)
    compression = (uint16)-1;
  if( TIFFGetField(out, TIFFTAG_PREDICTOR, &predictor) == 0 )
    predictor = (uint16)-1;
  if( TIFFGetField(out, TIFFTAG_TILEWIDTH, &tilewidth) == 0 )
    tilewidth = (uint32)-1;
  if( TIFFGetField(out, TIFFTAG_TILELENGTH, &tilelength) == 0)
    tilelength = (uint32)-1;
  if( TIFFGetField(out, TIFFTAG_ROWSPERSTRIP, &rowsperstrip) == 0 )
    rowsperstrip = 0;

  /* Set the output values */
  TIFFGetField(in, TIFFTAG_BITSPERSAMPLE, &bps);
  TIFFGetField(in, TIFFTAG_SAMPLESPERPIXEL, &spp);

  TIFFSetField(out, TIFFTAG_IMAGEWIDTH, width);
  TIFFSetField(out, TIFFTAG_IMAGELENGTH, length);
  TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, bps);
  TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, spp);


  if( compression == (uint16)-1 )
    TIFFSetField(out, TIFFTAG_COMPRESSION, compression);
  else
  {
    CopyField(TIFFTAG_COMPRESSION, compression);
  }

  if (compression == COMPRESSION_JPEG)
  {
    uint16 input_compression, input_photometric;

    if (TIFFGetField(in, TIFFTAG_COMPRESSION, &input_compression)
        && input_compression == COMPRESSION_JPEG)
    {
      TIFFSetField(in, TIFFTAG_JPEGCOLORMODE, JPEGCOLORMODE_RGB);
    }
    if (TIFFGetField(in, TIFFTAG_PHOTOMETRIC, &input_photometric))
    {
      if(input_photometric == PHOTOMETRIC_RGB)
      {
        if (jpegcolormode == JPEGCOLORMODE_RGB)
          TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_YCBCR);
        else
          TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
      }
      else
        TIFFSetField(out, TIFFTAG_PHOTOMETRIC, input_photometric);
    }
  }

  else if (compression == COMPRESSION_SGILOG || compression == COMPRESSION_SGILOG24)
    TIFFSetField(out, TIFFTAG_PHOTOMETRIC, spp == 1 ? PHOTOMETRIC_LOGL : PHOTOMETRIC_LOGLUV);
  else
    CopyTag(TIFFTAG_PHOTOMETRIC, 1, TIFF_SHORT);
  if (fillorder != 0)
    TIFFSetField(out, TIFFTAG_FILLORDER, fillorder);
  else
    CopyTag(TIFFTAG_FILLORDER, 1, TIFF_SHORT);

  /*
   * Will copy `Orientation' tag from input image
   */
  TIFFGetFieldDefaulted(in, TIFFTAG_ORIENTATION, &orientation);
  switch (orientation)
  {
    case ORIENTATION_BOTRIGHT:
    case ORIENTATION_RIGHTBOT: /* XXX */
      TIFFWarning(TIFFFileName(in), "using bottom-left orientation");
      orientation = ORIENTATION_BOTLEFT;
      /* fall thru... */
    case ORIENTATION_LEFTBOT: /* XXX */
    case ORIENTATION_BOTLEFT:
      break;
    case ORIENTATION_TOPRIGHT:
    case ORIENTATION_RIGHTTOP: /* XXX */
    default:
      TIFFWarning(TIFFFileName(in), "using top-left orientation");
      orientation = ORIENTATION_TOPLEFT;
      /* fall thru... */
    case ORIENTATION_LEFTTOP: /* XXX */
    case ORIENTATION_TOPLEFT:
      break;
  }
  TIFFSetField(out, TIFFTAG_ORIENTATION, orientation);

  /*
   * Choose tiles/strip for the output image according to
   * the command line arguments (-tiles, -strips) and the
   * structure of the input image.
   */
  if (outtiled)
  {
    if( tilewidth == (uint32)-1 )
      TIFFGetField(in, TIFFTAG_TILEWIDTH, &tilewidth);
    if( tilelength == (uint32)-1 )
      TIFFGetField(in, TIFFTAG_TILELENGTH, &tilelength);

    if (tilewidth > width)
      tilewidth = width;
    if (tilelength > length)
      tilelength = length;

    TIFFDefaultTileSize(out, &tilewidth, &tilelength);
    TIFFSetField(out, TIFFTAG_TILEWIDTH, tilewidth);
    TIFFSetField(out, TIFFTAG_TILELENGTH, tilelength);
  }
  else
  {
    /*
     * RowsPerStrip is left unspecified: use either the
     * value from the input image or, if nothing is defined,
     * use the library default.
     */
    if (rowsperstrip == 0 )
    {
      if (!TIFFGetField(in, TIFFTAG_ROWSPERSTRIP, &rowsperstrip))
      {
        rowsperstrip = TIFFDefaultStripSize(out, rowsperstrip);
      }
      if (rowsperstrip > length)
        rowsperstrip = length;
    }
    else if (rowsperstrip == (uint32)-1 )
      rowsperstrip = length;

    TIFFSetField(out, TIFFTAG_ROWSPERSTRIP, rowsperstrip);
  }

  /* Set Other configuration */
  if (config != (uint16) -1)
    TIFFSetField(out, TIFFTAG_PLANARCONFIG, config);
  else
    CopyField(TIFFTAG_PLANARCONFIG, config);

  if (spp <= 4)
    CopyTag(TIFFTAG_TRANSFERFUNCTION, 4, TIFF_SHORT);
  CopyTag(TIFFTAG_COLORMAP, 4, TIFF_SHORT);



  /* SMinSampleValue & SMaxSampleValue */
  switch (compression)
  {
    case COMPRESSION_JPEG:
      TIFFSetField(out, TIFFTAG_JPEGQUALITY, quality);
      TIFFSetField(out, TIFFTAG_JPEGCOLORMODE, jpegcolormode);
      break;
    case COMPRESSION_LZW:
    case COMPRESSION_ADOBE_DEFLATE:
    case COMPRESSION_DEFLATE:
      if (predictor != (uint16)-1)
        TIFFSetField(out, TIFFTAG_PREDICTOR, predictor);
      else
        CopyField(TIFFTAG_PREDICTOR, predictor);
      break;
    case COMPRESSION_CCITTFAX3:
    case COMPRESSION_CCITTFAX4:
      if (compression == COMPRESSION_CCITTFAX3)
      {
        if (g3opts != (uint32) -1)
          TIFFSetField(out, TIFFTAG_GROUP3OPTIONS, g3opts);
        else
          CopyField(TIFFTAG_GROUP3OPTIONS, g3opts);
      }
      else
        CopyTag(TIFFTAG_GROUP4OPTIONS, 1, TIFF_LONG);

      CopyTag(TIFFTAG_BADFAXLINES, 1, TIFF_LONG);
      CopyTag(TIFFTAG_CLEANFAXDATA, 1, TIFF_LONG);
      CopyTag(TIFFTAG_CONSECUTIVEBADFAXLINES, 1, TIFF_LONG);
      CopyTag(TIFFTAG_FAXRECVPARAMS, 1, TIFF_LONG);
      CopyTag(TIFFTAG_FAXRECVTIME, 1, TIFF_LONG);
      CopyTag(TIFFTAG_FAXSUBADDRESS, 1, TIFF_ASCII);
      break;
  }

  {
    uint32 len32;
    void** data;
    if (TIFFGetField(in, TIFFTAG_ICCPROFILE, &len32, &data))
      TIFFSetField(out, TIFFTAG_ICCPROFILE, len32, data);
  }

  {
    uint16 ninks;
    const char* inknames;
    if (TIFFGetField(in, TIFFTAG_NUMBEROFINKS, &ninks))
    {
      TIFFSetField(out, TIFFTAG_NUMBEROFINKS, ninks);
      if (TIFFGetField(in, TIFFTAG_INKNAMES, &inknames))
      {
        int inknameslen = std::strlen(inknames) + 1;
        const char* cp = inknames;
        while (ninks > 1)
        {
          cp = std::strchr(cp, '\0');
          if (cp)
          {
            cp++;
            inknameslen += (strlen(cp) + 1);
          }
          ninks--;
        }
        TIFFSetField(out, TIFFTAG_INKNAMES, inknameslen, inknames);
      }
    }
  }

  {
    unsigned short pg0, pg1;
    if (TIFFGetField(in, TIFFTAG_PAGENUMBER, &pg0, &pg1))
    {
      TIFFSetField(out, TIFFTAG_PAGENUMBER, pagenum, total_pages);
    }
  }

  for (p = tags; p < &tags[NTAGS]; p++)
    CopyTag(p->tag, p->count, p->type);

  return 1;
}

} // namespace image
} // namespace sedeen
