#include "image/tiff/TIFFWriter.h"

#include <cassert>
#include <cmath>
#include <cstring>

#include <boost/thread/thread.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include "global/Debug.h"
#include "global/UnitDefs.h"
#include "global/geometry/Point.h"
#include "global/geometry/Rect.h"
#include "global/geometry/Size.h"
#include "global/geometry/SizeF.h"
#include "image/io/DecodeRequest.h"
#include "image/io/Exceptions.h"
#include "image/io/ImageAccessor.h"
#include "image/io/ImageParser.h"
#include "image/io/ImagePyramid.h"
#include "image/io/ThreadedRegionBlock.h"
#include "image/tiff/TIFFCrop.h"

namespace sedeen {
namespace image {
namespace {

// JPEG compression requires multiples of 8 rows per strip
const boost::uint32_t DefaultRowsPerStrip = 8;

// Default compression type
const boost::uint32_t DefaultTileSize = 256;

// Default compression type
const boost::uint32_t DefaultCompression = COMPRESSION_LZW;

// Library version
const char LibraryVersion[] = "PathCore TIFFWriter 1.0";

// Deleter function for TIFF header
void CloseTiffHeader(tifhdr_t *tif) {
  tifClose(tif);
}

// Gets a TIFF tag from the current directory
template <class T>
T getTag(TIFF *tif, boost::uint32_t tag) {
  assert(tif);
  T tmp;
  if (1 == TIFFGetField(tif, tag, &tmp)) return tmp;
  else return 0;
}

// Calculates bits per sample from source
// \return bits per sample \a source or 0 on error
boost::uint16_t tiffBitsPerSample(const ImageAccessor &source) {
  const ImageParser &parser = source.imageParser();
  const boost::uint16_t bpc = parser.bpc(0);
  for (int comp = 1; comp < parser.components(); ++comp) {
    if (parser.bpc(comp) != bpc) {
      debug::assert_x(false, "Bits per components must be the same",
                      __FUNCTION__, __LINE__);
      return 0;
    }
  }
  return bpc;
}

// Converts image color to TIFF color
// The equlivalent TIFF color for \a source or 0 on error
boost::uint32_t tiffPhotometric(const ImageAccessor &source) {
  switch(source.imageParser().color()) {
    case ARGB_8:
      // Fall-through
    case ARGB_16:
      // Fall-through
    case RGB_8:
      // Fall-through
    case RGB_16:
      return PHOTOMETRIC_RGB;

    case Gray_8:
      // Fall-through
    case Gray_16:
      return PHOTOMETRIC_MINISBLACK;

    case Alpha:
    default:
      debug::assert_x(false, "Invalid color type", __FUNCTION__, __LINE__);
      return 0;
  }
}

} // namespace

class TIFFWriter::Private {
 public:
  Private()
      : tif(),
        mutex(),
        m_thread(),
        abort(false),
        done(true),
        success(true),
        progress_steps_total(100),
        progress_steps_complete(0),
        stripped(false),
        flat(false),
        tile_size(0),
        jpeg_quality(0),
        compression_mode(0) {
  }

  boost::shared_ptr<tifhdr_t> tif;

  boost::recursive_mutex mutex;

  boost::thread m_thread;

  bool abort; // TODO: make atomic - available in boost  v1.5+

  bool done; // TODO: make atomic

  bool success; // TODO: make atomic

  int progress_steps_total; // TODO: make atomic

  int progress_steps_complete; // TODO: make atomic

  bool stripped;

  bool flat;

  boost::uint32_t tile_size;

  boost::uint16_t jpeg_quality;

  boost::uint16_t compression_mode;
}; // class TIFFWriter::Private

TIFFWriter::TIFFWriter(const std::string &filename) : d(0) {
  tifhdr_t *tif = tifOpen(filename.c_str(), "wm");
  if (0 == tif) throw FileAccessError(filename);
  d.reset(new Private());
  d->tif.reset(tif, CloseTiffHeader);
}

TIFFWriter::~TIFFWriter() {
  // Get handle
  assert(d && d->tif);
  assert(d->tif.unique());

  abort();
  d->m_thread.join();
}



void TIFFWriter::setTiled(boost::uint32_t tilesize) {
  assert(d && d->tif);
  boost::lock_guard<boost::recursive_mutex> lock(d->mutex);

  d->tile_size = tilesize;
  d->stripped = false;
}

void TIFFWriter::setStripped(bool enable) {
  assert(d && d->tif);
  boost::lock_guard<boost::recursive_mutex> lock(d->mutex);

  d->stripped = enable;
  d->tile_size = 0;
}

void TIFFWriter::setFlat(bool enable) {
  assert(d && d->tif);
  boost::lock_guard<boost::recursive_mutex> lock(d->mutex);

  d->flat = enable;
}

void TIFFWriter::setCompression(boost::uint16_t mode, int jpegquality) {
  assert(d && d->tif);
  boost::lock_guard<boost::recursive_mutex> lock(d->mutex);

  d->compression_mode = mode;
  d->jpeg_quality = jpegquality;
}

void TIFFWriter::abort() {
  assert(d && d->tif);
  d->abort = true;
}

bool TIFFWriter::aborted() const {
  assert(d && d->tif);
  return d->abort;
}

bool TIFFWriter::success() const {
  assert(d && d->tif);
  return d->success;
}

bool TIFFWriter::isDone() const {
  assert(d && d->tif);
  return d->done;
}

int TIFFWriter::progress() const {
  assert(d && d->tif);
  assert(d->progress_steps_total);
  return static_cast<int>(100.0 * d->progress_steps_complete /
                          d->progress_steps_total);
}

void TIFFWriter::writeImage(const ImageAccessor &source, const Rect &source_area) {
  // Lock
  boost::lock_guard<boost::recursive_mutex> lock(d->mutex);

  return writeImage(source, source_area, size(source_area));
}

void TIFFWriter::writeImage(const ImageAccessor &source, const Rect &source_area,
                            const Size &output_size) {
  assert(0 < output_size.width());
  assert(0 < output_size.height());

  // Complete any previous process
  abort();
  d->m_thread.join();
  assert(isDone());

  d->abort = false;
  d->done = false;
  d->success = false;
  d->progress_steps_total = 100;
  d->progress_steps_complete = 0;
  d->m_thread = boost::thread(boost::bind(&TIFFWriter::run, this,
                              source, source_area, output_size));

}

bool TIFFWriter::run(const ImageAccessor &source, const Rect &source_area,
                            const Size &output_size) {

  // Lock
  boost::lock_guard<boost::recursive_mutex> lock(d->mutex);
  bool success = true;

  // Check that source region is valid
  Rect source_image(Point(0, 0), source.getDimensions(0));
  if (!source_image.contains(source_area))
    return false;

  // If we need a flat image, do so
  if (d->flat) {
    success = writeResolution(source, 0, source_area, output_size);
  }

  // Otherwise write all resolutions
  else {
    ImagePyramidPtr pyramid = source.imageParser().pyramid();
    for (int level = 0; level < source.getNumResolutionLevels(); ++level) {
      // Convert coordinates
      Rect source_rect = mapRegionToAlternateResolution(source_area,
                                                        0, level, *pyramid);
      Rect output_rect = mapRegionToAlternateResolution(Rect(Point(0, 0),
                                                             output_size),
                                                        0, level, *pyramid);
      // Crop
      success = writeResolution(source, level, source_rect, size(output_rect));
      if(!success) break;
    }
  }

   // return
   d->done = true;
   d->success = success && !d->abort;
   return d->success;
}

bool TIFFWriter::writeResolution(ImageAccessor source, int resolution,
                            const Rect &source_area, const Size &output_size) {
  // Write image
  if (!writeHeader(source, resolution, output_size))
    return false;
  if (!writeData(source, source_area, resolution, output_size))
    return false;

  return true;
}

bool TIFFWriter::writeHeader(const ImageAccessor &source, int resolution,
                             const Size &output_size) {
  // Get handle
  assert(d && d->tif && d->tif->tfile);
  assert(!isEmpty(output_size));
  assert(resolution < source.getNumResolutionLevels());
  TIFF *out = d->tif->tfile;

  // Lock
  boost::lock_guard<boost::recursive_mutex> lock(d->mutex);

  // Get image properties
  boost::uint16_t bps = tiffBitsPerSample(source),
      spp = source.imageParser().components(),
      photometric = tiffPhotometric(source);
  boost::uint32_t compression = (d->compression_mode ?
                                 d->compression_mode : DefaultCompression);
  std::string description = source.imageParser().description();
  if (0 == bps) return false;
  if (0 == spp) return false;
  if (0 == photometric) return false;

  // Only one must be non zero ( tiled OR stripped)
  assert(d->stripped ^ static_cast<bool>(d->tile_size));

  if (d->stripped) {
    TIFFSetField(out, TIFFTAG_ROWSPERSTRIP, DefaultRowsPerStrip);
  } else {
    TIFFSetField(out, TIFFTAG_TILEWIDTH, d->tile_size);
    TIFFSetField(out, TIFFTAG_TILELENGTH, d->tile_size);
  }

  // Set Pixel size for baseline image
  if (!isEmpty(source.getPixelSize()) && (0 == resolution)) {
    float xres = static_cast<float>(metricToMetric(
        source.getPixelSize().width(),
        length::um, length::cm));
    float yres = static_cast<float>(metricToMetric(
        source.getPixelSize().height(),
        length::um, length::cm));
    xres = 1.0 / xres;
    yres = 1.0 / yres;
    TIFFSetField(out, TIFFTAG_XRESOLUTION, xres);
    TIFFSetField(out, TIFFTAG_YRESOLUTION, yres);
    TIFFSetField(out, TIFFTAG_RESOLUTIONUNIT, RESUNIT_CENTIMETER);
  }

  // Set other tags
  TIFFSetField(out, TIFFTAG_IMAGEWIDTH, output_size.width());
  TIFFSetField(out, TIFFTAG_IMAGELENGTH, output_size.height());
  TIFFSetField(out, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
  TIFFSetField(out, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
  TIFFSetField(out, TIFFTAG_FILLORDER, FILLORDER_MSB2LSB);
  TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, spp);
  TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, bps);
  TIFFSetField(out, TIFFTAG_COMPRESSION, compression);
  TIFFSetField(out, TIFFTAG_PHOTOMETRIC, photometric);
  TIFFSetField(out, TIFFTAG_IMAGEDESCRIPTION, description.c_str());
  TIFFSetField(out, TIFFTAG_SOFTWARE, LibraryVersion);

  // Ensure compression is supported and apply special configurations
  switch(compression) {
    case COMPRESSION_NONE:
      break;

    case COMPRESSION_DEFLATE:
      // Fall-through
    case COMPRESSION_LZW:
      TIFFSetField(out, TIFFTAG_PREDICTOR, PREDICTOR_HORIZONTAL);
      break;

    case COMPRESSION_JPEG:
      TIFFSetField(out, TIFFTAG_JPEGQUALITY, 75);
      TIFFSetField(out, TIFFTAG_JPEGCOLORMODE, JPEGCOLORMODE_RGB);
      break;

    case COMPRESSION_JP2000:
      // Fall-though
    case COMPRESSION_OJPEG: // overridden by JPEG in TIFF Tech Note 2
      // Fall-though
    case COMPRESSION_CCITTRLE:
      // Fall-though
    case COMPRESSION_CCITTFAX3:
      // Fall-though
    case COMPRESSION_CCITTFAX4:
      // Fall-though
    case COMPRESSION_NEXT:
      // Fall-though
    case COMPRESSION_CCITTRLEW:
      // Fall-though
    case COMPRESSION_PACKBITS:
      // Fall-though
    case COMPRESSION_THUNDERSCAN:
      // Fall-though
    case COMPRESSION_IT8CTPAD:
      // Fall-though
    case COMPRESSION_IT8LW:
      // Fall-though
    case COMPRESSION_IT8MP:
      // Fall-though
    case COMPRESSION_IT8BL:
      // Fall-though
    case COMPRESSION_PIXARFILM:
      // Fall-though
    case COMPRESSION_PIXARLOG:
      // Fall-though
    case COMPRESSION_ADOBE_DEFLATE:
      // Fall-though
    case COMPRESSION_DCS:
      // Fall-though
    case COMPRESSION_JBIG:
      // Fall-though
    case COMPRESSION_SGILOG:
      // Fall-though
    case COMPRESSION_SGILOG24:
      debug::warning_x(false, "Unsupported compression format",
                       __FUNCTION__, __LINE__);
      return false;

    default:
      debug::warning_x(false, "Unknown compression format",
                       __FUNCTION__, __LINE__);
      return false;
  }
  return true;
}

bool TIFFWriter::writeData(const ImageAccessor &source, const Rect &source_area,
                           int resolution, const Size &output_size) {
  // Get handle
  assert(d && d->tif && d->tif->tfile);
  assert(!isEmpty(source_area));
  assert(!isEmpty(output_size));
  TIFF *out = d->tif->tfile;

  //Lock
  boost::lock_guard<boost::recursive_mutex> lock(d->mutex);

  // Write Image
  bool success = false;
  if (TIFFIsTiled(out)) {
    success = writeTiledData(source, source_area, resolution, output_size);
  } else {
    success = writeStrippedData(source, source_area, resolution, output_size);
  }

  // Close directory
  if (success) {
    success = (1 == TIFFWriteDirectory(d->tif->tfile));
  }

  return success;
}

bool TIFFWriter::writeTiledData(const ImageAccessor &source, const Rect &source_area,
                                int resolution, const Size &output_size) {
  // Get handle
  assert(d && d->tif && d->tif->tfile);
  assert(!isEmpty(source_area));
  assert(!isEmpty(output_size));
  assert(TIFFIsTiled(d->tif->tfile));

  //Lock
  boost::lock_guard<boost::recursive_mutex> lock(d->mutex);

  // Get output properties
  TIFF *out = d->tif->tfile;
  const boost::uint16_t config =
      getTag<boost::uint16_t>(out, TIFFTAG_PLANARCONFIG);
  const boost::uint16_t spp =
      getTag<boost::uint16_t>(out, TIFFTAG_SAMPLESPERPIXEL);
  const boost::uint16_t bps =
      getTag<boost::uint16_t>(out, TIFFTAG_BITSPERSAMPLE);

  const boost::uint32_t tilewidth =
      getTag<boost::uint32_t>(out, TIFFTAG_TILEWIDTH);
  const boost::uint32_t tileheight =
      getTag<boost::uint32_t>(out, TIFFTAG_TILELENGTH);
  const boost::uint32_t total_cols = static_cast<boost::uint32_t>(std::ceil(
      static_cast<double>(output_size.width()) / tilewidth));
  const boost::uint32_t total_rows = static_cast<boost::uint32_t>(std::ceil(
      static_cast<double>(output_size.height()) / tileheight));

  // Determine scaling properties
  const SizeF scale(
      static_cast<double>(output_size.width()) / source_area.width(),
      static_cast<double>(output_size.height()) / source_area.height());

  // Determine the size of block to read from source
  // We need fractional precision to ensure accuracy if scaling is applied
  const double source_block_width = tilewidth / scale.width();
  const double source_block_height = tileheight / scale.width();
  Size block_size(static_cast<boost::uint32_t>(std::ceil(source_block_width)),
                  static_cast<boost::uint32_t>(std::ceil(source_block_height)));

  // Top left coordinate of the output tile; is passed to TIFFWriteTile()
  // To be incremtented in the main loops
  boost::uint32_t tlx = 0;
  boost::uint32_t tly = 0;

  // Updated progress step
  d->progress_steps_total += total_rows * total_cols;

  // Fill tiles by rows and columns
  for (uint32 row = 0; row < total_rows; tly += tileheight, ++row) {
    tlx = 0; // reset the tile coordinate counter
    for (uint32 col = 0; col < total_cols; tlx += tilewidth, ++col) {
      // abort?
      if (d->abort)
        return false;

      // progress
      d->progress_steps_complete++;

      // Calculate source area needed for output tile
      boost::uint32_t x = static_cast<boost::uint32_t>(
          std::floor(source_block_width * col));
      boost::uint32_t y = static_cast<boost::uint32_t>(
          std::floor(source_block_height * row));
      Rect tile(Point(source_area.x() + x, source_area.y() + y), block_size);

      // Get output tile image and scale
      ThreadedRegionBlock blocker(source, resolution, tile);
      RawImage block_image = blocker.getImage();
      if (block_image.isNull()) {
        debug::warning("Could not get source tile.", __FUNCTION__, __LINE__);
        return false;
      }

      // Scale image if required
      if (SizeF(1,1) != scale) {
        RawImage target(Size(tilewidth, tileheight), block_image);
        target.copy(target.rect(), block_image, block_image.rect());
        block_image = target;
      }
      assert(static_cast<boost::uint32_t>(block_image.width()) == tilewidth);
      assert(static_cast<boost::uint32_t>(block_image.height()) == tileheight);

      // Get TIFF buffer
      assert(static_cast<boost::uint32_t>(block_image.bytes()) ==
             TIFFTileSize(out));
      tdata_t buffer = const_cast<tdata_t>(block_image.data().get());

      // Write tile to file
      switch(config) {
        case PLANARCONFIG_CONTIG:
          TIFFWriteTile(out, buffer, tlx, tly, 0, 0);
          break;

        case PLANARCONFIG_SEPARATE:
          for (int s = 0; s < spp; ++s) {
            tdata_t component = tifGetComponent(s, buffer,
                                                tilewidth, tileheight,
                                                spp, bps);
            TIFFWriteTile(out, component, tlx, tly, 0, s);
            _TIFFfree(component);
          }
          break;

        default:
          debug::warning("Invalid planar configuration.", __FUNCTION__,
                         __LINE__);
          return false;
      }
    }
  }
  return true;
}

bool TIFFWriter::writeStrippedData(const ImageAccessor &source,
                                   const Rect &source_area, int resolution,
                                   const Size &output_size) {
  // Get handle
  assert(d && d->tif && d->tif->tfile);
  assert(!isEmpty(source_area));
  assert(!isEmpty(output_size));
  assert(!TIFFIsTiled(d->tif->tfile));
  TIFF *out = d->tif->tfile;

  //Lock
  boost::lock_guard<boost::recursive_mutex> lock(d->mutex);

  // Get output properties
  const boost::uint16_t config =
      getTag<boost::uint16_t>(out, TIFFTAG_PLANARCONFIG);
  const boost::uint16_t spp =
      getTag<boost::uint16_t>(out, TIFFTAG_SAMPLESPERPIXEL);
  const boost::uint16_t bps =
      getTag<boost::uint16_t>(out, TIFFTAG_BITSPERSAMPLE);

  const boost::uint32_t rowsperstrip =
      getTag<boost::uint32_t>(out, TIFFTAG_ROWSPERSTRIP);
  const boost::uint32_t total_strips = static_cast<boost::uint32_t>(std::ceil(
      static_cast<double>(output_size.height()) / rowsperstrip));

  // Determine scaling properties
  const SizeF scale(
      static_cast<double>(output_size.width()) / source_area.width(),
      static_cast<double>(output_size.height()) / source_area.height());

  // Determine output strip size
  Size strip_size(output_size.width(), rowsperstrip);
  //int progress = 0;

  // Determine the size of strips to read from source
  // We need fractional precision to ensure accuracy if scaling is applied
  const double source_rows_per_output_strip = rowsperstrip / scale.width();
  Size block_size(source_area.width(),
                  static_cast<boost::int32_t>(std::ceil(
                      source_rows_per_output_strip)));


  // Updated progress step
  d->progress_steps_total += total_strips;

  // Fill output image
  for (uint32 strip = 0; strip < total_strips; ++strip) {
      // abort?
      if (d->abort)
        return false;

    // progress
      d->progress_steps_complete++;

    // Get output tile bourdaries
    boost::uint32_t y = static_cast<boost::uint32_t>(std::floor(
        source_rows_per_output_strip * strip));
    Rect block(Point(source_area.x(), source_area.y() + y), block_size);

    // Get output tile image
    ThreadedRegionBlock blocker(source, resolution, block);
    RawImage block_image = blocker.getImage();
    if (block_image.isNull()) {
      debug::warning("Could not get source strip.", __FUNCTION__, __LINE__);
      return false;
    }

    // Scale block if required
    if (SizeF(1,1) != scale) {
      RawImage target(strip_size, block_image);
      target.copy(target.rect(), block_image, block_image.rect());
      block_image = target;
    }
    assert(block_image.width() == strip_size.width());
    assert(block_image.height() == strip_size.height());

    // Get stip info
    tstrip_t stripno = TIFFComputeStrip(out, y, 0);
    tsize_t stripsize = TIFFStripSize(out);
    assert(block_image.bytes() == static_cast<uint64_t>(stripsize));
    assert(stripno == strip);

    // Get TIFF buffer
    tdata_t buffer = const_cast<tdata_t>(block_image.data().get());

    // Write strips
    switch(config) {
      case PLANARCONFIG_CONTIG:
        TIFFWriteEncodedStrip(out, stripno, buffer, stripsize);
        break;

      case PLANARCONFIG_SEPARATE:
        for (uint32 compno = 0; compno < spp; ++compno) {
          tdata_t component = tifGetComponent(
              compno, buffer,
              strip_size.width(), strip_size.height(),
              spp, bps);
          TIFFWriteEncodedStrip(out, stripno, component, stripsize);
          _TIFFfree(component);
        }
        break;

      default:
        debug::warning("Invalid planar configuration.", __FUNCTION__, __LINE__);
        return false;
    }
  }
  return true;
}

} // namespace image
} // namespace sedeen
