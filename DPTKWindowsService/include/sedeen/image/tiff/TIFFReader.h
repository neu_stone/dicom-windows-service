#ifndef SEDEEN_SRC_IMAGE_TIFF_TIFFREADER_H
#define SEDEEN_SRC_IMAGE_TIFF_TIFFREADER_H

#include <string>
#include <cstdint>
#include <memory>

#include "image/DllApi.h"

namespace sedeen {

class Point;
class Rect;
class Size;
class SizeF;

namespace image {

class RawImage;

/// Simplifies reading TIFF files of many colors and configurations
class SEDEEN_IMAGE_API TIFFReader {
 public:
  /// Opens a TIFF file for reading. Throws FileAccessError exception on error.
  //
  /// \param filename
  /// the file to open
  ///
  /// \param mode
  /// TIFF open mode, see TIFFOpen()
  TIFFReader(const std::string& filename);

  /// Copy constructor
  TIFFReader(const TIFFReader& other);

  /// Destructor, close the file if open.
  ~TIFFReader();

  /// Assignment operator
  TIFFReader& operator=(TIFFReader rhs);

  /// No-fail swap operator.
  //
  /// \param other
  /// TIFFReader to swap with *this.
  ///
  /// \return
  /// *this
  TIFFReader& swap(TIFFReader& other);

  /// Gets a 16 bit value tiff tag from the file
  //
  /// \param tdir
  /// the TIFF directory
  ///
  /// \param tag
  /// the property to retrieve
  ///
  /// \return
  /// tag value
  uint16_t getTag16(uint16_t tdir, uint32_t tag) const;

  /// Gets a string valued TIFF tag from the file
  //
  /// \copydoc getTag16()
  std::string getTagString(uint16_t tdir, uint32_t tag) const;

  /// Gets a 32 bit valued tiff tag from the file
  //
  /// \copydoc getTag16()
  uint32_t getTag32(uint16_t tdir, uint32_t tag) const;

  /// Gets a floating point valued tiff tag from the file
  //
  /// \copydoc getTag16()
  float getTagFloat(uint16_t tdir, uint32_t tag) const;

  /// Gets a double valued tiff tag from the file
  //
  /// \copydoc getTag16()
  double getTagDouble(uint16_t tdir, uint32_t tag) const;

  /// Returns the number of directories in the image
  uint16_t numDirectories() const;

  /// Returns filename bound to this instance
  std::string filename() const;

  /// Returns the size of a directory
  //
  /// \return
  /// image dimensions or Null on error
  Size dimensions(uint16_t tdir) const;

  /// Returns the size for tile size of a directory
  //
  /// \return
  /// tile dimensions or 0 if image is not tiled
  Size tileSize(uint16_t tdir) const;

  /// Returns the rows per strip of a directory
  //
  /// \return
  /// rows per strip or 0 if image is not tiled
  uint32_t rowsPerStrip(uint16_t tdir) const;

  /// Gets the directory number of the largest image by area.
  //
  /// \return
  /// directory number
  uint16_t getLargestDirectory() const;

  /// Extract an arbitrary region of data as an RGB image
  //
  /// The output is always stored in continuous planer configration, i.e. RGB
  /// RGB RGB ... RGB in the case of RGB images.
  ///
  /// \param tdir
  /// The TIFF directory to read (zero for first directory)
  ///
  /// \param region
  /// the region to extract , must have non-negative origin
  RawImage getRegion(uint16_t tdir, const Rect& region) const;

  /// Extract a tile of data as an RGB image
  //
  /// Extracts a tile using TIFFReadTile and additionaly ensures that the
  /// returned buffer has unit sampling. (i.e. YCC is converted to RGB).
  ///
  /// The output is always stored in continuous planer configration, i.e. RGB
  /// RGB RGB ... RGB in the case of RGB images.
  ///
  /// \note
  /// This function is only suitable for tiled images  but is  slightly more
  /// efficient that getRegion() for this purpose.
  ///
  /// \note
  /// Bits-per-sample may be 8, 16 or 32 however, use caution where reading the
  /// buffer.
  ///
  /// \param tdir
  /// The TIFF directory to read
  ///
  /// \param point
  /// a position within the tile on interest, must NOT be negative
  ///
  /// \return
  /// RawImage to the tile or NULL RawImage on error
  RawImage getTile(uint16_t tdir, const Point& point) const;

  /// Extract a strip of data as an RGB image.
  //
  /// Extracts a strip from file using TIFFEncodedStrip() and additionaly
  /// ensures that the returned buffer has unit sampling. (i.e. YCC is converted
  /// to RGB).
  ///
  /// The output is always stored in continuous planer configration, i.e. RGB
  /// RGB RGB ... RGB in the case of RGB images.
  ///
  /// \note
  /// This function is only suitable for stripped images, but is slightly more
  /// efficient that getRegion() for this purpose.
  ///
  /// \note
  /// Bits-per-sample may be 8, 16 or 32 however, use caution where reading the
  /// buffer.
  ///
  /// \sa TIFFReadEncodedStrip()
  ///
  /// \param tdir
  /// The TIFF directory to read
  ///
  /// \param row
  /// the row on interest, must be >= 0
  ///
  /// \param sample
  /// used only if data are organized in separate planes
  ///
  /// \return
  /// Pointer to a buffer holding the strip or 0 on error
  RawImage getStrip(uint16_t tdir, int row) const;

  /// Attmpets to extracts a region of interest for display.
  //
  /// Similar to getRegion() except that the returned buffer is converted to
  /// RGB, RGBA where possible. Grayscale images are not converted.
  ///
  /// There are some cases where a conversion is not possible at this time, see
  /// tifConvertForDisplay() for details. Where a conversion is not possible,
  /// the returned image is marked with Unknown format.
  ///
  /// \copydoc getRegion()
  RawImage getRegionForDisplay(uint16_t tdir, const Rect& region) const;

  /// Attmpets to convert \a image RGB or RGBA where possible.
  //
  /// Where a conversion is not required, the image is returned without
  /// modification. Grayscale images are returned without modification. If
  /// conversion is not possible, the image is returned without modificaion.
  ///
  /// \note
  /// \a tdir must be the same as that used to extract \a image, otherwise, the
  /// results are undefined.
  ///
  /// \note
  /// see tifConvertForDisplay() for limitations of color conversion.
  ///
  /// \param tdir
  /// the TIFF directory to read for color information
  ///
  /// \param image
  /// the image to convert
  RawImage convertForDisplay(const uint16_t tdir, RawImage image) const;

 private:
  class TIFFReaderPrivate;

  std::shared_ptr<TIFFReaderPrivate> d_;

}; // class TIFFReader

} // end namespace image
} // end namespace sedeen

#endif
