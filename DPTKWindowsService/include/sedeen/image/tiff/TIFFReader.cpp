#include "image/tiff/TIFFReader.h"

#include <cassert>

#include <boost/thread/locks.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include "global/geometry/Point.h"
#include "global/geometry/Rect.h"
#include "global/geometry/Size.h"
#include "image/io/Exceptions.h"
#include "image/buffer/RawImageFactory.h"
#include "image/tiff/TIFFCrop.h"
#include "image/tiff/TIFFParser.h"

namespace sedeen {
namespace image {
namespace {

// Deleter function for tiff allocated data
void DeleteTIFFBuffer(tdata_t buffer) {
  _TIFFfree(static_cast<void*>(buffer));
}

// Deleter function for tifhdr_t
void CloseTifHeader(tifhdr_t *tif) {
  tifClose(tif);
}

// Gets a TIFF tag
template <class T>
T getTag(TIFF *tif, uint16_t tdir, uint32_t tag) {
  assert(tif);
  assert(tdir < TIFFNumberOfDirectories(tif));

  // Failure to change directory means something has gone terribly wrong
  if (!TIFFSetDirectory(tif, tdir)) {
    throw FileAccessError("");
  }

  // Failure to read a tag may simply indicate the tag did not exist.
  T tag_value = 0;
  if (1 != TIFFGetField(tif, tag, &tag_value)) {
    // Clear anything that may have been erroneously written to tag_value
    tag_value = 0;
  }

  return tag_value;
}

// Create a RawImage from the given information
RawImage createImage(const TIFFReader &reader, uint16_t tdir,
                     tdata_t buffer,
                     uint32_t width, uint32_t height) {
  assert(tdir < reader.numDirectories());

  // Get image properties
  const Color color  = getColorFromTiff(reader, tdir);
  const uint16_t spp = reader.getTag16(tdir, TIFFTAG_SAMPLESPERPIXEL);
  const uint16_t bps = reader.getTag16(tdir, TIFFTAG_BITSPERSAMPLE);

  // Return Image Buffer
  RawDataPtr rawData(static_cast<void*>(buffer), DeleteTIFFBuffer);
  RawImageBufferPtr rawBuffer = createIntegerRawImageBuffer(rawData,
                                                            width, height,
                                                            spp, Interleaved,
                                                            bps, false, color);
  return RawImage(rawBuffer);
}

} // namespace

class TIFFReader::TIFFReaderPrivate {
 public:
  TIFFReaderPrivate(const std::string& filename)
      : tif(tifOpen(filename.c_str(), "rmCh"), CloseTifHeader),
        mutex() {
    if (0 == tif.get())
      throw FileAccessError(filename);
  }

  boost::shared_ptr<tifhdr_t> tif;
  boost::recursive_mutex mutex;
}; // class TIFFReader

TIFFReader::TIFFReader(const std::string& filename)
  : d_(new TIFFReaderPrivate(filename)) {
}

TIFFReader::TIFFReader(const TIFFReader& other) : d_(other.d_) {
}

TIFFReader& TIFFReader::operator=(TIFFReader rhs) {
  return this->swap(rhs);
}

TIFFReader& TIFFReader::swap(TIFFReader& other) {
  using std::swap;
  swap(d_, other.d_);
  return *this;
}

TIFFReader::~TIFFReader() {
  assert(d_.get() && d_->tif.get());
  boost::lock_guard<boost::recursive_mutex> lock(d_->mutex);
}

uint16_t TIFFReader::numDirectories() const {
  // Lock not required - using readonly tifhdr
  // Get handle
  assert(d_.get() && d_->tif.get());

  return d_->tif->ndirs;
}

std::string TIFFReader::filename() const {
  // Lock not required - using readonly tifhdr
  // Get handle
  assert(d_.get() && d_->tif.get());
  return TIFFFileName(d_->tif->tfile);
}

Size TIFFReader::tileSize(uint16_t tdir) const {
  // Lock not required - using readonly tifhdr
  // Get handle
  assert(d_.get() && d_->tif.get());
  assert(tdir < d_->tif->ndirs);

  return Size(d_->tif->tilew[tdir], d_->tif->tilel[tdir]);
}

uint32_t TIFFReader::rowsPerStrip(uint16_t tdir) const {
  // Lock not required - using readonly tifhdr
  // Get handle
  assert(d_.get() && d_->tif.get());
  assert(tdir < d_->tif->ndirs);

  return d_->tif->rowsperstrip[tdir];
}

Size TIFFReader::dimensions(uint16_t tdir) const {
  // Lock not required - using readonly tifhdr
  // Get handle
  assert(d_.get() && d_->tif.get());
  assert(tdir < d_->tif->ndirs);

  return Size(d_->tif->imgw[tdir], d_->tif->imgl[tdir]);
}

uint16_t TIFFReader::getLargestDirectory() const {
  // Lock - probably not required - just incase tifGetLargetsDirectory() needs
  // to modify the current directory
  boost::lock_guard<boost::recursive_mutex> lock(d_->mutex);

  // Get handle
  assert(d_.get() && d_->tif.get());

  return tifGetLargestDirectory(d_->tif->tfile);
}

RawImage TIFFReader::getRegionForDisplay(uint16_t tdir,
                                         const Rect& region) const {
  // Lock
  boost::lock_guard<boost::recursive_mutex> lock(d_->mutex);

  RawImage img = getRegion(tdir, region);
  return convertForDisplay(tdir, img);
}

RawImage TIFFReader::getRegion(uint16_t tdir, const Rect& region) const {
  // Lock
  boost::lock_guard<boost::recursive_mutex> lock(d_->mutex);

  // Get handle
  assert(d_.get() && d_->tif.get());
  assert(tdir < numDirectories());
  assert(0 <= origin(region).getX() && 0 <= origin(region).getY());

  // Decode image to buffer
  const tdata_t data = tifGetRegion(d_->tif.get(), tdir, region.x(), region.y(),
                                    xMax(region), yMax(region));

  // Return
  if (data) return createImage(*this, tdir, data,
                               region.width(), region.height());
  else return RawImage();
}

RawImage TIFFReader::getTile(uint16_t tdir, const Point& point) const {
  // Lock
  boost::lock_guard<boost::recursive_mutex> lock(d_->mutex);

  // Get handle
  assert(d_.get() && d_->tif.get());
  assert(0 <= point.getX() && 0 <= point.getY());
  assert(tdir < numDirectories());

  // Get tile
  tdata_t data = tifGetTile(d_->tif.get(), tdir, point.getX(), point.getY());
  uint32_t width = d_->tif->tilew[tdir];
  uint32_t height = d_->tif->tilel[tdir];

  // Return
  if (data) return createImage(*this, tdir, data, width, height);
  else return RawImage();
}

RawImage TIFFReader::getStrip(uint16_t tdir, int row) const {
  // Lock
  boost::lock_guard<boost::recursive_mutex> lock(d_->mutex);

  // Get handle
  assert(d_.get() && d_->tif.get());
  assert(0 <= row);
  assert(tdir < numDirectories());

  // Get strip
  tdata_t data = tifGetStrip(d_->tif.get(), tdir, row);
  uint32_t width = d_->tif->imgw[tdir];
  uint32_t height = d_->tif->rowsperstrip[tdir];

  // Return
  if (data) return createImage(*this, tdir, data, width, height);
  else return RawImage();
}

RawImage TIFFReader::convertForDisplay(const uint16_t tdir,
                                       RawImage image) const {
  // Lock
  boost::lock_guard<boost::recursive_mutex> lock(d_->mutex);

  // Get handle
  assert(d_.get() && d_->tif.get());
  assert(tdir < numDirectories());

  // Do we have a valid image
  if (image.isNull()) {
    return image;
  }

  // Get color information
  Color color   = getColorFromTiff(*this, tdir);
  uint32_t spp  = d_->tif->spp[tdir];
  if (PHOTOMETRIC_PALETTE == d_->tif->photometric[tdir]) {
    assert(1 == spp);
    spp = 3;
  }

  // Convert to 8bpp RGBA, RGBA or Grayscale
  tdata_t buffer = const_cast<tdata_t>(image.data().get());
  tdata_t converted = tifConvertForDisplay(d_->tif.get(), tdir, buffer,
                                           image.width(), image.height());

  // conversion not required
  if( converted == buffer ) {
    return image;
  }

  // data was converted
  else if( converted ) {
    color = (1 == spp ? Gray_8 : (3 == spp ? RGB_8: ARGB_8));
    RawDataPtr rawData(static_cast<void*>(converted), DeleteTIFFBuffer);
    return RawImage(rawData, image.size(), color, Interleaved);
  }

  // conversion failed; return buffer as is
  else {
    return image;
  }
}

std::string TIFFReader::getTagString(uint16_t tdir,
                           uint32_t tag) const {
  // Valid tags
  assert((TIFFTAG_ARTIST == tag) ||
         (TIFFTAG_COPYRIGHT == tag) ||
         (TIFFTAG_DATETIME == tag) ||
         (TIFFTAG_DOCUMENTNAME == tag) ||
         (TIFFTAG_HOSTCOMPUTER == tag) ||
         (TIFFTAG_IMAGEDESCRIPTION == tag) ||
         (TIFFTAG_INKNAMES == tag) ||
         (TIFFTAG_MAKE == tag) ||
         (TIFFTAG_MODEL == tag) ||
         (TIFFTAG_PAGENAME == tag) ||
         (TIFFTAG_SOFTWARE == tag) ||
         (TIFFTAG_TARGETPRINTER == tag));

  assert(d_.get() && d_->tif.get());
  assert(tdir < numDirectories());

  // Lock
  boost::lock_guard<boost::recursive_mutex> lock(d_->mutex);

  // Return value
  char *string = getTag<char*>(d_->tif->tfile, tdir, tag);
  if (0 == string) return std::string();
  else return std::string(string);
}

uint16_t TIFFReader::getTag16(uint16_t tdir,
                                     uint32_t tag) const {
  // Valid tags
  assert((TIFFTAG_BITSPERSAMPLE == tag) ||
         (TIFFTAG_CLEANFAXDATA == tag) ||
         (TIFFTAG_COMPRESSION == tag) ||
         (TIFFTAG_DATATYPE == tag) ||
         (TIFFTAG_DOTRANGE == tag) ||
         (TIFFTAG_FILLORDER == tag) ||
         (TIFFTAG_HALFTONEHINTS == tag) ||
         (TIFFTAG_INKSET == tag) ||
         (TIFFTAG_MATTEING == tag) ||
         (TIFFTAG_MAXSAMPLEVALUE == tag) ||
         (TIFFTAG_MINSAMPLEVALUE == tag) ||
         (TIFFTAG_ORIENTATION == tag) ||
         (TIFFTAG_PAGENUMBER == tag) ||
         (TIFFTAG_PHOTOMETRIC == tag) ||
         (TIFFTAG_PLANARCONFIG == tag) ||
         (TIFFTAG_PREDICTOR == tag) ||
         (TIFFTAG_RESOLUTIONUNIT == tag) ||
         (TIFFTAG_SAMPLEFORMAT == tag) ||
         (TIFFTAG_SAMPLESPERPIXEL == tag) ||
         (TIFFTAG_THRESHHOLDING == tag) ||
         (TIFFTAG_YCBCRPOSITIONING == tag) ||
         (TIFFTAG_YCBCRSUBSAMPLING == tag));

  assert(d_.get() && d_->tif.get());
  assert(tdir < numDirectories());

  // Lock
  boost::lock_guard<boost::recursive_mutex> lock(d_->mutex);
  return getTag<uint16_t>(d_->tif->tfile, tdir, tag);
}

uint32_t TIFFReader::getTag32(uint16_t tdir,
                                     uint32_t tag) const {
  // Valid tags
  assert((TIFFTAG_BADFAXLINES == tag) ||
         (TIFFTAG_GROUP3OPTIONS == tag) ||
         (TIFFTAG_GROUP4OPTIONS == tag) ||
         (TIFFTAG_IMAGEDEPTH == tag) ||
         (TIFFTAG_IMAGELENGTH == tag) ||
         (TIFFTAG_IMAGEWIDTH == tag) ||
         (TIFFTAG_ROWSPERSTRIP == tag) ||
         (TIFFTAG_SUBFILETYPE == tag) ||
         (TIFFTAG_TILEDEPTH == tag) ||
         (TIFFTAG_TILELENGTH == tag) ||
         (TIFFTAG_TILEWIDTH == tag) ||
         (TIFFTAG_FAXMODE == tag) ||
         (TIFFTAG_JPEGCOLORMODE == tag) ||
         (TIFFTAG_JPEGQUALITY == tag) ||
         (TIFFTAG_JPEGTABLESMODE == tag));

  assert(d_.get() && d_->tif.get());
  assert(tdir < numDirectories());

  // Check for pseudo tags that are not stored in file
  if (TIFFTAG_JPEGCOLORMODE == tag)
    return d_->tif->forceRGBJPEG[tdir] ? JPEGCOLORMODE_RGB : JPEGCOLORMODE_RAW;

  // Lock
  boost::lock_guard<boost::recursive_mutex> lock(d_->mutex);
  return getTag<uint32_t>(d_->tif->tfile, tdir, tag);
}

float TIFFReader::getTagFloat(uint16_t tdir,
                           uint32_t tag) const {
  // Valid tags
  assert((TIFFTAG_XPOSITION == tag) ||
         (TIFFTAG_XRESOLUTION == tag) ||
         (TIFFTAG_YPOSITION == tag) ||
         (TIFFTAG_YRESOLUTION == tag));

  assert(d_.get() && d_->tif.get());
  assert(tdir < numDirectories());

  // Lock
  boost::lock_guard<boost::recursive_mutex> lock(d_->mutex);
  return getTag<float>(d_->tif->tfile, tdir, tag);
}

double TIFFReader::getTagDouble(uint16_t tdir,
                           uint32_t tag) const {
  // Valid tags
  assert((TIFFTAG_SMAXSAMPLEVALUE == tag) ||
         (TIFFTAG_SMINSAMPLEVALUE == tag));

  assert(d_.get() && d_->tif.get());
  assert(tdir < numDirectories());

  // Lock
  boost::lock_guard<boost::recursive_mutex> lock(d_->mutex);
  return getTag<double>(d_->tif->tfile, tdir, tag);
}

} // namespace image
} // namespace sedeen
