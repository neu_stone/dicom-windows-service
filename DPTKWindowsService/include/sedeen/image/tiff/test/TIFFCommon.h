
#ifndef SEDEEN_SRC_IMAGE_TIFF_TEST_TIFFCOMMON_H
#define SEDEEN_SRC_IMAGE_TIFF_TEST_TIFFCOMMON_H

namespace sedeen {
class Size;
class Rect;

namespace image {

class RawImage;
class ImageAccessor;
class TIFFWriter;

namespace tiff {
namespace test {

// Prints the top-left and bottom-right RGB values \a img
// Used only for debugging TIFF tests
void PrintImageRange(const RawImage &img);

// Write image and return TRUE on success
bool WriteImage(TIFFWriter &writer, ImageAccessor source, 
                const Rect &source_area);

// Checks that \a img contents match the extracted coordinates.
// Check is only valid if \a img has been created with getImage(Size)
bool CheckImage(const RawImage &img, const Rect &region);

// Returns an RGB_8 image with \a size dimensions
// Red Channel has a vertical gradient
// Green channel has a horizontal gradient 
// Blue Channel is contant
ImageAccessor getImage(const Size &size);

}   // namespace test
}   // namespace tiff
}   // namespace image
}   // namespace sedeen


#endif // SEDEEN_SRC_IMAGE_TIFF_TEST_TIFFCOMMON_H