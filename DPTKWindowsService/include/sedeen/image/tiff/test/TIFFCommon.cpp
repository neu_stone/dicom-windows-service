
#include "image/tiff/test/TIFFCommon.h"

#include <iostream>
#include <boost/date_time.hpp>
#include <boost/thread/thread.hpp>

#include "image/io/ImageAccessor.h"
#include "image/tiff/TIFFWriter.h"
#include "image/io/ImageData.h"
#include "image/standard/StandardImage.h"
#include "global/geometry/Size.h"
#include "global/geometry/Rect.h"


namespace sedeen {
namespace image {
namespace tiff {
namespace test {

// Prints the top-left and bottom-right RGB values the image
// Used only for debugging tests
void PrintImageRange(const RawImage &img) {
  // Check top-left corner of region
  Rect r = img.rect();
  std::cout << "Image (" << &img << "):\tTop-Left: (" 
            << img.at(0,0,0) << ", " 
            << img.at(0,0,1) << ", " 
            << img.at(0,0,2) << ")\n\t\t\tBottom-Right: ("
            << img.at(xMax(r), yMax(r), 0) << ", " 
            << img.at(xMax(r), yMax(r), 1) << ", " 
            << img.at(xMax(r), yMax(r), 2) << ")\n";
}

// Checks that \a img contents match the extracted coordinates.
// Check is only valud if it was created with getImage(Size)
bool CheckImage(const RawImage &img, const Rect &region) {
  // Check top-left corner of region
  return img.at(0,0,0) == region.y() &&
          img.at(0,0,1) == region.x() &&
          img.at(0,0,2) == 1 &&
  // Check bottom-right corner
          img.at(region.width()-1, region.height()-1, 0) == yMax(region) &&
          img.at(region.width()-1, region.height()-1, 1) == xMax(region) &&
          img.at(region.width()-1, region.height()-1, 2) == 1;
}

// Returns an RGB_8 image with \a size dimensions
// Red Channel has a vertical gradient
// Green channel has a horizontal gradient 
// Blue Channel is contant
ImageAccessor getImage(const Size &size) {
  RawImage img(size, RGB_8);
  for (int y = 0; y < size.height(); ++y) {
    for (int x = 0; x < size.width(); ++x) {
      img.setValue(x, y, 0, y);
      img.setValue(x, y, 1, x);
      img.setValue(x, y, 2, 1);
    }
  }
  return ImageAccessor(boost::shared_ptr<ImageData>(
      new standard::StandardImage(img)));
}

bool WriteImage(TIFFWriter &writer, ImageAccessor source, 
                const Rect &source_area) {
  // Start writer
  writer.writeImage(source, source_area);
  
  // wait till done
  while (!writer.isDone()) {
    boost::this_thread::sleep(boost::posix_time::milliseconds(10)); 
  }
  return writer.success();
}

}   // namespace test
}   // namespace tiff
}   // namespace image
}   // namespace sedeen


