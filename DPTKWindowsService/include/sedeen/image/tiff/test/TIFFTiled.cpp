#include "image/tiff/TIFFReader.h"
#include "image/tiff/TIFFWriter.h"

#include "gtest/gtest.h"
#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>

#include "image/io/ImageAccessor.h"
#include "image/io/ImageData.h"
#include "image/standard/StandardImage.h"
#include "image/io/Exceptions.h"
#include "global/geometry/Size.h"
#include "global/geometry/Rect.h"
#include "global/geometry/Point.h"

#include "image/tiff/test/TIFFCommon.h"

namespace sedeen {
namespace image {
namespace tiff {
namespace test {

const std::string tiledFilename("tiff_tiled_test.tif");

// TIFFReader fixture
class TIFFReaderTiledTest : public ::testing::Test {
public:
  TIFFReaderTiledTest() : reader(tiledFilename) {};
protected:
  TIFFReader reader;
};

// Test constructor exception
TEST(TIFFWriterTiledTest, ConstructorException) {
  EXPECT_THROW(TIFFWriter(""), FileAccessError);
}

// tiled image test
TEST(TIFFWriterTiledTest, WriteTiledImage) {
  boost::scoped_ptr<TIFFWriter> writer;
  EXPECT_NO_THROW(writer.reset(new TIFFWriter(tiledFilename)));

  writer->setTiled(16);

  ImageAccessor img = getImage(Size(50, 50));
  EXPECT_TRUE(WriteImage(*writer, img, rect(img, 0)));

  img = getImage(Size(100, 100));
  EXPECT_TRUE(WriteImage(*writer, img, rect(img, 0)));

  img = getImage(Size(25, 25));
  EXPECT_TRUE(WriteImage(*writer, img, rect(img, 0)));
}


// Test constructor exception
TEST_F(TIFFReaderTiledTest, ConstructorException) {
  EXPECT_THROW(TIFFReader(""), FileAccessError);
}

TEST_F(TIFFReaderTiledTest, NumDirectories) {
  EXPECT_EQ(3, reader.numDirectories());
}

TEST_F(TIFFReaderTiledTest, GetLargestDirectory) {
  EXPECT_EQ(1, reader.getLargestDirectory());
}

TEST_F(TIFFReaderTiledTest, Dimensions) {
  EXPECT_EQ(Size(50,50), reader.dimensions(0));
  EXPECT_EQ(Size(100,100), reader.dimensions(1));
  EXPECT_EQ(Size(25,25), reader.dimensions(2));
}

TEST_F(TIFFReaderTiledTest, TileSize) {
  EXPECT_EQ(Size(16,16), reader.tileSize(0));
  EXPECT_EQ(Size(16,16), reader.tileSize(1));
  EXPECT_EQ(Size(16,16), reader.tileSize(2));
}

TEST_F(TIFFReaderTiledTest, RowsPerStrip) {
  EXPECT_EQ(0u, reader.rowsPerStrip(0));
  EXPECT_EQ(0u, reader.rowsPerStrip(1));
  EXPECT_EQ(0u, reader.rowsPerStrip(2));
}

TEST_F(TIFFReaderTiledTest, GetRegion) {
  Rect region(Point(10, 10), Size(20, 20));
  RawImage img = reader.getRegion(0, region);
  EXPECT_TRUE(CheckImage(img, region));

  region = Rect(Point(15, 16), Size(2, 2));
  img = reader.getRegion(1, region);
  EXPECT_TRUE(CheckImage(img, region));

  region = Rect(Point(15, 15), Size(1, 1));
  img = reader.getRegion(2, region);
  EXPECT_TRUE(CheckImage(img, region));
}

TEST_F(TIFFReaderTiledTest, GetStrip) {
  RawImage
  img = reader.getStrip(0, 2);
  EXPECT_TRUE(img.isNull());

  img = reader.getStrip(1, 2);
  EXPECT_TRUE(img.isNull());

  img = reader.getStrip(2, 2);
  EXPECT_TRUE(img.isNull());
}

//TEST_F(TIFFReaderTiledTest, GetTileNegative) {
//  ::testing::FLAGS_gtest_death_test_style = "threadsafe";
//  EXPECT_DEATH(reader.getTile(0, Point(-1,0)), ".*Assertion.*failed.*");
//  EXPECT_DEATH(reader.getTile(0, Point(0,-1)), ".*Assertion.*failed.*");
//  EXPECT_DEATH(reader.getTile(0, Point(-1,-1)), ".*Assertion.*failed.*");
//}

TEST_F(TIFFReaderTiledTest, GetTile) {
  int tw = reader.getTag32(0, TIFFTAG_TILEWIDTH);
  int th = reader.getTag32(0, TIFFTAG_TILELENGTH);

  Rect region(Point(0, 0), Size(tw, th));
  RawImage img = reader.getTile(0, origin(region));
  EXPECT_TRUE(CheckImage(img, region));

  region = Rect(Point(tw*3, tw*3), Size(tw, th));
  img = reader.getTile(1, origin(region));
  EXPECT_TRUE(CheckImage(img, region));
}

} //namespace test
} //namespace tiff
} //namespace sedeen
} //namespace image
