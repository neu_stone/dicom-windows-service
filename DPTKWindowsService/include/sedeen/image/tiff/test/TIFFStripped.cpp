#include "image/tiff/TIFFReader.h"
#include "image/tiff/TIFFWriter.h"

#include "gtest/gtest.h"
#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>

#include "image/io/ImageAccessor.h"
#include "image/io/ImageData.h"
#include "image/standard/StandardImage.h"
#include "image/io/Exceptions.h"
#include "global/geometry/Size.h"
#include "global/geometry/Rect.h"
#include "global/geometry/Point.h"

#include "image/tiff/test/TIFFCommon.h"

namespace sedeen {
namespace image {
namespace tiff {
namespace test {

const std::string strippedFilename("tiff_stripped_test.tif");

// TIFFReader fixture
class TIFFReaderStrippedTest : public ::testing::Test {
public:
  TIFFReaderStrippedTest() : reader(strippedFilename) {};
protected:
  TIFFReader reader;
};


// Test constructor exception
TEST(TIFFWriterStrippedTest, ConstructorException) {
  EXPECT_THROW(TIFFWriter(""), FileAccessError);
}

// stripped image test
TEST(TIFFWriterStrippedTest, WriteStrippedImage) {
  boost::scoped_ptr<TIFFWriter> writer;
  EXPECT_NO_THROW(writer.reset(new TIFFWriter(strippedFilename)));

  writer->setStripped(true);

  // Create a three level image
  ImageAccessor img = getImage(Size(50, 50));
  EXPECT_TRUE(WriteImage(*writer, img, rect(img, 0)));

  img = getImage(Size(100, 100));
  EXPECT_TRUE(WriteImage(*writer, img, rect(img, 0)));

  img = getImage(Size(25, 25));
  EXPECT_TRUE(WriteImage(*writer, img, rect(img, 0)));

}

// Test constructor exception
TEST_F(TIFFReaderStrippedTest, ConstructorException) {
  EXPECT_THROW(TIFFReader(""), FileAccessError);
}

TEST_F(TIFFReaderStrippedTest, NumDirectories) {
  EXPECT_EQ(3, reader.numDirectories());
}

TEST_F(TIFFReaderStrippedTest, GetLargestDirectory) {
  EXPECT_EQ(1, reader.getLargestDirectory());
}

TEST_F(TIFFReaderStrippedTest, Dimensions) {
  EXPECT_EQ(Size(50,50), reader.dimensions(0));
  EXPECT_EQ(Size(100,100), reader.dimensions(1));
  EXPECT_EQ(Size(25,25), reader.dimensions(2));
}

TEST_F(TIFFReaderStrippedTest, TileSize) {
  EXPECT_EQ(Size(0,0), reader.tileSize(0));
  EXPECT_EQ(Size(0,0), reader.tileSize(1));
  EXPECT_EQ(Size(0,0), reader.tileSize(2));
}

TEST_F(TIFFReaderStrippedTest, RowsPerStrip) {
  EXPECT_GT(reader.rowsPerStrip(0), 0u);
  EXPECT_GT(reader.rowsPerStrip(1), 0u);
  EXPECT_GT(reader.rowsPerStrip(2), 0u);
}

//TEST_F(TIFFReaderStrippedTest, GetRegionNegative) {
//  Rect region(Point(-1,0), Size(20, 20));
//  EXPECT_DEATH(reader.getRegion(0, region), ".*Assertion.*failed.*");
//
//  region = Rect(Point(0,-1), Size(20, 20));
//  EXPECT_DEATH(reader.getRegion(0, region), ".*Assertion.*failed.*");
//
//  region = Rect(Point(-1,-1), Size(20, 20));
//  EXPECT_DEATH(reader.getRegion(0, region), ".*Assertion.*failed.*");
//}

TEST_F(TIFFReaderStrippedTest, GetRegion) {
  Rect region(Point(10, 10), Size(20, 20));
  RawImage img = reader.getRegion(0, region);
  EXPECT_TRUE(CheckImage(img, region));

  region = Rect(Point(15, 16), Size(17, 18));
  img = reader.getRegion(1, region);
  EXPECT_TRUE(CheckImage(img, region));

  region = Rect(Point(15, 15), Size(1, 1));
  img = reader.getRegion(2, region);
  EXPECT_TRUE(CheckImage(img, region));
}

//TEST_F(TIFFReaderStrippedTest, GetStripNegative){
//  ::testing::FLAGS_gtest_death_test_style = "threadsafe";
//  ASSERT_DEATH(reader.getStrip(0, -1), ".*Assertion.*failed.*");
//}

TEST_F(TIFFReaderStrippedTest, GetStrip) {
  int rps = reader.getTag32(0, TIFFTAG_ROWSPERSTRIP);

  Rect region(Point(0, rps*0), Size(50, rps));
  RawImage img = reader.getStrip(0, region.y());
  EXPECT_TRUE(CheckImage(img, region));

  region = Rect(Point(0, rps*1), Size(100, rps));
  img = reader.getStrip(1, region.y());
  EXPECT_TRUE(CheckImage(img, region));
}

TEST_F(TIFFReaderStrippedTest, GetTile) {
  RawImage
  img = reader.getTile(0, Point(10, 10));
  EXPECT_TRUE(img.isNull());

  img = reader.getTile(1, Point(10, 10));
  EXPECT_TRUE(img.isNull());

  img = reader.getTile(2, Point(10, 10));
  EXPECT_TRUE(img.isNull());
}

} //namespace test
} //namespace tiff
} //namespace sedeen
} //namespace image
