#include "image/iterator/GridDimension.h"

#include <algorithm>
#include <cassert>

namespace sedeen {
namespace image {
namespace iterator {

namespace {

/// Integer math trick for rounding-up division - for positive integers only.
//
/// \param dividend
/// Must be non-negative.
///
/// \param divisor
/// Must be greater than zero.
int DivideRoundingUp(int dividend, int divisor) {
  assert(0 <= dividend);
  assert(0 < divisor);
  return (dividend + divisor - 1) / divisor;
}

/// Compute the first grid element starting at or before zero.
//
/// Element zero of the grid will be that which contains the element to the
/// right of the origin. This means that the grid is described by an offset
/// value in the range (-cell_size,0].
///
/// \param offset
/// Offset of the grid. May have any integer value.
///
/// \param cell_size
/// Size of the grid segments. Must be greater than zero.
///
/// \return
/// Offset of the first element of the grid cell straddling the origin. If the
/// offset is zero, the function returns zero.
int StandardOffset(int offset, int cell_size) {
  assert(0 < cell_size);
  // Integer division/modulus with negatives undefined. Correct for this.
  if (offset < 0) {
    offset += (-offset / cell_size) * cell_size;
  } else {
    offset -= DivideRoundingUp(offset, cell_size) * cell_size;
  }
  assert(0 >= offset);
  return offset;
}

} // namespace

GridDimension::GridDimension() : step_size_(1), offset_(0) {}

GridDimension::GridDimension(int step_size, int offset)
    : step_size_(step_size),
      offset_(StandardOffset(offset, step_size)) {
  // Preconditions
  assert(0 < step_size);
  // Postconditions
  assert(0 >= offset_);
  assert(-offset_ < step_size_);
}

int GridDimension::offset() const {
  return offset_;
}

int GridDimension::step_size() const {
  return step_size_;
}

bool operator==(const GridDimension& lhs, const GridDimension& rhs) {
  return (lhs.step_size() == rhs.step_size()) &&
      (lhs.offset() == rhs.offset());
}

int GlobalFrameToGrid(const GridDimension& grid, int position) {
  return position - grid.offset();
}

int GridFrameToGlobal(const GridDimension& grid, int position) {
  return position + grid.offset();
}

int StartingOffset(const GridDimension& grid, int cell) {
  return GridFrameToGlobal(grid, cell * grid.step_size());
}

int ContainingStep(const GridDimension& grid, int position) {
  assert(0 <= position);
  // This is an invariant of GridDimension, but test anyway.
  assert(0 < grid.step_size());
  return GlobalFrameToGrid(grid, position) / grid.step_size();
}

int CellsWithin(const GridDimension& grid, int span) {
  assert(0 <= span);
  int cell_count = 0;
  if (span > 0) {
    cell_count = DivideRoundingUp(GlobalFrameToGrid(grid, span),
                                  grid.step_size());
  }
  return cell_count;
}

namespace {

/// Constrain \c value within \c minimum and \c maximum, inclusive.
//
/// \param value
/// Value to be constrained.
///
/// \param minimum
/// Minimum allowed value.
///
/// \param maximum
/// Maximum allowed value.
///
/// \return The value itself, or the closest of \c minimum or \c maximum.
int Clip(int value, int minimum, int maximum) {
  assert(minimum <= maximum);
  return std::max(minimum, std::min(maximum, value));
}

} // namespace

int CroppedOffset(const GridDimension& grid, const int index,
                  const int span_begin, const int span_length) {
  assert(0 <= span_length);
  return Clip(StartingOffset(grid, index), span_begin,
              span_begin + span_length);
}

int CroppedLength(const GridDimension& grid, const int index,
                  const int span_begin, const int span_length) {
  assert(0 <= span_length);
  int start = StartingOffset(grid, index);
  int end = start + grid.step_size();
  int span_end = span_begin + span_length;
  start = Clip(start, span_begin, span_end);
  end = Clip(end, span_begin, span_end);
  return end - start;
}

GridDimension BuildGridDimension::operator()(int step_size, int offset) const {
  return GridDimension(step_size, offset);
}

} // namespace iterator
} // namespace image
} // namespace sedeen
