#ifndef SEDEEN_SRC_IMAGE_ITERATOR_BUFFER_H
#define SEDEEN_SRC_IMAGE_ITERATOR_BUFFER_H

#include <QtCore>
#include <boost/scoped_ptr.hpp>
#include <boost/thread.hpp>

#include "global/geometry/Rect.h"
#include "image/DllApi.h"
#include "image/io/ImageAccessor.h"
#include "image/buffer/RawImage.h"

namespace sedeen {
namespace image {
namespace iterator {

/// Provides a point of contact to the ImageAccessor.
//
/// Used to fetch and store the next tile during iteration.
class SEDEEN_IMAGE_API Buffer : public QObject {
  Q_OBJECT;

  typedef image::RawImage RawImage;

 public:
  /// Set the source ImageAccessor and the target resolution.
  Buffer(const image::ImageAccessor& image_accessor, int resolution);

  /// Copy constructor.
  //
  /// Blocks until the buffer is between tasks.
  Buffer(const Buffer& rhs);

  /// Begin loading a segment of data.
  //
  /// This function is not thread-safe.
  Buffer& Load(const Rect& request);

  /// True if the last requested region is complete.
  bool ready() const;

  /// Retrieve the most recent result.
  //
  /// Blocks until the Update signal is received from the SelectiveDecoder.
  image::RawImage image() const;

 private slots:
  /// Update the stored copy of the image and wake up all waiting threads.
  void Update(const RawImage& tile, bool complete);

 private:
  /// Assignment operator.
  //
  /// Blocks until the buffer is between tasks.
  Buffer& operator=(const Buffer& rhs);

  /// Data source. Set by the constructor.
  image::ImageAccessor image_accessor_;

  /// Resolution used for requests. Set by the constructor.
  int resolution_;

  /// Flag signalling a result is ready for extraction.
  bool ready_;

  /// Local cache of requested image data.
  image::RawImage image_;

  mutable boost::mutex mutex_;

  mutable boost::condition_variable image_ready_;
}; // class Buffer

} // namespace iterator
} // namespace image
} // namespace sedeen

#endif
