// Primary header
#include "image/iterator/Iterator.h"

#include <QtCore>
#include <cassert>

#include "image/iterator/Buffer.h"

namespace sedeen {
namespace image {
namespace iterator {
namespace {

const ElementCount kDefaultGridXOffset = 0;
const ElementCount kDefaultGridYOffset = 0;
const ElementCount kDefaultCellWidth = 256;
const ElementCount kDefaultCellHeight = 256;

}

Iterator::Iterator()
    : grid_(),
      cell_index_(),
      element_(),
      end_(),
      buffer_(),
      data_() {}

Iterator::Iterator(const ImageAccessor& image_accessor)
    : grid_(rect(image_accessor, 0), kDefaultCellWidth, kDefaultCellHeight,
            kDefaultGridXOffset, kDefaultGridYOffset),
      cell_index_(),
      element_(),
      end_(),
      buffer_(new Buffer(image_accessor, 0)),
      data_(),
      pixel_() {
  MoveToCell(0);
}

Iterator::Iterator(const ImageAccessor& image_accessor, int resolution)
    : grid_(rect(image_accessor, resolution), kDefaultCellWidth,
            kDefaultCellHeight, kDefaultGridXOffset, kDefaultGridYOffset),
      cell_index_(),
      element_(),
      end_(),
      buffer_(new Buffer(image_accessor, resolution)),
      data_(),
      pixel_() {
  assert(0 <= resolution);
  assert(image_accessor.getNumResolutionLevels() > resolution);
  MoveToCell(0);
}

Iterator::Iterator(const ImageAccessor& image_accessor, int resolution,
                   const Rect& region)
    : grid_(region, kDefaultCellWidth, kDefaultCellHeight, kDefaultGridXOffset,
            kDefaultGridYOffset),
      cell_index_(),
      element_(),
      end_(),
      buffer_(new Buffer(image_accessor, resolution)),
      data_(),
      pixel_() {
  assert(0 <= resolution);
  assert(image_accessor.getNumResolutionLevels() > resolution);
  MoveToCell(0);
}

Iterator::~Iterator() {}

Iterator::Iterator(const Iterator& rhs)
    : grid_(rhs.grid_),
      cell_index_(rhs.cell_index_),
      element_(rhs.element_),
      end_(rhs.end_),
      buffer_(rhs.buffer_.get() ? new Buffer(*rhs.buffer_) : 0),
      data_(rhs.data_),
      pixel_(rhs.pixel_) {}

Iterator& Iterator::operator=(Iterator rhs) {
  swap(*this, rhs);
  return *this;
}

Iterator Iterator::End() const {
  Iterator it(*this);
  it.MoveToCell(grid_.NumCells());
  return it;
}

Coordinate Iterator::x() const {
  return ElementX(grid_, cell_index_, element_);
}

Coordinate Iterator::y() const {
  return ElementY(grid_, cell_index_, element_);
}

Iterator& Iterator::operator++() {
  assert(element_ != end_);
  ++element_;
  if (element_ == end_) MoveToCell(cell_index_ + 1);
  return *this;
}

Iterator& Iterator::operator+=(ElementOffset step) {
  // Leave range checking to the Jump() function.
  iterator::Location loc = Jump(grid_, cell_index_, element_, step);
  // Only call MoveToCell() if we're changing cells.
  if (loc.cell() != cell_index_) MoveToCell(loc.cell());
  // Manually reset the index.
  element_ = loc.element();
  return *this;
}

Iterator& Iterator::operator-=(ElementOffset step) {
  *this += -step;
  return *this;
}

bool Iterator::operator==(const Iterator& iterator) const {
  assert(this->GeometryMatches(iterator));
  return (element_ == iterator.element_) &&
      (cell_index_ == iterator.cell_index_);
}

bool Iterator::operator<(const Iterator& iterator) const {
  assert(this->GeometryMatches(iterator));
  return (cell_index_ < iterator.cell_index_) ||
      ((cell_index_ == iterator.cell_index_) &&
       (element_ <  iterator.element_));
}

ElementOffset Iterator::operator-(const Iterator& rhs) const {
  assert(this->GeometryMatches(rhs));
  return grid_.GlobalIndex(cell_index_, element_) -
      rhs.grid_.GlobalIndex(rhs.cell_index_, rhs.element_);
}

const std::vector<int>& Iterator::operator*() const {
  assert(element_ != end_);
  assert(false == data_.isNull());

  LoadPixel();
  return pixel_;
}

const std::vector<int>* Iterator::operator->() const {
  assert(element_ != end_);
  assert(false == data_.isNull());

  LoadPixel();
  return &pixel_;
}

void Iterator::LoadPixel() const {
  // Increase the index to take into account the many channels
  pixel_.resize(data_.components());
  ElementIndex idx = element_ * pixel_.size();
  for (auto i = 0u; pixel_.size() != i; ++i) pixel_[i] = data_[idx + i];
}

void Iterator::BufferCell(CellIndex cell) {
  assert(0 <= cell);
  assert(grid_.NumCells() >= cell);
  if (cell != grid_.NumCells()) {
    buffer_->Load(grid_.CellRect(cell));
  }
}

void Iterator::GetBufferedCell() {
  data_ = buffer_->image();
}

void Iterator::MoveToCell(int cell) {
  assert(0 <= cell);
  assert(grid_.NumCells() >= cell);
  // If the cell is not the next (buffered) cell, order it.
  if (cell != (cell_index_ + 1)) BufferCell(cell);
  // Extract the data from the buffer
  GetBufferedCell();
  // Buffer the next cell's data - unless this is the last cell
  if (cell != grid_.NumCells()) BufferCell(cell + 1);
  // Set the navigation variables
  SetNavigationVariables(cell);
}

void Iterator::SetNavigationVariables(int cell) {
  assert(0 <= cell);
  assert(grid_.NumCells() >= cell);
  cell_index_ = cell;
  element_ = 0;
  end_ = grid_.CellArea(cell);
}

bool Iterator::GeometryMatches(const Iterator& iterator) const {
  return grid_ == iterator.grid_;
}

void swap(Iterator& lhs, Iterator& rhs) {
  using std::swap;
  swap(lhs.grid_, rhs.grid_);
  swap(lhs.cell_index_, rhs.cell_index_);
  swap(lhs.element_, rhs.element_);
  swap(lhs.end_, rhs.end_);
  swap(lhs.buffer_, rhs.buffer_);
  swap(lhs.data_, rhs.data_);
}

bool operator!=(const Iterator& lhs, const Iterator& rhs) {
  return !(lhs == rhs);
}

bool operator>(const Iterator& lhs, const Iterator& rhs) {
  return rhs < lhs;
}

bool operator<=(const Iterator& lhs, const Iterator& rhs) {
  return !(lhs > rhs);
}

bool operator>=(const Iterator& lhs, const Iterator& rhs) {
  return !(lhs < rhs);
}

Iterator operator+(Iterator iterator, ElementOffset offset) {
  return iterator += offset;
}

Iterator operator-(Iterator iterator, ElementOffset offset) {
  return iterator += -offset;
}

} // namespace iterator
} // namespace image
} // namespace sedeen
