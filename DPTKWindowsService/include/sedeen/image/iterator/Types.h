#ifndef SEDEEN_SRC_IMAGE_ITERATOR_TYPES_H
#define SEDEEN_SRC_IMAGE_ITERATOR_TYPES_H

#include <boost/cstdint.hpp>

namespace sedeen {
namespace image {
namespace iterator {

/// Linear index of pixels.
typedef boost::int_fast64_t ElementIndex;

/// Count of pixels.
typedef ElementIndex ElementCount;

/// Offset type
//
/// Shall be signed and should be 64-bit.
typedef boost::int_fast64_t ElementOffset;

/// Linear index of image tiles.
typedef boost::int_fast32_t CellIndex;

/// Cartesian coordinate of an element
typedef boost::int_fast64_t Coordinate;

/// Coordinate of image tiles.
typedef CellIndex CellCoordinate;

/// Count of image tiles.
typedef CellIndex CellCount;

} // namespace iterator
} // namespace image
} // namespace sedeen

#endif
