#include "image/iterator/Buffer.h"

#include "gtest/gtest.h"

#include "global/geometry/Point.h"
#include "global/geometry/Size.h"

namespace sedeen {
namespace image {
namespace iterator {

class BufferNullImage : public ::testing::Test {
 public:
  BufferNullImage() : buffer_(ImageAccessor(), 0) {}
 protected:
  Buffer buffer_;
}; // class BufferNullImage

TEST_F(BufferNullImage, Ready) {
  EXPECT_EQ(true, buffer_.ready());
}

TEST_F(BufferNullImage, Image) {
  EXPECT_TRUE(buffer_.image().isNull());
}

TEST_F(BufferNullImage, Load) {
  Rect region(Point(12, 15), Size(200, 200));
  buffer_.Load(region);
  EXPECT_TRUE(buffer_.image().isNull());
}

} // namespace iterator
} // namespace image
} // namespace sedeen

