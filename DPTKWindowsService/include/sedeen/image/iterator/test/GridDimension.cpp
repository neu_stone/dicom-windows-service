#include "image/iterator/GridDimension.h"

#include <cstdlib>

#include "gtest/gtest.h"

namespace sedeen {
namespace image {
namespace iterator {
namespace {

const int kStepSize = 5;
const int kOffset = 3;

const int kIndexRangeMin = -30;
const int kIndexRangeMax = 30;

}

// Test positive offset parameters
TEST(GridDimensionConstructor, PositiveOffsets) {
  const int kPositiveOffsets[5] = {0, -4, -3, -2, -1};
  for (int i = 0; i != 22; ++i) {
    GridDimension d(kStepSize, i);
    EXPECT_EQ(kStepSize, d.step_size());
    EXPECT_EQ(kPositiveOffsets[i%kStepSize], d.offset());
  }
}

// Test negative offset parameters
TEST(GridDimensionConstructor, NegativeOffset) {
  const int kNegativeOffsets[5] = {0, -1, -2, -3, -4};
  for (int i = -1; i != -22; --i) {
    GridDimension d(kStepSize, i);
    EXPECT_EQ(kStepSize, d.step_size());
    EXPECT_EQ(kNegativeOffsets[std::abs(i)%kStepSize], d.offset());
  }
}

// Test zero step size failure
TEST(GridDimensionConstructorDeathTest, InvalidStepSize) {
  ::testing::FLAGS_gtest_death_test_style = "threadsafe";
 // EXPECT_DEATH(GridDimension(0, 0), ".*Assertion.*failed.");
//  EXPECT_DEATH(GridDimension(-2, 0), ".*Assertion.*failed.");
}

namespace {

// Simple constant object used for the following tests.
const GridDimension d(kStepSize, kOffset);

}

// Test equality operator
TEST(GridDimensionEquality, Equal) {
  GridDimension d0(kStepSize, kOffset);
  EXPECT_EQ(d, d0);
  GridDimension d1(kStepSize, kOffset + kStepSize);
  EXPECT_EQ(d, d1);
  GridDimension d2(kStepSize, kOffset - 9*kStepSize);
  EXPECT_EQ(d, d2);
}

TEST(GridDimensionEquality, Unequal) {
  GridDimension d0(kStepSize, 1);
  EXPECT_FALSE(d0 == d);
}

TEST(GridDimensionFrameTranslation, ToGrid) {
  for (int i = kIndexRangeMin; i != kIndexRangeMax; ++i) {
    EXPECT_EQ(i - d.offset(), GlobalFrameToGrid(d, i));
  }
}

TEST(GridDimensionFrameTranslation, ToGlobal) {
  for (int i = kIndexRangeMin; i != kIndexRangeMax; ++i) {
    EXPECT_EQ(i + d.offset(), GridFrameToGlobal(d, i));
  }
}

TEST(StartingOffsetTest, FullRange) {
  for (int i = kIndexRangeMin; i != kIndexRangeMax; ++i) {
    EXPECT_EQ(d.offset() + kStepSize * i, StartingOffset(d, i));
  }
}

TEST(ContainingStepTest, PositivePositions) {
  EXPECT_EQ(0, ContainingStep(d, 0));
  EXPECT_EQ(1, ContainingStep(d, d.offset() + kStepSize));
  EXPECT_EQ(0, ContainingStep(d, d.offset() + kStepSize - 1));
  EXPECT_EQ(3, ContainingStep(d, d.offset() + 3*kStepSize + kStepSize/2));
}

TEST(CellsWithin, PositivePositions) {
  EXPECT_EQ(0, CellsWithin(d, 0));
  EXPECT_EQ(1, CellsWithin(d, d.offset() + kStepSize));
  EXPECT_EQ(2, CellsWithin(d, kStepSize));
}

TEST(CroppedOffsetTest, CellFits) {
  int cell_index = 0;
  int cell_start = StartingOffset(d, cell_index);
  int span_begin = cell_start - 10;
  int span_length = kStepSize + 20;
  EXPECT_EQ(cell_start, CroppedOffset(d, cell_index, span_begin, span_length));
}

TEST(CroppedOffsetTest, SqueezedUpward) {
  int cell_index = 0;
  int shift = 2;
  int cell_start = StartingOffset(d, cell_index);
  int span_begin = cell_start + shift;
  int span_length = kStepSize;
  EXPECT_EQ(span_begin, CroppedOffset(d, cell_index, span_begin, span_length));
}

TEST(CroppedOffsetTest, SqueezedDownward) {
  int cell_index = 0;
  int shift = -2 * kStepSize;
  int cell_start = StartingOffset(d, cell_index);
  int span_begin = cell_start + shift;
  int span_length = kStepSize;
  EXPECT_EQ(span_begin + span_length,
            CroppedOffset(d, cell_index, span_begin, span_length));
}

TEST(CroppedLengthTest, Fits) {
  int cell_index = 1;
  int cell_begin = StartingOffset(d, cell_index);
  int span_begin = cell_begin - kStepSize;
  int span_length = 3 * kStepSize;
  EXPECT_EQ(d.step_size(),
            CroppedLength(d, cell_index, span_begin, span_length));
}

TEST(CroppedLengthTest, SqueezedDown) {
  int cell_index = 1;
  int cell_begin = StartingOffset(d, cell_index);
  int span_begin = cell_begin - kStepSize/2;
  int span_length = kStepSize;
  EXPECT_EQ(d.step_size() - kStepSize/2,
            CroppedLength(d, cell_index, span_begin, span_length));
}

TEST(CroppedLengthTest, SqueezedUp) {
  int cell_index = 1;
  int cell_begin = StartingOffset(d, cell_index);
  int span_begin = cell_begin + kStepSize/2;
  int span_length = kStepSize;
  EXPECT_EQ(d.step_size() - kStepSize/2,
            CroppedLength(d, cell_index, span_begin, span_length));
}

TEST(CroppedLengthTest, NonIntersecting) {
  int cell_index = 1;
  int cell_begin = StartingOffset(d, cell_index);
  int span_begin = cell_begin + kStepSize;
  int span_length = kStepSize;
  EXPECT_EQ(0, CroppedLength(d, cell_index, span_begin, span_length));
}

TEST(GridDimensionGenerator, Creation) {
  BuildGridDimension builder;
  int offset = 19;
  EXPECT_EQ(GridDimension(kStepSize, offset), builder(kStepSize, offset));
}

} // namespace iterator
} // namespace image
} // namespace sedeen
