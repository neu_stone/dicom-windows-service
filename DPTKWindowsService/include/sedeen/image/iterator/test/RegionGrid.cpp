#include "image/iterator/RegionGrid.h"

#include <algorithm>
#include <vector>

#include "gtest/gtest.h"

#include "global/geometry/Point.h"
#include "global/geometry/Size.h"

namespace sedeen {
namespace image {
namespace iterator {

/// Simple test to
TEST(RegionGridLocation, Constructor) {
  const int cell_index = 4;
  const int element_index = 99;

  Location l(cell_index, element_index);
  EXPECT_EQ(cell_index, l.cell());
  EXPECT_EQ(element_index, l.element());
}

TEST(RegionGridLocation, Equality) {
  EXPECT_EQ(Location(235, 11), Location(235, 11));
  EXPECT_FALSE(Location(2,1) == Location(0,1));
}

TEST(RegionGridDefaultConstructor, AllFunctions) {
  RegionGrid regionGridDefault;

  EXPECT_EQ(0, regionGridDefault.NumCells());
  EXPECT_EQ(0, regionGridDefault.NumElements());

  EXPECT_EQ(Rect(), regionGridDefault.CellRect(0));

  EXPECT_EQ(0, regionGridDefault.CellX(0));
  EXPECT_EQ(0, regionGridDefault.CellY(0));

  EXPECT_EQ(0, regionGridDefault.CellWidth(0));
  EXPECT_EQ(0, regionGridDefault.CellHeight(0));
  EXPECT_EQ(0, regionGridDefault.CellArea(0));

  EXPECT_EQ(0, regionGridDefault.StartingX(0));
  EXPECT_EQ(0, regionGridDefault.StartingY(0));
  EXPECT_EQ(0, regionGridDefault.StartingGlobalIndex(0));
  EXPECT_EQ(0, regionGridDefault.ContainingCell(0));
  EXPECT_EQ(0, regionGridDefault.GlobalIndex(0, 0));

  Location loc = regionGridDefault.Locate(0);
  EXPECT_EQ(0, loc.cell());
  EXPECT_EQ(0, loc.element());

  // Equality testing vs. another default-constructed RegionGrid.
  EXPECT_EQ(RegionGrid(), regionGridDefault);

  loc = Jump(regionGridDefault, 0, 0, 0);
  EXPECT_EQ(0, loc.cell());
  EXPECT_EQ(0, loc.element());
}

// For these next tests, the test grid covers 12x10 region centred at (10,8).
// The grid is divided up into 3x4 cells offset by [15,13].
//
// The desired logical cell membership map:
// y\x  10  11  12  13  14  15  16  17  18  19  20  21
//    -------------------------------------------------
//   8|  0   0|  1   1   1|  2   2   2|  3   3   3|  4|
//    |-----------------------------------------------|
//   9|  5   5|  6   6   6|  7   7   7|  8   8   8|  9|
//    |       |           |           |           |   |
//  10|  5   5|  6   6   6|  7   7   7|  8   8   8|  9|
//    |       |           |           |           |   |
//  11|  5   5|  6   6   6|  7   7   7|  8   8   8|  9|
//    |       |           |           |           |   |
//  12|  5   5|  6   6   6|  7   7   7|  8   8   8|  9|
//    |-----------------------------------------------|
//  13| 10  10| 11  11  11| 12  12  12| 13  13  13| 14|
//    |       |           |           |           |   |
//  14| 10  10| 11  11  11| 12  12  12| 13  13  13| 14|
//    |       |           |           |           |   |
//  15| 10  10| 11  11  11| 12  12  12| 13  13  13| 14|
//    |       |           |           |           |   |
//  16| 10  10| 11  11  11| 12  12  12| 13  13  13| 14|
//    |-----------------------------------------------|
//  17| 15  15| 16  16  16| 17  17  17| 18  18  18| 19|
//    -------------------------------------------------
//
// The desired cell-wrapped index map:
// y\x  10  11  12  13  14  15  16  17  18  19  20  21
//    -------------------------------------------------
//   8|  0   1|  0   1   2|  0   1   2|  0   1   2|  0|
//    |-----------------------------------------------|
//   9|  0   1|  0   1   2|  0   1   2|  0   1   2|  0|
//    |       |           |           |           |   |
//  10|  2   3|  3   4   5|  3   4   5|  3   4   5|  1|
//    |       |           |           |           |   |
//  11|  4   5|  6   7   8|  6   7   8|  6   7   8|  2|
//    |       |           |           |           |   |
//  12|  6   7|  9  10  11|  9  10  11|  9  10  11|  3|
//    |-----------------------------------------------|
//  13|  0   1|  0   1   2|  0   1   2|  0   1   2|  0|
//    |       |           |           |           |   |
//  14|  2   3|  3   4   5|  3   4   5|  3   4   5|  1|
//    |       |           |           |           |   |
//  15|  4   5|  6   7   8|  6   7   8|  6   7   8|  2|
//    |       |           |           |           |   |
//  16|  6   7|  9  10  11|  9  10  11|  9  10  11|  3|
//    |-----------------------------------------------|
//  17|  0   1|  0   1   2|  0   1   2|  0   1   2|  0|
//    -------------------------------------------------
//
// The desired cell-wrapped global index map:
// y\x  10  11  12  13  14  15  16  17  18  19  20  21
//    -------------------------------------------------
//   8|  0   1|  2   3   4|  5   6   7|  8   9  10| 11|
//    |-----------------------------------------------|
//   9| 12  13| 20  21  22| 32  33  34| 44  45  46| 56|
//    |       |           |           |           |   |
//  10| 14  15| 23  24  25| 35  36  37| 47  48  49| 57|
//    |       |           |           |           |   |
//  11| 16  17| 26  27  28| 38  39  40| 50  51  52| 58|
//    |       |           |           |           |   |
//  12| 18  19| 29  30  31| 41  42  44| 53  54  55| 59|
//    |-----------------------------------------------|
//  13| 60  61| 68  69  70| 80  81  82| 92  93  94|104|
//    |       |           |           |           |   |
//  14| 62  63| 71  72  73| 83  84  85| 95  96  97|105|
//    |       |           |           |           |   |
//  15| 64  65| 74  75  76| 86  87  88| 98  99 100|106|
//    |       |           |           |           |   |
//  16| 66  67| 77  78  79| 89  90  91|101 102 103|107|
//    |-----------------------------------------------|
//  17|108 109|110 111 112|113 114 115|116 117 118|119|
//    -------------------------------------------------

namespace {

const int kX0 = 10;
const int kY0 = 8;
const int kRegionWidth = 12;
const int kRegionHeight = 10;
const Rect region(Point(kX0, kY0), Size(kRegionWidth, kRegionHeight));

const int kTileWidth = 3;
const int kTileHeight = 4;
const int kGridOffsetX = 15;
const int kGridOffsetY = 13;

RegionGrid grid(region, kTileWidth, kTileHeight, kGridOffsetX, kGridOffsetY);

}

TEST(RegionGrid, AreaReports) {
  EXPECT_EQ(20, grid.NumCells());
  EXPECT_EQ(kRegionWidth * kRegionHeight, grid.NumElements());
}

namespace {

std::vector<int> cellStartingX() {
  std::vector<int> x(5);
  x[0] = 10;
  x[1] = 12;
  x[2] = 15;
  x[3] = 18;
  x[4] = 21;
  return x;
}

std::vector<int> cellStartingY() {
  std::vector<int> y(4);
  y[0] = 8;
  y[1] = 9;
  y[2] = 13;
  y[3] = 17;
  return y;
}

std::vector<int> cellWidths() {
  std::vector<int> x(5);
  x[0] = 2;
  x[1] = 3;
  x[2] = 3;
  x[3] = 3;
  x[4] = 1;
  return x;
}

std::vector<int> cellHeights() {
  std::vector<int> y(4);
  y[0] = 1;
  y[1] = 4;
  y[2] = 4;
  y[3] = 1;
  return y;
}

std::vector<int> startingIndices() {
  std::vector<int> starting_indices;
  starting_indices.reserve(grid.NumCells());

  starting_indices.push_back(0);
  starting_indices.push_back(2);
  starting_indices.push_back(5);
  starting_indices.push_back(8);
  starting_indices.push_back(11);

  starting_indices.push_back(12);
  starting_indices.push_back(20);
  starting_indices.push_back(32);
  starting_indices.push_back(44);
  starting_indices.push_back(56);

  starting_indices.push_back(60);
  starting_indices.push_back(68);
  starting_indices.push_back(80);
  starting_indices.push_back(92);
  starting_indices.push_back(104);

  starting_indices.push_back(108);
  starting_indices.push_back(110);
  starting_indices.push_back(113);
  starting_indices.push_back(116);
  starting_indices.push_back(119);

  return starting_indices;
}

} // namespace

TEST(RegionGrid, CellRectValid) {
  std::vector<int> start_x = cellStartingX();
  std::vector<int> start_y = cellStartingY();
  std::vector<int> width = cellWidths();
  std::vector<int> height = cellHeights();

  for (int i = 0; i != grid.NumCells(); ++i) {
    Rect cellRect = grid.CellRect(i);
    int x = i % start_x.size();
    int y = i / start_x.size();
    EXPECT_EQ(start_x[x], cellRect.x()) <<
        "At iteration " << i << ": (" << x << "," << y << ")";
    EXPECT_EQ(start_y[y], cellRect.y()) <<
        "At iteration " << i << ": (" << x << "," << y << ")";
    EXPECT_EQ(width[x], cellRect.width()) <<
        "At iteration " << i << ": (" << x << "," << y << ")";
    EXPECT_EQ(height[y], cellRect.height()) <<
        "At iteration " << i << ": (" << x << "," << y << ")";
  }
}

TEST(RegionGrid, CellRectEnd) {
  Rect cellRect = grid.CellRect(grid.NumCells());
  EXPECT_TRUE(isEmpty(cellRect));
}

TEST(RegionGrid, CellXValid) {
  std::vector<int> start_x = cellStartingX();
  for (int i = 0; i != grid.NumCells(); ++i) {
    int x = i % start_x.size();
    EXPECT_EQ(x, grid.CellX(i)) <<
        "At iteration " << i << ": x = " << x;
  }
}

TEST(RegionGrid, CellXEnd) {
  EXPECT_EQ(5, grid.CellX(grid.NumCells()));
}

TEST(RegionGrid, CellYValid) {
  std::vector<int> start_x = cellStartingX();
  for (int i = 0; i != grid.NumCells(); ++i) {
    int y = i / start_x.size();
    EXPECT_EQ(y, grid.CellY(i)) <<
        "At iteration " << i << ": y = " << y;
  }
}

TEST(RegionGrid, CellYEnd) {
  EXPECT_EQ(4, grid.CellY(grid.NumCells()));
}

TEST(RegionGrid, CellWidthValid) {
  std::vector<int> width = cellWidths();
  for (int i = 0; i != grid.NumCells(); ++i) {
    EXPECT_EQ(width[grid.CellX(i)], grid.CellWidth(i));
  }
}

TEST(RegionGrid, CellWidthEnd) {
  EXPECT_EQ(0, grid.CellWidth(grid.NumCells()));
}

TEST(RegionGrid, CellHeightValid) {
  std::vector<int> height = cellHeights();
  for (int i = 0; i != grid.NumCells(); ++i) {
    EXPECT_EQ(height[grid.CellY(i)], grid.CellHeight(i));
  }
}

TEST(RegionGrid, CellHeightEnd) {
  EXPECT_EQ(0, grid.CellHeight(grid.NumCells()));
}

TEST(RegionGrid, CellAreaValid) {
  // Manually compute the areas of the cells
  std::vector<int> area;
  area.reserve(grid.NumCells());
  std::vector<int> width = cellWidths();
  std::vector<int> height = cellHeights();
  typedef std::vector<int>::const_iterator it;
  for (it i = height.begin(); height.end() != i; ++i) {
    for (it j = width.begin(); width.end() != j; ++j) {
      area.push_back(*i * *j);
    }
  }

  for (int i = 0; grid.NumCells() != i; ++i) {
    EXPECT_EQ(area[i], grid.CellArea(i));
  }
}

TEST(RegionGrid, StartingXAndYValid) {
  std::vector<int> start_x = cellStartingX();
  std::vector<int> start_y = cellStartingY();
  for (int i = 0; grid.NumCells() != i; ++i) {
    EXPECT_EQ(start_x[grid.CellX(i)], grid.StartingX(i));
    EXPECT_EQ(start_y[grid.CellY(i)], grid.StartingY(i));
  }
}

TEST(RegionGrid, StartingXAndYEnd) {
  EXPECT_EQ(kRegionWidth + kX0, grid.StartingX(grid.NumCells()));
  EXPECT_EQ(kRegionHeight + kY0, grid.StartingY(grid.NumCells()));
}

TEST(RegionGrid, StartingGlobalIndexValid) {
  std::vector<int> starting_indices = startingIndices();
  for (int i = 0; grid.NumCells() != i; ++i) {
    EXPECT_EQ(starting_indices[i], grid.StartingGlobalIndex(i));
  }
}

TEST(RegionGrid, StartingGlobalIndexEnd) {
  EXPECT_EQ(grid.NumElements(), grid.StartingGlobalIndex(grid.NumCells()));
}

TEST(RegionGrid, ContainingCellValid) {
  std::vector<int> inds = startingIndices();
  typedef std::vector<int>::iterator it;
  for (int i = 0; grid.NumElements() != i; ++i) {
    it cell = std::upper_bound(inds.begin(), inds.end(), i);
    --cell;
    int entry = distance(inds.begin(), cell);
    EXPECT_EQ(entry, grid.ContainingCell(i)) <<
        "At index " << i;
  }
}

TEST(RegionGrid, ContainingCellEnd) {
  EXPECT_EQ(grid.NumCells(), grid.ContainingCell(grid.NumElements()));
}

TEST(RegionGrid, GlobalIndexValid) {
  for (int i = 0; grid.NumElements() != i; ++i) {
    // Compute the cell and local index for the global index, i
    int cell = grid.ContainingCell(i); // verified in another test
    int local_index = i - grid.StartingGlobalIndex(cell);
    EXPECT_EQ(i, grid.GlobalIndex(cell, local_index));
  }
}

TEST(RegionGrid, GlobalIndexEnd) {
  EXPECT_EQ(grid.NumElements(), grid.GlobalIndex(grid.NumCells(), 0));
}

TEST(RegionGrid, LocateValid) {
  for (int i = 0; grid.NumElements() != i; ++i) {
    Location l = grid.Locate(i);
    EXPECT_EQ(grid.ContainingCell(i), l.cell());
    EXPECT_EQ(i - grid.StartingGlobalIndex(l.cell()), l.element());
  }
}

TEST(RegionGrid, LocateEnd) {
  Location l = grid.Locate(grid.NumElements());
  EXPECT_EQ(grid.NumCells(), l.cell());
  EXPECT_EQ(grid.CellArea(l.cell()), l.element());
}

TEST(RegionGrid, Equality) {
  EXPECT_EQ(RegionGrid(region, kTileWidth, kTileHeight, kGridOffsetX,
                       kGridOffsetY), grid);
}

TEST(RegionGrid, Inequality) {
  EXPECT_FALSE(grid==RegionGrid());
}

TEST(RegionGrid, JumpBeginningToEnd) {
  Location end = grid.Locate(grid.NumElements());
  Location jump = Jump(grid, 0, 0, grid.NumElements());
  EXPECT_EQ(end, jump);
}

TEST(RegionGrid, JumpArbitrarily) {
  // Try jumping forward
  EXPECT_EQ(Location(7, 8), Jump(grid, 6, 0, 20));
  // Try jumping backward
  EXPECT_EQ(Location(0, 1), Jump(grid, 13, 10, -101));
}

TEST(RegionGrid, ElementX) {
  EXPECT_EQ(16, ElementX(grid, 7, 4));
}

} // namespace iterator
} // namespace image
} // namespace sedeen
