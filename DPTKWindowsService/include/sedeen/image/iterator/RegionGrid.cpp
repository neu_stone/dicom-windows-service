// Primary header
#include "image/iterator/RegionGrid.h"

#include <algorithm>
#include <cassert>

#include "global/geometry/Point.h"
#include "global/geometry/Size.h"
#include "image/iterator/GridDimension.h"

namespace sedeen {
namespace image {
namespace iterator {

Location::Location(int cell, int element)
    : cell_(cell),
      element_(element) {
  assert(0 <= cell_);
  assert(0 <= element_);
}

int Location::cell() const {
  return cell_;
}

int Location::element() const {
  return element_;
}

bool operator==(const Location& lhs, const Location& rhs) {
  return (lhs.cell() == rhs.cell()) && (lhs.element() == rhs.element());
}

RegionGrid::RegionGrid()
    : region_(),
      x_grid_(1, 0),
      y_grid_(1, 0),
      num_cells_x_(),
      num_cells_y_() {}

RegionGrid::RegionGrid(const Rect& region, int cell_width, int cell_height,
                       int x_offset, int y_offset)
    : region_(region),
      x_grid_(cell_width, x_offset - region_.x()),
      y_grid_(cell_height, y_offset - region_.y()),
      num_cells_x_(CellsWithin(x_grid_, region_.width())),
      num_cells_y_(CellsWithin(y_grid_, region_.height())) {
  assert(0 <= region.width());
  assert(0 <= region.height());
  assert(0 <= num_cells_x_);
  assert(0 <= num_cells_y_);
}

int RegionGrid::NumCells() const {
  return num_cells_x_ * num_cells_y_;
}

int RegionGrid::NumElements() const {
  return region_.width() * region_.height();
}

Rect RegionGrid::CellRect(int cell) const {
  assert(0 <= cell);
  assert(NumCells() >= cell);
  return Rect(Point(StartingX(cell), StartingY(cell)),
              Size(CellWidth(cell), CellHeight(cell)));
}

int RegionGrid::CellX(int cell) const {
  assert(0 <= cell);
  assert(NumCells() >= cell);
  // Handles both the case of num_cells_x_ == 0 and cell == NumCells().
  return NumCells() == cell ? num_cells_x_ : cell % num_cells_x_;
}

int RegionGrid::CellY(int cell) const {
  assert(0 <= cell);
  assert(NumCells() >= cell);
  // Handles both the case of num_cells_x_ == 0 and cell == NumCells().
  return NumCells() == cell ? num_cells_y_ : cell / num_cells_x_;
}

int RegionGrid::CellWidth(int cell) const {
  assert(0 <= cell);
  assert(NumCells() >= cell);
  return CroppedLength(x_grid_, CellX(cell), 0, region_.width());
}

int RegionGrid::CellHeight(int cell) const {
  assert(0 <= cell);
  assert(NumCells() >= cell);
  return CroppedLength(y_grid_, CellY(cell), 0, region_.height());
}

int RegionGrid::CellArea(int cell) const {
  assert(0 <= cell);
  assert(NumCells() >= cell);
  return CellWidth(cell) * CellHeight(cell);
}

int RegionGrid::StartingX(int cell) const {
  assert(0 <= cell);
  assert(NumCells() >= cell);
  return region_.x() + CroppedOffset(x_grid_, CellX(cell), 0, region_.width());
}

int RegionGrid::StartingY(int cell) const {
  assert(0 <= cell);
  assert(NumCells() >= cell);
  return region_.y() + CroppedOffset(y_grid_, CellY(cell), 0, region_.height());
}

int RegionGrid::StartingGlobalIndex(int cell) const {
  assert(0 <= cell);
  assert(NumCells() >= cell);
  int full_rows = (StartingY(cell) - StartingY(0)) * region_.width();
  int full_cells = (StartingX(cell) - StartingX(0)) * CellHeight(cell);
  return full_rows + full_cells;
}

int RegionGrid::ContainingCell(int global_index) const {
  assert(0 <= global_index);
  assert(NumElements() >= global_index);

  // This is the fail-safe case
  int cell_index = NumCells();

  // Note: the special case check is required because ContainingStep does not
  // take the region constraints into account.
  if (global_index != NumElements()) {
    // Compute which row of the image this element would be in if we accessed
    // the image in the standard (row-major) manner.
    assert(0 < region_.width());
    int element_y = global_index / region_.width();
    // Compute which cell row contains this y-coordinate
    assert(region_.height() > element_y); // This case is not handled correctly
    int cell_y = ContainingStep(y_grid_, element_y);
    // Get the index of the cell at the start of the row
    cell_index = CellIndex(0, cell_y);
    // Subtract the starting value of this cell from the input index
    global_index -= StartingGlobalIndex(cell_index);
    // Get the height of cells in this row
    int cell_height = CellHeight(cell_index);
    // Perform the same trick above, but along the x-direction.
    assert(0 < cell_height);
    int element_x = global_index / cell_height;
    cell_index += ContainingStep(x_grid_, element_x);
  }
  return cell_index;
}

int RegionGrid::GlobalIndex(int cell, int local_index) const {
  assert(0 <= cell);
  assert(NumCells() >= cell);
  assert(0 <= local_index);
  assert(CellArea(cell) >= local_index);
  return StartingGlobalIndex(cell) + local_index;
}

Location RegionGrid::Locate(int global_index) const {
  assert(0 <= global_index);
  assert(NumElements() >= global_index);
  int cell = ContainingCell(global_index);
  int first_element = StartingGlobalIndex(cell);
  assert(global_index >= first_element);
  return Location(cell, global_index - first_element);
}

bool RegionGrid::operator==(const RegionGrid& rhs) const {
  return (region_ == rhs.region_) &&
      (x_grid_ == rhs.x_grid_) &&
      (y_grid_ == rhs.y_grid_);
}

int RegionGrid::CellIndex(int cell_x, int cell_y) const {
  assert(0 <= cell_x);
  assert(num_cells_x_ > cell_x);
  assert(0 <= cell_y);
  assert(num_cells_y_ > cell_y);
  return cell_x + num_cells_x_ * cell_y;
}

Location Jump(const RegionGrid& nav, int start_cell, int start_local_index,
              int jump) {
  assert(0 <= start_cell);
  assert(nav.NumCells() >= start_cell);
  assert(0 <= start_local_index);
  assert(nav.CellArea(start_cell) >= start_local_index);
  int destination = nav.GlobalIndex(start_cell, start_local_index) + jump;
  assert(0 <= destination);
  assert(nav.NumElements() >= destination);
  return nav.Locate(destination);
}

int ElementX(const RegionGrid& nav, int cell_index, int element_index) {
  assert(0 <= cell_index);
  assert(nav.NumCells() >= cell_index);
  assert(0 <= element_index);
  assert(nav.CellArea(cell_index) >= element_index);
  int x = nav.StartingX(cell_index);
  int width = nav.CellWidth(cell_index);
  if (0 != width) x += element_index % width;
  return x;
}

int ElementY(const RegionGrid& nav, int cell_index, int element_index) {
  assert(0 <= cell_index);
  assert(nav.NumCells() >= cell_index);
  assert(0 <= element_index);
  assert(nav.CellArea(cell_index) >= element_index);
  int y = nav.StartingY(cell_index);
  int width = nav.CellWidth(cell_index);
  if (0 != width) y += element_index / width;
  return y;
}

} // namespace iterator
} // namespace image
} // namespace sedeen
