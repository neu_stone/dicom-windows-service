// Primary header
#include "image/iterator/Buffer.h"

#include "image/io/SelectiveDecoder.h"

namespace sedeen {
namespace image {
namespace iterator {
namespace {

void ConnectSignals(const image::ImageAccessor* accessor,
                    const Buffer* buffer) {
  using sedeen::image::RawImage;
  QObject::connect(accessor,
                   SIGNAL(imageUpdated(const RawImage&, bool)),
                   buffer,
                   SLOT(Update(const RawImage&, bool)),
                   Qt::DirectConnection);
}

} // namespace

Buffer::Buffer(const image::ImageAccessor& image_accessor, int resolution)
    : image_accessor_(image_accessor),
      resolution_(resolution),
      ready_(true),
      image_(),
      mutex_(),
      image_ready_() {
  assert(0 <= resolution_);
  assert(image_accessor.getNumResolutionLevels() > resolution);
  ConnectSignals(&image_accessor_, this);
}

Buffer::Buffer(const Buffer& rhs)
    : image_accessor_(rhs.image_accessor_),
      resolution_(rhs.resolution_),
      ready_(true),
      image_(),
      mutex_(),
      image_ready_() {
  boost::unique_lock<boost::mutex> lock(rhs.mutex_);
  if (!rhs.ready_) rhs.image_ready_.wait(lock);
  image_ = rhs.image_;
  ConnectSignals(&image_accessor_, this);
}

Buffer& Buffer::Load(const Rect& request) {
  {
    boost::lock_guard<boost::mutex> lock(mutex_);
    ready_ = false;
  }
  // This may call update() immediately, leading to a double-lock of the
  // mutex.
  image::region(image_accessor_, resolution_, request, image::kNormal);
  return *this;
}

bool Buffer::ready() const {
  return ready_;
}

image::RawImage Buffer::image() const {
  boost::unique_lock<boost::mutex> lock(mutex_);
  if (!ready_) image_ready_.wait(lock);
  return image_;
}

void Buffer::Update(const image::RawImage& image, bool complete) {
  if (complete) {
    boost::lock_guard<boost::mutex> lock(mutex_);
    image_ = image;
    ready_ = true;
    image_ready_.notify_all();
  }
}

} // namespace iterator
} // namespace image
} // namespace sedeen
