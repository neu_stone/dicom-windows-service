#ifndef SEDEEN_SRC_IMAGE_ITERATOR_INDEXEDACCESS_H
#define SEDEEN_SRC_IMAGE_ITERATOR_INDEXEDACCESS_H

#include <boost/scoped_ptr.hpp>

#include "global/geometry/Rect.h"
#include "global/geometry/Size.h"
#include "image/DllApi.h"
#include "image/io/ImageAccessor.h"
#include "image/buffer/RawImage.h"

namespace sedeen {
namespace image {
namespace iterator {

class Buffer;

} // namespace iterator

class SEDEEN_IMAGE_API IndexedAccess {
 public:
  IndexedAccess();

  /// Constructor.
  //
  /// \param image_accessor
  /// The source of the data.
  ///
  /// \param resolution
  /// Resolution level at which to access the data. Must be within the range
  /// [0..levels(image_accessor)).
  explicit IndexedAccess(const ImageAccessor& image_accessor,
                         int resolution = 0);

  ~IndexedAccess();

  /// Extract data at the given coordinates.
  //
  /// \param x
  /// X-coordinate of the pixel. Must be in range [0..Width()).
  ///
  /// \param y
  /// Y-coordinate of the pixel. Must be in range [0..Height()).
  ///
  /// \param component
  /// Component to extract. Must be in range [0..NumComponets()).
  int GetData(int x, int y, int component) const;

  /// Return the width of the image.
  int width() const;

  /// Return the height of the image.
  int height() const;

  /// Return the number of components available at each pixel.
  int num_components() const;

 private:
  /// Copying disallowed
  IndexedAccess(const IndexedAccess& rhs);

  /// Assignment disallowed
  IndexedAccess& operator=(const IndexedAccess& rhs);

  /// Compute a region to buffer, centred on (x,y).
  Rect regionCentredOn(int x, int y) const;

  /// Currently-buffered region
  mutable Rect region_;

  /// Available area
  Size image_size_;

  /// Available components
  int num_components_;

  /// Available image data
  boost::scoped_ptr<RawImage> data_;

  /// Data-fetch class.
  boost::scoped_ptr<iterator::Buffer> buffer_;
}; // class IndexedAccess

} // namespace image
} // namespace sedeen

#endif
