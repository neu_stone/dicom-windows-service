#ifndef SEDEEN_SRC_IMAGE_ITERATOR_GRIDDIMENSION_H
#define SEDEEN_SRC_IMAGE_ITERATOR_GRIDDIMENSION_H

#include <functional>

#include "image/DllApi.h"

namespace sedeen {
namespace image {
namespace iterator {

/// Information about a single dimension of a grid structure.
class SEDEEN_IMAGE_API GridDimension {
 public:
  GridDimension();

  /// Construct a helper class for a single dimension of a grid.
  //
  /// \param step_size
  /// The size of each cell in the grid. Must be greater than zero.
  ///
  /// \param offset
  /// The offset of the entire grid. Initially, element 0 lies at the left
  /// edge of cell 0. Values outside the range (-step_size,0] will be
  /// translated into that range.
  GridDimension(int step_size, int offset);

  // Default destructor
  // Default copy constructor
  // Default assignment operator

  /// Get the offset of the grid pattern.
  //
  /// This offset is the location of the left-most element of grid cell 0 in
  /// global coordinates.
  ///
  /// \return
  /// The result will be in the range (-cell_length(), 0].
  int offset() const;

  /// The span of each segment of the grid.
  //
  /// \return
  /// The result will be a value greater than zero.
  int step_size() const;

 private:
  int step_size_;
  int offset_;
}; // class GridDimension

/// Equality tested on both offset and step size.
//
/// \relates GridDimension
SEDEEN_IMAGE_API
bool operator==(const GridDimension& lhs, const GridDimension& rhs);

/// Translate the position from global coordinates to grid-centred coordinates.
//
/// \relates GridDimension
SEDEEN_IMAGE_API
int GlobalFrameToGrid(const GridDimension& grid, int position);

/// Translate the position from grid-centred coordinates to global coordinates.
//
/// \relates GridDimension
SEDEEN_IMAGE_API
int GridFrameToGlobal(const GridDimension& grid, int position);

/// Offset of the beginning of the requested cell along a given dimension.
//
/// \relates GridDimension
///
/// \param grid
/// GridDimension object describing the cell distribution.
///
/// \param cell
/// Index of the requested cell. By convention, cell 0 contains element 0.
///
/// \return
/// The index of the lowest element index of the requested cell.
SEDEEN_IMAGE_API
int StartingOffset(const GridDimension& grid, int cell);

/// Index of the cell containing the given coordinate.
//
/// \relates GridDimension
///
/// This is roughly the inverse of the function Offset().
///
/// \param grid
/// GridDimension object describing the cell distribution.
///
/// \param position
/// Position, in global coordinates. Must not be negative.
///
/// \return
/// The index of the cell containing the given coordinate.
SEDEEN_IMAGE_API
int ContainingStep(const GridDimension& grid, int position);

/// Count the number of cells touched by the span [0,span).
//
/// \relates GridDimension
///
/// \param grid
///
/// \param span
/// Exclusive end-point of the enclosing span. Must be positive.
SEDEEN_IMAGE_API
int CellsWithin(const GridDimension& grid, int span);

/// Offset of the beginning of the requested cell, constrained within a span.
//
/// \relates GridDimension
///
/// \param grid
///
/// \param cell
/// Index of the cell along the grid dimension. See StartingOffset().
///
/// \param span_begin
/// Inclusive beginning offset of the allowed region.
///
/// \param span_length
/// Length of the span. Must not be negative.
///
/// \return
/// Points testing less than span_begin will be set to span_begin. Anything
/// lying past the end of the span will be set to span_begin + span_length.
/// Consequently, if the span_length is 0, all points will be set to span_begin.
SEDEEN_IMAGE_API
int CroppedOffset(const GridDimension& grid, int cell, int span_begin,
                  int span_length);

/// Get the length of a segment of the grid, constrained to be within a span.
//
/// \relates GridDimension
///
/// \param grid
///
/// \param cell
/// Index of the cell along the grid dimension to crop.
///
/// \param span_begin
/// Inclusive beginning off of the allowed region.
///
/// \param span_length
/// Length of the region. Must be non-negative.
SEDEEN_IMAGE_API
int CroppedLength(const GridDimension& grid, int cell, int span_begin,
                  int span_length);

class SEDEEN_IMAGE_API BuildGridDimension
    : std::binary_function<GridDimension, int, int> {
 public:
  GridDimension operator()(int step_size, int offset) const;
}; // class BuildGridDimension

} // namespace iterator
} // namespace image
} // namespace sedeen

#endif
