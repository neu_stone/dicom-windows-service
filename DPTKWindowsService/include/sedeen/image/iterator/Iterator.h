#ifndef SEDEEN_SRC_IMAGE_ITERATOR_ITERATOR_H
#define SEDEEN_SRC_IMAGE_ITERATOR_ITERATOR_H

#include <boost/scoped_ptr.hpp>
#include <vector>

#include "image/iterator/RegionGrid.h"
#include "image/iterator/Types.h"
#include "image/DllApi.h"
#include "image/io/ImageAccessor.h"
#include "image/buffer/RawImage.h"
#include "global/geometry/Rect.h"

namespace sedeen {
namespace image {
namespace iterator {

class Buffer;

/// Forward iterator for very large images.
//
///
class SEDEEN_IMAGE_API Iterator {
 public:
  Iterator();

  /// Iterate over the entire image at the highest resolution.
  //
  /// \param image_accessor
  /// Accessor for the image data to analyze.
  explicit Iterator(const ImageAccessor& image_accessor);

  /// Iterate over the entire image at a given resolution.
  //
  /// \param image_accessor
  /// Accessor for the image data to analyze.
  ///
  /// \param resolution
  /// Resolution level at which to iterate over the image. Must be in the range
  /// [0,image_accessor.levels()).
  Iterator(const ImageAccessor& image_accessor, int resolution);

  /// Iterate over a region of the image.
  //
  /// \param image_accessor
  /// Accessor for the image data to analyze.
  ///
  /// \param resolution
  /// Resolution level at which to iterate over the image. Must be in the range
  /// [0,image_accessor.levels()).
  ///
  /// \param region
  /// Region to iterate over, specified at the same level as \a resolution.
  Iterator(const ImageAccessor& image_accessor, int resolution,
           const Rect& region);

  ~Iterator();

  Iterator(const Iterator& rhs);

  Iterator& operator=(Iterator rhs);

  /// Construct an end-point for this iterator.
  Iterator End() const;

  /// Get the x-coordinate of the current element.
  //
  /// \return
  /// The x-coordinate of the iterator's position relative to the origin, rather
  /// than the smallest x-value of the \a region used to construct the iterator.
  Coordinate x() const;

  /// Get the y-coordinate of the current element.
  //
  /// \return
  /// The y-coordinate of the iterator's position relative to the origin, rather
  /// than the smallest y-value of the \a region used to construct the iterator.
  Coordinate y() const;

  /// Advance the iterator.
  //
  /// \pre
  /// *this != end()
  ///
  /// \post
  /// Iterator instance is pointing at the next pixel in the current tile, or
  /// the start of the next tile.
  Iterator& operator++();

  /// Advance the iterator by an arbitrary amount.
  //
  /// \param offset
  /// Amount by which to step forward. May be any integer, but must not move the
  /// iterator past either bound of the range.
  Iterator& operator+=(ElementOffset offset);

  /// Reverse the iterator by an arbitrary amount.
  //
  /// \param offset
  /// Amount by which to step backward. May be any integer, but must not move
  /// the iterator past either bound of the range.
  Iterator& operator-=(ElementOffset offset);

  /// Test for equality with another pixel iterator.
  bool operator==(const Iterator& iterator) const;

  /// Compares positions along the range of the two iterators.
  bool operator<(const Iterator& iterator) const;

  /// Compute the difference in position between this and another Iterator.
  //
  /// \param rhs
  /// An Iterator instance of matching geometry. This means it ought to have
  /// been created by copying this iterator, copy-incremented from this
  /// iterator, or created via End().
  ElementOffset operator-(const Iterator& rhs) const;

  /// Read access to the pixel's data.
  const std::vector<int>& operator*() const;

  const std::vector<int>* operator->() const;

 private:
  void LoadPixel() const;

  /// Set the buffer loading cell data.
  //
  /// \param cell
  /// Index of a cell. In the range [0,RegionGrid::NumCells()].
  void BufferCell(CellIndex cell);

  /// Transfer the buffered data into the iterator.
  void GetBufferedCell();

  /// Move the iterator to the beginning of a cell.
  //
  /// \param cell
  /// Index of the cell to which to move. In the range
  /// [0,RegionGrid::NumCells()]. Note that no check is made for moving to
  /// the current cell from the current cell. Calling this with cell_index_
  /// will cause the cell to be re-loaded from ImageData. If you change this
  /// behaviour, you must adjust the constructor,
  ///
  /// \post
  /// The numbered cell data has been copied to the data_ variable. If the
  /// cell immediately following this one is within the allowed range, it has
  /// been ordered from the Buffer.
  void MoveToCell(int cell);

  /// Set the member variables to the beginning of a cell.
  //
  /// \param cell
  /// Index of a cell. In the range [0,RegionGrid::NumCells()].
  void SetNavigationVariables(int cell);

  /// Compare the geometry of this iterator with another.
  bool GeometryMatches(const Iterator& iterator) const;

  /// Calculator class for cell/element navigation.
  RegionGrid grid_;

  /// Index of the current cell.
  CellIndex cell_index_;

  /// Local element index to current cell.
  ElementIndex element_;

  /// One-past-the-end index for the current cell.
  ElementIndex end_;

  /// Buffer object used to pre-fetch data for the next cell.
  boost::scoped_ptr<Buffer> buffer_;

  /// Image data for current cell.
  RawImage data_;

  /// Cache of image data for the current pixel.
  mutable std::vector<int> pixel_;

  friend SEDEEN_IMAGE_API void swap(Iterator& lhs, Iterator& rhs);
}; // class Iterator

SEDEEN_IMAGE_API
void swap(Iterator& lhs, Iterator& rhs);

///
//
/// \relates Iterator
SEDEEN_IMAGE_API
bool operator!=(const Iterator& lhs, const Iterator& rhs);

///
//
/// \relates Iterator
SEDEEN_IMAGE_API
bool operator>(const Iterator& lhs, const Iterator& rhs);

///
//
/// \relates Iterator
SEDEEN_IMAGE_API
bool operator<=(const Iterator& lhs, const Iterator& rhs);

///
//
/// \relates Iterator
SEDEEN_IMAGE_API
bool operator>=(const Iterator& lhs, const Iterator& rhs);

/// Get an offset copy of \c iterator.
//
/// \relates Iterator
SEDEEN_IMAGE_API
Iterator operator+(Iterator iterator, ElementOffset offset);

/// Get an offset copy of \c iterator.
//
/// \relates Iterator
SEDEEN_IMAGE_API
Iterator operator-(Iterator iterator, ElementOffset offset);

} // namespace iterator
} // namespace image
} // namespace sedeen

#endif
