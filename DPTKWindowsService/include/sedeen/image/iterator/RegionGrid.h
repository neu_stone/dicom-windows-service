#ifndef SEDEEN_SRC_IMAGE_ITERATOR_REGIONGRID_H
#define SEDEEN_SRC_IMAGE_ITERATOR_REGIONGRID_H

#include "image/iterator/Types.h"
#include "image/iterator/GridDimension.h"
#include "global/geometry/Rect.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {
namespace iterator {

/// Value class wrapping a cell/element pair.
class SEDEEN_IMAGE_API Location {
 public:
  /// Set the cell and element index values.
  //
  /// \param cell
  /// Linear cell index. Must not be negative.
  ///
  /// \param element
  /// Cell-local element index. Must not be negative.
  Location(int cell, int element);

  // Default destructor
  // Default copy constructor
  // Default assignment operator

  /// Get the linear index of the cell.
  int cell() const;

  /// Get the cell-local element index.
  int element() const;

 private:
  int cell_;
  int element_;
}; // class Location

/// Locations are equal if all contents are equal.
SEDEEN_IMAGE_API
bool operator==(const Location& lhs, const Location& rhs);

/// Calculation assistance class for cell-navigation of an array.
//
/// This class offers an alternative to row-major interpretation of a linear
/// array indexing a two-dimensional region. It indexes a two-dimensional region
/// as a row-major array of two-dimensional cells, each arranged as a standard
/// row-major linear array.
///
/// This approach more closely tracks the structure of accessing a large
/// two-dimensional array, e.g. a large image, as a series of smaller cells.
///
/// RegionGrid uses a few different indices to describe locations with the
/// array:
/// -# \b cell: A row-major linear index of the array of cells used to
///              represent the image.
/// -# \b global_index: A unique index for each element in the region. Increases
///                     through each cell rather than along a whole row of the
///                     region.
/// -# \b local_index: The index of an element relative to the start of the
///                    containing cell.
class SEDEEN_IMAGE_API RegionGrid {
 public:
  /// Default constructor.
  //
  /// Builds a navigator for an empty region.
  RegionGrid();

  /// Construct a navigator for a finite region.
  //
  /// \param region
  /// Total region to break up into cells.
  ///
  /// \param cell_width
  /// Maximum width of cell to use, in a number of elements.
  ///
  /// \param cell_height
  /// Maximum height of cell to use, in a number of elements.
  ///
  /// \param x_offset
  ///
  /// \param y_offset
  RegionGrid(const Rect& region, int cell_width, int cell_height,
             int x_offset, int y_offset);

  /// Get the number of cells into which the region has been broken.
  int NumCells() const;

  /// Get the total number of elements in the input region.
  int NumElements() const;

  /// Returns a Rect for a given cell
  //
  /// The return object preserves the offset of the initial region.
  ///
  /// \param cell
  /// Must be in the range [0..NumCells()]. Note that cell index NumCells()
  /// corresponds to a zero-area cell whose existence eases one-past-the-end
  /// calculations.
  Rect CellRect(int cell) const;

  /// X-coordinate of the cell within the cell array.
  //
  /// \param cell
  /// Linear index of the cell in question. Must be in range [0,NumCells()].
  /// Note that cell index NumCells() corresponds to a zero-area cell whose
  /// existence eases one-past-the-end calculations.
  ///
  /// \return
  /// X-coordinate of the cell in the cell array. When cell == NumCells(), the
  /// number of cells along the x-dimension is returned.
  int CellX(int cell) const;

  /// Y-coordinate of the cell within the cell array.
  //
  /// \param cell
  /// Linear index of the cell in question. Must be in range [0,NumCells()].
  /// Note that cell index NumCells() corresponds to a zero-area cell whose
  /// existence eases one-past-the-end calculations.
  ///
  /// \return
  /// Y-coordinate of the cell in the cell array. When cell == NumCells(), the
  /// number of cells along the y-dimension is returned.
  int CellY(int cell) const;

  /// Calculate the width of a cell.
  //
  /// Covers the possibility that cells along the edge are truncated.
  ///
  /// \param cell
  /// Linear index of the cell in question. Must be in range [0,NumCells()].
  /// Note that cell index NumCells() corresponds to a zero-area cell whose
  /// existence eases one-past-the-end calculations.
  ///
  /// \return
  /// Width of the cell as a number of elements.
  int CellWidth(int cell) const;

  /// Calculate the height of a cell.
  //
  /// Covers the possibility that cells along the edge are truncated.
  ///
  /// \param cell
  /// Linear index of the cell in question. Must be in range [0,NumCells()].
  /// Note that cell index NumCells() corresponds to a zero-area cell whose
  /// existence eases one-past-the-end calculations.
  ///
  /// \return
  /// Height of the cell as a number of elements.
  int CellHeight(int cell) const;

  /// Get the area of a cell.
  //
  /// \param cell
  /// Cell in the navigator's array. Must be within [0,NumCells()]. Note that
  /// cell index NumCells() corresponds to a zero-area cell whose existence
  /// eases one-past-the-end calculations.
  int CellArea(int cell) const;

  /// Get the global x-location of the first element of the given cell.
  //
  /// \param cell
  /// Row-major index of the cell in the region. Must be within [0,NumCells()].
  /// Note that cell index NumCells() corresponds to a zero-area cell whose
  /// existence eases one-past-the-end calculations.
  ///
  /// \return
  /// X-coordinate of the element at the upper, left-hand corner of the cell.
  /// Calculated relative to the origin common with the \a region used to
  /// construct the RegionGrid.
  int StartingX(int cell) const;

  /// Get the global y-location of the first element of the given cell.
  //
  /// \param cell
  /// Row-major index of the cell in the region. Must be within [0,NumCells()].
  /// Note that cell index NumCells() corresponds to a zero-area cell whose
  /// existence eases one-past-the-end calculations.
  ///
  /// \return
  /// Y-coordinate of the element at the upper, left-hand corner of the cell.
  /// Calculated relative to the origin common with the \a region used to
  /// construct the RegionGrid.
  int StartingY(int cell) const;

  /// Compute the global index of the first element of the given cell.
  //
  /// \param cell
  /// Cell in the navigator's array. Must be within [0,NumCells()]. Note that
  /// cell index NumCells() corresponds to a zero-area cell whose existence
  /// eases one-past-the-end calculations.
  ///
  /// \return
  /// The index of the upper, left-hand element of the specified cell. In the
  /// case of cell == NumCells(), NumElements() is returned.
  int StartingGlobalIndex(int cell) const;

  /// Compute the cell containing the given global index.
  //
  /// \param global_index
  /// Cell-wrapped index of the cell to query. Must be within [0,NumCells()].
  /// Note that cell index NumCells() corresponds to a zero-area cell whose
  /// existence eases one-past-the-end calculations.
  int ContainingCell(int global_index) const;

  /// Transform cell-local element index to a global element index.
  //
  /// \param cell
  /// Cell containing the element to transform. Must be within [0,NumCells()].
  ///
  /// \param local_index
  /// Element index, counting from the beginning of the containing cell. Must
  /// be within [0,CellArea(cell)].
  int GlobalIndex(int cell, int local_index) const;

  /// Turn a global-index to a cell / local-index pair.
  //
  /// \param global_index
  /// The index of an element with the region. Must be within [0,NumElements()].
  ///
  /// \return
  /// A location object containing the containing cell and local_index
  /// corresponding to the input global_index.
  Location Locate(int global_index) const;

  /// Equality tested on matching geometries.
  bool operator==(const RegionGrid& rhs) const;

 private:
  /// Compute the row-major index of a cell given x/y coordinates.
  int CellIndex(int cell_x, int cell_y) const;

 private:
  /// Navigation region
  Rect region_;

  /// Description of the grid spacing along x.
  GridDimension x_grid_;

  /// Description of the grid spacing along y.
  GridDimension y_grid_;

  /// Number of cells (both full and partial) along the x-axis.
  int num_cells_x_;

  /// Number of cells (both full and partial) along the y-axis.
  int num_cells_y_;
}; // class RegionGrid

/// Compute the new cell/element location after a given jump.
//
/// \relates RegionGrid
///
/// \param nav
/// Cell navigator object.
///
/// \param start_cell
/// Index of the cell within the navigator's array from which to start. Must
/// be in the range [0,nav.NumCells()].
///
/// \param start_local_index
/// Element within the above-mentioned cell from which to start. Must be in the
/// range [0,nav.CellArea(cell)].
///
/// \param jump
/// Global offset value by which to move. Must not result in landing either at
/// a negative global index or in a value beyond nav.NumElements().
///
/// \return A Location object describing the cell/element arrived at.
SEDEEN_IMAGE_API
Location Jump(const RegionGrid& nav, int start_cell, int start_local_index,
              int jump);

/// Compute the x-coordinate of an element within a cell.
//
/// \param nav
/// RegionGrid describing the cell layout.
///
/// \param cell_index
/// Cell index within the RegionGrid. Must be within [0,NumCells()]. See
/// RegionGrid documentation for information on cell ordering.
///
/// \param element_index
/// Index of an element within cell \a cell_index. Must be within
/// [0,CellArea(cell_index)]. Note that, though the allowed range includes the
/// one-past-the-end value, the returned index is undefined.
SEDEEN_IMAGE_API
int ElementX(const RegionGrid& nav, int cell_index, int element_index);

/// Compute the y-coordinate of an element within a cell.
//
/// \param nav
/// RegionGrid describing the cell layout.
///
/// \param cell_index
/// Cell index within the RegionGrid. Must be within [0,NumCells()]. See
/// RegionGrid documentation for information on cell ordering.
///
/// \param element_index
/// Index of an element within cell \a cell_index. Must be within
/// [0,CellArea(cell_index)]. Note that, though the allowed range includes the
/// one-past-the-end value, the returned index is undefined.
SEDEEN_IMAGE_API
int ElementY(const RegionGrid& nav, int cell_index, int element_index);

} // namespace iterator
} // namespace image
} // namespace sedeen

#endif
