#include "image/iterator/IndexedAccess.h"

#include <cassert>

#include "image/iterator/Buffer.h"
#include "global/geometry/Point.h"
#include "image/io/ImageParser.h"

namespace {

// These sizes ought to be odd for the math to work out correctly.
const int BUFFER_WIDTH = 127;
const int BUFFER_HEIGHT = 127;

} // namespace

namespace sedeen {
namespace image {

IndexedAccess::IndexedAccess()
    : region_(),
      image_size_(),
      num_components_(),
      data_(),
      buffer_() {}

IndexedAccess::IndexedAccess(const ImageAccessor& image_accessor,
                             int resolution)
    : region_(),
      image_size_(image_accessor.getDimensions(0).width(),
                  image_accessor.getDimensions(0).height()),
      num_components_(image_accessor.imageParser().components()),
      data_(new RawImage()),
      buffer_(new iterator::Buffer(image_accessor, resolution)) {
  assert(image_accessor.getNumResolutionLevels() > resolution);
}

IndexedAccess::~IndexedAccess() {}

int IndexedAccess::GetData(int x, int y, int component) const {
  assert(0 <= x);
  assert(width() > x);
  assert(0 <= y);
  assert(height() > y);
  assert(0 <= component);
  assert(num_components() > component);

  if (!contains(region_, x, y)) {
    Rect newRegion = regionCentredOn(x, y);
    buffer_->Load(regionCentredOn(x, y));
    *data_ = buffer_->image();
    region_ = newRegion;
  }
  return data_->at(x - region_.x(), y - region_.y(), component);
}

int IndexedAccess::width() const {
  return image_size_.width();
}

int IndexedAccess::height() const {
  return image_size_.height();
}

int IndexedAccess::num_components() const {
  return num_components_;
}

Rect IndexedAccess::regionCentredOn(int x, int y) const {
  assert(BUFFER_WIDTH % 2 != 0);
  assert(BUFFER_HEIGHT % 2 != 0);

  Point origin = Point(x - BUFFER_WIDTH / 2, y - BUFFER_WIDTH / 2);
  return constrain(Rect(Point(), image_size_),
                   Rect(origin, Size(BUFFER_WIDTH, BUFFER_HEIGHT)));
}

} // namespace image
} // namespace sedeen
