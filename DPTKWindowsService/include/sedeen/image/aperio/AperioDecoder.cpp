#include "image/aperio/AperioDecoder.h"

#include <algorithm>
#include <cassert>

#include <tiffio.h>

#include <CImg.h>

#include <boost/cstdint.hpp>
#include <boost/thread/locks.hpp>

#include "pjasper/include/pjasper.h"

// User includes
#include "global/geometry/Point.h"
#include "global/geometry/Rect.h"
#include "image/aperio/AperioSVSParser.h"
#include "image/io/DecodeRequest.h"
#include "image/io/Exceptions.h"
#include "image/io/ImagePyramid.h"
#include "image/buffer/RawImage.h"
#include "image/j2k/JP2Decoder.h"
#include "image/tiff/TIFFParser.h"
#include "image/tiff/TIFFReader.h"

namespace sedeen {
namespace image {

namespace {

// Deleter funtion for JP2 codec allocated memory
void DeleteJasperBuffer(void *buffer) {
  rsa_dec_free(buffer);
}

// Deleter function for TIFF handle
void TIFFDeleter(void* handle) {
  assert(handle);
  TIFFClose(static_cast<TIFF*>(handle));
}

// Returns TRUE if file uses Aperio style JPEG-2000 compression
bool hasJP2Encoding(const TIFFReader &reader) {
  for (int dir = 0; dir < reader.numDirectories(); ++dir) {
    int style = reader.getTag16(dir, TIFFTAG_COMPRESSION);
    if ((AperioSVSParser::APERIO_COMPRESSION_JP2K_YCBCR == style) ||
        (AperioSVSParser::APERIO_COMPRESSION_JP2K_RGB == style))
      return true;
  }
  return false;
}

// Extracts a component from the previosly decoded region
RawDataPtr getComponent(sdec_t *dec, int component) {
  assert(dec);
  assert((int)rsa_dec_numcomps(dec) > component);

  int width;
  int height;
  int *buffer = 0;

  if (!(buffer = rsa_dec_getcomp(dec, component, &width, &height))) {
    return RawDataPtr();
  }

  return RawDataPtr(buffer, DeleteJasperBuffer);
}

/// Decodes a YCC encoded JPEG-2000 code stream
RawImage decodeYCC(char* buffer, int buflen) {
  assert(buffer && buflen);
  using cimg_library::CImg;

  // Decode buffer
  sdec_t *dec =	rsa_dec_open_jpc_buffer(buffer, buflen, RSA_COLOR_YCC);
  rsa_codes_t error_code = rsa_dec_decodeimage(dec, 0);
  if (RSA_DEC_SUCCESS != error_code) {
    return RawImage();
  }

  // Allocate YCC image
  const int w = rsa_dec_compwidth(dec, 0);
  const int h = rsa_dec_compheight(dec, 0);
  boost::shared_ptr<unsigned char> rawData(new unsigned char[w*h*3],
                                           [](unsigned char* data) {
                                             delete[] data;
                                           });
  CImg<unsigned char> scaled_image(rawData.get(), w, h, 1, 3, true);

  // Cycle through all components and convert to RGB
  // pJasper color conversion are slow, use custom methods
  for (int comp = 0; comp < 3; ++comp) {
    // get component
    int cw = rsa_dec_compwidth(dec, comp);
    int ch = rsa_dec_compheight(dec, comp);
    RawDataPtr data = getComponent(dec, comp);
    CImg<int> component(static_cast<int*>(data.get()), cw, ch, 1, 1, true);

    // Scale component if necessary
    CImg<int> scaled_component;
    if ((cw != w) || (ch != h))
      scaled_component = component.get_resize(w, h);
    else
      scaled_component = CImg<int>(component, true);

    // cast component and fill image
    unsigned char *dest = scaled_image.data(0,0,0,comp);
    int *src = scaled_component.data();
    for (int i=w*h; i >0 ; --i, ++dest, ++src) {
      *dest = static_cast<unsigned char>(*src);
    }
  }

  // Clean up
  rsa_dec_close(dec);

  // Convert to RGB
  scaled_image.YCbCrtoRGB();

  // Image must be in interleaved format
  scaled_image.permute_axes("cxyz");

  // Return Image Buffer
  return RawImage(rawData, Size(w, h), RGB_8, Interleaved);
}

/// Decodes a YCC encoded JPEG-2000 code stream
RawImage decodeRGB(char* buffer, int buflen) {
  assert(buffer && buflen);

  // Create Decoder
  sdec_t *dec =	rsa_dec_open_jpc_buffer(buffer, buflen, RSA_COLOR_RGB);
  unsigned int w = rsa_dec_imagewidth(dec);
  unsigned int h = rsa_dec_imageheight(dec);
  int error_code = RSA_DEC_SUCCESS;

  assert(w && h);
  RawImage img = JP2DecodeRegion(dec, 0, 0, 0, w-1, h-1, error_code);

  // Return
  rsa_dec_close(dec);
  return img;
}

/// Gets an ROI from Aperio TIFF files with compression tag 33003 or 33005.
//
/// These compression settings represent JPEG 2000 compression formats.
///
/// \note
/// The output is always stored in continuous planer configration. For example
/// for RGB images, the returned buffer is arranged as RGB RGB ... RGB and
/// subsampled data, are converted to have unit sampling.
///
/// \param tinfo
/// Pointer to tifhdr_t type.
///
/// \param tdir
/// The TIFF directory to read.
///
/// \param region
/// The area to extract from directory
///
/// \return
/// The pointer to the buffer holding the ROI.
RawImage getRegionAperioJP2(TIFF *in, tdir_t tdir, Rect region) {
  assert(in);
  RawImage region_image(size(region), RGB_8);

  // Set directory
  if (0 == TIFFSetDirectory(in, tdir)) return RawImage();

  // Get tile size
  uint32 tilew = 0, tileh = 0;
  TIFFGetField(in, TIFFTAG_TILEWIDTH, &tilew);
  TIFFGetField(in, TIFFTAG_TILELENGTH, &tileh);
  assert(tilew && tileh);

  // Check compression format
  uint16 compression = 0;
  TIFFGetField(in, TIFFTAG_COMPRESSION, &compression);
  assert((AperioSVSParser::APERIO_COMPRESSION_JP2K_RGB==compression) ||
         (AperioSVSParser::APERIO_COMPRESSION_JP2K_YCBCR==compression));

  // Allocate buffer
  assert(TIFFIsTiled(in));
  tsize_t tilesize = TIFFTileSize(in);
  tdata_t tile = _TIFFmalloc(tilesize);
  if (0 == tile) return RawImage();

  for (int y = region.y(); y <= yMax(region); y += tileh) {
    for (int x = region.x(); x <= xMax(region); x += tilew) {
      // Get TIFF Tile
      ttile_t tileno = TIFFComputeTile(in, x, y, 0, 0);
      if (0 == TIFFReadRawTile(in, tileno, tile, tilesize)) {
        _TIFFfree(tile);
        return RawImage();
      }

      // Decode tile
      // Note:
      //   Jasper color conversion is too slow - use custom YCC to RGB routine
      RawImage tile_image;
      if (AperioSVSParser::APERIO_COMPRESSION_JP2K_RGB == compression) {
        tile_image = decodeRGB(static_cast<char*>(tile), tilesize);
      }
      else if (AperioSVSParser::APERIO_COMPRESSION_JP2K_YCBCR == compression) {
        tile_image = decodeYCC(static_cast<char*>(tile), tilesize);
      }

      if (tile_image.isNull())
        return tile_image;

      assert(static_cast<uint32>(tile_image.width()) == tilew);
      assert(static_cast<uint32>(tile_image.height()) == tileh);

      // Calculate where this tile fits in region
      int tilex = (x / tilew) * tilew - region.x(); // flooring is intentional
      int tiley = (y / tileh) * tileh - region.y(); // flooring is intentional
      Rect dest_rect(Point(tilex, tiley), Size(tilew, tileh));

      // paint tile onto region
      if (size(dest_rect) == size(region)) {
        region_image = tile_image;
        break;
      }
      else {
        region_image.copy(dest_rect, tile_image, tile_image.rect());
      }
    }
  }

  // Free tile
  _TIFFfree(tile);

  return region_image;
}

} // namespace

AperioDecoder::AperioDecoder(const std::string& filename, QObject *parent)
    : SelectiveDecoder(ImageParserPtr(new AperioSVSParser(filename.c_str())),
      parent),
      tif_(),
      reader_(new TIFFReader(filename)) {
  init(filename);
}

AperioDecoder::AperioDecoder(boost::shared_ptr<TIFFParser> parser,
                             QObject * parent)
    : SelectiveDecoder(parser, parent),
      tif_(),
      reader_(new TIFFReader(parser->filename())) {
  init(parser->filename());
}

void AperioDecoder::init(const std::string& filename) {
  // Initialize the TIFF pointer if we expect JPEG-2000
  if (hasJP2Encoding(*reader_.get())) {
    tif_.reset(TIFFOpen(filename.c_str(), "rmCh"), TIFFDeleter);
    if (tif_.get() == 0) {
      throw FileAccessError(filename);
    }
  }
}

SelectiveDecoder::SelectiveDecoderPtr AperioDecoder::clone() const {
  SelectiveDecoderPtr ptr;
  try {
    ptr.reset(new AperioDecoder(filename(), parent()));
  }
  catch(const std::runtime_error&) {
  }
  return ptr;
}

AperioDecoder::~AperioDecoder() {
  /* Cleanup codec */
  waitForExit();
}

bool AperioDecoder::canRead(const std::string &filename) {
  try {
    AperioDecoder aperio(filename.c_str());
    return true;
  }
  catch(...) {
    return false;
  }
}

RawImage AperioDecoder::decodeRegion(const DecodeRequest &request) {
  const TIFFParser *aperio_parser =
    static_cast<const TIFFParser*>(&parser());
  assert(aperio_parser);

  // GEt region properties
  const tdir_t tdir = aperio_parser->dirs()[request.resolution()];
  Rect region = request.region(request.resolution());

  // Decode JPEG-2000 format
  boost::uint16_t compression = reader_->getTag16(tdir, TIFFTAG_COMPRESSION);
  if (AperioSVSParser::APERIO_COMPRESSION_JP2K_YCBCR == compression ||
      AperioSVSParser::APERIO_COMPRESSION_JP2K_RGB == compression ) {
    assert(tif_.get());
    return getRegionAperioJP2(static_cast<TIFF*>(tif_.get()), tdir, region);
  }

  // Decode regular TIFF format
  else {
    return reader_->getRegionForDisplay(tdir, region);
  }
}

} // namespace image
} // namespace sedeen
