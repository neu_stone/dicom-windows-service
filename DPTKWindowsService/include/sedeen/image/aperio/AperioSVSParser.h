#ifndef SEDEEN_SRC_IMAGE_APERIO_APERIOSVSPARSER_H
#define SEDEEN_SRC_IMAGE_APERIO_APERIOSVSPARSER_H

#include <vector>

#include "image/tiff/TIFFParser.h"
#include "image/tiff/TIFFReader.h"

namespace sedeen {
namespace image {

/// Contains Aperio-specific details
class AperioSVSParser : public TIFFParser {
 public:
  // Additional compression formats used in Aperio SVS files
  enum AperioCompressionFormats {
    APERIO_COMPRESSION_JP2K_YCBCR = 33003,
    APERIO_COMPRESSION_JP2K_RGB = 33005,
  };

  /// Attempts to open and read a SVS image with given \a filename.
  //
  /// \throws FileAccessError
  /// If the image cannot be read.
  ///
  /// \throws InvalidImage
  /// If the contents are not as expected.
  ///
  /// \param filename
  /// The image to open.
  explicit AperioSVSParser(const char *filename);

  ~AperioSVSParser();

 private:
  /// Parses TIFF description tag for relevant information
  bool parseTiffDescription(const TIFFReader& reader);

  // virtual functions from TIFFParser
  virtual TIFFReader doGetReader() const;

  virtual const TIFFDirVector& doGetDirs() const;

  virtual int doGetLabelDir() const;

  virtual int doGetMacroDir() const;

  virtual int doGetThumbnailDir() const;

  virtual std::string doGetCompression() const;

  // virtual functions from ImageParser
  virtual std::string doGetProducer() const;

  virtual SizeF doGetPixelSize() const;

  virtual double doGetMagnification() const;

  virtual ImagePyramidPtr doGetPyramid() const;

 private:
  typedef TIFFParser::TIFFDirVector TIFFDirVector;

  TIFFReader tif_;

  ImagePyramidPtr pyramid_;

  TIFFDirVector dirs_;

  int thumb_;

  int label_;

  int macro_;

  double mag_;

  double psize_;
};

}   // namespace image
}   // namespace sedeen

#endif
