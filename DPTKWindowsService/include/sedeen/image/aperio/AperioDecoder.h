#ifndef SEDEEN_SRC_IMAGE_APERIO_APERIODECODER_H
#define SEDEEN_SRC_IMAGE_APERIO_APERIODECODER_H

#include <boost/smart_ptr/scoped_ptr.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>

#include "image/io/SelectiveDecoder.h"

namespace sedeen {
namespace image {

class TIFFReader;
class TIFFParser;

/// TIFF decoder class implemented in Qt.
//
/// It selectively decodes TIFF images from disk using the libTiff library
/// (v4.0.0 or later).
///
/// \sa SelectiveDecoder for more details.
class AperioDecoder : public SelectiveDecoder {
  Q_OBJECT;

 public:
  /// Creates a decoder for the given image.
  //
  /// \sa getRegion()
  ///
  /// On error a runtime_error exception of type FileAccessError or InvalidImage
  /// is thrown.
  ///
  /// \param filename
  ///
  /// \param parent
  explicit AperioDecoder(const std::string& filename, QObject * parent = 0);

  /// Overloaded constructor.
  //
  //  TODO: change argument to accept AperioSVSParser to saftey. Currently
  //  a TIFFParser is used to allow ObjectivePathology images to be use the
  //  Aperio decoder
  //
  /// \param parser
  /// The image parser
  ///
  /// \param parent
  explicit AperioDecoder(boost::shared_ptr<TIFFParser> parser, 
                        QObject * parent = 0);

  /// Closes the image and releases all resources.
  virtual ~AperioDecoder();

  virtual SelectiveDecoderPtr clone() const;

  /// Checks to make sure file is readable.
  //
  /// The file must be readable, otherwise, return value is \c false.
  ///
  /// \param filename
  /// The name of the file to check.
  ///
  /// \return
  /// \c true if readable, \c false otherwise.
  static bool canRead(const std::string &filename);

 protected:
  virtual RawImage decodeRegion (const DecodeRequest &request);

 private:
  void init(const std::string& filename);

  /// Deleted.
  AperioDecoder(const AperioDecoder& decoder);

  /// Deleted.
  AperioDecoder& operator=(const AperioDecoder&);

  /// Get TIFF pixel spacing info from the given directory.
  //
  /// \param dir
  void getPixelSpacing(uint16_t dir);

 private:
  /// Used to handle JPEG2000 images
  boost::shared_ptr<void> tif_;

  boost::scoped_ptr<TIFFReader> reader_;
};

} // namespace image
} // namespace sedeen

#endif
