#include "image/aperio/AperioSVSParser.h"

#include <cstring>

#include <sstream>

#include <boost/algorithm/string/erase.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/trim.hpp>

#include <tiffio.h>

#include "global/Debug.h"
#include "global/UnitDefs.h"
#include "image/io/Exceptions.h"
#include "image/tiff/TIFFReader.h"

namespace sedeen {
namespace image {
namespace {

const char* APERIO_STRING = "Aperio SVS";

/// Parses old style Aperio metadata string (TIFF description tag).
//
/// Fills the input variables with magnification and pixel size values.
///
/// \param magnification
///
/// \param pixel_size
///
/// \return
/// \c true on success, \c false otherwise and sets both input variables to 0
bool parseVersionOld(const std::string &desc, double &magnification,
                     double &pixel_size) {
  magnification = 0;
  pixel_size = 0;
  std::stringstream descStream( desc );

  // Find pixel spacing in description tag
  while ((0 == pixel_size) || (0 == magnification)) {
    // Find next segment in string
    if (!descStream.good())
      return false;
    std::string substr;
    descStream >> substr;

    // get pixel size
    if ((0==pixel_size) && boost::contains(substr, "MPP")) {
      descStream >> substr;
      descStream >> pixel_size;
      pixel_size *= sedeen::length::um;
    } else if ((0 == magnification) && boost::contains(substr, "AppMag")) {
      descStream >> substr;
      descStream >> magnification;
    }
  }
  return true;
}

/// Parses the newer style Aperio Image Library TIFF description tag.
//
/// copydetails parseVersionOld()
bool parseVersionNew(const std::string &desc, double &magnification,
                     double &pixel_size) {
  magnification = 0;
  pixel_size = 0;
  std::stringstream descStream( desc );

  // Find pixel spacing in description tag
  while (0 == pixel_size) {
    // Find next segment in string
    if (!descStream.good())
      return false;
    std::string substr;
    descStream >> substr;

    // get pixel size
    if ((0==pixel_size) && boost::contains(substr, "Resolution")) {
      // get unit
      descStream >> substr;
      boost::erase_all(substr, " ");
      boost::erase_all(substr, "(");
      boost::erase_all(substr, "m");
      boost::erase_all(substr, ")");
      assert(substr.length()==1);
      const char symbol = substr[0];
      sedeen::LengthUnit unit = sedeen::fromStringToMetric(symbol);

      // get value
      descStream >> substr;
      descStream >> pixel_size;
      pixel_size *= unit;
    }
  }

  // caluculate magnification
  magnification = 10.0 / pixel_size;

  return true;
}

} // namespace

AperioSVSParser::AperioSVSParser(const char *filename)
    : TIFFParser(),
      tif_(TIFFReader(filename)),
      pyramid_(),
      dirs_(),
      thumb_(-1),
      label_(-1),
      macro_(-1),
      mag_(0),
      psize_(0) {
  // Check for errors oppening file
  if (0 == tif_.numDirectories()) {
    throw InvalidImage(filename, "File does not contain an image");
  }

  // ensure aperio image
  if (!parseTiffDescription(tif_))
    throw InvalidImage(filename,
                       "File does not appear to be in Aperio SVS format.");

  // create image pyramid -
  // thrown InvalidImage exception on error
  try {
    pyramid_ = getTiffPyramid(dirs_, tif_);
  }
  catch (InvalidPyramid &err) {
    throw InvalidImage(filename, err.what());
  }
}

AperioSVSParser::~AperioSVSParser() {
}

TIFFReader AperioSVSParser::doGetReader() const {
  return tif_;
}

const AperioSVSParser::TIFFDirVector& AperioSVSParser::doGetDirs() const {
  return dirs_;
}

int AperioSVSParser::doGetLabelDir() const {
  return label_;
}

int AperioSVSParser::doGetMacroDir() const {
  return macro_;
}

int AperioSVSParser::doGetThumbnailDir() const {
  return thumb_;
}

std::string AperioSVSParser::doGetCompression() const {
  assert(reader().numDirectories() > 0);
  std::string format = getCompressionFromTiff(reader(), 0);

  if (boost::icontains(format, "Unknown")) {
    switch(reader().getTag16(0, TIFFTAG_COMPRESSION)) {

      case APERIO_COMPRESSION_JP2K_YCBCR:
        format = "JPEG-2000 (YCbCr)";
        break;

      case APERIO_COMPRESSION_JP2K_RGB:
        format = "JPEG-2000 (RGB)";
        break;

      default:
        debug::assert_x(false, "Invalid format", __FUNCTION__, __LINE__);
        break;
    }
  }
  return format;
}

std::string AperioSVSParser::doGetProducer() const {
  return APERIO_STRING;
}

SizeF AperioSVSParser::doGetPixelSize() const {
  return SizeF(psize_,psize_);
}

double AperioSVSParser::doGetMagnification() const {
  return mag_;
}

ImagePyramidPtr AperioSVSParser::doGetPyramid() const {
  return pyramid_;
}

/*
  The first image in an SVS file is always the baseline image (full resolution).
  The second image is always a thumbnail, typically with dimensions of about
  1024 x 768 pixels. Following the thumbnail there may be one or more
  intermediate �pyramid� images.

  Optionally at the end of an SVS file there may be a slide label image,
  which is a low resolution picture taken of the slide�s label, and/or a macro
  camera image, which is a low resolution picture taken of the entire slide

  source: Aperio "Digital Slides and Third-Party Data Interchange"
  http://www.aperiotech.com/documents/api/Aperio_Digital_Slides_and_Third-party_data_interchange.pdf
*/
bool AperioSVSParser::parseTiffDescription(const TIFFReader& reader) {
  //Look in image description and find "Aperio Image"
  std::string description = reader.getTagString(0, TIFFTAG_IMAGEDESCRIPTION);
  if (!boost::icontains(description, "Aperio Image")) {
    return false;
  }

  // Get pixel size and magnification
  double mag = 0;
  double psize = 0;
  if (!parseVersionOld(description, mag, psize))
    if (!parseVersionNew(description, mag, psize))
      return false;
  psize_ = psize;
  mag_ = mag;

  // Set Thumbnail is the 2nd directory
  if (reader.numDirectories() < 2)
    return false;
  thumb_ = 1;

  // Find pyramid directories
  dirs_.push_back(0);
  for (int dir = 2; dir < reader.numDirectories(); ++dir) {
    description = reader.getTagString(dir, TIFFTAG_IMAGEDESCRIPTION);
    if (boost::icontains(description, "label")) {
      label_ = dir;
      continue;
    } else if (boost::icontains(description, "macro")) {
      macro_ = dir;
      continue;
    }
    dirs_.push_back(dir);
  }

  return true;
}

}   // namespace image
}   // namespace sedeen

