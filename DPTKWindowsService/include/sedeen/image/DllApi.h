#ifndef SEDEEN_SRC_IMAGE_DLLAPI_H
#define SEDEEN_SRC_IMAGE_DLLAPI_H

#include "DllApiControl.h"

#ifdef SEDEEN_image_EXPORT
#define SEDEEN_IMAGE_API SEDEEN_API_EXPORT
#else
#define SEDEEN_IMAGE_API SEDEEN_API_IMPORT
#endif

#endif
