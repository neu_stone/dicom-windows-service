// Primary header
#include "image/standard/StandardImageParser.h"

#include <cassert>
#include <sstream>
#include <limits>

#include "global/geometry/Size.h"
#include "image/io/Exceptions.h"
#include "image/standard/StandardPyramid.h"

namespace sedeen {
namespace image {
namespace standard {

namespace {

int numBitsPerComponent(const StandardImage::CImgType& image) {
  return static_cast<int>(std::log10(
             std::numeric_limits<StandardImage::CImgType::value_type>::max()+1.) 
              / std::log10(2.));
}

std::vector<int> extractBitsPerChannel(
    const StandardImage::CImgType& image) {
  return std::vector<int>(image.spectrum(), numBitsPerComponent(image));
}

Color extractColor(const StandardImage::CImgType& image,
                         const std::string& filename) {
  switch (image.spectrum()) {
    case 1: return Gray_8;
    case 3: return RGB_8;
    case 4: return ARGB_8;
    case 0:
    default: return Unknown;
  }
}

} // namespace

StandardImageParser::StandardImageParser(const StandardImage::CImgType& image,
                                         const std::string& filename) :
filename_(filename),
components_(image.spectrum()),
bpp_(numBitsPerComponent(image)*image.spectrum()),
bpc_(extractBitsPerChannel(image)),
color_(extractColor(image, filename_)),
image_pyramid_(new StandardPyramid(Size(image.width(),image.height()))) {
}

StandardImageParser::~StandardImageParser() {
}

ImagePyramidPtr StandardImageParser::doGetPyramid() const {
  return image_pyramid_;
}

bool StandardImageParser::doGetHasLabelImage() const {
  return false;
}

bool StandardImageParser::doGetHasMacroImage() const {
  return false;
}

bool StandardImageParser::doGetHasThumbnailImage() const {
  return false;
}

std::string StandardImageParser::doGetFilename() const {
  return filename_;
}

SizeF StandardImageParser::doGetPixelSize() const {
  return SizeF(0,0);
}

double StandardImageParser::doGetMagnification() const {
  return 0;
}

int StandardImageParser::doGetComponents() const {
  return components_;
}

int StandardImageParser::doGetBpp() const {
  return bpp_;
}

int StandardImageParser::doGetBpc(const int comp) const {
  assert(components_ > comp);
  return bpc_[comp];
}

std::string StandardImageParser::doGetProducer() const {
  return "Unknown";
}

Color StandardImageParser::doGetColor() const {
  return color_;
}

std::string StandardImageParser::doGetCompression() const {
  return "Unknown";
}

std::string StandardImageParser::doGetDescription() const {
  return "";
}

RawImage StandardImageParser::doGetLabelImage() const {
  return RawImage();
}

RawImage StandardImageParser::doGetMacroImage() const {
  return RawImage();
}

RawImage StandardImageParser::doGetThumbnailImage() const {
  return RawImage();
}

} // namespace standard
} // namespace image
} // namespace sedeen

