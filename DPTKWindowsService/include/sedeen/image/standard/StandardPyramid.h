#ifndef SEDEEN_SRC_IMAGE_STANDARD_STANDARDPYRAMID_H
#define SEDEEN_SRC_IMAGE_STANDARD_STANDARDPYRAMID_H

// user
#include "image/DllApi.h"
#include "image/buffer/RawImageTypes.h"
#include "image/io/ImagePyramid.h"

namespace sedeen {
namespace image {

class TileInfo;

namespace standard {

/// Describes the tile pyramid structure for a JPEG-2000 image.
//
/// Note that resolution level (reslvl) 0 must the represents full resolution
/// image.
class SEDEEN_IMAGE_API StandardPyramid : public ImagePyramid {
 public:
  /*!
    \brief constructor
  */
  StandardPyramid(const Size& size);

  /*!
    \brief Destructor
  */
  ~StandardPyramid();

 protected:
  int32_t doGetLevel(const double scale) const;
  ImagePyramidPtr doGetClone() const;
};

} // namespace standard
} // namespace image
} // namespace sedeen

#endif


