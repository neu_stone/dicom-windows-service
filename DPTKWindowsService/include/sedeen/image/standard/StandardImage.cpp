// Primary header
#include "image/standard/StandardImage.h"

#include <boost/make_shared.hpp>
#include <cassert>
#include <iostream>
#include <string>
#include <stdexcept>

// User includes
#include "global/Debug.h"
#include "global/geometry/Rect.h"
#include "global/geometry/Size.h"
#include "image/io/DecodeRequest.h"
#include "image/io/Exceptions.h"
#include "image/standard/StandardImageParser.h"

namespace sedeen {
namespace image {
namespace standard {
namespace {
const std::string RawImageFilename("sedeen::image::RawImage");

int getPermutedColors(const StandardImage::CImgType& img) {
  return img.width();
}

int getPermutedWidth(const StandardImage::CImgType& img) {
  return img.height();
}

int getPermutedHeight(const StandardImage::CImgType& img) {
  return img.depth();
}

} // namespace

StandardImage::CImgType* readImage(const std::string& filename) {
  try {
    return new StandardImage::CImgType(filename.c_str());
  } catch(...) {
    throw sedeen::image::FileAccessError(filename);
  }
  return new StandardImage::CImgType();
}

// Constructs an planar image with the contents of \a image
StandardImage::CImgType* toCImg(RawImage image) {
  assert(8 == image.bitsPerComponent() && false == image.isSigned());
  try {
    const boost::uint8_t *dataptr = static_cast<const boost::uint8_t*>(
                                              image.data().get());
    if (Planar == image.order()) {
      return new StandardImage::CImgType(dataptr,
                                        image.size().width(), 
                                        image.size().height(),
                                        1, image.components());
    } else {
      assert(Interleaved == image.order());
      StandardImage::CImgType *cimg = new StandardImage::CImgType(dataptr,
                                                        image.components(),
                                                        image.size().width(), 
                                                        image.size().height(),
                                                        1);
    cimg->permute_axes("yzcx");
    return cimg;
    }
  } catch(...) {
    throw sedeen::image::FileAccessError(RawImageFilename);
  }
  return new StandardImage::CImgType();
}

StandardImage::StandardImage(const std::string& filename)
    : ImageData(),
      m_image(readImage(filename)),
      m_filename(filename),
      m_image_parser(new StandardImageParser(*m_image, m_filename)) {
  if (m_image->data() == 0) throw FileAccessError(filename);
  // change image from planar configuration to interleaved
  m_image->permute_axes("cxyz");
}

StandardImage::StandardImage(RawImage image)
    : ImageData(),
      m_image(toCImg(image)),
      m_filename(RawImageFilename),
      m_image_parser(new StandardImageParser(*m_image, m_filename)) {
  // Accept Null RawImage's
  // change image from planar configuration to interleaved
  m_image->permute_axes("cxyz");
}

StandardImage::~StandardImage() {}

StandardImage::StandardImage(const StandardImage& standardImage)
    : ImageData(),
      m_image( new CImgType(*(standardImage.m_image)) ),
      m_filename(standardImage.m_filename),
      m_image_parser(new StandardImageParser(*m_image, m_filename)) {}

boost::shared_ptr<ImageData> StandardImage::doClone() const {
  return boost::shared_ptr<ImageData>(new StandardImage(*this));
}

Size StandardImage::doGetSize() const {
  // note: image axes have been permuted
  return Size(getPermutedWidth(*m_image), getPermutedHeight(*m_image));
}

std::string StandardImage::doGetFilename() const {
  return m_filename;
}

SizeF StandardImage::doGetPixelSize() const {
  return SizeF(m_image_parser->pixelSize().width(),
                m_image_parser->pixelSize().height());
}

boost::uint64_t StandardImage::doSetBufferCapacity(boost::uint64_t bytes) {
  return doGetBufferCapacity();
}

boost::uint64_t StandardImage::doGetBufferCapacity() const {
  return m_image->size() * m_image->max() / 255;
}

bool StandardImage::doIsBufferCapacityFixed() const {
  return true;
}

boost::uint64_t StandardImage::doBufferSizeRequired(int width,
                                                    int height) const {
  return doGetBufferCapacity();
}

boost::uint64_t StandardImage::doGetBufferSize() const {
  return doGetBufferCapacity();
}

RawImage StandardImage::doBufferImage(double scale) const {
  RawImage img(Size(1,1), Gray_8);
  img.fill(255);
  return img;
}

void StandardImage::doRequestRegion(const ImageAccessor& client,
                                 const DecodeRequestPtr& request,
                                 Priority priority) {

  // Crop the region required  -
  // image has been permuted
  const Rect region = request->region(0);
  CImgType part = m_image->get_crop(0, region.x(), region.y(),
                                    getPermutedColors(*m_image)- 1,
                                    xMax(region), yMax(region));
  if (part.is_empty())
    debug::warning("could not crop image\n.", __FUNCTION__, __LINE__);

  // Resize region
  const Size size = request->size();
  part.resize(getPermutedColors(part), size.width(), size.height());

  // make return buffer
  RawImage buffer;
  switch( getPermutedColors(part) ) {
    case 1:
      buffer = RawImage(Size(getPermutedWidth(part), getPermutedHeight(part)),
                        Gray_8);
      break;

    case 3:
      buffer = RawImage(Size(getPermutedWidth(part), getPermutedHeight(part)),
                        RGB_8);
      break;

    case 4:
      buffer = RawImage(Size(getPermutedWidth(part), getPermutedHeight(part)),
                        ARGB_8);
      break;

    default:
      assert(false);
      break;
  }
  // Fill image buffer
  if (!buffer.isNull()) {
    CImgType::value_type *ptr = static_cast<CImgType::value_type*>(part.data());
    unsigned int elements = part.size();
    for (unsigned int i = 0; i < elements; i++)
      buffer.setValue(i, ptr[i]);
  }
  updateClient(client, buffer, true);
}

const ImageParser& StandardImage::getImageParser() const {
  return *m_image_parser;
}

} // namespace standard
} // namespace image
} // namespace sedeen
