//main header
#include "image/standard/StandardPyramid.h"

// system includes
#include <cassert>
#include <vector>
#include <cmath>
#include <boost/make_shared.hpp>

#include "global/geometry/Size.h"

namespace sedeen {
namespace image {
namespace standard {

StandardPyramid::StandardPyramid(const Size& size)
    : ImagePyramid(std::vector<Size>(1,size), size) {}

StandardPyramid::~StandardPyramid() {}

int32_t StandardPyramid::doGetLevel(const double scale) const {
  return 0;
}

ImagePyramidPtr StandardPyramid::doGetClone() const {
  return boost::make_shared<StandardPyramid>(size(0));
}

} // namespace standard
} // namespace image
} // namespace sedeen


