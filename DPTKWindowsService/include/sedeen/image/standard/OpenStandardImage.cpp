// Primary header
#include "image/standard/OpenStandardImage.h"

// User headers
#include "image/standard/StandardImage.h"

namespace sedeen {
namespace image {
namespace standard {

std::auto_ptr<ImageData> OpenStandardImage::doOpen(
    const std::string& filename) const {
  // Declare the return variable.
  std::auto_ptr<ImageData> image;

  // Attempt to let Qt handle the opening of the file
  try {
    image.reset(new StandardImage(filename));
  } catch (...) {
    // Just return the uninitialized image.
  }

  return image;
}

QStringList OpenStandardImage::getFilter() const {
  // Make a list of all suffixes accepted by CImg
  QStringList suffixes;
  suffixes << "jpg" << "bmp" << "png" << "gif" << "tif";
  return suffixes;
}

} // namespace standard
} // namespace image
} // namespace sedeen
