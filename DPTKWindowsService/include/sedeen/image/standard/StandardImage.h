#ifndef SEDEEN_SRC_IMAGE_STANDARD_STANDARDIMAGE_H
#define SEDEEN_SRC_IMAGE_STANDARD_STANDARDIMAGE_H

#include <memory>
#include <boost/cstdint.hpp>
#include <boost/make_shared.hpp>

// User includes
#include "image/DllApi.h"
#include "image/io/ImageData.h"

// Include AFTER ImageData.h - qt includes fail otherwise.
#include <CImg.h>

namespace sedeen {
namespace image {
namespace standard {

class StandardImageParser;

/// Access an image loaded entirely into memory.
//
/// This class is provided to offer an interface to QImage objects as if they
/// were selectively decoded objects.
class SEDEEN_IMAGE_API StandardImage : public ImageData {
 public:
  /// Open file using a CImg.
  //
  /// If CImg will not accept it, throw an FileAccessError exception.
  //  This ensures that all
  /// instances of StandardImage are valid, working Image objects. This is as
  /// opposed to creating zombie objects
  /// (http://www.parashift.com/c++-faq-lite/exceptions.html#faq-17.8).
  ///
  /// \param filename
  /// Location of file to be opened.
  ///
  /// \throw
  /// A runtime_error containing a message stating the file could not be opened.
  ///
  /// \post
  /// A valid StandardImage is constructed.
  explicit StandardImage(const std::string& filename);

  // Copies a 8 bit-per-sample RawImage
  //
  /// Throws an FileAccessError exception on error. Assigned a default filename
  /// to the instance of \a sedeen::image::RawImage
  StandardImage(RawImage image);

  virtual ~StandardImage();

  typedef cimg_library::CImg<boost::uint8_t> CImgType;

 protected:
  StandardImage(const StandardImage&);

 private:
  /// Disallow assignment
  StandardImage& operator=(const StandardImage&);

  boost::shared_ptr<ImageData> doClone() const;

  // Virtual
  Size doGetSize() const;

  // Virtual
  std::string doGetFilename() const;

  // Virtual
  SizeF doGetPixelSize() const;

  // Virtual
  boost::uint64_t doSetBufferCapacity(boost::uint64_t bytes);

  // Virtual
  boost::uint64_t doGetBufferCapacity() const;

  // Virtual
  bool doIsBufferCapacityFixed() const;

  // Virtual
  boost::uint64_t doBufferSizeRequired(int width, int height) const;

  // Virtual
  RawImage doBufferImage(double scale) const;

  // Virtual
  boost::uint64_t doGetBufferSize() const;

  // Virtual
  void doRequestRegion(const ImageAccessor& client,
                       const DecodeRequestPtr& request,
                       Priority priority);

  const ImageParser& getImageParser() const;

 private:
  typedef std::auto_ptr<CImgType> CImgPtr;
  typedef std::auto_ptr<StandardImageParser> StandardImageParserPtr;

  /// Handle to the image data.
  CImgPtr m_image;

  /// Name of the opened file.
  std::string m_filename;

  StandardImageParserPtr m_image_parser;
}; // end class StandardImage

} // namespace standard
} // namespace image
} // namespace sedeen

#endif
