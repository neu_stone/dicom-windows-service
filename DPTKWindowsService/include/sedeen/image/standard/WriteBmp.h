#ifndef SEDEEN_SRC_IMAGE_STANDARD_WRITEBMP_H
#define SEDEEN_SRC_IMAGE_STANDARD_WRITEBMP_H

// User headers
#include "image/DllApi.h"
#include "image/io/ImageWriter.h"

namespace sedeen {
namespace image {
namespace standard {

class SEDEEN_IMAGE_API WriteBmp : public ImageWriter {
 public:
  WriteBmp(QObject* parent = 0);

  virtual ~WriteBmp();

 private:
  virtual void doWrite(image::ImageAccessor imageAccessor,
                       const QString& filename, const QRect& inputRegion,
                       double zoom) const;

  virtual QString getDefaultExtension() const;

  virtual QString getFileFilter() const;

  virtual QWidget* buildOptionWidget() const;
}; // class WriteBmp

} // namespace standard
} // namespace crop
} // namespace sedeen

#endif
