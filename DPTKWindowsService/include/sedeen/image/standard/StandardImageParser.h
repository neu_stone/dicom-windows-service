#ifndef SEDEEN_SRC_IMAGE_STANDARD_STANDARDIMAGEPARSER_H
#define SEDEEN_SRC_IMAGE_STANDARD_STANDARDIMAGEPARSER_H

#include "image/io/ImageParser.h"
#include "image/io/ImagePyramid.h"

#include "image/standard/StandardImage.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {
namespace standard {

class SEDEEN_IMAGE_API StandardImageParser : public ImageParser {
  public:
    StandardImageParser(const StandardImage::CImgType& image,
                        const std::string& filename);
    ~StandardImageParser();

  private:
    // virtual functions
    bool doGetHasLabelImage() const;
    bool doGetHasMacroImage() const;
    bool doGetHasThumbnailImage() const;
    std::string doGetFilename() const;
    std::string doGetProducer() const;
    Color doGetColor() const;
    std::string doGetCompression() const;
    std::string doGetDescription() const;
    SizeF doGetPixelSize() const;
    int doGetComponents() const;
    int doGetBpp() const;
    int doGetBpc(const int comp) const;
    double doGetMagnification() const;
    RawImage doGetLabelImage() const;
    RawImage doGetThumbnailImage() const;
    RawImage doGetMacroImage() const;
    ImagePyramidPtr doGetPyramid() const;

  private:
    const std::string filename_;
    const int components_;
    const int bpp_;
    const std::vector<int> bpc_;
    const Color color_;
    ImagePyramidPtr image_pyramid_;
}; // class StandardImageParser

} // namespace standard
} // namespace image
} // namespace sedeen

#endif  // ifdef sedeen_image_standard_StandardImageParser__h
