#ifndef SEDEEN_SRC_IMAGE_STANDARD_OPENSTANDARDIMAGE_H
#define SEDEEN_SRC_IMAGE_STANDARD_OPENSTANDARDIMAGE_H

// System headers
#include <QtCore>
#include <memory>

// User headers
#include "image/DllApi.h"
#include "image/io/ImageOpenHandler.h"

namespace sedeen {
namespace image {
namespace standard {

/// Fall-back point using QImage to open anything it can.
class SEDEEN_IMAGE_API OpenStandardImage : public ImageOpenHandler {
 private:
  virtual std::auto_ptr<ImageData> doOpen(const std::string& filename) const;

  /// Appends all QImage-compatible extensions.
  //
  /// This will include any formats enabled by the Qt plugin system.
  virtual QStringList getFilter() const;
}; // class OpenStandardImage

} // namespace standard
} // namespace image
} // namespace sedeen

#endif
