#ifndef SEDEEN_SRC_IMAGE_IMAGEREPOSITORY_H
#define SEDEEN_SRC_IMAGE_IMAGEREPOSITORY_H

// System headers
#include <list>
#include <string>
#include <map>

// Qt includes
#include <QtCore>

// Boost includes
#include <boost/cstdint.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

// User includes
#include "image/DllApi.h"
#include "image/io/ImageOpener.h"

namespace sedeen {
namespace image {

class ImageAccessor;
class ImageData;

extern const boost::uint64_t MaxBytesPerImageBuffer;

/// holds settings for ImageRepository
class SEDEEN_IMAGE_API ImageRepositorySettings {
 public:
  /// Set the total buffer limits for all open imges
  //
  /// The default value of zero establishes and default buffer limit based on
  /// half of the system memory
  ///
  /// \param bufferCapacity
  /// total buffer capacity of all open images or 0 to determine automatically
  ///
  /// \param maxBufferCapacity
  /// ?
  ImageRepositorySettings(boost::uint64_t bufferCapacity = 0,
                          boost::uint64_t maxBufferCapacity = 0x7FFFFFFF);
  bool operator==(const ImageRepositorySettings& settings) const;
  bool operator!=(const ImageRepositorySettings& settings) const;
  boost::uint64_t m_bufferCapacity;
  boost::uint64_t m_maxBufferCapacity;
};

/// Singleton collection of open image resources.
//
/// Instead of creating a new set of resources for every client of an image
/// file, the ImageRepository offers an organizational collection-point for
/// shared resources. By using the image() function rather than instantiating an
/// Image derivative manually, the client receives a clone of the current Image-
/// type accessing the named file.
///
/// Periodically, the purge() function must be called to close off any files not
/// currently in use.
class SEDEEN_IMAGE_API ImageRepository {
 public:
  typedef std::map<std::string, boost::weak_ptr<ImageData> > Map_t;

  static ImageRepository& instance();

  ~ImageRepository();

  /// Open or retrieve an instance of an image.
  ImageAccessor get(const std::string& filename);

  /// purges the repository of expired handles to images no longer being used.
  /// The value of count() may change after a purge()
  void purge();

  /// Gets the list of recognized file suffixes.
  QString filter() const;

  /// Sets the settings of repository
  void setSettings(const ImageRepositorySettings& settings);

  /// Set the total buffer capacity of all resource managed by the repository
  //
  /// If \a bytes is zero, a default buffer limit is determined based on half of
  /// the avaiable memory. Note that no image will ever get more than
  /// \a MaxBytesPerImageBuffer unless the image requires it.
  ///
  /// \param bytes
  /// Total size in bytes to allocate for images
  void setTotalBufferLimit(boost::uint64_t bytes);

  /// Sum the buffer size of all open images.
  boost::uint64_t getTotalBufferSize() const;

  /// Gets the settings of repository
  const ImageRepositorySettings& settings() const;
 private:

  /// Sets the current settings() to the open images
  void applySettings();

  ImageRepository(const ImageOpener& imageOpener);

  Q_DISABLE_COPY(ImageRepository)

  /// Extract the data from the repository.
  boost::shared_ptr<ImageData> find(const std::string& filename);

  /// Open the file and create a new entry.
  boost::shared_ptr<ImageData> create(const std::string& filename);

  /// \relates ImageRepository
  /// \brief Determines the maximum buffer size for individual images.
  /// The result of this method is a suitable buffer size for tiled images given
  /// the memory limit for the repository.
  /// \note the return value will change based the number and type of images in
  /// the reposiotry. Therefore, value should be queried everytime it is needed.
  ///
  /// \param total_bytes memory allocated for the entire repository (in bytes)
  ///        and must be non-zero
  /// \return the buffering limit for individual images in the repository
  boost::uint64_t
  imageBufferSize(boost::uint64_t total_bytes);

 private:
  /// Map of file name to data object.
  Map_t m_images;

  /// Image opener stack.
  ImageOpener m_opener;

  /// Instance-specific lock for threaded access.
  mutable QReadWriteLock m_locker;

  /// Instance of the null pointer
  boost::shared_ptr<ImageData> m_nullImageData;

  /// Settings class
  ImageRepositorySettings m_settings;
}; // end class ImageRepository

} // end namespace image
} // end namespace sedeen

#endif
