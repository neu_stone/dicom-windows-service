// Main headers
#include "image/leica/LeicaXMLParser.h"

// System headers
#include <cstring>
#include <sstream>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include <tinyxml2/tinyxml2.h>

// User headers
#include "global/UnitDefs.h"
#include "global/geometry/Point.h"
#include "global/geometry/Size.h"
#include "global/geometry/Rect.h"

namespace sedeen {
namespace image {
namespace leica {
namespace {

const char* LEICA_XML_NAMESPACE =
    "http://www.leica-microsystems.com/scn/2010/10/01";

/// Parses the collection element and extract properties
//
/// \param collection
///
/// \return
/// The collection tag properties or a NULL pointer on error
LeicaSCNPropertiesPtr parseCollectionNode(
    const tinyxml2::XMLElement& collection);

/// Returns a vector containing the layers in a "pixels" element
//
/// \param pixels
///
/// \return
/// Each element contains the size of the layer and the TIFF directory of the
/// image.
std::vector<int> parsePixelsNode(const tinyxml2::XMLElement& pixels);

/// Returns the contents of the "scanSettings" element
//
/// \param settings
///
/// \return
/// The scan propertes or a NULL pointer on error.
boost::shared_ptr<LeicaScanSettings> parseScanSettingsNode(
    const tinyxml2::XMLElement& settings);

/// Fill \a settings from the "channelSettings" element
//
/// \param channel
///
/// \param settings
///
/// \return
/// RGB colors for each channel.
void parseChannelSettingsNode(const tinyxml2::XMLElement& channel,
                              LeicaScanSettings& settings);

/// Get model attributes
//
/// \param element
///
/// \return
/// The 1st part of the model attribute from element or an empty string on
/// error.
std::string getModelAttribute(const tinyxml2::XMLElement& element);

/// Returns the RGB attribute of the XML channel element or -1 on error.
//
/// \param element
int getChannelColorAttribute(const tinyxml2::XMLElement& element);

/// Returns the sizeX and sizeY attributes from \a element or NULL Size on error
//
/// \param element
Size getSizeAttribute(const tinyxml2::XMLElement& element);

/// Returns the offsetX and offsetY attributes from \a element or NULL Size on error
//
/// \param element
Point getOffsetAttribute(const tinyxml2::XMLElement& element);

/// Returns \a attribute value as integer from \a element or -1 on error
//
/// \param element
///
/// \param attribute
int getIntAttribute(const tinyxml2::XMLElement& element, const char *attribute);

/// Returns \a attribute value from \a element or NULL string on error
//
/// \param element
///
/// \param attribute
std::string getStringAttribute(const tinyxml2::XMLElement& element,
                               const char *attribute);

} // namespace

//class definition
class PrivateScanSettings {
 public:
  PrivateScanSettings(LeicaScanSettings::ImageType type, double magnification,
                      double numerical_aperture)
      : type(type),
        mag(magnification),
        na(numerical_aperture) {
    assert(0 < na && 0 < mag);
  }

 public:
  LeicaScanSettings::ImageType type;
  double mag;
  double na;

  std::map<int, std::string> name;
  std::map<int, int> color;
  std::map<int, int> exposure;
  std::map<int, int> ccd_gain;
  std::map<int, ChannelMap> fluorescence_cube;
  std::map<int, ChannelMap> fluorescence_filter;
};

//class definition
class PrivateImageProperties {
 public:
  PrivateImageProperties(const IntVector& directories, const Rect &view_area,
                         const std::string &model, const std::string &version,
                         const std::string &creation_date,
                         const LeicaScanSettings &settings)
      : area(view_area),
        dirs(directories),
        date(creation_date),
        model(model),
        version(version),
        settings(settings) {
    assert(directories.size());
  }

 public:
  Rect area;
  IntVector dirs;
  std::string date;
  std::string model;
  std::string version;
  LeicaScanSettings settings;
};

//class definition
LeicaSCNProperties::LeicaSCNProperties(const LeicaImageProperties &main,
                                       const LeicaImageProperties &macro)
    : main_(main),
      macro_(macro) {
}

const LeicaImageProperties& LeicaSCNProperties::macro() const {
  return macro_;
}

const LeicaImageProperties& LeicaSCNProperties::main() const {
  return main_;
}

//class definition
LeicaImageProperties::LeicaImageProperties(const IntVector& directories,
                                           const Rect &view_area,
                                           const std::string &model,
                                           const std::string &version,
                                           const std::string &creation_date,
                                           const LeicaScanSettings &settings)
    : properties_(new PrivateImageProperties(directories, view_area, model,
                                             version, creation_date,
                                             settings)) {
}

const Rect& LeicaImageProperties::rect() const {
  return properties_->area;
}

const std::string& LeicaImageProperties::date() const {
  return properties_->date;
}

const std::string& LeicaImageProperties::model() const {
  return properties_->model;
}

const std::string& LeicaImageProperties::version() const {
  return properties_->version;
}

const IntVector& LeicaImageProperties::dirs() const {
  return properties_->dirs;
}

const LeicaScanSettings& LeicaImageProperties::settings() const {
  return properties_->settings;
}

//class definition
LeicaScanSettings::LeicaScanSettings(LeicaScanSettings::ImageType type,
                                     double magnification,
                                     double numerical_aperture)
    : settings_(new PrivateScanSettings(type, magnification,
                                        numerical_aperture)) {
}

LeicaScanSettings::ImageType LeicaScanSettings::type() const {
  return settings_->type;
}

double LeicaScanSettings::numericalAperture() const {
  return settings_->na;
}

double LeicaScanSettings::magnification() const {
  return settings_->mag;
}

int LeicaScanSettings::channels() const {
  assert(Fluorescence == type());
  return static_cast<int>(settings_->color.size());
}

int LeicaScanSettings::color(int channel_index) const {
  assert(Fluorescence == type());
  assert(channel_index <= channels());
  return static_cast<int>(settings_->color[channel_index]);
}

int LeicaScanSettings::gain(int channel_index) const {
  assert(Fluorescence == type());
  assert(channel_index <= channels());
  return static_cast<int>(settings_->ccd_gain[channel_index]);
}

int LeicaScanSettings::exposureTime(int channel_index) const {
  assert(Fluorescence == type());
  assert(channel_index <= channels());
  return static_cast<int>(settings_->exposure[channel_index]);
}

const std::string& LeicaScanSettings::cubeProperties(
    int channel_index,
    FluorescenceProperty id) const {
  assert(Fluorescence == type());
  assert(channel_index <= channels());
  return settings_->fluorescence_cube[channel_index][id];
}

const std::string& LeicaScanSettings::filterProperties(
    int channel_index,
    FluorescenceProperty id) const {
  assert(Fluorescence == type());
  assert(channel_index <= channels());
  return settings_->fluorescence_filter[channel_index][id];
}

void LeicaScanSettings::setChannel(int index,
                                   const char* name,
                                   int color,
                                   int exposure_time,
                                   int ccd_gain,
                                   ChannelMap fluorescence_cube,
                                   ChannelMap fluorescence_filter) {
  assert(Fluorescence == type());
  assert(index <= channels());
  settings_->name[index] = name;
  settings_->color[index] = color;
  settings_->exposure[index] = exposure_time;
  settings_->ccd_gain[index] = ccd_gain;
  settings_->fluorescence_cube[index] = fluorescence_cube;
  settings_->fluorescence_filter[index] = fluorescence_filter;
}

//
// LeicaSCNProperties Friend function
//
LeicaSCNPropertiesPtr parseLeicaXML(const std::string &xml_description) {
  using tinyxml2::XMLDocument;
  using tinyxml2::XMLElement;
  using tinyxml2::XML_NO_ERROR;

  LeicaSCNPropertiesPtr ptr;

  // Parse XML
  XMLDocument doc;
  if (doc.Parse(xml_description.c_str()) != XML_NO_ERROR)
    return ptr;

  // Check the namespace attribute in scn node
  const XMLElement* node = doc.FirstChildElement("scn");
  if (0 == node ||
      0 == node->Attribute("xmlns") ||
      0 != strcmp(LEICA_XML_NAMESPACE, node->Attribute("xmlns")))
    return ptr;

  // Parse the collection element
  node = node->FirstChildElement("collection");
  if (0 == node) return ptr;

  // return
  return parseCollectionNode(*node);
}

namespace {

LeicaSCNPropertiesPtr parseCollectionNode(
    const tinyxml2::XMLElement& collection) {
  using namespace tinyxml2;

  assert(0 == strcmp(collection.Name(), "collection"));

  boost::shared_ptr<LeicaImageProperties> macro_image, main_image;
  LeicaSCNPropertiesPtr ptr;

  // Find the collection node and extract its size
  const Rect collection_area(getOffsetAttribute(collection), 
                             getSizeAttribute(collection));
  if (isEmpty(collection_area)) return ptr;

  // Find the image node
  const XMLElement* image_node = collection.FirstChildElement("image");
  if (0 == image_node) return ptr;

  while (true) {
    // Find the date node
    const XMLElement *node = image_node->FirstChildElement("creationDate");
    if (0 == node) return ptr;
    const std::string date = node->GetText();

    // Find the device node
    node = image_node->FirstChildElement("device");
    if (0 == node) return ptr;
    const std::string model = getModelAttribute(*node);
    const std::string version = getStringAttribute(*node, "version");

    // Find the view node
    node = image_node->FirstChildElement("view");
    if (0 == node) return ptr;
    const Rect view_area(getOffsetAttribute(*node), getSizeAttribute(*node));
    if (isEmpty(view_area)) return ptr;

    // Find the pixels node
    node = image_node->FirstChildElement("scanSettings");
    if (0 == node) return ptr;
    boost::shared_ptr<LeicaScanSettings> settings= parseScanSettingsNode(*node);
    if (0 == settings.get()) return ptr;

    // Find the pixels node
    node = image_node->FirstChildElement("pixels");
    if (0 == node) return ptr;
    const Size pixels_size = getSizeAttribute(*node);
    if (isEmpty(pixels_size)) return ptr;
    const std::vector<int> layers = parsePixelsNode(*node);
    if (layers.empty()) return ptr;

    // Check for macro image
    if (view_area == collection_area) {
      macro_image.reset(new LeicaImageProperties(layers, view_area, model,
                                                 version, date, *settings));
    }
    // Main image, record its magnificaion and directories
    else {
      main_image.reset(new LeicaImageProperties(layers, view_area, model,
                                                version, date, *settings));
    }

    // Get next image node
    image_node = image_node->NextSiblingElement("image");
    if (0 == image_node) break;
  }

  // Return
  if (main_image.get() && macro_image.get())
    ptr.reset(new LeicaSCNProperties(*main_image, *macro_image));

  return ptr;
}

boost::shared_ptr<LeicaScanSettings> parseScanSettingsNode(
    const tinyxml2::XMLElement& settings) {
  using namespace tinyxml2;

  assert(0 == strcmp(settings.Name(), "scanSettings"));
  boost::shared_ptr<LeicaScanSettings> ptr;

  // Find the objective node
  const XMLElement* node = settings.FirstChildElement("objectiveSettings");
  if (0 == node) return ptr;
  node = node->FirstChildElement("objective");
  if (0 == node) return ptr;
  const double magnification = atof(node->GetText());

  // Find the objective node
  node = settings.FirstChildElement("illuminationSettings");
  if (0 == node) return ptr;
  node = node->FirstChildElement("numericalAperture");
  if (0 == node) return ptr;
  const double numerical_aperture = atof(node->GetText());

  // Find the objective node
  LeicaScanSettings::ImageType type = LeicaScanSettings::Brightfield;
  node = settings.FirstChildElement("illuminationSettings");
  if (0 == node) return ptr;
  node = node->FirstChildElement("illuminationSource");
  if (0 == node) return ptr;
  if (0 == strcmp(node->GetText(), "fluorescence"))
    type = LeicaScanSettings::Fluorescence;

  boost::shared_ptr<LeicaScanSettings> scan_settings(
      new LeicaScanSettings(type, magnification, numerical_aperture));

  // Find the channel settings node
  node = settings.FirstChildElement("channelSettings");
  if (0 != node) parseChannelSettingsNode(*node, *scan_settings);

  return scan_settings;
}

std::vector<int> parsePixelsNode(const tinyxml2::XMLElement& pixels) {
  using namespace tinyxml2;
  assert(0 == strcmp(pixels.Name(), "pixels"));
  std::vector<int> dirs;

  // Find the dimensions node
  const XMLElement* node = pixels.FirstChildElement("dimension");
  if (0 == node) return dirs;

  // Iterate through all of the pixels tags and record the sizes
  while (true) {
    // record size
    int dir = getIntAttribute(*node, "ifd");
    if (-1 == dir) break;
    else dirs.push_back(dir);

    // go to next element
    node = node->NextSiblingElement ("dimension");
    if (0 == node) break;
  }

  return dirs;
}

void parseChannelSettingsNode(const tinyxml2::XMLElement& channelSettings,
                              LeicaScanSettings &settings) {
  using namespace tinyxml2;
  assert(0 == strcmp(channelSettings.Name(), "channelSettings"));

  // Find the dimensions node
  const XMLElement* channel_node = channelSettings.FirstChildElement("channel");
  if (0 == channel_node) return;

  // Iterate through all of the pixels tags and record the sizes
  while (true) {
    // record channel color
    const std::string name = getStringAttribute(*channel_node, "name");
    const int index = getIntAttribute(*channel_node, "index");
    const int color = getChannelColorAttribute(*channel_node);
    if ((-1 == index) || (-1 == color)) break;
    assert(settings.channels() == index);

    // record channel exposure
    const XMLElement* node = channel_node->FirstChildElement("exposureTime");
    if (0 == node) break;
    const int exposure = atoi(node->GetText());

    // record channel gain
    node = channel_node->FirstChildElement("ccdGain");
    if (0 == node) break;
    const int gain = atoi(node->GetText());

    // record fluorescence cube
    typedef LeicaScanSettings LSS;
    ChannelMap cube;
    const XMLElement* cube_node = channel_node->FirstChildElement(
        "fluorescenceCube");
    if (0 != cube_node) {
      node = cube_node->FirstChildElement("name");
      if (node && node->GetText())
        cube[LSS::Name] = node->GetText();
      node = cube_node->FirstChildElement("shortName");
      if (node && node->GetText())
        cube[LSS::ShortName] = node->GetText();
      node = cube_node->FirstChildElement("excitationFilter");
      if (node && node->GetText())
        cube[LSS::ExcitationFilter] = node->GetText();
      node = cube_node->FirstChildElement("dichromaticMirror");
      if (node && node->GetText())
        cube[LSS::DichromaticMirror] = node->GetText();
      node = cube_node->FirstChildElement("suppressionFilter");
      if (node && node->GetText())
        cube[LSS::SuppressionFilter] = node->GetText();
      node = cube_node->FirstChildElement("materialNumber");
      if (node && node->GetText())
        cube[LSS::MaterialNumber] = node->GetText();
      node = cube_node->FirstChildElement("comment");
      if (node && node->GetText())
        cube[LSS::Comment] = node->GetText();
    }

    // record fluorescence filter
    ChannelMap filter;
    const XMLElement *filter_node = channel_node->FirstChildElement(
        "fluorescenceFilter");
    if (0 != filter_node) {
      node = filter_node->FirstChildElement("name");
      if (node && node->GetText())
        filter[LSS::Name] = node->GetText();
      node = filter_node->FirstChildElement("excitationFilter");
      if (node && node->GetText())
        filter[LSS::ExcitationFilter] = node->GetText();
      node = filter_node->FirstChildElement("materialNumber");
      if (node && node->GetText())
        filter[LSS::MaterialNumber] = node->GetText();
      node = filter_node->FirstChildElement("comment");
      if (node && node->GetText())
        filter[LSS::Comment] = node->GetText();
    }

    // Add channel to settings
    settings.setChannel(index, name.c_str(), color,
                        exposure, gain, cube, filter);

    // go to next element
    channel_node = channel_node->NextSiblingElement("channel");
    if (0 == channel_node) break;
  }
}

Size getSizeAttribute(const tinyxml2::XMLElement& element) {
  const int x = getIntAttribute(element,"sizeX");
  const int y = getIntAttribute(element,"sizeY");
  if ((-1 != x) || (-1 != y)) return Size(x,y);
  else return Size();
}

Point getOffsetAttribute(const tinyxml2::XMLElement& element) {
  const int x = getIntAttribute(element,"offsetX");
  const int y = getIntAttribute(element,"offsetY");
  if ((-1 != x) || (-1 != y)) return Point(x,y);
  else return Point();
}

int getChannelColorAttribute(const tinyxml2::XMLElement& element) {
  assert(0 == strcmp(element.Name(), "channel"));
  const char *value = element.Attribute("rgb");
  if (0 != value) {
    std::string color(value);
    boost::replace_first(color, "#", "0x");

    std::stringstream ss;
    unsigned int val;
    ss << std::hex << color;
    ss >> val;
    return val;
  }
  else return -1;
}

int getIntAttribute(const tinyxml2::XMLElement& element,
                    const char* attribute) {
  assert(attribute);
  const char* value = element.Attribute(attribute);
  if (0 != value) return atoi(value);
  else return -1;
}

std::string getStringAttribute(const tinyxml2::XMLElement& element,
                               const char *attribute) {
  assert(attribute);
  const char *value = element.Attribute(attribute);
  if (0 != value) return value;
  else return "";
}

std::string getModelAttribute(const tinyxml2::XMLElement& element) {
  if (element.Attribute("model")) {
    std::vector<std::string> fields;
    std::string attribute = element.Attribute("model");
    boost::split(fields, attribute, boost::is_any_of(";"));
    return fields[0];
  }
  else return "";
}

} // namespace
} // namespace leica
} // namespace image
} // namespace sedeen
