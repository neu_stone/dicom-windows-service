#ifndef SEDEEN_SRC_IMAGE_APERIO_LEICASCNPARSER_H
#define SEDEEN_SRC_IMAGE_APERIO_LEICASCNPARSER_H

// System headers
#include <vector>

// User headers
#include "image/tiff/TIFFParser.h"
#include "image/tiff/TIFFReader.h"

namespace sedeen {
namespace image {
namespace leica {

class LeicaSCNProperties;

} // namespace leica

/// Contains Aperio specific details
class LeicaSCNParser : public TIFFParser {
  public:
  /// Attempts to open and read a Leica formated SCN image.
  //
  /// \throws FileAccessError
  /// If the image cannot be read.
  ///
  /// \throws InvalidImage
  /// If the contents are not as expected.
  ///
  /// \param filename
  /// The image to open
  explicit LeicaSCNParser(const char *filename);

  ~LeicaSCNParser();

 private:
  /// Parses TIFF description tag for relevant information
  bool parseTiffDescription(TIFFReader reader);

  // virtual functions from TIFFParser
  virtual TIFFReader doGetReader() const;

  virtual const TIFFDirVector& doGetDirs() const;

  /// Get the label image.
  //
  /// \notes Leica does not explicitly provide a label image. Rather the label
  /// is part of the macro image. We take the 1/3 of the slide area and assume 
  /// that it contains the lable. This assumntion is actually inaccurate and will 
  /// lead to label images that include portions of the slide area.
  //
  /// \return 
  /// label image
  virtual RawImage doGetLabelImage() const;

  /// Get the thumbnial image.
  //
  /// \notes Leica does not explicitly provide a thumbnail image. Rather it
  /// is part of the macro image. 
  //
  /// \return 
  /// label image
  virtual RawImage doGetThumbnailImage() const;

  virtual int doGetLabelDir() const;

  virtual int doGetMacroDir() const;

  virtual int doGetThumbnailDir() const;

  // virtual functions from ImageParser
  virtual std::string doGetProducer() const;

  virtual SizeF doGetPixelSize() const;

  virtual double doGetMagnification() const;

  virtual ImagePyramidPtr doGetPyramid() const;

 private:
  typedef TIFFParser::TIFFDirVector TIFFDirVector;

  typedef boost::shared_ptr<leica::LeicaSCNProperties> LeicaSCNPropertiesPtr;

  TIFFReader tif_;

  ImagePyramidPtr pyramid_;

  LeicaSCNPropertiesPtr properties_;

  std::vector<int> dirs_;
};

/// \relates LeicaImageParser
//
/// Converts a region to a bottom-right coordinates system given a top-left 
/// cooridnate system
//
/// \note The origin of Leica images is at the bottom-right, while most often
/// the display has an origin at the top-left. (conversion are required to 
/// maintain the appropriate oritentation.
//
/// \note Concider rotating the image by 180 degrees if displayed in a top-left
/// coordianate system
//
/// \a image_area 
//  the total area of the image
//
/// \a top_left_region
///  The region to be converted into the bottom-right origin coordinate system
Rect toLeicaOrigin(const Rect &image_area, const Rect &top_left_region);

}   // namespace image
}   // namespace sedeen

#endif

