#ifndef SEDEEN_SRC_IMAGE_APERIO_LEICAXMLPARSER_H
#define SEDEEN_SRC_IMAGE_APERIO_LEICAXMLPARSER_H

// System headers
#include <map>
#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>

namespace sedeen {

class Rect;

namespace image {
namespace leica {

class PrivateImageProperties;
class PrivateScanSettings;

typedef std::vector<std::string> StringVector;
typedef std::vector<int> IntVector;
typedef std::map<int, std::string> ChannelMap;

/// \relates LeicaSCNParser
/// A container class for Leica scan settings properties
class LeicaScanSettings {
 public:
  /// Image Types
  enum ImageType{
    Brightfield,
    Fluorescence,
  };

  /// Text properties
  enum FluorescenceProperty{
    Name = 0,
    ShortName,
    ExcitationFilter,
    DichromaticMirror,
    SuppressionFilter,
    MaterialNumber,
    Comment,
  };

  LeicaScanSettings();

  LeicaScanSettings(ImageType type, double magnification,
                    double numerical_aperture);

  /// Available for all image types
  ImageType type() const;

  /// Available for all image types
  double numericalAperture() const;

  /// Available for all image types
  double magnification() const;

  /// Number of fluorescece channels
  //
  /// Available only for type() == Fluorescence
  int channels() const;

  /// Per channel color
  //
  /// Available only for type() == Fluorescence
  int color(int channel_index) const;

  /// Per channel CCD gain
  //
  /// Available only for type() == Fluorescence
  int gain(int channel_index) const;

  /// Per channel exposure time
  //
  /// Available only for type() == Fluorescence
  int exposureTime(int channel_index) const;

  /// Per channel fluorescence cube properties
  //
  /// Available only for type() == Fluorescence
  const std::string& cubeProperties(int channel_index,
                                    FluorescenceProperty id) const;

  /// Per channel fluorescence filter properties
  //
  /// Available only for type() == Fluorescence
  const std::string& filterProperties(int channel_index,
                                      FluorescenceProperty id) const;

  /// Available only for type() == Fluorescence
  //
  /// Must set channels in order, i.e. set index 0 first, then 1, 2, and so on
  /// Existing index' will be overwritten
  void setChannel(int index,
                  const char* name,
                  int color,
                  int exposure_time,
                  int ccd_gain,
                  ChannelMap fluorescence_cube,
                  ChannelMap fluorescence_filter);

 private:
  boost::shared_ptr<PrivateScanSettings> settings_;
};

class LeicaImageProperties {
 public:
  LeicaImageProperties(const IntVector& directories,
                       const Rect &view_area,
                       const std::string &model,
                       const std::string &version,
                       const std::string &creation_date,
                       const LeicaScanSettings &settings);
  /// the bounding box of the image in slide coordinates
  const Rect &rect() const;
  
  /// the image name as recorded by the scanner
  const std::string &name() const;
  
  /// image creation date
  const std::string &date() const;
  
  /// device model
  const std::string &model() const;
  
  /// a complex version string which may also contain a date
  const std::string &version() const;

  /// A vector containing TIFF directories corresponding to image resolutions
  const IntVector& dirs() const;

  /// scan settings
  const LeicaScanSettings& settings() const;
 private:
  boost::shared_ptr<PrivateImageProperties> properties_;
};

/// \relates LeicaSCNParser
/// A container class for Leica image properties
class LeicaSCNProperties{
 public:
  LeicaSCNProperties(const LeicaImageProperties &main,
                     const LeicaImageProperties &macro);

  /// Returns the main image properties
  const LeicaImageProperties& main() const;

  /// Returns the macro image properties
  const LeicaImageProperties& macro() const;

 private:
  LeicaImageProperties main_;
  LeicaImageProperties macro_;
};

typedef boost::shared_ptr<LeicaSCNProperties> LeicaSCNPropertiesPtr;

/// Parses the XML description
//
/// \relates LeicaSCNParser
///
/// \return
/// The properties or empty pointer on error
///
/// Leica SCN format details
/// --------------------------
/// Leica slides contain a pyramidal main image and a pyramidal macro image.
///
/// The ImageDescription tag contains valid XML in the namespace
/// http://www.leica-microsystems.com/scn/2010/10/01.
///
/// The XML description contais <image> tags for the macro image and main image.
/// Typically the macro image has fewer levels (descibed by the <dimension> tag)
/// than the main image and may appear first the XML description. However, the
/// 'size' value in the <pixel> and largest <dimension> of the macro image
/// should match. The <dimension> tag also contains the TIFF directory of each
/// image.
///
/// Other properties include <objective>, <illuminationSource>, <objective>,
/// <offsetX>, <offsetY>, <spacingZ>, <device>, <creationDate>s
///
/// source: OpenSlide SCN descrition
/// http://openslide.org/formats/leica/
LeicaSCNPropertiesPtr parseLeicaXML(const std::string &xml_description);

} // namespace leica
} // namespace image
} // namespace sedeen

#endif
