// Primary header
#include "image/leica/LeicaSCNParser.h"

// Boost headers
#include <boost/shared_ptr.hpp>

#include <tiffio.h>
#include <CImg.h>

#include "image/io/Exceptions.h"
#include "image/buffer/RawImage.h"
#include "image/leica/LeicaXMLParser.h"
#include "image/tiff/TIFFReader.h"
#include "global/geometry/Point.h"
#include "global/geometry/Size.h"
#include "global/geometry/Rect.h"

namespace sedeen {
namespace image {
namespace {

const char* LEICA_PRODUCER = "Leica SCN";

/// Extract \a region from the macro image in \a properties
/// \a region must be valid and contained in macro image
RawImage extractFromMacro(const LeicaSCNParser &parser,
                          const leica::LeicaSCNProperties &properties,
                          const Rect &region) {
  // Get macro image
  RawImage macro_image = parser.macroImage();
  assert(!macro_image.isNull());

  // Get slide regions
  Rect slide_area = properties.macro().rect();
  assert(slide_area.contains(region));

  // Macro's origin is top-left - do not convert
  Rect region_area = region;

  // Map the lable area to macro image coordinates
  region_area = project(region_area, slide_area, macro_image.rect());

  // copy the region into image
  RawImage image(size(region_area), macro_image.color());
  image.copy(image.rect(), macro_image, region_area);

  // finally, rorate the label image 180 degrees because the origin is
  // on the bottom left
  image.flipVertical();
  image.flipHorizontal();

  return image;
}

} // namespace

LeicaSCNParser::LeicaSCNParser(const char *filename)
    : TIFFParser(),
      tif_(TIFFReader(filename)),
      pyramid_(),
      properties_(),
      dirs_() {
  if (0 == tif_.numDirectories()) {
    throw InvalidImage(filename, "File does not contain an image");
  }

  // ensure aperio image
  if (!parseTiffDescription(tif_))
    throw InvalidImage(filename,
                       "File does not appear to be in Leica SCN format.");

  // create image pyramid -
  // thrown InvalidImage exception on error
  try {
    pyramid_ = getTiffPyramid(doGetDirs(), tif_);
  }
  catch (InvalidPyramid &err) {
    throw InvalidImage(filename, err.what());
  }
}

LeicaSCNParser::~LeicaSCNParser() {
}

TIFFReader LeicaSCNParser::doGetReader() const {
  return tif_;
}

const LeicaSCNParser::TIFFDirVector& LeicaSCNParser::doGetDirs() const {
  return dirs_;
}

int LeicaSCNParser::doGetLabelDir() const {
  return doGetMacroDir();
}

int LeicaSCNParser::doGetMacroDir() const {
  assert(properties_->macro().dirs().size());
  return properties_->macro().dirs()[0];
}

int LeicaSCNParser::doGetThumbnailDir() const {
  return doGetMacroDir();
}

std::string LeicaSCNParser::doGetProducer() const {
  return LEICA_PRODUCER;
}

SizeF LeicaSCNParser::doGetPixelSize() const {
  return getPixelSizeFromTiff(reader(), properties_->main().dirs()[0]);
}

double LeicaSCNParser::doGetMagnification() const {
  return properties_->main().settings().magnification();
}

ImagePyramidPtr LeicaSCNParser::doGetPyramid() const {
  return pyramid_;
}

RawImage LeicaSCNParser::doGetThumbnailImage() const {
  /// Extract the thumbnail image from the marco image using the
  /// image bourdies provided in the hearder

  assert(hasThumbnailImage() && hasMacroImage());

  // Get slide regions
  Rect slide_area = properties_->macro().rect();
  Rect tissue_area = properties_->main().rect();

  // Assume the worst, return the macro image
  if (!slide_area.contains(tissue_area))
    return macroImage();
  else
    return extractFromMacro(*this, *properties_, tissue_area);
}

RawImage LeicaSCNParser::doGetLabelImage() const {
  // Extract label image from the macro image
  //  - the label shall be the bottom 1/3 of the slide area
  //  - this is actually inaccurate and will lead to label images that
  //    include portions of the slide area

  assert(hasLabelImage() && hasMacroImage());

  // Get slide regions
  Rect slide_area = properties_->macro().rect();

  // Get the approximate lable area
  Rect label_area = slide_area;
  int max_height = slide_area.height() / 3;
  label_area.setHeight(max_height);
  label_area.setY(slide_area.height() - max_height);

  // Assume the worst, return the macro image
  if (!slide_area.contains(label_area))
    return macroImage();
  else
    return extractFromMacro(*this, *properties_, label_area);
}

bool LeicaSCNParser::parseTiffDescription(TIFFReader reader) {
  // Leica stores meta tags in the desciption field as XML data
  std::string xml_description =
      reader.getTagString(0, TIFFTAG_IMAGEDESCRIPTION);
  if (xml_description.empty()) return false;

  // Get properties
  properties_ = leica::parseLeicaXML(xml_description);
  if (0 == properties_.get()) return false;

  // Fix directories for multi channel fluorescence image
  // Decoder can only cope with one image
  if (leica::LeicaScanSettings::Fluorescence ==
      properties_->main().settings().type()) {
    const int num_channels = properties_->main().settings().channels();
    const std::vector<int> &directories = properties_->main().dirs();

    for (unsigned int i = 0; i < directories.size(); i += num_channels) {
      dirs_.push_back(directories[i]);
    }
  } else {
    dirs_ = properties_->main().dirs();
  }

  // Success
  return true;
}


Rect toLeicaOrigin(const Rect &image_area, const Rect &top_left_region) {
  // Size of region should stay the same, only a new origin is needed
  const int originx = image_area.width() - top_left_region.width() -
                      top_left_region.x();
  const int originy = image_area.height() - top_left_region.height() -
                      top_left_region.y();

  return Rect(Point(originx, originy), size(top_left_region));
}

}   // namespace image
}   // namespace sedeen

