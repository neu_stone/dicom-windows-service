#include "image/leica/LeicaDecoder.h"

#include <exception>

#include "global/geometry/Point.h"
#include "global/geometry/Rect.h"
#include "image/io/DecodeRequest.h"
#include "image/leica/LeicaSCNParser.h"
#include "image/tiff/TIFFPyramid.h"
#include "image/tiff/TIFFReader.h"

namespace sedeen {
namespace image {

LeicaDecoder::LeicaDecoder(const std::string& filename, QObject *parent)
    : SelectiveDecoder(ImageParserPtr(new LeicaSCNParser(filename.c_str())),
                       parent),
      reader_(new TIFFReader(parser().filename().c_str())) {
}

LeicaDecoder::LeicaDecoder(boost::shared_ptr<LeicaSCNParser> parser,
                            QObject * parent)
    : SelectiveDecoder(parser, parent),
      reader_(new TIFFReader(parser->filename().c_str())) {
}

SelectiveDecoder::SelectiveDecoderPtr LeicaDecoder::clone() const {
  SelectiveDecoderPtr ptr;
  try {
    ptr.reset(new LeicaDecoder(filename(), parent()));
  }
  catch(const std::runtime_error&) {
  }
  return ptr;
}

LeicaDecoder::~LeicaDecoder() {
  /* Cleanup codec */
  waitForExit();
}

bool LeicaDecoder::canRead(const std::string &filename) {
  try {
    LeicaDecoder reader(filename.c_str());
    return true;
  }
  catch(...) {
    return false;
  }
}

RawImage LeicaDecoder::decodeRegion(const DecodeRequest &request) {
  const LeicaSCNParser* lParser = static_cast<const LeicaSCNParser*>(&parser());
  assert(lParser);

  // Get the requested region
  const boost::uint16_t tdir = lParser->dirs()[request.resolution()];
  Rect region = request.region(request.resolution());

  // The origin of Leica images is at the bottom-left, while the request
  // assumes a top-left origin.
  //
  // Must convert between the two systems by traslating region
  const Rect image_rect = lParser->pyramid()->rect(request.resolution());
  region = toLeicaOrigin(image_rect, region);
  assert(image_rect.contains(region));

  // Get the region
  RawImage img = reader_->getRegionForDisplay(tdir, region);

  // Rotate the image by 180 degrees
  img.flipVertical();
  img.flipHorizontal();

  return img;
}

} // namespace image
} // namespace sedeen
