// Primary header
#include "image/ImageRepository.h"

// System includes
#include <algorithm>
#include <cassert>
#include <numeric>
#include <vector>

// Boost includes
#include <boost/make_shared.hpp>
#include <boost/scoped_ptr.hpp>

// User includes
#include "global/memory/MemoryInfo.h"
#include "image/io/Exceptions.h"
#include "image/io/ImageAccessor.h"
#include "image/io/ImageData.h"
#include "image/io/NullImage.h"
#include "image/tiff/OpenBigTiff.h"
#include "image/j2k/OpenJ2k.h"
#include "image/standard/OpenStandardImage.h"

namespace sedeen {
namespace image {

const boost::uint64_t MaxBytesPerRawImage = 150 * 1024 * 1024; //150MB

namespace {

ImageOpener createImageOpener()
{
  // Load image read types
  typedef std::auto_ptr<ImageOpenHandler> HandlerPtr;

  ImageOpener imageOpener;
  imageOpener.addHandler(HandlerPtr(new OpenJ2k));
  imageOpener.addHandler(HandlerPtr(new OpenBigTiff));
  imageOpener.addHandler(HandlerPtr(new standard::OpenStandardImage));

  return imageOpener;
}

} // namespace

ImageRepositorySettings::ImageRepositorySettings(
    boost::uint64_t bufferCapacity,
    boost::uint64_t maxBufferCapacity)
    : m_bufferCapacity(bufferCapacity),
      m_maxBufferCapacity() {
  boost::scoped_ptr<MemoryInfo> memInfo(MemoryInfo::instance());
  std::min(static_cast<boost::uint64_t>(0x5FFFFFFF),
           std::min(maxBufferCapacity,
                    static_cast<boost::uint64_t>(memInfo->system()/2.0)));
}

bool ImageRepositorySettings::operator==(
    const ImageRepositorySettings& settings) const {
  return (this->m_bufferCapacity == settings.m_bufferCapacity &&
          this->m_maxBufferCapacity == settings.m_maxBufferCapacity);
}
bool ImageRepositorySettings::operator!=(
    const ImageRepositorySettings& settings) const {
  return !(*this==settings);
}

ImageRepository& ImageRepository::instance() {
  static ImageRepository repo = ImageRepository(createImageOpener());
  return repo;
}

ImageRepository::ImageRepository(const ImageOpener& imageOpener)
    : m_images(),
      m_opener(imageOpener),
      m_locker(QReadWriteLock::Recursive),
      m_nullImageData(nullImageData()),
      m_settings(){
  // Manually install the null case.
  m_images[""] = m_nullImageData;
}

ImageRepository::~ImageRepository() {}

void ImageRepository::setSettings(const ImageRepositorySettings& settings) {
  m_locker.lockForRead();
  if( settings != m_settings )
  {
    m_settings = settings;
    applySettings();
  }
  m_locker.unlock();
}

const ImageRepositorySettings& ImageRepository::settings() const {
  return m_settings;
}

void ImageRepository::applySettings() {
  m_locker.lockForRead();
  if( 0 == settings().m_bufferCapacity )
    setTotalBufferLimit( settings().m_maxBufferCapacity );
  else
    setTotalBufferLimit( settings().m_bufferCapacity );
  m_locker.unlock();
}

void ImageRepository::purge()
{
  m_locker.lockForRead();
  Map_t::iterator i = m_images.begin();
  while( i!=m_images.end() ) {
    if( i->second.expired() )
      m_images.erase(i++);
    else
      ++i;
  }
  m_locker.unlock();
}

ImageAccessor ImageRepository::get(const std::string& filename) {
  // Try to locate the image among the currently open entries
  boost::shared_ptr<ImageData> imageData(find(filename));

  // If it did not exist, create a new ImageData instance and map it.
  if (!imageData) imageData = create(filename);

  // Failure to handle the filename results in a thrown exception. If we get to
  // this point in the code, the 'image' ought to be non-null.
  Q_ASSERT(imageData);

  return ImageAccessor(imageData);
}

QString ImageRepository::filter() const {
  return m_opener.filterList();
}

boost::shared_ptr<ImageData> ImageRepository::find(
    const std::string& filename) {
  boost::shared_ptr<ImageData> id;

  Q_ASSERT(!id);

  m_locker.lockForRead();
  // Do not use map::operator[] - this may be a write operation.
  Map_t::const_iterator i = m_images.find(filename);
  if (m_images.end() != i) id = i->second.lock();
  m_locker.unlock();

  return id;
}

boost::shared_ptr<ImageData> ImageRepository::create(const std::string& filename) {
  // Perform a lock-and-double-check to avoid two threads creating the missing entry.

  // Use the openers to access the file
  // This function may throw. Place it outside the lock.
  boost::shared_ptr<ImageData> newImage(nullImageData());
  try {
    newImage = m_opener.open(filename);
  }
  catch(...)
  {
    return newImage;
  }

  // Ensure nobody else has created this entry
  m_locker.lockForWrite();

  // Find or create an entry for this filename
  boost::weak_ptr<ImageData>& entry = m_images[filename];

  // If the entry is empty, place the new image inside.
  if (entry.expired()) {
    entry = newImage;
  } else {
    newImage = entry.lock();
  }

  // Release the mutex
  m_locker.unlock();

  // apply settings
  applySettings();

  return newImage;
}

namespace {

class SetBufferCapacity
    : public std::unary_function<ImageRepository::Map_t::value_type, void> {
 public:
  SetBufferCapacity(boost::uint64_t capacity) : cap_(capacity) {
  }

  void operator()(ImageRepository::Map_t::value_type& i) const {
    boost::shared_ptr<ImageData> image = i.second.lock();
    if (image) image->setBufferCapacity(cap_);
  }

 private:
  boost::uint64_t cap_;
}; // class SetBufferCapacity

} // namespace

void ImageRepository::setTotalBufferLimit(boost::uint64_t bytes) {
  // check for value of zero
  if (0 == bytes) {
    bytes = MemoryInfo::instance()->available() / 2;
  }

  // lock mutex
  QReadLocker lock(&m_locker);

  // Set the buffer limits
  boost::uint64_t maximumImageSize = std::min(MaxBytesPerRawImage,
                                              imageBufferSize(bytes));

  std::for_each(m_images.begin(), m_images.end(),
                SetBufferCapacity(maximumImageSize));
}

namespace {

class GetBufferSize
    : public std::unary_function<ImageRepository::Map_t::value_type,
                                 boost::uint64_t> {
 public:
  boost::uint64_t operator()(
      const ImageRepository::Map_t::value_type& i) const {
    boost::shared_ptr<ImageData> image = i.second.lock();
    return image ? image->bufferSize() : 0;
  }
}; // class GetBufferSize

} // namespace

boost::uint64_t ImageRepository::getTotalBufferSize() const {
  QReadLocker lock(&m_locker);
  std::vector<boost::int64_t> bufferSizes;
  std::transform(m_images.begin(), m_images.end(),
                 std::back_inserter(bufferSizes), GetBufferSize());
  return std::accumulate(bufferSizes.begin(), bufferSizes.end(), 0);
}

namespace {

class GetFixedBufferSize
    : public std::unary_function<ImageRepository::Map_t::value_type,
                                 boost::uint64_t> {
 public:
  /// Return the buffer capacity if the image is valid and of fixed buffer size.
  //
  /// \return
  /// Buffer size if the image is valid and if the buffer capacity is fixed,
  /// otherwise 0.
  boost::uint64_t operator()(
      const ImageRepository::Map_t::value_type& i) const {
    boost::shared_ptr<ImageData> image = i.second.lock();
    return (image && image->isBufferCapacityFixed()) ?
        image->bufferCapacity() :
        0;
  }
}; // class GetFixedBufferSize

class IsFixedBuffer
    : public std::unary_function<ImageRepository::Map_t::value_type, bool> {
 public:
  bool operator()(const ImageRepository::Map_t::value_type& i) const {
    boost::shared_ptr<ImageData> image = i.second.lock();
    return image && image->isBufferCapacityFixed();
  }
}; // class IsFixedBuffer

} // namespace

boost::uint64_t ImageRepository::imageBufferSize(boost::uint64_t total_bytes) {
  assert(0 != total_bytes);

  // lock mutex
  QReadLocker lock(&m_locker);

  // Sum the ram used by fixed buffers
  std::vector<boost::uint64_t> fixed_buffer_sizes;
  std::transform(m_images.begin(), m_images.end(),
                 std::back_inserter(fixed_buffer_sizes), GetFixedBufferSize());
  boost::uint64_t sizeRamImages = std::accumulate(fixed_buffer_sizes.begin(),
                                                  fixed_buffer_sizes.end(), 0);

  // Get the number of tiled images
  int numTiledImages = std::count_if(m_images.begin(), m_images.end(),
                                     std::not1(IsFixedBuffer()));

  // Calclaute max size of each tiled image
  boost::uint64_t bytesPerTiledImage = 0;
  if (0 < numTiledImages) {
    boost::uint64_t minRequiredBytes = std::max(total_bytes, sizeRamImages);
    bytesPerTiledImage = (minRequiredBytes - sizeRamImages) / numTiledImages;
  }

  // return
  return bytesPerTiledImage;
}

} // namespace image
} // namespace sedeen
