#ifndef SEDEEN_SRC_IMAGE_BUFFER_RAWIMAGEDATA_H
#define SEDEEN_SRC_IMAGE_BUFFER_RAWIMAGEDATA_H

// User includes
#include "image/buffer/RawImageBuffer.h"

namespace sedeen {
namespace image {
/*!
  \class RawImageData
  \brief A buffer for a 3D array of elements. Buffer can be bound to an already exisiting c-style array.
*/
template<class T>
class RawImageData : public RawImageBuffer
{
 public:

  /// \copydoc RawImageBuffer
  RawImageData();

  /// Performs a deep copy
  //
  /// If image buffer cannot be allocated, the image will be set to \c null.
  /// Check state with isNull() for safety.
  RawImageData(const RawImageData<T>&);

  /// \brief Accepts a buffer \a data pointing to with \a width * \a height * \a components elements.
  /// \param data
  /// \param width
  /// \param height
  /// \param components
  /// \param order defines the way in which elements are arranged
  /// \param color space of data
  RawImageData(   RawDataPtr data,
                  int32_t width,
                  int32_t height,
                  int8_t components,
                  PixelOrder order,
                  Color color = Unknown);

  /// Create a image buffer with given properties
  //
  /// If image buffer cannot be allocated, the image will be set to \c null.
  /// Check state with isNull() for safety.
  //
  /// \param width
  ///
  /// \param height
  ///
  /// \param components
  ///
  /// \param order
  ///
  /// \param color
  RawImageData(   int32_t width,
                  int32_t height,
                  int8_t components,
                  PixelOrder order,
                  Color color = Unknown);
 protected:
  RawImageBufferPtr doGetClone() const;
  ConstRawDataPtr doGetData() const;
  int8_t doGetNumComponents() const;
  Color doGetColor() const;
  PixelOrder doGetOrder() const;
  uint64_t doGetCount() const;
  Size doGetSize() const;
  int32_t doGetWidth() const;
  int32_t doGetHeight() const;
  int32_t doGetMaxValue() const;
  int32_t doGetMinValue() const;
  bool doGetIsSigned() const;
  uint64_t doGetBytes() const;
  int32_t doGetBytesPerPixel() const;
  int32_t doGetBitsPerComponent() const;
  int32_t doGetBufferValue(uint64_t offset) const;
  void doSetBufferValue(uint64_t offset, int32_t value);
  void doFillBuffer(int32_t value);
  void doCopy(const Rect& destRegion, const RawImageBuffer& srcImage, const Rect& srcRegion);
  virtual void doFlipVertical();
  virtual void doFlipHorizontal();

 private:
  RawDataPtr buffer() const;
  void copyInterleaved(const Rect& destRegion, const RawImageBuffer& srcImage,
                  const Rect& srcRegion);
  void copyPlanar(const Rect& destRegion, const RawImageBuffer& srcImage,
            const Rect& srcRegion);

 private:
  uint64_t    _count;
  Size        _dims;
  int8_t      _components;
  PixelOrder  _order;
  Color       _color;
  RawDataPtr  _buffer;
};

}   // namespace sedeen
}   // namespace image

#include "RawImageData.txx"

#endif


