#ifndef SEDEEN_SRC_IMAGE_BUFFER_RAWIMAGE_H
#define SEDEEN_SRC_IMAGE_BUFFER_RAWIMAGE_H

// System includes
#include <boost/cstdint.hpp>

// User includes
#include "image/buffer/RawImageTypes.h"
#include "image/DllApi.h"

namespace sedeen {

class Rect;
class Size;

namespace image {

class RawImageBuffer;

/// A handle to a RawImageBuffer.
//
/// Provides value semantics for RawImageBufferPtr with copy-on-write
/// functionality.
class SEDEEN_IMAGE_API RawImage {
 public:
  /// Defines a NULL image. \sa isNull()
  RawImage();

  /// Copy constructor
  //
  /// If image buffer cannot be allocated, the image will be set to \c null.
  /// Check state with isNull() for safety.
  RawImage(const RawImage& image);

  /// Create an un-initialized image with the properties from \a other.
  //
  /// If image buffer cannot be allocated, the image will be set to \c null.
  /// Check state with isNull() for safety.
  //
  /// \param size image dimensions
  /// \param color image color
  RawImage(const Size& size, Color color);

  /// Create an un-initialized image with properties copied from \a other.
  //
  /// If image buffer cannot be allocated, the image will be set to \c null.
  /// Check state with isNull() for safety.
  //
  /// \param size image dimensions
  /// \param other reference image
  RawImage(const Size& size, const RawImage& other);

  /// Binds \a data to this instance.
  //
  /// \param data
  RawImage(RawImageBufferPtr data);

  /// Create an image with given \a size and \a color using values from \a data
  //
  /// \param data a buffer with at least enough elements to satisfy \a size
  /// and \a color
  /// \param size image dimensions
  /// \param color image color
  /// \param order pixel order
  RawImage(const RawDataPtr& data, const Size& size, Color color,
           PixelOrder order = Interleaved);

  /// Assignment operator
  RawImage& operator=(const RawImage& image);

  /// Equality operator
  bool operator==(const RawImage& image);

  /// Flips the image about its x-axis
  void flipVertical();

  /// Flips the image about its y-axis
  void flipHorizontal();

  /// \copydoc RawImageBuffer::at(int32_t,int32_t,int32_t) const
  int32_t at(int32_t x, int32_t y, int32_t c) const;

  /// \copydoc RawImageBuffer::at(uint64_t) const
  int32_t at(uint64_t index) const;

  /// \copydoc RawImageBuffer::operator[](uint64_t) const
  int32_t operator[](uint64_t index) const;

  /// \copydoc RawImageBuffer::fill(int32_t)
  void fill(int32_t value);

  /// \copydoc RawImageBuffer::setValue(int32_t,int32_t,int32_t,int32_t)
  void setValue(int32_t x, int32_t y, int32_t c, int32_t value);

  /// \copydoc RawImageBuffer::setValue(uint64_t,int32_t)
  void setValue(uint64_t index, int32_t value);

  /// \copydoc RawImageBuffer::isNull()
  bool isNull() const;

  /// \copydoc RawImageBuffer::copy(const Rect&,const RawImageBuffer&,const Rect&)
  void copy(const Rect& destination_region, const RawImage& source_image,
            const Rect& source_region);


  /// \copydoc RawImageBuffer::rect() const
  Rect rect() const;

  /// \copydoc RawImageBuffer::count() const
  uint64_t count() const;

  /// \copydoc RawImageBuffer::size() const
  Size size() const;

  /// \copydoc RawImageBuffer::width() const
  int32_t width() const;

  /// \copydoc RawImageBuffer::height() const
  int32_t height() const;

  /// \copydoc RawImageBuffer::components() const
  int8_t components() const;

  /// \copydoc RawImageBuffer::bytes() const
  uint64_t bytes() const;

  /// \copydoc RawImageBuffer::bytesPerPixel() const
  int32_t bytesPerPixel() const;

  /// \copydoc RawImageBuffer::bitsPerComponent() const
  int32_t bitsPerComponent() const;

  /// \copydoc RawImageBuffer::maxValue() const
  int32_t maxValue() const;

  /// \copydoc RawImageBuffer::minValue() const
  int32_t minValue() const;

  /// \copydoc RawImageBuffer::isSigned() const
  bool isSigned() const;

  /// \copydoc RawImageBuffer::color() const
  Color color() const;

  /// \copydoc RawImageBuffer::order() const
  PixelOrder order() const;

  /// \copydoc RawImageBuffer::contains() const
  bool contains(int32_t x, int32_t y, int32_t c) const;

  /// \copydoc RawImageBuffer::contains(uint64_t) const
  bool contains(uint64_t index) const;

  /// \copydoc RawImageBuffer::index(int32_t,int32_t,int32_t) const
  uint64_t index(int32_t x, int32_t y, int32_t c) const;

  /// \copydoc RawImageBuffer::data() const
  ConstRawDataPtr data() const;

 private:
  void detach();
  RawImageBufferPtr buffer() const;

 private:
  RawImageBufferPtr buffer_;
};

} // namespace image
} // namespace sedeen

#endif
