// Main include
#include "image/buffer/RawImageFactory.h"

// User includes
#include "global/Debug.h"
#include "global/geometry/Size.h"
#include "image/buffer/RawImageBuffer.h"
#include "image/buffer/RawImageData.h"
#include "image/buffer/RawImageTypes.h"

namespace sedeen {
namespace image {

namespace{

/// Create a RawImageBuffer from given parameters. if \a data is null, returns
/// a null buffer.
template<class T>
RawImageBufferPtr createRawImageBuffer( RawDataPtr data,
                                        int32_t width,
                                        int32_t height,
                                        int8_t components,
                                        PixelOrder order,
                                        Color color) {
  if (0<data.get())
    return RawImageBufferPtr(new RawImageData<T>(data,
                                                 width, height, components,
                                                 order, color) );
  else
    return createNullRawImageBuffer<T>();
}

/// Create a RawImageBuffer from give parameters
RawDataPtr createRawData(uint64_t num_elements) {
  uint8_t *ptr = 0;
  try { ptr = new uint8_t[num_elements]; }
  catch(...) { ptr = 0; };
  return RawDataPtr(ptr, DeleteNewArray<uint8_t>);
}
}   // namespace


template<typename T>
RawImageBufferPtr createNullRawImageBuffer() {
  return RawImageBufferPtr(new RawImageData<T>());
}

RawImageBufferPtr createRawImageBuffer(const Size &size,
                                       Color color) {
  uint64_t num_elements = static_cast<uint64_t>(bytesPerPixel(color)) *
                                                size.width()* size.height();
  RawDataPtr data = createRawData(num_elements);
  return createRawImageBuffer(data, size, color, Interleaved);

}

RawImageBufferPtr createRawImageBuffer(const RawDataPtr &data,
                                       const Size &size,
                                       Color color,
                                       PixelOrder order) {
  switch (color) {
    case Alpha:
    case Gray_8:
    case RGB_8:
    case ARGB_8:
      return createRawImageBuffer<uint8_t>(data,
                                           size.width(), size.height(),
                                           componentsPerPixel(color),
                                           order, color);
      break;
    case RGB_16:
    case ARGB_16:
    case Gray_16:
      return createRawImageBuffer<uint16_t>(data,
                                      size.width(), size.height(),
                                      componentsPerPixel(color),
                                      order, color);
      break;
    default: debug::assert_x(false, "Unsupported color type.", __FILE__, __LINE__);
      return RawImageBufferPtr();
      break;
  }

}

RawImageBufferPtr createIntegerRawImageBuffer(int32_t width,
                                              int32_t height,
                                              int8_t components,
                                              PixelOrder order,
                                              int8_t bitsUsed,
                                              bool is_signed,
                                              Color color) {
  uint64_t num_elements = static_cast<uint64_t>(components) *
                                                ((bitsUsed+7) / 8) *
                                                width * height;
  return createIntegerRawImageBuffer(createRawData(num_elements),
                                     width, height, components,
                                     order, bitsUsed, is_signed, color);
}

RawImageBufferPtr createIntegerRawImageBuffer(RawDataPtr data,
                                              int32_t width,
                                              int32_t height,
                                              int8_t components,
                                              PixelOrder order,
                                              int8_t bitsUsed,
                                              bool is_signed,
                                              Color color) {
  if (8 >= bitsUsed) {
    if( !is_signed )
      return createRawImageBuffer<uint8_t>(data,
                                           width, height, components,
                                           order, color);
    else
      return createRawImageBuffer<int8_t>(data,
                                          width, height, components,
                                          order, color);
  } else if (16 >= bitsUsed) {
    if( !is_signed )
    return createRawImageBuffer<uint16_t>(data,
                                          width, height, components,
                                          order, color);
    else
      return createRawImageBuffer<int16_t>(data,
                                          width, height, components,
                                          order, color);
  } else {
    debug::assert_x(false, "Unsupported data type.", __FILE__, __LINE__);
    return RawImageBufferPtr();
  }
}

}   // namespace image
}   // namespace sedeen


