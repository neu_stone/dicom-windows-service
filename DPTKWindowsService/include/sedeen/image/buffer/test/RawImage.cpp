
#include "gtest/gtest.h"

#include "global/geometry/Point.h"
#include "global/geometry/Rect.h"
#include "global/geometry/Size.h"
#include "image/buffer/RawImage.h"
#include "image/buffer/RawImageFactory.h"
#include "image/buffer/RawImageTypes.h"

namespace sedeen {
namespace image {
namespace {

// Test image properties
const Size image_size(50,50);
const bool image_signed = false;
const int32_t image_value = 100;

// Test class Parameters
class TestParameters {
 public:
  TestParameters(Color color1, PixelOrder order1, int32_t components1,
                  int32_t bitsPerComponent1)
    : order(order1),
      color(color1),
      components(components1),
      bitsPerComponent(bitsPerComponent1) {
  }
  PixelOrder order;
  Color color;
  int32_t components;
  int32_t bitsPerComponent;
};

/// Performs a copy and returns true if copy is validated successfuly.
//
/// Fills source with 0 and copies into destination as specified and checks
/// that the values are copied correclty
//
/// \return
/// TRUE is return if successful
bool CopyAndCheck(RawImage &dest, const Rect &dest_area,
                  RawImage &source, const Rect &source_area) {
  // copy into destination
  source.fill(0);
  dest.copy(dest_area, source, source_area);

  // Get the destination area
  Rect common = intersection(dest_area, dest.rect());

  // Test Copy
  for (int y = common.y(); y <= yMax(common); ++y)
    for (int x = common.x(); x <= xMax(common); ++x)
      for (int c = 0; c < dest.components(); ++c)
        if (dest.at(x,y,c) != 0)
          return false;

  return true;
}


} // namespace


//
/// RawImage test fixture class
//
class RawImageTest : public ::testing::TestWithParam<TestParameters> {
 public:
  RawImageTest()
      : image() {
    // Make a palnar buffer
    RawImageBufferPtr rawbuffer = createIntegerRawImageBuffer(
                                                    image_size.width(),
                                                    image_size.height(),
                                                    GetParam().components,
                                                    GetParam().order,
                                                    GetParam().bitsPerComponent,
                                                    image_signed,
                                                    GetParam().color);

    // Make a planar image
    image = RawImage(rawbuffer);
    image.fill(image_value);
  }

 protected:
   RawImage image;
}; // class RawImageTest


//
// Instantiate Tests with parameters
//
INSTANTIATE_TEST_CASE_P(VariousRawImages, RawImageTest,
                ::testing::Values(TestParameters(RGB_8, Interleaved, 3, 8),
                                  TestParameters(RGB_8, Planar, 3, 8),
                                  TestParameters(RGB_16, Interleaved, 3, 16),

                                  TestParameters(ARGB_8, Interleaved, 4, 8),
                                  TestParameters(ARGB_16, Interleaved, 4, 16),

                                  TestParameters(Gray_8, Interleaved, 1, 8),
                                  TestParameters(Gray_16, Interleaved, 1, 16),

                                  TestParameters(Unknown, Interleaved, 1, 8),
                                  TestParameters(Unknown, Interleaved, 4, 8),
                                  TestParameters(Unknown, Interleaved, 4, 8),

                                  TestParameters(Alpha, Interleaved, 1, 8)
                                 ));


//
// Define tests
//
TEST_P(RawImageTest, ConstructorDefault) {
  RawImage img;
  EXPECT_TRUE(img.isNull());
}

TEST_P(RawImageTest, ConstructorCopy) {
  RawImage img(image);

  EXPECT_FALSE(img.isNull());
  EXPECT_EQ(img.color(), image.color());
  EXPECT_EQ(img.order(), image.order());
  EXPECT_EQ(img.components(), image.components());
  EXPECT_EQ(img.size(), image.size());
  EXPECT_EQ(img.data().get(), image.data().get());
}

TEST_P(RawImageTest, Assignment) {
  RawImage img = image;

  EXPECT_FALSE(img.isNull());
  EXPECT_EQ(img.color(), image.color());
  EXPECT_EQ(img.order(), image.order());
  EXPECT_EQ(img.components(), image.components());
  EXPECT_EQ(img.size(), image.size());
  EXPECT_EQ(img.data().get(), image.data().get());
}

TEST_P(RawImageTest, Equality) {
  RawImage img = image;
  EXPECT_TRUE(img == image);
}

TEST_P(RawImageTest, ConstructorLikeOther) {
  RawImage img(image.size(), image);

  EXPECT_FALSE(img.isNull());
  EXPECT_EQ(img.color(), image.color());
  EXPECT_EQ(img.order(), image.order());
  EXPECT_EQ(img.components(), image.components());
  EXPECT_EQ(img.size(), image.size());
  EXPECT_NE(img.data().get(), image.data().get());
}

TEST_P(RawImageTest, ConstructorColor) {
  RawImage img = RawImage(image.size(), Gray_8);

  EXPECT_FALSE(img.isNull());
  EXPECT_EQ(img.color(), Gray_8);
  EXPECT_EQ(img.components(), 1);
  EXPECT_EQ(img.size(), image.size());
}

TEST_P(RawImageTest, Copy) {
  // Create source image
  Size source_size(3,3);
  Rect source_area(Point(0,0), source_size);
  RawImage source_image(source_size, image);

  // Copy into destination
  Rect dest_area(Point(-1,-1), source_size);
  EXPECT_TRUE(CopyAndCheck(image, dest_area, source_image, source_area));

  // Copy into destination
  dest_area = Rect(Point(10,10), source_size);
  EXPECT_TRUE(CopyAndCheck(image, dest_area, source_image, source_area));

  // Copy into destination
  dest_area = Rect(Point(49, 49), source_size);
  EXPECT_TRUE(CopyAndCheck(image, dest_area, source_image, source_area));
}

TEST_P(RawImageTest, FlipHorizonal) {
  RawImage img(Size(3,3), image);

  // Make a horizontal gradient
  for (int y = 0; y < img.height(); ++y)
    for (int x = 0; x < img.width(); ++x)
      for (int c = 0; c < img.components(); ++c)
        img.setValue(x,y,c, y+x+c);

  img.flipHorizontal();

  // TEst image
  for (int y = 0; y < img.height(); ++y)
    for (int x = 0, xflipped = img.width()-1; x < img.width(); ++x, --xflipped)
      for (int c = 0; c < img.components(); ++c)
        EXPECT_EQ(img.at(x,y,c), xflipped +y + c);
}

TEST_P(RawImageTest, FlipVertical) {
  RawImage img(Size(3,3), image);

  // Make a horizontal gradient
  for (int y = 0; y < img.height(); ++y)
    for (int x = 0; x < img.width(); ++x)
      for (int c = 0; c < img.components(); ++c)
        img.setValue(x,y,c, y+x+c);

  img.flipVertical();

  // TEst image
  for (int y = 0, yflipped = img.height()-1; y < img.height(); ++y, --yflipped)
    for (int x = 0; x < img.width(); ++x)
      for (int c = 0; c < img.components(); ++c)
        EXPECT_EQ(img.at(x,y,c), x + yflipped + c);
}

TEST_P(RawImageTest, AtXyz) {
  for (int32_t c = 0; c < GetParam().components; ++c) {

    EXPECT_EQ(image.at(0, 0, c), image_value);

    int32_t x = image_size.width() / 2;
    int32_t y = image_size.height() / 2;
    EXPECT_EQ(image.at(x, y, 0), image_value);

    x = image_size.width() - 1;
    y = image_size.height() - 1;
    EXPECT_EQ(image.at(x, y, 0), image_value);
  }
}

//TEST_P(RawImageTest, AtXyzDeathTest) {
//  //::testing::FLAGS_gtest_death_test_style = "threadsafe";
//
//  int32_t x = image_size.width();
//  int32_t y = image_size.height();
//  EXPECT_DEATH(image.at(x, y, 0), ".*Warning.*");
//}

TEST_P(RawImageTest, AtIndex) {
  for (int32_t c = 0; c < GetParam().components; ++c) {

    int32_t component_offset = image_size.width() * image_size.height() * c;

    EXPECT_EQ(image.at(component_offset), image_value);

    int32_t index =
      (image_size.width() * image_size.height() / 2) + component_offset;
    EXPECT_EQ(image.at(index), image_value);

    index = (image_size.width() * image_size.height() - 1) + component_offset;
    EXPECT_EQ(image.at(index), image_value);
  }
}

//TEST_P(RawImageTest, AtIndexDeathTest) {
//  ::testing::FLAGS_gtest_death_test_style = "threadsafe";
//
//  uint64_t index = image_size.width() * image_size.height() * GetParam().components;
//  EXPECT_DEATH(image.at(index), ".*Warning.*");
//}

TEST_P(RawImageTest, OperatorArray) {
  for (int32_t c = 0; c < GetParam().components; ++c) {

    int32_t component_offset = image_size.width() * image_size.height() * c;

    EXPECT_EQ(image.at(component_offset), image_value);

    int32_t index = image_size.width() *
                    image_size.height() / 2 +
                    component_offset;
    EXPECT_EQ(image.at(index), image_value);

    index = (image_size.width() * image_size.height() - 1) + component_offset;
    EXPECT_EQ(image.at(index), image_value);

    index = (image_size.width() * image_size.height()) + component_offset;
  }
}

TEST_P(RawImageTest, IndexInterleaved) {
  if (GetParam().order != Interleaved)
    return;

  for (int32_t c = 0; c < GetParam().components; ++c) {

    EXPECT_EQ(image.index(0,0,c), static_cast<uint64_t>(c));

    int32_t x = image_size.width() - 1;
    int32_t y = 0;
    int32_t index = x * GetParam().components + c;
    EXPECT_EQ(image.index(x, y, c), static_cast<uint64_t>(index));

    x = 0;
    y = image_size.height() - 1;
    index = image_size.width() * (image_size.height()-1) * GetParam().components + c;
    EXPECT_EQ(image.index(x, y, c), static_cast<uint64_t>(index));
  }
}

TEST_P(RawImageTest, IndexPlanar) {
  if (GetParam().order != Planar)
    return;

  // TEst index'
  for (int32_t c = 0; c < GetParam().components; ++c) {

    int32_t component_offset = image_size.width() * image_size.height() * c;

    EXPECT_EQ(image.index(0,0,c), static_cast<uint64_t>(component_offset));

    int32_t x = image_size.width() - 1;
    int32_t y = 0;
    int32_t index = y * image_size.width() + x + component_offset;
    EXPECT_EQ(image.index(x, y, c), static_cast<uint64_t>(index));

    x = 0;
    y = image_size.height() - 1;
    index = y * image_size.width() + x + component_offset;
    EXPECT_EQ(image.index(x, y, c), static_cast<uint64_t>(index));
  }
}

//TEST_P(RawImageTest, IndexDeathTest) {
//  EXPECT_DEATH(image.index(image_size.width() ,image_size.height(),
//                            GetParam().components), ".*Warning.*");
//}

TEST_P(RawImageTest, Data) {
  EXPECT_NE(image.data().get(), static_cast<void*>(0));
}

TEST_P(RawImageTest, Fill) {
  image.fill(10);

  EXPECT_EQ(image.at(0), 10);

  int32_t last_index = image.count() - 1;
  EXPECT_EQ(image.at(last_index), 10);
}

TEST_P(RawImageTest, SetValueXyx) {
  for (int32_t c = 0; c < GetParam().components; ++c) {

    image.setValue(0, 0, c, 10);
    EXPECT_EQ(image.at(0, 0, c), 10);

    image.setValue(image_size.width() - 1 , image_size.height() - 1, c, 10);
    EXPECT_EQ(image.at(image_size.width() - 1, image_size.height() - 1, c), 10);
  }
}

//TEST_P(RawImageTest, SetValueXyxDeathTest) {
//  EXPECT_DEATH(image.setValue(image_size.width() ,image_size.height(),
//                            GetParam().components, 10), ".*Warning.*");
//}

TEST_P(RawImageTest, SetValueIndex) {
  for (int32_t c = 0; c < GetParam().components; ++c) {

    int32_t component_offset = image_size.width() * image_size.height() * c;
    int32_t index = image_size.width() *
                    image_size.height() / 2 +
                    component_offset;

    image.setValue(index, 10);
    EXPECT_EQ(image.at(index), 10);

    index = (image_size.width() * image_size.height() - 1) + component_offset;
    image.setValue(index, 10);
    EXPECT_EQ(image.at(index), 10);
  }
}

//TEST_P(RawImageTest, SetValueIndexDeathTest) {
//  EXPECT_DEATH(image.setValue(image_size.width() ,image_size.height(),
//                            GetParam().components, 10), ".*Warning.*");
//}

TEST_P(RawImageTest, IsNull) {
  EXPECT_FALSE(image.isNull());

  RawImage img;
  EXPECT_TRUE(img.isNull());
}

TEST_P(RawImageTest, Rect) {
  EXPECT_EQ(image.rect(), Rect(Point(0,0), image_size));
}

TEST_P(RawImageTest, Count) {
  EXPECT_EQ(image.count(), static_cast<uint64_t>(image_size.width() *
                                                 image_size.height() *
                                                 GetParam().components));
}

TEST_P(RawImageTest, Size) {
  EXPECT_EQ(image.size(), image_size);
}

TEST_P(RawImageTest, Width) {
  EXPECT_EQ(image.width(), image_size.width());
}

TEST_P(RawImageTest, Height) {
  EXPECT_EQ(image.height(), image_size.height());
}

TEST_P(RawImageTest, Components) {
  EXPECT_EQ(image.components(), GetParam().components);
}

TEST_P(RawImageTest, Bytes) {
  int32_t bytes_per_component = GetParam().bitsPerComponent >> 3;
  EXPECT_EQ(image.bytes(), static_cast<uint64_t>(image_size.width() *
                                                 image_size.height() *
                                                 GetParam().components *
                                                 bytes_per_component));
}

TEST_P(RawImageTest, BytesPerPixel) {
  int32_t bytes_per_component = GetParam().bitsPerComponent >> 3;
  EXPECT_EQ(image.bytesPerPixel(), GetParam().components * bytes_per_component);
}

TEST_P(RawImageTest, BitsPerComponent) {
  EXPECT_EQ(image.bitsPerComponent(), GetParam().bitsPerComponent);
}

TEST_P(RawImageTest, MaxValue) {
  int32_t max_value = (1 << GetParam().bitsPerComponent) - 1;
  EXPECT_EQ(image.maxValue(), max_value);
}

TEST_P(RawImageTest, MinValue) {
  EXPECT_EQ(image.minValue(), 0);
}

TEST_P(RawImageTest, IsSigned) {
  EXPECT_EQ(image.isSigned(), image_signed);
}

TEST_P(RawImageTest, ContainsXYZ) {
  EXPECT_TRUE(image.contains(0,0,0));

  EXPECT_TRUE(image.contains(image_size.width() - 1,
                            image_size.height() - 1,
                            GetParam().components - 1));

  EXPECT_FALSE(image.contains(image_size.width(),
                            image_size.height(),
                            GetParam().components));
}

TEST_P(RawImageTest, ContainsIndex) {
  EXPECT_TRUE(image.contains(0));

  EXPECT_TRUE(image.contains(image_size.width() *
                            image_size.height() *
                            GetParam().components - 1));

  EXPECT_FALSE(image.contains(image_size.width() *
                              image_size.height() *
                              GetParam().components));
}

TEST_P(RawImageTest, IndexXYZ) {
  EXPECT_EQ(image.index(0,0,0), 0u);

  EXPECT_EQ(image.index(image_size.width() - 1,
                       image_size.height() - 1,
                       GetParam().components - 1),

            static_cast<uint64_t>(image_size.width() *
                                  image_size.height() *
                                  GetParam().components - 1));
}

TEST(RawImage, InterleavedCopyTest) {
  // Test image: 10x10
  const auto WIDTH = 10;
  const auto HEIGHT = 10;

  auto image = RawImage(Size(WIDTH, HEIGHT), RGB_8);

  // The first channel is the x-coordinate, the second channel is the
  // y-coordinate, the third channel is the pixel's raster index.
  for (int y = 0; HEIGHT != y; ++y) {
    for (int x = 0; WIDTH != x; ++x) {
      image.setValue(x, y, 0, x);
      image.setValue(x, y, 1, y);
      image.setValue(x, y, 2, x + y * WIDTH);
    }
  }

  // Make a copy of the inner 8x8 region
  auto image_copy = RawImage(Size(WIDTH-2, HEIGHT-2), image);
  image_copy.copy(image_copy.rect(), image, Rect(Point(1, 1), Size(WIDTH-2,
                                                                   HEIGHT-2)));
  // Test the resultant values
  for (int y = 0; HEIGHT-2 != y; ++y) {
    for (int x = 0; WIDTH-2 != x; ++x) {
      EXPECT_EQ(x+1, image_copy.at(x, y, 0));
      EXPECT_EQ(y+1, image_copy.at(x, y, 1));
      EXPECT_EQ(x+1 + (y+1)*WIDTH, image_copy.at(x, y, 2));
    }
  }
}

} // namespace image
} // namespace sedeen
