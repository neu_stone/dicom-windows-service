#ifndef SEDEEN_SRC_IMAGE_BUFFER_RAWIMAGEFACTORY_H
#define SEDEEN_SRC_IMAGE_BUFFER_RAWIMAGEFACTORY_H

// User includes
#include "image/buffer/RawImageBuffer.h"
#include "image/buffer/RawImage.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {

/// \relates RawImage
  /// Creates a NULL buffer
  ///
  /// Note that some of the buffer's properties depend on the template type.
  template<typename T>
  RawImageBufferPtr createNullRawImageBuffer();

  /// \relates RawImageBuffer
  //
  /// Creates an unintialized 2D RawImageBuffer for the given color.
  //
  /// \param size dimensions of buffer
  /// \param color the buffer format
SEDEEN_IMAGE_API
RawImageBufferPtr createRawImageBuffer(const Size &size, Color color);

/// \relates RawImageBuffer
  //
  /// Creates a 2D RawImageBuffer bound to \a data.
  //
  /// Buffer is templated over the appropriate type based on \a color.
  /// \param data must be a valid pointer allocated to the correct size:
  /// \code size.width() * size.height() * bytesPerPixel(color) \endcode
/// \param size
/// \param color
  /// \param order the pixel order of the buffer
SEDEEN_IMAGE_API
RawImageBufferPtr createRawImageBuffer(const RawDataPtr &data, const Size &size,
                                       Color color,
                                       PixelOrder order = Interleaved);

  /// \relates RawImageBuffer
  //
  /// Creates an integral raw buffer bound to \a data.
  //
  /// Buffer is templated over either char, short, int or their unsigned
  /// versions depdening on \a bitsUsed  and \a is_signed.
  /// \param data must be a valid pointer allocated to the correct size:
  /// \code components * width * height * std::ceil(bitsUsed/8) \endcode
  /// \param width buffer width
  /// \param height buffer height
  /// \param components buffer components per pixel
  /// \param order buffer pixel order
  /// \param bitsUsed number of bits per component [1 16]
  /// \param is_signed TRUE if data elements are signed, false otherwise
  /// \param color effectively ignored and used for reference purposes only
SEDEEN_IMAGE_API
RawImageBufferPtr createIntegerRawImageBuffer(RawDataPtr data, int32_t width,
                                              int32_t height, int8_t components,
                                              PixelOrder order, int8_t bitsUsed,
                                              bool is_signed,
                                              Color color = Unknown);

  /// \relates RawImageBuffer
  /// Create an uninitialized RawImageBuffer.
  //
  /// Buffer is templated over either char, short, int or their unsigned
  /// versions depending on the specified \a bitsUsed  and \a is_signed.
  /// \param width buffer width
  /// \param height buffer height
  /// \param components buffer components per pixel
  /// \param order buffer pixel order
  /// \param bitsUsed number of bits per component [1 16]
  /// \param is_signed TRUE if data elements are signed, false otherwise
  /// \param color effectively ignored and used for reference purposes only
SEDEEN_IMAGE_API
RawImageBufferPtr createIntegerRawImageBuffer(int32_t width, int32_t height,
                                              int8_t components,
                                              PixelOrder order, int8_t bitsUsed,
                                              bool is_signed,
                                              Color color = Unknown);

}   // namespace sedeen
}   // namespace image

#endif



