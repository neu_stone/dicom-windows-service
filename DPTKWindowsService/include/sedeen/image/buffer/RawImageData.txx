// Main include
#include "image/buffer/RawImageData.h"

// System includes
#include <iostream>
#include <limits>
#include <CImg.h>
#include <boost/make_shared.hpp>

#include "global/Debug.h"

namespace sedeen {
namespace image {

namespace {
    /// custom deleter function for shared_ptr
    template<class T>
    void DeleteNewArray(void* buffer)
    { delete [] static_cast<T*>(buffer);
    }

    /// allocator function
    template<class T>
    RawDataPtr allocBuffer(uint64_t size)
    {
        T* ptr = 0;
        try { ptr = new T[size]; }
        catch(std::bad_alloc &) { ptr = 0; }
        return RawDataPtr(ptr, DeleteNewArray<T>);
    }
}

template<class T>
RawImageData<T>::RawImageData(  RawDataPtr data,
                                int32_t width,
                                int32_t height,
                                int8_t components,
                                PixelOrder order,
                                Color color) :
RawImageBuffer(),
_count(width*height*components),
_dims(width,height),
_components(components),
_order(order),
_color(color),
_buffer(data) {
  debug::assert_x(!((0<data.get())^(0<width && 0<height && 0<components)),
                  "Invalid Size.", __FILE__, __LINE__);
}

template<class T>
RawImageData<T>::RawImageData(  int32_t width,
                                int32_t height,
                                int8_t components,
                                PixelOrder order,
                                Color color) :
RawImageBuffer(),
_count(width*height*components),
_dims(width,height),
_components(components),
_order(order),
_color(color),
_buffer(allocBuffer<T>(_count))
{
  // protect again out of memory error
  if (0 == _buffer.get()) {
    *this = RawImageData<T>();
  }
}

template<class T>
RawImageData<T>::RawImageData(const RawImageData<T>& other) :
RawImageBuffer(),
_count(other.count()),
_dims(other.width(),other.height()),
_components(other.components()),
_order(other.order()),
_color(other.color()),
_buffer(allocBuffer<T>(_count))
{
  // protect again out of memory error
  if (0 == _buffer.get()) {
    *this = RawImageData<T>();
  }
  else {
    char* dest= static_cast<char*>(_buffer.get());
    const char* src = static_cast<const char*>(other._buffer.get());
    std::copy(src, src+bytes(), dest);
  }
}

template<class T>
RawImageData<T>::RawImageData():
RawImageBuffer(),
_count(0),
_dims(),
_components(0),
_order(Interleaved),
_color(Unknown),
_buffer()
{}

template<class T>
int8_t RawImageData<T>::doGetNumComponents() const
{ return _components;
}

template<class T>
Color RawImageData<T>::doGetColor() const
{ return _color;
}

template<class T>
PixelOrder RawImageData<T>::doGetOrder() const
{ return _order;
}

template<class T>
uint64_t RawImageData<T>::doGetCount() const
{ return _count;
}

template<class T>
Size RawImageData<T>::doGetSize() const
{ return _dims;
}

template<class T>
int32_t RawImageData<T>::doGetWidth() const
{ return _dims.width();
}

template<class T>
int32_t RawImageData<T>::doGetHeight() const
{ return _dims.height();
}

template<class T>
bool RawImageData<T>::doGetIsSigned() const
{ return std::numeric_limits<T>::is_signed;
}

template<class T>
int32_t RawImageData<T>::doGetMaxValue() const
{ return std::numeric_limits<T>::max();
}

template<class T>
int32_t RawImageData<T>::doGetMinValue() const
{ return std::numeric_limits<T>::min();
}

template<class T>
int32_t RawImageData<T>::doGetBitsPerComponent() const
{ return sizeof(T) * 8;
}

template<class T>
uint64_t RawImageData<T>::doGetBytes() const
{ return count() * sizeof(T);
}

template<class T>
int32_t RawImageData<T>::doGetBytesPerPixel() const
{ return doGetNumComponents() * sizeof(T);
}

template<class T>
ConstRawDataPtr RawImageData<T>::doGetData() const
{ return _buffer;
}

template<class T>
RawDataPtr RawImageData<T>::buffer() const
{ return _buffer;
}

template<class T>
int32_t RawImageData<T>::doGetBufferValue(uint64_t offset) const
{  const T* ptr = static_cast<T*>(_buffer.get());
   return static_cast<int32_t>(ptr[offset]);
}

template<class T>
void RawImageData<T>::doSetBufferValue(uint64_t offset, int32_t value)
{ T* ptr = static_cast<T*>(_buffer.get());
  ptr[offset] = static_cast<T>(value);

  debug::warning_x(value <= doGetMaxValue(),
                "Value exceeds the component's capacity and will be truncated",
                  __FUNCTION__, __LINE__);
  debug::warning_x(value >= doGetMinValue(),
                "Value is below the component's capacity and will be truncated",
                  __FUNCTION__, __LINE__);
}

template<class T>
void RawImageData<T>::doFillBuffer(int32_t value)
{ const uint64_t size = uint64_t(height()) * width() * components();
  T* ptr = static_cast<T*>(_buffer.get());
  for( uint64_t i= 0; i < size; i++)
      ptr[i] = static_cast<T>(value);

  debug::warning_x(value <= doGetMaxValue(),
                "Value exceeds the component's capacity and will be truncated",
                  __FUNCTION__, __LINE__);
  debug::warning_x(value >= doGetMinValue(),
                "Value is below the component's capacity and will be truncated",
                  __FUNCTION__, __LINE__);
}

template<class T>
RawImageBufferPtr RawImageData<T>:: doGetClone() const
{ return boost::make_shared<RawImageData<T> >(*this);
}

template<class T>
void RawImageData<T>::doCopy(const Rect& destRegion,
                             const RawImageBuffer& srcImage,
                             const Rect& srcRegion) {
  switch (order()) {
  case Interleaved:
    assert(srcImage.order() == Interleaved);
    copyInterleaved(destRegion, srcImage, srcRegion);
    break;

  case Planar:
    assert(srcImage.order() == Planar);
    copyPlanar(destRegion, srcImage, srcRegion);
    break;

  default:
    assert(false);
    break;
  }
}

template<class T>
void RawImageData<T>::copyInterleaved(const Rect& destRegion,
                                      const RawImageBuffer& srcImage,
                                      const Rect& srcRegion) {

    typedef cimg_library::CImg<T> CImgType;

    // bind buffer to CImg objects
    CImgType dest(static_cast<const T*>(_buffer.get()),
                  components(),
                  width(),
                  height(),
                  1,
                  true);

    CImgType src(static_cast<const T*>(srcImage.data().get()),
                 srcImage.components(),
                 srcImage.width(),
                 srcImage.height(),
                 1,
                 true);


    CImgType target;
    try {
        // Crop the source region - producing an independent copy
        target = src.get_crop(0,
                              srcRegion.x(),
                              srcRegion.y(),
                              0,
                              srcImage.components() - 1,
                              xMax(srcRegion),
                              yMax(srcRegion),
                              0);

        // resize to required dimensions
        target.resize(components(),
                      destRegion.width(),
                      destRegion.height(),
                      1);
    }
    catch (cimg_library::CImgException &e) {
      // out of memory - fill image with 0
      dest = static_cast<T>(0);
      debug::warning(e.what(), __FUNCTION__, __LINE__);
      return;
    }

    // Draw image onto destination
    dest.draw_image(0,
                    destRegion.x(),
                    destRegion.y(),
                    0,
                    target);
}

template<class T>
void RawImageData<T>::copyPlanar(const Rect& destRegion,
                                 const RawImageBuffer& srcImage,
                                 const Rect& srcRegion) {

    typedef cimg_library::CImg<T> CImgType;

    // bind buffer to CImg objects
    CImgType dest(static_cast<const T*>(_buffer.get()),
                  width(),
                  height(),
                  1,
                  components(),
                  true);

    CImgType src(static_cast<const T*>(srcImage.data().get()),
                 srcImage.width(),
                 srcImage.height(),
                 1,
                 srcImage.components(),
                 true);


    CImgType target;
    try {
        // Crop the source region - producing an independent copy
        target = src.get_crop(srcRegion.x(),
                              srcRegion.y(),
                              0,
                              0,
                              xMax(srcRegion),
                              yMax(srcRegion),
                              0,
                              srcImage.components() - 1);

        // resize to required dimensions
        target.resize(destRegion.width(),
                      destRegion.height(),
                      1,
                      components());
    }
    catch (cimg_library::CImgException &e) {
      // out of memory - fill image with 0
      dest = static_cast<T>(0);
      debug::warning(e.what(), __FUNCTION__, __LINE__);
      return;
    }

    // Draw image onto destination
    dest.draw_image(destRegion.x(),
                    destRegion.y(),
                    0,
                    0,
                    target);
}

//template<class T>
//void copyFromImage(ImageBuffer& destImage, const Rect& destRegion, const ImageBuffer& srcImage, const Rect& srcRegion)
//{
//    QImage dest( static_cast<uchar*>(destImage._buffer.get()),
//                 destImage.width(),
//                 destImage.height(),
//                 destImage.width() * 3,
//                 QImage::Format_RGB888 );
//
//    QImage src( static_cast<const uchar*>(srcImage._buffer.get()),
//                 srcImage.width(),
//                 srcImage.height(),
//                 srcImage.width() * 3,
//                 QImage::Format_RGB888 );
//
//    QPainter painter(&dest);
//    painter.dImageBuffer( QRect(destRegion.x(),
//                             destRegion.y(),
//                             destRegion.width(),
//                             destRegion.height()),
//                       src,
//                      QRect(srcRegion.x(),
//                             srcRegion.y(),
//                             srcRegion.width(),
//                             srcRegion.height())
//                             );
//}

template<class T>
void RawImageData<T>::doFlipVertical() {
  switch (order()) {
  case Interleaved:
    {
      cimg_library::CImg<T> cimg(static_cast<const T*>(data().get()),
                                  components(), width(), height(), 1, true);
      cimg.mirror('z');
    }
    break;

  case Planar:
    {
      cimg_library::CImg<T> cimg(static_cast<const T*>(data().get()),
                                 width(), height(), 1, components(), true);
      cimg.mirror('y');
    }
    break;

  default:
    assert(false);
    break;
  }
}

template<class T>
void RawImageData<T>::doFlipHorizontal() {
  switch (order()) {
  case Interleaved:
    {
      cimg_library::CImg<T> cimg(static_cast<const T*>(data().get()),
                                  components(), width(), height(), 1, true);
      cimg.mirror('y');
    }
    break;

  case Planar:
    {
      cimg_library::CImg<T> cimg(static_cast<const T*>(data().get()),
                                 width(), height(), 1, components(), true);
      cimg.mirror('x');
    }
    break;

  default:
    assert(false);
    break;
  }
}

}   // namespace sedeen
}   // namespace image

