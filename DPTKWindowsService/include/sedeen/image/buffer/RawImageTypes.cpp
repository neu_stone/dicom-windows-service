#include "image/buffer/RawImageTypes.h"

#include <string>

#include "global/Debug.h"

namespace sedeen {
namespace image {

std::string colorDescription(Color color) {
  switch(color) {
    case Alpha: return "Alpha image (8 bpp)";
    case Gray_8: return "Grayscale (8 bpp)";
    case Gray_16: return "Grayscale (16 bpp) ";
    case RGB_8: return "RGB (24 bpp)";
    case ARGB_8: return "RGB (32 bpp)";
    case RGB_16: return "RGB (48 bpp)";
    case ARGB_16: return "RGB (72 bpp)";
    case Unknown: return "Unknown";
  }
  debug::assert_x(false, "Unknown color", __FUNCTION__, __LINE__);
  return "Unknown";
}

int bytesPerPixel(Color color) {
  switch(color) {
    case Alpha:
    case Gray_8: return 1;
    case Gray_16: return 2;
    case RGB_8: return 3;
    case RGB_16: return 6;
    case ARGB_8: return 4;
    case ARGB_16: return 8;
    case Unknown: return 0;
  }
  debug::assert_x(false, "Unknown color", __FUNCTION__, __LINE__);
  return 0;
}

int componentsPerPixel(Color color) {
  switch(color) {
    case Alpha:
    case Gray_8:
    case Gray_16: return 1;
    case RGB_8:
    case RGB_16: return 3;
    case ARGB_8:
    case ARGB_16: return 4;
    case Unknown: return 0;
  }
  debug::assert_x(false, "Unknown color", __FUNCTION__, __LINE__);
  return 0;
}

}   // namespace image
}   // namespace sedeen



