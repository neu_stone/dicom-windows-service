#ifndef SEDEEN_SRC_IMAGE_BUFFER_RAWIMAGETYPES_H
#define SEDEEN_SRC_IMAGE_BUFFER_RAWIMAGETYPES_H

// System includes
#include <boost/cstdint.hpp>
#include <boost/shared_ptr.hpp>

#include "image/DllApi.h"

namespace sedeen {
namespace image {

// Read-Write class
class RawImageBuffer;
typedef boost::shared_ptr<RawImageBuffer> RawImageBufferPtr;
typedef boost::shared_ptr<const RawImageBuffer> ConstRawImageBufferPtr;

// Read-Write data type
typedef boost::shared_ptr<void> RawDataPtr;
typedef boost::shared_ptr<const void> ConstRawDataPtr;

typedef boost::uint8_t     uint8_t;
typedef boost::uint16_t    uint16_t;
typedef boost::uint32_t    uint32_t;
typedef boost::uint64_t    uint64_t;

typedef boost::int8_t      int8_t;
typedef boost::int16_t     int16_t;
typedef boost::int32_t     int32_t;
typedef boost::int64_t     int64_t;

/// The buffer's color space
enum Color {
  ARGB_8,   ///< defines ARGB color with 8 bit components
  ARGB_16,  ///< defines ARGB color with 16 bit components
  RGB_8,    ///< defines RGB color with 8 bit components
  RGB_16,   ///< defines RGB color with 16 bit components
  Gray_8,   ///< defines garyscale color with 8 bit components
  Gray_16,  ///< defines garyscale color with 16 bit components
  Alpha,  ///< defines an alpha image with 8 bit components (0 is fully opque which 255 if fully transparent)
  //HSV,
  //LAB,
  //XYZ,
  //ICC,
  //YCC,
  //CMYK,
  //PALLETTE,
  Unknown,  ///< defines an unknown color
};

/// Pixel order enumeration
enum PixelOrder {
  Interleaved,  ///< Componets positioned together: RGB..RGB..
  Planar,       ///< Componets positioned seperately: RR..GG..BB..
};

/// Returns a text description of the color enum
SEDEEN_IMAGE_API
std::string colorDescription(Color color);

/// Returns byte count per pixel for the given color or 0 if Unknown.
SEDEEN_IMAGE_API
int bytesPerPixel(Color color);

/// Returns component count per pixel for the given color or 0 if Unknown.
SEDEEN_IMAGE_API
int componentsPerPixel(Color color);

}   // namespace image
}   // namespace sedeen

#endif



