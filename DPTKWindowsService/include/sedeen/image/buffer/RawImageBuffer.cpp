// Main include
#include "image/buffer/RawImageBuffer.h"

// User includes
#include "global/Debug.h"
#include "global/geometry/Point.h"
#include "global/geometry/Size.h"

namespace sedeen {
namespace image {

RawImageBuffer::RawImageBuffer()
{}

RawImageBuffer::~RawImageBuffer()
{}

bool RawImageBuffer::contains(int32_t x, int32_t y, int32_t c) const
{ return ( x < width() &&
          y < height() &&
          c < components() &&
          x >= 0 &&
          y >= 0 &&
          c >= 0 );
}

bool RawImageBuffer::contains(uint64_t index) const
{ return index < count();
}

bool RawImageBuffer::isNull() const
{ return count() == 0;
}

Rect RawImageBuffer::rect() const
{ return Rect(Point(0, 0), size());
}

uint64_t RawImageBuffer::count() const
{ return doGetCount();
}

Size RawImageBuffer::size() const
{ return doGetSize();
}

int32_t RawImageBuffer::width() const
{ return doGetWidth();
}

int32_t RawImageBuffer::height() const
{ return doGetHeight();
}

int8_t RawImageBuffer::components() const
{ return doGetNumComponents();
}

Color RawImageBuffer::color() const
{ return doGetColor();
}

PixelOrder RawImageBuffer::order() const
{ return doGetOrder();
}

uint64_t RawImageBuffer::bytes() const
{ return doGetBytes();
}

int32_t RawImageBuffer::bytesPerPixel() const
{ return doGetBytesPerPixel();
}

int32_t RawImageBuffer::bitsPerComponent() const
{ return doGetBitsPerComponent();
}

int32_t RawImageBuffer::maxValue() const
{ return doGetMaxValue();
}

int32_t RawImageBuffer::minValue() const
{ return doGetMinValue();
}

bool RawImageBuffer::isSigned() const
{ return doGetIsSigned();
}

ConstRawDataPtr RawImageBuffer::data() const
{ return doGetData();
}

RawImageBufferPtr RawImageBuffer::clone() const
{ return doGetClone();
}

uint64_t RawImageBuffer::index(int32_t x, int32_t y, int32_t c) const
{ debug::assert_x( x < width(), "Invalid index", __FILE__, __LINE__);
  debug::assert_x( y < height(), "Invalid index", __FILE__, __LINE__);
  debug::assert_x( c < components(), "Invalid index", __FILE__, __LINE__);
  switch( order() ) {
    case Interleaved: return indexInterleaved(x,y,c); break;
    case Planar: return indexPlanar(x,y,c); break;
    default: debug::assert_x(false, "Invalid pixel order", __FILE__, __LINE__);
             return 0; break;
  }
}

uint64_t RawImageBuffer::indexInterleaved(int32_t x, int32_t y, int32_t c) const
{ uint64_t off = uint64_t( width() ) * y + x;
  return (off * components()) + c;
}

uint64_t RawImageBuffer::indexPlanar(int32_t x, int32_t y, int32_t c) const
{ uint64_t off = uint64_t( width() ) * y + x;
  uint64_t planSpan = uint64_t( width() ) * height();
  return off + planSpan * c;
}

void RawImageBuffer::setValue(int32_t x, int32_t y, int32_t c, int32_t value)
{ setValue(index(x,y,c), value);
}

void RawImageBuffer::setValue(uint64_t index, int32_t value)
{ if( !isNull() && contains(index) ) {
    doSetBufferValue(index, value);
 }
else {
     debug::assert_x(false, "Index out of range.", __FILE__, __LINE__);
 }
}

int32_t RawImageBuffer::at(int32_t x, int32_t y, int32_t c) const
{ return at(index(x,y,c));
}

int32_t RawImageBuffer::at(uint64_t index) const
{ if( !isNull() && contains(index) )
    return operator[](index);
else {
    debug::assert_x(false, "Index our of range.", __FILE__, __LINE__);
    return 0;
 }
}

int32_t RawImageBuffer::operator[](uint64_t index) const
{ return doGetBufferValue(index);
}

void RawImageBuffer::fill(int32_t value)
{ doFillBuffer(value);
}

void RawImageBuffer::copy(const Rect& destination_region, const RawImageBuffer& source_image, const Rect& source_region)
{ doCopy(destination_region, source_image, source_region);
}

void RawImageBuffer::flipVertical()
{ doFlipVertical();
}

void RawImageBuffer::flipHorizontal()
{ doFlipHorizontal();
}
}   // namespace sedeen
}   // namespace image

