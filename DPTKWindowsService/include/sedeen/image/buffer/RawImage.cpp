// Main include
#include "image/buffer/RawImage.h"

#include <QtCore>

// User includes
#include "global/geometry/Point.h"
#include "global/geometry/Size.h"
#include "image/buffer/RawImageFactory.h"
#include "image/buffer/RawImageBuffer.h"

namespace sedeen {
namespace image {
namespace {

int qt_meta_type = qRegisterMetaType<RawImage>("RawImage");

}

RawImage::RawImage() : buffer_(createNullRawImageBuffer<unsigned char>()) {
}

RawImage::RawImage(const RawImage& image) : buffer_(image.buffer_) {
}

RawImage::RawImage(const Size& size, Color color)
    : buffer_(createRawImageBuffer(size, color)) {
}

RawImage::RawImage(const Size& size, const RawImage& other)
    : buffer_(createIntegerRawImageBuffer(size.width(), size.height(),
                                          other.components(), other.order(),
                                          other.bitsPerComponent(),
                                          other.isSigned(), other.color())) {
}

RawImage::RawImage(RawImageBufferPtr data) : buffer_(data) {
}

RawImage::RawImage(const RawDataPtr& data, const Size& size, Color color,
                   PixelOrder order)
    : buffer_(createRawImageBuffer(data, size, color, order)) {
}

RawImage& RawImage::operator=(const RawImage& image) {
  buffer_ = image.buffer_;
  return *this;
}

bool RawImage::operator==(const RawImage& other) {
  return buffer_ == other.buffer_;
}

void RawImage::flipVertical() {
  detach();
  buffer_->flipVertical();
}

void RawImage::flipHorizontal() {
  detach();
  buffer_->flipHorizontal();
}

int32_t RawImage::at(int32_t x, int32_t y, int32_t c) const {
  return at(index(x,y,c));
}

int32_t RawImage::at(uint64_t index) const {
  return buffer_->at(index);
}

int32_t RawImage::operator[](uint64_t index) const {
  return (*buffer_)[index];
}

void RawImage::fill(int32_t value) {
  detach();
  buffer_->fill(value);
}

void RawImage::setValue(int32_t x, int32_t y, int32_t c, int32_t value) {
  setValue(index(x,y,c), value);
}

void RawImage::setValue(uint64_t index, int32_t value) {
  detach();
  buffer_->setValue(index, value);
}

bool RawImage::isNull() const {
  return buffer_->isNull();
}

void RawImage::copy(const Rect& destination_region,
                    const RawImage& source_image, const Rect& source_region) {
  detach();
  buffer_->copy(destination_region, *source_image.buffer_.get(), source_region);
}

Rect RawImage::rect() const {
  return Rect(Point(0, 0), size());
}

uint64_t RawImage::count() const {
  return buffer_->count();
}

Size RawImage::size() const {
  return buffer_->size();
}

int32_t RawImage::width() const {
  return buffer_->width();
}

int32_t RawImage::height() const {
  return buffer_->height();
}

int8_t RawImage::components() const {
  return buffer_->components();
}

uint64_t RawImage::bytes() const {
  return buffer_->bytes();
}

int32_t RawImage::bytesPerPixel() const {
  return buffer_->bytesPerPixel();
}

int32_t RawImage::bitsPerComponent() const {
  return buffer_->bitsPerComponent();
}

int32_t RawImage::maxValue() const {
  return buffer_->maxValue();
}

int32_t RawImage::minValue() const {
  return buffer_->minValue();
}

bool RawImage::isSigned() const {
  return buffer_->isSigned();
}

Color RawImage::color() const {
  return buffer_->color();
}

PixelOrder RawImage::order() const {
  return buffer_->order();
}

bool RawImage::contains(int32_t x, int32_t y, int32_t c) const {
  return buffer_->contains(x,y,c);
}

bool RawImage::contains(uint64_t index) const {
  return buffer_->contains(index);
}

uint64_t RawImage::index(int32_t x, int32_t y, int32_t c) const {
  return buffer_->index(x,y,c);
}

ConstRawDataPtr RawImage::data() const {
  return buffer_->data();
}

void RawImage::detach() {
  if (!buffer_.unique())
    buffer_ = buffer_->clone();
}

RawImageBufferPtr RawImage::buffer() const {
  return buffer_;
}

}   // namespace sedeen
}   // namespace image
