#ifndef SEDEEN_SRC_IMAGE_BUFFER_RAWIMAGEBUFFER_H
#define SEDEEN_SRC_IMAGE_BUFFER_RAWIMAGEBUFFER_H

// System includes
#include <boost/cstdint.hpp>

// User includes
#include "image/buffer/RawImageTypes.h"
#include "global/geometry/Rect.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {
    /*!
        \class RawImageBuffer
        \brief A buffer for a 3D array of elements. Buffer can be bound to an already exisiting c-style array.
	 */
class SEDEEN_IMAGE_API RawImageBuffer {
public:

        /// \brief constructor
        RawImageBuffer();

        virtual ~RawImageBuffer();

        /// \brief clones the object
        RawImageBufferPtr clone() const;

        /// \brief Gets the \a index of an element in buffer.
        //
        /// Throws an exception if out of range.
        int32_t at(int32_t x, int32_t y, int32_t c) const;

        /// \brief access the \a index element in buffer.
        //
        /// Throws a debug assertion if element is out
        int32_t at(uint64_t index) const;

        /// \brief Gets the element at position \a index from buffer.
        int32_t operator[](uint64_t index) const;

        /// \brief Gets index of the given element
        /// Throws a debug assertion if element is out of range
        uint64_t index(int32_t x, int32_t y, int32_t c) const;

        /// \brief Gets raw pointer to buffer data
        ConstRawDataPtr data() const;

        /// \brief Fills buffer.
        void fill(int32_t value);

        /// \brief Set a component. Throws a debug assertion if \a index is out of range and does nothing. \a value is statically cast to the buffer data type.
        void setValue(int32_t x, int32_t y, int32_t c, int32_t value);

        /// \brief Set a value  at the given index in the buffer. Throws a debug assertion if \a index is out of range and does nothing. \a value is statically cast to the buffer data type.
        void setValue(uint64_t index, int32_t value);

        /// \brief True if the image have no data and zero dimensions
        bool isNull() const;

        /// Copies a portion of the \a source_image into this instance.
        //
        /// \note \a source_region is scaled to fit \a destination_region.
        //
        /// \param destination_region the area of this instance to fill
        /// \param source_image the image to copy
        /// \param source_region the area of the source to copy
        void copy(const Rect& destination_region, const RawImageBuffer& source_image, const Rect& source_region);

        /// Flips the image vertically
        void flipVertical();

        /// Flips the image horizonally
        void flipHorizontal();

        /// \brief Gets the bounding box of image
        Rect rect() const;

        /// \brief Gets total number of elements
        uint64_t count() const;

        /// \brief Gets the dimensions of image
        Size size() const;

        /// \brief Gets image width
        int32_t width() const;

        /// \brief Gets image height
        int32_t height() const;

        /// \brief Gets number of color channels
        int8_t components() const;

        /// \brief Gets the color space
        Color color() const;

        /// \brief Gets the pixel order of buffer
        PixelOrder order() const;

        /// \brief Gets number of buffered bytes
        uint64_t bytes() const;

        /// \brief Gets bytes per pixel
        int32_t bytesPerPixel() const;

        /// \brief Gets the the number of bits in memory for each component
        int32_t bitsPerComponent() const;

        /// \brief Gets maximum value that each component can hold.
        //
        /// This value is dictated by bitsPercomponent() and isSigned()
        int32_t maxValue() const;

        /// \brief Gets minimum value that each component can hold.
        //
        /// \copydetails maxValue() const
        int32_t minValue() const;

        /// \brief TRUE if the data type is unsigned
        bool isSigned() const;

        /// \brief Check to see if the buffer contains position(x,y,c)
        bool contains(int32_t x, int32_t y, int32_t c) const;

        /// \brief Check to see if the buffer contains the index
        bool contains(uint64_t index) const;

    protected:
        // protect copy constructor
        RawImageBuffer(const RawImageBuffer&);

        virtual RawImageBufferPtr doGetClone() const = 0;
        virtual ConstRawDataPtr doGetData() const = 0;
        virtual int8_t doGetNumComponents() const = 0;
        virtual Color doGetColor() const = 0;
        virtual PixelOrder doGetOrder() const = 0;
        virtual Size doGetSize() const = 0;
        virtual uint64_t doGetCount() const = 0;
        virtual int32_t doGetWidth() const = 0;
        virtual int32_t doGetHeight() const = 0;
        virtual int32_t doGetMaxValue() const = 0;
        virtual int32_t doGetMinValue() const = 0;
        virtual bool doGetIsSigned() const = 0;
        virtual uint64_t doGetBytes() const = 0;
        virtual int32_t doGetBytesPerPixel() const = 0;
        virtual int32_t doGetBitsPerComponent() const = 0;
        virtual int32_t doGetBufferValue(uint64_t offset) const = 0;
        virtual void doSetBufferValue(uint64_t offset, int32_t value) = 0;
        virtual void doFillBuffer(int32_t value) = 0;
        virtual void doCopy(const Rect& destRegion, const RawImageBuffer& srcImage, const Rect& srcRegion) = 0;
        virtual void doFlipVertical() = 0;
        virtual void doFlipHorizontal() = 0;
    private:
        uint64_t indexInterleaved(int32_t x, int32_t y, int32_t c) const;
        uint64_t indexPlanar(int32_t x, int32_t y, int32_t c) const;
};

}   // namespace sedeen
}   // namespace image

#endif


