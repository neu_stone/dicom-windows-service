// Primary header
#include "image/io/OpenTiledImage.h"

// System includes
#include <QtGlobal>

// User includes
#include "image/io/SelectiveDecoder.h"
#include "image/io/TiledImage.h"

using namespace sedeen::image;

OpenTiledImage::
OpenTiledImage()
{
}

OpenTiledImage::
~OpenTiledImage()
{
}

std::auto_ptr<ImageData>
OpenTiledImage::
doOpen(const std::string& filename) const
{
  std::auto_ptr<ImageData> imageData;

  // Attempt to load a selective decoder
  std::auto_ptr<SelectiveDecoder> sd(getSelectiveDecoder(filename));
  if (sd.get())
  {
    imageData.reset(new TiledImage(sd));

    Q_ASSERT(0 != imageData.get());
  }

  return imageData;
}
