// Primary header
#include "image/io/QueueManager.h"

// System headers
#include <QtGlobal>
#include <algorithm>
#include <functional>
#include <ostream>
#include <utility>

// User headers
#include "global/Debug.h"
#include "global/geometry/Size.h"
#include "global/geometry/Rect.h"
#include "image/io/DecodeRequest.h"

namespace sedeen {
namespace image {
namespace queue_manager {

/// Encapsulates a decode request from a specific client.
//
/// This class is not thread-safe, but is reentrant.
class Request {
 public:
  Request() : m_client(), m_request(), m_priority() {}

  /// Create a new request object.
  //
  /// \param client
  /// Pointer to the TiledImage instance originating the request.
  ///
  /// \param request
  ///
  /// \param priority
  /// The relative priority of the request.
  Request(const ImageAccessor* client, const DecodeRequestPtr& request,
          Priority priority)
      : m_client(client),
        m_request(request),
        m_priority(priority) {}

  /// Equality tested on client address.
  bool operator==(const Request& rhs) const {
    return m_client == rhs.m_client;
  }

  /// Test equality of clients.
  bool operator==(const ImageAccessor* client) const {
    return m_client == client;
  }

  /// Get the client originating this request
  const ImageAccessor* client() const { return m_client; }

  /// Get the request to be decoded
  const DecodeRequestPtr& request() const { return m_request; }

  /// Get the priority of the request
  Priority priority() const { return m_priority; }

 private:
  /// Client image.
  const ImageAccessor* m_client;

  /// Target region of the source image.
  DecodeRequestPtr m_request;

  /// Relative priority of this request.
  Priority m_priority;

  friend std::ostream& operator<<(std::ostream&, const Request&);
}; // end class Request

class Prioritize {
 public:
  bool operator()(const Request& lhs, const Request& rhs) const {
    return lhs.priority() < rhs.priority();
  }
}; // class Prioritize

std::ostream& operator<<(std::ostream& ostream, const Request& request) {
  return ostream
      << "Client: " << request.client() << std::endl
      << "Region: " << request.request()->region(0)
      << std::endl
      << "Size: " << request.request()->size().width() << ","
      <<  request.request()->size().height() << std::endl
      << "Priority: " << int(request.m_priority) << std::endl;
}

/// Puts queue_manager::Request objects into a stream.
class Streamer : public std::unary_function<Request,void> {
 public:
  /// Bind to an ostream.
  Streamer(std::ostream& ostream) : m_stream(ostream) {}
  /// Put a Request in the bound stream.
  void operator()(const Request& request) { m_stream << request << std::endl; }
 private:
  std::ostream& m_stream;
}; // class Streamer

} // namespace queue_manager

QueueManager::QueueManager() : queue_() {}

QueueManager::~QueueManager() {}

bool QueueManager::submitRequest(const ImageAccessor* client,
                                 const DecodeRequestPtr& request,
                                 Priority priority) {
  queue_manager::Request req(client, request, priority);
  const bool currentTaskAltered = enqueue(req);
  return currentTaskAltered;
}

bool QueueManager::cancelRequest(const ImageAccessor* client) {
  // Remove any references to this client
  return purge(client);
}

bool QueueManager::next() {
  assert(!queue_.empty());
  queue_.pop_back();
  return !empty();
}

const ImageAccessor* QueueManager::client() const {
  assert(!empty());
  return queue_.back().client();
}

const DecodeRequestPtr& QueueManager::request() const {
  assert(!empty());
  return queue_.back().request();
}

bool QueueManager::empty() const {
  return queue_.empty();
}

bool QueueManager::purge(const ImageAccessor* client) {
  // Reset the current request, if it matches
  bool altered_current_request = !queue_.empty() && queue_.back() == client;
  // Remove matching Requests from the queue.
  queue_.erase(std::remove(queue_.begin(), queue_.end(), client), queue_.end());
  return altered_current_request;
}

bool QueueManager::enqueue(const queue_manager::Request& newRequest) {
  bool alteredCurrentRequest = purge(newRequest.client());
  // Insert the new value into its sorted position
  queue_.insert(std::lower_bound(queue_.begin(), queue_.end(), newRequest,
                                 queue_manager::Prioritize()),
                newRequest);
  // If we've inserted the new request at the back, signal that the current
  // request has been changed.
  if (newRequest == queue_.back()) alteredCurrentRequest = true;
  return alteredCurrentRequest;
}

std::ostream& operator<<(std::ostream& ostream,
                         const QueueManager& queueManager) {
  std::for_each(queueManager.queue_.begin(), queueManager.queue_.end(),
                queue_manager::Streamer(ostream));
  return ostream;
}

} // namespace image
} // namespace sedeen
