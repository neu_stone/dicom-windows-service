// Primary header
#include "image/io/ImageWriter.h"

#include "image/io/ImageAccessor.h"

using namespace sedeen::image;

namespace sedeen {
namespace image {

ImageWriter::~ImageWriter() {}

void ImageWriter::write(const ImageAccessor& imageAccessor,
                        const QString& outFilename,
                        const QRect& inputRegion,
                        double zoom) const {
  doWrite(imageAccessor, outFilename, inputRegion, zoom);
}

QString ImageWriter::defaultExtension() const {
  QString ext = getDefaultExtension();

  Q_ASSERT_X(!ext.isNull(), __FUNCTION__,
             "Must generate a non-null default extension.");

  return ext.toLower();
}

QString ImageWriter::fileFilter() const {
  return getFileFilter();
}

QWidget* ImageWriter::options(QWidget* parent) const {
  Q_ASSERT_X(0 != parent, __FUNCTION__, "Must assign a parent to the widget");

  QWidget* optionWidget = buildOptionWidget();

  // If there is, in fact, a widget, assign a parent
  if (0 != optionWidget) optionWidget->setParent(parent);
  return optionWidget;
}

ImageWriter::ImageWriter(QObject* parent) : QObject(parent) {}

QWidget* ImageWriter::buildOptionWidget() const {
  return 0;
}

} // namespace image
} // namespace sedeen
