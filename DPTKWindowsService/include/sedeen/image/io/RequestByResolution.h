#ifndef SEDEEN_SRC_IMAGE_IO_REQUESTBYRESOLUTION_H
#define SEDEEN_SRC_IMAGE_IO_REQUESTBYRESOLUTION_H

// User headers
#include "global/geometry/Rect.h"
#include "image/io/DecodeRequest.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {

class ImagePyramid;

/// Describes a region to be decoded. To be used by SelectiveDecoder.
class SEDEEN_IMAGE_API RequestByResolution : public DecodeRequest {
 public:
  typedef DecodeRequest::TileVec          TileVec;
  typedef DecodeRequest::TileVecPtr       TileVecPtr;
  typedef DecodeRequest::TileVecConstPtr  TileVecConstPtr;

  /// Constructs a NULL request
  RequestByResolution();

  ///  Defines a request given in the coordinates system of resolution
  /// \a resolution.
  //
  /// \param pyramid
  ///
  /// \param region
  /// area to be decoded at resolution \a resolution
  ///
  /// \param resolution
  /// a valid resolution level
  ///
  /// \param cache
  RequestByResolution(const ImagePyramidPtr &pyramid,
                      const Rect &region,
                      const int32_t resolution,
                      const bool cache = false);

  /// Assignment operator
  RequestByResolution& operator=(const RequestByResolution& rhs);

 private:
  bool doGetIsNull() const;
  Rect doGetRegion(int32_t level) const;
  Size doGetSize() const;
  SizeF doGetScale() const;
  int32_t doGetResolution() const;
  bool doGetCache() const;
  ImagePyramidPtr doGetPyramid() const;
  TileVecConstPtr doGetTiles() const;
  DecodeRequestPtr doGetClone() const;

 private:
  ImagePyramidPtr _pyramid;
  Rect        _region;
  int32_t     _resolution;
  bool        _cache;
  TileVecPtr  _tiles;
}; // class RequestByResolution

}   // namespace image
}   // namespace sedeen

#endif // SEDEEN_SRC_IMAGE_IO_REQUESTBYRESOLUTION_H
