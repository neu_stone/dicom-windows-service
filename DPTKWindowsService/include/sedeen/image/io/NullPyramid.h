#ifndef SEDEEN_SRC_IMAGE_IO_NULLPYRAMID_H
#define SEDEEN_SRC_IMAGE_IO_NULLPYRAMID_H

// user
#include "image/buffer/RawImageTypes.h"
#include "image/io/ImagePyramid.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {
class TileInfo;

	/*!
        \class NullPyramid
        \brief Describes the tile pyramid structure for a JPEG-2000 image.

        Note that resolution level (reslvl) 0 must the represents full resolution image.

     */
class SEDEEN_IMAGE_API NullPyramid : public ImagePyramid {
public:
        /*!
            \brief constructor
        */
        NullPyramid();

        /*!
            \brief Destructor
        */
        ~NullPyramid();

    protected:
        int32_t doGetLevel(const double scale) const;
        ImagePyramidPtr doGetClone() const;
    };

}   // namespace sedeen
}   // namespace image
#endif // SEDEEN_SRC_IMAGE_IO_NULLPYRAMID_H


