#ifndef SEDEEN_SRC_IMAGE_IO_IMAGELISTENER_H
#define SEDEEN_SRC_IMAGE_IO_IMAGELISTENER_H

// System headers
#include <QtGui>

#include "image/buffer/RawImage.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {

class ImageAccessor;

/// Utility class used for catching signals from ImageAccessor.
class SEDEEN_IMAGE_API ImageListener : public QObject {
  Q_OBJECT;
 public:
  /// Bind an instance to a specific ImageAccessor.
  ImageListener(const ImageAccessor& client);

  ~ImageListener();

  /// Check if the decode is finished.
  bool complete() const;

  /// Get the current version of the decoded image.
  RawImage image() const;

 private slots:
  /// Update internal cache of the current image and decoder status.
  void imageUpdated(RawImage image, bool complete);

  /// handles error conditions
  void error(const std::string &error, const std::string &filename);

  /// Clear the cached version of the image.
  void imageClosed();

 private:
  /// Cached copy of the decoded image.
  RawImage m_imageCache;

  /// Flag signaling the completion of the decode task.
  bool m_complete;
}; // end class ImageListener

} // end namespace image
} // end namespace sedeen

#endif
