// Primary header
#include "image/io/RequestBySize.h"

// System headers
#include <boost/make_shared.hpp>
#include <cmath>

// User Headers
#include "global/Debug.h"
#include "image/io/NullPyramid.h"

namespace sedeen {
namespace image {
namespace {
/// finds a valid resolution level closest to \a desired_resolution
int32_t autoSelectLevel(const ImagePyramid& pyramid, const SizeF &scale,
                        const int32_t desired_resolution) {
  int32_t level = getNearestValidResoution(pyramid, desired_resolution);
  if (desired_resolution != level) {
    double scl = std::min(scale.width(), scale.height());
    return pyramid.level(scl);
  } else {
    return level;
  }
}
/// finds the scale of the \a size w.r.t. \a region
SizeF getScale(const Rect &region, const Size &size) {
  assert(!isEmpty(region));
  return SizeF(static_cast<double>(size.width()) / region.width(),
               static_cast<double>(size.height()) / region.height());
}

} // namespace

RequestBySize::RequestBySize(const ImagePyramidPtr &pyramid, const Rect &region,
                             const Size &size, const bool cache,
                             const int32_t desired_resolution)
    : _pyramid(pyramid),
      _region(region),
      _size(size),
      _scale(getScale(region,size)),
      _resolution(autoSelectLevel(*pyramid, _scale, desired_resolution)),
      _cache(cache),
      _tiles(tileListSpanningRegion(region, 0, _resolution, *pyramid)) {}

RequestBySize::RequestBySize()
    : _pyramid(boost::make_shared<NullPyramid>()),
      _region(),
      _size(0,0),
      _scale(0,0),
      _resolution(0),
      _cache(false),
      _tiles(boost::make_shared<TileVec>()) {}

DecodeRequestPtr RequestBySize::doGetClone() const {
  return boost::make_shared<RequestBySize>(*this);
}

bool RequestBySize::doGetIsNull() const {
  return tiles()->size() == 0;
}

Rect RequestBySize::doGetRegion(int32_t level) const {
  debug::assert_x(level >= 0 && level < pyramid()->levels(),
                  "Invalid resolution level specified", __FUNCTION__, __LINE__);
  if (level == 0)
    return _region;
  else
    return mapRegionToAlternateResolution(_region, 0, level, *pyramid());
}

int32_t RequestBySize::doGetResolution() const {
  return _resolution;
}

bool RequestBySize::doGetCache() const {
  return _cache;
}

Size RequestBySize::doGetSize() const {
  return _size;
}

SizeF RequestBySize::doGetScale() const {
  return _scale;
}

ImagePyramidPtr RequestBySize::doGetPyramid() const {
  return _pyramid;
}

RequestBySize::TileVecConstPtr RequestBySize::doGetTiles() const {
  return _tiles;
}

DecodeRequestPtr requestByHeight(const ImagePyramidPtr &pyramid,
                                 const Rect &region, const int32_t height,
                                 const bool cache,
                                 const int32_t desired_resolution) {
  assert(!isEmpty(region));
  int32_t width = static_cast<int32_t>(std::ceil( static_cast<double>(height) / region.height() * region.width()));
  return boost::make_shared<RequestBySize>(pyramid, region, Size(width,height), cache, desired_resolution);
}

DecodeRequestPtr requestByWidth(const ImagePyramidPtr &pyramid,
                                const Rect &region, const int32_t width,
                                const bool cache,
                                const int32_t desired_resolution) {
  assert(!isEmpty(region));
  int32_t height = static_cast<int32_t>(std::ceil( static_cast<double>(width) / region.width() * region.height()));
  return boost::make_shared<RequestBySize>(pyramid, region, Size(width,height), cache, desired_resolution);
}

}   // namespace image
}   // namespace sedeen
