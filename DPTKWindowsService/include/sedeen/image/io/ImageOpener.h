#ifndef SEDEEN_SRC_IMAGE_IO_IMAGEOPENER_H
#define SEDEEN_SRC_IMAGE_IO_IMAGEOPENER_H

// System headers
#include <QtCore>
#include <boost/shared_ptr.hpp>
#include <functional>
#include <vector>
#include <boost/shared_ptr.hpp>

#include "image/DllApi.h"

namespace sedeen {
namespace image {

class ImageData;
class ImageOpenHandler;

/// Organizational class for open handlers.
//
/// Handles ordering of various image open handlers.
class SEDEEN_IMAGE_API ImageOpener {
 public:
  ImageOpener();

  ~ImageOpener();

  /// Insert an ImageOpenHandler to the opener.
  //
  /// \param imageOpenHandler
  ///
  void addHandler(std::auto_ptr<ImageOpenHandler> imageOpenHandler);

  /// Get a list of file filter expressions.
  //
  /// The returned list is compatible with Qt's file dialogs.
  QString filterList() const;

  /// Attempt to open the image at the specified location.
  //
  /// \param filename
  /// Path and filename of the file to be opened. File must exist.
  ///
  /// \throws
  /// If the file cannot be opened, throws an exception of type FileOpenError.
  ///
  /// \throws
  /// If no appropriate handler can be found, throws an exception of type UnhandledFileType.
  ///
  /// \return
  /// An auto_ptr to the appropriate derivative of ImageData.
  boost::shared_ptr<ImageData> open(const std::string& filename) const;

 private:
  typedef std::vector< boost::shared_ptr<ImageOpenHandler> > Openers_t;

  /// Collection of open handelrs.
  Openers_t m_openers;

}; // end class ImageOpener

namespace image_opener {

/// Function object to collect filters from a number of openers.
class SEDEEN_IMAGE_API CollectFilters :
      std::unary_function<boost::shared_ptr<ImageOpenHandler>,void> {
 public:
  CollectFilters();

  /// Add the opener's filter to the list.
  void operator()(boost::shared_ptr<ImageOpenHandler> opener);

  /// Get the collected list.
  QStringList filterList() const;

 private:
  /// Collection variable.
  boost::shared_ptr<QStringList> m_filters;
}; // end class CollectFilters

} // end namespace image_opener
} // end namespace image
} // end namespace sedeen

#endif
