// main header
#include "image/io/ImageParser.h"
#include "image/io/ImagePyramid.h"
#include "global/geometry/Rect.h"

// System includes
#include <cassert>

// User includes
#include "global/UnitDefs.h"

namespace sedeen
{
namespace image
{

namespace {
// Checks if the \a parser is a NullParser
bool isNull(const ImageParser &parser) {
  return isEmpty((parser.pyramid()->rect(0)));
}
}

ImageParser::ImageParser()
{}

ImageParser::~ImageParser()
{}

ImagePyramidPtr ImageParser::pyramid() const {
  return doGetPyramid();
}

bool ImageParser::hasLabelImage() const {
  return doGetHasLabelImage();
}

bool ImageParser::hasMacroImage() const {
  return doGetHasMacroImage();
}

bool ImageParser::hasThumbnailImage() const {
  return doGetHasThumbnailImage();
}

bool ImageParser::hasPixelSpacing() const {
  return !isEmpty(pixelSize());
}

bool ImageParser::hasMagnification() const {
  return doGetMagnification() > 0 ||
        hasPixelSpacing();
}

std::string ImageParser::filename() const {
  assert(!doGetFilename().empty() ^ isNull(*this));
  return doGetFilename();
}

SizeF ImageParser::pixelSize() const {
  return doGetPixelSize();
}

int ImageParser::components() const {
  assert((doGetComponents() > 0) ^ isNull(*this));
  return doGetComponents();
}

int ImageParser::bpp() const {
  assert((doGetBpp() > 0) ^ isNull(*this));
  return doGetBpp();
}

int ImageParser::bpc(int comp) const {
  assert((comp < components())  ^ isNull(*this));
  assert((doGetBpc(comp) > 0) ^ isNull(*this));
  return doGetBpc(comp);
}

double ImageParser::magnification() const {
  assert(doGetMagnification()>=0);
  assert( !(hasPixelSpacing() ^ hasMagnification()) );
  return doGetMagnification();
}

std::string ImageParser::producer() const {
  assert(!doGetProducer().empty() ^ isNull(*this));
  return doGetProducer();
}

Color ImageParser::color() const {
  return doGetColor();
}

std::string ImageParser::compression() const {
  assert(!doGetCompression().empty() ^ isNull(*this));
  return doGetCompression();
}

std::string ImageParser::description() const {
  return doGetDescription();
}

RawImage ImageParser::labelImage() const {
  RawImage im = doGetLabelImage();
  assert(!(!im.isNull() ^ hasLabelImage()));
  return im;
}

RawImage ImageParser::thumbnailImage() const {
  RawImage im = doGetThumbnailImage();
  assert(!(!im.isNull() ^ hasThumbnailImage()));
  return im;
}

RawImage ImageParser::macroImage() const {
  RawImage im = doGetMacroImage();
  assert(!(!im.isNull() ^ hasMacroImage()));
  return im;
}

double calculateDefaultMagnification(const SizeF &pixelSize, double twenty_x_size) {
  assert(twenty_x_size > 0);
  // calculate magnification only when pixel size is isotropic and non-zero
  if ((pixelSize.width()>0) && (pixelSize.width()==pixelSize.height())) {
    return twenty_x_size / pixelSize.width() * 20.0;
  }
  else
    return 0;
}

}   // namespace image
}   // namespace sedeen

