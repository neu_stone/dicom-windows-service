//main header
#include "image/io/TilePyramid.h"

// system  headers
#include <cassert>
#include <cmath>

// User headers
#include "global/geometry/Rect.h"
#include "global/geometry/Size.h"
#include "image/io/ImagePyramid.h"

namespace sedeen {
namespace image {

TilePyramid::TilePyramid(const ImagePyramidPtr &pyramid, PurgeMode mode)
    : _pyramid(pyramid),
      _buffer() {
  _buffer.reserve(pyramid->levels());
  for (int32_t lvl = 0; lvl < pyramid->levels(); ++lvl) {
    _buffer.push_back(boost::shared_ptr<TileMatrix>(
        new TileMatrix(pyramid->rows(lvl), pyramid->cols(lvl), mode)));
  }
}

TilePyramid::~TilePyramid() {}

uint32_t TilePyramid::purgeReferenceTile(uint32_t level) const {
  assert(level < _buffer.size());

  return _buffer[level]->purgeReferenceTile();
}

void TilePyramid::setPurgeReferenceTile(uint32_t level, uint32_t tileno) {
  assert(level < _buffer.size());

  _buffer[level]->setPurgeReferenceTile(tileno);
}

void TilePyramid::setPurgeExceptions(uint32_t level,
                                     TileExceptionSetPtr exceptions) {
  assert(level < _buffer.size());

  _buffer[level]->setPurgeExceptions(exceptions);
}

ImagePyramidPtr TilePyramid::pyramid() const {
  return _pyramid;
}

bool TilePyramid::isFull(uint32_t level) const {
  assert(level < _buffer.size());

  return _buffer[level]->isFull();
}

void TilePyramid::purge(uint32_t level, uint32_t count) {
  assert(level < _buffer.size());

  _buffer[level]->purge(count);
}

uint32_t TilePyramid::size(uint32_t level) const {
  assert(level < _buffer.size());

  return _buffer[level]->size();
}

uint32_t TilePyramid::capacity(uint32_t level) const {
  assert(level < _buffer.size());

  return _buffer[level]->capacity();
}

uint32_t TilePyramid::count() const {
  uint32_t cnt = 0;
  for (int32_t lvl = 0; lvl < pyramid()->levels(); ++lvl)
    cnt += _buffer[lvl]->count();

  return cnt;
}

bool TilePyramid::hasTile(uint32_t level, uint32_t tileno) const {
  assert(level < _buffer.size());

  return _buffer[level]->hasTile(tileno);
}

bool TilePyramid::hasTile(uint32_t level, uint32_t row, uint32_t col) const {
  assert(level < _buffer.size());

  return hasTile(level, _buffer[level]->getTileno(row, col));
}

Rect TilePyramid::span(uint32_t level, uint32_t tileno) const {
  return sedeen::image::tileSpan(*pyramid(), level, tileno);
}

Rect TilePyramid::span(uint32_t level, uint32_t row, uint32_t col) const {
  return sedeen::image::tileSpan(*pyramid(), level, row, col);
}

const Tile& TilePyramid::tile(uint32_t level, uint32_t tileno) const {
  assert(level < _buffer.size());
  assert(tileno < (uint32_t)pyramid()->count(level));

  return _buffer[level]->tile(tileno);
}

const Tile& TilePyramid::tile(uint32_t level, uint32_t row,
                              uint32_t col) const {
  assert(level < _buffer.size());
  assert(col < (uint32_t)pyramid()->cols(level));
  assert(row < (uint32_t)pyramid()->rows(level));

  return _buffer[level]->tile(row, col);
}

void TilePyramid::setTile(uint32_t level, uint32_t tileno,
                          const RawImage &image, TileState state) {
  assert(level < _buffer.size());
  assert(tileno < (uint32_t)pyramid()->count(level));

  _buffer[level]->setTileImage(tileno, image, state);
}

void TilePyramid::setTile(uint32_t level, uint32_t row, uint32_t col,
                          const RawImage &image, TileState state) {
    assert(level < _buffer.size());
    assert(col < (uint32_t)pyramid()->cols(level));
    assert(row < (uint32_t)pyramid()->rows(level));

  _buffer[level]->setTileImage(row, col, image, state);
}

void TilePyramid::setCapacity(uint32_t screenWidth, uint32_t screenHeight,
                              uint32_t multiple) {
  for (int32_t lvl = 0; lvl < pyramid()->levels(); ++lvl) {
    uint32_t rows = static_cast<uint32_t>(std::ceil(
        static_cast<double>(screenHeight) / pyramid()->tileSpanSize(lvl).height()));
    uint32_t cols = static_cast<uint32_t>(std::ceil(
        static_cast<double>(screenWidth) / pyramid()->tileSpanSize(lvl).width()));

    _buffer[lvl]->setCapacity(rows*cols*multiple);
  }
}

void TilePyramid::setCapacity(uint32_t level, uint32_t numtiles) {
  assert(level < _buffer.size());

  _buffer[level]->setCapacity(numtiles);
}

TilePyramid::TileItrConst TilePyramid::begin(uint32_t level) const {
  assert(level < _buffer.size());

  return _buffer[level]->begin();
}

TilePyramid::TileItrConst TilePyramid::end(uint32_t level) const {
  assert(level < _buffer.size());

  return _buffer[level]->end();
}

} // namespace sedeen
} // namespace image

