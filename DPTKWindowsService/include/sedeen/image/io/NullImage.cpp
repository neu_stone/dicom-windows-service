// Primary header
#include "image/io/NullImage.h"

#include <boost/make_shared.hpp>

#include "global/geometry/Size.h"

namespace sedeen {
namespace image {

NullImage::NullImage() : ImageData(), image_parser_() {}

NullImage::~NullImage() {}

NullImage::NullImage(const NullImage&) : ImageData(), image_parser_() {}

boost::shared_ptr<ImageData> NullImage::doClone() const {
  return boost::make_shared<NullImage>(*this);
}

Size NullImage::doGetSize() const {
  return Size(0,0);
}

SizeF NullImage::doGetPixelSize() const {
  return image_parser_.pixelSize();
}

std::string NullImage::doGetFilename() const {
    return std::string();
}

boost::uint64_t NullImage::doSetBufferCapacity(boost::uint64_t bytes) {
    return 0;
}

boost::uint64_t NullImage::doGetBufferCapacity() const {
    return 0;
}

bool NullImage::doIsBufferCapacityFixed() const {
    return true;
}

boost::uint64_t NullImage::doBufferSizeRequired(int width, int height) const {
    return doGetBufferCapacity();
}

boost::uint64_t NullImage::doGetBufferSize() const {
    return doGetBufferCapacity();
}

RawImage NullImage::doBufferImage(double scale) const {
    return RawImage();
}

void NullImage::doRequestRegion(const ImageAccessor& client,
                               const DecodeRequestPtr& request,
                               Priority priority) {
  updateClient(client, RawImage(), true);
}

const ImageParser& NullImage::getImageParser() const {
  return image_parser_;
}

const boost::shared_ptr<ImageData>& nullImageData() {
  static boost::shared_ptr<ImageData> null_image(
      boost::make_shared<NullImage>());
  return null_image;
}

} // end class image
} // end namespace sedeen
