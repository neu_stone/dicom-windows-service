// Primary header
#include "image/io/ImageAccessor.h"

// System headers
#include <cassert>
#include <cmath>

#include "global/Debug.h"
#include "global/geometry/Point.h"
#include "global/geometry/Rect.h"
#include "global/geometry/Size.h"
#include "global/geometry/SizeF.h"
#include "image/io/ImageListener.h"
#include "image/io/ImageParser.h"
#include "image/io/ImagePyramid.h"
#include "image/io/NullImage.h"
#include "image/buffer/RawImage.h"
#include "image/io/RequestByResolution.h"
#include "image/io/RequestBySize.h"

namespace sedeen {
namespace image {

ImageAccessor::ImageAccessor()
    : QObject(),
      m_data(nullImageData()) {
  connectSignals();
}

ImageAccessor::ImageAccessor(const boost::shared_ptr<ImageData>& imageData)
    : QObject(),
      m_data(imageData) {
  // If the data is null, install the NullImage.
  if (!imageData) m_data = nullImageData();
  connectSignals();
}

ImageAccessor::ImageAccessor(const ImageAccessor& imageAccessor)
    : QObject(),
      m_data(imageAccessor.m_data) {
  connectSignals();
}

ImageAccessor& ImageAccessor::operator=(const ImageAccessor& imageAccessor) {
  disconnectSignals();
  m_data->cancelRequest(*this);
  m_data = imageAccessor.m_data;
  connectSignals();
  return *this;
}

ImageAccessor::~ImageAccessor() {
  m_data->cancelRequest(*this);
}

bool ImageAccessor::operator==(const ImageAccessor& imageAccessor) const {
  return m_data == imageAccessor.m_data;
}

Size ImageAccessor::getDimensions(int level) const {
  assert(getNumResolutionLevels() > level);

  return imageParser().pyramid()->size(level);
}

int ImageAccessor::getNumResolutionLevels() const {
  return imageParser().pyramid()->levels();
}

SizeF ImageAccessor::getPixelSize() const {
  return m_data->pixelSize();
}

RawImage ImageAccessor::bufferImage(double scale) const {
    return m_data->bufferImage(scale);
}

SelectiveDecoder* ImageAccessor::selectiveDecoder() const {
  return m_data->selectiveDecoder();
}

ImageAccessor ImageAccessor::independentCopy() const {
  return ImageAccessor(m_data->clone());
}

const ImageParser& ImageAccessor::imageParser() const {
  return m_data->imageParser();
}

bool isNull(const ImageAccessor& imageAccessor) {
  return isEmpty(imageAccessor.getDimensions(0));
}

std::string filePath(const ImageAccessor& imageAccessor) {
  return imageAccessor.m_data->filename();
}

void region(ImageAccessor& imageAccessor, double scale, const Rect& region,
            Priority priority, bool cacheSurrounding) {
  // create request
  const int32_t width = std::ceil(scale * region.width());
  DecodeRequestPtr request = requestByWidth(
      imageAccessor.imageParser().pyramid(),
      region,
      width,
      cacheSurrounding);
  // Pass on call to the data class
  imageAccessor.m_data->requestRegion(imageAccessor, request, priority);
}

void region(ImageAccessor& imageAccessor, int resolution, const Rect& target,
            Priority priority, bool cache) {
  assert(0 <= resolution);
  assert(imageAccessor.getNumResolutionLevels() > resolution);
  DecodeRequestPtr request(new RequestByResolution(
      imageAccessor.imageParser().pyramid(),
      target,
      resolution,
      cache));
  imageAccessor.m_data->requestRegion(imageAccessor, request, priority);
}

void ImageAccessor::updateImage(const RawImage& image, bool complete) const {
  //debug::message("\tImageAccessor::updateImage()");
  emit imageUpdated(image, complete);
}

void ImageAccessor::connectSignals() const {
  assert(m_data.get());
  this->connect(m_data.get(), SIGNAL(decoderCaching(int)),
                SIGNAL(decoderCaching(int)));
}

void ImageAccessor::disconnectSignals() {
  assert(m_data.get());
  this->disconnect(m_data.get());
}

void ImageAccessor::imageError(const std::string& message) {
  // Deactivate this instance
  std::string filename = m_data->filename();
  disconnectSignals();
  m_data = nullImageData();

  // Alert all clients that something has gone wrong
  emit error(message, filename);
}

RawImage regionBlock(ImageAccessor& imageAccessor, double scale,
                     const Rect& area, Priority priority,
                     bool cacheSurrounding) {
  // Build a listener to catch update signals from the decoder
  ImageListener listener(imageAccessor);

  // Request the region
  region(imageAccessor, scale, area, priority, cacheSurrounding);

  // Wait for the request to be completed, handling other events meanwhile
  const unsigned int DELAY_TIME_ms = 100; // Pause, in milliseconds
  while (!listener.complete()) {
    QCoreApplication::processEvents(QEventLoop::AllEvents, DELAY_TIME_ms);
  }

  return listener.image();
}

RawImage regionBlock(ImageAccessor& imageAccessor, int resolution,
                   const Rect& area, Priority priority,
                   bool cacheSurrounding) {
  // Build a listener to catch update signals from the decoder
  ImageListener listener(imageAccessor);

  // Request the region
  region(imageAccessor, resolution, area, priority, cacheSurrounding);

  // Wait for the request to be completed, handling other events meanwhile
  const unsigned int DELAY_TIME_ms = 100; // Pause, in milliseconds
  while (!listener.complete()) {
    QCoreApplication::processEvents(QEventLoop::AllEvents, DELAY_TIME_ms);
  }

  // do not test for NULL on listner().image()
  // if region is invalid, a NULL image is the expected output
  Q_ASSERT(isNull(imageAccessor) || !listener.image().isNull());

  return listener.image();
}

SizeF getPhysicalSize(const ImageAccessor& imageAccessor) {
  Size size = imageAccessor.getDimensions(0);
  SizeF pixelSize = imageAccessor.getPixelSize();

  return SizeF(size.width() * pixelSize.width(),
               size.height() * pixelSize.height());
}

void thumbnail(ImageAccessor& imageAccessor, const Size& max,
               Priority priority) {
  // Avoid divide-by-zero.
  assert(!isEmpty(max));

  if (isNull(imageAccessor)) {
    region(imageAccessor, 0, Rect(), priority);
  }
  else {
    Size size = imageAccessor.getDimensions(0);
    assert(!isEmpty(size));
    double thumbScale = std::min(double(max.width()) / double(size.width()),
                                 double(max.height()) / double(size.height()));
    Rect area(Point(), size);
    region(imageAccessor, thumbScale, area, priority, false);
  }
}

RawImage thumbnailBlock(ImageAccessor& imageAccessor, const Size& max,
                      Priority priority) {
  if (!isNull(imageAccessor)) {
    Size size = imageAccessor.getDimensions(0);
    double thumbScale = std::min(double(max.width()) / double(size.width()),
                                 double(max.height()) / double(size.height()));
    Rect area(Point(), imageAccessor.getDimensions(0));
    return regionBlock(imageAccessor, thumbScale, area, priority, false);
  } else {
    return RawImage();
  }
}

Rect rect(const ImageAccessor& imageAccessor, int resolution) {
  assert(0 <= resolution);
  assert(imageAccessor.getNumResolutionLevels() > resolution);
  return imageAccessor.imageParser().pyramid()->rect(resolution);
}

std::string fileName(const ImageAccessor& imageAccessor) {
  QFileInfo info(filePath(imageAccessor).c_str());
  return info.fileName().toStdString();
}

} // namespace image
} // namespace sedeen
