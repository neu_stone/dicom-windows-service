#ifndef SEDEEN_SRC_IMAGE_IO_GLOBAL_H
#define SEDEEN_SRC_IMAGE_IO_GLOBAL_H

namespace sedeen {
namespace image {

/// Priority level associated with image requests.
//
/// Intended to be passed as signed char values, so please do not add additional types outside [-128,127]. Recall that priority is relative.
enum PriorityLevels {
  kMinimum = -128, ///< The minimum-possible priority value.
  kLow     =  -64, ///< Lower priority.
  kNormal  =    0, ///< The default priority value.
  kHigh    =   64, ///< Higher priority.
  kMaximum =  127, ///< The maximum-possible priority value.
};

/// Image decoding priority level.
typedef char Priority;

} // end namespace image
} // end namespace sedeen

#endif
