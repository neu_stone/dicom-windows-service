#ifndef SEDEEN_SRC_IMAGE_IO_TILEVECTOR_H
#define SEDEEN_SRC_IMAGE_IO_TILEVECTOR_H

// System headers
#include <map>
#include <set>
#include <memory>
#include <vector>

// User headers
#include "image/io/Tile.h"
#include "image/buffer/RawImage.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {
    enum PurgeMode {
        PurgeOldestFirst,       ///< uses time stamp for purging
        PurgeFarthestFirst,     ///< Uses euclidean distance between tiles and reference tile
        PurgeFarthestRingFirst, ///< Counts the concentric rings between any given tile and the reference tile
                                ///<   i.e the 8 tiles surrounding any given tiles are at distance 1 from the reference.
    };

	/*!
        \class TileVector
        \brief A container that hold a series of tiles. All tiles in the
        container have the same dimensions given by tileHeight() and tileWidth().
        And the size of the container is fixed.

        The number of buffered tiles in the container can be managed through
        setCapacity(). The container will purge tiles as needed to ensure that
        capacity() is not exceeded. By default, oldest tiles are purged firts,
        see setPurgeMode(). The default capacity is the full size of the vector, if unchanged
        the vector will never purge any tiles.

        Can be used to represent the tiles in a multi-resolutional image.
        This class is NOT thread-safe.
     */
class SEDEEN_IMAGE_API TileVector {
    public:
        typedef std::vector<Tile>               TileBuffer;
        typedef std::map<uint32_t,uint32_t>     TileTimeMap;
        typedef std::auto_ptr<TileTimeMap>      TileTimeMapPtr;
        typedef TileTimeMap::iterator           TileTimeMapItr;
        typedef TileTimeMap::const_iterator     TileTimeMapItrConst;

        typedef std::set<uint32_t>                  TileExceptionSet;
        typedef std::auto_ptr<TileExceptionSet>     TileExceptionSetPtr;
        typedef TileExceptionSet::iterator          TileExceptionSetItr;
        typedef TileExceptionSet::const_iterator    TileExceptionSetItrConst;

        typedef std::multimap<uint32_t,uint32_t>    DistanceTileMMap;
        typedef std::auto_ptr<DistanceTileMMap>     DistanceTileMMapPtr;
        typedef DistanceTileMMap::iterator          DistanceTileMMapItr;
        typedef DistanceTileMMap::const_iterator    DistanceTileMMapItrConst;

        /*!
            \brief Create a TileVector. Currently the size of vector cannot be expanded and is fixed to \a count elements.
            If \a count is zero a empty vector will be created. This is default constructor is provided to be used smart_array s.
        */
        explicit TileVector(uint32_t count = 0, PurgeMode mode = PurgeOldestFirst);
        virtual ~TileVector();

        /*!
            \brief Adds the image to buffer.
            The buffer size will not exceed capacity() and purge()
            may be used to free up room for new tiles.

            Notes:
                - A NULL \a image cannot be set with \a state other than TileEmpty.
                - dimension of \a image must match tileWidth() and tileHeight()
                - out_of_range exception is thrown if \a tileno is out of range

            \param tileno the image's tile number
            \param image
            \param state the state of the image
            \param isReferenceForPurge if TRUE tile is used as the reference tile for purge modes other than PurgeOldestFirst
            \return
        */
        void setTileImage(uint32_t tileno, RawImage image, TileState state, bool isReferenceForPurge = false);

        /*!
            \brief Returns a reference to the  Tile at position \a tileno.

            The difference between this member function and member operator
            function operator[] or operator() is that TileVector::tile() signals
            if the requested position is out of range by throwing an out_of_range
            exception.

            \param tileno tile number
            \return
        */
        const Tile& tile(uint32_t tileno) const;

        /*!
            \brief Returns a reference to the  Tile at position \a tileno and \a tileno must be less than size().

            A similar member function, TileVector::tile(), has the same behavior
            as this operator function, except that TileVector::tile()
            signals if the requested position is out of range by throwing an exception.

            \param tileno tile number
            \return
        */
        const Tile& operator[](uint32_t tileno) const;

        const Tile& operator()(uint32_t tileno) const;

       /*!
            \brief Returns iterator to the first buffered tile.
            The Tile map iterator contains a pair<uint32_t,uint32_t> with tileno
            as key and its timestamp as the value.
            \return
        */
        TileTimeMapItrConst begin() const;

        /*!
            \brief Returns an iterator referring to the past-the-end element in the container.
            \copydetails begin()
        */
        TileTimeMapItrConst end() const;

        /*!
            \brief Purges the \a count oldest tile in the buffer.
            A tiles time stamped updated everytime setTileImage(),
            tile() for any of the operators are used.

            \param count number of tiles to delete
            \return number of tiles deleted
        */
        uint32_t purge(uint32_t count);

        /*!
            \brief Sets the purge mode. If purge mode is true, the reference tile is ignored.
            Otherwise, the purgeReferenceTile() is used to determine the farthest tiles.

            \param mode
        */
        void setPurgeMode(PurgeMode mode);

        /*!
            \brief Return the purge mode.
            \return
        */
        PurgeMode purgeMode()const;

        /*!
            \brief Return the reference tile number.
            \return
        */
        uint32_t purgeReferenceTile() const;

        /*!
            \brief Sets the reference tile to calculate distance when purge mode is anything other than PurgeOldestFirst
            and is otherwise ignored.
            The reference tile can also be set directly in setTileImage().
            \return
        */
        void setPurgeReferenceTile(uint32_t tileno);

        /*!
            \brief Sets the tiles that are to never be purges and works independtly of purgeReferenceTile()
            \return exceptions the exception list
        */
        void setPurgeExceptions(TileExceptionSetPtr exceptions);

        /*!
            \brief Number of tile elements in buffer.
            \return
        */
        uint32_t size() const;

        /*!
            \brief Number of tiles currently being buffered.
            \return
        */
        uint32_t count() const;

        /*!
            \brief Number of tiles that can be buffered buffer.
            \return
        */
        uint32_t capacity() const;

        /*!
            \brief Check if buffer is at capacity.
            Equivalent to count() == capacity()
            \return
        */
        bool isFull() const;

        /*!
            \brief Checks if a tile is currently buffered
            \return
        */
        bool hasTile(uint32_t tileno) const;

        /*!
            \brief Changes the buffer's tile capacity.
            The buffer size will not exceed this amoutn and purge()
            may be used to free up room for new tiles.

            \param capacity Maximum number of tiles to buffer.
            \return
        */
        void setCapacity(uint32_t capacity);

    protected:
        /*!
            \brief Returns a map of all tiles in the buffer where the key is square
            of the distance from the referenceTile().
            NOTE:
                Key are arragned in ascending order, so that the closest
                tile is first in the map.
            \return
        */
        virtual DistanceTileMMapPtr purgeListFarthest() const;

        /*!
            \brief Returns a map of all tiles in the buffer where the key
            is specifies the number of rings away from the referenceTile().
            NOTE:
                Key are arragned in ascending order, so that the closest
                tile is first in the map.
            \return
        */
        virtual DistanceTileMMapPtr purgeListFarthestRing() const;

    private:
        TileVector(const TileVector& vec);
        /*!
            \brief Returns a map of all tiles in the buffer where the key is the most recent access pseudo timestamp.
            \return
        */
        DistanceTileMMapPtr purgeListOldest() const;
        void stampTile(uint32_t tileno) const;
        void removeTile(uint32_t tileno);
        void insertTile(uint32_t tileno, RawImage image, TileState state, bool isReferenceForPurge);
        uint32_t purgeFromEnd(const DistanceTileMMapPtr purgeList, const uint32_t purgeCount);
        uint32_t purgeFromBeginning(const DistanceTileMMapPtr purgeList, const uint32_t purgeCount);

    private:
        TileBuffer      _buffer;                ///< the tile buffer
        uint32_t        _capacity,              ///< maximum size of buffer
                        _reference;             ///< the reference tile for PurgeFarthestFirst
        PurgeMode       _purgeMode;             ///< purge mode
        TileExceptionSetPtr _purgeExceptions;
        mutable uint32_t
                        _accessStamp;           ///< access stamp
        mutable TileTimeMap
                        _tileAccessStamp;       ///< access stamp for each time in buffer



	};

}   // namespace sedeen
}   // namespace image
#endif // ifdef SEDEEN_IMAGE_TILEVECTOR_H


