// Primary header
#include "image/io/ImageOpenHandler.h"

#include "image/io/ImageData.h"

namespace sedeen {
namespace image {

ImageOpenHandler::~ImageOpenHandler() {}

boost::shared_ptr<ImageData> ImageOpenHandler::open(const std::string& filename) const {
  // Translate to a shared pointer
  std::auto_ptr<ImageData> imageData = doOpen(filename);

  return boost::shared_ptr<ImageData>(imageData);
}

QStringList ImageOpenHandler::fileFilter() const {
  return getFilter();
}

ImageOpenHandler::ImageOpenHandler() {}

} // end namespace image
} // end namespace sedeen
