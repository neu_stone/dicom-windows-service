#ifndef SEDEEN_SRC_IMAGE_IO_TILEDIMAGE_H
#define SEDEEN_SRC_IMAGE_IO_TILEDIMAGE_H

// System includes
#include <QtGui>
#include <boost/make_shared.hpp>
#include <boost/scoped_ptr.hpp>
#include <memory>

// User includes
#include "image/io/ImageData.h"
#include "image/io/QueueManager.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {

class ImageAccessor;
class SelectiveDecoder;

/// Concrete implementation of Image for selectively decoded images.
//
///
class SEDEEN_IMAGE_API TiledImage : public ImageData {
  Q_OBJECT;
 public:
  TiledImage(std::auto_ptr<SelectiveDecoder> dec);

  virtual ~TiledImage();

 protected:
  TiledImage(const TiledImage& tiledImage);

 private:
  /// Disable assignment
  TiledImage& operator=(const TiledImage&);

  virtual boost::shared_ptr<ImageData> doClone() const;

  virtual Size doGetSize() const;

  virtual SizeF doGetPixelSize() const;

  virtual void doCancelRequest(const ImageAccessor& client);

  virtual std::string doGetFilename() const;

  virtual boost::uint64_t doSetBufferCapacity(boost::uint64_t bytes);

  virtual boost::uint64_t doGetBufferCapacity() const;

  virtual bool doIsBufferCapacityFixed() const;

  virtual boost::uint64_t doGetBufferSize() const;

  virtual boost::uint64_t doBufferSizeRequired(int width, int height) const;

  virtual RawImage doBufferImage(double scale) const;

  virtual void doRequestRegion(const ImageAccessor& client,
                               const DecodeRequestPtr& request,
                               Priority priority);

  virtual SelectiveDecoder* getSelectiveDecoder() const;

  virtual const ImageParser& getImageParser() const;

  /// Transmit the current request to the decoder.
  //
  /// This function is reentrant, but not thread-safe. The class is made
  /// thread-safe by locking the functions that call processQueue().
  void processQueue();

 private slots:
  /// Catch signal from the SelectiveDecoder.
  //
  /// These signals are Qt::DirectConnections, meaning they are all processed
  /// sequentially in the receiving thread. It will not perform simultaneous
  /// writes to the client classes, but semaphors will be needed to protect
  /// reads to the QueueManager.
  void decoderFinished(int percent, int id);

  void decoderError(const std::string& message);

 signals:
  void error(std::string message);

 private:
  /// The associated selective decoder.
  std::auto_ptr<SelectiveDecoder> m_dec;

  /// Used to control access to the queue.
  QMutex m_mutex;

  /// Queue manager.
  QueueManager m_queue;
}; // class TiledImage

} // namespace image
} // namespace sedeen

#endif
