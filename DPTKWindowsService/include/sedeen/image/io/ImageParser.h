#ifndef SEDEEN_SRC_IMAGE_IO_IMAGEPARSER_H
#define SEDEEN_SRC_IMAGE_IO_IMAGEPARSER_H

// System headers

// User headers
#include "global/UnitDefs.h"
#include "global/geometry/SizeF.h"
#include "image/DllApi.h"
#include "image/io/ImageIOTypes.h"
#include "image/buffer/RawImage.h"

namespace sedeen {
namespace image {

class ImagePyramid;

  ///   \class ImageParser
  /// \brief Opens an image file and determines its properties.
  /// \note Subclasses, should emit exception of type FileAccessError or InvalidImage on failure during construction.
  /// This also ensures that all parsers are valid objects at all times.
class SEDEEN_IMAGE_API ImageParser {
public:
  /// \brief Destructor
  virtual ~ImageParser();

    /// \brief Gets the image pyramid.
    ImagePyramidPtr pyramid() const;

    /// \brief Returns TRUE if the file contains a slide label and FALSE otherwise. see labelImage()
    bool hasLabelImage() const;

    /// \brief Returns TRUE if the file contains a slide macro image and FALSE otherwise. see macroImage()
    bool hasMacroImage() const;

    /// \brief Returns TRUE if file contains a dedicated thumbnail image and FALSE otherwise. see thumbnailImage()
    bool hasThumbnailImage() const;

    /// \brief Returns TRUE if pixel size information is available in the file and FALSE otherwise.
    bool hasPixelSpacing() const;

    /// \brief Returns TRUE if magnification value or pixel size is available in the file and FALSE otherwise. see magnification()
    bool hasMagnification() const;

    /// \brief Gets the filename of the open file.
    std::string filename() const;

    /// \brief Gets the image pixel size or zero if not specified in metric units. see hasPixelSpacing()
    /// \note It's technically possible to have a pixel size of 0 specified in the file.
    SizeF pixelSize() const;

    /// \brief Returns the number of channels in the image.
    int components() const;

    /// \brief Gets the number of bits per pixels.
    int bpp() const;

    /// \brief Gets the number of bits per channel. In some cases bits per channel may not be equal.
    int bpc(int comp) const;

    /// \brief Retruns the apparent objective magnification or zero if the value is not specified in the file.
    double magnification() const;

    /// \brief Gets name of the liberary or softare that created the file.
    std::string producer() const;

    /// \brief Returns a decripttion of the images color format.
    Color color() const;

    /// \brief Returns a description of the compression format.
    std::string compression() const;

    /// \brief Returns the description of the image if one is available.
    std::string description() const;

    /// \brief Returns the slide's label image or a NULL image is one is not available. see hasLabelImage()
    RawImage labelImage() const;

    /// \brief Returns the thumbnail of the image or a NULL image is one is not available. see hasThumbnailImage()
    RawImage thumbnailImage() const;

    /// \brief Returns the slide macro image or a NULL image is one is not available. see hasMacroImage()
    RawImage macroImage() const;

  protected:
    /// \brief Constructor
    ImageParser();

  private:
    virtual bool doGetHasLabelImage() const = 0;
    virtual bool doGetHasMacroImage() const = 0;
    virtual bool doGetHasThumbnailImage() const = 0;

    virtual std::string doGetFilename() const = 0;
    virtual std::string doGetProducer() const = 0;
    virtual Color doGetColor() const = 0;
    virtual std::string doGetCompression() const = 0;
    virtual std::string doGetDescription() const = 0;

    virtual SizeF doGetPixelSize() const = 0;
    virtual int doGetComponents() const = 0;
    virtual int doGetBpp() const = 0;
    virtual int doGetBpc(const int comp) const = 0;
    virtual double doGetMagnification() const = 0;

    virtual RawImage doGetLabelImage() const = 0;
    virtual RawImage doGetThumbnailImage() const = 0;
    virtual RawImage doGetMacroImage() const = 0;

    virtual ImagePyramidPtr doGetPyramid() const = 0;

private:
    // do not allow copying
    // because the shared resource (i.e file) management can be tricky
    ImageParser(const ImageParser&);
    ImageParser& operator= (const ImageParser&);
};

  /// \relates ImageParser
  /// \brief Calculates the magnificaiton values based on a given 20X equivalent pixel size.
  /// \note Calculates magnification only when pixelSize is isotropic and non-zero, otherwise returns 0
/// \param pixelSize
  /// \param twenty_x_size The default assumes one microns per pixel for 20X apparent magnification.
  /// \return magnification value or 0 on error
SEDEEN_IMAGE_API
double calculateDefaultMagnification(const SizeF &pixelSize,
                                     double twenty_x_size = 0.5*length::um);

}   // namespace image
}   // namespace sedeen

#endif // SEDEEN_SRC_IMAGE_IO_IMAGEPARSER_H

