#ifndef SEDEEN_SRC_IMAGE_IO_TILEPYRAMID_H
#define SEDEEN_SRC_IMAGE_IO_TILEPYRAMID_H

// System headers
#include <memory>
#include <boost/scoped_array.hpp>

// User headers
#include "image/io/TileMatrix.h"
#include "image/io/ImageIOTypes.h"
#include "image/DllApi.h"

namespace sedeen {

class Rect;

namespace image {

class Tile;

/*!
  \class TilePyramid
  \brief Provides storage for tiles in an multi-resolutional image pyramid.

  Tiles are buffered upto to the specified capacity() and old tiles are
  purged so that capacity() is not exceeded.
*/
class SEDEEN_IMAGE_API TilePyramid {
 public:
  /*!
    \
  */
  typedef TileVector::TileTimeMapItrConst TileItrConst;

  typedef TileVector::TileExceptionSet    TileExceptionSet;
  typedef TileVector::TileExceptionSetPtr TileExceptionSetPtr;

  TilePyramid(const ImagePyramidPtr &pyramid, PurgeMode mode = PurgeFarthestFirst);
  ~TilePyramid();

  ImagePyramidPtr pyramid() const;
  uint32_t capacity(uint32_t level) const;
  uint32_t size(uint32_t level) const;
  uint32_t count() const;
  bool isFull(uint32_t level) const;
  void purge(uint32_t level, uint32_t count);

  /*!
    \copydoc TileVector::begin()
    \param level
  */
  TileItrConst begin(uint32_t level) const;

  /*!
    \copydoc TileVector::end()
    \param level
  */
  TileItrConst end(uint32_t level) const;

  bool hasTile(uint32_t level, uint32_t tileno) const;
  bool hasTile(uint32_t level, uint32_t row, uint32_t col) const;

  Rect span(uint32_t level, uint32_t tileno) const;
  Rect span(uint32_t level, uint32_t row, uint32_t col) const;

  uint32_t purgeReferenceTile(uint32_t level) const;
  void setPurgeReferenceTile(uint32_t level, uint32_t tileno);

  const Tile& tile(uint32_t level, uint32_t tileno) const;
  const Tile& tile(uint32_t level, uint32_t row, uint32_t col) const;

  void setPurgeExceptions(uint32_t level, TileExceptionSetPtr exceptions);

  void setTile(uint32_t level, uint32_t tileno, const RawImage &image,
               TileState state);
  void setTile(uint32_t level, uint32_t row, uint32_t col,
               const RawImage &image, TileState state);

  void setCapacity(uint32_t screenWidth, uint32_t screenHeight,
                   uint32_t multiple);
  void setCapacity(uint32_t level, uint32_t numtiles);

 private:
  /// Type of the arrangment of layers of tiles into a pyramid.
  typedef std::vector<boost::shared_ptr<TileMatrix> > TilePyramidType;

  ImagePyramidPtr  _pyramid;

  /// Arrangement of layers of tiles into a pyramid.
  TilePyramidType _buffer;
}; // class TilePyramid

}   // namespace sedeen
}   // namespace image

#endif // SEDEEN_SRC_IMAGE_IO_TILEPYRAMID_H
