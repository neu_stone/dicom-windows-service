#ifndef SEDEEN_SRC_IMAGE_IO_REQUESTBYSIZE_H
#define SEDEEN_SRC_IMAGE_IO_REQUESTBYSIZE_H

// User headers
#include "global/geometry/Rect.h"
#include "global/geometry/Size.h"
#include "global/geometry/SizeF.h"
#include "image/io/DecodeRequest.h"
#include "image/buffer/RawImageTypes.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {

/// Descibres a region to be decoded. To be used by SelectiveDecoder.
class SEDEEN_IMAGE_API RequestBySize : public DecodeRequest {
 public:
  typedef DecodeRequest::TileVec          TileVec;
  typedef DecodeRequest::TileVecPtr       TileVecPtr;
  typedef DecodeRequest::TileVecConstPtr  TileVecConstPtr;

  /// Constructs a NULL request
  RequestBySize();

  /// Defines a request to extract \a region from full resolution image grid and
  /// scaled to \a size pixels (possibly altering the aspect ratio).
  //
  /// For the case of multi-resolution images, a resolution level is
  /// automatically selected which optimizes the process, see resolution().
  /// However, the request can be generated from any resolution via the
  /// \a desired_resolution parameter.
  ///
  /// \param pyramid
  ///
  /// \param region
  /// the area to be decoded in full resolution coordinates
  ///
  /// \param size
  /// dimensions of the request in pixels after scaling
  ///
  /// \param cache
  /// indicates whether the areas surrounding \a region should also
  /// be cached.
  ///
  /// \param desired_resolution
  /// -1 auto-selects the best resolution level for this request (i.e closest to
  /// the required scale), otherwise extracts region from given resolution
  /// level. If \a desired_resolution is not within the range resolutions given
  /// by \a pyramid, a debug assertion is triggered and the resolution defaults
  /// to the auto selected value.
  RequestBySize(const ImagePyramidPtr &pyramid, const Rect &region,
                const Size &size, const bool cache = false,
                const int32_t desired_resolution = -1);

  /// \brief Assignment operator
  RequestBySize& operator=(const RequestBySize& rhs);

 private:
  DecodeRequestPtr doGetClone() const;
  bool doGetIsNull() const;
  Rect doGetRegion(int32_t level) const;
  Size doGetSize() const;
  SizeF doGetScale() const;
  int32_t doGetResolution() const;
  bool doGetCache() const;
  ImagePyramidPtr doGetPyramid() const;
  TileVecConstPtr doGetTiles() const;

 private:
  ImagePyramidPtr _pyramid;
  Rect        _region;
  Size        _size;
  SizeF       _scale;
  int32_t     _resolution;
  bool        _cache;
  TileVecPtr  _tiles;
};

/// Defines a request to extract \a region from full resolution image grid and
/// scaled to \a height pixels (perseves the aspect ratio).
//
/// \relates RequestBySize
///
/// \copydetails RequestBySize
SEDEEN_IMAGE_API
DecodeRequestPtr requestByHeight(const ImagePyramidPtr &pyramid,
                                 const Rect &region, const int32_t height,
                                 const bool cache = false,
                                 const int32_t desired_resolution = -1);

/// Defines a request to extract \a region from full resolution image grid and
/// scaled to \a width pixels (perseves the aspect ratio).
//
/// \relates RequestBySize
///
/// \copydetails RequestBySize
SEDEEN_IMAGE_API
DecodeRequestPtr requestByWidth(const ImagePyramidPtr &pyramid,
                                const Rect &region, const int32_t width,
                                const bool cache = false,
                                const int32_t desired_resolution = -1);

}   // namespace image
}   // namespace sedeen

#endif // SEDEEN_SRC_IMAGE_IO_REQUESTBYSIZE_H
