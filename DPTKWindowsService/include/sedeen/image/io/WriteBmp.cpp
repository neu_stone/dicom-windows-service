// Primary header
#include "image/io/WriteBmp.h"

// User headers
#include "image/io/ImageAccessor.h"

using namespace sedeen::image;

WriteBmp::WriteBmp(QObject* parent) : ImageWriter(parent) {}

WriteBmp::~WriteBmp() {}

void WriteBmp::doWrite(ImageAccessor imageAccessor, const QString& filename,
                       const QRect& inputRegion, double zoom) const {}

QString WriteBmp::getDefaultExtension() const {
  return QString("bmp");
}

QString WriteBmp::getFileFilter() const {
  return QString("Bitmap (*.bmp)");
}

QWidget* WriteBmp::buildOptionWidget() const {
  return 0;
}
