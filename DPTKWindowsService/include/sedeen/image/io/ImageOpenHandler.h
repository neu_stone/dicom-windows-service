#ifndef SEDEEN_SRC_IMAGE_IO_IMAGEOPENHANDLER_H
#define SEDEEN_SRC_IMAGE_IO_IMAGEOPENHANDLER_H

// System headers
#include <QtCore>
#include <boost/shared_ptr.hpp>

#include "image/DllApi.h"

namespace sedeen {
namespace image {

// Forward declarations
class ImageData;

/// Base of the image file reader hierarchy.
//
/// This family of classes employs a chain-of-responsibility pattern. The openers should be installed in reverse order of specificity, i.e. start with the most general, catch-all sort of classes, and move to the most specialized. This way, the specialized classes will get first crack at opening files.
///
/// If a file makes its way through the entire chain without a handler accepting it, the base class throws an exception.
class SEDEEN_IMAGE_API ImageOpenHandler {
 public:
  virtual ~ImageOpenHandler();

  /// Attempts to open the image at the specified location.
  //
  /// \param filename
  /// Path and filename of the file to be opened. Existence of the file will be checked at this stage.
  ///
  /// Returns a valid auto_ptr on success and a null auto_ptr on failure.
  boost::shared_ptr<ImageData> open(const std::string& filename) const;

  QStringList fileFilter() const;

 protected:
  ImageOpenHandler();

 private:
  /// Attempt to open the given file.
  //
  /// \param filename
  /// Path and filename of the file to be opened.
  ///
  /// \return
  /// On successful opening, a valid ImageData auto_ptr. Otherwise, a null auto_ptr.
  virtual std::auto_ptr<ImageData> doOpen(
      const std::string& filename) const = 0;

  /// Return a list of suffixes accepted by this handler.
  //
  /// The filter expressions should follow the Qt4 convention. See [http://doc.qt.nokia.com/4.7/qfiledialog.html#getOpenFileName].
  virtual QStringList getFilter() const = 0;
}; // end class ImageOpenHandler

} // end namespace image
} // end namespace sedeen

#endif
