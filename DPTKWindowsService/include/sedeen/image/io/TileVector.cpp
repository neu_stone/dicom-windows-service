// Main header
#include "image/io/TileVector.h"

// System headers
#include <iostream>
#include <cassert>
#include <exception>
#include <limits>
#include <stdexcept>

#include "global/Debug.h"

// User headers


namespace sedeen
{
namespace image
{

namespace{
    const uint32_t VectorCapacity = 30;         // initial vector size
    const double MultiplePurgePercent = .05;    // Purge 5% of tiles at once
    const uint32_t MultiplePurgeMax = 10;       // Do not purge more that 10 at once
}

TileVector::TileVector(uint32_t count, PurgeMode mode) :
_buffer( TileBuffer(count,Tile()) ),
_capacity(count),
_reference(0),
_purgeMode(mode),
_purgeExceptions(new TileExceptionSet()),
_accessStamp(0),
_tileAccessStamp()
{
}

TileVector::~TileVector()
{
}

uint32_t TileVector::size() const
{
    return _buffer.size();
}

bool TileVector::isFull() const
{
    assert( count() <= capacity() );
    return count() == capacity();
}

uint32_t TileVector::count() const
{
    return _tileAccessStamp.size();
}

uint32_t TileVector::capacity() const
{
    return _capacity;
}

void TileVector::setCapacity(uint32_t capacity)
{
    assert( count() <= size() );
    if( count() > capacity )
    {
        // Purge as many tiles as possible
        int purge_count = count() - capacity;
        purge( purge_count );
        //int purged = purge( purge_count );
    }
    _capacity = capacity;
}

const Tile& TileVector::operator[](uint32_t tileno) const
{
    assert( tileno < size() );
    return _buffer[tileno];
}

const Tile& TileVector::operator()(uint32_t tileno) const
{
    return operator[](tileno);
}

TileVector::TileTimeMapItrConst TileVector::begin() const
{
    return _tileAccessStamp.begin();
}

TileVector::TileTimeMapItrConst TileVector::end() const
{
    return _tileAccessStamp.end();
}

void TileVector::setPurgeMode(PurgeMode mode)
{
    _purgeMode = mode;
}

PurgeMode TileVector::purgeMode() const
{
    return _purgeMode;
}

uint32_t TileVector::purgeReferenceTile() const
{
    return _reference;
}

void TileVector::setPurgeReferenceTile(uint32_t tileno)
{
    assert( tileno < size() );
    _reference = std::min( tileno, size() );
}

void TileVector::setPurgeExceptions(TileExceptionSetPtr exceptions)
{
    _purgeExceptions = exceptions;
}

void TileVector::setTileImage(uint32_t tileno, RawImage image, TileState state, bool isReferenceForPurge)
{
    assert( tileno < size() );

    // send warnings
    if( image.isNull() && TileEmpty != state )
    {
        debug::warning("ignoring invalid tile state for NULL image.",
                        __FUNCTION__, __LINE__);
    }
    if( !image.isNull() && TileEmpty == state )
    {
        debug::warning("tile state cannot be Empty when image is NOT NULL, defaulting to Fuzzy.",
          __FUNCTION__, __LINE__);
        state = TileFuzzy;
    }

    // deal with NULL image
    if( image.isNull() )
    {
        removeTile(tileno);
    }
    else
    {
        insertTile(tileno, image, state, isReferenceForPurge);
    }
}

const Tile& TileVector::tile(uint32_t tileno) const
{
    if( tileno >= size() )
        throw std::out_of_range("");

    // modify the access time for this tile
    if( hasTile(tileno) )
        stampTile(tileno);

    // return tile
    return _buffer[tileno];
}

void TileVector::removeTile(uint32_t tileno)
{
    assert( tileno < size() );

    if( hasTile(tileno) )
    {
        _buffer[tileno].clear();
        _tileAccessStamp.erase(tileno);
    }
}

void TileVector::insertTile(uint32_t tileno, RawImage image, TileState state, bool isReferenceForPurge)
{
    assert( !image.isNull() );
    assert( TileEmpty != state );
    assert( tileno < size() );

    if( tileno >= size() )
        throw std::out_of_range("");

    // set reference tile
    if( isReferenceForPurge || count() == 0 )
        setPurgeReferenceTile(tileno);

    // update buffer state
    if( !hasTile(tileno) )
    {
        // purge
        if( isFull() ) {
            int purged = purge( std::min(    MultiplePurgeMax,
                                (uint32_t)std::max(1., count()*MultiplePurgePercent) ) );
            if( 0 == purged ) {
              debug::warning("Insertion failed. Unable to purge full buffer (check purge exceptions)", __FUNCTION__, __LINE__);
              return;
            }
        }
    }
    else
        assert( _tileAccessStamp.count(tileno) == 1 );

    // update image
    _buffer[tileno].setImage(image, state);

    // modify the access time for this tile
    stampTile(tileno);
}

bool TileVector::hasTile(uint32_t tileno) const
{
    assert( tileno < size() );
    assert( !(   _tileAccessStamp.count(tileno) ^
                !_buffer[tileno].image().isNull()) );

    return !_buffer[tileno].image().isNull();
}

void TileVector::stampTile(uint32_t tileno) const
{
    assert( tileno < size() );

    // make sure stamp does not overflow
    if( std::numeric_limits<uint32_t>::max() == _accessStamp )
    {
        // Sort the tiles in buffer by the time stamps
        TileTimeMap sorted;
        TileTimeMap::iterator it = _tileAccessStamp.begin();
        while( it != _tileAccessStamp.end() )
        {
            sorted.insert( TileTimeMap::value_type(it->second,it->first) );
            ++it;
        }

        // reset the time stamps to 0 based values
        _accessStamp = 0;
        for( it = sorted.begin(); it != sorted.end(); ++it )
            _tileAccessStamp[it->second] = _accessStamp++;
    }

    // modify the access time for this tile
   _tileAccessStamp[tileno] = _accessStamp++;
}

uint32_t TileVector::purge(const uint32_t purgeCount)
{
    //nothing to do
    if( 0 == purgeCount ||
        0 == count() )
        return 0;

    // Get ordered purge list
    bool purgefromstart;
    DistanceTileMMapPtr purgeList;
    switch(_purgeMode)
    {
    case PurgeOldestFirst:
        purgeList = purgeListOldest();
        purgefromstart = true;
        break;
    case PurgeFarthestFirst:
        purgeList = purgeListFarthest();
        purgefromstart = false;
        break;
    case PurgeFarthestRingFirst:
        purgeList = purgeListFarthestRing();
        purgefromstart = false;
        break;
    default:
        assert(false);
        return 0;
    }
    assert( purgeList->size() == count() );

    // Purge tiles
    if( purgefromstart )
        return purgeFromBeginning(purgeList, purgeCount);
    else
        return purgeFromEnd(purgeList, purgeCount);
}

uint32_t TileVector::purgeFromBeginning(const DistanceTileMMapPtr purgeList, const uint32_t purgeCount)
{
    // remove the oldest tiles
    DistanceTileMMapItrConst it = purgeList->begin();
    uint32_t purged = 0;
    while(  count() &&
            purged < purgeCount &&
            it != purgeList->end() )
    {
        if( _purgeExceptions->count(it->second) == 0 )
        {
            removeTile(it->second);
            purged++;
        }
        ++it;
    }
    return purged;
}

uint32_t TileVector::purgeFromEnd(const DistanceTileMMapPtr purgeList, const uint32_t purgeCount)
{
    // remove the oldest tiles
    DistanceTileMMap::const_reverse_iterator it = purgeList->rbegin();
    uint32_t purged = 0;
    while(  count() &&
            purged < purgeCount &&
            it != purgeList->rend() )
    {
        if( _purgeExceptions->count(it->second) == 0 )
        {
            removeTile(it->second);
            purged++;
        }
        ++it;
    }
    return purged;
}

TileVector::DistanceTileMMapPtr TileVector::purgeListOldest() const
{
    DistanceTileMMapPtr sorted(new DistanceTileMMap);

    // Sort the tiles in buffer by the time stamps
    TileTimeMapItrConst it = begin();
    while( it != end() )
    {
        sorted->insert( DistanceTileMMap::value_type(it->second,it->first) );
        ++it;
    }
    return sorted;
}

TileVector::DistanceTileMMapPtr TileVector::purgeListFarthest() const
{
    DistanceTileMMapPtr sorted(new DistanceTileMMap);

    // Sort the tiles in buffer by the time stamps
    TileTimeMapItrConst it = begin();
    while( it != end() )
    {
      uint32_t dist = std::abs( int(purgeReferenceTile()) - int(it->first) );
        sorted->insert( DistanceTileMMap::value_type(dist,it->first) );
        ++it;
    }
    return sorted;
}

TileVector::DistanceTileMMapPtr TileVector::purgeListFarthestRing() const
{
    return purgeListFarthest();
}

}   // namespace sedeen
}   // namespace image
