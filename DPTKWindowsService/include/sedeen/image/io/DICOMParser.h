#ifndef SEDEEN_SRC_IMAGE_IO_DICOMPARSER_H
#define SEDEEN_SRC_IMAGE_IO_DICOMPARSER_H

// System headers
#include <vector>
#include <memory>
#include <boost/cstdint.hpp>

// user includes
#include "image/io/ImageParser.h"
#include "image/io/TiffCrop.h
#include "image/DllApi.h"

namespace sedeen
{
namespace image
{
    class DcmFileFormat;
    class DcmDataSet;

    /*!
        Known TIFF Formats.
    */
    enum DICOMProducer {
        DICOMByUnknown      = 0x0200,
    };

    /*!
        \class DICOMParser
        \brief Looks at a DICOM file and determins its properties such
        as its:
            - producer
            - ImagePyramid
            - directories used to make the ImagePyramid
            - a DICOM dataset

            On failure a std::runtime_error exception is thrown
            of type FileAccessError or InvalidImage.
     */
    class SEDEEN_IMAGE_API DICOMParser : public ImageParser
    {
    public:

        /*!
            \brief Attempts to open and parse \a filename.
            Throws an InvalidImage or FileAccessError exception on error.

            \param filename the image to open
        */
        DICOMParser(const char *filename);

        /*!
            \brief closes the open file and frees resources.
        */
        ~DICOMParser();

        const ImagePyramid& pyramid() const;

        /*!
            \brief Gets the dataset.
            \return
        */
        const DcmDataSet& dataSet() const;

       std::string filename() const;

        virtual boost::uint16_t magnification(bool *defaultValue = 0) const;

        boost::uint32_t producer() const;

        virtual std::string compression() const;

        virtual std::string description() const;

       bool hasLabel() const;

       bool hasThumbnail() const;

       QImage label() const;

       QImage thumbnail() const;

    private:
        // do not allow copying
        DICOMParser(const DICOMParser&);
        DICOMParser& operator= (const DICOMParser&);

    private:
        std::string _filename;
        std::auto_ptr<DcmFileFormat> _file;
    };

}   // namespace image
}   // namespace sedeen
#endif // SEDEEN_SRC_IMAGE_IO_DICOMPARSER_H
