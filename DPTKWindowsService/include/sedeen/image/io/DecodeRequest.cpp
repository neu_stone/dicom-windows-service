// Main headers
#include "image/io/DecodeRequest.h"

// User headers
#include "image/io/ImagePyramid.h"
#include "global/Debug.h"
#include "global/geometry/Point.h"
#include "global/geometry/Rect.h"
#include "global/geometry/Size.h"
#include "global/geometry/SizeF.h"

// system headers
#include <cassert>
#include <cmath>
#include <iostream>
#include <boost/make_shared.hpp>

namespace sedeen {
namespace image {
namespace {

/// gets a vector an empty vector
DecodeRequest::TileVecPtr getEmptyTileList() {
  return boost::make_shared<DecodeRequest::TileVec>();
}

/// gets a vector of tile numbers in the range (x_min, y_min) to (x_max, y_max)
DecodeRequest::TileVecPtr getTileList(const Rect& region,
                                      const ImagePyramid& pyramid,
                                      const int32_t level) {
  // Create vector of proper length
  DecodeRequest::TileVecPtr vec = boost::make_shared<DecodeRequest::TileVec>();
  vec->reserve(area(region));

  // The number of columns in the image.
  const int32_t bufw = pyramid.cols(level);

  // add tiles to the vector
  for (int32_t row = 0; row < region.height(); ++row) {
    int32_t tileno = (region.y() + row) * bufw + region.x();
    for (int32_t col = 0; col < region.width(); ++col, ++tileno) {
      assert(tileno < pyramid.count(level));
      vec->push_back(tileno);
    }
  }
  return vec;
}

} // namespace

DecodeRequest::DecodeRequest() {}

DecodeRequest::~DecodeRequest() {}

DecodeRequest::DecodeRequest(const DecodeRequest& /*decode_request*/) {}

DecodeRequestPtr DecodeRequest::clone() const {
  return doGetClone();
}

bool DecodeRequest::isNull() const {
  return doGetIsNull();
}

Rect DecodeRequest::region(int32_t level) const {
  return doGetRegion(level);
}

int32_t DecodeRequest::resolution() const {
  return doGetResolution();
}

bool DecodeRequest::cache() const {
  return doGetCache();
}

Size DecodeRequest::size() const {
  return doGetSize();
}

SizeF DecodeRequest::scale() const {
  return doGetScale();
}

ImagePyramidPtr DecodeRequest::pyramid() const {
  return doGetPyramid();
}

DecodeRequest::TileVecConstPtr DecodeRequest::tiles() const {
  return doGetTiles();
}

bool operator==(const DecodeRequest& lhs, const DecodeRequest& rhs) {
  return (lhs.resolution() == rhs.resolution()) &&
      (lhs.region(lhs.resolution()) == rhs.region(rhs.resolution())) &&
      (lhs.size() == rhs.size()) && (lhs.cache() == rhs.cache()) &&
      (lhs.tiles() == rhs.tiles());
}

bool operator!=(const DecodeRequest& lhs, const DecodeRequest& rhs) {
  return !(lhs==rhs);
}

std::ostream& operator<< (std::ostream& stream, const DecodeRequest& r) {
  const Rect rect = r.region(0);
  stream << "DecodeRequest (0x" << std::hex << &r << std::dec << ")"
         << "\n\t Region: " << rect
         << "\n\t Cache = " << std::boolalpha << r.cache()
         << "\n\t Size = " << r.size().width() << " x " << r.size().height()
         << "\n\t Tiles: (" << r.tiles()->size() << ") ";
  for (int i = 0; i < (int)r.tiles()->size(); ++i) {
    stream << r.tiles()->at(i) << ", ";
  }
  stream << std::endl<< std::endl;
  return stream;
}

DecodeRequest::TileVecPtr tileListSpanningRegion(
    Rect region,
    const int32_t region_level,
    const int32_t desired_level,
    const ImagePyramid& pyramid) {
  assert(region_level < pyramid.levels());
  assert(desired_level < pyramid.levels());

  // Only examine the region overlapping the valid image area
  region = intersection(region, pyramid.rect(region_level));

  if (region_level != desired_level) {
    // Project the region onto the target resolution level
    region = mapRegionToAlternateResolution(region, region_level, desired_level,
                                            pyramid);
  }

  // Project the region into tile-space
  Size offset_size = pyramid.tileOffset(region_level);
  Point offset(-offset_size.width(), -offset_size.height());
  region = project(region, Rect(offset, pyramid.tileSize()),
                   Rect(Point(), Size(1, 1)));

  // Get the touched tiles
  return getTileList(region, pyramid, desired_level);
}

Rect mapRegionToAlternateResolution(Rect region,
                                    const int32_t region_res,
                                    const int32_t final_res,
                                    const ImagePyramid& pyramid) {
  debug::assert_x(region_res < pyramid.levels() && final_res < pyramid.levels(),
                  "Invalid resolution level(s) specified",
                  __FUNCTION__,
                  __LINE__);

  if (region_res != final_res) {
    Rect source_space(Point(), pyramid.size(region_res));
    Rect target_space(Point(), pyramid.size(final_res));
    region = project(region, source_space, target_space);
  }
  return region;
}

int32_t getNearestValidResoution(const ImagePyramid& pyramid,
                                 const int32_t level) {
  return std::max<int32_t>(0, std::min<int32_t>(level, pyramid.levels() - 1));
}

} // namespace image
} // namespace sedeen

