#ifndef SEDEEN_SRC_IMAGE_IO_TEST_MOCKIMAGEPYRAMID_H
#define SEDEEN_SRC_IMAGE_IO_TEST_MOCKIMAGEPYRAMID_H

// External library headers
#include "gmock/gmock.h"

// User headers
#include "image/io/ImagePyramid.h"

namespace sedeen {
namespace image {

class MockImagePyramid : public ImagePyramid {
 public:
  MockImagePyramid(const std::vector<Size>& imageSize, const Size& tileSize,
                   const std::vector<Size>& tileOffsets);

  MOCK_CONST_METHOD1(doGetLevel, int32_t(double scale));

  /// By default, this function expects never to be called.
  MOCK_CONST_METHOD0(doGetClone, ImagePyramidPtr());
}; // class MockImagePyramid

} // namespace image
} // namespace sedeen

#endif
