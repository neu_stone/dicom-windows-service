#ifndef SEDEEN_SRC_IMAGE_IO_TEST_MOCKIMAGEPARSER_H
#define SEDEEN_SRC_IMAGE_IO_TEST_MOCKIMAGEPARSER_H

// External library headers
#include <boost/shared_ptr.hpp>
#include "gmock/gmock.h"

// User headers
#include "image/io/ImageParser.h"

namespace sedeen {
namespace image {

class MockImagePyramid;

class MockImageParser : public ImageParser {
 public:
  MockImageParser(const std::vector<Size>& imageSize, const Size& tileSize,
                  const std::vector<Size>& tileOffsets);
  MOCK_CONST_METHOD0(doGetHasLabelImage, bool());
  MOCK_CONST_METHOD0(doGetHasMacroImage, bool());
  MOCK_CONST_METHOD0(doGetHasThumbnailImage, bool());

  MOCK_CONST_METHOD0(doGetFilename, std::string());
  MOCK_CONST_METHOD0(doGetProducer, std::string());
  MOCK_CONST_METHOD0(doGetColor, Color());
  MOCK_CONST_METHOD0(doGetCompression, std::string());
  MOCK_CONST_METHOD0(doGetDescription, std::string());

  MOCK_CONST_METHOD0(doGetPixelSize, SizeF());
  MOCK_CONST_METHOD0(doGetComponents, int());
  MOCK_CONST_METHOD0(doGetBpp, int());
  MOCK_CONST_METHOD1(doGetBpc, int(const int comp));
  MOCK_CONST_METHOD0(doGetMagnification, double());

  MOCK_CONST_METHOD0(doGetLabelImage, RawImage());
  MOCK_CONST_METHOD0(doGetThumbnailImage, RawImage());
  MOCK_CONST_METHOD0(doGetMacroImage, RawImage());

  MOCK_CONST_METHOD0(doGetPyramid, ImagePyramidPtr());

  boost::shared_ptr<MockImagePyramid> m_pyramid;
}; // class MockImageParser

} // namespace image
} // namespace sedeen

#endif
