#ifndef SEDEEN_SRC_IMAGE_IO_TEST_MOCKIMAGEDATA_H
#define SEDEEN_SRC_IMAGE_IO_TEST_MOCKIMAGEDATA_H

// External library headers
#include <boost/make_shared.hpp>
#include "gmock/gmock.h"

// User headers
#include "global/geometry/Size.h"
#include "image/io/test/MockImageParser.h"
#include "image/io/ImageAccessor.h"
#include "image/io/ImageData.h"
#include "image/io/ImageParser.h"

namespace sedeen {
namespace image {

class MockImageData : public ImageData {
 public:
  MockImageData(const std::vector<Size>& imageSize, const Size& tileSize,
                const std::vector<Size>& tileOffsets);
  MOCK_CONST_METHOD0(doClone, boost::shared_ptr<ImageData>());
  MOCK_CONST_METHOD0(doGetSize, Size());
  MOCK_CONST_METHOD0(doGetPixelSize, SizeF());
  MOCK_CONST_METHOD0(doGetFilename, std::string());
  MOCK_CONST_METHOD1(doBufferImage, RawImage(double scale));
  MOCK_METHOD1(doSetBufferCapacity, boost::uint64_t(boost::uint64_t));
  MOCK_CONST_METHOD0(doGetBufferCapacity, boost::uint64_t());
  MOCK_CONST_METHOD0(doIsBufferCapacityFixed, bool());
  MOCK_CONST_METHOD2(doBufferSizeRequired, boost::uint64_t(int width,
                                                           int height));
  MOCK_CONST_METHOD0(doGetBufferSize, boost::uint64_t());
  MOCK_METHOD3(doRequestRegion, void(const ImageAccessor& client,
                                     const DecodeRequestPtr& request,
                                     Priority priority));
  MOCK_METHOD1(doCancelRequest, void(const ImageAccessor& client));
  MOCK_CONST_METHOD0(getSelectiveDecoder, SelectiveDecoder*());
  MOCK_CONST_METHOD0(getImageParser, const ImageParser&());

  MockImageParser m_parser;
}; // class MockImageData

} // namespace image
} // namespace sedeen

#endif
