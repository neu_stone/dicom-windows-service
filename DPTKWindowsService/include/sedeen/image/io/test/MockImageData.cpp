#include "image/io/test/MockImageData.h"

namespace sedeen {
namespace image {

using ::testing::ReturnRef;

MockImageData::MockImageData(const std::vector<Size>& imageSize,
                             const Size& tileSize,
                             const std::vector<Size>& tileOffsets)
    : m_parser(imageSize, tileSize, tileOffsets) {
  EXPECT_CALL(*this, doClone()).Times(0);
  ON_CALL(*this, getImageParser()).WillByDefault(ReturnRef(m_parser));
  EXPECT_CALL(*this, getImageParser()).Times(::testing::AnyNumber());
}

} // namespace image
} // namespace sedeen
