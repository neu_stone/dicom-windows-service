#include "image/io/test/MockImageParser.h"

#include <boost/make_shared.hpp>

#include "image/io/test/MockImagePyramid.h"

namespace sedeen {
namespace image {

using ::testing::Return;

MockImageParser::MockImageParser(const std::vector<Size>& imageSize,
                                 const Size& tileSize,
                                 const std::vector<Size>& tileOffsets)
    : m_pyramid(boost::make_shared<MockImagePyramid>(
        imageSize, tileSize, tileOffsets)) {
  ON_CALL(*this, doGetPyramid()).WillByDefault(Return(m_pyramid));
  EXPECT_CALL(*this, doGetPyramid()).Times(::testing::AnyNumber());
}

} // namespace image
} // namespace sedeen
