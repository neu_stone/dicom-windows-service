#include "image/io/test/MockImagePyramid.h"

namespace sedeen {
namespace image {

MockImagePyramid::MockImagePyramid(const std::vector<Size>& imageSize,
                                   const Size& tileSize,
                                   const std::vector<Size>& tileOffsets)
    : ImagePyramid(imageSize, tileSize, tileOffsets) {

}

} // namespace image
} // namespace sedeen
