#include "image/io/RequestByResolution.h"

#include <cmath>
#include <vector>
#include <boost/make_shared.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>

#include "gtest/gtest.h"

#include "global/geometry/Point.h"
#include "global/geometry/Size.h"
#include "global/geometry/Rect.h"
#include "global/MathOps.h"
#include "image/tiff/TIFFPyramid.h"

namespace sedeen {
namespace image {
namespace {

const int kLevels = 6;
const int kTileSize = 2;
const int kRequestSize = 3;

std::vector<Size> getLevelSizes() {
  std::vector<Size> sizes;
  for (int i = 0; i != kLevels; ++i) {
    sizes.push_back(Size(static_cast<int>(std::pow(2.0,i+1)),
                         static_cast<int>(std::pow(2.0,i+1))));
  }

  return sizes;
}

Size getTileSize() {
  return Size(kTileSize,kTileSize);
}

Rect getRequestRect() {
  return Rect(Point(), Size(kRequestSize, kRequestSize));
}

} // namespace

class RequestByResolutionTest : public ::testing::Test {
 public:
  RequestByResolutionTest()
      : m_pyramid(boost::make_shared<TIFFPyramid>(getLevelSizes(),
                                                  getTileSize())),
        m_req(m_pyramid, getRequestRect(), 3, false) {}

 protected:
  boost::shared_ptr<TIFFPyramid> m_pyramid;
  RequestByResolution m_req;
}; // class RequestByResolutionTest

TEST_F(RequestByResolutionTest, Size) {
  EXPECT_EQ(size(getRequestRect()), m_req.size());
}

TEST_F(RequestByResolutionTest, Region) {
  EXPECT_EQ(Rect(Point(),
                 Size(kRequestSize*8, kRequestSize*8)), m_req.region(0));
  EXPECT_EQ(Rect(Point(),
                 Size(kRequestSize*4, kRequestSize*4)), m_req.region(1));
  EXPECT_EQ(Rect(Point(),
                 Size(kRequestSize*2, kRequestSize*2)), m_req.region(2));
  EXPECT_EQ(Rect(Point(), Size(kRequestSize, kRequestSize)), m_req.region(3));
  EXPECT_EQ(Rect(Point(), Size(math_ops::intDivideRoundUp(kRequestSize, 2),
                               math_ops::intDivideRoundUp(kRequestSize, 2))),
                 m_req.region(4));
  EXPECT_EQ(Rect(Point(), Size(math_ops::intDivideRoundUp(kRequestSize, 4),
                               math_ops::intDivideRoundUp(kRequestSize, 4))),
                 m_req.region(5));
}

} // namespace image
} // namespace sedeen
