#ifndef SEDEEN_SRC_IMAGE_IO_IMAGEWRITER_H
#define SEDEEN_SRC_IMAGE_IO_IMAGEWRITER_H

// System headers
#include <QtGui>

#include "image/DllApi.h"

namespace sedeen {
namespace image {

class ImageAccessor;

/// Base of all image writing utilities.
class SEDEEN_IMAGE_API ImageWriter : public QObject {
  Q_OBJECT;
 public:
  virtual ~ImageWriter();

  /// Save the image with the current set of options.
  void write(const image::ImageAccessor& imageAccessor,
             const QString& outFilename,
             const QRect& inputRegion,
             double zoom) const;

  /// Get an extension appended to the filename by default.
  QString defaultExtension() const;

  /// Get a filter for the type of files writable by this class.
  QString fileFilter() const;

  /// Build an option interface for this class.
  //
  /// This widget will be embedded in the CropWidget window.
  QWidget* options(QWidget* parent) const;

 protected:
  ImageWriter(QObject* parent = 0);

 private:
  virtual void doWrite(image::ImageAccessor imageAccessor,
                       const QString& filename,
                       const QRect& inputRegion,
                       double zoom) const = 0;

  /// Return the extension for this file type.
  virtual QString getDefaultExtension() const = 0;

  ///
  virtual QString getFileFilter() const = 0;

  /// Build an option dialog box for this class.
  //
  /// By default, returns a null pointer.
  virtual QWidget* buildOptionWidget() const;
}; // end class ImageWriter

} // end namespace crop
} // end namespace Sedeen

#endif
