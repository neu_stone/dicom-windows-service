#include "image/io/ThreadedRegionBlock.h"

#include "global/geometry/Rect.h"
#include "image/io/ImageAccessor.h"
#include "image/buffer/RawImage.h"

namespace sedeen {
namespace image {

class ThreadedRegionBlock::Private {
 public:
  Private(const ImageAccessor &accessor)
      : accessor(accessor),
        img(),
        loop() {
  }
  ImageAccessor accessor;
  RawImage img;
  QEventLoop loop;
};

ThreadedRegionBlock::ThreadedRegionBlock(const ImageAccessor &accessor,
  int resolution, const Rect &region)
    : d(new ThreadedRegionBlock::Private(accessor)) {

  // connet signals and call region()
  QObject::connect(&d->accessor, SIGNAL(imageUpdated(const RawImage&, bool)),
                  this, SLOT(imageUpdated(const RawImage&, bool)));
  image::region(d->accessor, (int)resolution, region, image::kNormal);

  // Start the loop only if the event loop iff region is not complete
  if (!isNull(d->accessor))
      d->loop.exec();
}

RawImage ThreadedRegionBlock::getImage() const {
  return d->img;
}

void ThreadedRegionBlock::imageUpdated(const RawImage& image, bool complete) {
  if (complete)  {
    d->loop.quit();
    d->img = image;
    d->accessor = ImageAccessor();
  }
}


} // namespace image
} // namespace sedeen

