// Primary header
#include "image/io/ImageOpener.h"

// System headers
#include <algorithm>

// User headers
#include "image/io/Exceptions.h"
#include "image/io/ImageData.h"
#include "image/io/ImageOpenHandler.h"

namespace sedeen {
namespace image {

ImageOpener::ImageOpener() : m_openers() {}

ImageOpener::~ImageOpener() {}

void ImageOpener::addHandler(std::auto_ptr<ImageOpenHandler> handler) {
  m_openers.push_back(boost::shared_ptr<ImageOpenHandler>(handler));
}

QString ImageOpener::filterList() const {
  // Aggregate the filters
  return std::for_each(m_openers.begin(), m_openers.end(),
                       image_opener::CollectFilters())
      .filterList().join(";;");
}

boost::shared_ptr<ImageData> ImageOpener::open(const std::string& filename) const {
  // Fail if the file does not exist
  if (!QFile::exists(filename.c_str())) throw FileAccessError(filename);

  // Step through the openers until one opens the file
  boost::shared_ptr<ImageData> imageData;
  for (Openers_t::const_iterator i = m_openers.begin();
      (m_openers.end() != i) && !imageData.get(); ++i) {
    imageData = (*i)->open(filename);
  }

  // Fail if no appropriate handler exists
  if (!imageData) throw UnhandledFormatError(filename);

  return imageData;
}

namespace image_opener {

CollectFilters::CollectFilters() : m_filters(new QStringList) {}

void CollectFilters::operator()(boost::shared_ptr<ImageOpenHandler> handler) {
  *m_filters += handler->fileFilter();
}

QStringList CollectFilters::filterList() const {
  return *m_filters;
}

} // namespace image_opener
} // namespace image
} // namespace sedeen
