#ifndef SEDEEN_SRC_IMAGE_IO_TILEMATRIX_H
#define SEDEEN_SRC_IMAGE_IO_TILEMATRIX_H

// System headers

// User headers
#include "image/io/TileVector.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {
	/*!
        \class TileMatrix
        \brief A container for 2D tile matrix.
         Can be used to represent on resolution level of a multi-resolutional image.

         This class is NOT thread-safe.
     */
class SEDEEN_IMAGE_API TileMatrix : public TileVector {
public:
        static const uint32_t MatrixCapacity;

        class Proxy {
            friend class TileMatrix;
            const Tile& operator[](uint32_t col) const;
        private:
            Proxy(const TileMatrix &matrix, uint32_t col);
            uint32_t _row;
            const TileMatrix& _matrix;
        };


        /*!
            \brief Creates a TileMatrix. Currently the dimensions of the matrix cannot be chnaged and is fixed to \a rows and \a cols.
            If the dimensions are zero, a NULL matrix will be created. This is default constructor is provided to be used smart_array s.
            To change the capacity of the matrix, see setCapacity(). setting the apacity will limit over the memory consumption of the
            matrix. By default the capacity is limited to the full matrix.
        */
        TileMatrix( uint32_t rows = 0,
                    uint32_t cols = 0,
                    PurgeMode mode = PurgeOldestFirst);
        virtual ~TileMatrix();

        uint32_t rows() const;
        uint32_t cols() const;
        uint32_t getTileno(uint32_t row, uint32_t col) const;

        void setTileImage(uint32_t tileno, RawImage image, TileState state, bool isReferenceForPurge = false);
        void setTileImage(uint32_t row, uint32_t col, RawImage image, TileState state, bool isReferenceForPurge = false);

        using TileVector::tile;
        const Tile& tile(uint32_t row, uint32_t col) const;

        using TileVector::operator ();
        const Tile& operator()(uint32_t row, uint32_t col) const;

        Proxy operator[](uint32_t row) const;

    protected:
        /*!
            \copydoc TileVector::purgeListFarthest
        */
        virtual TileVector::DistanceTileMMapPtr purgeListFarthest() const;

        /*!
            \copydoc TileVector::purgeListFarthestRing()
        */
        virtual DistanceTileMMapPtr purgeListFarthestRing() const;

    private:
        TileMatrix(const TileMatrix&);

    private:
        uint32_t _rows,
                 _cols;
};

}   // namespace sedeen
}   // namespace image

#endif // ifdef SEDEEN_SRC_IMAGE_IO_TILEMATRIX_




