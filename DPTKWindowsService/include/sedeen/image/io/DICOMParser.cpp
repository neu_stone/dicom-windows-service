//
//// main header
//#include "DicomParser.h"
//
//// User headers
//#include "Exceptions.h"
//#include "global/UnitDefs.h"
//
//// system header
//#include <iostream>
//#include <set>
//
//// Should switch to DCMTK
////#include <dcmtk/config/osconfig.h>
//#include <dcmtk/dcmdata/dctk.h>
//#include <dcmtk/dcmdata/dcistrmf.h>
//
//
//namespace sedeen{
//namespace image{
//
//DICOMParser::DICOMParser(const char* filename) :
//ImageParser(),
//_filename(filename),
//_file(new DcmFileFormat())
//{
//    OFCondition status = _file->loadFile(filename.c_str());
//
//    if( !status.good() )
//    {
//        _file->clear();
//        throw FileAccessError("Could not load file");
//    }
//}
//
//const ImagePyramid& DICOMParser::pyramid() const {
//    return *_pyramid;
//}
//
//void* DICOMParser::handel() const {
//    return _jp2;
//}
//
//const std::string& DICOMParser::filename() const {
//    return _filename;
//}
//
//uint16_t DICOMParser::magnification(bool *defaultValue) const {
//    const double twentyXSize = 0.5 * si_prefix::u;
//    *defaultValue = true;
//    return pixelWidth() / twentyXSize;
//}
//
//uint32_t DICOMParser::producer() const {
//    return _producer;
//}
//
//std::string DICOMParser::compression() const {
//    return "JPEG-2000";
//}
//
//std::string DICOMParser::description() const {
//    assert(false);
//    return std::string();
//}
//
//bool DICOMParser::hasLabel() const {
//    return false;
//}
//
//bool DICOMParser::hasThumbnail() const {
//    return false;
//}
//
//QImage DICOMParser::label() const {
//     return QImage();
//}
//
//QImage DICOMParser::thumbnail() const {
//     return QImage();
//}
//
//DICOMParser::uint64_t DICOMParser::width() const
//{
//    Uint16 value;
//    OFCondition status = _file->getDataset()->findAndGetUint16(DcmTagKey(0x0028,0x0011), value, 0, OFFalse);
//    if( status.good() )
//        return value;
//    else
//        return 0;
//
//}
//
//DICOMParser::uint64_t DICOMParser::height() const
//{
//    Uint16 value;
//    OFCondition status = _file->getDataset()->findAndGetUint16(DcmTagKey(0x0028,0x0010), value, 0, OFFalse);
//    if( status.good() )
//        return value;
//    else
//        return 0;
//}
//
//DICOMParser::uint16_t DICOMParser::spp() const
//{
//    Uint16 value;
//    OFCondition status = _file->getDataset()->findAndGetUint16(DcmTagKey(0x0028,0x0002), value, 0, OFFalse);
//    if( status.good() )
//        return value;
//    else
//        return 0;
//}
//
//DICOMParser::uint16_t DICOMParser::bps() const
//{
//    Uint16 value;
//    OFCondition status = _file->getDataset()->findAndGetUint16(DcmTagKey(0x0028,0x0101), value, 0, OFFalse);
//    if( status.good() )
//        return value;
//    else
//        return 0;
//}
//
//double DICOMParser::pixelWidth() const
//{
//    OFString value;
//    OFCondition status = _file->getDataset()->findAndGetOFString(DcmTagKey(0x0028,0x0030), value, 0, OFFalse);
//    if( status.good() )
//        return atof(value.data()) * si_prefix::m;
//    else
//        return 0;
//}
//
//double DICOMParser::pixelHeight() const
//{
//    OFString value;
//    OFCondition status = _file->getDataset()->findAndGetOFString(DcmTagKey(0x0028,0x0030), value, 1, OFFalse);
//    if( status.good() )
//        return atof(value.data()) * si_prefix::m;
//    else
//        return 0;
//}
//
//uint16_t DICOMParser::magnification() const
//{
//}
//
//std::string DICOMParser::color() const
//{
//    OFString value;
//    OFCondition status = _file->getDataset()->findAndGetOFString(DcmTagKey(0x0028,0x0030), value);
//    if( status.good() )
//        return value.c_str();
//    else
//        return "Unknown color";
//}
//
//std::string DICOMParser::compression() const
//{
//    OFString value;
//    OFCondition status = _file->getDataset()->findAndGetOFString(DcmTagKey(0x0028,0x0030), value);
//
//    // http://www.medicalconnections.co.uk/wiki/Transfer_Syntax
//    // see refernce for transfer syntax definitions
//    if( !status.good() )
//       return "Unknown compression";
//
//    else if( value.compare("1.2.840.10008.1.2.4.91") == 0 )
//        return "JPEG-2000 (lossy)";
//
//    else if( value.compare("1.2.840.10008.1.2.4.51") == 0 )
//        return "JPEG (12 bit)";
//
//    else if( value.compare("1.2.840.10008.1.2.4.50") == 0 )
//        return "JPEG";
//
//    else if( value.compare("1.2.840.10008.1.2.4.90") == 0 )
//        return "JPEG-2000 (lossless)";
//
//    else if( value.compare("1.2.840.10008.1.2.5") == 0 )
//        return "RLE (lossless)";
//
//    else if( value.compare("1.2.840.10008.1.2.4.70") == 0 )
//        return "PCM - Huffman Coding (lossless)";
//
//    else if( value.compare("1.2.840.10008.1.2.4.57") == 0 )
//        return "PCM - First Order Huffman Coding (lossless)";
//
//    else if( value.compare("1.2.840.10008.1.2.2") == 0 )
//        return "Explicit VR Big-endian";
//
//    else if( value.compare("1.2.840.10008.1.2.1") == 0 )
//        return "Explicit VR Little-endian";
//
//    else if( value.compare("1.2.840.10008.1.2") == 0 )
//        return "Implicit VR Little-endian";
//
//    else
//        return "Unknown compression";
//
//}
//
//std::string DICOMParser::creator() const
//{
//    return "";
//}
//
//const std::string& DICOMParser::filename() const
//{
//    return _filename;
//}
//
//const DcmDataSet* DICOMParser::dataSet() const
//{
//    return *(_file->getDataset());
//}
//
//
//}   // namespace image
//}   // namespace sedeen


