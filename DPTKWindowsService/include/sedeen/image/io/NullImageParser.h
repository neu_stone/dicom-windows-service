#ifndef SEDEEN_SRC_IMAGE_IO_NULLIMAGEPARSER_H
#define SEDEEN_SRC_IMAGE_IO_NULLIMAGEPARSER_H

#include "image/io/ImageParser.h"
#include "image/buffer/RawImageTypes.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {

class SEDEEN_IMAGE_API NullImageParser : public ImageParser {
 public:
  NullImageParser();
  virtual ~NullImageParser();
  private:
  // virtual functions
  bool doGetHasLabelImage() const;
  bool doGetHasMacroImage() const;
  bool doGetHasThumbnailImage() const;
  std::string doGetFilename() const;
  std::string doGetProducer() const;
  Color doGetColor() const;
  std::string doGetCompression() const;
  std::string doGetDescription() const;
  SizeF doGetPixelSize() const;
  int doGetComponents() const;
  int doGetBpp() const;
  int doGetBpc(const int comp) const;
  double doGetMagnification() const;
  RawImage doGetLabelImage() const;
  RawImage doGetThumbnailImage() const;
  RawImage doGetMacroImage() const;
  ImagePyramidPtr doGetPyramid() const;
 private:
  ImagePyramidPtr image_pyramid_;
}; // class NullImageParser

} // namespace image
} // namespace sedeen

#endif
