#ifndef SEDEEN_SRC_IMAGE_IO_SELECTIVEDECDOER_H
#define SEDEEN_SRC_IMAGE_IO_SELECTIVEDECDOER_H

// System headers
#include <list>
#include <memory>

#include <QtCore>

// User headers
#include "image/io/TilePyramid.h"
#include "image/io/ImageParser.h"
#include "image/io/ImageIOTypes.h"
#include "global/geometry/Size.h"
#include "image/DllApi.h"

Q_DECLARE_METATYPE(std::string);


template<class T> class CImg;

namespace sedeen {
namespace image {

enum DecoderErrorType{
  NoError,
  RegionError,
  MemoryError,
  DecodeError,
};

typedef std::list<int32_t> List;
typedef std::auto_ptr<List> ListPtr;
typedef List::iterator ListItr;

typedef std::vector<int32_t> Vector;
typedef std::auto_ptr<Vector> VectorPtr;
typedef Vector::iterator VectorItr;

/// A mistreated selective image decoder.
//
/// Selectively decodes images from disk in two threads requests are accepted in
/// the main thread, while the actual decoding is performed in the background.
///
/// Basic image properties can be retrieved using: pyramid(), parser()
///
/// getRegion() starts a background process for decode a request. Once a request
/// is submitted, decoderFinished() signals are emitted every time a new data is
/// available. The correct way to respond to decoderFinished() signal is to call
/// updateRegion() which returns an updated image for the requested region.
///
/// The decoderCaching() is progress signal that is emitted with the amount of
/// work queued for caching, see decoderCaching() for more details.
///
/// \note
/// This is an abstract class for any image that supports selective decoding.
/// Pay particular attention to decodeRegion(), decodeTile(), decodeImage(),
/// setLastError() and waitForExit().
///
/// To improve the disk i/o procedure, the class implements automatic buffering
/// of decoded tiles. The buffer size size can be adjusted using
/// setBufferCapacity().
class SEDEEN_IMAGE_API SelectiveDecoder : public QThread {
  Q_OBJECT

  public:

  /// Default fuzzy image width
  static const uint32_t DefaultFuzzyImageWidth;

  /// Default buffer capacity
  static const uint32_t DefaultBufferCapacity;

  /// Smart pointer used for SelectiveDecoder
  typedef std::auto_ptr<SelectiveDecoder> SelectiveDecoderPtr;

  /// Constructor.
  SelectiveDecoder(ImageParserPtr parser, QObject*  parent = 0);

  /// Destructor.
  virtual ~SelectiveDecoder();

  /// Creates an independent version of this decoder.
  //
  /// On failure returns a NULL pointer.
  virtual SelectiveDecoderPtr clone() const = 0;

  /// Gets the filename of the open file.
  const std::string filename() const;

  /// Gets the image parser.
  virtual const ImageParser& parser() const;

  /// Gets the ImagePyramid.
  //
  /// Contains information about the image's internal structure such as the
  /// number of resolution levels, tile size, tile boundaries, etc.
  ImagePyramidPtr pyramid() const;

  /// Sets the approximate maximum size in Mega-Bytes allowed for buffering.
  //
  /// The actual bufferCapacity() is calculated such that it's an integer
  /// multiple of bytes per the tile and is always less than \a memsize. If
  /// \a memsize is too low, the buffer size will be adjusted on the fly to
  /// provide actuate space for requested region.
  ///
  /// \sa
  /// bufferCount(), bufferCapacity()
  ///
  /// \param memsize
  /// The maximum memory to use for buffering in bytes
  ///
  /// \return
  /// The actual buffer limit
  uint64_t setBufferCapacity( uint64_t memsize);

  /// Gets the maximum size in bytes that can be buffered.
  //
  /// \sa
  /// setBufferCapacity(), bufferCapacity(), bufferSizeRequired(),
  /// bufferCount(), tileSize().
  uint64_t bufferCapacity() const;

  /// Calculate the minimum buffer size for a screen resolution.
  //
  /// \sa setBufferCapacity(), bufferCapacity(), bufferSizeRequired(),
  /// bufferCount(), tileSize().
  ///
  /// \param width
  ///
  /// \param height
  ///
  /// \return
  /// Buffer size in bytes.
  uint64_t bufferSizeRequired(int width, int height) const;

  /// Gets the current buffer size in bytes.
  //
  /// \sa setBufferCapacity(), bufferCapacity(), bufferSizeRequired(),
  /// bufferCount(), tileSize().
  ///
  /// \return
  /// Buffer size in bytes.
  uint64_t bufferSize() const;

  /// Gets the number of tiles buffered at all levels.
  //
  /// \sa setBufferCapacity(), bufferCapacity(), bufferSizeRequired(),
  /// bufferCount(), tileSize().
  ///
  /// \return
  /// The current tile count
  uint32_t bufferCount() const;

  /// Gets the size of each tile in bytes.
  //
  /// \sa setBufferCapacity(), bufferCapacity(), bufferSizeRequired(),
  /// bufferCount(), tileSize().
  ///
  /// \return
  /// Tile size
  uint64_t tileSize() const;

  /// Returns a binary image representing the buffered tiles at \a zoom level
  //
  /// The width and height of the image represent the number of tiles in the
  /// tile-buffer at the specified zoom level. Every tile that is currently-
  /// buffered is set to 1 and un-buffered tiles are set to 0.
  ///
  /// \param zoom
  /// The scale factor of interest
  ///
  /// \return
  /// A binary image of the buffered tiles in the image
  RawImage bufferImage(const double& zoom) const;

  /// Process \a request by extracting data from image.
  //
  /// Any subsequent calls to getRegion(), clears the previous requests if they
  /// are incomplete.
  ///
  /// \note
  /// Starts a background process to decode \a request. The decoderCaching() and
  /// decoderFinished() signals are emitted to indicate progress of the
  /// background process.
  ///
  /// \param request
  /// Defines the region to be extracted
  void getRegion(const DecodeRequestPtr& request);

  /// Sets a request to cache after other requests have been processed.
  ///
  /// \param request
  /// The request object, the cache() of which flag in \a request
  void cacheRegion(const DecodeRequestPtr& request);

  /// Gets the last cache request.
  ///
  /// \return
  /// A NULL requests indicates that cached requested have been processed.
  DecodeRequestPtr cacheRequest() const;

  /// Returns an updated image.
  //
  /// This image contains the same region that was given to the last call of
  /// getRegion(). It is intended to be used after a decoderFinished() signal so
  /// that the last requested region can be updated with new data.
  ///
  /// \note
  /// This method is much faster then calling getRegion() repeatedly when the
  /// decoderFinished() signal is emitted. Returns a NULL image if called before
  /// any calls to getRegion(). Return the same region as in last call to
  /// getRegion().
  ///
  /// \return
  /// An image of last requested region or a NULL image on error
  RawImage updateRegion();

  /// Checks to see if the last requested region has been decoded.
  bool isRequestDecoded() const;

  /// Clear requests and signal for background tasks to terminate.
  void clearRequest();

  /// Checks to see if in-progress request has been modified
  bool isRequestChanged() const;

  /// Identifies the current request with a unique integer.
  //
  /// ID eventually rolls over to zero and increments again. Useful for
  /// identifying the age of signals (i.e. whether a signal has been emitted in
  /// response to the current request).
  int currentRequestID() const;

 protected:
  /// Returns when the background thread has existed.
  //
  /// \note
  /// Must be called from derived class destructors to ensure orderly
  /// destruction.
  void waitForExit();

  /// Retrieves thumbnial image for filling requests that are not yet cached
  //
  /// The default implementation check parser() for a thumbnial, otherwise, it
  /// extracts a thumbnail from the image pyramid that has a minimum width of 
  /// DefaultFuzzyImageWidth.
  //
  /// Derived classes many want to override this function.
  virtual RawImage getFuzzyImage();

  /// Decodes the specified request from the image.
  //
  /// \note
  /// Subclasses must report error via setLastError() and shoud not emit the
  /// decoderError() signal.
  //
  /// \param request
  ///
  /// \return
  /// An image on or NULL image on error
  virtual RawImage decodeRegion (const DecodeRequest& request) = 0;

  /// Decodes the specified tile form the image using decodeRegion().
  //
  /// \note
  /// It may be possible to provide a more efficient implementation in derived
  /// classes.
  //
  /// \note
  /// Subclasses must report error via setLastError() and shoud not emit the
  /// decoderError() signal.
  //
  /// \param lvl
  /// The required resolution level
  ///
  /// \param tileno
  /// The tile number to decode
  ///
  /// \return
  /// An image on or NULL image on error
  virtual RawImage decodeTile (int32_t lvl, int32_t tileno);

  /// Decodes the entire image using decodeRegion().
  //
  /// \note
  /// It may be possible to provide a more efficient implementation in derived
  /// classes.
  //
  /// \note
  /// Subclasses must report error via setLastError() and shoud not emit the
  /// decoderError() signal.
  //
  /// \param lvl
  /// The resolution level to decode
  ///
  /// \return
  /// Pointer to the decoded image or a NULL RawImage
  virtual RawImage decodeImage (uint32_t lvl);

  /// The method by which subclasses should indicate errors. Subclasses should
  /// not call the decoderError() signal or take other action.
  //
  /// \param error
  /// code
  ///
  /// \param message
  /// Default messages are available for defined codes
  void setLastError(DecoderErrorType error,
                    const std::string& message = "");

  /// Returns the last reported error.
  DecoderErrorType lastError() const;

 private:
  /// Checks if buffer contains the given tile (fuzzy or detailed version)
  //
  /// \param lvl
  /// resolution level
  ///
  /// \param tileno
  ///
  /// \return
  /// \c true on success, \c false otherwise
  bool hasTile(int32_t lvl, int32_t tileno) const;

  /// Checks if buffer contains the given tile (detailed version)
  //
  /// \param lvl
  /// resolution level
  ///
  /// \param tileno
  ///
  /// \return
  /// \c true on success, \c false otherwise
  bool hasDetailedTile(int32_t lvl, int32_t tileno) const;

  /// Checks if buffer contains the given tile (fuzzy version)
  //
  /// \param lvl
  /// resolution level
  ///
  /// \param tileno
  ///
  /// \return
  /// \c true on success, \c false otherwise
  bool hasFuzzyTile(int32_t lvl, int32_t tileno) const;

  /// cannot be copied
  SelectiveDecoder(const SelectiveDecoder& SelectiveDecoder);

  /// cannot be assigned
  SelectiveDecoder& operator=(const SelectiveDecoder&);

  /// \copydoc isRequestDecoded().
  //
  /// \note
  /// Does not lock mutex.
  bool isRequestDecodedNoLock() const;

  /// \copydoc isRequestChanged().
  //
  /// \note
  /// Does not lock mutex.
  bool isRequestChangedNoLock() const;

  /// \copydoc clearRequest().
  //
  /// \note
  /// Does not lock mutex.
  void clearRequestNoLock();

  /// \copydoc clearRequest().
  //
  /// \note
  /// Does not lock mutex.
  int currentRequestIDNoLock() const;

  /// \copydoc hasTile().
  //
  /// \note
  /// Does not lock mutex.
  bool hasTileNoLock(int32_t lvl, int32_t tileno) const;

  /// \copydoc hasDetailedTile().
  //
  /// \note
  /// Does not lock mutex.
  bool hasDetailedTileNoLock(int32_t lvl, int32_t tileno) const;

  /// \copydoc hasFuzzyTile().
  //
  /// \note
  /// Does not lock mutex.
  bool hasFuzzyTileNoLock(int32_t lvl, int32_t tileno) const;

  /// The preferred method to signals errors by subclasses.
  //
  /// \param error
  /// code
  ///
  /// \param message
  /// Default messages are available for defined codes
  void emitError(DecoderErrorType error, const std::string& message = "");

  /// \copydoc emitError().
  //
  /// \note
  /// Does not lock mutex.
  void emitErrorNoLock(DecoderErrorType error,
                       const std::string& message = "");

  /// Emits the last error if any and resets last error.
  void emitLastErrorNoLock();

  /// Consumes all requests and schedules them.
  /// \return
  //  FALSE on error, TRUE otherwise
  bool processRequest();

  /// Makes a fuzzy image for the current request
  void makeFuzzyImage();

  /// Updates the purge function with the latest request.
  //
  /// Tiles are prioritized for purging based on their distance to the
  /// \a request tiles.
  void setPurgeReference(TilePyramid& buffer, const DecodeRequestPtr& request);

  /// Sets the cache request
  void setCacheRequest(const DecodeRequestPtr& request);

  /// Set the new request and clears old request.
  //
  /// Configures TilePyramid's purge reference and purge exceptions parameters
  /// so that \a request can be executed efficiently.
  ///
  /// \return
  /// \c true if \a request can be serviced, \c false otherwise
  bool setRequest(const DecodeRequestPtr& request);

  /// Sets the minimum buffer capacity for the given request.
  void setMinimumBufferCapacity(TilePyramid& buffer,
                                const DecodeRequestPtr& request);

  /// start or restart the background thread.
  void restart();

  /// Fills the specified tile with image data.
  //
  /// If tile is already buffered, returns \c true immediately. On error, emit
  /// MemoryError decoderError()
  ///
  /// \param lvl
  /// The required resolution level
  ///
  /// \param tileno
  /// The tile to fill with data
  ///
  /// \return
  /// \c true if a new tile successfully decoded, \c false on error (usually
  /// associated with out of memory)
  bool decodeToBuffer (int32_t lvl, int32_t tileno);

  /// Allocates and paints an image for a last requested region.
  //
  /// If the image cannot be allocated, emitError() is called with MemoryError
  /// as argument.
  ///
  /// \return
  /// \c true on success, \c false if image cannot be allocated
  bool createBufferImage();

  /// This documentation doesn't make a lot of sense for the function name.
  //
  /// Calculates the tiles needed for the current request and ensures that are
  /// filled with fuzzy data.
  ///
  /// \return
  /// \c true on success, \c false otherwise
  bool getDecodeList();

  /// Fills the given tile in buffer with fuzzy data
  //
  /// \param lvl
  ///
  /// \param tileno
  ///
  /// \return
  /// \c true on success, \c false otherwise
  bool bufferFuzzyTile(int32_t lvl, int32_t tileno);

  /// Worker thread instructions
  void run();

  /// Used by worker thread for fetching fuzzy image
  void setFuzzyImage();

  /// Used by worker thread for fetching tiles
  //
  /// \return
  /// \c true if successful, \c false if process was aborted for any reason
  bool decodeTiles();

  /// Decode tiles surrounding the last request.
  //
  /// Decodes the list of tiles around the last requested view which are **NOT**
  /// needed for immediately. These tiles surround the requested region and
  /// could be requested.
  ///
  /// \return
  /// \c true if successful, \c false if process was aborted for any reason
  bool cacheTiles();

  /// Cache tiles surrounding a given request.
  //
  /// \param request
  /// Request around which to be cached.
  ///
  /// \param ring
  /// Surrounding region to cache.
  ///
  /// \param maxTilesToCache
  /// Limit on the number of tiles to cache.
  ///
  /// \param errorDetected
  ///
  /// \return
  /// Something?
  uint32_t cacheTiles(const DecodeRequestPtr& request, const uint32_t ring,
                      const uint32_t maxTilesToCache, bool& errorDetected);

 signals:
  /// Emitted when the background process is activated for caching.
  //
  /// \note
  /// Unlike decoderFinished(), which only responds to the progress for the
  /// current request, this signal is emitted to indicate that the decoder is
  /// actively caching outside the current requested.
  ///
  /// \param percent
  /// [0, 100] indicates the amount of decoding that remains to complete the
  /// cache at value of 100 indicates that caching is complete. Note that if the
  /// caching is aborted with value 100 if the a new request is encountered.
  void decoderCaching(int percent);

  /// Emitted when an updated image for the current request becomes available.
  //
  /// \note
  /// Typically emitted immediately after a call to getRegion() with a fuzzy
  /// image and after every decoded tile. The most efficient way to update in
  /// response to this signal is to use updateRegion().
  ///
  /// \param percent
  /// [0, 100] indicates the amount of decoding that remains to complete the
  /// current request at 100, updateRegion() will return a detailed image,
  /// otherwise, updateRegion() returns an image containing fuzzy data
  ///
  /// \param id
  /// A number associated withe the current request. see currentRequestID()
  void decoderFinished(int percent, int id);

  /// Is emitted on error.
  //
  /// Should not be emitted by subclasses, instead, use emitError().
  ///
  /// \param mes
  /// The message form the decoder
  void decoderError(const std::string& mes);

 private:
  ImageParserPtr m_parser; ///< image parser
  TilePyramid m_buffer; ///< Tile pyramid buffer
  mutable QMutex m_mutex; ///< member access mutex

  // Other image storage
  RawImage m_image; ///< image created of the last requested region
  List m_imageUpdateList; ///< list of tiles to update m_image with - initialized to m_decodeList
  RawImage m_imageFuzzy; ///< Hold the full image at the fuzzy res level

  // Members for thread management
  bool m_restart; ///< restart variable
  bool m_abort; ///< aborts the thread
  QWaitCondition  m_restartCondition; ///< wait condition for halting thread when processing done

  // Last decoded image
  DecodeRequestPtr m_request; ///< the last requested decoded region
  DecodeRequestPtr m_requestCache; ///< the last request for ac cache
  bool m_requestChanged; ///< indicated the current request has been cancelled
  int m_requestID; ///< emitted with decoder finished to indicated which request is being handled

  // Tiles to be decoded
  List m_decodeList; ///< list of fuzzy tiles scheduled to decoding
  uint32_t m_decodeLevel; ///< Resolution level for tiles in decode-queue

  DecoderErrorType m_lastError;
  std::string m_lastErrorMessage;
};

SEDEEN_IMAGE_API
void paintTileOnImage(RawImage& imageToPaint, const DecodeRequest& imageRegion,
                      const TilePyramid& buffer, int32_t tileno,
                      int32_t level);

/// Compute ring of tiles surrounding the given region.
//
/// \relates SelectiveDecoder
///
/// The rectangular region is described by its top-left tile \a topLeftTile and
/// bottom-right tile \a bottomRightTile.
///
/// For \a dist == 1, the ring of tiles immediately surrounds the region.
///
/// \param pyramid
///
/// \param level
///
/// \param topLeftTile
///
/// \param bottomRightTile
///
/// \param dist
/// The nth concentric rectangular ring around the region
///
/// \return
/// List of tiles surrounding region
SEDEEN_IMAGE_API
VectorPtr getCacheList(const ImagePyramid& pyramid, int32_t level,
                       int32_t topLeftTile, int32_t bottomRightTile,
                       int32_t dist = 1);

} // end namespace image
} // end namespace sedeen

#endif // SEDEEN_SRC_IMAGE_IO_SELECTIVEDECDOER_H