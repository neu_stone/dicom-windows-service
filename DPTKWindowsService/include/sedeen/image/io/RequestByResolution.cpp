// Primary header
#include "image/io/RequestByResolution.h"

// System headers
#include <boost/make_shared.hpp>

// User Headers
#include "global/Debug.h"
#include "global/geometry/Size.h"
#include "image/io/NullPyramid.h"

namespace sedeen {
namespace image {

RequestByResolution::RequestByResolution(const ImagePyramidPtr &pyramid,
                                         const Rect &region,
                                         const int32_t resolution,
                                         const bool cache)
    : _pyramid(pyramid),
      _region(region),
      _resolution(resolution),
      _cache(cache),
      _tiles(tileListSpanningRegion(region, _resolution, _resolution, *pyramid)) {
  debug::assert_x(resolution < pyramid->levels(), "Invalid resolution level",
                  __FUNCTION__, __LINE__);
}

RequestByResolution::RequestByResolution()
    : _pyramid(boost::make_shared<NullPyramid>()),
      _region(),
      _resolution(0),
      _cache(false),
      _tiles(boost::make_shared<TileVec>()) {}

DecodeRequestPtr RequestByResolution::doGetClone() const {
  return boost::make_shared<RequestByResolution>(*this);
}

bool RequestByResolution::doGetIsNull() const {
  return tiles()->size() == 0;
}

Rect RequestByResolution::doGetRegion(int32_t level) const {
  debug::assert_x(level >= 0 && level < pyramid()->levels(),
                  "Invalid resolution level specified", __FUNCTION__, __LINE__);
  if (resolution() == level)
    return _region;
  else
    return mapRegionToAlternateResolution(_region, resolution(), level,
                                          *pyramid());
}

int32_t RequestByResolution::doGetResolution() const {
  return _resolution;
}

bool RequestByResolution::doGetCache() const {
  return _cache;
}

Size RequestByResolution::doGetSize() const {
  return sedeen::size(_region);
}

SizeF RequestByResolution::doGetScale() const {
  return pyramid()->scale(resolution());
}

ImagePyramidPtr RequestByResolution::doGetPyramid() const {
  return _pyramid;
}

RequestByResolution::TileVecConstPtr RequestByResolution::doGetTiles() const {
  return _tiles;
}

}   // namespace image
}   // namespace sedeen

