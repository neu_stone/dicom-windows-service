// Primary header
#include "image/io/ImageListener.h"

// User includes
#include "image/io/ImageAccessor.h"

namespace sedeen {
namespace image {

ImageListener::ImageListener(const ImageAccessor& client)
    : QObject(0),
      m_imageCache(),
      m_complete(false) {
  connect(&client, SIGNAL(imageUpdated(RawImage, bool)),
          this, SLOT(imageUpdated(RawImage, bool)));
  connect(&client, SIGNAL(error(const std::string&,const std::string&)),
          this, SLOT(error(const std::string&,const std::string&)));
  connect(&client, SIGNAL(destroyed()),
          this, SLOT(imageClosed()));
}

ImageListener::~ImageListener() {}

bool ImageListener::complete() const {
  return m_complete;
}

RawImage ImageListener::image() const {
  return m_imageCache;
}

void ImageListener::imageUpdated(RawImage image, bool complete) {
  m_imageCache = image;
  m_complete = complete;
}

void ImageListener::error(const std::string &error, 
  const std::string &filename) {
  m_complete = true;
}

void ImageListener::imageClosed() {
  m_imageCache = RawImage();
  m_complete = true;
}

} // namespace image
} // namespace sedeen
