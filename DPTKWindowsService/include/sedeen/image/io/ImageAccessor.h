#ifndef SEDEEN_SRC_IMAGE_IO_IMAGEACCESSOR_H
#define SEDEEN_SRC_IMAGE_IO_IMAGEACCESSOR_H

// System headers
#include <QtCore>
#include <boost/shared_ptr.hpp>

// User includes
#include "image/io/Global.h"
#include "image/DllApi.h"

namespace sedeen {

class Rect;
class Size;
class SizeF;

/// A front-end for all image data access classes.
//
/// Sedeen, and its associated utilities, exist to facilitate the viewing and
/// analysis of images in awkward locations, images of very sizes, or both. In
/// order to accomplish access, Sedeen offers a request-based data access
/// system. Clients submit the region and output resolution of a target image
/// and the decoder engine returns, via Qt's signal-and-slot system, an
/// increasingly-refined version of the target image.
///
/// The ImageAccessor constitutes the primary access point to image data. To
/// address memory consumption through multiple images caches, each
/// ImageAccessor requesting data from the same file can share a single instance
/// of the underlying access class, a member of the family of ImageData classes.
/// ImageData serializes the incoming requests based on users' assigned
/// priorities.
///
/// Rather than creating ImageAccessors directly, the ImageRepository offers a
/// mechanism to ensure shared data among ImageAccessor instances. It also can
/// dynamically extend the types of image files it can process. The
/// ImageOpenHandler system encapsulates the task of choosing a concrete
/// ImageData type to access a given image file.
namespace image {

class ImageData;
class ImageParser;
class ImagePyramid;
class RawImage;
class SelectiveDecoder;

/// Handle class for shared ImageData.
//
/// This class acts as both a unique client for image retrieval and a
/// Signal/Slot point of contact.
///
/// Several ImageAccessor instances may share one source of data, allowing
/// control over both network access and greater control over local caching
/// demands. Users access image data by submitting decode requests via region()
/// and catching the decoded data from the emitted imageUpdated() signal. Many
/// separate instances of ImageAccessor may submit requests to a common data
/// source, but if a single instance submits more than one request, the
/// scheduler will replace any queued or currently-processing request from that
/// client. For this reason, users ought to ensure that each data sink has its
/// own copy of the ImageAccessor instance before using any access functions
/// (region(), regionBackground(), thumbnail()).
///
/// Though manually passing a shared pointer to an ImageData instance will
/// result in a functional ImageAccessor instance, users may prefer to use the
/// ImageRepository class to retrieve ImageAccessor instances. The
/// ImageRepository will handle creating the appropriate ImageData derivative
/// for a given file and ensures that each unique filename corresponds to
/// exactly one ImageData instance.
///
/// The ImageAccessor class is reentrant, but is not thread-safe.
class SEDEEN_IMAGE_API ImageAccessor : public QObject {
  Q_OBJECT;
 public:
  ImageAccessor();

  /// Open an image file.
  //
  /// \param imageData
  /// Shared pointer to the ImageData instance to be read. If no valid ImageData
  /// object is given, the instance will revert to a null state.
  ///
  /// \post
  /// All associated methods will function.
  explicit ImageAccessor(const boost::shared_ptr<ImageData>& imageData);

  /// Cancels all requests on destruction.
  ~ImageAccessor();

  /// Shallow copy constructor
  //
  /// \param imageAccessor
  ///
  /// Creates a parentless and childless copy of this ImageAccessor. The copy is
  /// independent, but is bound to the same image data as the source.
  ImageAccessor(const ImageAccessor& imageAccessor);

  /// Shallow assignment operator
  //
  /// Changes the image data to which the ImageAccessor points. Does not affect
  /// the QObject base of the class; parents stay parents and children stay
  /// children. More importantly, does not affect connected slots.
  ///
  /// Does not force an update of connected observers. This will need to be
  /// signaled manually by clients.
  ImageAccessor& operator=(const ImageAccessor& imageAccessor);

  /// Tests if the ImageAccessor instances are sharing data.
  bool operator==(const ImageAccessor& imageAccessor) const;

  /// Get the dimensions, in pixels, of a level of the image.
  //
  /// \param level
  /// Level of the image to query. Must be within [0,getNumResolutionLevels()).
  Size getDimensions(int level) const;

  /// Query the number of resolution levels contained in the image.
  int getNumResolutionLevels() const;

  /// Gets the image pixel size or zero if not specified in metric units.
  //
  /// \note
  /// It's technically possible to have a pixel size of 0 specified in the file.
  SizeF getPixelSize() const;

  /// Get a binary image of buffered tiles for the given zoom level
  /// Each tile is represented by one pixel in the retured image.
  /// Tiles (pixels) currently in the buffer will be marked by 1, otherise 0.
  ///
  /// \param scale the scale of interest
  RawImage bufferImage(double scale) const;

  /// Do not use this function.
  SelectiveDecoder* selectiveDecoder() const;

  /// Create an ImageAccessor with an independent data source.
  ImageAccessor independentCopy() const;

  /// Do not use this function.
  const ImageParser& imageParser() const;

 private:
  /// Alert all observers that the current image has changed.
  void updateImage(const RawImage& image, bool complete) const;

  /// Forward appropriate signals from the the contained data object.
  void connectSignals() const;

  /// Disconnect signals from the current data object
  void disconnectSignals();

 private slots:
  /// Alert all observers that the current image has erred.
  //
  /// This function is not thread-safe. Since most of these errors originate in
  /// worker threads, they should communicate via a signal connection.
  void imageError(const std::string& message);

 signals:
  /// Sent when a request is filled, in part or in whole.
  //
  /// \param image
  /// Most recent version of the image being decoded.
  ///
  /// \param complete
  /// TRUE if the accompanying image is the last to be emitted for the previous
  /// request, FALSE if the image is still only partially-decoded.
  void imageUpdated(const RawImage& image, bool complete) const;

  /// Sent when an read error has occurred with an open image.
  //
  /// This signal means that the decoder mechanism has hit an unrecoverable
  /// error. The ImageAccessor instance will revert to its null state to protect
  /// clients.
  ///
  /// \param message
  /// Description of the error that has occurred.
  ///
  /// \param filename
  /// The image file originating the error.
  void error(const std::string& message, const std::string& filename) const;

  void decoderCaching(int);

 private:
  /// Implementation pointer.
  boost::shared_ptr<ImageData> m_data;

  friend class sedeen::image::ImageData;
  friend SEDEEN_IMAGE_API std::string filePath(const ImageAccessor&);
  friend SEDEEN_IMAGE_API void region(ImageAccessor& imageAccessor,
                                      double scale, const Rect& region,
                                      Priority priority, bool cacheSurrounding);
  friend SEDEEN_IMAGE_API void region(ImageAccessor&, int, const Rect&,
                                      Priority, bool);
}; // end class ImageAccessor

/// Returns true if the image is null.
//
/// A null image may either be a default constructed image, a closed image, or
/// an open but emtpy image.
SEDEEN_IMAGE_API
bool isNull(const ImageAccessor& imageAccessor);

/// Get the filename of the image.
//
/// \return
/// The full path and filename. For parsing functions, see the nonmember
/// convenience functions, below.
///
/// \relates ImageAccessor
SEDEEN_IMAGE_API
std::string filePath(const ImageAccessor& imageAccessor);

/// Request the data from a region sampled to arbitrary dimensions.
//
/// This is the primary mode of access; the other access functions wrap this
/// one. The function does not enforce the aspect ratio of the original image.
/// Manually ensure agreement, or use one of the wrapper functions, below.
///
/// \param imageAccessor
/// The image from which to extract a region.
///
/// \param scale
/// The fraction by which to scale the image. Must be a non-negative value.
///
/// \param region
/// The region to sample of the original image. If region is invalid, a NULL
/// RawImage is returned.
///
/// \param priority
/// The relative priority of this request.
///
/// \param cacheSurrounding
/// Enables caching of areas adjacent to the \a region
///
/// \return
/// A handle to the resulting RawImage. If the decoder requires more time to
/// produce the requested image, it will return a partially-decoded image and
/// the isDone() function will return FALSE. In this case, you will need to
/// connect to the imageUpdated() signal to receive incremental updates.
///
/// \relates ImageAccessor
SEDEEN_IMAGE_API
void region(ImageAccessor& imageAccessor, double scale, const Rect& region,
            Priority priority, bool cacheSurrounding = false);

SEDEEN_IMAGE_API
void region(ImageAccessor& imageAccessor, int resolution, const Rect& region,
            Priority priority, bool cache = false);

/// A blocking version of the region call.
//
/// \relates ImageAccessor
SEDEEN_IMAGE_API
RawImage regionBlock(ImageAccessor& imageAccessor, double scale,
                     const Rect& region, Priority priority,
                     bool cacheSurrounding = false);

/// A blocking version of the region call.
//
/// \relates ImageAccessor
SEDEEN_IMAGE_API
RawImage regionBlock(ImageAccessor& imageAccessor, int resolution,
                     const Rect& region, Priority priority,
                     bool cacheSurrounding = false);

/// Compute the physical dimensions of the opened image.
//
/// \relates ImageAccessor
SEDEEN_IMAGE_API
SizeF getPhysicalSize(const ImageAccessor& imageAccessor);

/// Get a scaled version of the entire image to fit in a given area.
//
/// Primarily intended for the creation of thumbnails, this function scales the
/// entire image so that it does not exceed the bounds of \c max while
/// maintaining its aspect ratio.
/// Uses region() to get the thumbnail.
//
/// \param imageAccessor
/// Image to be accessed.
///
/// \param max
/// Region in which to fit the scaled version.
///
/// \param priority
/// Relative priority of this request.
///
/// \relates ImageAccessor
SEDEEN_IMAGE_API
void thumbnail(ImageAccessor& imageAccessor, const Size& max,
               Priority priority);

/// Get a scaled version of the entire image to fit in a given area.
//
/// Primarily intended for the creation of thumbnails, this function scales the
/// entire image so that it does not exceed the bounds of \c max while
/// maintaining its aspect ratio.
/// Uses regionBlock() to get the thumbnail and is therefore a blocking call.
//
/// \param imageAccessor
/// Image to be accessed.
///
/// \param max
/// Region in which to fit the scaled version.
///
/// \param priority
/// Relative priority of this request.
///
/// \return
/// A copy of the image scaled to fit within DEFAULT_THUMBNAIL_SIZE.
///
/// \relates ImageAccessor
SEDEEN_IMAGE_API
RawImage thumbnailBlock(ImageAccessor& imageAccessor, const Size& max,
                        Priority priority);

/// Return a Rect of the entire image at a given resolution.
SEDEEN_IMAGE_API
Rect rect(const ImageAccessor& imageAccessor, int resolution);

///
//
/// \relates ImageAccessor
SEDEEN_IMAGE_API
std::string fileName(const ImageAccessor& imageAccessor);

} // end namespace image
} // end namespace sedeen

#endif
