#ifndef SEDEEN_SRC_IMAGE_IO_IMAGEDATA_H
#define SEDEEN_SRC_IMAGE_IO_IMAGEDATA_H

// System includes
#include <boost/cstdint.hpp>
#include <boost/shared_ptr.hpp>

// Qt includes
#include <QtCore>

// User includes
#include "image/io/Global.h"
#include "image/buffer/RawImage.h"
#include "global/geometry/SizeF.h"
#include "image/io/ImageIOTypes.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {

class ImageAccessor;
class ImageParser;
class SelectiveDecoder;

/// Base class for image access.
//
/// This class hierarchy exists to support read-only access to various types of images. The need for incremental access to very large (multi-gigabyte) images motivates this interface's design.
///
/// We designed this base class to work closely with the ImageAccessor class, a lightweight handle that supports signals and slots for data transmission. The dependence of ImageAccessor on this class closely resembles the pimpl idiom, with several ImageAccessor instances sharing one ImageData instance.
///
/// \note
/// This class in not thread-safe. This is left up to the concrete implementations. The class is, however, reentrant.
class SEDEEN_IMAGE_API ImageData : public QObject {
  Q_OBJECT;
 public:
  /// Virtual, public destructor.
  virtual ~ImageData();

  /// Create a copy, with an independent data source.
  boost::shared_ptr<ImageData> clone() const;

  /// Get the dimensions of the entire image, in pixels.
  //
  /// \return
  /// A valid SizeF object containing the image's dimensions.
  Size size() const;

  /// Get the original dimensions of the image's pixels.
  //
  /// Certain file formats specify this. It is used as a hint for rendering systems. This is a two-dimensional pixel specification.
  ///
  /// \return
  /// A valid SizeFF object containing the image's pixel-size.
  SizeF pixelSize() const;

  /// Get the filename of the image.
  //
  /// \return
  /// The full path and filename.
  std::string filename() const;

  /// Request the data from a region sampled to arbitrary dimensions.
  //
  /// This is the primary mode of access; the other access functions wrap this one. The function does not enforce the aspect ratio of the original image. Manually ensure agreement, or use one of the wrapper functions, below.
  ///
  /// \param client
  /// The image from which to extract a region.
  ///
  /// \param request
  /// A description of the request
  ///
  /// \param priority
  /// The relative priority of this request.
  void requestRegion(const ImageAccessor& client,
                     const DecodeRequestPtr& request,
                     Priority priority);

  /// Remove any requests by this ImageAccessor from the decoder.
  //
  /// \param client
  /// ImageAccessor class originating the requests.
  void cancelRequest(const ImageAccessor& client);

  /// Gets the buffered tiles for the given scale factor
  ///
  /// \param scale
  RawImage bufferImage(double scale) const;

  SelectiveDecoder* selectiveDecoder() const;

  /// Get an ImageParser object.
  const ImageParser& imageParser() const;

  /// Sets the desired buffering limit for this image.
  /// This limit may not be honoured by the underlying image data
  /// \param bytes
  /// \return number of bytes actually used
  boost::uint64_t setBufferCapacity(boost::uint64_t bytes);

  /// Gets the buffering limit for this image
  /// \return bytes
  boost::uint64_t bufferCapacity() const;

  /// Gets the current buffer size for this image.
  /// \return bytes
  boost::uint64_t bufferSize() const;

  /// Gets the buffering limit for this image is fixed or variable.
  /// \return fixed if not NULL his set to TRUE if the buffer limit for image at \a index is fixed, and FALSE if its changable
  bool isBufferCapacityFixed() const;

  /// Gets the buffer size required for a the given screen resolution.
  //
  /// \param width screen width
  /// \param height screen height
  /// \return bytes
  boost::uint64_t bufferSizeRequired(int width, int height) const;

signals:
  void decoderCaching(int);

 protected:
  /// Protected constructor.
  ImageData();

  /// Protected copy constructor.
  ImageData(const ImageData& imageData);

  /// Update the client with a more recent image.
  //
  /// Mechanism by which the derived classes may update a client's image data. This offers a control bottleneck to help avoid calling invalid ImageAccessor objects.
  ///
  /// \param client
  /// ImageAccessor to be updated. No check is made for the validity of the referenced object.
  ///
  /// \param image
  /// A handle to the image data to be transmitted.
  ///
  /// \param complete
  /// Shall be set to \c true if this is the last update from the current request, and \c false otherwise.
  void updateClient(const ImageAccessor& client, RawImage image,
                    bool complete) const;

 private:
  // Disable assignment
  ImageData& operator=(const ImageData& imageData);

  virtual boost::shared_ptr<ImageData> doClone() const = 0;

  /// Return the dimensions of the open image.
  virtual Size doGetSize() const = 0;

  /// Return the physical dimensions of the open image.
  //
  /// By default, assumes each pixel is 1 um.
  virtual SizeF doGetPixelSize() const = 0;

  virtual std::string doGetFilename() const = 0;

  virtual RawImage doBufferImage(double scale) const = 0;

  virtual boost::uint64_t doSetBufferCapacity(boost::uint64_t bytes) = 0;

  virtual boost::uint64_t doGetBufferCapacity() const = 0;

  virtual bool doIsBufferCapacityFixed() const = 0;

  virtual boost::uint64_t doBufferSizeRequired(int width, int height) const = 0;

  virtual boost::uint64_t doGetBufferSize() const =0;

  /// Process the request for a
  virtual void doRequestRegion(const ImageAccessor& client,
                               const DecodeRequestPtr& request,
                               Priority priority) = 0;

  /// Remove all references to the named ImageAccessor.
  //
  /// The default implementation is no-op.
  ///
  /// \param client
  /// The ImageAccessor for which the request in question was made.
  virtual void doCancelRequest(const ImageAccessor& client);

  virtual SelectiveDecoder* getSelectiveDecoder() const;

  virtual const ImageParser& getImageParser() const = 0;
}; // end class ImageData

} // end class image
} // end namespace sedeen

#endif
