//main header
#include "image/io/NullPyramid.h"

// system includes
#include <cassert>
#include <vector>
#include <cmath>
#include <boost/make_shared.hpp>


namespace sedeen
{
namespace image
{

NullPyramid::NullPyramid() :
ImagePyramid()
{}

NullPyramid::~NullPyramid()
{}

int32_t NullPyramid::doGetLevel(const double scale) const
{ return 0;
}

ImagePyramidPtr NullPyramid::doGetClone() const
{ return boost::make_shared<NullPyramid>();
}


}   // namespace sedeen
}   // namespace imagePyramid.h"


