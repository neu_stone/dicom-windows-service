#ifndef SEDEEN_SRC_IMAGE_IO_THREADEDREGIONBLOCK_H
#define SEDEEN_SRC_IMAGE_IO_THREADEDREGIONBLOCK_H

// System headers
#include <QtCore>
#include <boost/shared_ptr.hpp>

// User includes
#include "image/DllApi.h"

namespace sedeen {

class Rect;

namespace image {

class RawImage;
class ImageAccessor;


/// \relates ImageAccessor
//
// This helper class enables calling regionBlock() from a worker thread.
//
/// This class creates an event loop internally, allowing the ImageAccessor
/// signals to be received in the thread this object is instantiated.
//
/// \note regionBlock() will never return if used in a thread that does not 
/// have a event loop installed.
//
/// \note Do not use in a thread that already has an event loop.
//
/// \code 
///   ThreadedRegionBlock blocker(accessor, resolution, region);
///   RawImage image = blocker.getImage();
/// \endcode
class SEDEEN_IMAGE_API ThreadedRegionBlock : public QObject {
  Q_OBJECT;

 public:
  /// Calls region block with the arguments provided
  //
  /// Constructor is a blokcing call. When done, call getImage() to retrieve
  /// the results.
  ThreadedRegionBlock(const ImageAccessor &accessor, int resolution, 
                      const Rect &region);

  /// Returns the results of the parameters passed into the constructor
  RawImage getImage() const;

 private slots:
  /// Catches the imageUpdated signals
  void imageUpdated(const RawImage& image, bool complete);

 private:
  class Private;

  // Cannot use boost scoped_ptr in QObjects - for unknown reason(s)
  // convert to scoped_ptr when QObject is removed
  boost::shared_ptr<Private> d;
};


} // namespace image
} // namespace sedeen

#endif // SEDEEN_SRC_IMAGE_IO_THREADEDREGIONBLOCK_H

