#ifndef SEDEEN_SRC_IMAGE_IO_OPENTILEDIMAGE_H
#define SEDEEN_SRC_IMAGE_IO_OPENTILEDIMAGE_H

// System headers
#include <QtCore>
#include <memory>

// User headers
#include "image/io/ImageOpenHandler.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {
class SelectiveDecoder;

/// Branch of ImageOpenHandlers that return TiledImage classes.
//
/// These all use a SelectiveDecoder derivative to access image data.
class SEDEEN_IMAGE_API OpenTiledImage : public ImageOpenHandler {
 protected:
  OpenTiledImage();

  /// Intermediate class. Does not offer a public interface.
  ~OpenTiledImage();

 private:
  virtual
  std::auto_ptr<ImageData>
  doOpen(const std::string& filename) const;

  /// Return a selective decoder for the target file.
  //
  /// \param filename
  /// The target file to be opened.
  ///
  /// \return
  /// A valid selective decoder on success and a null auto_ptr on failure.
  virtual
  std::auto_ptr<SelectiveDecoder>
  getSelectiveDecoder(const std::string& filename) const = 0;

}; // end class OpenTiledImage
} // end namespace image
} // end namespace sedeen

#endif
