//main header
#include "image/io/ImagePyramid.h"

// system headers
#include <cassert>
#include <cmath>
#include <algorithm>
#include <functional>

// user includes
#include "global/Debug.h"
#include "global/MathOps.h"
#include "global/geometry/Point.h"
#include "global/geometry/Rect.h"
#include "global/geometry/Size.h"
#include "image/io/Exceptions.h"

namespace sedeen {
namespace image {

namespace {

bool greaterWidth(const Size& ele1, const Size& ele2) {
  return (ele1.width() > ele2.width() ? true : false);
}
std::vector<SizeF> calculateScales(const std::vector<Size> &dims) {
  std::vector<SizeF> scales(dims.size(), SizeF());
  for (unsigned int lvl = 0; lvl < dims.size(); lvl++) {
    // test for non-zero positive dimensions
    if (isEmpty(dims[lvl]))
      throw InvalidPyramid("Invalid pyramid dimensions (widths and heights must"
                           " be postive and non-zero).");

    // calculate scales at each level
    scales[lvl] = SizeF(static_cast<double>(dims[lvl].width())/dims[0].width(),
                        static_cast<double>(dims[lvl].height())/dims[0].height()
                        );
  }

  // Ensure image dimensions at all levels are proportional.
  for (unsigned int lvl = 1; lvl < dims.size(); lvl++) {
    // Calculate the scale between adjacent levels
    //   Use scale factor for the longest dimention; it is most precise.
    const double scale = (dims[lvl].width() >= dims[lvl].height() ?
                   static_cast<double>(dims[lvl].width())/dims[lvl-1].width() :
                   static_cast<double>(dims[lvl].height())/dims[lvl-1].height());

    // TEst proportionality
    const double w = (scale * dims[lvl-1].width() ) - dims[lvl].width();
    const double h = (scale * dims[lvl-1].height()) - dims[lvl].height();
    if (std::fabs(h) > 2 ||
        std::fabs(w) > 2)
      throw InvalidPyramid("Invalid pyramid dimensions (widths and heights are "
                           "not porportional).");
  }

  return scales;
}

} // namespace

ImagePyramid::ImagePyramid(const std::vector<Size> &imageSize,
                           const Size &tileSize,
                           const std::vector<Size> &tileOffset)
    : _rows (std::vector<int32_t>(imageSize.size(),0)),
      _cols (std::vector<int32_t>(imageSize.size(),0)),
      _imgSize(imageSize),
      _tileSpan(std::vector<Size>(imageSize.size(),Size())),
      _tileOffset(tileOffset),
      _scale() {
  debug::assert_x( tileOffset.size() == imageSize.size(),
                   "Size of TileOffset vector must equal the imageSize vector",
                   __FUNCTION__, __LINE__);
  init(tileSize);
}

ImagePyramid::ImagePyramid(const std::vector<Size> &imageSize,
                           const Size &tileSize)
    : _rows (std::vector<int32_t>(imageSize.size(),0)),
      _cols (std::vector<int32_t>(imageSize.size(),0)),
      _imgSize(imageSize),
      _tileSpan(std::vector<Size>(imageSize.size(),Size())),
      _tileOffset(std::vector<Size>(imageSize.size(),Size(0,0))),
      _scale() {
  init(tileSize);
}

ImagePyramid::ImagePyramid()
    : _rows (std::vector<int32_t>(1,0)),
      _cols (std::vector<int32_t>(1,0)),
      _imgSize(std::vector<Size>(1,Size(0,0))),
      _tileSpan(std::vector<Size>(1,Size(1,1))),
      _tileOffset(std::vector<Size>(1,Size(0,0))),
      _scale(std::vector<SizeF>(1,SizeF(1.,1.))) {}


void ImagePyramid::init(const Size &tileSize) {
  // sort arrays
  std::sort(_imgSize.begin(), _imgSize.end(), greaterWidth);

  // test given dimensions
  _scale = calculateScales(_imgSize);

  // calculate pyramid properties
  for (int32_t lvl = 0; lvl < levels(); lvl++) {
    _tileSpan[lvl]  = Size( static_cast<int32_t>(std::ceil((double)tileSize.width()  / _scale[lvl].width())),
                            static_cast<int32_t>(std::ceil((double)tileSize.height() / _scale[lvl].height())) );
    _cols[lvl]  = (int32_t) std::ceil( sedeen::math_ops::intdiv(_imgSize[0].width(), _tileSpan[lvl].width()) );
    _rows[lvl]  = (int32_t) std::ceil( sedeen::math_ops::intdiv(_imgSize[0].height(), _tileSpan[lvl].height()) );
  }
}

ImagePyramid::~ImagePyramid() {}

ImagePyramidPtr ImagePyramid::clone() const {
  return doGetClone();
}

const std::vector<Size>& ImagePyramid::sizeVec() const {
  return _imgSize;
}

int32_t ImagePyramid::levels() const {
  return _imgSize.size();
}

int32_t ImagePyramid::level(const double scale) const {
  assert(scale > 0);
  return doGetLevel(scale);
}

const Size& ImagePyramid::tileSize() const {
  assert(0<levels());
  return _tileSpan[0];
}

const Size& ImagePyramid::tileSpanSize(int32_t level) const {
  assert((0<=level) && (level<levels()));
  return _tileSpan[level];
}

const Size& ImagePyramid::size(int32_t level) const {
  assert((0<=level) && (level<levels()));
  return _imgSize[level];
}

Rect ImagePyramid::rect(int32_t level) const {
  assert((0<=level) && (level<levels()));
  return Rect(Point(0, 0), size(level));
}

int32_t ImagePyramid::count(int32_t level) const {
  assert((0<=level) && (level<levels()));
  return _rows[level] * _cols[level];
}

int32_t ImagePyramid::rows(int32_t level) const {
  assert((0<=level) && (level<levels()));
  return _rows[level];
}

int32_t ImagePyramid::cols(int32_t level) const {
  assert((0<=level) && (level<levels()));
  return _cols[level];
}

const SizeF&ImagePyramid::scale(int32_t level) const {
  assert((0<=level) && (level<levels()));
  return _scale[level];
}

const Size& ImagePyramid::tileOffset(int32_t level) const {
  assert((0<=level) && (level<levels()));
  return _tileOffset[level];
}

Rect tileSpan(const ImagePyramid& pyramid, int32_t level, int32_t tileno) {
  assert((0<=level) && (level<pyramid.levels()));
  assert((0<=tileno) && (tileno<pyramid.count(level)));

  int32_t row = tileno / pyramid.cols(level);
  int32_t col = tileno % pyramid.cols(level);
  return tileSpan(pyramid, level, row, col);
}

Rect tileRect(const ImagePyramid& pyramid, int32_t level, int32_t tileno) {
  assert((0<=level) && (level<pyramid.levels()));
  assert((0<=tileno) && (tileno<pyramid.count(level)));

  int32_t row = tileno / pyramid.cols(level);
  int32_t col = tileno % pyramid.cols(level);
  return tileRect(pyramid, level, row, col);
}

Rect tileSpan(const ImagePyramid& pyramid, int32_t level, int32_t row,
              int32_t col) {
  assert((0<=level) && (level<pyramid.levels()));
  assert((0<=row) && (row<pyramid.rows(level)));
  assert((0<=col) && (col<pyramid.cols(level)));

  // Number of elements constituting a tile
  Size tileSize = pyramid.tileSpanSize(level);

  // Offset of the tile grid origin from the image grid origin
  Point offset = Point(pyramid.tileOffset(0).width(),
                       pyramid.tileOffset(0).height());

  // Location of the tile in question within the tile grid
  Point location = Point(col, row);

  Rect footprint = Rect(tileSize * location - offset, tileSize);
  Rect imageBoundary = Rect(Point(0, 0), pyramid.size(0));
  return intersection(footprint, imageBoundary);
}

Rect tileRect(const ImagePyramid& pyramid, int32_t level, int32_t row,
              int32_t col) {
  assert((0<=level) && (level<pyramid.levels()));
  assert((0<=row) && (row<pyramid.rows(level)));
  assert((0<=col) && (col<pyramid.cols(level)));

  Size tileSize = pyramid.tileSize();
  Point location = Point(col, row);
  Point offset = Point(pyramid.tileOffset(level).width(),
                       pyramid.tileOffset(level).height());

  return intersection(Rect(tileSize*location - offset, tileSize),
                      (Rect(Point(0, 0), pyramid.size(level))));
}

}   // namespace image
}   // namespace sedeen
