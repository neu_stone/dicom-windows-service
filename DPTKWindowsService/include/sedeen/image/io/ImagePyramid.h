#ifndef SEDEEN_SRC_IMAGE_IO_IMAGEPYRAMID_H
#define SEDEEN_SRC_IMAGE_IO_IMAGEPYRAMID_H

#include <vector>

// user includes
#include "global/geometry/Size.h"
#include "global/geometry/SizeF.h"
#include "image/DllApi.h"
#include "image/io/ImageIOTypes.h"
#include "image/io/Tile.h"

namespace sedeen {
namespace image {
    /*!
        \class ImagePyramid
        \brief Read-only structure that describes the structure for a multi-resolutional image.

        Note that resolution level 0 must the represents full resolution image.
     */
class SEDEEN_IMAGE_API ImagePyramid {
public:

        /*!
            \copydoc ImagePyramid(const std::vector<Size>&,const Size&)
            \param tileOffsets the offset of the first tile offset at each resolution level (see JPEG-2000 standard for utility) from the tile origin to the image origin.
        */
        ImagePyramid(  const std::vector<Size> &imageSize,
                       const Size& tileSize,
                       const std::vector<Size> &tileOffsets);

        /*!
            \brief Creates a image pyramid structure where the number of resolution levels and size of each resolution is give by \a imagSize.
            \param imageSize vector containing image dimensions (positive and non-zero) at each resolution level
            \param tileSize the entire image pyramid must have a uniform tile size
        */
        ImagePyramid(  const std::vector<Size> &imageSize,
                       const Size& tileSize);

        /// \brief constructs a null pyramid. Contains one level with 0 dimensions
        ImagePyramid();

        /*!
            \brief Destructor
        */
        virtual ~ImagePyramid();

        /// \brief Clones the pyramid
        ImagePyramidPtr clone() const;

        /*!
            \brief Gets the number of resolution levels.
            \return the number of resolution levels.
        */
        int32_t levels() const;

        /*!
            \brief Gets offset of the top-left tile at each resolution
            \return tile offset
        */
        const Size& tileOffset(int32_t level) const;

        /*!
            \brief Gets the image's tile size. Tiles have the same dimensions at all levels.
        */
        const Size& tileSize() const;

        /*!
            \brief Gets the image's tile size  at resolution \a level.

            Note: Tiles at \a level > 0, have physical size given by
            tileWidth() and tileHeight() but represent a larger region
            of the image due to decimation.

            \param level the resolution level
        */
        const Size& tileSpanSize(int32_t level) const;

        /*!
            \brief Gets the image size at resolution \a level.
            \param level the resolution level
        */
        const Size& size(int32_t level) const;

        /*!
            \brief Gets the image rectangle at resolution \a level.
            \param level the resolution level
        */
        Rect rect(int32_t level) const;

        /*!
            \brief Gets the number of tiles at resolution \a level.
            \param level the resolution level
            \return the image tile height.
        */
        int32_t count(int32_t level) const;

        /*!
            \brief Gets the number of tiles spanning the image horizontally at resolution \a level.
            \param level the resolution level
            \return the number of tile in the horizontal direction
        */
        int32_t rows(int32_t level) const;

        /*!
            \brief Gets the number of tiles spanning the image vertically at resolution \a level.
            \param level the resolution level
            \return the number of tile in the vertical direction
        */
        int32_t cols(int32_t level) const;

        /*!
            \brief Gets the scale of the image at resolution \a level.
            \note The horizontal and vertical scale factors may be slightly different. The dimensions of lower resolutions images are always rounded to an integer and therefore an exact (and uniform) scale factor in both dimentions is not always possible.
            \param level the resolution level
            \return the image scale factor, typically in the range (0,1]
        */
        const SizeF& scale(int32_t level) const;

        /*!
            \brief Gets the resolution level closest to the given scale.
            \param scale
            \return best resolution level
        */
        int32_t level(const double scale) const;

        /*!
            \brief REturns the size of all levels
        */
        const  std::vector<Size>& sizeVec() const;

    protected:
        virtual int32_t doGetLevel(const double scale) const = 0;
        virtual ImagePyramidPtr doGetClone() const = 0;

    private:
        void init(const Size &tileSize);

        // do not allow copying
        ImagePyramid(const ImagePyramid&);

    private:
        int32_t _xoff, _yoff;
        std::vector<int32_t> _rows, _cols;
        std::vector<Size>   _imgSize, _tileSpan, _tileOffset;
        std::vector<SizeF> _scale;
    };

    /*!
        \relates ImagePyramid
        \brief Gets the region spaned by the give tile on the full resolution image grid.
        \note Tiles at \a level > 0, have physical size given by tileWidth() and tileHeight() but represent a larger region of the image due to decimation.

        see sedeen::image::tileSpan(const ImagePyramid&,int32_t,int32_t,int32_t), see sedeen::image::tileRect(const ImagePyramid&,int32_t,int32_t,int32_t)
        \param pyramid
        \param level the resolution level
        \param tileno the specific tile
        \return the tile properties
    */
SEDEEN_IMAGE_API
Rect tileSpan(const ImagePyramid& pyramid, int32_t level, int32_t tileno);

    /*!
        \relates ImagePyramid
        \brief Gets the region spaned by the give tile on the full resolution image grid.
        see sedeen::image::tileSpan(const ImagePyramid&,int32_t,int32_t)
        \param pyramid
        \param level the resolution level
        \param row
        \param col
        \return the tile properties
    */
SEDEEN_IMAGE_API
Rect tileSpan(const ImagePyramid& pyramid, int32_t level, int32_t row, int32_t col);

   /*!
        \relates ImagePyramid
        \brief Gets the region spaned by the give tile on the give resolution image grid.
        see sedeen::image::tileSpan(const ImagePyramid&,int32_t,int32_t), sedeen::image::tileRect(const ImagePyramid&,int32_t,uint32_t)
        \param pyramid
        \param level the resolution level
        \param tileno
        \return the tile properties
    */
SEDEEN_IMAGE_API
Rect tileRect(const ImagePyramid& pyramid, int32_t level, int32_t tileno);

    /*!
        \relates ImagePyramid
        \brief Gets the region spaned by the give tile on the give resolution image grid.
        see sedeen::image::tileSpan(const ImagePyramid&,uint32_t,uint32_t), sedeen::image::tileRect(const ImagePyramid&,int32_t,int32_t)
        \param pyramid
        \param level the resolution level
        \param row
        \param col
        \return the tile properties
    */

/// Gets the tile rect
SEDEEN_IMAGE_API
Rect tileRect(const ImagePyramid& pyramid, int32_t level, int32_t row, int32_t col);

}   // namespace image
}   // namespace sedeen
#endif // SEDEEN_SRC_IMAGE_IO_IMAGEPYRAMID_H


