#ifndef SEDEEN_SRC_IMAGE_IO_NULLIMAGEDATA_H
#define SEDEEN_SRC_IMAGE_IO_NULLIMAGEDATA_H

#include "image/io/ImageData.h"
#include "image/io/NullImageParser.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {

/// Lightweight non-functional derivative of the Image class.
//
/// Since Image classes have no zombie state, this class may be used when a null image is required. For example, ImageDecorator uses an instance of NullImage to avoid requiring null tests on the decorated image.
class SEDEEN_IMAGE_API NullImage : public ImageData {
 public:
  NullImage();
  virtual ~NullImage();

  NullImage(const NullImage& nullImage);

 private:
  /// Disable assignment
  NullImage& operator=(const NullImage& nullImage);

  boost::shared_ptr<ImageData> doClone() const;

  Size doGetSize() const;

  SizeF doGetPixelSize() const;

  std::string doGetFilename() const;

  boost::uint64_t doSetBufferCapacity(boost::uint64_t bytes);

  boost::uint64_t doGetBufferCapacity() const;

  bool doIsBufferCapacityFixed() const;

  boost::uint64_t doBufferSizeRequired(int width, int height) const;

  boost::uint64_t doGetBufferSize() const;

  RawImage doBufferImage(double scale) const;

  void doRequestRegion(const ImageAccessor&,
                       const DecodeRequestPtr&,
                       Priority);

  const ImageParser& getImageParser() const;

  NullImageParser image_parser_;
}; // class NullImage

SEDEEN_IMAGE_API
const boost::shared_ptr<ImageData>& nullImageData();

} // end class image
} // end namespace sedeen

#endif
