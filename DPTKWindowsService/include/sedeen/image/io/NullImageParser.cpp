// Primary header
#include "image/io/NullImageParser.h"

#include <string>

#include "image/io/NullPyramid.h"

namespace sedeen {
namespace image {

NullImageParser::NullImageParser()
    : image_pyramid_(new NullPyramid()) {}

NullImageParser::~NullImageParser() {}

ImagePyramidPtr NullImageParser::doGetPyramid() const {
  return image_pyramid_;
}

bool NullImageParser::doGetHasLabelImage() const {
  return false;
}

bool NullImageParser::doGetHasMacroImage() const {
  return false;
}

bool NullImageParser::doGetHasThumbnailImage() const {
  return false;
}

std::string NullImageParser::doGetFilename() const {
  return "";
}

SizeF NullImageParser::doGetPixelSize() const {
  return SizeF(0,0);
}

double NullImageParser::doGetMagnification() const {
  return 0;
}

int NullImageParser::doGetComponents() const {
  return 0;
}

int NullImageParser::doGetBpp() const {
  return 0;
}

int NullImageParser::doGetBpc(int) const {
  return 0;
}

std::string NullImageParser::doGetProducer() const {
  return "Unknown";
}

Color NullImageParser::doGetColor() const {
  return image::Unknown;
}

std::string NullImageParser::doGetCompression() const {
  return "";
}

std::string NullImageParser::doGetDescription() const {
  return "";
}

RawImage NullImageParser::doGetLabelImage() const {
  return RawImage();
}

RawImage NullImageParser::doGetMacroImage() const {
  return RawImage();
}

RawImage NullImageParser::doGetThumbnailImage() const {
  return RawImage();
}

} // namespace image
} // namespace sedeen
