#ifndef SEDEEN_SRC_IMAGE_IO_EXCEPTIONS_H
#define SEDEEN_SRC_IMAGE_IO_EXCEPTIONS_H

// System includes
#include <stdexcept>
#include <string>

#include "image/DllApi.h"

namespace sedeen {
namespace image {

/// Thrown on file access problems
class SEDEEN_IMAGE_API FileAccessError : public std::runtime_error {
 public:
  /// Sole constructor.
  //
  /// \param filename
  /// Filename whose access raised the exception.
  FileAccessError(const std::string& filename);
}; // end class FileAccessError

/// Thrown when an ImageOpenHandler stack rejects a filename.
class SEDEEN_IMAGE_API UnhandledFormatError : public std::runtime_error {
 public:
  /// Sole constructor.
  //
  /// \param filename
  /// Filename whose format could not be read by any of the available filters.
  UnhandledFormatError(const std::string& filename);
}; // end class UnhandledFormatError


/// Thrown when an ImagePyramid with invalid dimensions is instantiated.
class SEDEEN_IMAGE_API InvalidPyramid : public std::runtime_error {
 public:
  /// Sole constructor.
  //
  /// \param error
  /// Filename whose format could not be read by any of the available filters.
  InvalidPyramid(const std::string& error);
}; // end class InvalidPyramid

/// Thrown when an image is not fouund
class SEDEEN_IMAGE_API InvalidImage : public std::runtime_error {
 public:
  /// Sole constructor.
  //
  /// \param filename
  /// Filename that was attempted and not opened - either cause its does not exists or due to insuffucient permissions
  ///
  /// \param error
  InvalidImage(const std::string& filename, const std::string& error);
}; // end class InvalidImage

} // end namespace image
} // end namespace sedeen

#endif
