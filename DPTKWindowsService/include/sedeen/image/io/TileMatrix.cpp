// Main header
#include "image/io/TileMatrix.h"

// System headers
#include <cassert>
#include <algorithm>

namespace sedeen
{
namespace image
{

TileMatrix::Proxy::Proxy(const TileMatrix &matrix, uint32_t row) :
_row(row),
_matrix(matrix)
{}

const Tile& TileMatrix::Proxy::operator[](uint32_t col) const
{
    return _matrix.tile(_row, col);
}

TileMatrix::Proxy TileMatrix::operator[](uint32_t row) const
{
    return Proxy(*this,row);
}

const Tile& TileMatrix::operator()(uint32_t row, uint32_t col) const
{
    return tile(row,col);
}

TileMatrix::TileMatrix( uint32_t rows,
                        uint32_t cols,
                        PurgeMode mode) :
TileVector(rows*cols, mode),
_rows(rows),
_cols(cols)
{}

TileMatrix::~TileMatrix()
{
}

uint32_t TileMatrix::cols() const
{
    return _cols;
}

uint32_t TileMatrix::rows() const
{
    return _rows;
}

void TileMatrix::setTileImage(uint32_t tileno, RawImage image, TileState state, bool isReferenceForPurge)
{
    assert( tileno < size() );
    TileVector::setTileImage(tileno, image, state, isReferenceForPurge);
}

void TileMatrix::setTileImage(uint32_t row, uint32_t col, RawImage image, TileState state, bool isReferenceForPurge)
{
    setTileImage( getTileno(row,col),
                  image, state,
                  isReferenceForPurge);
}

const Tile& TileMatrix::tile(uint32_t row, uint32_t col) const
{
    return tile( getTileno(row, col) );
}

uint32_t TileMatrix::getTileno(uint32_t row, uint32_t col)  const
{
    assert( col < cols() );
    assert( row < rows() );

    return cols()*row + col;
}

TileVector::DistanceTileMMapPtr TileMatrix::purgeListFarthest() const
{
    TileVector::DistanceTileMMapPtr sorted( new TileVector::DistanceTileMMap );
    int32_t refRow = purgeReferenceTile() % cols();
    int32_t refCol = purgeReferenceTile() / cols();

    TileTimeMapItrConst it = begin();
    while( it != end() )
    {
        int32_t row = it->first % cols();
        int32_t col = it->first / cols();
        double dx = refCol - col;
        double dy = refRow - row;
        dx *= dx;
        dy *= dy;

        sorted->insert( TileVector::DistanceTileMMap::value_type((uint32_t)(dx+dy),it->first) );
        ++it;
    }
    return sorted;
}

TileVector::DistanceTileMMapPtr TileMatrix::purgeListFarthestRing() const
{
    TileVector::DistanceTileMMapPtr sorted( new TileVector::DistanceTileMMap );
    int32_t refRow = purgeReferenceTile() % cols();
    int32_t refCol = purgeReferenceTile() / cols();

    TileTimeMapItrConst it = begin();
    while( it != end() )
    {
        int32_t row = it->first % cols();
        int32_t col = it->first / cols();
        int32_t dist = std::max( std::abs(refCol - col),
                                 std::abs(refRow - row) );

        sorted->insert( TileVector::DistanceTileMMap::value_type(dist,it->first) );
        ++it;
    }
    return sorted;
}


}   // namespace sedeen
}   // namespace image

