// main header
#include "image/io/Tile.h"

// system header
#include <cassert>


namespace sedeen
{
namespace image
{

Tile::Tile( RawImage image,
            TileState state
            ) :

_img(image),
_state(state)
{ assert( !(image.isNull() ^ (TileEmpty == state)) );
}

Tile::~Tile()
{}

TileState Tile::state() const
{ return _state;
}

RawImage Tile::image() const
{ return _img;
}

void Tile::clear()
{ _state = TileEmpty;
  _img = RawImage();
}

void Tile::setImage(const RawImage &image, const TileState state)
{ assert( !(image.isNull() ^ (TileEmpty == state)) );
  _img = image;
  _state = state;
}

uint32_t Tile::width() const
{ return _img.width();
}

uint32_t Tile::height() const
{ return _img.height();
}

}   //namespace image
}   //namespace sedeen

