// Primary header
#include "image/io/ImageData.h"

// User includes
#include "global/Debug.h"
#include "global/UnitDefs.h"
#include "global/geometry/Size.h"
#include "image/io/DecodeRequest.h"
#include "image/io/ImageAccessor.h"

namespace sedeen {
namespace image {

ImageData::~ImageData() {}

boost::shared_ptr<ImageData> ImageData::clone() const {
  boost::shared_ptr<ImageData> aClone = doClone();
  debug::assert_x(typeid(*aClone) == typeid(*this) ,
           "doClone incorrectly overridden");
  return aClone;
}

Size ImageData::size() const {
  Size s = doGetSize();
  return s;
}

SizeF ImageData::pixelSize() const {
  SizeF ps = doGetPixelSize();
  return ps;
}

std::string ImageData::filename() const {
  return doGetFilename();
}

boost::uint64_t ImageData::setBufferCapacity(boost::uint64_t bytes){
  return doSetBufferCapacity(bytes);
}

bool ImageData::isBufferCapacityFixed() const{
  return doIsBufferCapacityFixed();
}

boost::uint64_t ImageData::bufferCapacity() const{
  return doGetBufferCapacity();
}

boost::uint64_t ImageData::bufferSizeRequired(int width, int height) const {
  return doBufferSizeRequired(width, height);
}

boost::uint64_t ImageData::bufferSize() const {
  return doGetBufferSize();
}

RawImage ImageData::bufferImage(double scale) const{
  return doBufferImage(scale);
}

void ImageData::requestRegion(const ImageAccessor& client,
                              const DecodeRequestPtr& request,
                              Priority priority) {
  debug::assert_x(request.get(), "Request is NULL", __FUNCTION__);

  // Pass on call to derived class if there is a valid area to decode
  if (!request->isNull()) {
    doRequestRegion(client, request, priority);
  } else {
    // Signal an update of an empty image
    updateClient(client, RawImage(), true);
  }
}

void ImageData::cancelRequest(const ImageAccessor& client) {
  doCancelRequest(client);
}

SelectiveDecoder* ImageData::selectiveDecoder() const {
  return getSelectiveDecoder();
}

const ImageParser& ImageData::imageParser() const {
  return getImageParser();
}

ImageData::ImageData() : QObject() {
}

ImageData::ImageData(const ImageData& image_data) {
}

void ImageData::updateClient(const ImageAccessor& client, RawImage image,
                             bool complete) const {
  //debug::message("\tImageData::updateClient()");
  client.updateImage(image, complete);
}

void ImageData::doCancelRequest(const ImageAccessor&) {}

SelectiveDecoder* ImageData::getSelectiveDecoder() const {
  return 0;
}

} // namespace image
} // namespace sedeen
