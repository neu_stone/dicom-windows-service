#ifndef SEDEEN_SRC_IMAGE_IO_TILE_H
#define SEDEEN_SRC_IMAGE_IO_TILE_H

// System headers
#include <boost/cstdint.hpp>

// User headers
#include "image/buffer/RawImage.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {
    /*!
	 	Tile state enum.
	 */
	enum TileState{
			TileEmpty,		///< buffer-tile has not been decoded
			TileFuzzy,		///< buffer-tile is set to be decoded and is currently fuzzy
			TileDetailed	///< buffer-tile has been fully decoded
    };

    /*!
        \class Tile
        \brief A tile in a multi-resolution pyramid structure.

        see image() and setImage().
	 */
class SEDEEN_IMAGE_API Tile {
    public:
        Tile(   RawImage image = RawImage(),
                TileState state = TileEmpty
                );
        ~Tile();

        TileState state() const;
        RawImage image() const;
        uint32_t width() const;
        uint32_t height() const;
        void clear();
        void setImage(const RawImage &image, const TileState state);

    private:
		RawImage      _img;		///< image that hold the tile data
		TileState	_state;	    ///< tile status value
	};


}   // namespace sedeen
}   // namespace image
#endif // ifdef SEDEEN_SRC_IMAGE_IO_TILE_H


