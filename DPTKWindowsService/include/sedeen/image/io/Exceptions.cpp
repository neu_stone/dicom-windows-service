// Primary header
#include "image/io/Exceptions.h"

namespace sedeen {
namespace image {
namespace {

/// The file open error message.
const std::string kFileAccessErrorMsg = "Cannot access file: ";

} // namespace

FileAccessError::FileAccessError(const std::string& filename)
    : runtime_error(kFileAccessErrorMsg + filename) {}

namespace {

/// The unhandled format error message.
const std::string kUnhandledFormatErrorMsg =
    "No suitable open handler was found for: ";

} // namespace

UnhandledFormatError::UnhandledFormatError(const std::string& filename)
    : runtime_error(kUnhandledFormatErrorMsg + filename) {}

InvalidPyramid::InvalidPyramid(const std::string& error)
    : runtime_error(error) {}

InvalidImage::InvalidImage(const std::string& filename, const std::string& error)
    : runtime_error(error + ": " + filename) {}


} // namespace image
} // namespace sedeen
