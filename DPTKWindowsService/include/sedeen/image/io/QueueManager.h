#ifndef SEDEEN_SRC_IMAGE_IO_QUEUEMANAGER_H
#define SEDEEN_SRC_IMAGE_IO_QUEUEMANAGER_H

// System includes
#include <QtCore>
#include <functional>
#include <ostream>
#include <vector>

// User includes
#include "image/io/Global.h"
#include "image/io/ImageIOTypes.h"
#include "image/DllApi.h"

namespace sedeen {
namespace image {

class ImageAccessor;

namespace queue_manager {

// Class defined in QueueManager.cpp
class Request;

}

/// Serializes access to a SelectiveDecoder class.
//
/// To control the amount of memory used when opening a selectively-decoded
/// image, Sedeen currently only opens one SelectiveDecoder derivative per
/// image. If multiple clients require access, this class offers shared,
/// prioritized access.
///
/// AccessScheduler's interface expects a TiledImage class to access it.
///
/// A note for future refactoring: to save on overhead, the AccessScheduler
/// calls the client TiledImage class directly to update the image information.
/// The TiledImage destructor calls cancelRequest() to avoid accessing
/// deallocated memory. In order to generalize this mechanism, a slot/signal
/// method may be required.
///
/// This class is not thread-safe, but is reentrant.
class SEDEEN_IMAGE_API QueueManager {
 public:
  /// Sole constructor.
  QueueManager();

  /// Destructor.
  ~QueueManager();

  /// Submit a new request.
  bool submitRequest(const ImageAccessor* client,
                     const DecodeRequestPtr& request, Priority priority);

  /// Cancel any request(s) enqueued by this client.
  //
  /// \param client
  /// The originaing client.
  ///
  /// \return
  /// TRUE if the current request has been altered.
  bool cancelRequest(const ImageAccessor* client);

  /// Mark the current task as complete and promote the next.
  //
  /// \return
  /// \c true if there is another task in the queue, else \c false.
  bool next();

  /// Do not call if queue is empty.
  const ImageAccessor* client() const;

  /// Do not call if queue is empty.
  const DecodeRequestPtr& request() const;

  /// Check if there are still queued jobs.
  bool empty() const;

 private:
  Q_DISABLE_COPY(QueueManager)

  /// Remove all Request objects with a matching client.
  //
  /// \param client
  /// The client whose request objects will be purged.
  ///
  /// \return
  /// TRUE if the modification affected the active request.
  bool purge(const ImageAccessor* client);

  /// Insert the request in ordered in the queue.
  //
  /// \param req
  /// Request to be added to the queue (in order of priority).
  ///
  /// \return
  /// TRUE if the modification affected the active request.
  bool enqueue(const queue_manager::Request& req);

  /// The ordered queue of requests.
  std::vector<queue_manager::Request> queue_;

  friend SEDEEN_IMAGE_API
    std::ostream& operator<<(std::ostream&, const QueueManager&);
}; // end class QueueManager

/// Stream the state of the queue.
//
/// Primarily for diagnostic reasons.
///
/// \relates QueueManager
SEDEEN_IMAGE_API
std::ostream& operator<<(std::ostream& out, const QueueManager& qm);

} // end namespace image
} // end namespace sedeen

#endif
