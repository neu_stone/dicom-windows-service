// Primary header
#include "image/io/SelectiveDecoder.h"

// System includes
#include <algorithm>
#include <iostream>
#include <limits>
#include <string>
#include <boost/make_shared.hpp>
#include <QtGui>
#include <QtCore>

// User includes
#include "global/MathOps.h"
#include "global/Debug.h"
#include "global/geometry/Point.h"
#include "image/io/RequestBySize.h"
#include "image/io/RequestByResolution.h"
#include "image/io/ImagePyramid.h"
#include "image/io/ImageParser.h"

namespace sedeen {
namespace image {
namespace{
const DecodeRequestPtr NullRequest = boost::make_shared<RequestByResolution>();

// Cache - Number of rings around requested view
const uint32_t RingsToCache = 1000;     

// Cache - current and coarser
const uint32_t LevelsToCache = 2;       

// Buffer size - minimum request multiple
const uint32_t MinimumRequestMultiple = 1;   

// Determines minimum resolution of thumbnial
const double MinimumThumbnailScale = 0.06;   

// Default error messages
const std::string RegionErrorMessage = "Decode regionexceeds the image area.";
const std::string MemoryErrorMessage = "Out of memory.";
const std::string DecodeErrorMessage= "Failed to decode region.";
}

const uint32_t SelectiveDecoder::DefaultFuzzyImageWidth = 1024;
const uint32_t SelectiveDecoder::DefaultBufferCapacity = 100*1024*1024; //bytes

SelectiveDecoder::SelectiveDecoder(ImageParserPtr parser, QObject *parent) :
QThread(parent),
m_parser(parser),
m_buffer(m_parser->pyramid(), PurgeFarthestRingFirst),
m_mutex(),
m_image(),
m_imageUpdateList(),
m_imageFuzzy(),
m_restart(false),
m_abort(false),
m_restartCondition(),
m_request(NullRequest),
m_requestCache(NullRequest),
m_requestChanged(false),
m_requestID(0),
m_decodeList(),
m_decodeLevel(0),
m_lastError(NoError),
m_lastErrorMessage() {
  assert(m_parser.get());

  // Set buffer capacity
  setBufferCapacity(DefaultBufferCapacity);

  // Add std:string to qt meta types
  if( qMetaTypeId<std::string>() == 0 )
      qRegisterMetaType<std::string>();
}

SelectiveDecoder::~SelectiveDecoder() {
  // ensure worker thread is done and unlocked mutex -- see documentation.
  waitForExit();
}

void SelectiveDecoder::waitForExit()
{
    this->disconnect();
    // Set abort flag
    {
        QMutexLocker lock(&m_mutex);
        m_restart        = true;
        m_abort          = true;
        m_restartCondition.wakeAll();
    }
    wait();
}

ImagePyramidPtr SelectiveDecoder::pyramid() const
{
    return m_buffer.pyramid();
}

const std::string SelectiveDecoder::filename() const
{
    return m_parser->filename();
}
const ImageParser& SelectiveDecoder::parser() const
{
    return *m_parser;
}

void SelectiveDecoder::setLastError(DecoderErrorType error,
                                    const std::string& message) {
  m_lastError = error;
  m_lastErrorMessage = message;
}

DecoderErrorType SelectiveDecoder::lastError() const {
  return m_lastError;
}

void SelectiveDecoder::emitLastErrorNoLock() {
  // private function: assumes mutex is locked
  assert(!m_mutex.tryLock());

  if (NoError != m_lastError) {
    emitErrorNoLock(m_lastError, m_lastErrorMessage);
    m_lastError = NoError;
    m_lastErrorMessage = "";
  }
}

void SelectiveDecoder::emitError(DecoderErrorType error,
                                 const std::string& message)
{
    QMutexLocker lock(&m_mutex);
    emitErrorNoLock(error, message);
}

void SelectiveDecoder::emitErrorNoLock(DecoderErrorType error,
                                       const std::string& message) {
  // private function: assumes mutex is locked
  assert(!m_mutex.tryLock());
  assert(NoError != error);

  // clear memory
  if (MemoryError == error) {
    for (int32_t i=0; i < pyramid()->levels(); i++)
      m_buffer.purge(i, m_buffer.capacity(i)/2);
  }

  // Set message
  std::string error_message = message;
  if (message.length() == 0 ) {
    switch( error ) {
      default: assert(false);
      case NoError: return;
      case RegionError: error_message = RegionErrorMessage; break;
      case MemoryError: error_message = MemoryErrorMessage; break;
      case DecodeError: error_message = DecodeErrorMessage; break;
    }
  }

  clearRequestNoLock();
  emit decoderError(error_message);
}

uint64_t SelectiveDecoder::setBufferCapacity(uint64_t memsize)
{
    QMutexLocker lock(&m_mutex);

    // adjust for fuzzy image size
    // ensure a non negative subtraction
    uint64_t fuzzy_size = m_imageFuzzy.bytes();
    memsize = std::max(memsize, fuzzy_size);
    memsize -= fuzzy_size;

    // calculate the number of tiles that fit into memsize
    double tile_size = tileSize();
    double tiles_remaining = std::ceil(memsize / tile_size);

    for( int32_t lvl = pyramid()->levels() - 1; 0 <= lvl ; --lvl )
    {
        // total tiles per level
        int32_t levels_remaining = lvl + 1;
        int32_t num_tiles = static_cast<int32_t>(
                            std::ceil(tiles_remaining / levels_remaining));


        int32_t level_tiles = m_buffer.size(lvl);
        num_tiles = std::min(num_tiles, level_tiles);
        tiles_remaining -= num_tiles;

        m_buffer.setCapacity(lvl, num_tiles);
    }

    // bytes used
    int64_t bytes = static_cast<int64_t>(std::ceil(tiles_remaining*tile_size));
    return memsize - bytes;
}

uint64_t SelectiveDecoder::bufferCapacity() const
{
    QMutexLocker lock(&m_mutex);
    return tileSize() * bufferCount() + (uint64_t)m_imageFuzzy.bytes();
}

uint32_t SelectiveDecoder::bufferCount() const
{
    QMutexLocker lock(&m_mutex);
    return m_buffer.count();
}

uint64_t SelectiveDecoder::bufferSize() const
{
    QMutexLocker lock(&m_mutex);
    return (uint64_t)(m_buffer.count()) * tileSize();
}

uint64_t SelectiveDecoder::bufferSizeRequired(int width, int height) const
{
    uint64_t ntiles = 0;
    for( int32_t lvl = 0; lvl < pyramid()->levels(); lvl++)
    {
        uint32_t cols = (uint32_t)std::ceil( (double)width / m_buffer.pyramid()->tileSpanSize(lvl).width() );
        uint32_t rows = (uint32_t)std::ceil( (double)height / m_buffer.pyramid()->tileSpanSize(lvl).height() );
        ntiles += rows*cols;
    }
    return ntiles*tileSize();
}

uint64_t SelectiveDecoder::tileSize() const
{
    // calculate the number of tiles
    uint64_t size = pyramid()->tileSize().width() *
                                pyramid()->tileSize().height() *
                                (parser().bpp() >> 3);
    size += sizeof(Tile);
    return size;
}
RawImage SelectiveDecoder::bufferImage(const double &zoom) const
{
    QMutexLocker lock(&m_mutex);

    // Check for valid level
    const int32_t lvl = pyramid()->level(zoom);

    // Create Image
    const uint32_t w = pyramid()->cols(lvl);
    const uint32_t h = pyramid()->rows(lvl);
    RawImage buf = RawImage(Size(w, h), Gray_8);
    buf.fill(0);

    // Construct image from buffer
    TilePyramid::TileItrConst it = m_buffer.begin(lvl);
    while( it != m_buffer.end(lvl) )
    {
        buf.setValue( it->first % w,
                      it->first / w,
                      0,
                      255 );
        ++it;
    }

    // return
    return buf;
}

bool SelectiveDecoder::isRequestDecoded() const
{
    QMutexLocker lock(&m_mutex);
    return isRequestDecodedNoLock();
}

bool SelectiveDecoder::isRequestDecodedNoLock() const
{
    // private function: assumes mutex is locked
    assert(!m_mutex.tryLock());
    return !m_imageFuzzy.isNull() && m_decodeList.empty();
}

DecodeRequestPtr SelectiveDecoder::cacheRequest() const
{
    QMutexLocker lock(&m_mutex);
    return m_requestCache;
}

void SelectiveDecoder::cacheRegion(const DecodeRequestPtr& request)
{
    QMutexLocker lock(&m_mutex);
    setCacheRequest(request);
    restart();
}

void SelectiveDecoder::setCacheRequest(const DecodeRequestPtr& request)
{
    // private function: assumes mutex is locked
    assert(!m_mutex.tryLock());

    // Get the requested tiles
    if( (*request) != (*m_requestCache) )
    {
        m_requestCache = request;
        setPurgeReference(m_buffer, request);
    }
}

void SelectiveDecoder::setPurgeReference(TilePyramid& buffer, const DecodeRequestPtr& request)
{
    // private function: assumes mutex is locked
    assert(!m_mutex.tryLock());

    if( request->tiles()->size() )
    {
        uint32_t reftile = request->tiles()->size() / 2;
        buffer.setPurgeReferenceTile(request->resolution(), request->tiles()->at(reftile));
    }
}

void SelectiveDecoder::setMinimumBufferCapacity(TilePyramid& buffer, const DecodeRequestPtr& request)
{
    // private function: assumes mutex is locked
    assert(!m_mutex.tryLock());

    // Set buffer capacity
    if( request->tiles()->size() )
    {
        int32_t level = request->resolution();
        uint32_t numtiles = request->tiles()->size() * MinimumRequestMultiple;
        if( buffer.capacity(level) < numtiles )
            buffer.setCapacity(level, numtiles);
    }
}

RawImage SelectiveDecoder::updateRegion()
{
    QMutexLocker lock(&m_mutex);

    // getRegion() must be called before updateRegion()
    if( m_imageFuzzy.isNull() || m_image.isNull() )
        return m_image;

    // Paint the pixmap
    List::iterator it = m_imageUpdateList.begin();
    while( it != m_imageUpdateList.end() )
    {
        if( hasDetailedTileNoLock(m_request->resolution(), *it) )
        {
            paintTileOnImage(m_image, *m_request, m_buffer, *it, m_request->resolution());
            it = m_imageUpdateList.erase(it);
        }
        else
        { ++it;
        }
    }

    return m_image;
}

void SelectiveDecoder::getRegion(const DecodeRequestPtr& request)
{
    QMutexLocker lock(&m_mutex);

    // set the request
    if( !setRequest(request) ) {
        return;
    }

    // build fuzzy image
    if( !processRequest() ) {
        return;
    }

    // Start background thread
    restart();

    // unlock and emit finished signal
    if( !m_image.isNull() )
    {
        const int percent = isRequestDecodedNoLock() * 100;
        const int id = currentRequestIDNoLock();
        lock.unlock();
        emit decoderFinished(percent, id);
    }
}

void SelectiveDecoder::clearRequest()
{
    QMutexLocker lock(&m_mutex);
    clearRequestNoLock();
}

void SelectiveDecoder::clearRequestNoLock()
{
    // private function: assumes mutex is locked
    assert(!m_mutex.tryLock());

    // prevent overlow of ID
    if( std::numeric_limits<int>::max() == m_requestID )
        m_requestID = 0;

    // set flags to cause worker thread to stop
    ++m_requestID;
    m_requestChanged = true;
    m_imageUpdateList.clear();
    m_decodeList.clear();
}

bool SelectiveDecoder::processRequest()
{
    // private function: assumes mutex is locked
    assert(!m_mutex.tryLock());

    if( m_imageFuzzy.isNull() )
    {
        // processRequest() is called once fuzzy image is available
        return true; 
    }
    
    if( m_request->isNull() )
    {
        m_image = RawImage(m_request->size(), m_imageFuzzy);
    }
    else
    {
        getDecodeList();
        createBufferImage();
    }

    // If we cannot build an image, request cannot be processed
    // Decoder error has already been fired by the offending call above
    if( m_image.isNull() ) {
      clearRequestNoLock();
      emitErrorNoLock(RegionError);
      return false;
    }

    return true;
}

bool SelectiveDecoder::isRequestChanged() const
{
    QMutexLocker lock(&m_mutex);
    return isRequestChangedNoLock();
}

int SelectiveDecoder::currentRequestID() const
{
    QMutexLocker lock(&m_mutex);
    return m_requestID;
}

int SelectiveDecoder::currentRequestIDNoLock() const
{
    // private function: assumes mutex is locked
    assert(!m_mutex.tryLock());
    return m_requestID;
}

bool SelectiveDecoder::isRequestChangedNoLock() const
{
    // private function: assumes mutex is locked
    assert(!m_mutex.tryLock());
    return m_requestChanged;
}

bool SelectiveDecoder::setRequest(const DecodeRequestPtr& request)
{
    // private function: assumes mutex is locked
    assert(!m_mutex.tryLock());

    // clear old requests
    clearRequestNoLock();

    // Get the requested tiles
    m_request = request;
    if( m_request->cache() )
    {
        setCacheRequest( m_request );

        // set reference tile for purge as the middle tile in request
        // do this before adding the fuzzy tiles - so that correct tiles are purged
        setPurgeReference(m_buffer, m_request);
    }

    // Make sure we can buffer the entire request
    setMinimumBufferCapacity(m_buffer, m_request);

    // Set Exception list
    TilePyramid::TileExceptionSet* exceptions = 
        new TilePyramid::TileExceptionSet( m_request->tiles()->begin(), 
                                           m_request->tiles()->end() );
    TilePyramid::TileExceptionSetPtr exceptionsPtr(exceptions);
    m_buffer.setPurgeExceptions(m_request->resolution(), exceptionsPtr);

    // Check for errors
    if( 0 == m_request->tiles()->size() )
    {
        emitErrorNoLock(RegionError);
    }

    return m_request->tiles()->size();
}

bool SelectiveDecoder::createBufferImage()
{
    // private function: assumes mutex is locked
    assert(!m_mutex.tryLock());

    // Allocate image
    if( m_image.size() != m_request->size() )
    {
        m_image = RawImage(m_request->size(), m_imageFuzzy);
        if( m_image.isNull() )
        {
            emitErrorNoLock(MemoryError);
            return false;
        }
    }

    // Paint the pixmap
    for( unsigned int i = 0; i < m_request->tiles()->size(); i++)
    {
        assert( hasTileNoLock(m_request->resolution(),
                            m_request->tiles()->at(i)) );
        paintTileOnImage(   m_image, *m_request,
                            m_buffer,
                            m_request->tiles()->at(i),
                            m_request->resolution());
    }

    return true;
}

bool SelectiveDecoder::getDecodeList()
{
    // private function: assumes mutex is locked
    assert(!m_mutex.tryLock());
    assert( !m_request->isNull() );

    // clear old requests for missing tiles
    m_decodeLevel = m_request->resolution();
    m_decodeList.clear();
    m_imageUpdateList.clear();

    // Get tiles not in buffer
    for(unsigned int i = 0; i < m_request->tiles()->size(); i++)
    {
        const uint32_t tileno = m_request->tiles()->at(i);

        switch( m_buffer.tile(m_decodeLevel, tileno).state() )
        {
            //Tile data is available
        case TileDetailed:
            // do nothing
            break;

            //Tile fuzzy, set for background decoding
        case TileFuzzy:
            m_decodeList.push_back(tileno);
            break;

        //Tile empty, get fuzzy data and set for background decoding
        case TileEmpty:
            if( !bufferFuzzyTile(m_decodeLevel, tileno) )
            {
                emitErrorNoLock(MemoryError);
                return false;
            }
            m_decodeList.push_back(tileno);
            break;

        // should not be here
        default:
            emitErrorNoLock(DecodeError, 
                            "Invalid tile state in getDecodeList()!!!");
            return false;
        }
    }

    // keep a copy of tiles decoded for image reconstruction
    m_imageUpdateList = m_decodeList;

    return true;
}

bool SelectiveDecoder::bufferFuzzyTile(int32_t level, int32_t tileno)
{
    // private function: assumes mutex is locked
    assert(!m_mutex.tryLock());

    // fuzzy image must be available
    assert( !m_imageFuzzy.isNull() );

    //do we have this tile
    if( m_buffer.tile(level,tileno).state() != TileEmpty )
        return true;

    // Calculate fuzzy image scale
    Size image_size = pyramid()->size(0);
    Size fuzzy_size = m_imageFuzzy.size();
    SizeF fuzzy_scale(
              static_cast<double>(fuzzy_size.width()) / image_size.width(),
              static_cast<double>(fuzzy_size.height()) / image_size.height());

    // Get tile data
    //Calulate area needed from fuzzy image
    Rect span = tileSpan(*pyramid(), level, tileno);
    assert(!isEmpty(span));
    uint32_t xs = (int) std::floor( fuzzy_scale.width() * span.x() ),
             ys = (int) std::floor( fuzzy_scale.height() * span.y() ),
             w  = (int) std::ceil ( fuzzy_scale.width() * span.width() ),
             h  = (int) std::ceil ( fuzzy_scale.height() * span.height() );

    //Copy ROI from fuzzy image to tile-buffer
    RawImage tile = RawImage(Size(w, h), m_imageFuzzy);
    if( tile.isNull() )
        return false;

    // paint tile
    tile.copy(tile.rect(), m_imageFuzzy, Rect(Point(xs, ys), Size(w, h)));

    // mark this tile as fuzzy
    m_buffer.setTile(level, tileno, tile, TileFuzzy);

    return true;
}

RawImage SelectiveDecoder::decodeImage(uint32_t lvl)
{
    RequestByResolution request( pyramid(),
                                pyramid()->rect(lvl),
                                lvl,
                                false);
    return decodeRegion(request);
}

RawImage SelectiveDecoder::decodeTile(int32_t lvl, int32_t tileno)
{
    Rect region = tileRect(*pyramid(), lvl, tileno);
    RequestByResolution request(pyramid(),
                                region,
                                lvl,
                                false);
    return decodeRegion(request);
}

void SelectiveDecoder::restart()
{
    // private function: assumes mutex is locked
    assert(!m_mutex.tryLock());

    if( !isRunning() )
    {
        start();
    }
    else
    {
        m_restart = true;
        m_restartCondition.wakeAll();
    }
}

bool SelectiveDecoder::hasDetailedTile(int32_t lvl, int32_t tileno) const
{
    QMutexLocker lock(&m_mutex);
    return hasDetailedTileNoLock(lvl, tileno);
}

bool SelectiveDecoder::hasDetailedTileNoLock(int32_t lvl, int32_t tileno) const
{
    // private function: assumes mutex is locked
    assert(!m_mutex.tryLock());
    return m_buffer.tile(lvl, tileno).state() == TileDetailed;
}


bool SelectiveDecoder::hasFuzzyTile(int32_t lvl, int32_t tileno) const
{
    QMutexLocker lock(&m_mutex);
    return hasFuzzyTileNoLock(lvl,tileno);
}

bool SelectiveDecoder::hasFuzzyTileNoLock(int32_t lvl, int32_t tileno) const
{
    // private function: assumes mutex is locked
    assert(!m_mutex.tryLock());
    return m_buffer.tile(lvl, tileno).state() == TileFuzzy;
}

bool SelectiveDecoder::hasTile(int32_t lvl, int32_t tileno) const
{
    QMutexLocker lock(&m_mutex);
    return hasTileNoLock(lvl,tileno);
}

bool SelectiveDecoder::hasTileNoLock(int32_t lvl, int32_t tileno) const
{
    // private function: assumes mutex is locked
    assert(!m_mutex.tryLock());
    return m_buffer.tile(lvl, tileno).state() != TileEmpty;
}


void SelectiveDecoder::run()
{
    // Get fuzzy image
    setFuzzyImage();

    // lock
    m_mutex.lock();

    // Note: must clear request changed flag so that decodeTiles() can finished outstanding requests
    m_requestChanged = false;

    while( !m_abort )
    {
        // unlock from previous itteration
        m_mutex.unlock();

        // decode requestd region if necessary
        if( decodeTiles() )
            cacheTiles();

        // Wait until we have something to do
        m_mutex.lock();
        if( !m_restart )
        {
            m_restartCondition.wait(&m_mutex);
        }
        m_restart = false;
        m_requestChanged = false;
    }

    m_mutex.unlock();
}

RawImage SelectiveDecoder::getFuzzyImage()
{
  // Try the parser first
  if (parser().hasThumbnailImage()) {
    return parser().thumbnailImage();
  }

  // Otherwise extract a thumbnial from the image
  else {
    uint32_t thumbnial_level = pyramid()->level(
    static_cast<double>(DefaultFuzzyImageWidth) / pyramid()->size(0).width());
    return decodeImage(thumbnial_level);
  }
}

void SelectiveDecoder::setFuzzyImage()
{
    // requires locking - called from run()

    // Locking at this point not required
    if( !m_imageFuzzy.isNull() )
        return;

    // Get a low res image
    RawImage img = getFuzzyImage();


    QMutexLocker lock(&m_mutex);
    // Set fuzzy image is available
    if( !img.isNull() )
    {
        m_imageFuzzy = img;
        if( !processRequest() ) 
           return;
    }
    // Has an error code been set by derived calsses
    else if( lastError() != NoError )
    {
        emitLastErrorNoLock();  
        return;
    }        
    // Emit decoder error
    else {
        emitErrorNoLock(DecodeError);
        return;
    }


    // unlock and emit finished signal
    const int percent = isRequestDecodedNoLock() * 100;
    const int id = currentRequestIDNoLock();
    lock.unlock();
    emit decoderFinished(percent, id);
}

bool SelectiveDecoder::decodeToBuffer(int32_t lvl, int32_t tileno)
{
    // called from run() requires locking
    QMutexLocker lock(&m_mutex);

    // Don't decode tiles already in buffer
    if( hasDetailedTileNoLock(lvl, tileno) )
    {
        return true;
    }

    // Decode tile
    lock.unlock();
    RawImage img = decodeTile(lvl, tileno);
    lock.relock();

    // Do not make the redundant call
    if( isRequestChangedNoLock() )
      return true;

    // Copy buffer to image
    if( !img.isNull() )
    {
        m_buffer.setTile(lvl, tileno, img, TileDetailed);
        return true;
    }
    // Has an error code been set by derived calsses
    else if( lastError() != NoError )
    {
        emitLastErrorNoLock();  
        return false;
    }        
    // Emit decoder error
    else {
        emitErrorNoLock(MemoryError);
        return false;
    }
}


bool SelectiveDecoder::decodeTiles()
{
    // called from run() requires locking
    QMutexLocker lock(&m_mutex);

    // Lock decodeMutex
    const uint32_t tileCount = m_decodeList.size();
    const uint32_t lvl = m_decodeLevel;
    uint32_t tilesDecoded = 0;

    // Decode all tiles on decode list
    List::iterator it = m_decodeList.begin();
    while( it != m_decodeList.end() )
    {
        // Get tile number and increment counts
        uint32_t tileno = *it;
        ++tilesDecoded;

        // Unlock to allow changes when decoding
        if( !hasDetailedTileNoLock(lvl, tileno) ){
          lock.unlock();
          decodeToBuffer(lvl, tileno);
        }
        lock.relock();

        // emit finished signal
        //  do not hold locks when sending signals (they may be connected via Qt::DirectConnection).
        if( !isRequestChangedNoLock() ){
            int percent = static_cast<int>( static_cast<double>(tilesDecoded) / tileCount * 100.0 );
            int id = currentRequestIDNoLock();
            lock.unlock();
            emit decoderFinished(percent, id);
            lock.relock();
        }

        // must recheck for the request abort signal cause the itterator may be invalid at this point
        if(  isRequestChangedNoLock() )
            return false;
        it = m_decodeList.erase(m_decodeList.begin());
    }

    return true;
}


bool SelectiveDecoder::cacheTiles()
{
    // called from run() requires locking

    QMutexLocker lock(&m_mutex);

    // Ignore if already complete
    if( m_requestCache->isNull() || m_restart )
        return true;

    // Get cache request
    DecodeRequestPtr request = m_requestCache;
    int32_t maxTilesToCache = (int32_t)m_buffer.capacity(request->resolution()) - request->tiles()->size();
    int32_t tilesCached = 0;

    // Cache multiple levels
    for( uint32_t lvl = 0; lvl < LevelsToCache && !m_restart; lvl++)
    {
        int32_t newLevel = request->resolution() + lvl;
        if( newLevel >= pyramid()->levels() )
            break;

        // Translate request to new level
        request = boost::make_shared<RequestByResolution>(pyramid(), request->region(newLevel), newLevel, false);
        maxTilesToCache = (int32_t)m_buffer.capacity(newLevel) - request->tiles()->size();

        // Cache multiple rings around requested region
        bool errorDetected = false;
        for( uint32_t ring = 1; ring < RingsToCache && tilesCached < maxTilesToCache && !m_restart; ring++ )
        {
            lock.unlock();
            tilesCached += cacheTiles(request, ring, maxTilesToCache, errorDetected);
            if( errorDetected )
            {
                return false;
            }
            else
            {
                const int percent = static_cast<int>( static_cast<double>(tilesCached) / maxTilesToCache * 100.0 );
                emit decoderCaching(percent);
            }
            lock.relock();
        }
    }

    // If we have finished caching
    if( !m_restart )
    {
        m_requestCache = NullRequest;
    }

    return true;
}


uint32_t SelectiveDecoder::cacheTiles(const DecodeRequestPtr& request, const uint32_t ring, const uint32_t maxTilesToCache, bool& errorDetected)
{
     // eventually called from run() requires locking

    QMutexLocker lock(&m_mutex);

    uint32_t tilesDecoded = 0;

    // Lock decodeMutex
    if( !m_restart )
    {
        lock.unlock();

        // Get tilelist
        VectorPtr clist = getCacheList( *pyramid(),
                                        request->resolution(),
                                        request->tiles()->front(),
                                        request->tiles()->back(), ring );
        VectorItr it = clist->begin();

        // Cache multiple rings around requested region
        lock.relock();
        while( !m_restart && it != clist->end() && ++tilesDecoded < maxTilesToCache )
        {
            lock.unlock();
            if( !decodeToBuffer(request->resolution(), *it++) )
            {
                // oh oh - must be out of memory stop caching
                errorDetected = true;
                --tilesDecoded;
                break;
            }
            lock.relock();
        }
    }

    return tilesDecoded;
}

VectorPtr getCacheList( const ImagePyramid& pyramid,
                        const int32_t level,
                        const int32_t topLeftTile,
                        const int32_t bottomRightTile,
                        const int32_t dist)
{
    assert( topLeftTile < pyramid.count(level) );
    assert( bottomRightTile < pyramid.count(level) );

    // get coordinates of tiles to cache around
    const int32_t  cols = pyramid.cols(level);
    const int32_t  rows = pyramid.rows(level);
    const int32_t  x_min = (int32_t)(topLeftTile % cols) - (int32_t)dist;
    const int32_t  y_min = (int32_t)(topLeftTile / cols) - (int32_t)dist;
    const int32_t  x_max = (int32_t)(bottomRightTile % cols) + dist;
    const int32_t  y_max = (int32_t)(bottomRightTile / cols) + dist ;

    // fix limits
    const int32_t x1 = std::max(x_min, static_cast<int32_t>(0));
    const int32_t y1 = std::max(y_min, static_cast<int32_t>(0));
    const int32_t x2 = std::min(x_max, cols-1);
    const int32_t y2 = std::min(y_max, rows-1);
    const int32_t w  = x2-x1+1;
    const int32_t h  = y2-y1+1;

    // Initialize flags
    const bool doleft  = (x_min+(int32_t)dist) > x1;
    const bool doright = (x_max-(int32_t)dist) < x2;
    const bool dotop = (y_min+(int32_t)dist) > y1;
    const bool dobot = (y_max-(int32_t)dist) < y2;

    // Reserve vector
    VectorPtr vec(new Vector);
    vec->reserve( w*dotop + w*dobot + h*doleft + h*doright );

    // Add top tiles
    for(int32_t col = x1, base = y1*cols+x1; col <= x2 && dotop; col++)
        vec->push_back(base++);

    // Add bottom tiles
    for(int32_t col = x1, base = y2*cols+x1; col <= x2 && dobot; col++)
        vec->push_back(base++);

    // Add left tiles (skip first/last row)
    for(int32_t row = y1+1, base = (y1+1)*cols+x1; row < y2 && doleft; row++, base += cols)
        vec->push_back(base);

    // Add right tiles (skip first/last row)
    for(int32_t row = y1+1, base = (y1+1)*cols+x2; row < y2 && doright; row++, base += cols)
        vec->push_back(base);

    return vec;
}

void paintTileOnImage(RawImage& imageToPaint, const DecodeRequest& imageRegion,
                      const TilePyramid& buffer, const int32_t tileno,
                      const int32_t level) {
    assert(buffer.size(level) > (uint32_t)tileno);

    // Get tile data
    Rect span = tileSpan( *(buffer.pyramid()), level, tileno );
    const Tile& tile = buffer.tile(level, tileno);

    if( tile.image().isNull() )
      debug::warning("tile is null", __FUNCTION__, __LINE__);

    // caculate region of image painted by tile
    Rect region = imageRegion.region(0);

    // top-left point of target area
    int32_t ix0 = (int32_t) std::floor( ((double)span.x() - (double)region.x()) * imageRegion.scale().width()  );
    int32_t iy0 = (int32_t) std::floor( ((double)span.y() - (double)region.y()) * imageRegion.scale().height() );

    // bottom-right point of target area
    int32_t ix1 = (int32_t) std::floor( ((double)span.x() + span.width()  - (double)region.x()) * imageRegion.scale().width()  );
    int32_t iy1 = (int32_t) std::floor( ((double)span.y() + span.height() - (double)region.y()) * imageRegion.scale().height() );

    // target paint area
    Rect target(Point(ix0, iy0), Size(ix1-ix0+1, iy1-iy0+1));

    // paint image
    imageToPaint.copy(target, tile.image(), tile.image().rect());
}

} // namespace image
} // namespace sedeen
