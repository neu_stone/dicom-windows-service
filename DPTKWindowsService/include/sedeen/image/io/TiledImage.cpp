// Primary header
#include "image/io/TiledImage.h"

// User headers
#include "image/io/ImagePyramid.h"
#include "image/io/ImageAccessor.h"
#include "image/io/SelectiveDecoder.h"
#include "image/io/DecodeRequest.h"
#include "global/Debug.h"

#include <boost/make_shared.hpp>

namespace sedeen {
namespace image {
namespace {

void connectSignals(const SelectiveDecoder* sd, const TiledImage* ti) {
  assert(0 != sd);
  assert(0 != ti);

  QObject::connect(sd, SIGNAL(decoderFinished(int,int)),
                   ti, SLOT(decoderFinished(int,int)),
                   Qt::DirectConnection);
  QObject::connect(sd, SIGNAL(decoderError(const std::string&)),
                   ti, SLOT(decoderError(const std::string&)),
                   Qt::DirectConnection);
  QObject::connect(sd, SIGNAL(decoderCaching(int)),
                   ti, SIGNAL(decoderCaching(int)),
                   Qt::DirectConnection);
}

} // namespace

TiledImage::TiledImage(std::auto_ptr<SelectiveDecoder> dec)
    : ImageData(),
      m_dec(dec),
      m_mutex(QMutex::Recursive),
      m_queue() {
  assert(0 != m_dec.get());
  connectSignals(m_dec.get(), this);
}

TiledImage::~TiledImage() {
  QMutexLocker lock(&m_mutex);
  m_dec.get()->disconnect(this);
  this->disconnect();
}

TiledImage::TiledImage(const TiledImage& tiledImage)
    : ImageData(),
      m_dec(tiledImage.m_dec->clone()),
      m_mutex(QMutex::Recursive),
      m_queue() {
  connectSignals(m_dec.get(), this);
}

void TiledImage::processQueue() {
  if (!m_queue.empty()) {
    // Submit the current request to the decoder.
    m_dec->getRegion(m_queue.request());
  }
}

boost::shared_ptr<ImageData> TiledImage::doClone() const {
  return boost::shared_ptr<ImageData>(new TiledImage(*this));
}

Size TiledImage::doGetSize() const {
  return m_dec->pyramid()->size(0);
}

SizeF TiledImage::doGetPixelSize() const {
  return m_dec->parser().pixelSize();
}

void TiledImage::doCancelRequest(const ImageAccessor& client) {
  QMutexLocker lock(&m_mutex);
  bool alteredCurrentTask = m_queue.cancelRequest(&client);
  if (alteredCurrentTask) processQueue();
}

std::string TiledImage::doGetFilename() const {
  return m_dec->filename();
}

boost::uint64_t TiledImage::doSetBufferCapacity(boost::uint64_t bytes) {
    return m_dec->setBufferCapacity(bytes);
}

boost::uint64_t TiledImage::doGetBufferCapacity() const {
    return m_dec->bufferCapacity();
}

bool TiledImage::doIsBufferCapacityFixed() const {
    return false;
}

boost::uint64_t TiledImage::doBufferSizeRequired(int width, int height) const {
   return m_dec->bufferSizeRequired(width, height);
}

boost::uint64_t TiledImage::doGetBufferSize() const {
    return m_dec->bufferSize();
}

RawImage TiledImage::doBufferImage(double scale) const{
    return m_dec->bufferImage(scale);
}

void TiledImage::doRequestRegion(const ImageAccessor& client,
                                 const DecodeRequestPtr& request,
                                 Priority priority) {
  QMutexLocker lock(&m_mutex);
  bool alteredTask = m_queue.submitRequest(&client, request, priority);
  if (alteredTask) processQueue();

  //RawImage img  = m_dec->updateRegion();
  //if( !img.isNull() )
  //  ImageData::updateClient(client, img ,false);
}

SelectiveDecoder* TiledImage::getSelectiveDecoder() const {
  return m_dec.get();
}

const ImageParser& TiledImage::getImageParser() const {
  return m_dec->parser();
}

void TiledImage::decoderFinished(int percent, int id) {
  QMutexLocker lock(&m_mutex);
  bool complete = (100 == percent);
  // Call the update function if this request has a client
  if( m_dec->currentRequestID() == id ) {
    if (!m_queue.empty()) {
        ImageData::updateClient(*m_queue.client(), m_dec->updateRegion(), complete);
      // If the request is finished, continue to the next one.
      if (complete && !m_queue.empty()) {
        m_queue.next();
        processQueue();
      }
    }
  }
}

void TiledImage::decoderError(const std::string& message) {
  QMutexLocker locker(&m_mutex);
  // Connect up all clients' error slots
  while (!m_queue.empty()) {
    if (m_queue.client()) {
      connect(this, SIGNAL(error(std::string)),
              m_queue.client(), SLOT(imageError(const std::string&)),
              Qt::DirectConnection);
    }
    m_queue.cancelRequest(m_queue.client());
  }
   emit error(message);
}

} // namespace image
} // namespace sedeen
