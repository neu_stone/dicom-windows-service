PROCESS_MODULE(
  SOURCES
  EllipseShape.cpp
  LineShape.cpp
  PolyShape.cpp
  PolygonShape.cpp
  PolylineShape.cpp
  RectShape.cpp
  ShapeBase.cpp
  ShapeCommand.cpp
  ShapeFactory.cpp

  SUBMODULES
  test
  )
