// Primary header
#include "geometry/shape/LineShape.h"

// User includes
#include "global/Geometry.h"

using namespace sedeen;

LineShape::LineShape() : PolyShape() {
  setVertex(0, QPointF(0,0));
  setVertex(1, QPointF(0,0));
}

LineShape::LineShape(const LineShape& line) : PolyShape(line) {
  setVertex(0, line.vertex(0));
  setVertex(1, line.vertex(1));
}

LineShape::LineShape(const QPointF &p1, const QPointF &p2) : PolyShape() {
  setVertex(0, p1);
  setVertex(1, p2);
}

LineShape::~LineShape() {
}

LineShape& LineShape::operator=(const LineShape &right) {
  setVertices(right.vertices());
  return *this;
}

QPointF LineShape::center() const
{
  return geometry::lineCenter( vertex(0), vertex(1) );
}

void LineShape::intersect(const QRectF &rect )
{
  const QVector<QPointF> &vec1 = vertices();

  // Check for intersection
  if( rect.contains(vec1[0]) )
  {
    if( rect.contains(vec1[1]) )
      return;
  }
  else if( rect.contains(vec1[1]) )
  {
    ;
  }
  else
  {
    invalidate();
    return;
  }


  // Create full polygon
  QPolygonF poly1;
  for( int i = 0; i < vec1.size(); i++ )
    poly1 << vec1[i];
  poly1 << vec1[0] + vec1[1];

  // intersect with rect
  QPolygonF poly2 = poly1.intersected( rect );

  // Create new polygon
  QVector<QPointF> vec2( 2, QPointF(0,0) );
  for( int i = 0; i < 2; i++ )
    vec2[i] = poly2[i];

  setVertices(vec2);
}

unsigned int LineShape::minVertices() const
{
  return 2;
}

unsigned int LineShape::maxVertices() const
{
  return 2;
}

bool LineShape::isClosed() const
{
  return false;
}

bool LineShape::hasArea() const
{
  return false;
}

bool LineShape::isInside(const QPointF& point) const
{
  return false;
}

QString LineShape::type() const
{
  return "Line";
}

void LineShape::invalidate()
{
  setVertex(0, QPointF(0,0));
  setVertex(1, QPointF(0,0));
}

bool LineShape::isNull() const
{
  return vertex(0) == vertex(1);
}

std::unique_ptr<ShapeBase> LineShape::doClone() const {
  return std::unique_ptr<ShapeBase>(new LineShape(*this));
}
