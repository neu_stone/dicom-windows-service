#ifndef SEDEEN_SRC_SEDEEN_OVERLAY_SHAPEBASE_H
#define SEDEEN_SRC_SEDEEN_OVERLAY_SHAPEBASE_H

#include <QtCore>
#include <cassert>
#include <memory>
#include <vector>

#include "geometry/DllApi.h"
#include "global/SRTTransform.h"

namespace sedeen {

class Rect;

/**
 * The base class for shape that can be drawn.
 * Assume the x-axis increaes to the right and the y-axis
 * increases downwards.
 *
 */
class SEDEEN_GEOMETRY_API ShapeBase {
 public:
  /**
   * Constructor - default
   */
  ShapeBase();

  /**
   * Destructor
   */
  virtual ~ShapeBase();

  /// Create a copy of the shape.
  std::unique_ptr<ShapeBase> clone() const;

  /**
   * Returnes a copy of the vertices composing the shape.
   *
   * @return  the vertices of the shape
   */
  virtual QVector<QPointF> vertices() const = 0;

  /**
   * Returns the coordinates of a vertex. If index
   * is out of range returns throws exception.
   * <br> see length(),
   *
   * @param index The vertex position
   * @return vertex coordinate
   */
  virtual QPointF vertex(unsigned int index) const = 0;

  /**
   * Moves the vertex at a given position to the given position.
   * If index is out of range nothing happens.
   * <br> see numVertices(),
   *
   * @param index The vertex to move
   * @param offset the amount by which to move the vertex
   */
  virtual void moveVertex(unsigned int index, const QPointF& offset) = 0;

  /**
   * Moves the entire shape (all vertices) by a given amount.
   *
   * @param offset the amount by which to move the vertices
   */
  virtual void move(const QPointF& offset) = 0;

  /**
   * Intersects the shape with the given rect. If there is
   * no intersection, the shape becomes null. see isNull()
   *
   * @param rect the area to use for the intersection
   */
  virtual void intersect(const QRectF& rect) = 0;

  /**
   * Invalidates the shape so that isNull() becomes true.
   */
  virtual void invalidate() = 0 ;

  /**
   * Returns true if the shape is invalid.
   * A valid shape has numberVertices() >=
   * minVertices and <= maxVertices()
   *
   * @return
   *      the status of the shape
   */
  virtual bool isNull() const = 0;

  /**
   * The number of vertices in the shape.
   *
   * @return number of vertices
   */
  virtual unsigned int numVertices() const = 0;

  /**
   * The minimum number of vertices a shape can have.
   *
   * @return minimum number of vertices possible
   */
  virtual unsigned int minVertices() const = 0;

  /**
   * The maximum number of vertices a shape can have.
   *
   * @return maximum number of vertices possible
   */
  virtual unsigned int maxVertices() const = 0;

  /**
   * Checks to see if a lies on a vertex;
   *
   * @param point The new position of the vertex
   * @return  the non-negative index of the vertex if point lies near a vertex, -1 otherwise
   */
  int isVertex(const QPointF& point) const;

  /**
   * Determines if a point is on the edge of the shape
   *
   * @param point the coordinate to validate
   * @param  radius the margin of error condidered acceptable
   */
  virtual bool isOnEdge(const QPointF& point,
                        unsigned int radius = 5) const = 0;

  /**
   * Checks to see if a point falls near one of the shape's vertices.
   * Input is assumed to be in widget coordinates. The radius argument
   * defines how close (in pixels) to the actual vertex a point must lie to be
   * concidered "near" to the vertex.
   *
   * @param  p the point in widget coordinates
   * @param  radius the "near" the vertex
   * @return  the non-negative index of the vertex if point lies near a vertex, -1 otherwise
   */
  int isNearVertex(const QPointF& p, int radius = 10) const;

  /**
   * Transforms each vertex coordiante using transform.
   *
   * @param tx the transformation
   */
  virtual void transform(const QTransform& tx) = 0;

  /**
   * Transforms each vertex coordiante using transform.
   *
   * @param tx the transformation to use
   * @param dir the transform direction
   */
  virtual void transform(const SRTTransform& tx, TransformDirection dir) = 0;

  /**
   * Writes a description of the shape to the stream.
   *
   * @param writer the xml stream to write to
   * @return true if sucessful, false otherwise
   */
  bool writeXml(QXmlStreamWriter& writer) const;

  /**
   * Reads the stream until a shape description is found.
   * Returns the shape object for the description.
   *
   * reader the xml stream to read
   */
  static ShapeBase* readXml(QXmlStreamReader& reader);

  /**
   * Returns true if a given point is inside the shape.
   * May not make sense call this for all shapes (i.e. a line),
   * in which case the result is always false;
   *
   * @param point the point to test
   * @return true if point is inside, false otherwise
   */
  virtual bool isInside(const QPointF& point) const = 0;

  /**
   * Returns the bounding box of the region. The box that
   * fully contains all vertices.
   *
   * @return the bounding box
   */
  virtual QRectF boundingBox() const;

  /**
   * Returns the total area of the shape.
   * See hasArea() and isClosed() - both will true if the shape
   * has a non-zero area.
   *
   * @return the area
   */
  virtual double area() const = 0;

  /**
   * Returns true if the area of shape can be calculated.
   * The shape must be closed for it to have an area.
   *
   * @return true of the shapes area can be calculated
   */
  virtual bool hasArea() const = 0;

  /**
   * Returns whether the shape is closed or open;
   *
   * @return returns true if the shape is closed, false otherwise
   */
  virtual bool isClosed() const = 0;

  /**
   * Returns the center of the shape. If the shape hasArea() and
   * isClosed() the centroid (i.e. center of gravity) will be
   * retruned otherwise the midpoint of the contour will be
   * calculated.
   *
   * @return the center
   */
  virtual QPointF center() const = 0;

  /**
   * Returns the shape type
   *
   * @return name of the shape
   */
  virtual QString type() const = 0;

  /**
   * Calculates the perimeter of the shape.
   * If the shape is open, then the perimeter is the
   * length of the segment.
   *
   * @return the shape's perimeter
   */
  virtual double perimeter() const = 0;

  /**
   * Checks to see if the shape is parametric.
   * If parametric the shape can be defined by a
   * few points and hence it's vertices() are the
   * points that describe the shape (ie. a rectangle).
   * Non-parametric shapes such as polygons require that all
   * its vertices to be defined and hence calling vertices()
   * returns the same result as nonParametricVertices()
   * <br><br>
   * see nonParametricVertices()
   */
  virtual bool isParametric() const = 0;

  /**
   * Gets a series of points that lie on the perimeter of the shape.
   * If the points are connected they will recreate the shape.
   *
   * @return  all the points tha lie on the perimeter
   */
  virtual QVector<QPointF> nonParametricVertices() const = 0;

 protected:
  ShapeBase(const ShapeBase& shape_base);

 private:
  virtual std::unique_ptr<ShapeBase> doClone() const = 0;
};

/// Return an integer-aligned Rect that completely contains this shape.
//
/// \param shape
SEDEEN_GEOMETRY_API
Rect containingRect(const ShapeBase& shape);

} // namespace sedeen

#endif // #ifdef

