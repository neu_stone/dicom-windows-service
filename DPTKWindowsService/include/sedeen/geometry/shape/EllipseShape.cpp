#include "geometry/shape/EllipseShape.h"

#include "global/UnitDefs.h"

namespace sedeen {

EllipseShape::EllipseShape(const QPointF& tl, const QPointF& br,
                           double angle, RotationCenter center)
    : RectShape(tl, br, angle, center) {
}

EllipseShape::EllipseShape(int x, int y, int w, int h, double angle,
                           RotationCenter center)
    : RectShape(x, y, w, h, angle, center) {
}

EllipseShape::EllipseShape(const QVector<QPointF>& v) : RectShape(v) {
}

EllipseShape::EllipseShape(const EllipseShape& rect) : RectShape(rect) {
  this->setP1(rect.p1());
  this->setP2(rect.p2());
  this->setRotation(rect.rotationAngle());
  this->setRotationCenter(rect.rotationCenter());
}

EllipseShape::~EllipseShape() {
}

EllipseShape& EllipseShape::operator=(const EllipseShape& right) {
  this->setP1(right.p1());
  this->setP2(right.p2());
  this->setRotation(right.rotationAngle());
  this->setRotationCenter(right.rotationCenter());

  return *this;
}

double EllipseShape::perimeter() const {
  double a = width() / 2;
  double b = height() / 2;
  double tmp = 3*(a+b) - sqrt((3*a+b) * (a+3*b));
  return tmp * PI;
}

QVector<QPointF> EllipseShape::nonParametricVertices() const {
  /* Setup vector */
  const unsigned int numsteps = 360;
  QVector<QPointF> vec;

  /* Sanity check */
  if (isNull()) {
    return vec;
  } else {
    vec.reserve(numsteps);
  }

  /* Get some properties */
  const QPointF cen = center();
  const double w = width() / 2;
  const double h = height() / 2;
  const double step = PI * 2 / numsteps;

  /* Create a point at every 1deg. */
  for (double rad = 0; rad < PI*2; rad+=step) {
    const double x = w * cos(rad) + cen.x();
    const double y = h * sin(rad) + cen.y();
    vec.push_back(rotate(QPointF(x,y), rotationAngle(), rotationCenter()));
  }

  return vec;
}

double EllipseShape::area() const {
  return width() * height() / 4 * PI;
}

QString EllipseShape::type() const {
  return QString("Ellipse");
}

std::unique_ptr<ShapeBase> EllipseShape::doClone() const {
  return std::unique_ptr<ShapeBase>(new EllipseShape(*this));
}

} // namespace sedeen
