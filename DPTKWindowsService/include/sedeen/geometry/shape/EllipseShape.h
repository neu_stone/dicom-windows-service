#ifndef SEDEEN_SRC_GEOMETRY_SHAPE_ELLIPSESHAPE_H
#define SEDEEN_SRC_GEOMETRY_SHAPE_ELLIPSESHAPE_H

// Qt includes
#include <QtCore>

// User includes
#include "geometry/DllApi.h"
#include "geometry/shape/RectShape.h"

namespace sedeen {

class SEDEEN_GEOMETRY_API EllipseShape : public RectShape {
 public:
  /// Constructor. Defines the bounds of the shape.
  //
  /// Can define a roatation about the center of the rectangle.
  ///
  /// \param tl
  /// the top left corner
  ///
  /// \param br
  /// the bottom right corner
  ///
  /// \param angle
  /// the oritentaion of the rectangle with respect to the vertical
  ///
  /// \param center
  /// the center of rotation
  explicit EllipseShape(const QPointF &tl = QPointF(0,0),
                        const QPointF &br = QPointF(0,0),
                        double angle = 0,
                        RotationCenter center = Center);

  /// Constructor. Defines the bounds of the shape.
  //
  /// \param x
  /// the top left x-corner
  ///
  /// \param y
  /// the top left y-corner
  ///
  /// \param w
  /// the width
  ///
  /// \param h
  /// the height
  ///
  /// \param angle
  /// the oritentaion of the rectangle with respect to the vertical
  ///
  /// \param center
  /// the center of rotation
  EllipseShape(int x, int y, int w, int h, double angle = 0,
               RotationCenter center = Center);

  /// Defines a rectangle by its 4 vertices.
  //
  /// \note
  /// There must be 4 vertics in the input vector and the must be ordered from
  /// vertex(0) to vertex(3). Vertices are labled clockwise starting from the
  /// top-left corner.
  ///
  /// \param vertices
  /// the four corners of the rectangle
  EllipseShape(const QVector<QPointF>& vertices);

  EllipseShape(const EllipseShape& ellipse);

  virtual ~EllipseShape();

  EllipseShape& operator=(const EllipseShape& right);

  virtual double area() const;

  virtual QString type() const;

  virtual double perimeter() const;

  virtual QVector<QPointF> nonParametricVertices() const;

 private:
  virtual std::unique_ptr<ShapeBase> doClone() const;
};

}  // namespace sedeen

#endif
