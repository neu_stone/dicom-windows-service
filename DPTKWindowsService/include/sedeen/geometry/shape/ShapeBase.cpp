#include "geometry/shape/ShapeBase.h"

#include <cassert>

#include "global/geometry/Rect.h"

using namespace sedeen;

ShapeBase::ShapeBase() {
 ///* make sure the derived class make sense */
 //assert( !(isClosed() ^ hasArea()) );
}


ShapeBase::~ShapeBase() {
}

std::unique_ptr<ShapeBase> ShapeBase::clone() const {
  auto copy = doClone();

  assert(typeid(*this) == typeid(*copy) &&
         "ShapeBase::clone() improperly overridden.");

  return copy;
}

int ShapeBase::isNearVertex(const QPointF& point, int radius) const {
 for( unsigned int i = 0; i < numVertices(); i++)
 {
  QPointF p = vertex(i) - point;
  double  d = sqrt( p.x()*p.x() + p.y()*p.y() );
  if( d <= radius )
   return i;
 }

 return -1;
}


int ShapeBase::isVertex(const QPointF& point) const {
 for( unsigned int i = 0; i < numVertices(); i++)
 {
  if( point == vertex(i) )
   return i;
 }

 return -1;
}

QRectF ShapeBase::boundingBox() const {
 double xmin, xmax, ymin, ymax;

 if( isNull() )
  return QRectF();

 xmax = xmin = vertex(0).x();
 ymax = ymin = vertex(0).y();

 for( uint i = 1; i < numVertices(); i++ )
 {
  const QPointF p = vertex(i);

  if( p.x() < xmin )
   xmin = p.x();
  else if( p.x() > xmax )
   xmax = p.x();

  if( p.y() < ymin )
   ymin = p.y();
  else if( p.y() > ymax )
   ymax = p.y();
 }

 return QRectF(xmin, ymin, xmax-xmin, ymax-ymin);
}

bool ShapeBase::writeXml(QXmlStreamWriter& writer) const {
 return false;
}

ShapeBase* ShapeBase::readXml(QXmlStreamReader& reader) {
 return 0;
}

ShapeBase::ShapeBase(const ShapeBase& shape_base) {
}

Rect sedeen::containingRect(const ShapeBase& shape) {
  return fromQt(shape.boundingBox().toAlignedRect());
}
