#ifndef SEDEEN_SRC_GEOMETRY_SHAPE_POLYGONSHAPE_H
#define SEDEEN_SRC_GEOMETRY_SHAPE_POLYGONSHAPE_H

// Qt includes
#include <QtCore>

// System includes
#include <vector>

// User includes
#include "geometry/DllApi.h"
#include "geometry/shape/PolyShape.h"

namespace sedeen
{
/**
 * Concrete class for a polygon. Can have an arbitrary
 * number of vertices, each of which can be positioned
 * anywhere.
 * <br><br>
 * Note that if the polgon crosses itself, the geometric
 * measurments such as area(), are no longer valid. It's
 * upto the user to ensure correct use in these situations.
 */
class SEDEEN_GEOMETRY_API PolygonShape : public PolyShape
{
 public:

  /**
   * Default constructor. Creates a NULL polygon.
   */
  PolygonShape();

  /**
   * Copy constructor.
   *
   * @param poly the rect shapt to copy
   */
  PolygonShape(const PolygonShape& poly);

  /**
   * Constructor. Defines the vertices of the polygon.
   *
   * @param points the polygons vertices
   */
  PolygonShape(const QVector<QPointF> &points  );

  /**
   * Destructor
   */
  virtual ~PolygonShape();

  /**
   * Assignament operator
   */
  PolygonShape& operator= (const PolygonShape &right);


  /* Virtual public methods defined here*/
  virtual unsigned int minVertices() const;

  virtual unsigned int maxVertices() const;

  virtual bool isClosed() const;

  virtual bool hasArea() const;

  virtual QString type() const;

  virtual std::unique_ptr<ShapeBase> doClone() const;
};

}  // namespace
#endif // #ifdef
