#ifndef SEDEEN_SRC_GEOMETRY_SHAPE_RECTSHAPE_H
#define SEDEEN_SRC_GEOMETRY_SHAPE_RECTSHAPE_H

// System includes
#include <iostream>

// User includes
#include "geometry/DllApi.h"
#include "geometry/shape/ShapeBase.h"

namespace sedeen
{
class SEDEEN_GEOMETRY_API RectShape : public ShapeBase
{
 public:
  enum RotationCenter
  {
    Center,
    TopLeft,
    TopRight,
    BottomRight,
    BottomLeft
  };

  /**
   * Constructor. Defines the bounds of the shape.
   * Can define a roatation about the center of the rectangle.
   *
   * @param p1 the starting corner
   * @param p2 the opposite corner across the rectangle
   * @param angle the oritentaion of the rectangle with respect to the y-axis (clockwise rotation is positive)
   * @param center the center of rotation
   */
  RectShape(const QPointF &p1 = QPointF(0,0),
            const QPointF &p2 = QPointF(0,0),
            double angle = 0,
            RotationCenter center = Center );

  /**
   * Constructor. Defines the bounds of the shape.
   *
   * @param x the top left x-corner
   * @param y the top left y-corner
   * @param w the width
   * @param h the height
   * @param angle the oritentaion of the rectangle with respect to the y-axis (clockwise rotation is positive)
   * @param center the center of rotation
   */
  RectShape(int x,
            int y,
            int w,
            int h,
            double angle = 0,
            RotationCenter center = Center  );

  /**
   * Constructor. Defines a rectangle by its 4 vertices.
   * Note that there must be 4 vertics in the input vector
   * and the must be ordered from vertex(0) to vertex(3).
   * Vertices are labled clockwise starting from the top-left
   * corner.
   *
   * @param vertices the four corners of the rectangle
   */
  RectShape(const QVector<QPointF> &vertices );

  /**
   * Destructor
   */
  virtual ~RectShape();

  /** Using default copy constructor */

  /** Using default assignment operator */

  /**
   * Gets the height of the rectangle, before any rotation.
   *
   * @return the height
   */
  double height() const;

  /**
   * Sets the height of the rectangle.
   *  Adjusts p2 (point 2) of the rectangle so that
   *  p2-p1 is equal to the specified width - before any rotation.
   *
   * @param h the width
   */
  void setHeight(double h);

  /**
   * Gets the width of the rectangle, before any rotation.
   *
   * @return the width
   */
  double width() const;

  /**
   * Sets the width of the rectangle.
   *  Adjusts p2 (point 2) of the rectangle so that
   *  p2-p1 is equal to the specified width - before any rotation.
   *
   * @param w the width
   */
  void setWidth(double w);

  /**
   * Sets p1 (point 1) of the rectangle. Does not
   * change p2's osition.
   * The change affect the shape before the rotation
   * is applied.
   *
   * @param p1 the left edge
   */
  void setP1(const QPointF &p1);

  /**
   * Moves p1 (point 1) of the rectangle.  Does not
   * change p2 position or the center of rotation.
   * The change affects the shape before the rotation
   * is applied.
   *
   * @param del the offset in the coners position
   */
  void moveP1(const QPointF &del);

  /**
   * Gets p1 (point 1) of the rectangle, before any rotation.
   *
   * @return the top-left point
   */
  const QPointF& p1() const;

  /**
   * Sets p2 (point 2) of the rectangle.  Does not
   * change p1 corners position.
   * The change affects the shape before the rotation
   * is applied.
   *
   * @param p2 the bottom-right point
   */
  void setP2(const QPointF &p2);

  /**
   * Moves p2 (point 2) corner of the rectangle.  Does not
   * change p1's position or the center of rotation.
   * The change affect the shape befor the rotation
   * is applied.
   *
   * @param del the offset in the coners position
   */
  void moveP2(const QPointF &del);

  /**
   * Gets p2 (point 2) of the rectangle, before any rotation.
   *
   * @return r the left edge
   */
  const QPointF& p2() const;

  /**
   * Gets the bottom-right point of the rectangle, before any rotation.
   *
   * @return r the left edge
   */
  QPointF bottomRight() const;

  /**
   * Gets the top-left point of the rectangle, before any rotation.
   *
   * @return the top-left point
   */
  QPointF topLeft() const;

  /**
   * Sets the clockwise rotation angle of the rectangle with respect to the y axis.
   *
   * @param angle the rotation angle
   */
  void setRotation(double angle);

  /**
   * Gets the clockwise rotation angle of the rectangle with respect to the y-axis.
   */
  const double & rotationAngle() const;

  /**
   * Gets the center of rotation in physical cooridnates.
   *
   * @return the rect's center
   */
  virtual const QPointF& rotationCenter() const;

  /**
   * Sets the center of rotation in physical cooridnates.
   *
   * @param center the rect's center of rotation
   */
  virtual void setRotationCenter(const QPointF& center);

  /**
   * Set the center of rotation to one of five places in the rectangle.
   */
  QPointF calcRotationCenter(RotationCenter center);

  /* Virtual public methods */
  virtual void intersect(const QRectF &rect );

  virtual unsigned int minVertices() const;

  virtual unsigned int maxVertices() const;

  virtual unsigned int numVertices() const;

  virtual bool isClosed() const;

  virtual bool hasArea() const;

  virtual double area() const;

  double perimeter() const;

  virtual QPointF center() const;

  virtual QString type() const;

  virtual bool isNull() const;

  void move(const QPointF &offset );

  bool isParametric() const;

  virtual void invalidate();

  virtual QVector<QPointF> nonParametricVertices() const;
  virtual void transform(const QTransform &tx);
  virtual void transform(const SRTTransform &tx, TransformDirection dir);
  virtual void moveVertex(unsigned int index, const QPointF &offset);
  virtual QVector<QPointF> vertices() const;
  virtual QPointF vertex(unsigned int index) const;
  virtual bool isInside(const QPointF &point) const;
  virtual bool isOnEdge(const QPointF &point, unsigned int radius = 5) const;

 protected:
  /**
   * Rotates the points in the vector through a given angle. To rotate
   * about the given center (clockwise rotation is positive)
   *
   * @param vec the point to rotate
   * @param degrees the clockwise rotation angle
   * @param center the center of rotation
   * @return the rotated point
   */
  void rotate(QVector<QPointF> &vec, double degrees,
              const QPointF &center = QPointF(0,0)) const;

  /**
   * Rotates the point through a given angle. To rotate
   * about the given center - default is (0,0).
   *
   * @param point the point to rotate
   * @param degrees the clockwise rotation angle
   * @param center the center of rotation
   * @return the rotated point
   */
  QPointF rotate(const QPointF &point, double degrees,
                 const QPointF &center = QPointF(0,0)) const;

 protected:
  RectShape(const RectShape& rect_shape);

 private:
  virtual std::unique_ptr<ShapeBase> doClone() const;

  /// the counter-clockwise rotation angle
  double  _phi;

  /// the rectangles first coordinate in the unrotated space
  QPointF _p1;

  /// the rectangles second coordinate in the unrotated space
  QPointF _p2;

  /// center of rotation
  QPointF _cen;
};

/**
 * Determine rectangle's angle.
 *
 * \param v
 * A vector of vertex points for a rectangle. When unrotated, the first point is the upper-left vertex. This vector must contain at least three points.
 *
 * \return
 * The angle through which the shape has been rotated.
 */
SEDEEN_GEOMETRY_API
double deriveRectAngle(const QVector<QPointF> &v);

} // namespace sedeen

/**
 * Define the stream operator to the class
 *
 * @param os the stream to write to
 * @param obj the object to stream
 * @return the input stream
 */
SEDEEN_GEOMETRY_API
std::ostream& operator<<(std::ostream &os, const sedeen::RectShape &obj);

#endif // #ifdef
