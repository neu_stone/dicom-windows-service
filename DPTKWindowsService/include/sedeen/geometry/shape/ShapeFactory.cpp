// Primary header
#include "geometry/shape/ShapeFactory.h"

// User headers
#include "geometry/shape/EllipseShape.h"
#include "geometry/shape/LineShape.h"
#include "geometry/shape/PolygonShape.h"
#include "geometry/shape/PolylineShape.h"
#include "geometry/shape/RectShape.h"
#include "geometry/shape/ShapeBase.h"
#include "global/Debug.h"

using namespace sedeen;

ShapeBase*
ShapeFactory::
create(const QString &name, const QVector<QPointF> &vertices, double rotation)
{
    if( name.compare("Rectangle", Qt::CaseInsensitive) == 0 )
    {
     if( vertices.size() == 2 )
      return new RectShape( vertices[0], vertices[1], rotation );
     else if( vertices.size() == 4 )
      return new RectShape( vertices );
     else
      return new RectShape();
    }

    else if( name.compare("Ellipse", Qt::CaseInsensitive) == 0 )
    {
     if( vertices.size() == 2 )
      return new EllipseShape( vertices[0], vertices[1], rotation);
     else if( vertices.size() == 4 )
      return new EllipseShape( vertices );
     else
      return new EllipseShape();
    }

    else if( name.compare("Polyline", Qt::CaseInsensitive) == 0 )
    {
     return new PolylineShape( vertices );
    }

    else if( name.compare("Line", Qt::CaseInsensitive) == 0 )
    {
     if( vertices.size() >= 2 )
      return new LineShape( vertices[0], vertices[1] );
     else
      return new LineShape();
    }

    else
    {
        // Warning
  if( name.compare("polygon", Qt::CaseInsensitive ) != 0 )
        {
            debug::warning("Invalid `shape`, defaulting to polygon."
              , __FUNCTION__, __LINE__);
        }

     return new PolygonShape( vertices );
    }


    return 0;
}

QStringList
ShapeFactory::
shapes( )
{
 QStringList list;
 list << "Rectangle"
   << "Ellipse"
   << "Polygon"
            << "Polyline"
            << "Line";
 return list;
}

