// Primary header
#include "geometry/shape/ShapeCommand.h"

// User headers
#include "geometry/shape/PolyShape.h"
#include "geometry/shape/ShapeBase.h"

using namespace sedeen;

//
//  ShapeCommand
//
ShapeCommand::
ShapeCommand()
{
}

ShapeCommand::
~ShapeCommand()
{
}

//
//  MergeBlockCommand
//
MergeBlockCommand::
MergeBlockCommand(ShapeBase &target)
    : m_target(target)
{
}


bool
MergeBlockCommand::
canUndo() const
{
    return false;
}

bool
MergeBlockCommand::
canRedo() const
{
    return false;
}

bool
MergeBlockCommand::
merge(const CommandBase &command)
{
    return false;
}

void
MergeBlockCommand::
execute()
{
    // nothing to do
}

void
MergeBlockCommand::
unExecute()
{
    // nothing to do
}

unsigned int
MergeBlockCommand::
id() const
{
    return sedeen::MergeBlock;
}

const ShapeBase&
MergeBlockCommand::
target() const
{
    return m_target;
}


//
//  InsertVertexCommandCommand
//
InsertVertexCommand::
InsertVertexCommand(PolyShape &target, const int &index, const QPointF &position)
    : m_target(target),
      m_index(index),
      m_position(position)
{
}

bool
InsertVertexCommand::
canUndo() const
{
    return true;
}

bool
InsertVertexCommand::
canRedo() const
{
    return true;
}

bool
InsertVertexCommand::
merge(const CommandBase &command)
{
    return false;
}

void
InsertVertexCommand::
execute()
{
    m_index = m_target.insertVertex(m_index, m_position);
}

void
InsertVertexCommand::
unExecute()
{
    m_target.removeVertex(m_index);
}

unsigned int
InsertVertexCommand::
id() const
{
    return sedeen::InsertVertex;
}

const ShapeBase&
InsertVertexCommand::
target() const
{
    return m_target;
}

int
InsertVertexCommand::
index() const
{
    return m_index;
}

const QPointF&
InsertVertexCommand::
position() const
{
    return m_position;
}

//
//  RemoveVertexCommand
//
RemoveVertexCommand::
RemoveVertexCommand(PolyShape &target, const int &index)
    : m_target(target),
      m_index(index),
      m_position()
{
}

bool
RemoveVertexCommand::
canUndo() const
{
    return true;
}

bool
RemoveVertexCommand::
canRedo() const
{
    return true;
}

bool
RemoveVertexCommand::
merge(const CommandBase &command)
{
    return false;
}


void
RemoveVertexCommand::
execute()
{
    m_position = m_target.vertex(m_index);
    m_target.removeVertex(m_index);
}

void
RemoveVertexCommand::
unExecute()
{
    m_target.insertVertex(m_index, m_position);
}

unsigned int
RemoveVertexCommand::
id() const
{
    return sedeen::RemoveVertex;
}

const ShapeBase&
RemoveVertexCommand::
target() const
{
    return m_target;
}

int
RemoveVertexCommand::
index() const
{
    return m_index;
}

const QPointF&
RemoveVertexCommand::
position() const
{
    return m_position;
}

//
//  MoveVertexCommand
//
MoveVertexCommand::
MoveVertexCommand( ShapeBase &target, const int &index, const QPointF &offset)
    : m_target(target),
      m_index(index),
      m_offset(offset)
{
}

bool
MoveVertexCommand::
canUndo() const
{
    return true;
}

bool
MoveVertexCommand::
canRedo() const
{
    return true;
}

void
MoveVertexCommand::
execute()
{
    m_target.moveVertex(m_index, m_offset);
}

void
MoveVertexCommand::
unExecute()
{
    m_target.moveVertex(m_index, -m_offset);
}

unsigned int
MoveVertexCommand::
id() const
{
    return sedeen::MoveVertex;
}

const ShapeBase&
MoveVertexCommand::
target() const
{
    return m_target;
}

bool
MoveVertexCommand::
merge(const CommandBase &command)
{
    if( command.id() != id() )
        return false;

    const MoveVertexCommand &move = static_cast<const MoveVertexCommand&>(command);

    if( &(target()) != &(move.target()) )
        return false;

    if( move.m_index != m_index )
        return false;

    m_offset += move.m_offset;
    return true;
}

int
MoveVertexCommand::
index() const
{
    return m_index;
}

const QPointF&
MoveVertexCommand::
offset() const
{
    return m_offset;
}

//
//  MoveShapeCommand
//
MoveShapeCommand::
MoveShapeCommand( ShapeBase &target, const QPointF &offset)
    : m_target(target),
      m_offset(offset)
{
}

bool
MoveShapeCommand::
canUndo() const
{
    return true;
}

bool
MoveShapeCommand::
canRedo() const
{
    return true;
}

void
MoveShapeCommand::
execute()
{
    m_target.move(m_offset);
}

void
MoveShapeCommand::
unExecute()
{
    m_target.move(-m_offset);
}

unsigned int
MoveShapeCommand::
id() const
{
    return sedeen::MoveShape;
}

const ShapeBase&
MoveShapeCommand::
target() const
{
    return m_target;
}

bool
MoveShapeCommand::
merge(const CommandBase &command)
{
    if( command.id() != id() )
        return false;

    const MoveShapeCommand &move = static_cast<const MoveShapeCommand&>(command);

    if( &(target()) != &(move.target()) )
        return false;

    m_offset += move.m_offset;
    return true;
}

const QPointF&
MoveShapeCommand::
offset() const
{
    return m_offset;
}

