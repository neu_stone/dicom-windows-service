#include "geometry/shape/RectShape.h"

#include "global/Geometry.h"

#include <QLineF>
#include <cerrno>
#include <cmath>
#include <cfloat>

using namespace sedeen;

RectShape::RectShape(const QPointF &p1,
                     const QPointF &p2,
                     double angle,
                     RotationCenter center)
    : _phi(angle),
      _p1(p1),
      _p2(p2),
      _cen(calcRotationCenter(center))
{
}

RectShape::RectShape(int x,
                     int y,
                     int w,
                     int h,
                     double angle,
                     RotationCenter center)
    : ShapeBase(),
      _phi(angle),
      _p1(x, y),
      _p2(x+w, y+h),
      _cen(calcRotationCenter(center))
{
}

namespace
{
}

RectShape::
RectShape(const QVector<QPointF> &v)
{
  // Must have at least three points.
  assert(4 == v.size());

  /* un-roate */
  double angle = deriveRectAngle(v);
  QVector<QPointF> vec = v;
  QPointF cen = geometry::lineCenter( vec[1], vec[3] );
  rotate(vec, -angle, cen);

  /* Set the vertices */
  setP1(vec[0]);
  setP2(vec[2]);
  setRotation(angle);
  setRotationCenter( cen );
}

RectShape::
~RectShape()
{
}


std::ostream&
operator << (std::ostream &os, const sedeen::RectShape &obj)
{
  os << "Top-left: (" << obj.topLeft().x() << "," << obj.topLeft().y() << ")\t"
     << "Bottom-right: (" << obj.bottomRight().x() << "," << obj.bottomRight().y() << ")\t"
     << "Rotation: " << obj.rotationAngle() << "\n";
  return os;
}

double
RectShape::
height() const
{
  return std::abs(_p2.y() - _p1.y());
}

void
RectShape::
setHeight(double h)
{
  _p2.setY(_p1.y() + h);
}

double
RectShape::
width() const
{
  return std::abs(_p2.x() - _p1.x());
}

void
RectShape::
setWidth(double w)
{
  _p2.setX( _p1.x() + w );
}

const QPointF&
RectShape::
p1() const
{
  return _p1;
}

void
RectShape::
setP1(const QPointF &p1)
{
  _p1 = p1;
}

void
RectShape::
moveP1(const QPointF &del)
{
  _p1 += del;
}

const QPointF&
RectShape::
p2() const
{
  return _p2;
}

void
RectShape::
setP2(const QPointF &p2)
{
  _p2 = p2;
}

void
RectShape::
moveP2(const QPointF &del)
{
  _p2 += del;
}

QPointF RectShape::topLeft() const
{
  return QPointF(std::min(_p1.x(), _p2.x()),
                 std::min(_p1.y(), _p2.y()));
}

QPointF RectShape::bottomRight() const
{
  return QPointF(std::max(_p1.x(), _p2.x()),
                 std::max(_p1.y(), _p2.y()));
}

void RectShape::setRotation(double angle)
{
  _phi = angle;
}

const double & RectShape::rotationAngle() const
{
  return _phi;
}

const QPointF& RectShape::rotationCenter() const
{
  return _cen;
}

void RectShape::setRotationCenter(const QPointF& center)
{
  _cen = center;
}

QPointF RectShape::calcRotationCenter(RotationCenter center)
{
  QPointF point;

  switch( center )
  {
    case TopLeft:
      point = QPointF(std::min(_p1.x(), _p2.x()),
                      std::min(_p1.y(), _p2.y()));
      break;

    case TopRight:
      point = QPointF(std::max(_p1.x(), _p2.x()),
                      std::min(_p1.y(), _p2.y()));
      break;

    case BottomRight:
      point = QPointF(std::max(_p1.x(), _p2.x()),
                      std::max(_p1.y(), _p2.y()));
      break;

    case BottomLeft:
      point = QPointF(std::min(_p1.x(), _p2.x()),
                      std::max(_p1.y(), _p2.y()));
      break;

    case Center:
      point = geometry::lineCenter(_p1, _p2);
      break;

    default:
      // Never get here.
      assert( false ); break;
  }

  return point;
}

void RectShape::intersect(const QRectF &rect )
{
  /* Calculate the shapes vertices */
  QVector<QPointF> vec = vertices();
  bool hasIntersection = false;

  /* Fixe the vertices */
  for( int i = 0; i < vec.size(); i++)
  {
    // Calculate how to fix the vertex
    QPointF del;
    if( vec[i].x() < rect.left() )
      del.rx() += rect.left() - vec[i].x();
    else if( vec[i].x() > rect.right() )
      del.rx() -= vec[i].x() - rect.right();

    if( vec[i].y() < rect.top() )
      del.ry() += rect.top() - vec[i].y();
    else if( vec[i].y() > rect.bottom() )
      del.ry() -= vec[i].y() - rect.bottom();

    // Fix the vertex
    moveVertex(i, del);

    // Check to see if the new vertex intersects
    if( rect.contains( vertex(i) ) )
      hasIntersection = true;
  }

  if( !hasIntersection )
    invalidate();
}

unsigned int RectShape::minVertices() const
{
  return 4;
}

unsigned int RectShape::maxVertices() const
{
  return 4;
}

unsigned int RectShape::numVertices() const
{
  return 4;
}

bool RectShape::isClosed() const
{
  return true;
}

bool RectShape::hasArea() const
{
  return true;
}

double RectShape::area() const
{
  return width()* height();
}

double RectShape::perimeter() const
{
  return ( 2 * (width()+height()) );
}

QPointF RectShape::center() const
{
  QPointF point( (_p1 + _p2) / 2.0 );
  return rotate( point, rotationAngle(), rotationCenter() );
}

QString RectShape::type() const
{
  return QString("Rectangle");
}

bool RectShape::isNull() const
{
  if( topLeft() == bottomRight() )
    return true;
  else
    return false;
}

void RectShape::move(const QPointF &offset )
{
  moveP1( offset );
  moveP2( offset );
  setRotationCenter( rotationCenter() + offset );
}

bool RectShape::isParametric() const
{
  return true;
}

void RectShape::invalidate()
{
  setP1( QPointF(0,0) );
  setP2( QPointF(0,0) );
}

QVector<QPointF> RectShape::nonParametricVertices() const
{
  // Container to hold the points.
  QVector<QPointF> vec;

  if( isNull() )
    return vec;

  // Size of each step
  const double STEP_SIZE = 20 * length::um;

  for( int i = 0, a = 3, b = 0; i < 4; i++, a++, b++)
  {
    /* Get the vertex numbers for on edge*/
    a = a % 4;
    b = b % 4;

    // Load the next segment
    QLineF segment(vertex(a), vertex(b));

    // Compute the number of steps to be taken along this edge
    const unsigned int NUM_POINTS = (unsigned int) floor(segment.length() / STEP_SIZE);

    // Use the QLineF function to return incremental steps along a line
    for (unsigned int j = 0; j < NUM_POINTS; j++)
    {
      vec.push_back(segment.pointAt(double(j)/NUM_POINTS));
    }

  }

  // Return
  return vec;
}

void
RectShape::
transform(const SRTTransform &tx, const TransformDirection dir)
{
  /* Calculate the rectangles vertices */
  QVector<QPointF> vec = vertices();

  /* Calculate the rectangles transformed vertices */
  sedeen::transform( tx, vec, dir );

  /* Create the new rectangle object */
  *this = RectShape( vec );
}

void
RectShape::
transform( const QTransform &tx )
{
  /* Calculate the rectangles vertices */
  QVector<QPointF> vec = vertices();

  /* Calculate the rectangles transformed vertices */
  for( unsigned int i = 0; i < maxVertices(); i++ )
    vec[i] = tx.map(vec[i]);

  /* Create the new rectangle object */
  *this = RectShape( vec );
}

void
RectShape::
rotate(QVector<QPointF> &vec, double degrees, const QPointF &center) const {
  using namespace angle;

  double radians = sedeen::metricToMetric(degrees, deg, rad);
  double cosv = cos(radians);
  double sinv = sin(radians);
  double x, y;
  QPointF p;

  for( int i = 0; i < vec.size(); i++ )
  {
    p = vec[i] - center;
    x = p.x() * cosv - p.y() * sinv;
    y = p.x() * sinv + p.y() * cosv;

    vec[i] = QPointF(x,y) + center;
  }
}

QPointF
RectShape::
rotate(const QPointF &point, double degrees, const QPointF &center) const {
  QVector<QPointF> vec(1, point);
  rotate(vec, degrees, center);
  return vec[0];
}

QVector<QPointF>
RectShape::
vertices() const
{
  /* Create the rectangle */
  QVector<QPointF> vec(4, _p1);
  vec[1] = QPointF(_p2.x(), _p1.y());
  vec[2] = _p2;
  vec[3] = QPointF(_p1.x(), _p2.y());

  /* rotate by give angle */
  rotate( vec, _phi, rotationCenter() );

  return vec;
}

QPointF
RectShape::
vertex(unsigned int index) const {
  assert( index < maxVertices() );

  switch( index )
  {
    case 0:
      return rotate(_p1, _phi, rotationCenter()); break;
    case 1:
      return rotate(QPointF(_p2.x(), _p1.y()), _phi, rotationCenter()); break;
    case 2:
      return rotate(_p2, _phi, rotationCenter()); break;
    case 3:
      return rotate(QPointF(_p1.x(), _p2.y()), _phi, rotationCenter()); break;
    default:
      return QPointF(); break;
  }
}

void
RectShape::
moveVertex(unsigned int index, const QPointF &offset) {
  assert( index < maxVertices() );

  /* Convert the point into upright coordinate system */
  QPointF p = rotate(offset, -_phi, QPointF(0,0));

  switch( index )
  {
    case 0:
      moveP1( p );
      break;
    case 1:
      moveP1( QPointF(0.0, p.y()) );
      moveP2( QPointF(p.x(), 0.0) );
      break;
    case 2:
      moveP2( p );
      break;
    case 3:
      moveP1( QPointF(p.x(), 0.0) );
      moveP2( QPointF(0.0, p.y()) );
      break;
    default:
      break;
  }
}

bool
RectShape::
isInside( const QPointF &point ) const
{
  /* Convert the point into upright coordinate system */
  QPointF p = rotate(point, -_phi, rotationCenter());

  /* Check inside ness */
  QPointF tl = topLeft();
  QPointF br = bottomRight();

  if( p.x() >= tl.x() && p.x() <= br.x() )
    if( p.y() >= tl.y() && p.y() <= br.y() )
      return true;

  return false;
}

bool
RectShape::
isOnEdge(const QPointF &point, unsigned int radius) const {
  QVector<QPointF> verts = vertices();

  // sanity check
  if( verts.size() < 2 )
    return false;

  // check for on edge
  for( int i = 0; i < verts.size()-1; i++)
  {
    // Convert the line segment to a QLineF
    QLineF segment(QPointF(), verts[i+1]-verts[i]);

    // Create a vector from the start of the segment to the point
    QLineF pointVector(QPointF(), point-verts[i]);

    // Check if the point overlaps the lenght of the line, within tolerance
    const double length = segment.length();

    // Dot product of the point vector and the unit segment
    double span = std::fabs(pointVector.x2()*segment.x2() +
                            pointVector.y2()*segment.y2()) / length;

    // Check if the point is in-radius in the perpendicular direction
    if ( (span-radius) > 0 && span < (length + radius))
    {
      QLineF normal = segment.normalVector();

      // Dot product of the point vector and the unit normal
      double perp = (pointVector.x2()*normal.x2() +
                     pointVector.y2()*normal.y2()) / length;

      if (perp < radius)
      {
        return true;
      }
    }

  }

  return false;
}

RectShape::RectShape(const RectShape& rect_shape)
    : ShapeBase(rect_shape),
      _phi(rect_shape._phi),
      _p1(rect_shape._p1),
      _p2(rect_shape._p2),
      _cen(rect_shape._cen) {
}

std::unique_ptr<ShapeBase> RectShape::doClone() const {
  return std::unique_ptr<ShapeBase>(new RectShape(*this));
}

double
sedeen::
deriveRectAngle(const QVector<QPointF> &v)
{
  // Must have at least three points to work.
  assert(3 <= v.size());

  double angle = 0;

  // If the rectangle has zero area, return an orientation of zero.
  if (v[0] != v[1])
  {
    // Make sure line vector is always in positive direction
    // because QLineF is direction sensitive
    if( v[0].x() < v[1].x() )
      angle = QLineF(v[0], v[1]).angle();
    else
      angle = QLineF(v[1], v[0]).angle();
  }
  else if (v[1] != v[2])
  {
    // Make sure line vector is always in positive direction
    // because QLineF is direction sensitive
    if( v[1].y() < v[2].y() )
      angle = QLineF(v[1], v[2]).angle() + 90;
    else
      angle = QLineF(v[2], v[1]).angle() + 90;
  }

  return -angle;
}
