#ifndef SEDEEN_SRC_GEOMETRY_SHAPE_SHAPECOMMAND_H
#define SEDEEN_SRC_GEOMETRY_SHAPE_SHAPECOMMAND_H

// System headers
#include <QtCore>

// User headers
#include "geometry/DllApi.h"
#include "global/CommandBase.h"

namespace sedeen
{
class PolyShape;
class ShapeBase;

/*!
 * Enum defining the shape commands
 */
enum ShapeCommandTypes
{
  MergeBlock      = 0,
  MoveShape     = 1,
  InsertVertex = 2,
  RemoveVertex = 4,
  MoveVertex     = 8,
};

/*!
 * Possible commands that can be given to the Overlay class.
 * Useful for undo and redo operations.
 */
class SEDEEN_GEOMETRY_API ShapeCommand : public CommandBase
{
 public:
  /*!
    \brief Destructor
  */
  virtual ~ShapeCommand() = 0;

  /*! \copydoc CommandBase::id() */
  virtual unsigned int id() const = 0;

  /*! \copydoc CommandBase::canUndo() */
  virtual bool canUndo() const = 0;

  /*! \copydoc CommandBase::canRedo() */
  virtual bool canRedo() const = 0;

  /*! \copydoc CommandBase::execute() */
  virtual void execute() = 0;

  /*! \copydoc CommandBase::unExecute() */
  virtual void unExecute()= 0;

  /*! \copydoc CommandBase::merge(const CommandBase&) */
  virtual bool merge(const CommandBase &command) = 0;

  /*!
    \brief Returns the target shape.

    \return the shape the commands operates on
  */
  virtual const ShapeBase& target() const = 0;

 protected:
  /*!
    \brief Constructor
  */
  ShapeCommand();

 private:
  /*!
    \brief Copy Constructor

    Deactivated - commands cannot be copied in general because they have different types
  */
  ShapeCommand(const ShapeCommand &);

  /*!
    \brief Assignment operator

    Deactivated - commands cannot be copied in general because they have different types
  */
  ShapeCommand& operator=(const ShapeCommand &);
};


/*!
  \class MergeBlockCommand

  \brief Command for preventing merges.

  If past and future command should not be
  merged, insert a MergeBlock command between them.
*/
class SEDEEN_GEOMETRY_API MergeBlockCommand : public ShapeCommand
{
 public:
  /*!
    \brief Constructor
    \param target
  */
  MergeBlockCommand( ShapeBase &target);

  /*! \copydoc ShapeCommand::id() */
  unsigned int id() const;

  /*! \copydoc ShapeCommand::canUndo() */
  bool canUndo() const;

  /*! \copydoc ShapeCommand::canRedo() */
  bool canRedo() const;

  /*! \copydoc ShapeCommand::execute() */
  void execute();

  /*! \copydoc ShapeCommand::unExecute() */
  void unExecute();

  /*! \copydoc ShapeCommand::target() */
  const ShapeBase& target() const;

  /*! \copydoc ShapeCommand::merge(const CommandBase&) */
  bool merge(const CommandBase &command);

 private:
  ShapeBase &m_target; ///< the vertex position
};

/*!
  \class InsertVertexCommand
  \brief Command for removing a vertex in a Shape
*/
class SEDEEN_GEOMETRY_API InsertVertexCommand : public ShapeCommand
{
 public:
  /*!
    \brief Constructor
    \param target
    \param index
    \param position
  */
  InsertVertexCommand( PolyShape &target, const int &index, const QPointF &position);

  /*! \copydoc ShapeCommand::id() */
  unsigned int id() const;

  /*! \copydoc ShapeCommand::canUndo() */
  bool canUndo() const;

  /*! \copydoc ShapeCommand::canRedo() */
  bool canRedo() const;

  /*! \copydoc ShapeCommand::execute() */
  void execute();

  /*! \copydoc ShapeCommand::unExecute() */
  void unExecute();

  /*! \copydoc ShapeCommand::target() */
  const ShapeBase& target() const;

  /*! \copydoc ShapeCommand::merge(const CommandBase&) */
  bool merge(const CommandBase &command);

  /*!
    \brief Returns the vertex index
    \return the index
  */
  int index() const;

  /*!
    \brief Returns the position of the vertex
    \return the offset
  */
  const QPointF& position() const;

 private:
  PolyShape &m_target; ///< the vertex position
  unsigned int m_index; ///< the affected vertex
  const QPointF m_position; ///< the vertex position
};


/*!
  \class RemoveVertexCommand
  \brief Command for removing a vertex in a Shape
*/
class SEDEEN_GEOMETRY_API RemoveVertexCommand : public ShapeCommand
{
 public:
  /*!
    \brief Constructor
    \param target
    \param index
  */
  RemoveVertexCommand( PolyShape &target, const int &index);

  /*! \copydoc ShapeCommand::id() */
  unsigned int id() const;

  /*! \copydoc ShapeCommand::canUndo() */
  bool canUndo() const;

  /*! \copydoc ShapeCommand::canRedo() */
  bool canRedo() const;

  /*! \copydoc ShapeCommand::execute() */
  void execute();

  /*! \copydoc ShapeCommand::unExecute() */
  void unExecute();

  /*! \copydoc ShapeCommand::target() */
  const ShapeBase& target() const;

  /*! \copydoc ShapeCommand::merge(const CommandBase&) */
  bool merge(const CommandBase &command);

  /*!
    \brief Returns the vertex index
    \return the index
  */
  int index() const;

  /*!
    \brief Returns the position of the vertex

    Note: Return value only valid after the command is executed.

    \return the position
  */
  const QPointF& position() const;

 private:
  PolyShape &m_target; ///< the vertex position
  const uint  m_index; ///< the affected vertex
  QPointF   m_position; ///< the vertex position
};


/*!
  \class MoveVertexCommand
  \brief Command for moving a vertex in a Shape
*/
class SEDEEN_GEOMETRY_API MoveVertexCommand : public ShapeCommand
{
 public:
  /*!
    \brief Constructor
    \param target
    \param index
    \param offset
  */
  MoveVertexCommand( ShapeBase &target, const int &index, const QPointF &offset);

  /*! \copydoc ShapeCommand::id() */
  unsigned int id() const;

  /*! \copydoc ShapeCommand::canUndo() */
  bool canUndo() const;

  /*! \copydoc ShapeCommand::canRedo() */
  bool canRedo() const;

  /*! \copydoc ShapeCommand::execute() */
  void execute();

  /*! \copydoc ShapeCommand::unExecute() */
  void unExecute();

  /*! \copydoc ShapeCommand::target() */
  const ShapeBase& target() const;

  /*! \copydoc ShapeCommand::merge(const CommandBase&) */
  bool merge(const CommandBase &command);

  /*!
    \brief Returns the vertex index
    \return the index
  */
  int index() const;

  /*!
    \brief Returns the offset of the vertex
    \return the offset
  */
  const QPointF& offset() const;

 private:
  ShapeBase  &m_target; ///< the vertex position
  const uint  m_index; ///< the affected vertex
  QPointF   m_offset; ///< the vertex position
};


/*!
  \class MoveShapeCommand
  \brief Command for moving an entire shape by a given offset
*/
class SEDEEN_GEOMETRY_API MoveShapeCommand : public ShapeCommand
{
 public:
  /*!
    \brief Constructor
    \param target
    \param offset
  */
  MoveShapeCommand( ShapeBase &target, const QPointF &offset);

  /*! \copydoc ShapeCommand::id() */
  unsigned int id() const;

  /*! \copydoc ShapeCommand::canUndo() */
  bool canUndo() const;

  /*! \copydoc ShapeCommand::canRedo() */
  bool canRedo() const;

  /*! \copydoc ShapeCommand::execute() */
  void execute();

  /*! \copydoc ShapeCommand::unExecute() */
  void unExecute();

  /*! \copydoc ShapeCommand::target() */
  const ShapeBase& target() const;

  /*! \copydoc ShapeCommand::merge(const CommandBase&) */
  bool merge(const CommandBase &command);

  /*!
    \brief Returns the offset of the command
    \return the offset
  */
  const QPointF& offset() const;
 private:
  ShapeBase &m_target; ///< the vertex position
  QPointF  m_offset; ///< the vertex position
};

}  // namespace

#endif

