#ifndef SEDEEN_SRC_GEOMETRY_SHAPE_POLYLINESHAPE_H
#define SEDEEN_SRC_GEOMETRY_SHAPE_POLYLINESHAPE_H

// System includes
#include <QPointF>
#include <QString>
#include <vector>

// User includes
#include "geometry/DllApi.h"
#include "geometry/shape/PolyShape.h"

namespace sedeen
{
class SEDEEN_GEOMETRY_API PolylineShape : public PolyShape
{
 public:
  /**
   * Default constructor. Creates a NULL polyline
   */
  PolylineShape ();

  /**
   * Copy constructor.
   *
   * @param poly the shape to copy
   */
  PolylineShape(const PolylineShape& poly);

  /**
   * Constructor. Defines the vertices of the polygon.
   *
   * @param points the polygons vertices
   */
  PolylineShape (const QVector<QPointF> &points);

  /**
   * Destructor
   */
  virtual ~PolylineShape();

  /**
   * Assignament operator
   */
  PolylineShape& operator= (const PolylineShape &right);

  /* Virtual public methods that are defined here*/
  virtual unsigned int minVertices() const;

  virtual unsigned int maxVertices() const;

  virtual bool isClosed() const;

  virtual bool hasArea() const;

  virtual double area() const;

  virtual QPointF center() const;

  virtual bool isInside( const QPointF &point ) const;

  virtual QString type() const;

  virtual std::unique_ptr<ShapeBase> doClone() const;
};

}  // namespace
#endif // #ifdef
