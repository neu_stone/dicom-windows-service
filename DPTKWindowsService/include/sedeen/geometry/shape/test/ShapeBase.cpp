#include "gtest/gtest.h"

#include "geometry/shape/RectShape.h"
#include "global/geometry/Point.h"
#include "global/geometry/Rect.h"
#include "global/geometry/Size.h"

namespace sedeen {

TEST(ShapeBase, BoundingBox) {
  const QPointF P1(0.9, 5.9);
  const QPointF P2(3.1, 9.5);
  const double ANGLE = 0;

  RectShape r(P1, P2, ANGLE);

  Rect expected_boundary(Point(0, 5), Size(4, 5));

  EXPECT_EQ(expected_boundary, containingRect(r));
}

} // namespace sedeen
