#ifndef SEDEEN_SRC_GEOMETRY_SHAPE_LINESHAPE_H
#define SEDEEN_SRC_GEOMETRY_SHAPE_LINESHAPE_H

// Qt includes
#include <QtCore>

// User includes
#include "geometry/DllApi.h"
#include "geometry/shape/PolyShape.h"

namespace sedeen {

class SEDEEN_GEOMETRY_API LineShape : public PolyShape {
 public:
  LineShape();

  LineShape(const LineShape& line);

  LineShape(const QPointF& p1, const QPointF& p2);

  virtual ~LineShape();

  LineShape& operator=(const LineShape &right);

  void intersect(const QRectF &rect );

  unsigned int minVertices() const;

  unsigned int maxVertices() const;

  bool isClosed() const;

  bool hasArea() const;

  void invalidate();

  bool isNull() const;

  bool isInside(const QPointF& point) const;

  QString type() const;

  QPointF center() const;

 private:
  virtual std::unique_ptr<ShapeBase> doClone() const;
};

} // namespace sedeen

#endif
