#include "geometry/shape/PolygonShape.h"

using namespace sedeen;

PolygonShape::PolygonShape() : PolyShape()
{
}

PolygonShape::PolygonShape(const PolygonShape& poly) : PolyShape(poly) {
}

PolygonShape::PolygonShape(const QVector<QPointF> &points  ) : PolyShape(points)
{
}

PolygonShape::~PolygonShape()
{
}

PolygonShape& PolygonShape::operator= (const PolygonShape &right)
{
  setVertices( right.vertices() );
  return *this;
}

unsigned int PolygonShape::minVertices() const
{
  return 3;
}

unsigned int PolygonShape::maxVertices() const
{
  return -1;
}

bool PolygonShape::isClosed() const
{
  return true;
}

bool PolygonShape::hasArea() const
{
  return true;
}

QString PolygonShape::type() const
{
  return "Polygon";
}

std::unique_ptr<ShapeBase> PolygonShape::doClone() const {
  return std::unique_ptr<ShapeBase>(new PolygonShape(*this));
}
