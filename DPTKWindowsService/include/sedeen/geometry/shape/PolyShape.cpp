// Primary header
#include "geometry/shape/PolyShape.h"

// User includes
#include "global/Geometry.h"

namespace sedeen {
namespace {

/// Check to see if a point is one of the corners
//
/// \param rect
/// the rectangle to compare against
///
/// \param point
/// the cooridnate of interest
///
/// \return
/// true if \a point is a corner, false if point in on boundary or outside
bool isCorner(const QRectF& rect, const QPointF& point) {
  return point == rect.topLeft() || point == rect.topRight()
      || point == rect.bottomRight() || point == rect.bottomLeft();
}

bool isPointInside(const QRectF& rect, const QPointF& point);
QVector<QPointF> findIntersections(const QLineF& line, const QRectF& rect);
QPointF projectOntoRectangle(const QRectF& rect, const QPointF& point);
QPointF getElbowPoint(const QRectF& rect, const QPointF& p1, const QPointF& p2);

} // namespace

PolyShape::PolyShape() {
}

PolyShape::PolyShape(const PolyShape& poly) : ShapeBase(poly) {
  setVertices(poly.vertices());
}

PolyShape::PolyShape(const QVector<QPointF>& points) {
  setVertices(points);
}

PolyShape& PolyShape::operator=(const PolyShape& right) {
  setVertices(right.vertices());
  return *this;
}

PolyShape::~PolyShape() {
}

void PolyShape::setVertices(const QVector<QPointF>& vertices) {
  m_vertices = vertices;
}

unsigned int PolyShape::setVertex(unsigned int index, const QPointF& point) {
  assert(index < maxVertices());

  if (index < numVertices()) {
    m_vertices[index] = point;
    return index;
  } else {
    m_vertices.push_back(point);
    return numVertices();
  }
}

unsigned int PolyShape::insertVertex(int index, const QPointF& vertex) {
  if (index < 0 || index >= (int)numVertices()) {
    m_vertices.push_back(vertex);
    return (numVertices() - 1);
  } else {
    m_vertices.insert(m_vertices.begin()+index, vertex);
    return index;
  }
}

void PolyShape::removeVertex(unsigned int index) {
  assert(index < numVertices());

  m_vertices.erase(m_vertices.begin()+index);
}

QVector<QPointF> PolyShape::vertices() const {
  return m_vertices;
}

QPointF PolyShape::vertex(unsigned int index) const {
  assert(index < numVertices());

  return m_vertices[index];
}

void PolyShape::moveVertex(unsigned int index, const QPointF& offset) {
  assert(index < numVertices());

  m_vertices[index] += offset;
}

void PolyShape::move(const QPointF& offset) {
  for (unsigned int i = 0; i < numVertices(); ++i)
    moveVertex(i, offset);
}

void PolyShape::intersect(const QRectF& rect) {
  // Create full polygon
  QPolygonF poly1(vertices());

  // intersect with rect
  QPolygonF poly2 = poly1.intersected(rect);

  // Create new polygon
  setVertices(QVector<QPointF>());
  for (int i = 0; i < poly2.size(); ++i)
    insertVertex(i, poly2[i]);
}

void PolyShape::invalidate() {
  setVertices(QVector<QPointF>(0));
}

bool PolyShape::isNull() const {
  return numVertices() < minVertices();
}

unsigned int PolyShape::numVertices() const {
  return vertices().size();
}

bool PolyShape::isOnEdge(const QPointF& point, unsigned int radius) const {
  QVector<QPointF> vec = vertices();

  // sanity check
  if (numVertices() < 2)
    return false;

  // check for on edge
  for (unsigned int i = 0; i < numVertices()-1; ++i) {

    // Convert the line segment into a QLineF
    QLineF segment(QPointF(), vec[i+1]-vec[i]);

    // Create a vector from the start of the segment to the point
    QLineF pointVector(QPointF(), point-vec[i]);

    // Check if the point overlaps the length of the line, within tolerance
    const double length = segment.length();
    double span = absDot(pointVector.p2(), segment.p2() / length);
    if ((span-radius) > 0 && span < (length + radius)) {
      double perp = absDot(pointVector.p2(),
                           segment.normalVector().p2() / length);

      if (perp < radius) {
        return true;
      }
    }
  }
  return false;
}

void PolyShape::transform(const QTransform& tx) {
  for (int i = 0; i < m_vertices.size(); i++)
    m_vertices[i] = tx.map(m_vertices.at(i));
}

void PolyShape::transform(const SRTTransform& tx,
                          const TransformDirection dir) {
  sedeen::transform(tx, m_vertices, dir);
}

bool PolyShape::isInside(const QPointF& p) const {
  if (!isClosed() || numVertices() < minVertices())
    return false;

  /* do inside test
     via ray casting (aka. odd-even test) */
  bool  oddNodes = false;
  for (unsigned int i = 0, j = numVertices() - 1; i < numVertices(); ++i) {
    if ((vertex(i).y() < p.y() && vertex(j).y() >= p.y()) ||
        (vertex(j).y() < p.y() && vertex(i).y() >= p.y())) {
      if ((vertex(i).x() + (p.y()-vertex(i).y()) /
           (vertex(j).y()-vertex(i).y()) *
           (vertex(j).x()-vertex(i).x())) < p.x()) {
        oddNodes = !oddNodes;
      }
    }
    j = i;
  }
  return oddNodes;
}

double PolyShape::area() const {
  double  area = 0;
  unsigned int i, j;

  /* make sure we have a valid shape */
  if (isNull() || !hasArea())
    return 0;

  /* Calculate the area */
  for (i = 0, j = numVertices()-1; i < numVertices(); i++) {
    area += (vertex(j).x() * vertex(i).y()) - (vertex(i).x() * vertex(j).y());
    j = i;
  }
  return std::abs(area / 2.0);
}

QPointF PolyShape::center() const {
  unsigned int i, j;
  double  cx = 0,
      cy = 0;

  /* make sure we have a valid shape */
  if (isNull())
    return QPointF(0,0);

  /* Calculate the center of gravity for a closed shape */
  if (hasArea()) {
    double area = 0,
        tmp;

    for (i = 0, j = numVertices()-1; i < numVertices(); i++) {
      tmp = (vertex(j).x() * vertex(i).y()) - (vertex(i).x() * vertex(j).y());
      cx += (vertex(j).x() + vertex(i).x()) * tmp;
      cy += (vertex(j).y() + vertex(i).y()) * tmp;
      area += tmp;
      j = i;
    }
    area = area / 2;
    cx  = cx / (6*area);
    cy  = cy / (6*area);

  } else if (numVertices() > 1) {
    /* Calculate the center as the midpoint of the contour */

    double distanceToMidpoint = perimeter() / 2;

    /* center of polyline */
    for (i = 0; i < numVertices()-1; i++) {

      // Make a line out of the current segment
      QLineF segment(vertex(i), vertex(i+1));
      double segmentLength = segment.length();

      // We've reached the segment containing the midpoint.
      if (segmentLength > distanceToMidpoint) {
        // Set the segment's length to be the remaining distance to the midpoint
        segment.setLength(distanceToMidpoint);
        // Take the end-point of the shortened segment as the midpoint of the contour
        cx = segment.x2();
        cy = segment.y2();
        break;
      } else {
        // Subtract this segment's length from the total
        distanceToMidpoint -= segmentLength;
      }
    }

  } else {
    /* The center is the only point in the shape */

    return vertex(0);
  }
  return QPointF(cx, cy);
}

double PolyShape::perimeter() const {
  double p = 0;

  /* make sure we have a valid shape */
  if (isNull())
    return 0;

  /* Calculat premiter */
  for (unsigned int i = 1; i < numVertices(); i++)
    p += geometry::l2Norm(vertex(i), vertex(i-1));

  if (isClosed())
    p += geometry::l2Norm(vertex(numVertices()-1), vertex(0));

  return p;
}

bool PolyShape::isParametric() const {
  return false;
}

QVector<QPointF> PolyShape::nonParametricVertices() const {
  return vertices();
}

double absDot(const QPointF& a, const QPointF& b) {
  return std::fabs(a.x()*b.x() + a.y()*b.y());
}

void intersected(PolyShape& poly, const QRectF& rect, PolyShape& fill) {
  QVector<QPointF> vec = poly.vertices();
  unsigned int k = 0;
  double dist = 0;
  bool lastWasInside = false;

  // Check conditions of first point
  if (vec.size()) {
    lastWasInside = rect.contains(vec[0]);
    if (lastWasInside) {
      fill.setVertex(k++, vec[0]);
    } else {
      QPointF p = projectOntoRectangle(rect, vec[0]);
      fill.setVertex( k++, p );
    }

    // Add the first point so the everything works properle
    // remove it later
    poly.insertVertex(poly.numVertices(), vec[0]);
  }

  // Loop through points and reduce their resolution
  for (int i = 1; i < vec.size(); i++) {
    bool currentIsInside = rect.contains(vec[i]);

    if (lastWasInside) {
      // we are inside the view area
      if (currentIsInside) {
        // Caculate distance between points
        QPointF del = vec[i] - vec[i-1];
        dist += std::abs(del.x()) + std::abs(del.y());

        // keep only points with enough seperation
        if (dist >= 10) {
          fill.setVertex(k++, vec[i]);
          dist = 0;
        }
      } else {
        // we are exiting the view area
        // This point will track our location on the outside until get back
        // inside
        QPointF trackingPoint = projectOntoRectangle(rect, vec[i]);

        QLineF line(fill.vertex(k-1), vec[i]);
        QVector<QPointF> points = findIntersections(line, rect);
        if (points.size()) {
          fill.setVertex(k++, points[0]);
          fill.setVertex(k++, trackingPoint);
          dist = 0;
        }
      }
    } else if (currentIsInside) {
      // we are entering the view area

      // Get the entry point on the boundary of view area
      QLineF line(vec[i-1], vec[i]);
      QVector<QPointF> points = findIntersections(line, rect);
      if (points.size()) {
        fill.setVertex( k++, points[0] );
      }

      // add the new point
      fill.setVertex( k++, vec[i] );
      dist = 0;
    } else {
      // we are outside the view area

      QPointF p = projectOntoRectangle(rect, vec[i]);

      if (fill.vertex(k-1) == p) {
        ;
      } else if ((fill.vertex(k-1).x() == p.x()) ^
                 (fill.vertex(k-1).y() == p.y())) {
        if (isCorner(rect,p)) {
          fill.setVertex(k++, p);
        }
      } else {
        // Only add elbow if this line segment does not reenter the view area
        //QLineF line(fill.vertex(k-1), vec[i]);
        QLineF line(vec[i-1], vec[i]);
        QVector<QPointF> points = findIntersections(line, rect);
        if (!points.size()) {
          QPointF elbow = getElbowPoint(rect, fill.vertex(k-1), p);
          fill.setVertex(k++, elbow);
          fill.setVertex(k++, p);
        }

        // add the intersection with the view area
        for (int i = 0; i < points.size(); i++)
          fill.setVertex(k++, points[i]);
      }
    }
    lastWasInside = currentIsInside;
  }

  // If: k ==1 the polygon does not with rect
  if (k <= 1) {
    fill.invalidate();
  }

  // Remove the extra points added
  if (vec.size()) {
    poly.removeVertex(poly.numVertices()-1);
  }

  // Remove the tracking point added when we exited the view area
  if (lastWasInside && fill.numVertices() > fill.minVertices())  {
    fill.removeVertex(fill.numVertices()-1);
  }
}

namespace {

/**
 *  searches for intersection between the \a line and the \a rect.
 *
 *  Ordrers the intersections found according to their distance to
 *  the beggining of the line. There can be a maximum of 2 intersections
 *  and a minimum of 0. If \a line is parallel to any side, no intersection
 *  will be found.
 *
 * @param line the line to inspect
 *  @param rect the rectangle to compare agains
 */
QVector<QPointF> findIntersections(const QLineF& line, const QRectF& rect) {
  QVector<QPointF> points;
  QPointF intersection;

  QLineF tline(rect.topLeft(), rect.bottomLeft());
  if (line.intersect(tline, &intersection) == QLineF::BoundedIntersection) {
    // make sure there are no rounding errors
    intersection.rx() = rect.left();
    points.push_back(intersection);
  }

  tline.setP1(rect.bottomRight());
  if( line.intersect(tline, &intersection) == QLineF::BoundedIntersection )
  {
    // make sure there are no rounding errors
    intersection.ry() = rect.bottom();

    // make sure we don't get the same point in twice
    // this can happpen at the ends of the line
    if( points.size() && points[0]==intersection )
      ;
    else
      points.push_back(intersection);
  }

  tline.setP2(rect.topRight());
  if( line.intersect(tline, &intersection) == QLineF::BoundedIntersection )
  {
    // make sure there are no rounding errors
    intersection.rx() = rect.right();

    // make sure we don't get the same point in twice
    // this can happpen at the ends of the line
    if( points.size() && points[0]==intersection )
      ;
    else
      points.push_back(intersection);
  }

  tline.setP1(rect.topLeft());
  if( line.intersect(tline, &intersection) == QLineF::BoundedIntersection )
  {
    // make sure there are no rounding errors
    intersection.ry() = rect.top();

    // make sure we don't get the same point in twice
    // this can happpen at the ends of the line segments
    if( points.size() && points[0]==intersection )
      ;
    else
      points.push_back(intersection);
  }

  //order the points
  if( points.size() == 2 )
  {
    double dx = points[0].x()-line.p1().x();
    double dy = points[0].y()-line.p1().y();
    double dist1 = dx*dx + dy*dy;

    dx = points[1].x()-line.p1().x();
    dy = points[1].y()-line.p1().y();
    double dist2 = dx*dx + dy*dy;

    if( dist2 < dist1 )
    {
      QPointF temp = points[0];
      points[0] = points[1];
      points[1] = temp;
    }
  }

  return points;
}

/**
 * Check to see if a point is inside a rect.
 *
 *  A point on the boundary is concidrered outside the rect.
 *  This is why this is difference from QRect::contains()
 *
 *  @param rect the rectangle to compare agains
 *  @param point the cooridnate of interest
 *  @return true if \a point is inside, false if point in on boundary or outside
 */
bool isPointInside(const QRectF &rect, const QPointF &point)
{
  if( point.x() > rect.left() &&
      point.y() > rect.top() &&
      point.x() < rect.right() &&
      point.y() < rect.bottom() )
    return true;
  else
    return false;
}

QPointF projectOntoRectangle(const QRectF &rect, const QPointF &point)
{
  //qreal   x1 = std::max(point.x(), rect.left());
  //        x1 = std::min(x1, rect.right());
  //qreal   y1 = std::max(point.y(), rect.top());
  //        y1 = std::min(y1, rect.bottom());
  //return QPointF(x1,y1);

  QPointF p(point);

  if( point.x() < rect.left() )
    p.rx() = rect.left();
  else if( point.x() > rect.right() )
    p.rx() = rect.right();

  if( point.y() < rect.top() )
    p.ry() = rect.top();
  else if( point.y() > rect.bottom() )
    p.ry() = rect.bottom();


  return p;
}

QPointF getElbowPoint(const QRectF &rect, const QPointF &p1, const QPointF &p2 )
{
  // Calculate the elbow position so that we always wrap around
  // to the entry point from outside widget's paint area
  //      notes:
  //          - the entry point and exit point are always on the boundary
  //          - elbow is only required if the unitVector has two non-zero components
  //          - elbow cannot be inside the widget's paint area

  Q_ASSERT(   p1.x() == rect.left() ||
              p1.x() == rect.right() ||
              p1.y() == rect.top() ||
              p1.y() == rect.bottom() );

  QPointF elbow(p2);
  QPointF cen(rect.center());
  QLineF vector(p1, p2);
  if( !(vector.dx() == 0 || vector.dy() == 0)  )
  {
    QLineF unitvec = vector.unitVector();

    elbow.rx() = unitvec.dx() * vector.length() + p1.x();
    elbow.rx() = (elbow.x() < cen.x() ? rect.left() : rect.right()); //account for rounding errors
    elbow.ry() = p1.y();

    if( isPointInside(rect,elbow) ||
        !isCorner(rect, elbow)      )
    {
      elbow.rx() = p1.x();
      elbow.ry() = unitvec.dy() * vector.length() + p1.y();
      elbow.ry() = (elbow.y() < cen.y() ? rect.top() : rect.bottom()); //account for rounding errors
    }
  }

  return elbow;
}

} // namespace
} // namespace sedeen
