#ifndef SEDEEN_SRC_GEOMETRY_SHAPE_SHAPEFACTORY_H
#define SEDEEN_SRC_GEOMETRY_SHAPE_SHAPEFACTORY_H

// System headers
#include <QtCore>

#include "geometry/DllApi.h"

namespace sedeen {

class ShapeBase;
/*!
  Shape factory class. Generates an shape object from the shape's vertices and name.
*/
class SEDEEN_GEOMETRY_API ShapeFactory
{
 public:
  /*!
    Generates a shape with the given vertices. If the number of elements
    in \a vertices are too few to correctly define the shape, or if \a vertices
    is empty, the shape returned is NULL. If there
    re too many elements, then the extra elements are ignored.
    If the shape's name give is invalid, a default polygon object is returned.

    \a rotation is used for shapes such as Rect and Ellipse iff
    the number of size of \a vertices is exactly 2, otherwise it's
    ignored.

    \a name must be valid - see shapes().

    \param name the type of shape
    \param vertices the shape's vertices
    \param rotation the rotation angle if applicable
    \return the shape object created or zero on error
  */
  static ShapeBase* create(const QString &name, const QVector<QPointF> &vertices = QVector<QPointF>(), double rotation = 0);

  /*!
    \brief Returnes the shapes that are valid.

    \return a list with all of the valid shapes
  */
  static QStringList shapes();
};

} //namespace

#endif //ifndef

