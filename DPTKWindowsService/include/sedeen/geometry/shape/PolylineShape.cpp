#include "geometry/shape/PolylineShape.h"

using namespace sedeen;

PolylineShape::PolylineShape() : PolyShape()
{
}

PolylineShape::PolylineShape(const PolylineShape& poly) : PolyShape(poly) {
}

PolylineShape::PolylineShape(const QVector<QPointF> &points  ) : PolyShape(points)
{
}

PolylineShape::~PolylineShape()
{
}

PolylineShape& PolylineShape::operator= (const PolylineShape &right)
{
  setVertices( right.vertices() );
  return *this;
}

unsigned int PolylineShape::minVertices() const
{
  return 2;
}

unsigned int PolylineShape::maxVertices() const
{
  return -1;
}

bool PolylineShape::isClosed() const
{
  return false;
}

bool PolylineShape::hasArea() const
{
  return false;
}

double PolylineShape::area() const
{
  return 0;
}

QPointF PolylineShape::center() const
{
  return QPointF(0,0);
}

bool PolylineShape::isInside(const QPointF &point) const
{
  return false;
}

QString PolylineShape::type() const
{
  return "Polyline";
}

std::unique_ptr<ShapeBase> PolylineShape::doClone() const {
  return std::unique_ptr<ShapeBase>(new PolylineShape(*this));
}
