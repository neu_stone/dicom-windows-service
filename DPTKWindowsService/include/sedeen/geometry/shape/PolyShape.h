#ifndef SEDEEN_SRC_GEOMETRY_SHAPE_POLYSHAPE_H
#define SEDEEN_SRC_GEOMETRY_SHAPE_POLYSHAPE_H

// Qt includes
#include <QtGui>

// User includes
#include "geometry/DllApi.h"
#include "geometry/shape/ShapeBase.h"

namespace sedeen {

/// Virutal base class for shapes that are polygons.
//
/// Polygons shapes are defined as those that are have
/// vertices positions any arbitray manner.
class SEDEEN_GEOMETRY_API PolyShape : public ShapeBase {
 public:
  PolyShape();

  PolyShape(const PolyShape& poly);

  /// Defines the vertices of the polygon.
  //
  /// \param points
  /// the polygons vertices
  PolyShape(const QVector<QPointF>& points);

  PolyShape& operator=(const PolyShape& right);

  virtual ~PolyShape();

  /// Sets the vertices of the shape.
  //
  /// If the length of input is MORE than maxVertices() the, extra vertices will
  /// be dropped.
  ///
  /// \param vertices
  /// the vertices of the shape
  void setVertices(const QVector<QPointF>& vertices);

  /// Replaces the given vertex with the given value.
  //
  /// If the index exists it will be overwritten, if not, the vertex is
  /// appended. The position must be less than maxVertices().
  ///
  /// \param index
  /// the vertex to replace
  ///
  /// \param point
  /// the vertex's value
  ///
  /// \return
  /// index where the vertex was positioned
  unsigned int setVertex(unsigned int index, const QPointF& point);

  /// Inserts a vertex into the the shape at the given position.
  //
  /// If the position does not exist, the new vertex is appended. The position
  /// must be less than maxVertices().
  ///
  /// \param index
  /// The position of the new vertex
  ///
  /// \param vertex
  /// the coordinates of the new vertex
  ///
  /// \return
  /// The actual position where the vertex was inserted or (uint)-1 on error
  virtual unsigned int insertVertex(int index, const QPointF& vertex);

  /// Removes a vertex from the shape at the given position.
  //
  /// If the position does not exist, an assertion error caused. Removal of a
  /// vertex may cause the shape to be NULL if the the number of vertices falls
  /// below minVertices().
  ///
  /// \param index
  /// The position of the new vertex
  virtual void removeVertex(unsigned int index);

  virtual QVector<QPointF> vertices() const;

  virtual QPointF vertex(unsigned int index) const;

  virtual void moveVertex(unsigned int index, const QPointF &offset);

  virtual void move(const QPointF& offset);

  virtual void intersect(const QRectF& rect);

  virtual void invalidate();

  virtual bool isNull() const;

  virtual unsigned int numVertices() const;

  virtual bool isOnEdge(const QPointF& point, unsigned int radius = 5) const;

  virtual void transform(const QTransform& tx);

  virtual void transform(const SRTTransform& tx, TransformDirection dir);

  virtual bool isInside(const QPointF& point) const;

  virtual double area() const;

  virtual QPointF center() const;

  virtual double perimeter() const;

  virtual bool isParametric() const;

  virtual QVector<QPointF> nonParametricVertices() const;

 private:
  QVector<QPointF> m_vertices;  ///< The vertices of the shape
};

/// Return the absolute value of the dot product of a and b.
SEDEEN_GEOMETRY_API
double absDot(const QPointF& a, const QPointF& b);

SEDEEN_GEOMETRY_API
void intersected(PolyShape &poly, const QRectF &rect, PolyShape &fill);

} // namespace sedeen

#endif
