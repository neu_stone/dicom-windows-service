#ifndef DPTK_SRC_DICOMLIB_DLLAPI_H
#define DPTK_SRC_DICOMLIB_DLLAPI_H

#include "DllApiControl.h"

#ifdef DPTK_dicomlib_EXPORT
#define DPTK_DICOMLIB_API SEDEEN_API_EXPORT
#else
#define DPTK_DICOMLIB_API SEDEEN_API_IMPORT
#endif

#endif
