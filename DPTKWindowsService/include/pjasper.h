/** \file libpjasper.h
 */


#ifndef LIBPJASPER_H
#define LIBPJASPER_H


/*******************************************************\
* Includes
\*******************************************************/
//#include "jpeglib.h"


/*******************************************************\
* Defines
\*******************************************************/
#ifdef __cplusplus
	#define EXTERN_C extern "C"
#else
	#define EXTERN_C extern
#endif


/*******************************************************\
* Imported Types
\*******************************************************/

/**	
 * Selective decoding structure type.
 * Type of pointer returned by rsa_dec_open_file() or rsa_dec_open_mem().
 */
#define sdec_t void


/**	
 *	Progressive encoding structure type.
 *	Type of pointer returned by rsa_enc_create().
 */
#define penc_t void


/** 
 *	Return code values.
 *	Values returned by decoding function and encoding funtions.
 */
typedef enum {
	// Decoder codes
	RSA_INSUFFICIENT_MEMORY,	///< Insufficient memory.
	RSA_READ_ERROR,				///< Error reading input file.
	RSA_WRITE_ERROR,			///< Error writting to output file.
	RSA_FORMAT_UNSUPPORTED,		///< Input or output image format is unsupported

	// Encoder specific codes
	RSA_ENC_SUCCESS,			///< An encoding opration finished successfully
	RSA_ENC_STOP,				///< An encoding opration was stoped
	RSA_ENC_ERROR,				///< An encoding opration was haulted because of errors

	// Decoder specific codes
	RSA_DEC_SUCCESS,			///< An decoding opration finished successfully
	RSA_DEC_ERROR,				///< An decoding opration was haulted because of errors
	RSA_DEC_INVALID_DECODER,	///< The decoder object used is invalid
	RSA_DEC_INVALID_REGION,		///< The requested region is not a valid region
	RSA_DEC_INVALID_TILE,		///< The requested tile does not exist
	RSA_DEC_INVALID_RESOLUTION	///< The requested resolution level does not exist
} rsa_codes_t;

/** 
 *	Color space code values.
 *	Values returned by rsa_dec_imageclrspc()
 */
typedef enum {
	// Decoder codes
	RSA_COLOR_GRAY,			///< Grayscale Color Space
	RSA_COLOR_RGB,			///< RGB Color Space
	RSA_COLOR_YCC,			///< YCbCr Color Space
	RSA_COLOR_ICC,			///< Restricted ICC profile is defined
	RSA_COLOR_UNKNOWN,		///< Unknown color space
} rsa_colors_t;


/*******************************************************\
* Progressive Encoding Functions
\*******************************************************/

/** 
 *	Gets the number of tiles that will be encoded.
 *	The result is only valid after a call to rsa_enc_encode().
 *	@return		the number of tiles in the files OR <0> on error, 
 */
EXTERN_C int rsa_enc_numtiles();


/** 
 *	Gets the color space code for the image.
 *	Color space codes are defined in rsa_codes_t struct.
 *
 *	@param		enc the encoding object received form rsa_enc_create() 
 *	@return		the color space code for the image 
 */
EXTERN_C rsa_colors_t rsa_dec_imageclrspc(const sdec_t *sdec);


/** 
 *	Gets the number of tiles encoded.
 *	The result is only valid after a call to rsa_enc_encode().
 *	Can be used to calculate the progress of the encoding process.
 *
 *	@param		enc the encoding object received form rsa_enc_create()
 *	@return		the number of tiles in the files OR <0> on error, 
 */
EXTERN_C int rsa_enc_numenctiles();


/** 
 *	Start the encoding process.
 *	Uses the encoding parameters that are supplied
 *	to perform the encoding process.
 *
 *	@param		enc the encoding object received form rsa_enc_create()
 *	@return		RSA_ENC_SUCCESS  or other rsa_codes_t value on error
 */
EXTERN_C rsa_codes_t rsa_enc_encode(penc_t * enc);

/** 
 *	Creates encoder object with default prameters.
 *	This structure needs to be passed to rsa_enc_encode().
 *	The default paramerters are:
 *		- tile size = 256 x 256
 *		- lazy mode enabled (i.e. arithmatic bypass)
 *		- lossless compression (i.e. rate = 1.0)
 *		- 6 resolution levels
 *		- the first image in the file is compressed
 *
 *	@param		infile the name of the file to be encoded
 *	@param		outfile the name of the encoded file (output filename)
 *	@return		pointer to <penc_t> encoding structure or <0> on error
 */
EXTERN_C penc_t* rsa_enc_create(const char* infile, const char* outfile);

/** 
 *	Destroys encoder object obtained from rsa_enc_create().
 *	This structure should be deleted after rsa_enc_encode().
 *
 *	@param		enc the encoder obeject returned by rsa_enc_create()
 */
EXTERN_C void rsa_enc_destroy(penc_t* enc);


/** 
 *	Terminates the encoding process already in progress.
 *	Encoding start once rsa_enc_encode() has been called and
 *	cannot be restarted after it has been terminated. This call
 *	has no effect if encoding process is finished or not started.
 */
EXTERN_C void rsa_enc_terminate();


/** 
 *	Sets the tile size parameters.
 *
 *	@param		enc the pointer to <penc_t> encoding structure obtained from rsa_enc_create()
 *	@param		tilew the new tile width
 *	@param		tileh new tile height
 *	@return		<1> on success or <0> on error
 */
EXTERN_C int rsa_enc_settilesize(penc_t* enc, int tilew, int tileh);

/** 
 *	Sets the encoding lossy paramters.
 *
 *	@param		enc the pointer to <penc_t> encoding structure obtained from rsa_enc_create()
 *	@param		lossy <false> for lossless coding OR <true> for lossy coding
 *	@param		rate the lossy encoding rate (0,1] (only used if lossy is <true>) 
 *	@return		<1> on success or <0> on error
 */
EXTERN_C int rsa_enc_setlossy(penc_t* enc, bool lossy, double rate);

/** 
 *	Sets the number of levels for encoding.
 *
 *	@param		enc the pointer to <penc_t> encoding structure obtained from rsa_enc_create()
 *	@param		levels the number of levels in JPEG 2000 file
 *	@return		<1> on success or <0> on error
 */
EXTERN_C int rsa_enc_setlevels(penc_t* enc, int levels);

/** 
 *	Creates encoder object with default prameters.
 *	This structure needs to be passed to rsa_enc_encode()
 *
 *	@param		enc the pointer to <penc_t> encoding structure obtained from rsa_enc_create()
 *	@param		imgnum the image to encode from input file (ie. TIFF files can have multiple images)
 *	@return		<1> on success or <0> on error
 */
EXTERN_C int rsa_enc_setimage(penc_t* enc, int imgnum);


/**	
 *	Set the lazy coding (i.e. arithmatic coding bypass).
 *
 *	@param		enc the pointer to <penc_t> encoding structure obtained from rsa_enc_create()
 *	@param		lazy true to enable lazy coding, else false
 *	@return		<1> on success or <0> on error
 */
EXTERN_C int rsa_enc_setlazy(penc_t* enc, bool lazy);



/*******************************************************\
* Progressive Decoding Functions
\***************************************************** */

/** 
 *	Gets the image width. 
 *
 *	@param sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@return	timage width OR <0> on error, 
 */
EXTERN_C unsigned int rsa_dec_imagewidth(const sdec_t *sdec);

/** 
 *	Gets the image height. 
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@return		image height OR <0> on error, 
 */
EXTERN_C unsigned int rsa_dec_imageheight(const sdec_t *sdec);

/** 
 *	Gets the number of components in the image. 
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@return		number of components OR <0> on error, 
 */
EXTERN_C unsigned int rsa_dec_numcomps(const sdec_t *sdec);

/** 
 *	Gets the number of resoltion levels in the image.
 *	The full resolution image is the 0th level. For <N> 
 *	resolution levels, there true resolution levels
 *	are in the range [0, N-1]
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@return		the number of resolution levels OR <0> on error, 
 */
EXTERN_C unsigned int rsa_dec_numrlvls(const sdec_t *sdec);

/** 
 *	Gets the nominal tile width. Tiles on the border 
 *	of the image have have smaller width than the 
 *	nominal tile width.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@return		tile width OR <0> on error, 
 */
EXTERN_C unsigned int rsa_dec_tilewidth(const sdec_t *sdec);

/** 
 *	Gets the nominal tile height. Tiles on the border 
 *	of the image have have smaller height than the 
 *	nominal tile height.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@return		tile height OR <0> on error, 
 */
EXTERN_C unsigned int rsa_dec_tileheight(const sdec_t *sdec);


/** 
 *	Gets the total number of tiles in the image.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@return		the number of tiles OR <0> on error, 
 */
EXTERN_C unsigned int rsa_dec_numtiles(const sdec_t *sdec);

/** 
 *	Gets the number of horizonal tiles in the image.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@return		the number of tiles OR <0> on error, 
 */
EXTERN_C unsigned int rsa_dec_numhtiles(const sdec_t *sdec);

/** 
 *	Gets the number of vertical tiles in the image.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@return		the number of tiles OR <0> on error, 
 */
EXTERN_C unsigned int rsa_dec_numvtiles(const sdec_t *sdec);

/** 
 *	Gets the horizonal image offset.
 *	With reference to the reference grid and it must be 
 *	greater than the horizonal tile offset.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@return		horizonal offset OR <0> on error, 
 */
EXTERN_C unsigned int rsa_dec_imagexoff(const sdec_t *sdec);

/** 
 *	Gets the  vertical image offset. 
 *	With reference to the reference grid and it must be 
 *	greater than the vertical tile offset.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@return		vertical offset OR <0> on error, 
 */
EXTERN_C unsigned int rsa_dec_imageyoff(const sdec_t *sdec);

/** 
 *	Gets the horizonal tile offset. 
 *	With reference to the reference grid and it must be 
 *	less than the horizontal tile offset.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@return		horizonal offset OR <0> on error, 
 */
EXTERN_C unsigned int rsa_dec_tilexoff(const sdec_t *sdec);

/** 
 *	Gets the  vertical tile offset. 
 *	With reference to the reference grid and it must be 
 *	less than the vertical tile offset.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@return		vertical offset OR <0> on error, 
 */
EXTERN_C unsigned int rsa_dec_tileyoff(const sdec_t *sdec);


/** 
 *	Gets the top-left x-coordinate of a tile.
 *	With reference to the reference grid.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@param		tileno the tile number of interest
 *	@return		coordinate OR (uint)(-1) on error, 
 */
EXTERN_C unsigned int rsa_dec_tiletlx(const sdec_t *sdec, int tileno);

/** 
 *	Gets the top-left y-coordinate of a tile.
 *	With reference to the reference grid.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@param		tileno the tile number of interest
 *	@return		coordinate OR (uint)(-1) on error, 
 */
EXTERN_C unsigned int rsa_dec_tiletly(const sdec_t *sdec, int tileno);

/** 
 *	Gets the bottom-right x-coordinate of a tile.
 *	With reference to the reference grid.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@param		tileno the tile number of interest
 *	@return		coordinate OR (uint)(-1) on error, 
 */
EXTERN_C unsigned int rsa_dec_tilebrx(const sdec_t *sdec, int tileno);

/** 
 *	Gets the bottom-right y-coordinate of a tile.
 *	With reference to the reference grid.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@param		tileno the tile number of interest
 *	@return		coordinate OR (uint)(-1) on error, 
 */
EXTERN_C unsigned int rsa_dec_tilebry(const sdec_t *sdec, int tileno);

/** 
 *	Gets the width of a component.
 *	Usefull is chroma subsampling is used.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@param		cmptno the component number of interest
 *	@return		width OR <0> on error, 
 */
EXTERN_C unsigned int rsa_dec_compwidth(const sdec_t *sdec, int cmptno);

/** 
 *	Gets the height of a component w.r.t the reference grid.
 *	Usefull is chroma subsampling is used.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@param		cmptno the component number of interest
 *	@return		height OR <0> on error, 
 */
EXTERN_C unsigned int rsa_dec_compheight(const sdec_t *sdec, int cmptno);

/** 
 *	Gets the component horizonal pixel spacing w.r.t the reference grid.
 *	Usefull is chroma subsampling is used.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@param		cmptno the component number of interest
 *	@return		spacing OR <0> on error, 
 */
EXTERN_C unsigned int rsa_dec_comphstep(const sdec_t *sdec, int cmptno);

/** 
 *	Gets the component vertical pixel spacing w.r.t the reference grid.
 *	Usefull is chroma subsampling is used.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@param		cmptno the component number of interest
 *	@return		spacing OR <0> on error, 
 */
EXTERN_C unsigned int rsa_dec_compvstep(const sdec_t *sdec, int cmptno);

/** 
 *	Gets the component percision. i.e. the number of bits ber component sample.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@param		cmptno the component number of interest
 *	@return		percision OR <0> on error, 
 */
EXTERN_C unsigned int rsa_dec_compprec(const sdec_t *sdec, int cmptno);

/** 
 *	Gets the signedness of the components.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@param		cmptno the component number of interest
 *	@return		<1> for signed , <0> for unsigned, (uint)(-1) on error, 
 */
EXTERN_C unsigned int rsa_dec_compsigned(const sdec_t *sdec, int cmptno);

/**	
 *	Gets the last decoded resolution. If called before
 *	anything has been decoded, the result is also <0>.
 *
 *	@param		sdec pointer to the decoder object returned from rsa_dec_open_file()
 *	@return		a resolution level OR <0> on error, 
 */
EXTERN_C unsigned int rsa_dec_decodedres(const sdec_t *sdec);


/**	
 *	Opens a file for selective decoding.
 *
 *	@param		infile the name of a JP2 or JPC codestream file to open
 *	@return		pointer to decoder object on success, <0> otherwise
 */
EXTERN_C sdec_t*	rsa_dec_open_file(const char* infile);

/**	
 *	Opens a JP2 memory buffer for reading
 *
 *	@param buf the memory buffer to read
 *	@param length the length of memory buffer
 *	@return retunrs <1> on success, <0> otherwise
 */
EXTERN_C sdec_t*	rsa_dec_open_jp2_buffer(char* buf, int length);

/**	
 *	Opens a JPC memory buffer for reading
 *
 *	@param buf the memory buffer to read
 *	@param length the length of memory buffer
 *  @param rsa_colors_t color_hint determine image color, JPC does not contain a color marker
 *	@return retunrs <1> on success, <0> otherwise
 */
EXTERN_C sdec_t*	rsa_dec_open_jpc_buffer(char* buf, int length, rsa_colors_t color_hint);

/**	Cleans up after the coded and closes an open file. 
 *
 *	@param		sdec pointer to the decoder object returned by rsa_dec_open_file() 
 */
EXTERN_C void rsa_dec_close(sdec_t *sdec);

/**	
 *	Determines if the JPEG 2000 image can be read. 
 *	There are two progression orders that are not supported 
 *	(PCRLPRG and CPRLPRG) and only RGB and Grayscale images 
 *	are supported.
 *
 *	@param		infile	the name of a JP2 or JPC codestream file
 *	@return		retunrs <1> on success, <0> otherwise
 */
EXTERN_C int rsa_dec_canread_file(const char* infile);

/**	
 *	Determines if the memory buffer can be read. 
 *	There are two progression orders that are not supported 
 *	(PCRLPRG and CPRLPRG) and only RGB and Grayscale images 
 *	are supported.
 *
 *	@param buf the memory buffer to read
 *	@param length the length of memory buffer
 *	@return retunrs <1> on success, <0> otherwise
 */
EXTERN_C int rsa_dec_canread_buffer(char *buf, int length);

/**	
 *	Decodes the specified tile at the specified resolution level.
 *
 *	@param		sdec pointer to the decoder object returned by rsa_dec_open_file()
 *	@param		rlvl the resolution level to decode
 *	@param		tileno the tile number to decode
 *	@return		RSA_DEC_SUCESS on success OR other rsa_codes_t on error
 */
EXTERN_C rsa_codes_t rsa_dec_decodetile(sdec_t *sdec, unsigned int rlvl, unsigned int tileno);

/**	
 *	Decodes the whole image at the specified resolution level.
 *
 *	@param		sdec pointer	to the decoder object, must be returned by rsa_dec_open_file()
 *	@param		rlvl the resolution level to decode
 *	@return		RSA_DEC_SUCESS on success OR other rsa_codes_t on error
 */
EXTERN_C rsa_codes_t rsa_dec_decodeimage(sdec_t *sdec, unsigned int rlvl);

/**	
 *	Decodes all tiles that contain pixels from the specified region.
 *	Decodes these tiles at the specified resolution level and the 
 *	region actually decoded is generally bigger than the requested 
 *	region (because whole tiles must be decoded).
 *	
 *		NOTES:
 *			-	For efficiency, consider regions that are aligned to 
 *				tile bounrdries. Otherwise when retrieving the image 
 *				an extra cropping step is required in order to extract
 *				the requested region from the decoded region. 
 *			-	There a host of function for calculating tile coordinates.
 *			-	The caller can determine the actual decoded region 
 *				by calling rsa_dec_decodedregion()
 *			-	rsa_dec_getcomp(), rsa_dec_getimage(), rsa_dec_getbitmap()
 *				functions return an image containing the specified region
 *				not the decoded region.
 *
 *	@param		sdec pointer to the decoder object, must be returned by rsa_dec_open_file()
 *	@param		rlvl the resolution level to decode
 *	@param		xstart the top-left x-coordiante of decode area in resolution of \c rlvl
 *	@param		ystart the top-left y-coordiante of decode area in resolution of \c rlvl
 *	@param		xend the bottom-right x-coordiante of decode area in resolution of \c rlvl
 *	@param		yend the bottom-right y-coordiante of decode area in resolution of \c rlvl
 *	@return		RSA_DEC_SUCESS on success OR other rsa_codes_t on error
 */
EXTERN_C rsa_codes_t rsa_dec_decoderegion(sdec_t *sdec, unsigned int rlvl, unsigned int xstart, unsigned int ystart, unsigned int xend, unsigned int yend);

/**	
 *	Returns the region what was last decoded with reference to the full resolution image.
 *	This can be used after a call to rsa_dec_decoderegion(), 
 *	rsa_dec_decodetile(), or rsa_dec_decodeimage().
 *
 *	@param		sdec pointer to the decoder object, must be returned by rsa_dec_open_file() 
 *	@param		xstart contains the top-left x-coordiante of the decoded region after function complete
 *	@param		ystart contains the top-left y-coordiante of the decoded region after function complete
 *	@param		xend contains the bottom-right x-coordiante of the decoded region after function complete
 *	@param		yend contains the bottom-right y-coordiante of the decoded region after function complete
 *	@return		<1> of sucess OR <0> on error
 */
EXTERN_C int rsa_dec_decodedregion(sdec_t *sdec, unsigned int* xstart, unsigned int* ystart, unsigned int* xend, unsigned int* yend);

/**	
 *	Returns the list of tiles that span the given region.
 *	The caller must free the returned buffer using rsa_dec_free().
 *
 *	@param		sdec pointer to the decoder object, must be returned by rsa_dec_open_file()
 *	@param		numtiles contains the number of tiles in region after call is complete
 *	@param		xstart the top-left x-coordiante of the ROI
 *	@param		ystart the top-left y-coordiante of the ROI
 *	@param		xend the bottom-right x-coordiante of the ROI
 *	@param		yend the bottom-right y-coordiante of the ROI
 *	@return					pointer to buffer containing the list of tiles OR <0> on error
 */
EXTERN_C unsigned int* rsa_dec_tilelist(const sdec_t *sdec, unsigned int* numtiles, unsigned int xstart,unsigned  int ystart, unsigned int xend, unsigned int yend);

/**	
 *	Return one component for the last decoded region, tile, or image.
 *	The returned buffer is a one dimentional array with <width>*<height>
 *	pixels.  The dimensions of the image depend on the resolution and 
 *	size of the last decoded region.
 *
 *	Notes:	
 *		- The caller must free the returned buffer using rsa_dec_free()
 *		- The number of dimentions can be found from rsa_dec_numcmpts()
 *
 *	@param		sdec pointer to the decoder object, must be returned by rsa_dec_open_file()
 *	@param		cmptno the component of interest
 *	@param		width pointer to a valid location to copy the width of returned image
 *	@param		height pointer to a valid location to copy the height of returned image
 *	@return		pointer to buffer containing the component or <0> on error.
 */
EXTERN_C int* rsa_dec_getcomp(const sdec_t *sdec, unsigned int cmptno, int *width, int *height);

/**	
 *	Returns the decoded image, tile, or region, which ever occured last.
 *	The returned buffer is a one dimentional array with 
 *	\a width * \a height * \a ncomps elements. The dimensions of the buffer 
 *	depend on the resolution and size of the last decoded region. For 
 *	multicomponent images, the components are interleaved, i.e. (rgb rgb ...). 
 *	
 *		Notes:	
 *			- The caller must free the returned buffer using rsa_dec_free()
 *			- YCbCr images with subsampling cannot be retrieved with this function
 *				- requires that all compoents have the same dimension
 *				- In this case use rsa_dec_getcomp() to retrieve each component.
 *
 *	@param		sdec pointer to the decoder object, must be returned by rsa_dec_open_file()
 *	@param		width pointer to a valid location to copy the width of returned buffer (unchanged on error)
 *	@param		height pointer to a valid location to copy the height of returned buffer (unchanged on error)
 *  @param      ncomps pointer to a valid location to copy the number of components of returned buffer (unchanged on error)
 *	@return		pointer to buffer containing the encoded image OR <0> on error
 */
EXTERN_C int* rsa_dec_getimage(const sdec_t *sdec, int *width, int *height, int *ncomps); 

/**	
 *	Returns in sRGB for grayscale image of the decoded image, tile, or region, which ever occured last.
 *	The returned buffer is a one dimentional array with \a width * \a height * \a ncomps elements.
 *	\a ncomps will be either 3 (for sRGB images) or 1 (for grayscale images). The dimensions of the image 
 *	depend on the resolution and size of the last decoded region. If the
 *	returned image is in sRGB format its components interleaved, i.e. (rgb rgb ...). 
 *	
 *		Notes:	
 *			- The caller must free the returned buffer using rsa_dec_free()
 *			- All color spaces are converted to RGB before being returned
 *
 *	@param		sdec pointer to the decoder object, must be returned by rsa_dec_open_file()
 *	@param		width pointer to a valid location to copy the width of returned image
 *	@param		height pointer to a valid location to copy the height of returned image
 *  @param      ncomps pointer to a valid location to copy the number of components of returned buffer (unchanged on error)
 *	@return		pointer to buffer containing the encoded image OR <0> on error
 */
EXTERN_C unsigned char* rsa_dec_getimageRGB(const sdec_t *sdec, int *width, int *height, int *ncomps); 


/**	
 *	Returns an BMP decoded image of the last decoded region, tile, 
 *	or image, which ever occured last. The returned buffer is a valid 
 *	BMP stream and can be directly written to file or loaded in memory.
 *
 *	Notes:	
 *		- The caller must free the returned buffer using rsa_dec_free()
 *
 *	@param		sdec pointer to the decoder object. must be returned by rsa_dec_open_file()
 *	@param		len pointer to a valid location to copy the length of returned buffer
 *	@return		pointer to buffer containing the encoded image OR <0> on error
 */
EXTERN_C unsigned char* rsa_dec_getbitmap(const sdec_t *sdec, unsigned int* len); 
 


/**	
 *	Frees memory returned by the coded. Simply calles
 *	free() on the provided pointer. 
 *
 *	@param		ptr pointer to a pointer returned by the coded
 */
EXTERN_C void rsa_dec_free(void *ptr);





#endif // LIBJASPER_H

