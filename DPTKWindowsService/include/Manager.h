/// Detailed description
/// 1. connect to DICOM SCP.
/// 2. send DICOM request to DICOM SCP
/// 3. call back to the observer when specific request/response arrive at DCM SCU side.

#ifndef DPDCMMGRUI_H
#define DPDCMMGRUI_H

#include "Global.h"
class CDcmtksSCU;
class DPDcmObserver;
class DataSet;

class DPTK_DICOMLIB_API Manager {
public:

  /**
   * Constructor.
     */
  Manager(const DPDcmObserver* pSCPObserver=NULL);
  
  /**
   * Destructor.
   */
  virtual ~Manager(void);
  
  /**
   * Connect to DCM server.
   * @param strAETitle: server's AE title
   * @param strIP: server's IP address.
   * @param nPort: server's port.
     * @param strSCUAETitle: client's AE title
     * @param bTransportLayer: the flag to add Transport Layer Security
   * @return : zero if successful; otherwise failed.
   */
  int connect(const char* strSCPAETitle, const char* strSCPIP, int nSCPPort,
              const char* strSCUAETitle, bool bTransportLayer=true);
              
  /**
  * Close the connection.
  * @return void.
  */
  void close();
  
  /**
   * CEchoe DCM class with DCM server.
   * @return : result of CEcho service.
   */
  int echo();
  
  /**
   * request CFind service from DICOM SCP side.
   * for example, Study Root Query/Retrieve Information Model might be
   * UID_FINDPatientRootQueryRetrieveInformationModel        "1.2.840.10008.5.1.4.1.2.1.1"
   * UID_MOVEPatientRootQueryRetrieveInformationModel        "1.2.840.10008.5.1.4.1.2.1.2"
   * UID_GETPatientRootQueryRetrieveInformationModel         "1.2.840.10008.5.1.4.1.2.1.3"
   * UID_FINDStudyRootQueryRetrieveInformationModel      "1.2.840.10008.5.1.4.1.2.2.1"
   * UID_MOVEStudyRootQueryRetrieveInformationModel      "1.2.840.10008.5.1.4.1.2.2.2"
   * UID_GETStudyRootQueryRetrieveInformationModel       "1.2.840.10008.5.1.4.1.2.2.3"
     * @param dcmDataset condition of the service.
   * @return status of the service at DCM SCP side.
   */
  int find(const DataSet& dcmAttrs);
  
  ///.
  int get(const DataSet& dcmAttrs);
  int create(const DataSet& dcmAttrs);
  int set(const DataSet& dcmAttrs);
  
  int action(int nActionID, const char* pstrSOPInstanceUID);
  
  ///.
  bool isWorking();
  
private:

  /// the instance of DICOM SCU
  CDcmtksSCU* m_pDcmtksSCU;
  
};

#endif // DPDCMMGRUI_H
